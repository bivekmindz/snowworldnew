-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 10, 2018 at 09:42 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `skiindia_snowworld`
--

DELIMITER $$
--
-- Procedures
--
CREATE  PROCEDURE `procedure_bookingquery_v` (IN `act_mode` VARCHAR(50), IN `email` VARCHAR(50), IN `bookingid` VARCHAR(50))  BEGIN
if(act_mode='selectbookingquery') then
select * from  tbl_orderpackage as t join tbl_user b on  b.user_id=t.userid where t.ticketid=bookingid   and  b.user_emailid=email;
end if;
END$$

CREATE  PROCEDURE `procedure_contactus_v` (IN `act_mode` VARCHAR(500), IN `Param1` VARCHAR(500), IN `Param2` VARCHAR(500), IN `Param3` VARCHAR(500), IN `Param4` VARCHAR(500), IN `Param5` VARCHAR(500), IN `Param6` VARCHAR(500), IN `Param7` VARCHAR(500), IN `Param8` VARCHAR(500), IN `Param9` VARCHAR(500), IN `Param10` VARCHAR(500), IN `Param11` VARCHAR(500), IN `Param12` VARCHAR(500), IN `Param13` VARCHAR(500))  BEGIN
#if(act_mode='selectcontact')then#insert into tbl_contact (cont_fname,cont_lname,cont_email,cont_phone,cont_message,cont_status,cont_addedon)values(fname,lname,email,phone,message,'1',NOW());#end if;
if(act_mode='paymentupdate') then
update  tbl_payment_gatway as p set p.pg_branchid=Param1, p.pg_merchant_id=Param2, p.pg_access_key=Param8,p.pg_working_key=Param3,p.pg_sucess_link=Param4,p.pg_fail_link=Param5,p.pg_currency=Param6,p.pg_language=Param7,p.pg_prefix=Param9,pg_agent_prefix=Param10,p.pg_updated=now() where p.pg_id=Param11;

end if;
END$$

CREATE  PROCEDURE `procedure_location` (IN `act_mode` VARCHAR(50), IN `locid` INT)  BEGIN
if(act_mode='s_viewactivityfrontend') then
select * from  tbl_activity as t where  t.activity_location=locid  and t.activity_status=1;
end if;

END$$

CREATE  PROCEDURE `proc_1` (IN `act_mode` VARCHAR(500), IN `Param1` BIGINT, IN `Param2` BIGINT, IN `Param3` VARCHAR(500), IN `Param4` VARCHAR(500), IN `Param5` VARCHAR(500), IN `Param6` VARCHAR(500))  BEGIN

 if(act_mode='update_pack_img_info')then
 update tbl_package_master set package_name=Param3,package_description=Param4,package_price=Param5,package_image=Param6 where package_id=Param1;
 end if;


if(act_mode='update_status_package')then
update tbl_daily_inventorypackages as d set `status`=Param2 where d.dailyinventorypackages_dailyinventoryid=Param1;
end if;

if(act_mode='update_pack_info')then
update tbl_package_master set package_name=Param3,package_description=Param4,package_price=Param5 where package_id=Param1;
end if;

if(act_mode='get_pack_info')then
select * from tbl_package_master as pm where pm.package_id=Param1;
end if;

if(act_mode='update_package_branch')then
update tbl_branch_package set bp_branchid=Param2 where bp_packageid=Param1;
end if;

if(act_mode='update_pack_status')then
update tbl_package_master set package_status=Param2 where package_id=Param1;
end if;




END$$

CREATE  PROCEDURE `proc_activity_s` (IN `act_mode` VARCHAR(50), IN `Param1` VARCHAR(50), IN `Param2` VARCHAR(50), IN `Param3` VARCHAR(50), IN `Param4` VARCHAR(50), IN `Param5` VARCHAR(50), IN `Param6` VARCHAR(50), IN `Param7` VARCHAR(50), IN `Param8` VARCHAR(50), IN `Param9` VARCHAR(50))  BEGIN
if(act_mode='s_addactivity') then
INSERT INTO `tbl_activity`(`activity_name`, `activity_discription`, `activity_price`, `activity_status`,activity_image,activity_location) VALUES (Param1,Param2,Param3,'1',Param4,Param5);
end if;


if(act_mode='s_addactivity_update') then
update tbl_activity as t set t.activity_name=Param1, t.activity_discription=Param2,t.activity_price=Param3,t.activity_modifiedon=NOW() where t.activity_id=Param4;
end if;


if(act_mode='s_viewactivity') then
select * from  tbl_activity as t where t.activity_status=1;
end if;


if(act_mode='delete_activity') then
update tbl_activity as t set t.activity_status='2' where activity_id=Param1;
end if;



END$$

CREATE  PROCEDURE `proc_addon` (IN `act_mode` INT, IN `branch_id` INT)  BEGIN
if(act_mode='addonscartses_package')then
select * from tbl_addon  where tbl_addon.addon_branchid=branch_id and addon_status=1 ;
end if;
if(act_mode='addonscart_package')then
select * from tbl_addon  where tbl_addon.addon_branchid=addon_id  and addon_status=1;
end if;
END$$

CREATE  PROCEDURE `proc_addon_s` (IN `act_mode` VARCHAR(50), IN `Param1` VARCHAR(50), IN `Param2` VARCHAR(50), IN `Param3` VARCHAR(50), IN `Param4` VARCHAR(50), IN `Param5` VARCHAR(50), IN `Param6` VARCHAR(50), IN `Param7` VARCHAR(50), IN `Param8` VARCHAR(50), IN `Param9` VARCHAR(50))  BEGIN
if(act_mode='addbranchAddon')then
INSERT INTO `tbl_addon`(`addon_name`, `addon_price`, `addon_status`,`addon_branchid`, `addon_description`,addon_image) VALUES (Param1,Param4,'1',Param2,Param3,Param5);
end if;

if(act_mode='s_viewaddon')then
select distinct(t.addon_name) from tbl_addon as t  where t.addon_status in (0,1);
end if;

if(act_mode='s_viewaddon_admin')then
select * from tbl_addon as t  where t.addon_status in (0,1);
end if;


if(act_mode='s_deleteaddon_admin')then
update tbl_addon as t set t.addon_status=2 where t.addon_id=Param1;
end if;

if(act_mode='s_statusaddon_admin')then
update tbl_addon as t set t.addon_status=Param2 where t.addon_id=Param1;
end if;

if(act_mode='addonviewupdate')then
select * from tbl_addon as t  where t.addon_id=Param1;
end if;


if(act_mode='addbranchAddon_update')then
update tbl_addon as t set t.addon_name=Param1,t.addon_price=Param4,t.addon_description=Param3,t.addon_modifiedon=now(),t.addon_branchid=Param5  where t.addon_id=Param2;
end if;

if(act_mode='addbranchAddon_update_pic')then
update tbl_addon as t set t.addon_name=Param1,t.addon_price=Param4,t.addon_description=Param3,t.addon_modifiedon=now(),t.addon_image=Param5,t.addon_branchid=Param6  where t.addon_id=Param2;
end if;

if(act_mode='test_del') then
delete from tempaddoncarttable where addonqty = 1  and addonid=Param1 and uniqid=Param2;
update tempaddoncarttable as t set t.addonprice=((t.addonprice)+((t.addonprice/(t.addonqty-1)))) where t.addonid=Param1 and t.uniqid=Param2;

select * from tempaddoncarttable as t where t.addonid=Param1 and t.uniqid=Param2;
end if;
END$$

CREATE  PROCEDURE `proc_Adminlogin` (IN `useremail` VARCHAR(255), IN `userpassword` VARCHAR(255), IN `act_mode` VARCHAR(250), IN `row_id` INT)  begin
if(act_mode='login')then
set @coun = (select count(*) from tbl_adminlogin where `UserName`=useremail and `Password`=userpassword and EmpStatus='A');
if(@coun > 0) then
select LoginID,UserName from tbl_adminlogin where `UserName`=useremail and `Password`=userpassword and EmpStatus='A' limit 0,1;
else 
select emp_main_id as LoginID,emp_email as UserName,emp_tbl_id as EmployeeId,emp_location as LocationId,(select t.branch_id from tbl_branch_master as t where tbl_employee.emp_location = t.branch_location limit 0,1) as branchid1,(select tloc.cityid from tbl_location as tloc where tbl_employee.emp_location = tloc.locationid limit 0,1) as cityid,(select tloc.stateid from tbl_location as tloc where tbl_employee.emp_location = tloc.locationid limit 0,1) as stateid,(select tloc.countryid from tbl_location as tloc where tbl_employee.emp_location = tloc.locationid limit 0,1) as countryid, tbl_employee.emp_branch_id as branchid from tbl_employee where `emp_email`=useremail and `emp_password`=userpassword and emp_status='1' limit 0,1;
end if ;
end if;
end$$

CREATE  PROCEDURE `proc_adminpasswordcheck` (IN `act_mode` VARCHAR(100), IN `n_email` VARCHAR(100), IN `oldpassword` VARCHAR(200), IN `row_id` INT)  begin
declare adminemailcount  INT;
if(act_mode='updatepassword') then
update tbl_adminlogin set `Password`=oldpassword where tbl_adminlogin.UserName=n_email and tbl_adminlogin.LoginID=row_id;
set adminemailcount=0;
select  adminemailcount;
end if;
if(act_mode='checkpassword') then
set adminemailcount = (select count(UserName) as email   from tbl_adminlogin where tbl_adminlogin.UserName=n_email and `Password`=oldpassword and LoginID=row_id);
select  adminemailcount;
end if;

end$$

CREATE  PROCEDURE `proc_agentorder` (IN `act_mode` VARCHAR(50), IN `orderid` INT, IN `tracking_id` VARCHAR(50), IN `order_status` VARCHAR(50), IN `status_message` TEXT, IN `paymentmode` VARCHAR(50), IN `paymentstatus` VARCHAR(50), IN `ordersucesmail` VARCHAR(50), IN `ordermailstatus` VARCHAR(50), IN `agent_id` INT, IN `cred_debet` VARCHAR(50), IN `amount` BIGINT, IN `bankagentcommision` VARCHAR(50))  BEGIN
if(act_mode='orderwalletupdatesucess') then
insert into tbl_agent_payment_history(ph_agent_id,ph_cred_debet_type,ph_amount,ph_timing,ph_orderid,ph_tracking_id,ph_order_status,ph_status_message,ph_paymentmode,ph_paymentstatus,ph_ordersucesmail,ph_ordermailstatus,ph_agentdiscount) values(agent_id,cred_debet,amount,NOW(),orderid,tracking_id,order_status,status_message,paymentmode,paymentstatus,ordersucesmail,ordermailstatus,bankagentcommision);

update tbl_agent_reg as t  set t.bank_creditamount=t.bank_creditamount-amount where t.agent_id=agent_id;

end if;

if(act_mode='selectwalletdata') then
select count(ph_agent_id) as agid from tbl_agent_payment_history;
end if;
if(act_mode='orderwalletselectagentamount') then
select *  from tbl_agent_payment_history  a join tbl_agent_reg b on a.ph_agent_id
=b.agent_id where a.ph_agent_id=agent_id ;
end if;

END$$

CREATE  PROCEDURE `proc_agent_payment` (IN `param` VARCHAR(30), IN `param1` VARCHAR(10), IN `param2` VARCHAR(10), IN `param3` VARCHAR(10), IN `param4` VARCHAR(10), IN `param5` TINYTEXT, IN `param6` TINYTEXT, IN `param7` TINYTEXT, IN `param8` VARCHAR(10), IN `param9` VARCHAR(10), IN `param10` VARCHAR(10), IN `param11` VARCHAR(40), IN `param12` VARCHAR(10), IN `param13` VARCHAR(10), IN `param14` VARCHAR(10), IN `param15` VARCHAR(10), IN `param16` VARCHAR(10), IN `param17` VARCHAR(200), IN `param18` VARCHAR(200), IN `param19` VARCHAR(15), IN `param20` TINYTEXT, IN `param21` TINYTEXT, IN `param22` VARCHAR(60), IN `param23` VARCHAR(60), IN `param24` VARCHAR(8), IN `param25` VARCHAR(30), IN `param26` VARCHAR(8), IN `param27` VARCHAR(40), IN `param28` VARCHAR(120), IN `param29` VARCHAR(120), IN `param30` VARCHAR(120), IN `param31` VARCHAR(10), IN `param32` VARCHAR(10), IN `param33` VARCHAR(10), IN `param34` VARCHAR(10), IN `param35` TINYTEXT, IN `param36` TINYTEXT, IN `param37` VARCHAR(40), IN `param38` VARCHAR(40), IN `param39` VARCHAR(40), IN `param40` VARCHAR(60), IN `param41` VARCHAR(60), IN `param42` VARCHAR(60), IN `param43` VARCHAR(120), IN `param44` VARCHAR(120), IN `param45` VARCHAR(5), IN `param46` VARCHAR(5), IN `param47` VARCHAR(5), IN `param48` VARCHAR(5), IN `param49` VARCHAR(5), IN `param50` VARCHAR(5), IN `param51` VARCHAR(5), IN `param52` VARCHAR(120), IN `param53` INT(11), IN `param54` INT(11), IN `param55` VARCHAR(255), IN `param56` INT(11), IN `param57` INT(11), IN `param58` VARCHAR(120), IN `param59` VARCHAR(3), IN `param60` VARCHAR(20), IN `param61` VARCHAR(200), IN `param62` VARCHAR(200), IN `param63` VARCHAR(200), IN `param64` INT(11), IN `param65` INT(11))  BEGIN

if(param = 'agent_payment') then

INSERT INTO `tbl_orderpackage`(`userid`, `billing_by`, `promocodeprice`, `total`, `waletAmount`, `subtotal`, `addedon`, `countryid`, `stateid`, `cityid`, `branch_id`,`locationid`, `departuredate`, `paymentstatus`, `paymentmode`, `ticketid`, `tracking_id`, `packproductname`,`packimg`,`package_qty`,`package_price`,`packpkg`,`packprice`,`orderstatus`,`op_ticket_print_status`, `op_printed_date`,`internethandlingcharges`,`op_usertype`,`order_status`,`status_message`,`paymenttype`,`title`,`billing_name`,`billing_email`,`billing_tel`,`billing_address`,`billing_city`,`billing_state`,`billing_zip`,`billing_country`,`billing_pdf`,`ordersucesmail`,`ordermailstatus`,`departuretime`,`frommin`,`tohrs`,`tomin`,`txtfromd`,`txttod`,`p_codename`,`op_ordertrackingupdate`,`order_status_date_time`,`op_orderjason`,`op_billing_cometoknow`,`billing_promocode`)VALUES(param64,param65,param10,param8,param37,param38,now(),param39,param40,param41,param42,param43,param27,param52,param53,param58,'NULL',param5,param6,param55,param7,param1,param2,1,0,'',param12,'Partner','NULL','NULL','NULL',param16,param17,param18,param19,param21,param22,param23,param24,param25,'','','',param45,param46,param47,param48,param49,param50,param11,'',now(),'',param20, '');

SELECT last_insert_id() as lastinsert_id;

end if;

if(param = 'insert_addon') then

INSERT INTO `tbl_orderaddone`( `addonename`, `addonevisiter`, `addoneprice`, `addonqty`, `lastid`, `addonid`, `Type`, `orderaddon_print_status`, `orderaddon_print_quantity`, `addon_image`) VALUES (param52,param1,param54,param53,param57, param56,'addon','','',param55);

SELECT last_insert_id() as lastinsert_addon_id;

end if;


if(param = 'insert_promo_data') then

INSERT INTO `tbl_user_promo`( `uid`, `p_id`, `inventory_id`, `created_on`) VALUES (param65,param56,param35,now());

SELECT last_insert_id() as lastinsert_userpromo_id;

end if;


if(param = 'insert_payment_history') then

INSERT INTO `tbl_agent_payment_history` ( `ph_agent_id`, `ph_tracking_id`, `ph_order_status`, `ph_status_message`, `ph_mer_amount`, `ph_wid`, `ph_paymentmode`) 
VALUES (param65,param58,param59,param60,param64,param65,param63);

SELECT last_insert_id() as lastinsert_paymenthistory_id;

end if;


if(param = 'get_wallet_amount') then

SELECT * FROM `tbl_agent_wallet` WHERE `user_id` = param65 ORDER BY `wid` DESC LIMIT 1;

end if;

if(param = 'insert_agent_customer_details') then

INSERT INTO `tbl_user`( `user_firstname`, `user_lastname`, `user_emailid`, `user_password`, `user_mobileno`, `user_status`,
`user_createdon`, `user_modifiedon`, `user_Address`, `user_varified`, `user_town`, `user_zip`, `user_country`, `user_title`, `user_dob`,
`user_state`, `user_city`, `user_pincodes`, `user_usertype`, `oauth_id`, `oauth_provider`, `oauth_link`, `oauth_picture_link`, `oauth_type`, 
`user_regdatafill`, `created_by`) VALUES (param17,'',param18,'',param19,'agent_customer',
now(),'',param21,1,param22,param24,param25,param16,'',
param23,param22,param24,'agent_customer','','','','','',
'',param65);

SELECT last_insert_id() as lastinsert_agentcustomer_id;

end if;


if(param = 'insert_agentcommission_details' ) then

INSERT INTO `tbl_user_commission_detail`(`uid`,`oid`,`order_price`,`status`,`created_on`) VALUES (param65, param32, param8, 'P', now() );

SELECT last_insert_id() as lastinsert_agentcommission_id;

end if;

END$$

CREATE  PROCEDURE `proc_agent_s` (IN `act_mode` VARCHAR(50), IN `Param1` VARCHAR(50), IN `Param2` VARCHAR(50), IN `Param3` VARCHAR(50), IN `Param4` VARCHAR(50), IN `Param5` VARCHAR(50), IN `Param6` VARCHAR(50), IN `Param7` VARCHAR(50), IN `Param8` VARCHAR(50), IN `Param9` VARCHAR(50))  BEGIN

    if(act_mode='agentamount') then
    select * from tbl_agent_payment_history as t  join tbl_agent_reg as tr on t.ph_agent_id=tr.agent_id where t.ph_agent_id=Param1 order by t.ph_id desc ;
    end if;


    if(act_mode='agentamount_new') then
    select * from tbl_agent_wallet as tr 
    where tr.user_id=Param1  order by tr.wid desc ;
    end if;


    if (act_mode='agent_last_amount')then
    select aw.credit_debit_amount as leftmoney from tbl_agent_wallet as aw where aw.`Status`=1 and aw.user_id=Param1   ;
    end if;



    if(act_mode='agentamount_show') then
     
    select * from tbl_agent_wallet as tr 
    where tr.user_id=Param1 and `status`='1' order by tr.wid desc limit 1 ;
    end if;

    if(act_mode='agentamount_show_1') then
    set @ch=concat('select count(user_id) from  tbl_agent_wallet where user_id=',Param1);
    select @ch;
    end if;


    if(act_mode='agentamount_add_new_credit') then
    set @chec=(select count(user_id) from  tbl_agent_wallet where user_id=Param1);
    if(@chec=0) then
    INSERT INTO `tbl_agent_wallet`(`user_id`, `credit_debit_amount`, `credit_debit_pety`, `Status`, `paymentby`,`total_amount`) 
    VALUES (Param1,Param2,Param3,'1',Param5,Param2);
    select last_insert_id() as last_id;
    else

    set @checks=(select total_amount from  tbl_agent_wallet where user_id=Param1 order by wid DESC limit 1);
    #select @checks,@chec,Param2,@checks+Param2;    INSERT INTO `tbl_agent_wallet`(`user_id`, `credit_debit_amount`, `credit_debit_pety`, `Status`, `paymentby`) 
    VALUES (Param1,Param2,Param3,Param4,Param5);
    set @last_id=last_insert_id();




    UPDATE `tbl_agent_wallet` SET `Status`='0' WHERE user_id=Param1;
    UPDATE `tbl_agent_wallet` SET  total_amount=(@checks+Param2),`Status`='1' WHERE wid=@last_id;
    end if;
    end if;

    if(act_mode='agentamount_add_new_debit') then
    set @chec=(select count(user_id) from  tbl_agent_wallet where user_id=Param1 
    and credit_debit_pety='C' 

    );
    if(@chec=0) then

    select 'No Amount in  for debit';

    else
    set @de_amount=(select total_amount from  tbl_agent_wallet where user_id=Param1 and `Status`='1' order by wid DESC limit 1);
    if(@de_amount>=Param2) then

    INSERT INTO `tbl_agent_wallet`(`user_id`, `credit_debit_amount`, `credit_debit_pety`, `Status`, `paymentby`) 
    VALUES (Param1,Param2,Param3,Param4,Param5);
    set @last_id=last_insert_id();

    UPDATE `tbl_agent_wallet` SET `Status`='0' WHERE user_id=Param1;

    UPDATE `tbl_agent_wallet` SET `total_amount`=@de_amount-Param2,`Status`='1' WHERE wid=@last_id;
    
    select last_insert_id() as last_id;
    
    else
    select'Amount is  grater then debit amount'Param2 ;
    end if;
    end if;
    end if;


    if(act_mode='agentamount_add') then
    update tbl_agent_reg as th set th.bank_creditamount = (th.bank_creditamount + Param2 ) where th.agent_id=Param1;
    INSERT INTO `tbl_agent_payment_history`(`ph_agent_id`, `ph_cred_debet_type`,`ph_amount`, `ph_timing`, `ph_agentdiscount`) values (Param1,'Credit',Param2,now(),'0'); 
    end if;

    if(act_mode='agentamount_sub') then
    update tbl_agent_reg as th set th.bank_creditamount = (th.bank_creditamount - Param2 ) where th.agent_id=Param1;
    INSERT INTO `tbl_agent_payment_history`(`ph_agent_id`, `ph_cred_debet_type`,`ph_amount`, `ph_timing`, `ph_agentdiscount`) values (Param1,'Debet',Param2,now(),'0'); 
    end if;


    if(act_mode='agentamount_comission') then
    update tbl_agent_reg as th set th.bank_agentcommision=Param2 where th.agent_id=Param1;
    end if;



    if(act_mode='agentamount_add_new_history') then

    INSERT INTO tbl_agent_payment_history( `ph_agent_id`, `ph_tracking_id`, `ph_order_status`, `ph_status_message`,`ph_paymentmode`,`ph_mer_amount`, `ph_wallet_amount`, `ph_wid`) VALUES ( Param1, Param2, Param3, Param4, Param5, Param6, Param8, Param7);
    end if;

    if(act_mode='agentamount_select_new_history') then

    select * from tbl_agent_payment_history where ph_agent_id=Param1 order by ph_id desc limit 0,1 ;
    end if;


    END$$

CREATE  PROCEDURE `proc_agent_wallet` (IN `act_mode` VARCHAR(60), IN `param` INT(8), IN `param1` VARCHAR(30))  BEGIN

if(act_mode='get_agent_wallet') then

SELECT * FROM `tbl_agent_wallet` WHERE `user_id` = param ORDER BY `wid` DESC LIMIT 1;

end if;

END$$

CREATE  PROCEDURE `proc_assignrole` (IN `p_role` VARCHAR(512), IN `p_user` VARCHAR(512), IN `p_menu` VARCHAR(512), IN `p_submenu` VARCHAR(512), IN `act_mode` VARCHAR(512), IN `start` VARCHAR(512), IN `total_rows` VARCHAR(512))  begin 
if(act_mode='insert') then
insert into tblassignrole(role_id,emp_id,main_menu,sub_menu) values(p_role,p_user,p_menu,p_submenu);
end if;
if(act_mode='viewemployee') then
select tm.*,tr.rolename from tblemployee as tm inner join tblrole as tr on tm.Role=tr.id where tm.t_status='Active';	
end if;
if(act_mode='viewsalesemployee') then
select tm.*,tr.rolename from tblemployee as tm inner join tblrole as tr on tm.Role=tr.id where tm.t_status='Active' and tm.Role='15';	
end if;
if(act_mode='assign') then
select trole.rolename,temp.F_Name,temp.L_Name,temp.ContactNo,temp.Email,tm.menuname,tass.sub_menu from tblrole as trole inner join tblemployee as temp on trole.id=temp.Role inner join tblassignrole as tass on trole.id=tass.role_id inner join tblmenu as tm on tass.main_menu=tm.id;
end if;
if(act_mode='editemployee') then 
select tm.*,tr.rolename from tblemployee as tm inner join tblrole as tr on tm.Role=tr.id where tm.EmployeeID=p_user;	
end if;
if(act_mode='viewempmanu') then 
select tm.firstname,tm.lastname from tbl_manufacturermaster as tm where tm.id=p_user;
end if;
if(act_mode='viewexistemployee') then
select * from tbl_manufacturermaster where tbl_manufacturermaster.vendortype='retail' and tbl_manufacturermaster.rstatus!='D' and FIND_IN_SET(tbl_manufacturermaster.id, p_submenu) order by tbl_manufacturermaster.firstname;
end if;
if(act_mode='fetchempretailers') then
select * from tblemployee as e where e.EmployeeID=p_user;
end if;
if(act_mode='fetchemployeedetail') then
select e.F_Name,e.L_Name,e.Email from tblemployee as e where e.EmployeeID=p_user;
end if;
if(act_mode='fetchretailerdetail') then
select * from tbl_manufacturermaster as m where m.id=p_user;
end if;

if(act_mode='fetchtotalorderamt') then
select * from tbl_incentive_logs as l where date(l.incentive_createdon) between p_role and p_submenu and l.retailer_id=p_user;
end if;
if(act_mode='fetchincentivestatus') then
select * from tbl_incentive_logs as l where date(l.incentive_createdon) between p_role and p_submenu and l.retailer_id in (p_menu);
end if;
if(act_mode='changeincentivestatus') then
update tbl_incentive_logs as l set l.incentive_status=`start` where date(l.incentive_createdon) between p_role and p_submenu  
and find_in_set(l.retailer_id,p_menu);
end if;
if(act_mode='fetchempincentives') then
select * from tbl_incentive_slabs as s where total_rows>=s.range_from and total_rows<=s.range_to and p_submenu>=s.range_salaryFrom and p_submenu<=s.range_salaryTo and s.range_status='1';
end if;
if(act_mode='viewincentiveslabs') then
select * from tbl_incentive_slabs;
end if;
if(act_mode='insertincentiveslabs') then
insert into tbl_incentive_slabs(range_from,range_to,range_type,range_incentive,range_salaryFrom,range_salaryTo) 
values(p_user,p_menu,p_submenu,total_rows,p_role,`start`);
end if;
if(act_mode='updateincentiveslab') then
update tbl_incentive_slabs as s set s.range_from=p_user,s.range_to=p_menu,s.range_salaryFrom=p_submenu,s.range_incentive=total_rows,s.range_salaryTo=`start`
where s.range_id=p_role;
end if;
if(act_mode='deleteincentiveslab') then
delete from tbl_incentive_slabs where range_id=p_role;
end if;
if(act_mode='fetchslabdetail') then
select * from tbl_incentive_slabs where range_id = p_user;
end if;
if(act_mode='remexistretail') then
update tblemployee set e_retailid=p_menu where EmployeeID=p_user;
end if;
end$$

CREATE  PROCEDURE `proc_bookingtype_v` (IN `act_mode` VARCHAR(50), IN `countryid` VARCHAR(50), IN `stateid` VARCHAR(50), IN `cityid` INT, IN `branch_id` INT, IN `locationid` INT)  BEGIN
if(act_mode='selectbookingtype') then
select bookingtypeid,bookingname  from  tbl_bookingtypemaster  where booking_branchid=branch_id and booking_status=1;
end if;


if(act_mode='viewbookingtype_admin') then
select *  from  tbl_bookingtypemaster as t where t.booking_status in (0,1) ;
end if;


if(act_mode='addbookingtype_admin') then
insert into tbl_bookingtypemaster (bookingname,booking_branchid) values (stateid,countryid);
end if;

if(act_mode='deletebookingtype_admin') then
update tbl_bookingtypemaster as t set t.booking_status=2 where t.bookingtypeid=countryid;
end if;


END$$

CREATE  PROCEDURE `proc_branch_s` (IN `act_mode` VARCHAR(50), IN `Param1` VARCHAR(50), IN `Param2` VARCHAR(50), IN `Param3` VARCHAR(50), IN `Param4` VARCHAR(50), IN `Param5` VARCHAR(50), IN `Param6` VARCHAR(50), IN `Param7` VARCHAR(50), IN `Param8` TEXT, IN `Param9` VARCHAR(50), IN `Param10` VARCHAR(50), IN `Param11` VARCHAR(50), IN `Param12` VARCHAR(50), IN `Param13` VARCHAR(50), IN `Param14` VARCHAR(50), IN `Param15` VARCHAR(50))  BEGIN
if(act_mode='s_addbranch') then
INSERT INTO `tbl_branch_master`( `branch_name`, `branch_company`, `branch_location`, `branch_status`,branch_logo,branch_background,branch_internet_handling_charge,branch_url,branch_add,branch_cronejob,branch_hrsrestriction,branch_minrestriction,branch_cronjobrunvalue)  VALUES (Param3,Param2,Param1,'1',Param4,Param5,Param6,Param7,Param8,Param10,Param11,Param12,Param13);
end if;

if(act_mode='s_viewbranch') then
select * from  tbl_branch_master as t where t.branch_status in (0,1);
end if;

if(act_mode='s_viewbranch_update') then
select * from  tbl_branch_master as t where t.branch_status in (0,1) and t.branch_id=Param1;
end if;

if(act_mode='branchstatus') then
update  tbl_branch_master as t set t.branch_status=Param2 where t.branch_id=Param1;
end if;

if(act_mode='delete_branch') then
update tbl_branch_master as t set t.branch_status='2' where t.branch_id=Param1;
end if;


if(act_mode='s_updatebranch') then
update tbl_branch_master as t set t.branch_location=Param1, t.branch_company=Param2 , t.branch_name=Param3 ,t.branch_modified=NOW() where t.branch_id=Param4;
end if;


if(act_mode='s_addbranch_update') then
update tbl_branch_master as t set t.branch_location=Param1, t.branch_company=Param2 , t.branch_name=Param3 ,t.branch_modified=NOW(),t.branch_background=Param5,t.branch_internet_handling_charge=Param6,t.branch_url=Param7,t.branch_add=Param8,t.branch_cronejob=Param10,t.branch_hrsrestriction=Param11,t.branch_minrestriction=Param12 ,t.branch_cronjobrunvalue=Param13 where  t.branch_id=Param9;
end if;

END$$

CREATE  PROCEDURE `proc_cartaddons_v` (IN `act_mode` VARCHAR(50), IN `branchid` INT, IN `addonid` INT, IN `addonqty` INT)  BEGIN
if(act_mode='addonscart_package') then
set @addqty=addonqty;
select addon_id,addon_name,addon_price,addon_image,@addqty as addonequantity from tbl_addon where addon_id=addonid ;
end if;

if(act_mode='update_status_package')then
update tbl_daily_inventorypackages  as dp set `status`=addonid where dp.dailyinventorypackages_id=branchid;
end if;

END$$

CREATE  PROCEDURE `proc_ccavenue_v` (IN `act_mode` VARCHAR(50), IN `branchid` VARCHAR(50))  BEGIN
if(act_mode='selectccavenue') then

select * from tbl_payment_gatway where pg_branchid=branchid and pg_status=1 order by pg_id desc limit 0,1 ;
end if;
END$$

CREATE  PROCEDURE `proc_changePassword` (IN `UserId` BIGINT(20), IN `newpassword` VARCHAR(200))  BEGIN
 
 UPDATE tbl_manufacturermaster SET `password` = newpassword WHERE id = UserId;
select "success";	
end$$

CREATE  PROCEDURE `proc_country_s` (IN `act_mode` VARCHAR(50), IN `Param1` VARCHAR(50), IN `Param2` VARCHAR(50), IN `Param3` VARCHAR(50), IN `Param4` VARCHAR(50), IN `Param5` VARCHAR(50), IN `Param6` VARCHAR(50), IN `Param7` VARCHAR(50), IN `Param8` VARCHAR(50), IN `Param9` VARCHAR(50))  BEGIN

if(act_mode='s_addcompany') then
INSERT INTO `tbl_company_master`(`comp_name`, `comp_status`) VALUES (Param1,'1');
end if;

if(act_mode='s_updatecompany') then
update tbl_company_master as t set t.comp_name=Param1 where t.comp_id=Param2;
end if;

if(act_mode='s_viewcompany') then
select * from  tbl_company_master as t ;
end if;

if(act_mode='s_deletetcompany') then
delete from tbl_company_master where comp_id=Param1;
end if;

if(act_mode='change_comp_status')then
update tbl_company_master set comp_status=Param2 where comp_id=Param1;
end if;

END$$

CREATE  PROCEDURE `proc_crone_v` (IN `act_mode` VARCHAR(50), IN `nextDate` VARCHAR(50), IN `starttime` VARCHAR(50), IN `branchid` VARCHAR(50), IN `dailyinventory_to` VARCHAR(50), IN `dailyinventory_seats` VARCHAR(50), IN `dailyinventory_status` VARCHAR(50), IN `dailyinventory_createdon` VARCHAR(50), IN `dailyinventory_modifiedon` VARCHAR(50), IN `dailyinventory_minfrom` VARCHAR(50), IN `dailyinventory_minto` VARCHAR(50))  BEGIN
if(act_mode='selectdate') then
select * from  tbl_daily_inventory as t where t.dailyinventory_date=nextDate and dailyinventory_from=starttime and dailyinventory_branchid=branchid;
end if;

if(act_mode='insertdate') then

INSERT INTO tbl_daily_inventory(dailyinventory_branchid, dailyinventory_from,dailyinventory_date,dailyinventory_to,dailyinventory_seats,dailyinventory_status,dailyinventory_createdon,dailyinventory_modifiedon,dailyinventory_minfrom,dailyinventory_minto) VALUES (branchid,starttime,nextDate,dailyinventory_to,dailyinventory_seats,dailyinventory_status,dailyinventory_createdon,dailyinventory_modifiedon,dailyinventory_minfrom,dailyinventory_minto);
select last_insert_id() as dd;
end if;

if(act_mode='s_viewtimeslotf') then
select * ,(SELECT count(t1.pacorderid) from `tbl_orderpackage` as t1 WHERE (t1.departuretime = t.dailyinventory_from || t1.departuretime = (CONCAT('0',(t.dailyinventory_from-12)))) and t1.departuredate=dailyinventory_to and (t1.departuretime=starttime || t1.departuretime = (CONCAT('0',(starttime-12))))  and t1.order_status!='Aborted') as coun from  tbl_daily_inventory as t where t.dailyinventory_date=nextDate and (dailyinventory_from=starttime || dailyinventory_from = (CONCAT('0',(starttime-12)))) and dailyinventory_branchid=branchid;
end if;

if(act_mode='insertdatepackage') then
INSERT INTO tbl_daily_inventorypackages (`dailyinventorypackages_dailyinventoryid`, `dailyinventorypackages_packagesid`, `status`) VALUES ( nextDate, starttime, 1);

end if;
if(act_mode='updateaddonedate') then
update tbl_daily_inventory as t set t.dailyinventory_addons=starttime where t.dailyinventory_id=nextDate;

end if;
if(act_mode='s_viewtimeslotdate') then
SELECT (DATEDIFF(dailyinventory_date, nextDate)) as date from  tbl_daily_inventory where dailyinventory_branchid=branchid  order by dailyinventory_id desc limit 0,1;

end if;
END$$

CREATE  PROCEDURE `proc_dashboardview` (IN `act_mode` VARCHAR(50), IN `row_id` INT)  begin
if(act_mode='orderday') then
select *  from tblorder as torder where date(torder.OrderDate)=curdate() order by torder.OrderId desc;

end if;
if(act_mode='pricofday') then
select *  from tblorder as torder where date(torder.OrderDate)=curdate() order by torder.OrderId desc;
end if;

if(act_mode='total_ofmonth') then
select * from tblorder where MONTH(OrderDate)=MONTH(curdate()) order by OrderId desc;
end if;
if(act_mode='order_ofmonth') then
select * from tblorder where MONTH(OrderDate)=MONTH(curdate()) order by OrderId desc; 
end if;
if(act_mode='pending_order') then

select * from tblorder as tor inner join tblorderstatus as ts on tor.OrderId=ts.OrdId
where ts.ShipStatus='pending';  

end if;
if(act_mode='dispatch_order') then

select * from tblorder as tor inner join tblorderstatus as ts on tor.OrderId=ts.OrdId
where ts.ShipStatus='dispatch';   

end if;
if(act_mode='pending_paymentof') then
select * from tblorder where tblorder.PaymentStatus='unpaid' order by OrderId desc;
end if;

if(act_mode='brnad_live') then
select * from (select count(brandid) as brandcount  from tbl_productmapforlisting   group by BrandID)as t; 
end if;

if(act_mode='product_live') then

select * from tbl_product as tpro inner join tbl_productmap as tmap on tpro.proid=
tmap.proid where tmap.pstatus='A';

end if;

end$$

CREATE  PROCEDURE `proc_dashboard_data` (IN `act_mode` VARCHAR(100))  begin
if(act_mode = 'total_order_count') then 
select count(o.OrderId) as total from tblorder as o inner join tbl_manufacturermaster as man on 
man.id=o.UserId  where Date(o.OrderDate)=curdate();
end if;

if(act_mode = 'total_order_cod') then 
select count(o.OrderId) as total from tblorder as o inner join tbl_manufacturermaster as man on man.id=o.UserId  where o.PaymentMode='COD';
end if;

if(act_mode = 'total_order_cheque') then 
	select count(o.UserId) as total from tblorder as o inner join tbl_manufacturermaster as man on man.id=o.UserId  where o.PaymentMode='CHEQUE';
end if;

if(act_mode = 'total_order_online') then 
	select count(o.UserId) as total from tblorder as o inner join tbl_manufacturermaster as man on man.id=o.UserId  where o.PaymentMode='ONLINE';
end if;

if(act_mode = 'total_order_credit') then 
	select count(o.UserId) as total from tblorder as o inner join tbl_manufacturermaster as man on man.id=o.UserId  where o.PaymentMode='CREDIT';
end if;

if(act_mode = 'total_orders') then 
	select * from tblorder o order by o.OrderId desc limit 0,5;
end if;

if(act_mode = 'manu_count') then 
	select count(*) as total from tbl_manufacturermaster r where r.vendortype='manufacturer';
end if;

if(act_mode = 'manu_list') then 
	select *  from tbl_manufacturermaster r where r.vendortype='manufacturer' order by r.id desc limit 0,5;
end if;

if(act_mode = 'retail_count') then 
	select count(*) as total from tbl_manufacturermaster r where r.vendortype='retail';
end if;

if(act_mode = 'consumer_count') then 
  select count(*) as total from tbl_manufacturermaster as man inner join tbl_state as st on st.stateid=man.stateid inner join tbl_city as ct on ct.cityid=man.city where man.vendortype='consumer' and man.rstatus='A';
end if;

if(act_mode = 'brand_count') then 
	select count(*) as total from tbl_brand r;
end if;
if(act_mode='order_ofmonth') then
select count(o.OrderId) as total from tblorder as o inner join tbl_manufacturermaster as man on 
man.id=o.UserId  where MONTH(o.OrderDate) = MONTH(CURRENT_DATE) AND YEAR(o.OrderDate) = YEAR(CURRENT_DATE);
end if;

if(act_mode='total_ofmonth') then
select sum(o.TotalAmt) as total from tblorder as o inner join tbl_manufacturermaster as man on 
man.id=o.UserId  where MONTH(o.OrderDate) = MONTH(CURRENT_DATE) AND YEAR(o.OrderDate) = YEAR(CURRENT_DATE);
end if;

if(act_mode='pending_order') then
select count(distinct(tos.OrdId)) as total from tblorder as o inner join tblorderstatus as tos on 
tos.OrdId=o.OrderId inner join tbl_manufacturermaster as man on 
man.id=o.UserId where tos.ShipStatus='pending' and tos.IsActive=1;
end if;

if(act_mode='dispatch_order') then
select count(distinct(tos.OrdId)) as total from tblorder as o inner join tblorderstatus as tos on 
tos.OrdId=o.OrderId inner join tbl_manufacturermaster as man on 
man.id=o.UserId where tos.ShipStatus='dispatch' and tos.IsActive=1;
end if;

if(act_mode='pending_payment') then

select count(*) as total from tblorderstatus where tblorderstatus.ShipStatus='pending';  

end if;
if(act_mode='product_live') then

select count(t.idcount) as total from (select count(id) as idcount  
from tbl_productmapforlisting   group by SKUNO)as t; 

end if;

if(act_mode='brnad_live') then
select count(distinct(t.BrandID)) as total from tbl_productmapforlisting as t;
end if;

if(act_mode='pric_ofday') then
select sum(o.TotalAmt) as total from tblorder as o inner join tbl_manufacturermaster as man on 
man.id=o.UserId  where Date(o.OrderDate)=curdate() order by o.OrderId desc;
end if;

if(act_mode='pending_paymentof') then
select count(o.TotalAmt) as total from tblorder as o inner join tbl_manufacturermaster as man on 
man.id=o.UserId  where o.PaymentStatus='unpaid';
end if;


end$$

CREATE  PROCEDURE `proc_forgetpassword` (IN `act_mode` VARCHAR(50), IN `email` VARCHAR(50), IN `Param3` VARCHAR(50))  BEGIN
if(act_mode = 'forgetpassword') then 
select * from tbl_user where user_emailid=email and user_usertype='User';
end if;

if(act_mode = 'forgetpassword_newpass') then 
update tbl_user as t SET t.user_password=Param3 where t.user_emailid=email;
end if;


if(act_mode = 'forgetpassword_newpassagent') then 

update tbl_agent_reg as t SET t.agent_password=Param3 where t.agent_username=email;


end if;

if(act_mode = 'forgetpasswordagent') then 
select * from tbl_agent_reg where agent_username=email;
end if;

END$$

CREATE  PROCEDURE `proc_frontuser_s` (IN `act_mode` VARCHAR(50), IN `Param1` VARCHAR(50), IN `Param2` VARCHAR(50), IN `Param3` VARCHAR(50), IN `Param4` VARCHAR(50), IN `Param5` VARCHAR(50), IN `Param6` VARCHAR(50), IN `Param7` VARCHAR(50), IN `Param8` VARCHAR(50), IN `Param9` VARCHAR(50))  BEGIN

if(act_mode = 's_viewfrontuser') then 
select * from tbl_user order by user_id desc;
end if;

END$$

CREATE  PROCEDURE `proc_geographic` (IN `act_mode` VARCHAR(100), IN `row_id` INT, IN `counname` VARCHAR(100), IN `coucode` VARCHAR(50), IN `commid` INT)  begin

if(act_mode='getstate')then
select * from tbl_state as t where t.stateid=row_id and t.isdeleted!='2';
end if;

if(act_mode='getcity')then
select * from tbl_city as t where t.cityid=row_id and t.cstatus!='2';
end if;

if(act_mode='locationdelete')then
update tbl_location as t set t.lstatus='2' where t.locationid=row_id;  
end if;

if(act_mode='de_to_act')then
update tbl_location as t set t.lstatus='1' where t.locationid=row_id;  
end if;

if(act_mode='act_to_de')then
update tbl_location as t set t.lstatus='0' where t.locationid=row_id;  
end if;

if(act_mode='viewlocation')then
select * from tbl_location as t where t.lstatus in(0,1);  
end if;

if(act_mode='countryinsert')then
insert into tbl_country (name,code,counstatus,createdon)values(counname,coucode,'1',NOW());
end if;
if(act_mode='viewcountry')then
select * from tbl_country as tc where tc.counstatus in (0,1) order by tc.conid desc;
end if;
if(act_mode='viewcounid')then
select * from tbl_country where tbl_country.conid=row_id;
end if;
if(act_mode='countryupdate')then
update tbl_country set name=counname,code=coucode,modifiedon=NOW() where conid=row_id ;
end if;
if(act_mode='countrydelete')then
update tbl_country as tc set tc.counstatus='2' where tc.conid=row_id;
end if;
if(act_mode='activeandinactive') then
update tbl_country set tbl_country.counstatus='0' where tbl_country.conid=row_id;
end if;
if(act_mode='inactiveandinactive') then
update tbl_country set tbl_country.counstatus='1' where tbl_country.conid=row_id;
end if;
if(act_mode='checkcoun') then
select  count(*) as count from tbl_country where tbl_country.name=counname;
end if;
if(act_mode='countrystate')then
select * from tbl_state where tbl_state.countryid=row_id and tbl_state.isdeleted=1 order by tbl_state.statename;
end if;
if(act_mode='cityinsert')then
insert into tbl_city(stateid,cityname,cstatus,createdon) values (row_id,counname,'1',now());
end if;
if(act_mode='viewcity')then
if(commid='')then
select c.*,s.statename from tbl_city as c inner join tbl_state as s on s.stateid=c.stateid where c.cstatus in(0,1);
else
select c.*,s.statename from tbl_city as c inner join tbl_state as s on s.stateid=c.stateid where c.cstatus in(0,1) limit row_id,commid;
end if;
end if;
if(act_mode='citydelete')then
update tbl_city as t set t.cstatus='2' where t.cityid=row_id;
end if;
if(act_mode='activecity') then
update tbl_city set tbl_city.cstatus='0' where tbl_city.cityid=row_id;
end if;
if(act_mode='inactivecity') then
update tbl_city set tbl_city.cstatus='1' where tbl_city.cityid=row_id;
end if;
if(act_mode='viewcityid')then
select * from tbl_city where tbl_city.cityid=row_id;
end if;
if(act_mode='cityupdate')then
update tbl_city as t set t.stateid=coucode,t.cityname=counname,t.modifiedon=NOW() where cityid=row_id ;
end if;
if(act_mode='checkcitycoun') then
select  count(*) as count from tbl_city where tbl_city.cityname=counname;
end if;
if(act_mode='stateexist')then
select count(st.statename) as statecount from tbl_state as st where st.countryid=row_id and st.statename=counname;
end if;
if(act_mode='stateinsert')then
insert into tbl_state (countryid,statename,statecode,isdeleted,createdon) values (row_id,counname,coucode,'1',NOW());
end if;
if(act_mode='viewstate')then
select st.*,cun.name from tbl_state as st inner join tbl_country as cun on st.countryid=cun.conid where st.isdeleted in (0,1);
end if;
if(act_mode='statedelete')then
delete from tbl_state where tbl_state.stateid=row_id;
end if;
if(act_mode='stateactiveandinactive') then
update tbl_state as t set t.isdeleted='0',t.modifiedon=now() where t.stateid=row_id;
end if;
if(act_mode='stateinactiveandinactive') then
update tbl_state as t set t.isdeleted='1',t.modifiedon=now() where t.stateid=row_id;
end if;
if(act_mode='vieweditstate')then
select st.*,cun.name from tbl_state as st 
inner join tbl_country as cun on st.countryid=cun.conid where st.stateid=row_id;
end if;
if(act_mode='stateupdate') then
update tbl_state set tbl_state.statename=counname,tbl_state.statecode=coucode,tbl_state.modifiedon=now(),tbl_state.countryid=commid where tbl_state.stateid=row_id;
end if;
if(act_mode='citystate')then
select * from tbl_city where tbl_city.stateid=row_id order by tbl_city.cityname;
end if;
if(act_mode='citymasterview')then
select * from tbl_citymaster;
end if;
if(act_mode='citygview')then
select * from tbl_city where tbl_city.cstatus!='D';
end if;
if(act_mode='citygcheck')then
select count(ct.cgid) as citycount from tbl_citygroup as ct where  ct.cityidd=commid and ct.stateid=counname;
end if;
if(act_mode='cityginsert')then
insert into tbl_citygroup (cmid,cityidd,stateid,cstatus)values(row_id,commid,counname,'A');
end if;
if(act_mode='citgview')then
if(commid='')then
select cm.mastname,c.cityname,cg.cgid from tbl_citygroup as cg inner join tbl_citymaster as cm on cg.cmid=cm.cmasterid inner join tbl_city as c on cg.cityidd=c.cityid;
else
select cm.mastname,c.cityname,cg.cgid from tbl_citygroup as cg inner join tbl_citymaster as cm on cg.cmid=cm.cmasterid inner join tbl_city as c on cg.cityidd=c.cityid limit row_id, commid;
end if;
end if;
if(act_mode='cgdelete')then
delete from tbl_citygroup where tbl_citygroup.cgid=row_id;
end if;
if(act_mode='agactive')then
update tbl_citygroup set cstatus='D' where cgid=row_id;
end if;
if(act_mode='aginactive')then
update tbl_citygroup set cstatus='A' where cgid=row_id;
end if;
end$$

CREATE  PROCEDURE `proc_getAdminMenu` (IN `loginid` INT)  Begin
select tm.* from tbladminmenu as tm where tm.parentid=0 and tm.r_status='Active' order by amlevel;

End$$

CREATE  PROCEDURE `proc_getAssignRole` (IN `empid` INT(10))  begin
select main_menu,sub_menu from tblassignrole where emp_id=empid;
end$$

CREATE  PROCEDURE `proc_getPassword` (IN `UserId` BIGINT(20), IN `old_password` VARCHAR(100))  BEGIN
 select count(`password`) as totalmatch  from tbl_manufacturermaster WHERE id = UserId and `password`= `old_password`;
 end$$

CREATE  PROCEDURE `proc_getseo` (IN `act_mode` VARCHAR(100), IN `p_catid` INT(10), IN `p_extra` VARCHAR(100))  begin
if(act_mode= 'cat') THEN

SELECT TC.catid,TC.metatitle,TC.metadesc,TC.metakeywords  FROM tbl_categorymaster AS  TC  WHERE TC.catid = p_catid; 

END IF;

if(act_mode= 'product') THEN

SELECT TP.proid,TP.metatitle as metatitle,TP.metakeywords,TP.metadescription metadesc  FROM tbl_product AS TP WHERE TP.proid =p_catid; 

END IF;

end$$

CREATE  PROCEDURE `proc_get_timeslots_by_date` (IN `param1` INT(11), IN `param2` INT(11), IN `param3` VARCHAR(60))  NO SQL
BEGIN

if(act_mode='getTimeSlotByDate')then

SELECT * FROM tbl_timeslot_branch WHERE timeslot_date >= CURDATE();

end if ;

END$$

CREATE  PROCEDURE `proc_groupbookinginsert_v` (IN `act_mode` VARCHAR(50), IN `bookingType` VARCHAR(50), IN `full_name` VARCHAR(50), IN `email_address` VARCHAR(50), IN `phone_number` INT, IN `departDate` VARCHAR(50), IN `sessionTime` VARCHAR(50), IN `number_people` INT, IN `socialGroupRadio` VARCHAR(50), IN `socialGroupName` VARCHAR(50), IN `stateid` VARCHAR(50), IN `cityid` INT, IN `branch_id` INT, IN `locationid` INT, IN `countryid` INT)  BEGIN
if(act_mode='insertgroups') then
INSERT INTO `tbl_groupbooking`(`bookingType`, `full_name`, `email_address`,`phone_number`,`departDate`, `sessionTime`, `number_people`,`socialGroupRadio`, `socialGroupName`, `addeddate`,`status`, `stateid`, `cityid`,`branch_id`, `locationid`,`countryid`) VALUES (bookingType,full_name,email_address,phone_number,departDate,sessionTime,number_people,socialGroupRadio,socialGroupName,NOW(),'1',stateid,cityid,branch_id,locationid,countryid);

select last_insert_id() as pack_id;
end if;
END$$

CREATE  PROCEDURE `proc_guestorderupdate_v` (IN `act_mode` VARCHAR(50), IN `user_id` VARCHAR(50), IN `order_id` VARCHAR(50), IN `title` VARCHAR(50), IN `billing_name` VARCHAR(50), IN `billing_email` VARCHAR(50), IN `billing_tel` VARCHAR(50), IN `billing_address` TEXT, IN `billing_city` VARCHAR(50), IN `billing_state` VARCHAR(50), IN `billing_zip` VARCHAR(50), IN `billing_country` VARCHAR(50))  BEGIN
if(act_mode = 'orderguestupdate') then
update tbl_orderpackage as t set t.userid=user_id,t.op_usertype='Guest',
t.title=title, t.billing_name=billing_name, t.billing_email=billing_email,
t.billing_tel=billing_tel, t.billing_address=billing_address,
t.billing_city=billing_city , t.billing_state=billing_state,
t.billing_zip=billing_zip ,t.billing_country=billing_country 
where t.pacorderid=order_id;

end if;

if(act_mode = 'orderuserupdate') then
update tbl_orderpackage set userid=user_id,op_usertype='User', title=title, billing_name=billing_name, billing_email=billing_email, billing_tel=billing_tel, billing_address=billing_address,  billing_city=billing_city , billing_state=billing_state, billing_zip=billing_zip ,billing_country=billing_country where pacorderid=order_id;
end if;
if(act_mode = 'orderagentupdate') then
update tbl_orderpackage set userid=user_id,op_usertype='Agent', title=title, billing_name=billing_name, billing_email=billing_email, billing_tel=billing_tel, billing_address=billing_address,  billing_city=billing_city , billing_state=billing_state, billing_zip=billing_zip ,billing_country=billing_country where pacorderid=order_id;
end if;


END$$

CREATE  PROCEDURE `proc_guestregister_v` (IN `act_mode` VARCHAR(50), IN `title` VARCHAR(50), IN `billing_name` VARCHAR(50), IN `billing_email` VARCHAR(50), IN `billing_tel` VARCHAR(50), IN `billing_address` TEXT, IN `billing_city` VARCHAR(50), IN `billing_state` VARCHAR(50), IN `billing_zip` VARCHAR(50), IN `billing_country` VARCHAR(50))  BEGIN
if(act_mode = 'guestregister') then
INSERT INTO `tbl_user`(`user_title`, `user_firstname`, `user_emailid`, `user_mobileno`, `user_Address`, `user_city`, `user_state`, `user_zip`, `user_country`,`user_usertype`) 
VALUES (title,billing_name,billing_email,billing_tel,billing_address,billing_city,billing_state,billing_zip,billing_country,'Guest');
select last_insert_id() as ls;


end if;
END$$

CREATE  PROCEDURE `proc_inventory_s` (IN `act_mode` VARCHAR(50), IN `Param1` VARCHAR(50), IN `Param2` VARCHAR(50), IN `Param3` VARCHAR(50), IN `Param4` VARCHAR(50), IN `Param5` VARCHAR(50), IN `Param6` VARCHAR(50), IN `Param7` VARCHAR(50), IN `Param8` VARCHAR(50), IN `Param9` VARCHAR(50))  BEGIN

if(act_mode='promo_view') then
select t.dailyinventory_seats as total_Seat,t.dailyinventory_from as tfrom from  tbl_daily_inventory as t where t.dailyinventory_branchid=Param1 and t.dailyinventory_status=1 and t.dailyinventory_date=Param2;
end if;

if(act_mode='promo_view1') then
select t.timeslot_seats as total_Seat,t.timeslot_from as tfrom from  tbl_timeslot_branch as t where t.timeslot_branchid=Param1 and t.timeslot_status=1;
end if;

if(act_mode='order_addon_view') then
select group_concat(t.packpkg) as package_qty,group_concat(t.departuretime) as package_time from tbl_orderpackage as t where t.branch_id=Param1 and t.departuredate=Param2 and order_status in ('Shipped','Success');
end if;

if(act_mode='insert_dailydate') then
update tbl_daily_inventory as t set t.dailyinventory_seats=Param2 where t.dailyinventory_id=Param1;
end if;


END$$

CREATE  PROCEDURE `proc_leftMenu` (IN `userid` INT(10), IN `moduleid` INT)  begin
select * from tbladminmenu as tm where tm.parentid=moduleid and tm.r_status='Active' order by tm.amlevel;


end$$

CREATE  PROCEDURE `proc_location_v` (IN `act_mode` VARCHAR(50), IN `Param1` VARCHAR(50), IN `Param2` VARCHAR(50), IN `Param3` VARCHAR(50), IN `Param4` VARCHAR(50), IN `Param5` VARCHAR(50), IN `Param6` VARCHAR(50), IN `Param7` VARCHAR(50), IN `Param8` VARCHAR(50), IN `Param9` VARCHAR(50))  BEGIN
if(act_mode='vieweditlocation')then
select * from tbl_location where locationid=Param3;
end if;
if(act_mode='locationupdatedata') then
update tbl_location set tbl_location.locationname=Param1,tbl_location.totalseatperslot=Param2,tbl_location.modifiedon=now() where tbl_location.locationid=Param3;
end if;
if(act_mode='locationdelete')then
delete from tbl_location where tbl_location.locationid=Param3;
end if;
if(act_mode='viewlocation')then
select * from tbl_location order by locationname;
end if;
if(act_mode='viewbranch')then
select * from tbl_branch_master  where tbl_branch_master.branch_location=Param1;
end if;
if(act_mode='viewactivity_package')then
select * from tbl_package_master a join tbl_branch_package b where a.package_id=b.bp_packageid and a.package_status='1' and b.bp_branchid=Param1 ;
end if;
if(act_mode='ticketcart_package')then
select * from tbl_package_master  where tbl_package_master.package_id=Param1 ;
end if;
if(act_mode='addonscart_package')then
select * from tbl_addon  where tbl_addon.addon_branchid=Param1 ;
end if;

if(act_mode='addonscartses_package')then
select * from tbl_addon  where tbl_addon.addon_id=Param1 ;
end if;






if(act_mode='getcity')then
select * from tbl_city as t where t.cityid=Param1 and t.cstatus!='2';
end if;

if(act_mode='loc_update_modal')then
update tbl_location as t set t.cityid=Param2, t.locationname=Param3,t.modifiedon=now() where t.locationid=Param1;
end if;


if(act_mode='citylocation') then
select * from  tbl_location as t where t.cityid=Param1 and t.lstatus=1;
end if;

if(act_mode = 'insert_promo') then
INSERT INTO `tbl_promo`(`promo_id`, `p_locationid`, `p_codename`, `p_type`, `p_discount`, `p_status`, `p_createdon`, `p_modifiedon`) VALUES ('',Param1,Param2,Param3,Param4,'1','','');
end if;

if(act_mode='view_promo') then
select * from  tbl_promo as t where t.p_status=1;
end if;

if(act_mode='delete_promo') then
delete from tbl_promo where promo_id=Param1;
end if;

if(act_mode='getlocation') then
select * from tbl_location as tl where tl.locationid=Param1;
end if;

if(act_mode='getcompany') then
select * from tbl_company_master as t where t.comp_id=Param1;
end if;

if(act_mode='s_addactivity') then
INSERT INTO `tbl_activity`(`activity_name`, `activity_discription`, `activity_price`, `activity_status`) VALUES (Param1,Param2,Param3,'1');
end if;

if(act_mode='s_viewactivity') then
select * from  tbl_activity as t where t.activity_status=1;
end if;

if(act_mode='delete_activity') then
update tbl_activity as t set t.activity_status='2' where activity_id=Param1;
end if;



if(act_mode='delete_package') then
update tbl_package_master as t set t.package_status='2' where t.package_id=Param1;
end if;



if(act_mode='s_addcompany') then
INSERT INTO `tbl_company_master`(`comp_name`, `comp_status`) VALUES (Param1,'1');
end if;
if(act_mode='s_viewcompany') then
select * from  tbl_company_master as t where t.comp_status=1;
end if;
if(act_mode='s_deletetcompany') then
delete from tbl_company_master where comp_id=Param1;
end if;
if(act_mode='s_updatecompany') then
update tbl_company_master as t set t.comp_name=Param1 where t.comp_id=Param2;
end if;


if(act_mode='s_addbranch') then
INSERT INTO `tbl_branch_master`( `branch_name`, `branch_company`, `branch_location`, `branch_status`)  VALUES (Param3,Param2,Param1,'1');
end if;
if(act_mode='s_viewbranch') then
select * from  tbl_branch_master as t where t.branch_status=1;
end if;

if(act_mode='delete_branch') then
update tbl_branch_master as t set t.branch_status='2' where t.branch_id=Param1;
end if;

if(act_mode='getbranch_update') then
select *  from tbl_branch_master as t where t.branch_id=Param1;
end if;

if(act_mode='getlocation_update') then
select *  from tbl_location as t where t.lstatus=1;
end if;


if(act_mode='s_updatebranch') then
update tbl_branch_master as t set t.branch_location=Param1, t.branch_company=Param2 , t.branch_name=Param3 ,t.branch_modified=NOW() where t.branch_id=Param4;
end if;

if(act_mode='s_addactivity_update') then
update tbl_activity as t set t.activity_name=Param1, t.activity_discription=Param2,t.activity_price=Param3,t.activity_modifiedon=NOW() where t.activity_id=Param4;
end if;


if(act_mode='s_viewpackage') then
select t.*,(select group_concat(tbp.bp_branchid) from tbl_branch_package as tbp where tbp.bp_packageid=t.package_id) as bp_id,(select group_concat(tap.ap_activityid) from tbl_activity_package as tap where tap.ap_packageid=t.package_id) as ap_id from tbl_package_master as t where t.package_status=1;
end if;


if(act_mode='s_addpackage_master') then
INSERT INTO `tbl_package_master`(`package_name`, `package_description`, `package_status`,`package_price`) VALUES (Param1,Param2,'1',Param3);
select last_insert_id() as pack_id;
end if;


if(act_mode='s_addpackage_branch') then
INSERT INTO `tbl_branch_package`(`bp_packageid`, `bp_branchid`, `bp_status`) VALUES (Param1,Param2,'1');
end if;

if(act_mode='s_addpackage_activity') then
INSERT INTO `tbl_activity_package`(`ap_packageid`, `ap_activityid`, `ap_status`) VALUES (Param1,Param2,'1');
end if;

if(act_mode='activityPrice') then
select ta.activity_price  from tbl_activity as ta where ta.activity_id=Param1;
end if;

if(act_mode='getbranch')then
select * from tbl_branch_master  where tbl_branch_master.branch_id=Param1;
end if;

if(act_mode='getactivity')then
select * from tbl_activity  where tbl_activity.activity_id=Param1;
end if;

if(act_mode='addbranchAddon')then
INSERT INTO `tbl_addon`(`addon_name`, `addon_price`, `addon_status`,`addon_branchid`, `addon_description`) VALUES (Param1,Param4,'1',Param2,Param3);
end if;

if(act_mode='addactivityAddon')then
INSERT INTO `tbl_addon`(`addon_name`, `addon_price`, `addon_status`,`addon_activityid`, `addon_description`) VALUES (Param1,Param4,'1',Param2,Param3);
end if;


if(act_mode='s_viewaddon')then
select distinct(t.addon_name) from tbl_addon as t  where t.addon_status='1';
end if;



END$$

CREATE  PROCEDURE `proc_mail_s` (IN `act_mode` VARCHAR(50), IN `Param1` VARCHAR(50), IN `Param2` VARCHAR(50), IN `Param3` VARCHAR(50), IN `Param4` VARCHAR(50), IN `Param5` VARCHAR(50), IN `Param6` VARCHAR(50), IN `Param7` VARCHAR(50), IN `Param8` VARCHAR(50), IN `Param9` VARCHAR(50), IN `Param10` VARCHAR(50), IN `Param11` VARCHAR(50), IN `Param12` VARCHAR(50))  BEGIN

if(act_mode = 's_viewmail') then
select * from tbl_bannerimage where bannerimage_status in (0,1);
end if;


if(act_mode = 's_addemail_details') then
INSERT INTO tbl_bannerimage ( bannerimage_branch, bannerimage_logo, bannerimage_top1, bannerimage_top2, bannerimage_top3, bannerimage_top4, bannerimage_subject, bannerimage_from, bannerimage_branch_contact, bannerimage_branch_email,internethandlingcharge,bannerimage_apikey) values (Param1,Param2,Param3,Param4,Param5,Param6,Param7,Param8,Param9,Param10,Param12,Param11);
end if;

if(act_mode = 's_viewmail_update') then
select * from tbl_bannerimage as t where t.bannerimage_id=Param1;
end if;

if(act_mode = 's_addemail_details_update') then
Update tbl_bannerimage set  bannerimage_branch=Param1, bannerimage_logo=Param2, bannerimage_top1=Param3, bannerimage_top2=Param4, bannerimage_top3=Param5, bannerimage_top4=Param6, bannerimage_subject=Param7, bannerimage_from=Param8, bannerimage_branch_contact=Param9, bannerimage_branch_email=Param10, bannerimage_apikey=Param12 where bannerimage_id=Param11;
end if;

if(act_mode = 's_viewmail_status') then
update tbl_bannerimage as t set t.bannerimage_status=Param2 where t.bannerimage_id=Param1;
end if;

if(act_mode = 's_viewmail_delete') then
update tbl_bannerimage as t set t.bannerimage_status=2 where t.bannerimage_id=Param1;
end if;

END$$

CREATE  PROCEDURE `proc_mail_v` (IN `act_mode` VARCHAR(50), IN `Param1` VARCHAR(50), IN `Param2` VARCHAR(50), IN `Param3` VARCHAR(50), IN `Param4` VARCHAR(50), IN `Param5` VARCHAR(50), IN `Param6` VARCHAR(50), IN `Param7` VARCHAR(50), IN `Param8` VARCHAR(50), IN `Param9` VARCHAR(50), IN `Param10` VARCHAR(50), IN `Param11` VARCHAR(50), IN `Param12` VARCHAR(50), IN `Param13` VARCHAR(50), IN `Param14` INT, IN `Param15` INT)  BEGIN

if(act_mode = 's_viewmail') then
select * from tbl_bannerimage where bannerimage_status in (0,1);
end if;


if(act_mode = 's_addemail_details') then
INSERT INTO tbl_bannerimage ( bannerimage_branch, bannerimage_logo, bannerimage_top1, bannerimage_top2, bannerimage_top3, bannerimage_top4, bannerimage_subject, bannerimage_from, bannerimage_branch_contact, bannerimage_branch_email,internethandlingcharge,bannerimage_apikey,bannerimage_gstno) values (Param1,Param2,Param3,Param4,Param5,Param6,Param7,Param8,Param9,Param10,Param12,Param11,Param13);
end if;

if(act_mode = 's_viewmail_update') then
select * from tbl_bannerimage as t where t.bannerimage_id=Param1;
end if;

if(act_mode = 's_addemail_details_update') then
Update tbl_bannerimage set  bannerimage_branch=Param1, bannerimage_logo=Param2, bannerimage_top1=Param3, bannerimage_top2=Param4, bannerimage_top3=Param5, bannerimage_top4=Param6, bannerimage_subject=Param7, bannerimage_from=Param8, bannerimage_branch_contact=Param9, bannerimage_branch_email=Param10, bannerimage_apikey=Param12 , bannerimage_gstno=Param13 where bannerimage_id=Param11;
end if;

if(act_mode = 's_viewmail_status') then
update tbl_bannerimage as t set t.bannerimage_status=Param2 where t.bannerimage_id=Param1;
end if;

if(act_mode = 's_viewmail_delete') then
update tbl_bannerimage as t set t.bannerimage_status=2 where t.bannerimage_id=Param1;
end if;

END$$

CREATE  PROCEDURE `proc_memberlogin_v` (IN `act_mode` VARCHAR(50), IN `username` VARCHAR(50), IN `password` VARCHAR(50))  BEGIN
if(act_mode='memlogin')then
select * from tbl_user a where a.user_emailid=username and a.user_password=password and a.user_status='1' ;
end if ;
END$$

CREATE  PROCEDURE `proc_memberlog_v` (IN `act_mode` VARCHAR(50), IN `user_id` INT)  BEGIN
if(act_mode='memuserlog')then
insert into tbl_userlog (userid,logindate)values(user_id,NOW());
end if;

if(act_mode='memusersesiid')then
select * from tbl_user where user_id=user_id;
end if;

END$$

CREATE  PROCEDURE `proc_memberregister_v` (IN `act_mode` VARCHAR(50), IN `Param1` VARCHAR(50), IN `Param2` VARCHAR(50), IN `Param3` VARCHAR(50), IN `Param4` VARCHAR(50), IN `Param5` VARCHAR(50), IN `Param6` VARCHAR(50), IN `Param7` TEXT, IN `Param8` VARCHAR(50), IN `Param9` VARCHAR(50), IN `Param10` VARCHAR(50), IN `Param11` VARCHAR(50), IN `Param12` VARCHAR(50), IN `Param13` VARCHAR(50), IN `Param14` VARCHAR(50), IN `Param15` VARCHAR(50))  BEGIN
if(act_mode='memregister')then
INSERT INTO tbl_user ( `user_firstname`, `user_lastname`, `user_emailid`, `user_password`, `user_mobileno`, `user_status`, `user_createdon`, `user_modifiedon`, `user_Address`, `user_varified`, `user_town`, `user_zip`, `user_country`, `user_title`, `user_dob`, `user_state`, `user_city`, `user_pincodes`, `user_usertype`, `oauth_id`, `oauth_link`, `oauth_picture_link`, `oauth_type`, `user_regdatafill`) VALUES ( Param2, NULL, Param5, Param6, Param4, '1', NOW(), NULL, Param7, '0', Param8, Param10, Param11,Param1, '', Param9, Param8, '', 'User', '', '', '', '', '1');


#INSERT INTO tbl_user ( `user_title`,`user_firstname`, `user_lastname`, `user_emailid`, `user_password`, `user_mobileno`, `user_status`, `user_createdon`) VALUES (title,fname, lastName, email, password, mobileno, 1, NOW());select last_insert_id() as ls;
end if ;

END$$

CREATE  PROCEDURE `proc_memberses_v` (IN `act_mode` VARCHAR(50), IN `userid` INT)  BEGIN
if(act_mode='memusersesiid')then
select * from tbl_user as tc where tc.user_id =userid;
end if;

if(act_mode='patrnusersesiid')then
select * from tbl_agent_reg as tc where tc.	agent_id =userid;
end if;



if(act_mode='selectorderbooking')then

select *,(select group_concat(t1.addonename) from tbl_orderaddone as t1 where t1.lastid=t.pacorderid) as addon,(select group_concat(t1.addonqty) from tbl_orderaddone as t1 where t1.lastid=t.pacorderid) as addonquantity,(select group_concat(t1.addoneprice) from tbl_orderaddone as t1 where t1.lastid=t.pacorderid) as addonprice,(select group_concat(t1.addon_image) from tbl_orderaddone as t1 where t1.lastid=t.pacorderid) as addon_image,(select group_concat(t1.addonqty) from tbl_orderaddone as t1 where t1.lastid=t.pacorderid) as addonqty,(select user_firstname from tbl_user as tu where t.userid=tu.user_id) as username,(select user_emailid from tbl_user as tu where t.userid=tu.user_id) as email,(select user_mobileno from tbl_user as tu where t.userid=tu.user_id ) as contact from tbl_orderpackage as t  where  t.orderstatus in (0,1) and t.op_ticket_print_status in (0,1)and t.userid=userid order by t.pacorderid desc;

end if;

if(act_mode='selectorderbookingcompses')then
select *,(select group_concat(t1.addonename) from tbl_orderaddone as t1 where t1.lastid=t.pacorderid) as addon,(select group_concat(t1.addonqty) from tbl_orderaddone as t1 where t1.lastid=t.pacorderid) as addonquantity,(select group_concat(t1.addoneprice) from tbl_orderaddone as t1 where t1.lastid=t.pacorderid) as addonprice,(select group_concat(t1.addon_image) from tbl_orderaddone as t1 where t1.lastid=t.pacorderid) as addon_image,(select group_concat(t1.addonqty) from tbl_orderaddone as t1 where t1.lastid=t.pacorderid) as addonqty,(select user_firstname from tbl_user as tu where t.userid=tu.user_id) as username,(select user_emailid from tbl_user as tu where t.userid=tu.user_id) as email,(select user_mobileno from tbl_user as tu where t.userid=tu.user_id ) as contact from tbl_orderpackage as t  where  t.orderstatus in(0,1)  and t.op_ticket_print_status =2 and t.userid=userid order by t.pacorderid desc;

end if;

if(act_mode='cancelorderbooking')then
update tbl_orderpackage set op_ticket_print_status=3 where  pacorderid=userid;
end if;

if(act_mode='selectorderbookingdata')then
select * from tbl_orderpackage  where  pacorderid=userid;
end if;


END$$

CREATE  PROCEDURE `proc_memberuserchangepassword_v` (IN `act_mode` VARCHAR(50), IN `userid` VARCHAR(50), IN `currentpassword` VARCHAR(50), IN `newpassword` VARCHAR(50), IN `confirmpassword` VARCHAR(50))  BEGIN
if(act_mode='memechangepassword')then
update tbl_user  set user_password=newpassword where user_id =userid;
end if;

if(act_mode='memecheckpassword')then
select user_password,user_id from tbl_user  where user_id =userid;
end if;
END$$

CREATE  PROCEDURE `proc_memberusercheck_v` (IN `act_mode` VARCHAR(50), IN `email` VARCHAR(50))  BEGIN
if(act_mode='checkuser')then
select * from tbl_user as tc where tc.user_emailid =email and user_usertype='User';
end if;

if(act_mode='emailverified')then
select * from tbl_user as tc where tc.user_emailid =email;
end if;

if(act_mode='emailverifiedupdate')then
update  tbl_user set varified=1 where emailid =email;

end if;
END$$

CREATE  PROCEDURE `proc_memberuserupdate_v` (IN `act_mode` VARCHAR(50), IN `userid` INT, IN `firstName` VARCHAR(50), IN `lastName` VARCHAR(50), IN `myprofiledob` VARCHAR(50), IN `email` VARCHAR(50), IN `mobile` VARCHAR(50), IN `address` TEXT, IN `countryName` VARCHAR(50), IN `stateName` VARCHAR(50), IN `cityName` VARCHAR(50), IN `pincode` VARCHAR(50))  BEGIN
if(act_mode='updateuser')then
update tbl_user set user_firstname=firstName,user_lastname=lastName,user_dob=myprofiledob,user_mobileno=mobile,user_Address=address,user_country=countryName,user_state=stateName,user_city=cityName,user_pincodes=pincode where user_id=userid;
end if;

if(act_mode='selectuser')then
select * from tbl_user  where user_id=userid;
end if;
END$$

CREATE  PROCEDURE `proc_menu` (IN `id` INT)  begin

select mas.catname,mas.url,map.parentid,map.childid,map.catlevel,cp.catname as parentname,
cimg.catlogo,cimg.imgfirst,cimg.imgsecond,cimg.imgthird,cimg.imgaltname,100 as products  from tbl_categorymap as map 
left join tbl_categorymaster as mas on map.childid=mas.catid 
left join tbl_categorymaster as cp ON cp.catid=map.parentid 
left join tbl_catimage as cimg on cimg.catid=map.childid
where map.mainprent=1 and map.cstatus='A' order by map.ordersort;

end$$

CREATE  PROCEDURE `proc_orderaddone_v` (IN `act_mode` VARCHAR(50), IN `addonnameses` VARCHAR(50), IN `addonquantityses` INT, IN `addonidses` INT, IN `lastidses` INT, IN `addonpriceses` INT, IN `ccartpackageimage` VARCHAR(255))  BEGIN
if(act_mode='orderaddone_insert')then
INSERT INTO tbl_orderaddone (addonename,addonevisiter,addoneprice,addonqty,lastid,addonid,Type,addon_image) VALUES (addonnameses,addonquantityses,addonpriceses,addonquantityses,lastidses,addonidses,'Addone',ccartpackageimage);
select last_insert_id();
end if;
if(act_mode='selectaddones')then
select * from tbl_orderaddone as a where a.lastid=lastidses;
end if;

if(act_mode='orderaddone_delete')then
delete from  tbl_orderaddone  where lastid=lastidses;
end if;


END$$

CREATE  PROCEDURE `proc_orderinsertpackage_v` (IN `act_mode` VARCHAR(50), IN `packagenameses` VARCHAR(50), IN `packagequantityses` INT, IN `packageidses` INT, IN `package_priceses` INT, IN `lastidses` INT)  if(act_mode='orderpackagedata_insert')then
 INSERT INTO tbl_orderaddone (addonename,addoneprice,lastid,addonqty,Type,addonid) VALUES (packagenameses,package_priceses,lastidses,packagequantityses,'Package',packageidses);
select last_insert_id();
end if$$

CREATE  PROCEDURE `proc_orderonline_v` (IN `act_mode` VARCHAR(50), IN `orderid` VARCHAR(50))  BEGIN

if(act_mode='selectorder')then

select * from 	tbl_orderpackage;
end if;

if(act_mode='selectaddons')then

select * from 	tbl_orderaddone;
end if;

if(act_mode='selecttime')then

select * from 	tbl_daily_inventory order by dailyinventory_id  desc ;
end if;

END$$

CREATE  PROCEDURE `proc_orderpackage_v` (IN `act_mode` VARCHAR(50), IN `subtotal` INT, IN `discountamount` INT, IN `total` INT, IN `countryid` INT, IN `stateid` INT, IN `cityid` INT, IN `branch_id` INT, IN `locationid` INT, IN `userid` INT, IN `txtDepartDate1` VARCHAR(50), IN `txtDepartdata` VARCHAR(50), IN `paymentmode` VARCHAR(50), IN `ticketid` VARCHAR(50), IN `packproductname` VARCHAR(50), IN `packimg` TINYTEXT, IN `packpkg` INT, IN `packprice` INT, IN `internethandlingcharges` VARCHAR(50), IN `txtfrommin` VARCHAR(50), IN `txttohrs` VARCHAR(50), IN `txttomin` VARCHAR(50), IN `txtfromd` VARCHAR(50), IN `txttod` VARCHAR(50), IN `p_codename` VARCHAR(50), IN `p_usertype` VARCHAR(50), IN `package_qty` VARCHAR(100), IN `package_price` TINYTEXT)  BEGIN
if(act_mode='orderpackage_insert')then
 insert into tbl_orderpackage  (paymentstatus,userid,promocodeprice,total,subtotal,addedon,countryid,stateid,cityid,branch_id,locationid,departuredate,departuretime,paymentmode,ticketid,packproductname,packimg,packpkg,packprice,internethandlingcharges,frommin,tohrs,tomin,txtfromd,txttod,p_codename,op_usertype,package_qty,package_price)values('0',userid,discountamount,total,subtotal,ADDTIME(now(), '12:30:00'),countryid,stateid,cityid,branch_id,locationid,txtDepartDate1,txtDepartdata,paymentmode,ticketid,packproductname,packimg,packpkg,packprice,internethandlingcharges,txtfrommin,txttohrs,txttomin,txtfromd,txttod,p_codename,p_usertype,package_qty,package_price);
select last_insert_id();
end if;

if(act_mode='selectbooking')then
select * from tbl_orderpackage a where a.ticketid=ticketid;
end if;

if(act_mode='orderpackage_update')then
update tbl_orderpackage as t set t.paymentstatus='0',t.userid=userid,t.promocodeprice=discountamount,t.total=total,t.subtotal=subtotal,t.addedon=ADDTIME(now(), '12:30:00'),t.countryid=countryid,t.stateid=stateid,t.cityid=cityid,t.branch_id=branch_id,t.locationid=locationid,t.departuredate=txtDepartDate1,t.departuretime=txtDepartdata,t.paymentmode=paymentmode,t.ticketid=ticketid,t.packproductname=packproductname,t.packimg=packimg,t.packpkg=packpkg,t.packprice=packprice,t.internethandlingcharges=internethandlingcharges,t.frommin=txtfrommin,t.tohrs=txttohrs,t.tomin=txttomin,t.txtfromd=txtfromd,t.txttod=txttod,t.p_codename=p_codename,t.op_usertype=p_usertype,t.package_qty=package_qty,t.package_price=package_price where t.ticketid=ticketid;
select t.pacorderid as 'last_insert_id()' from tbl_orderpackage as t where t.ticketid=ticketid;
end if;


END$$

CREATE  PROCEDURE `proc_orderpackage_vback` (IN `act_mode` VARCHAR(50), IN `subtotal` VARCHAR(50))  BEGIN
if(act_mode='orderpackage_insert')then
INSERT INTO tbl_orderpackage (subtotal) VALUES (subtotal);
select last_insert_id();
end if;
END$$

CREATE  PROCEDURE `proc_orderuser_v` (IN `act_mode` VARCHAR(50), IN `userid` INT)  BEGIN
if(act_mode='select_customer')then

select * from tbl_user where user_id=userid;
end if;

if(act_mode='select_partner')then

select * from tbl_agent_reg where agent_id=userid;
end if;
END$$

CREATE  PROCEDURE `proc_order_filter_s` (IN `act_mode` VARCHAR(50), IN `Param1` VARCHAR(50), IN `Param2` VARCHAR(50), IN `Param3` VARCHAR(50), IN `Param4` VARCHAR(50), IN `Param5` VARCHAR(50), IN `Param6` VARCHAR(50), IN `Param7` VARCHAR(50), IN `Param8` VARCHAR(50), IN `Param9` VARCHAR(50), IN `Param10` VARCHAR(50), IN `Param11` VARCHAR(50), IN `Param12` VARCHAR(50), IN `Param13` VARCHAR(50), IN `Param14` VARCHAR(50), IN `Param15` VARCHAR(50), IN `Param16` VARCHAR(50), IN `Param17` VARCHAR(50), IN `Param18` VARCHAR(50), IN `Param19` VARCHAR(50))  BEGIN
if(act_mode='S_vieworder') then
if(Param7 = '-1') then
set @q3 = (select t1.addedon from tbl_orderpackage as t1 order by t1.addedon asc limit 0,1);
else
set @q3 = Param7;
end if;

if(Param8 = '-1') then
set @q4 = (select t1.addedon from tbl_orderpackage as t1 order by t1.addedon desc limit 0,1);
else
set @q4 = Param8;
end if;


if(Param9 = '-1') then
set @q1 = (select t1.departuredate from tbl_orderpackage t1 order by t1.departuredate asc limit 0,1);
else
set @q1 = Param9;
end if;

if(Param10 = '-1') then
set @q2 = (select t1.departuredate from tbl_orderpackage t1 order by t1.departuredate desc limit 0,1);
else
set @q2 = Param10;
end if;

if(Param12 = '-1') then
set @q5 = (select t1.op_printed_date from tbl_orderpackage t1 order by t1.op_printed_date asc limit 0,1);
else
set @q5 = Param12;
end if;

if(Param13 = '-1') then
set @q6 = (select t1.op_printed_date from tbl_orderpackage t1 order by t1.op_printed_date desc limit 0,1);
else
set @q6 = Param13;
end if;

select tbm.branch_name,t.*,group_concat(t1.addonename order by t1.orderaddoneid)  as addon,
 group_concat(t1.addonqty order by t1.orderaddoneid) as addonquantity,
group_concat(t1.addoneprice order by t1.orderaddoneid)  as addonprice,
group_concat(t1.orderaddon_print_status order by t1.orderaddoneid) as orderaddon_print_status, group_concat(t1.orderaddon_print_quantity order by t1.orderaddoneid) as orderaddon_print_quantity from tbl_orderaddone as t1 right join tbl_orderpackage as t on t.pacorderid=t1.lastid inner join tbl_branch_master as tbm on t.branch_id=tbm.branch_id  where t.orderstatus in (0,1) and t.billing_email!='NULL' and t.addedon >= DATE_SUB(CURDATE(),INTERVAL 3 day) and ((t.branch_id in (Param1)) or Param1=-1) and ((t.billing_name like concat(Param2,'%')) or Param2=-1) and ((t.billing_email like concat(Param3,'%')) or Param3=-1) and ((t.ticketid like concat(Param4,'%')) or Param4=-1) and ((t.billing_tel like concat(Param5,'%')) or Param5=-1) and ((t.paymentmode like concat(Param6,'%')) or Param6=-1)and (t.addedon between (@q3) and (@q4)) and (t.departuredate between (@q1) and (@q2)) and (( find_in_set (t.op_ticket_print_status,Param11)) or Param11=-1) and(t.op_printed_date between (@q5) and (@q6)) and((t.departuredate=Param14 or Param14=-1 ) and (t.departuretime=Param15 or Param15=-1 ))and (( find_in_set (t.departuretime,Param16)) or Param16=-1) and (( find_in_set (t.order_status,Param17)) or Param17=-1)group by t.pacorderid order by t.pacorderid desc ;


  end if;



if(act_mode='branch_list_for_filter') then
select distinct(t.timeslot_from),(t.timeslot_to),(t.timeslot_minfrom),(t.timeslot_minto) from  tbl_timeslot_branch as t where t.timeslot_status=1 order by t.timeslot_from asc;
end if;

if(act_mode='status_list_for_filter') then
select distinct(order_status) from  tbl_orderpackage ;
end if;


END$$

CREATE  PROCEDURE `proc_order_filter_v` (IN `act_mode` VARCHAR(50), IN `Param1` VARCHAR(50), IN `Param2` VARCHAR(50), IN `Param3` VARCHAR(50), IN `Param4` VARCHAR(50), IN `Param5` VARCHAR(50), IN `Param6` VARCHAR(50), IN `Param7` VARCHAR(50), IN `Param8` VARCHAR(50), IN `Param9` VARCHAR(50), IN `Param10` VARCHAR(50), IN `Param11` VARCHAR(50), IN `Param12` VARCHAR(50), IN `Param13` VARCHAR(50), IN `Param14` VARCHAR(50), IN `Param15` VARCHAR(50), IN `Param16` VARCHAR(50), IN `Param17` VARCHAR(50), IN `Param18` VARCHAR(50), IN `Param19` VARCHAR(50))  BEGIN

if(act_mode='S_vieworder_adminpanel') then
if(Param7 = '-1') then
set @q3 = (select t1.addedon from tbl_orderpackage as t1 order by t1.addedon asc limit 0,1);
else
set @q3 = Param7;
end if;

if(Param8 = '-1') then
set @q4 = (select t1.addedon from tbl_orderpackage as t1 order by t1.addedon desc limit 0,1);
else
set @q4 = Param8;
end if;


if(Param9 = '-1') then
set @q1 = (select t1.departuredate from tbl_orderpackage t1 order by t1.departuredate asc limit 0,1);
else
set @q1 = Param9;
end if;

if(Param10 = '-1') then
set @q2 = (select t1.departuredate from tbl_orderpackage t1 order by t1.departuredate desc limit 0,1);
else
set @q2 = Param10;
end if;

if(Param12 = '-1') then
set @q5 = (select t1.op_printed_date from tbl_orderpackage t1 order by t1.op_printed_date asc limit 0,1);
else
set @q5 = Param12;
end if;

if(Param13 = '-1') then
set @q6 = (select t1.op_printed_date from tbl_orderpackage t1 order by t1.op_printed_date desc limit 0,1);
else
set @q6 = Param13;
end if;



select t.*,tbm.branch_name from tbl_orderpackage as t inner join tbl_branch_master as tbm on t.branch_id=tbm.branch_id   where ((t.branch_id in (Param1)) or Param1=-1) and 
 ((t.billing_name like concat(Param2,'%')) or Param2=-1) and 
((t.billing_email like concat(Param3,'%')) or Param3=-1) and
 ((t.ticketid like concat(Param4,'%')) or Param4=-1) and 
 ((t.billing_tel like concat(Param5,'%')) or Param5=-1) and 
 ((t.paymentmode like concat(Param6,'%')) or Param6=-1) and 
 (t.addedon between (@q3) and (@q4)) and
  (t.departuredate between (@q1) and (@q2)) and 
  (( find_in_set (t.op_ticket_print_status,Param11)) or Param11=-1) and
  (t.op_printed_date between (@q5) and (@q6)) and
   ((t.departuredate=Param14 or Param14=-1 ) and (t.departuretime=Param15 or Param15=-1 ))
	 and (( find_in_set (t.departuretime,Param16)) or Param16=-1) and (( find_in_set (t.order_status,Param17)) or Param17=-1)  and t.op_usertype='Agent'  and t.billing_email!='NULL' order by t.pacorderid desc;
  end if;
  #vieworder in adminpanel-shalini-end

if(act_mode='S_vieworder') then
if(Param7 = '-1') then
set @q3 = (select t1.addedon from tbl_orderpackage as t1 order by t1.addedon asc limit 0,1);
else
set @q3 = Param7;
end if;

if(Param8 = '-1') then
set @q4 = (select t1.addedon from tbl_orderpackage as t1 order by t1.addedon desc limit 0,1);
else
set @q4 = Param8;
end if;


if(Param9 = '-1') then
set @q1 = (select t1.departuredate from tbl_orderpackage t1 order by t1.departuredate asc limit 0,1);
else
set @q1 = Param9;
end if;

if(Param10 = '-1') then
set @q2 = (select t1.departuredate from tbl_orderpackage t1 order by t1.departuredate desc limit 0,1);
else
set @q2 = Param10;
end if;

if(Param12 = '-1') then
set @q5 = (select t1.op_printed_date from tbl_orderpackage t1 order by t1.op_printed_date asc limit 0,1);
else
set @q5 = Param12;
end if;

if(Param13 = '-1') then
set @q6 = (select t1.op_printed_date from tbl_orderpackage t1 order by t1.op_printed_date desc limit 0,1);
else
set @q6 = Param13;
end if;



select t.*,tbm.branch_name from tbl_orderpackage as t inner join tbl_branch_master as tbm on t.branch_id=tbm.branch_id   where ((t.branch_id in (Param1)) or Param1=-1) and 
 ((t.billing_name like concat(Param2,'%')) or Param2=-1) and 
((t.billing_email like concat(Param3,'%')) or Param3=-1) and
 ((t.ticketid like concat(Param4,'%')) or Param4=-1) and 
 ((t.billing_tel like concat(Param5,'%')) or Param5=-1) and 
 ((t.paymentmode like concat(Param6,'%')) or Param6=-1) and 
 (t.addedon between (@q3) and (@q4)) and
  (t.departuredate between (@q1) and (@q2)) and 
  (( find_in_set (t.op_ticket_print_status,Param11)) or Param11=-1) and
  (t.op_printed_date between (@q5) and (@q6)) and
   ((t.departuredate=Param14 or Param14=-1 ) and (t.departuretime=Param15 or Param15=-1 ))
	 and (( find_in_set (t.departuretime,Param16)) or Param16=-1) and (( find_in_set (t.order_status,Param17)) or Param17=-1) and t.userid=Param19 and t.op_usertype='Agent'  and t.billing_email!='NULL' order by t.pacorderid desc;
  end if;



if(act_mode='branch_list_for_filter') then
select distinct(t.timeslot_from) from  tbl_timeslot_branch as t where t.timeslot_status=1 order by t.timeslot_from asc;
end if;

if(act_mode='status_list_for_filter') then
select distinct(order_status) from  tbl_orderpackage ;
end if;

if(act_mode='S_vieworderagent') then
if(Param7 = '-1') then
set @q3 = (select t1.addedon from tbl_orderpackage as t1 order by t1.addedon asc limit 0,1);
else
set @q3 = Param7;
end if;

if(Param8 = '-1') then
set @q4 = (select t1.addedon from tbl_orderpackage as t1 order by t1.addedon desc limit 0,1);
else
set @q4 = Param8;
end if;


if(Param9 = '-1') then
set @q1 = (select t1.departuredate from tbl_orderpackage t1 order by t1.departuredate asc limit 0,1);
else
set @q1 = Param9;
end if;

if(Param10 = '-1') then
set @q2 = (select t1.departuredate from tbl_orderpackage t1 order by t1.departuredate desc limit 0,1);
else
set @q2 = Param10;
end if;

if(Param12 = '-1') then
set @q5 = (select t1.op_printed_date from tbl_orderpackage t1 order by t1.op_printed_date asc limit 0,1);
else
set @q5 = Param12;
end if;

if(Param13 = '-1') then
set @q6 = (select t1.op_printed_date from tbl_orderpackage t1 order by t1.op_printed_date desc limit 0,1);
else
set @q6 = Param13;
end if;



select t.*,tbm.branch_name from tbl_orderpackage as t inner join tbl_branch_master as tbm on t.branch_id=tbm.branch_id   where ((t.branch_id in (Param1)) or Param1=-1) and 
 ((t.billing_name like concat(Param2,'%')) or Param2=-1) and 
((t.billing_email like concat(Param3,'%')) or Param3=-1) and
 ((t.ticketid like concat(Param4,'%')) or Param4=-1) and 
 ((t.billing_tel like concat(Param5,'%')) or Param5=-1) and 
 ((t.paymentmode like concat(Param6,'%')) or Param6=-1) and 
 (t.addedon between (@q3) and (@q4)) and
  (t.departuredate between (@q1) and (@q2)) and 
  (( find_in_set (t.op_ticket_print_status,Param11)) or Param11=-1) and
  (t.op_printed_date between (@q5) and (@q6)) and
   ((t.departuredate=Param14 or Param14=-1 ) and (t.departuretime=Param15 or Param15=-1 ))
	 and (( find_in_set (t.departuretime,Param16)) or Param16=-1) and (( find_in_set (t.order_status,Param17)) or Param17=-1) and t.userid=Param19 and t.op_usertype='Agent'  and t.billing_email!='NULL' order by t.pacorderid desc;
  end if;


END$$

CREATE  PROCEDURE `proc_order_partner` (IN `act_mode` VARCHAR(50), IN `Param1` VARCHAR(50), IN `Param2` VARCHAR(50), IN `Param3` VARCHAR(50), IN `Param4` VARCHAR(50), IN `Param5` VARCHAR(50), IN `Param6` VARCHAR(50), IN `Param7` VARCHAR(50), IN `Param8` VARCHAR(50), IN `Param9` VARCHAR(50))  BEGIN

if(act_mode='S_vieworder_new_adminpanel') then
select t.*,group_concat(t1.addonename order by t1.orderaddoneid)  as addon,
 group_concat(t1.addonqty order by t1.orderaddoneid) as addonquantity,
group_concat(t1.addoneprice order by t1.orderaddoneid)  as addonprice,
group_concat(t1.orderaddon_print_status order by t1.orderaddoneid) as orderaddon_print_status, group_concat(t1.orderaddon_print_quantity
 order by t1.orderaddoneid) as orderaddon_print_quantity from tbl_orderaddone as t1 right join tbl_orderpackage as t on t.pacorderid=t1.lastid  where t.orderstatus in (0,1) and t.op_usertype='Partner' and t.billing_email!='NULL'
  group by t.pacorderid order by t.pacorderid desc;
end if;
#list all agent in admin-shalini-end
if(act_mode='S_vieworder') then
select *,
(select group_concat(t1.addonename) from tbl_orderaddone as t1 where t1.lastid=t.pacorderid) as addon,
(select group_concat(t1.addonqty) from tbl_orderaddone as t1 where t1.lastid=t.pacorderid) as addonquantity,
(select group_concat(t1.addoneprice) from tbl_orderaddone as t1 where t1.lastid=t.pacorderid) as addonprice,
(select group_concat(t1.orderaddon_print_status) from tbl_orderaddone as t1 where t1.lastid=t.pacorderid) as orderaddon_print_status,
(select group_concat(t1.orderaddon_print_quantity) from tbl_orderaddone as t1 where t1.lastid=t.pacorderid) as orderaddon_print_quantity,
(select user_firstname from tbl_user as tu where t.userid=tu.user_id) as username,
(select user_emailid from tbl_user as tu where t.userid=tu.user_id) as email,
(select user_mobileno from tbl_user as tu where t.userid=tu.user_id) as contact
 from tbl_orderpackage as t  where t.orderstatus in (0,1) and t.billing_email!='NULL' order by t.pacorderid desc;
end if;

if(act_mode='S_vieworder_new') then

select t.*,group_concat(t1.addonename order by t1.orderaddoneid)  as addon,
 group_concat(t1.addonqty order by t1.orderaddoneid) as addonquantity,
group_concat(t1.addoneprice order by t1.orderaddoneid)  as addonprice,
group_concat(t1.orderaddon_print_status order by t1.orderaddoneid) as orderaddon_print_status, group_concat(t1.orderaddon_print_quantity
 order by t1.orderaddoneid) as orderaddon_print_quantity from tbl_orderaddone as t1 right join tbl_orderpackage as t on t.pacorderid=t1.lastid  where t.orderstatus in (0,1) and t.billing_by=Param9 and t.op_usertype='Partner' and t.billing_email!='NULL'
  group by t.pacorderid order by t.pacorderid desc;
end if;



if(act_mode='op_ticket_print_status') then

if(Param2='2') then
update tbl_orderpackage as t set t.op_ticket_print_status=Param2, t.op_printed_date=now()  where t.pacorderid=Param1;
else
update tbl_orderpackage as t set t.op_ticket_print_status=Param2 where t.pacorderid=Param1;
end if;
select op_ticket_print_status as ticket_status from tbl_orderpackage where pacorderid=Param1;
end if;

if(act_mode='S_viewordergp') then
select * from tbl_groupbooking;
end if;
END$$

CREATE  PROCEDURE `proc_order_s` (IN `act_mode` VARCHAR(50), IN `Param1` VARCHAR(50), IN `Param2` VARCHAR(50), IN `Param3` VARCHAR(50), IN `Param4` VARCHAR(50), IN `Param5` VARCHAR(50), IN `Param6` VARCHAR(50), IN `Param7` VARCHAR(50), IN `Param8` VARCHAR(50), IN `Param9` VARCHAR(50))  BEGIN


if(act_mode='check_fb_user')then
select count(*) as c from tbl_user as u where u.user_emailid=Param2;
end if;


if(act_mode='total_print_ticket')then
select count(op.op_printed_date) as totalprint from tbl_orderpackage as op where op.op_ticket_print_status=2;
end if;


if(act_mode='total_users')then
select count(*) AS c from tbl_user where user_password IS NOT NULL;
end if;


if(act_mode='total_guest')then
select count(*) AS g from tbl_user where user_password IS  NULL;
end if;



if(act_mode='total_trans_amount')then
select sum(op.total) as total from tbl_orderpackage as op;
end if;



if(act_mode='count_order_by_month')then
select   month(op.addedon) as m,count(op.addedon) as coun,DATE_FORMAT(DATE(op.addedon), "%M") as monthname,year(op.addedon)
from tbl_orderpackage as op  where date(op.addedon) between  DATE_SUB(curdate(), INTERVAL 5 MONTH)
AND LAST_DAY(curdate())
group by m order by op.addedon asc;
end if;

if(act_mode='count_order_by_month_date')then
select   month(op.addedon) as m,count(op.addedon) as coun,DATE_FORMAT(DATE(op.addedon), "%M") as monthname,year(op.addedon)
from tbl_orderpackage as op  where date(op.addedon) >= Param1 and date(op.addedon) <= Param2 group by m order by op.addedon asc;
end if;
#end

if(act_mode='count_total_order')then
select count(op.pacorderid) as totalorder from tbl_orderpackage as op where op.paymentstatus != 2 ;
end if;

if(act_mode='count_total_agent')then
select count(ar.agent_id) as totalagent from tbl_agent_reg as ar ;
end if;



if(act_mode='S_vieworder') then
select tbm.branch_name,t.*,group_concat(t1.addonename order by t1.orderaddoneid)  as addon,
 group_concat(t1.addonqty order by t1.orderaddoneid) as addonquantity,
group_concat(t1.addoneprice order by t1.orderaddoneid)  as addonprice,
group_concat(t1.orderaddon_print_status order by t1.orderaddoneid) as orderaddon_print_status,
 group_concat(t1.orderaddon_print_quantity order by t1.orderaddoneid) as orderaddon_print_quantity
  from tbl_orderaddone as t1 right join tbl_orderpackage as t on t.pacorderid=t1.lastid inner join 
tbl_branch_master as tbm on t.branch_id=tbm.branch_id  where t.orderstatus in (0,1) and t.billing_email!='NULL' and t.addedon >= DATE_SUB(CURDATE(),INTERVAL 3 day) group by t.pacorderid order by t.pacorderid desc ;
end if;


if(act_mode='S_vieworder_new') then
select t.*,group_concat(t1.addonename order by t1.orderaddoneid)  as addon,
 group_concat(t1.addonqty order by t1.orderaddoneid) as addonquantity,
group_concat(t1.addoneprice order by t1.orderaddoneid)  as addonprice,
group_concat(t1.orderaddon_print_status order by t1.orderaddoneid) as orderaddon_print_status, group_concat(t1.orderaddon_print_quantity order by t1.orderaddoneid) as orderaddon_print_quantity from tbl_orderaddone as t1 right join tbl_orderpackage as t on t.pacorderid=t1.lastid  where t.orderstatus in (0,1) and t.billing_email!='NULL' group by t.pacorderid order by t.pacorderid desc ;
end if;



if(act_mode='op_ticket_print_status') then

if(Param2='2') then
update tbl_orderpackage as t set t.op_ticket_print_status=Param2, t.op_printed_date=now()  where t.pacorderid=Param1;
else
update tbl_orderpackage as t set t.op_ticket_print_status=Param2 where t.pacorderid=Param1;
end if;
select op_ticket_print_status as ticket_status from tbl_orderpackage where pacorderid=Param1;
end if;

if(act_mode='S_viewordergp') then
select * from tbl_groupbooking;
end if;


if(act_mode='Total_order_agent') then
select count(t1.orderaddoneid) as ordercnt from tbl_orderaddone as t1 right join tbl_orderpackage as t on t.pacorderid=t1.lastid  where t.orderstatus in (0,1) and t.billing_by=Param1 and t.op_usertype='Agent' and t.billing_email!='NULL'
  group by t.pacorderid order by t.pacorderid desc;
end if;

if(act_mode='last_transaction_amount') then
select t.total as totalamount,t.addedon,t.ticketid from tbl_orderaddone as t1 right join tbl_orderpackage as t on t.pacorderid=t1.lastid  where t.orderstatus in (0,1) and t.billing_by=Param1 and t.op_usertype='Agent' and t.billing_email!='NULL'
  group by t.pacorderid order by t.pacorderid desc limit 1;
end if;

if(act_mode='getmonthtransationorder') then
select DATE(addedon) AS MONTH,DATE_FORMAT(DATE(addedon), "%M") as monthname,SUM(total) as amnt from tbl_orderpackage as t 
left join tbl_orderaddone as t1 on t.pacorderid=t1.lastid  where t.orderstatus in (0,1) and t.billing_by=Param1  and t.op_usertype='Agent' and  t.billing_email!='NULL' and addedon >= NOW() - INTERVAL 6 MONTH GROUP BY MONTH(addedon);

end if;

if(act_mode='getmonthtransationorder') then
select DATE(addedon) AS MONTH,DATE_FORMAT(DATE(addedon), "%M") as monthname,SUM(total) as amnt from tbl_orderpackage as t 
left join tbl_orderaddone as t1 on t.pacorderid=t1.lastid  where t.op_usertype='Agent' and  t.billing_email!='NULL'  and  date(t.addedon) >= Param1 and date(t.addedon) <= Param2 group by m order by t.addedon asc;

end if;

if(act_mode='getmonthtransationordercount') then
select count(t.pacorderid) as ordercnt from tbl_orderpackage as t 
left join tbl_orderaddone as t1 on t.pacorderid=t1.lastid  where t.orderstatus in (0,1) and t.billing_by=Param1  and t.op_usertype='Agent' and  t.billing_email!='NULL' and  addedon >= NOW() - INTERVAL 6 MONTH GROUP BY MONTH(addedon);
end if;
if(act_mode='usertotalamount') then
select sum(t.total) as totalamount from tbl_orderpackage as t 
left join tbl_orderaddone as t1 on t.pacorderid=t1.lastid  where t.orderstatus in (0,1) and t.billing_by=Param1  and t.op_usertype='Agent'
 and  t.billing_email!='NULL';
end if;
if(act_mode='lastcreditamount') then
select * from tbl_agent_wallet where user_id =Param1 order by wid desc limit 1;
end if;


if(act_mode='getorderbyagentlist') then
select sum(t.total) as totalamount,t.billing_name from tbl_orderpackage as t 
left join tbl_orderaddone as t1 on t.pacorderid=t1.lastid  where t.orderstatus in (0,1) and t.op_usertype='Agent' and  t.billing_email!='NULL' group by t.userid order by t.total desc limit 10;
end if;

if(act_mode='count_top10_order_by_agent')then
select a.agent_agencyname as agent,count(o.pacorderid) as totalorder from tbl_orderpackage as o inner join tbl_agent_reg as a on o.billing_by=a.agent_id
 group by a.agent_id order by count(o.pacorderid) desc;
end if;


if(act_mode='count_top10_order_by_date')then
  select a.agent_agencyname as agent,count(o.pacorderid) as totalorder from tbl_orderpackage as o inner join tbl_agent_reg as a on o.billing_by=a.agent_id
where date(o.addedon)>=Param1 and date(o.addedon)<= Param2
group by a.agent_id
order by count(o.pacorderid) desc;
end if;


END$$

CREATE  PROCEDURE `proc_order_v` (IN `act_mode` VARCHAR(50), IN `orderid` INT)  BEGIN
if(act_mode='select_order') then
select * from  tbl_orderpackage as t where t.pacorderid=orderid;
end if;
if(act_mode='select_package') then
select * from  tbl_orderpackage  as pack where pack.pacorderid=orderid ;
end if;
if(act_mode='select_addone') then
select * from  tbl_orderaddone as pack where pack.lastid=orderid and pack.Type='Addone';
end if;

if(act_mode='orderpaymentupdatedata') then
update  tbl_orderpackage as t set t.ordermailstatus='1' where pacorderid=orderid ;

end if;

if(act_mode='select_orderjason') then

select * from  tbl_orderpackage as t where op_ordertrackingupdate!='24' order by pacorderid desc ; 

end if;

if(act_mode='select_orderjasonup') then

select * from  tbl_orderpackage as t where t.op_orderjason=0; 

end if;

if(act_mode='get_day_count') then
select count(DISTINCT(DAYOFMONTH(`created_on`))) as countorder from tbl_user_commission_detail where  created_on <= NOW() 
and invoice_id ='' and update_status=0 ;
end if;



END$$

CREATE  PROCEDURE `proc_order_vdate` (IN `act_mode` VARCHAR(50), IN `orderid` INT, IN `order_status_date_time` VARCHAR(50))  BEGIN

if(act_mode='orderpaymentupdatejsonnew') then
update  tbl_orderpackage as t set t.order_status_date_time=order_status_date_time , t.op_orderjason=1 where pacorderid=orderid ;

end if;



END$$

CREATE  PROCEDURE `proc_order_vf` (IN `act_mode` VARCHAR(50), IN `orderid` INT, IN `tracking_id` VARCHAR(50), IN `order_status` VARCHAR(50), IN `status_message` VARCHAR(50), IN `paymentmode` VARCHAR(50), IN `paymentstatus` VARCHAR(50), IN `ordersucesmail` INT, IN `ordermailstatus` INT)  BEGIN


if(act_mode='orderpaymentupdate') then
update  tbl_orderpackage as t set t.paymentstatus=paymentstatus,t.paymentmode='CCAVENUE',t.tracking_id=tracking_id,t.order_statuspayment=order_status,t.order_status=order_status,t.status_message=status_message,t.paymenttype=paymentmode,t.ordersucesmail=ordersucesmail,t.ordermailstatus=ordermailstatus where t.pacorderid=orderid ;

end if;

if(act_mode='orderpaymentupdatejson') then
update  tbl_orderpackage as t set t.paymentstatus=paymentstatus,t.paymentmode='CCAVENUE',t.tracking_id=tracking_id,t.order_status=order_status,t.status_message=status_message,t.paymenttype=paymentmode,t.ordersucesmail=ordersucesmail,t.ordermailstatus=ordermailstatus, t.op_ordertrackingupdate=(t.op_ordertrackingupdate+1) where t.pacorderid=orderid ;

end if;

if(act_mode='orderpaymentupdatesucess') then
update  tbl_orderpackage as t set t.paymentstatus=paymentstatus,t.paymentmode='CCAVENUE',t.tracking_id=tracking_id,t.order_statuspayment=order_status,t.order_status=order_status,t.status_message=status_message,t.paymenttype=paymentmode,t.ordersucesmail=ordersucesmail,t.ordermailstatus=ordermailstatus, t.op_ordertrackingupdate=3 where t.pacorderid=orderid ;

end if;

if(act_mode='orderpdfupdatesucess') then
update  tbl_orderpackage as t set t.billing_pdf=tracking_id where t.pacorderid=orderid ;

end if;
if(act_mode='credit_debit_insert') then
insert into tbl_agent_wallet(user_id,credit_debit_amount,credit_debit_pety,total_amount,Status) values();
end if;


END$$

CREATE  PROCEDURE `proc_packages_s` (IN `act_mode` VARCHAR(50), IN `Param1` VARCHAR(50), IN `Param2` TEXT, IN `Param3` VARCHAR(50), IN `Param4` VARCHAR(50), IN `Param5` VARCHAR(50), IN `Param6` VARCHAR(50), IN `Param7` VARCHAR(50), IN `Param8` VARCHAR(50), IN `Param9` VARCHAR(50))  BEGIN

if(act_mode='s_addpackage_master') then
INSERT INTO `tbl_package_master`(`package_name`, `package_description`, `package_status`,`package_price`,package_image) VALUES (Param1,Param2,'1',Param3,Param4);
select last_insert_id() as pack_id;
end if;

if(act_mode='s_addpackage_branch') then
INSERT INTO `tbl_branch_package`(`bp_packageid`, `bp_branchid`, `bp_status`) VALUES (Param1,Param2,'1');
end if;

if(act_mode='s_addpackage_activity') then
INSERT INTO `tbl_activity_package`(`ap_packageid`, `ap_activityid`, `ap_status`) VALUES (Param1,Param2,'1');
end if;

if(act_mode='s_viewbranch') then
select * from  tbl_branch_master as t where t.branch_status=1;
end if;

if(act_mode='s_viewactivity') then
select * from  tbl_activity as t where t.activity_status=1;
end if;

if(act_mode='s_viewpackage') then
select t.*,(select group_concat(tbp.bp_branchid) from tbl_branch_package as tbp where tbp.bp_packageid=t.package_id and tbp.bp_status=1) as bp_id,(select group_concat(tap.ap_activityid) from tbl_activity_package as tap where tap.ap_packageid=t.package_id) as ap_id from tbl_package_master as t
where t.package_status!=2;

end if;

if(act_mode='activityPrice') then
select ta.activity_price  from tbl_activity as ta where ta.activity_id=Param1;
end if;

if(act_mode='getwalletprice') then
select *  from tbl_agent_wallet as aw where aw.user_id=Param1 order by aw.wid desc limit 1;
end if;


if(act_mode='delete_package') then
update tbl_package_master as t set t.package_status='2' where t.package_id=Param1;
end if;

if(act_mode='update_package_data') then
select *,(select group_concat(tbp.bp_branchid) from tbl_branch_package as tbp where tbp.bp_packageid=t.package_id and tbp.bp_status=1) as bp_id from  tbl_package_master as t where t.package_id=Param1;
end if;




if(act_mode='s_addpackage_master_update') then
update tbl_package_master as t set t.package_name=Param1, t.package_description=Param2,t.package_modifiedon=now(),t.package_price=Param3 where t.package_id=Param4;
update tbl_branch_package as t set t.bp_status=2 where t.bp_packageid=Param4;
end if;

if(act_mode='s_addpackage_branch_update') then
INSERT INTO `tbl_branch_package`(`bp_packageid`, `bp_branchid`, `bp_status`) VALUES (Param1,Param2,'1');
end if;
if(act_mode='get_commission_list') then
select * from  tbl_commission as t where 1;
end if;

if(act_mode='getorderlistagent') then
select count(t.ticketid) as cntorder from tbl_orderpackage as t 
left join tbl_orderaddone as t1 on t.pacorderid=t1.lastid  where t.orderstatus in (0,1) and t.op_usertype='Agent' and  t.billing_email!='NULL' and t.order_status IN('Success','Shipped') group by t.userid order by t.total desc limit 10;
end if;


if(act_mode='getwalletamount') then

select sum(ud.commission_price) as commprice,ud.invoice_id,ud.uid,ud.commission from  tbl_user_commission_detail ud where ud.uid =Param1 and  ud.status='P' group by ud.invoice_id;
end if;

if(act_mode='getwalletamount_history') then

select sum(ud.commission_price) as commprice,ud.invoice_id,ud.uid,ud.commission from  tbl_user_commission_detail ud where ud.uid =Param1 and  ud.status='S' group by ud.invoice_id;
end if;

if(act_mode='getwalletamount_total_history_list_detail') then
select * from  tbl_user_commission_detail ud where ud.uid =Param1 and update_status=1 and  ud.status='P';
end if;

if(act_mode='get_commission_payment') then
select * from  tbl_commissioin_payment_date where 1;
end if;

if(act_mode='get_commission_payment_history_detail') then
select sum(ud.commission_price) as commprice,ud.invoice_id,ud.uid,ud.commission from  tbl_user_commission_detail ud where  ud.status='S' group by ud.invoice_id;
end if;
    

if(act_mode='update_commission_payment') then
update tbl_commissioin_payment_date cp set cp.commission_time =Param2 where 1;
end if;

if(act_mode='get_commission_order') then
select * from tbl_user_commission_detail where invoice_id ='' and created_on <= NOW() and update_status=0 order by uid;
end if;

if(act_mode='get_commission_order_user_array') then
select count(uid) as cntuser,uid from tbl_user_commission_detail where invoice_id ='' and created_on <= NOW() and update_status=0 group by uid order by uid;
end if;

if(act_mode='get_day_count_start') then
select * from tbl_user_commission_detail where  created_on <= NOW() 
and invoice_id ='' and update_status=0 ORDER BY created_on DESC LIMIT 1;

end if;

if(act_mode='get_day_count_end') then
select * from tbl_user_commission_detail where  created_on <= NOW() 
and invoice_id ='' and update_status=0 ORDER BY created_on ASC LIMIT 1;
end if;

if(act_mode='get_total_order_payment') then
select count(c_id) as ordercnt from tbl_user_commission_detail where invoice_id ='' and created_on <= NOW()  and update_status=0;
end if;
if(act_mode='update_status_commission') then
update tbl_user_commission_detail set  status ='S' where  created_on <= NOW()  and update_status=1 and uid=Param1;
end if;

if(act_mode='update_order_payment_invoice') then
update tbl_user_commission_detail cd set cd.commission_price= Param1,cd.invoice_id=Param2,cd.update_status=1,cd.update_on = now(),cd.commission=Param4 where cd.c_id= Param5;
end if;
END$$

CREATE  PROCEDURE `proc_packages_v` (IN `act_mode` VARCHAR(50), IN `branchid` INT, IN `packageid` INT, IN `packageqty` INT, IN `txtDepartDate` DATE)  BEGIN

if(act_mode='view_package')then
set @pkgqty=packageqty;
select package_id,package_name,package_image,package_description,package_price,@pkgqty as pkg from tbl_package_master a join tbl_branch_package b on b.bp_packageid=a.package_id where a.package_id=packageid  and a.package_status =1 order by package_name;

end if;

if(act_mode='select_package')then
set @branch_id_new = branchid;

select a.package_image,a.package_id,a.package_name,a.package_description,a.package_price from tbl_package_master a join tbl_branch_package b on b.bp_packageid=a.package_id where b.bp_branchid=branchid and a.package_status =1 and b.bp_status=1 order by a.package_id asc limit 0,1;
end if;

if(act_mode='select_packagepartner')then
set @branch_id_new = branchid;

select a.package_image,a.package_id,a.package_name,a.package_description,a.package_price from tbl_package_master a join tbl_branch_package b on b.bp_packageid=a.package_id where b.bp_branchid=branchid and a.package_status =1  and b.bp_status=1 order by a.package_id desc ;
end if;

if(act_mode='select_packageuserguest')then
set @branch_id_new = branchid;

select a.package_id,a.package_image,a.package_name,a.package_description,a.package_price,
c.dailyinventory_from,c.dailyinventory_minfrom,
c.dailyinventory_to,c.dailyinventory_minto,c.dailyinventory_seats,c.dailyinventory_createdon,c.dailyinventory_date
from tbl_package_master a 
LEFT join tbl_branch_package b on b.bp_packageid=a.package_id 
LEFT join tbl_daily_inventorypackages d on d.dailyinventorypackages_packagesid = a.package_id
LEFT join tbl_daily_inventory c on c.dailyinventory_id = d.dailyinventorypackages_dailyinventoryid 
where b.bp_branchid=1 and a.package_status =1 and b.bp_status=1 and  c.dailyinventory_date = txtDepartDate;
end if;


if(act_mode='ticketcart_package')then
select * from tbl_package_master  where tbl_package_master.package_id=packageid ;
end if;
END$$

CREATE  PROCEDURE `proc_partnerlogin_v` (IN `act_mode` VARCHAR(50), IN `pusername` VARCHAR(50), IN `ppassword` VARCHAR(50))  BEGIN
if(act_mode='partnerlogincorporate')then
select * from tbl_agent_reg where agent_username=pusername and agent_status='1' and logintype='1' ;
end if;
 
if(act_mode='partnerloginofficer')then

select * from tbl_agent_reg where agent_username=pusername and agent_status='1' and  logintype='2';
end if;

END$$

CREATE  PROCEDURE `proc_partnerregister_v` (IN `act_mode` VARCHAR(50), IN `title` VARCHAR(50), IN `firstname` VARCHAR(50), IN `lastname` VARCHAR(50), IN `row_id` INT, IN `logintype` VARCHAR(50), IN `agencyname` VARCHAR(50), IN `office_address` TEXT, IN `city` VARCHAR(50), IN `state` VARCHAR(50), IN `pincode` VARCHAR(50), IN `telephone` VARCHAR(50), IN `mobile` VARCHAR(50), IN `fax` VARCHAR(50), IN `website` VARCHAR(50), IN `email` VARCHAR(50), IN `primarycontact_name` VARCHAR(50), IN `primarycontact_email_mobile` VARCHAR(50), IN `primarycontact_telephone` INT, IN `secondrycontact_name` VARCHAR(50), IN `secondrycontact_email_phone` VARCHAR(50), IN `secondrycontact_telephone` INT, IN `management_executives_name` VARCHAR(50), IN `management_executives_email_phone` VARCHAR(50), IN `management_executives_telephone` INT, IN `branches` VARCHAR(50), IN `yearofestablism` VARCHAR(50), IN `organisationtype` VARCHAR(50), IN `lstno` VARCHAR(50), IN `cstno` VARCHAR(50), IN `registrationno` VARCHAR(50), IN `vatno` VARCHAR(50), IN `panno` VARCHAR(50), IN `servicetaxno` VARCHAR(50), IN `tanno` VARCHAR(50), IN `pfno` VARCHAR(50), IN `esisno` VARCHAR(50), IN `officeregistrationno` VARCHAR(50), IN `exceptiontax` VARCHAR(50), IN `otherexceptiontax` VARCHAR(50), IN `bank_benificialname` VARCHAR(50), IN `bank_benificialaccno` VARCHAR(50), IN `bank_benificialbankname` VARCHAR(50), IN `bank_benificialbranchname` VARCHAR(50), IN `bank_benificialaddress` TEXT, IN `bank_benificialifsc` VARCHAR(50), IN `bank_benificialswiftcode` VARCHAR(50), IN `bank_benificialibanno` VARCHAR(50), IN `intermidiatebankname` VARCHAR(50), IN `intermidiatebankaddress` TEXT, IN `intermidiatebankswiftcode` VARCHAR(50), IN `bank_ecs` VARCHAR(50), IN `copypancart` VARCHAR(50), IN `copyservicetax` VARCHAR(50), IN `copytan` VARCHAR(50), IN `copyaddressproof` VARCHAR(50), IN `emailstatus` VARCHAR(50), IN `emailcurrency` VARCHAR(50), IN `emailcountry` VARCHAR(50), IN `username` VARCHAR(50), IN `password` VARCHAR(50), IN `countryid` INT, IN `stateid` INT, IN `cityid` INT, IN `branch_id` INT, IN `locationid` INT, IN `bank_creditamount` VARCHAR(50), IN `bank_agentcommision` VARCHAR(50), IN `aadhaar_number` VARCHAR(50), IN `ppid` INT, IN `pppermission` TEXT)  BEGIN
if(act_mode='insertpartner')then
INSERT INTO tbl_agent_reg (`agent_agencyname`, `agent_office_address`,`agent_city`, `agent_state`, `agent_pincode` , `agent_telephone`, `agent_mobile`, `agent_fax`, `agent_website`, `agent_email`, `agent_primarycontact_name`, `agent_primarycontact_email_mobile`, `agent_primarycontact_telephone`, `agent_secondrycontact_name`, `agent_secondrycontact_email_phone`, `agent_secondrycontact_telephone`, `agent_management_executives_name`, `agent_management_executives_email_phone`, `agent_management_executives_telephone`, `agent_branches`, `agent_yearofestablism`, `agent_organisationtype`, `agent_lstno`, `agent_cstno`, `agent_registrationno`, `agent_vatno`,`agent_panno`, `agent_servicetaxno`, `agent_tanno`, `agent_pfno`, `agent_esisno`, `agent_officeregistrationno`, `agent_exceptiontax`, `agent_otherexceptiontax`, `agent_bank_benificialname`,agent_bank_benificialaccno,agent_bank_benificialbankname, bank_benificialaddress, bank_benificialifsc, bank_benificialswiftcode, bank_benificialibanno,`agent_intermidiatebankname`,`agent_intermidiatebankaddress`,`intermidiatebankswiftcode`,`agent_bank_ecs`,`agent_copypancart`, `agent_copyservicetax`,`agent_copytan`, `agent_copyaddressproof`,  `agent_emailstatus`,`agent_emailcurrency`, `agent_emailcountry`, `agent_username`, `agent_password`, `agent_createdon`, `agent_status`,`logintype`,`countryid`,`stateid`,`cityid`,`branch_id`,`locationid`,`agent_aadhar`)
VALUES (agencyname, office_address, city, state, pincode, telephone, mobile, fax, website, email, primarycontact_name, primarycontact_email_mobile, primarycontact_telephone, secondrycontact_name, secondrycontact_email_phone, secondrycontact_telephone, management_executives_name, management_executives_email_phone, management_executives_telephone, branches, yearofestablism, organisationtype, lstno, cstno, registrationno, vatno,panno, servicetaxno, tanno, pfno, esisno
, officeregistrationno, exceptiontax, otherexceptiontax,bank_benificialname, bank_benificialaccno, bank_benificialbankname, bank_benificialaddress, bank_benificialifsc, bank_benificialswiftcode, bank_benificialibanno, intermidiatebankname, intermidiatebankaddress, intermidiatebankswiftcode, bank_ecs, copypancart, copyservicetax,copytan,copyaddressproof,emailstatus,emailcurrency,emailcountry,username,password,NOW(),'0',logintype,countryid,stateid,cityid,branch_id,locationid,aadhaar_number);
end if;


if(act_mode='insertpartnerstartstage')then

INSERT INTO tbl_agent_reg (`agent_title`,`agent_firstname`,`agent_lastname`, `agent_mobile`, `agent_password`, `agent_createdon`, `agent_status`,`countryid`,`agent_email`,`stateid`,`cityid`,`branch_id`,`locationid`,`agent_username`,`agent_aadhar`)VALUES (title,firstname, lastname, mobile, password, NOW(),'0',countryid,username,stateid,cityid,branch_id,locationid,username,aadhaar_number);
select last_insert_id() as lid;
end if;


if(act_mode='selectpartneremail')then

select agent_id from  tbl_agent_reg where agent_username=username;
end if;



if(act_mode='selectpartner')then

select ar.*,fn_credit_debit(ar.agent_id) as credit_debit from  tbl_agent_reg as ar where ar.agent_status in (0,1);
end if;

if(act_mode='selectpartner_histry')then

select ar.*,fn_credit_debit(ar.agent_id) as credit_debit from  tbl_agent_reg as ar INNER JOIN  tbl_user_commission_detail ud
on ar.agent_id = ud.uid where ar.agent_status in (0,1) and ud.status ='S'  group by ud.invoice_id;
end if;

if(act_mode='agentdelete')then

update  tbl_agent_reg set 	agent_status=2 where agent_id=row_id;
end if;



if(act_mode='activeandinactive') then
update tbl_agent_reg set tbl_agent_reg.agent_status=0 where tbl_agent_reg.agent_id=row_id;
end if;
if(act_mode='inactiveandinactive') then
update tbl_agent_reg set tbl_agent_reg.agent_status=1 where tbl_agent_reg.agent_id=row_id;
end if;

if(act_mode='inactiveandinactive') then
update tbl_agent_reg set tbl_agent_reg.agent_status=1 where tbl_agent_reg.agent_id=row_id;
end if;




if(act_mode='updatepartner')then
update  tbl_agent_reg as t set t.agent_agencyname=agencyname,t.agent_office_address=office_address, t.agent_state=state, t.agent_pincode=pincode , t.agent_telephone=telephone, t.agent_mobile=mobile, t.agent_fax=fax, t.agent_website=website, t.agent_email=email, t.agent_primarycontact_name=primarycontact_name, t.agent_primarycontact_email_mobile=primarycontact_email_mobile, t.agent_primarycontact_telephone=primarycontact_telephone, t.agent_secondrycontact_name=secondrycontact_name, t.agent_secondrycontact_email_phone=secondrycontact_email_phone, t.agent_secondrycontact_telephone=secondrycontact_telephone, t.agent_management_executives_name=management_executives_name, t.agent_management_executives_email_phone=management_executives_email_phone, t.agent_management_executives_telephone=management_executives_telephone, t.agent_branches=branches, t.agent_yearofestablism=yearofestablism, t.agent_organisationtype=organisationtype, t.agent_lstno=lstno, t.agent_cstno=cstno, t.agent_registrationno=registrationno, t.agent_vatno=vatno,t.agent_panno=panno, t.agent_servicetaxno=servicetaxno, t.agent_tanno=tanno, t.agent_pfno=pfno, t.agent_esisno=esisno, t.agent_officeregistrationno=officeregistrationno, t.agent_exceptiontax=exceptiontax, t.agent_otherexceptiontax=otherexceptiontax, t.agent_bank_benificialname=bank_benificialname,t.agent_bank_benificialaccno=bank_benificialaccno,t.agent_bank_benificialbankname=bank_benificialbankname, t.bank_benificialaddress=bank_benificialaddress, t.bank_benificialifsc=bank_benificialifsc, t.bank_benificialswiftcode=bank_benificialswiftcode, t.bank_benificialibanno=bank_benificialibanno,t.agent_intermidiatebankname=intermidiatebankname,t.agent_intermidiatebankaddress=intermidiatebankaddress,t.intermidiatebankswiftcode=intermidiatebankswiftcode,t.agent_bank_ecs=bank_ecs,t.agent_copypancart=copypancart, t.agent_copyservicetax=copyservicetax,t.agent_copytan=copytan, t.agent_copyaddressproof=copyaddressproof,  t.agent_emailstatus=emailstatus,t.agent_emailcurrency=emailcurrency, t.agent_emailcountry=emailcountry, t.agent_username=username, t.logintype=1
=city,t.agent_state=state,t.agent_pincode=pincode,t.agent_telephone=telephone,t.agent_mobile=mobile,t.agent_fax=fax,t.agent_website=website,t.agent_email=email,t.agent_primarycontact_name=primarycontact_name,t.agent_primarycontact_email_mobile=primarycontact_email_mobile,t.agent_bank_benificialbranchname=bank_benificialbranchname,t.bank_creditamount=t.bank_creditamount+bank_creditamount,t.bank_agentcommision=bank_agentcommision ,t.agent_aadhar=aadhaar_number where t.agent_id=row_id;

update  tbl_agent_reg as t set t.logintype=logintype  where t.agent_id=row_id;

end if;
if(act_mode='insertpartnerstep1')then

update  tbl_agent_reg set 	logintype=logintype,agent_emailcountry=emailcountry,agent_agencyname=agencyname,agent_office_address=office_address,	agent_city=	city, agent_mobile=mobile,agent_state=state,agent_pincode=pincode,agent_telephone=telephone,agent_website=website,agent_fax=fax,	agent_emailstatus=emailstatus,	agent_emailcurrency=emailcurrency where agent_id=row_id and agent_username=username;


end if;

if(act_mode='insertpartnerstep2')then
update  tbl_agent_reg set agent_primarycontact_name= primarycontact_name,agent_primarycontact_email_mobile=primarycontact_email_mobile,agent_primarycontact_telephone=primarycontact_telephone,agent_secondrycontact_name=secondrycontact_name,	agent_secondrycontact_email_phone=secondrycontact_email_phone,agent_secondrycontact_telephone=secondrycontact_telephone,agent_management_executives_name=management_executives_name,agent_management_executives_email_phone=management_executives_email_phone,	agent_branches=branches,agent_yearofestablism=yearofestablism,	agent_organisationtype=organisationtype  where agent_id=row_id and agent_username=username;
end if;

if(act_mode='insertpartnerstep4')then
update  tbl_agent_reg set  agent_lstno=lstno,agent_cstno=cstno,agent_registrationno=registrationno,agent_vatno=vatno,agent_panno=panno,agent_servicetaxno=servicetaxno,agent_tanno=tanno,agent_pfno=pfno,agent_esisno=esisno,agent_officeregistrationno=officeregistrationno,	agent_exceptiontax=exceptiontax,agent_otherexceptiontax=otherexceptiontax where agent_id=row_id and agent_username=username;
end if;

if(act_mode='insertpartnerstep5')then

update  tbl_agent_reg set agent_bank_benificialname=bank_benificialname ,agent_bank_benificialaccno=bank_benificialaccno,agent_bank_benificialbankname=bank_benificialbankname,agent_bank_benificialbranchname=bank_benificialbranchname,bank_benificialaddress=bank_benificialaddress ,bank_benificialifsc=bank_benificialifsc,bank_benificialswiftcode=bank_benificialswiftcode,bank_benificialibanno=bank_benificialibanno ,agent_intermidiatebankname=intermidiatebankname,agent_intermidiatebankaddress=intermidiatebankaddress,intermidiatebankswiftcode=intermidiatebankswiftcode ,agent_bank_ecs=bank_ecs where agent_id=row_id and agent_username=username;
end if;

if(act_mode='insertpartnerstep6')then
update  tbl_agent_reg set  agent_copytan=copytan,agentprofilecomplete=1,agent_copyaddressproof=copyaddressproof,agent_copypancart=copypancart,agent_copyservicetax=copyservicetax where agent_id=row_id and agent_username=username;
end if;

if(act_mode='insertsubpartner')then

INSERT INTO tbl_agent_reg (`agent_title`,`agent_firstname`,`agent_lastname`, `agent_mobile`, `agent_password`, `agent_createdon`, `agent_status`,`countryid`,`agent_email`,`stateid`,`cityid`,`branch_id`,`locationid`,`agent_username`,`agent_aadhar`,`agent_ppid`,`agent_pppermission`)VALUES (title,firstname, lastname, mobile, password, NOW(),'0',countryid,username,stateid,cityid,branch_id,locationid,username,aadhaar_number,ppid,pppermission);
select last_insert_id() as lid;
end if;

if(act_mode='selectsubpartner')then

SELECT * FROM `tbl_user` where 	created_by = bank_creditamount;

end if;
if(act_mode='selectpartnerdata') then

select * from tbl_agent_reg where agent_id=row_id;

end if;

if(act_mode='selectpartnerdatauser') then
SELECT * FROM `tbl_user` where user_id = row_id;

end if;



if(act_mode='updateagentpartner')then
update  tbl_agent_reg as t set t.agent_title=title,t.agent_firstname=firstname,t.agent_lastname=lastname where agent_id=row_id;


end if;
END$$

CREATE  PROCEDURE `proc_payment_s` (IN `act_mode` VARCHAR(50), IN `Param1` VARCHAR(50), IN `Param2` VARCHAR(50), IN `Param3` VARCHAR(50), IN `Param4` VARCHAR(50), IN `Param5` VARCHAR(50), IN `Param6` VARCHAR(50), IN `Param7` VARCHAR(50), IN `Param8` VARCHAR(50), IN `Param9` VARCHAR(50), IN `Param10` VARCHAR(50))  BEGIN


if(act_mode='paymentinsert') then
INSERT INTO `tbl_payment_gatway`(`pg_branchid`, `pg_merchant_id`, `pg_access_key`, `pg_working_key`, `pg_sucess_link`, `pg_fail_link`, `pg_currency`, `pg_language`,pg_prefix,pg_agent_prefix) VALUES (Param1,Param2,Param8,Param3,Param4,Param5,Param6,Param7,Param9,Param10);
end if;

if(act_mode='paymentview') then
select p.*,b.branch_name from tbl_payment_gatway as p left join tbl_branch_master as b on b.branch_id=p.pg_branchid where p.pg_status in (0,1); 
end if;


if(act_mode='paymentdelete') then
update tbl_payment_gatway as p set p.pg_status=2 where p.pg_id=Param1;
end if;

if(act_mode='paymentstatus') then
update  tbl_payment_gatway as p set  p.pg_status=Param2 where p.pg_id=Param1;
end if;

if(act_mode='paymentup') then
select * from  tbl_payment_gatway as p  where p.pg_id=Param1;
end if;


if(act_mode='paymentupdate') then
update  tbl_payment_gatway as p set p.pg_branchid=Param1, p.pg_merchant_id=Param2, p.pg_access_key=Param8,p.pg_working_key=Param3,p.pg_sucess_link=Param4,p.pg_fail_link=Param5,p.pg_currency=Param6,p.pg_language=Param7,p.pg_prefix=Param9,p.pg_updated=now() where p.pg_id=Param10;

end if;


END$$

CREATE  PROCEDURE `proc_promocode_package` (IN `act_mode` VARCHAR(100), IN `Param1` VARCHAR(500), IN `Param2` VARCHAR(500), IN `Param3` VARCHAR(500), IN `Param4` VARCHAR(500), IN `Param5` VARCHAR(500), IN `Param6` VARCHAR(500), IN `Param7` VARCHAR(500), IN `Param8` VARCHAR(500), IN `Param9` VARCHAR(500), IN `Param10` VARCHAR(500), IN `Param11` VARCHAR(500), IN `Param12` VARCHAR(500), IN `Param13` VARCHAR(500), IN `Param14` VARCHAR(500), IN `Param15` VARCHAR(500), IN `Param16` VARCHAR(500), IN `Param17` VARCHAR(500), IN `Param18` VARCHAR(500), IN `Param19` VARCHAR(500), IN `Param20` VARCHAR(500))  BEGIN

if(act_mode='paymentinsert') then
INSERT INTO `tbl_payment_gatway`(`pg_branchid`, `pg_merchant_id`, `pg_access_key`, `pg_working_key`, `pg_sucess_link`, `pg_fail_link`, `pg_currency`, `pg_language`,pg_prefix,pg_agent_prefix,pg_order_sucess_url,pg_order_fail_url,pg_wallet_sucess_url,pg_wallet_fail_url,pg_prefix_agent_wallet) VALUES (Param1,Param2,Param8,Param3,Param4,Param5,Param6,Param7,Param9,Param10,Param15,Param16,Param11,Param12,Param13);
end if;

if(act_mode='paymentupdate') then
update  tbl_payment_gatway as p set p.pg_branchid=Param1, p.pg_merchant_id=Param2, p.pg_access_key=Param8,p.pg_working_key=Param3,p.pg_sucess_link=Param4,p.pg_fail_link=Param5,p.pg_currency=Param6,p.pg_language=Param7,p.pg_prefix=Param9,pg_agent_prefix=Param10,p.pg_updated=now(),p.pg_order_sucess_url=Param14,p.pg_order_fail_url=Param15,p.pg_wallet_sucess_url=Param12,p.pg_wallet_fail_url=Param13,p.pg_prefix_agent_wallet=Param16 where p.pg_id=Param11;

end if;


if(act_mode = 'insert_promo') then
INSERT INTO `tbl_promo`(`p_branchid`, `p_codename`, `p_type`, `p_discount`, `p_status`, `p_expiry_date`, `p_allowed_per_user`,
 `p_start_date`, `p_allowed_times`,p_logintype,p_addonedata,p_addoneqty,p_usertype) VALUES 
 (Param1,Param2,Param3,Param4,'1',Param8,Param5,Param7,Param6,Param9,Param10,Param11,Param9);
select last_insert_id() as ids;
end if;

if(act_mode = 'insert_package') then
INSERT INTO `tbl_promopackage`(`promopackagepromo_id`, `promopackagedailyinventory_id`, `promopackage_timesallowed`, `promopackage_peruserallowed`) 
VALUES (Param1,Param2,Param3,Param4);
end if;

if(act_mode = 'update_promo') then

update tbl_promo set p_branchid=Param1,p_codename=Param2,p_type=Param3,p_discount=Param4,p_expiry_date=Param8,p_allowed_per_user=Param5,p_start_date=Param7,p_allowed_times=Param6,p_logintype=Param9,p_addonedata=Param10,p_addoneqty=Param11
,p_usertype =Param9 where promo_id=Param12;
end if;
if(act_mode = 'delete_promocode') then
delete from tbl_promopackage where promopackagepromo_id = Param1;
end if;

END$$

CREATE  PROCEDURE `proc_promocode_s` (IN `act_mode` VARCHAR(50), IN `Param1` VARCHAR(50), IN `Param2` VARCHAR(50), IN `Param3` VARCHAR(50), IN `Param4` VARCHAR(50), IN `Param5` VARCHAR(50), IN `Param6` VARCHAR(50), IN `Param7` VARCHAR(50), IN `Param8` VARCHAR(50), IN `Param9` VARCHAR(50), IN `Param10` VARCHAR(50), IN `Param11` VARCHAR(50), IN `Param12` VARCHAR(50))  BEGIN

if(act_mode = 'insert_promo') then
INSERT INTO `tbl_promo`(`p_branchid`, `p_codename`, `p_type`, `p_discount`, `p_status`, `p_expiry_date`, `p_allowed_per_user`, `p_start_date`, `p_allowed_times`,p_logintype,p_addonedata,p_addoneqty) VALUES (Param1,Param2,Param3,Param4,'1',Param8,Param5,Param7,Param6,Param9,Param10,Param11);
end if;

if(act_mode='view_promo') then
select * from  tbl_promo as t where t.p_status in (1,0);
end if;

if(act_mode='delete_promo') then
update tbl_promo as t set t.p_status='2' where promo_id=Param1;
end if;

if(act_mode='status_promo') then
update tbl_promo as t set t.p_status=Param2 where promo_id=Param1;
end if;

if(act_mode='editselect_promo') then
select * from  tbl_promo  where promo_id=Param1;
end if;

if(act_mode='editselect_promo_list_package') then
select * from  tbl_promopackage  where promopackagepromo_id = Param1;
end if;

if(act_mode='editselect_promo_package') then
select di.*,dip.*,m.package_name from  tbl_daily_inventory di join tbl_daily_inventorypackages dip on 
di.dailyinventory_id =dip.dailyinventorypackages_dailyinventoryid JOIN tbl_package_master m ON
 dip.dailyinventorypackages_packagesid = m.package_id where  di.dailyinventory_id =Param1;

end if;

if(act_mode = 'update_promo') then

update tbl_promo set p_branchid=Param1,p_codename=Param2,p_type=Param3,p_discount=Param4,p_expiry_date=Param8,p_allowed_per_user=Param5,p_start_date=Param7,p_allowed_times=Param6,p_logintype=Param9,p_addonedata=Param10,p_addoneqty=Param11 where promo_id=Param12;

end if;

if(act_mode='getpacagedata') then
select * from  tbl_daily_inventory  where  dailyinventory_date >= Param2 and dailyinventory_date <= Param3 group by dailyinventory_date;
end if;
if(act_mode='getpacagedata_time') then
select * from  tbl_daily_inventory  where  dailyinventory_date = Param2;
end if;	
if(act_mode='getpacagedata_pakage') then
select di.*,ppk.promopackage_timesallowed,ppk.promopackage_peruserallowed from  tbl_daily_inventory di  left join tbl_promopackage ppk on  
di.dailyinventory_id= ppk.promopackagedailyinventory_id where  di.dailyinventory_id =Param3;

#select di.*,dip.*,m.package_name,ppk.promopackage_timesallowed,ppk.promopackage_peruserallowed from  tbl_daily_inventory di join tbl_daily_inventorypackages dip on #di.dailyinventory_id =dip.dailyinventorypackages_dailyinventoryid JOIN tbl_package_master m ON dip.dailyinventorypackages_packagesid = m.package_id left join tbl_promopackage ppk on  di.dailyinventory_id= ppk.promopackagedailyinventory_id where  di.dailyinventory_id =Param3;
end if;

if(act_mode='get_user_detail') then
select * from  tbl_user u  where  u.user_id = Param2;
end if;
if(act_mode='get_user_detail_agent') then
select * from  tbl_agent_reg u  where  u.agent_id = Param2;
end if;

if(act_mode='checkpromode_condition') then
select * from  tbl_promo p INNER JOIN tbl_promopackage pp on p.promo_id= pp.promopackagepromo_id where  p.p_codename = Param3;
end if;

if(act_mode='datevalidation') then
select count(promo_id) as pcnt from  tbl_promo where  p_start_date <= Param3 and p_expiry_date >= Param3;
end if;
if(act_mode='checkpromode_user_type') then
select *  from  tbl_promo p where  p.p_codename = Param3;
end if;

if(act_mode='checkpromocode') then
select count(p.promo_id) as cnt from  tbl_promo p where  p.p_codename = Param3;
end if;

if(act_mode='checkpromode_date_time') then

SELECT * FROM  tbl_promopackage pp join tbl_daily_inventory di on pp.promopackagedailyinventory_id = di.dailyinventory_id  WHERE 
pp.promopackagepromo_id =Param6 and di.dailyinventory_date =Param3 and  di.dailyinventory_from= Param7
and di.dailyinventory_minfrom = Param8 and di.dailyinventory_to =Param9 and di.dailyinventory_minto =Param10;
end if;

if(act_mode='checkpromocodecount_inventery') then
SELECT count(*) as cntpromo FROM `tbl_user_promo` p WHERE p.`p_id` =Param3 and p.`inventory_id` = Param5;
end if;
 
if(act_mode='checkpromocodecount_user_inventery') then
SELECT count(DISTINCT(p.uid)) as cntpromouid FROM `tbl_user_promo` p WHERE p.`p_id` =Param3 and p.`inventory_id` = Param5 ;
end if;

if(act_mode='getaddonlist') then
SELECT * FROM `tbl_addon`  WHERE  FIND_IN_SET(addon_id,Param3);
end if;


END$$

CREATE  PROCEDURE `proc_promodata_s` (IN `act_mode` VARCHAR(50), IN `promoname` VARCHAR(50), IN `lastidses` VARCHAR(50))  BEGIN
if(act_mode='orderaddone_insert') then
update tbl_orderpackage as t set t.billing_promocode=promoname where pacorderid=lastidses;
end if;
END$$

CREATE  PROCEDURE `proc_promo_v` (IN `act_mode` VARCHAR(50), IN `promovalue` VARCHAR(50), IN `locationid` INT, IN `branch_id` INT, IN `userid` VARCHAR(50))  BEGIN
if(act_mode= 'promoselect_package') THEN
SELECT *  FROM tbl_promo AS  a  WHERE  a.p_branchid = branch_id and p_codename=promovalue and p_status=1 and (userid between p_start_date and p_expiry_date); 
END IF;

if(act_mode= 'promoselectuser_package') THEN
SELECT count(pacorderid) as num  FROM tbl_orderpackage AS  a  WHERE  a.userid = userid and a.p_codename=promovalue and a.paymentstatus=1 ; 
END IF;
END$$

CREATE  PROCEDURE `proc_rolemanage_s` (IN `act_mode` VARCHAR(50), IN `Param1` VARCHAR(50), IN `Param2` VARCHAR(50), IN `Param3` VARCHAR(50), IN `Param4` VARCHAR(50), IN `Param5` VARCHAR(50), IN `Param6` VARCHAR(50), IN `Param7` VARCHAR(50), IN `Param8` VARCHAR(50), IN `Param9` VARCHAR(50))  BEGIN

if(act_mode='update_timeslot_status')then
update tbl_employee as e set e.emp_status=Param2 where e.emp_tbl_id=Param1;
end if;

if(act_mode='addrole') then
INSERT INTO `tblrole`(`role_name`) VALUES (Param1);
end if;

if(act_mode='addemp') then
INSERT INTO `tbl_employee`(`emp_name`,`emp_email`, `emp_password`, `emp_contact`, `emp_branch_id`) VALUES (Param1,Param3,Param4,Param5,Param6);
select last_insert_id() as dd;
end if;

if(act_mode='addemp_role') then
set @aa='';
set @aa = (select t.parentid from tbladminmenu as t where t.id=Param1);
INSERT INTO tblassignrole  (role_id,emp_id,main_menu,sub_menu) values ('100',Param2,@aa,Param1);
select * from tblassignrole as t where t.assign_id=last_insert_id();

end if;

if(act_mode='viewemp') then
select *,(select group_concat((select group_concat(menu.menuname) from tbladminmenu as menu where menu.id=dd.sub_menu)) from tblassignrole as dd where dd.emp_id=t.emp_tbl_id limit 0,1) as submenuu from tbl_employee as t where t.emp_status in(0,1);
end if;

if(act_mode='viewemp_update') then
select * from tbl_employee as t where t.emp_tbl_id=Param1;
end if;


if(act_mode='delemp') then
update tbl_employee as t set t.emp_status=2 where t.emp_tbl_id=Param1;
end if;

if(act_mode='viewrole') then
select * from tblrole as t where t.role_status=1;
end if;

if(act_mode='viewmenu') then
select * from tbladminmenu as t where t.parentid=0 and t.r_status='Active';
end if;

if(act_mode='deleterole') then
update tblrole as t set t.role_status=2,t.role_modifiedon=now() where t.role_id=Param1;
end if;



if(act_mode='getsubmenu') then
select * from tbladminmenu as t where find_in_set(parentid,Param1) and t.r_status='Active';
end if;


if(act_mode='getmainmenu') then
select parentid as pid from tbladminmenu as t where t.id=Param1;
end if;

if(act_mode='insertadminrole') then
set @a = (select count(*) from tblassignrole where role_id=Param1 and emp_id=Param2 and main_menu=Param3 and sub_menu=Param4);
if(@a = 0) then
INSERT INTO `tblassignrole`(`role_id`, `emp_id`, `main_menu`, `sub_menu`) VALUES (Param1,Param2,Param3,Param4);
end if;
end if;


if(act_mode='addasignrole_DELETE') then
DELETE from tblassignrole where role_id=Param1 and emp_id=Param2;
end if;

if(act_mode='get_menu_submenu') then
select (group_concat(distinct(t.main_menu))) as mainmenu , (group_concat(distinct(t.sub_menu))) as submenu   from tblassignrole as t  where t.role_id=Param1 and t.emp_id=Param2;
end if;


if(act_mode='upd_emp') then
update tbl_employee as t set t.emp_branch_id=Param6,t.emp_name=Param1,t.emp_email=Param3,t.emp_password=Param4,t.emp_contact=Param5 where t.emp_tbl_id=Param7;
end if;

END$$

CREATE  PROCEDURE `proc_select_banner_v` (IN `act_mode` VARCHAR(50), IN `branchid` VARCHAR(50))  BEGIN
if(act_mode='selectbannerimages') then
select * from tbl_bannerimage  where bannerimage_branch=branchid and bannerimage_status=1;
end if;

if(act_mode='selectsms') then
select * from tbl_sms_gatway  where sms_branchid=branchid and sms_status=1 order by sms_id desc ;
end if;

if(act_mode='selecttearms') then
select * from tbl_tearm  where tearm_branchid=branchid and term_status=1 order by term_id asc ;
end if;

END$$

CREATE  PROCEDURE `proc_select_branch_v` (IN `act_mode` VARCHAR(50), IN `weburl` VARCHAR(50))  BEGIN
if(act_mode='selectbranch') then
select  a.branch_cronjobrunvalue,a.branch_minrestriction,a.branch_cronejob,a.branch_hrsrestriction,a.branch_url,a.branch_add,a.branch_location,a.branch_background,a.branch_logo,a.branch_internet_handling_charge,a.branch_id,b.cityid,c.stateid,d.countryid from tbl_branch_master a join tbl_location b on a.branch_location=b.locationid join tbl_city c on c.cityid=b.cityid join tbl_state d on d.stateid=c.stateid  where a.branch_status=1 and  a.branch_url=weburl;
end if;
END$$

CREATE  PROCEDURE `proc_select_timeslot_v` (IN `act_mode` VARCHAR(50), IN `branchid` INT, IN `destinationType` INT, IN `timeslot_date` VARCHAR(40))  BEGIN

if(act_mode='selecttimeslot') then
select *  from tbl_timeslot_branch   where timeslot_branchid=branchid and timeslot_status=1;
end if;
if(act_mode='selectsestimeslot') then
select *  from tbl_timeslot_branch   where timeslot_branchid=branchid and timeslot_status=1 and timeslot_from=destinationType;
end if;

if(act_mode='getTimeSlotByDate') then
	
select *,(CASE dailyinventory_from when 12 then 12 else dailyinventory_from%12 end) as time_value_from
,
(CASE  when (dailyinventory_from <12) then 'AM' else 'PM' end ) as AM_PM_from

, (CASE dailyinventory_to when 12 then 12 else dailyinventory_to%12 end) as time_value_to
,
(CASE  when (dailyinventory_to <12) then 'AM' else 'PM' end ) as AM_PM_to  from tbl_daily_inventory where dailyinventory_branchid = branchid and dailyinventory_status=1 and dailyinventory_date = timeslot_date;	

end if;

END$$

CREATE  PROCEDURE `proc_siteconfig` (IN `act_mode` VARCHAR(50), IN `row_id` INT)  begin

if(act_mode='bizzgainconfig')then
select * from tbl_bizzgainconfig as tbc where tbc.name='Email'; 
end if;



if(act_mode='shopotoxcofigph')then
select * from tbl_bizzgainconfig as tbc where tbc.name='Contact Us'; 
end if;
if(act_mode='servicetax')then
select * from tbl_bizzgainconfig as tbc where tbc.name='Service Tax'; 
end if;
if(act_mode='accconfig')then
select * from tbl_bizzgainconfig as tbc where tbc.name='account change'; 
end if;
if(act_mode='Offersconfig')then
select * from tbl_bizzgainconfig as tbc where tbc.name='Offers'; 
end if;
if(act_mode='complainconfig')then
select * from tbl_bizzgainconfig as tbc where tbc.name='complain'; 
end if;
if(act_mode='adminconfig')then
select * from tbl_bizzgainconfig as tbc where tbc.name='admin mail'; 
end if;
if(act_mode='vendorconfig')then
select * from tbl_bizzgainconfig as tbc where tbs.name='vendor mail'; 
end if;
if(act_mode='viewsiteconfig')then
select * from tbl_bizzgainconfig limit 8; 
end if;

if(act_mode='viewupdateconfig')then
select * from tbl_bizzgainconfig where tbl_bizzgainconfig.id=row_id; 
end if;

if(act_mode='viewseotags')then
select * from tbl_seoskiindia where tbl_seoskiindia.seo_id=row_id; 
end if;

end$$

CREATE  PROCEDURE `proc_smsgatway` (IN `act_mode` VARCHAR(50), IN `Param1` VARCHAR(50), IN `Param2` VARCHAR(50), IN `Param3` VARCHAR(50), IN `Param4` VARCHAR(50), IN `Param5` VARCHAR(50), IN `Param6` VARCHAR(50), IN `Param7` VARCHAR(50), IN `Param8` VARCHAR(50), IN `Param9` VARCHAR(50), IN `Param10` VARCHAR(50))  BEGIN



if(act_mode='smsview') then
select * from  tbl_sms_gatway where sms_status in(1,0);
end if;


if(act_mode='smsinsert') then
INSERT INTO tbl_sms_gatway ( `sms_branchid`, `sms_username`, `sms_password`, `sms_created`,  `sms_status`,`sms_feedid`) VALUES ( Param1,Param2,Param3, NOW(), '1',Param4);
select last_insert_id();
end if;

if(act_mode='smspaymentstatus') then
update  tbl_sms_gatway as p set  p.sms_status=Param2 where p.sms_id=Param1;
end if;

if(act_mode='paymentup') then
select * from  tbl_sms_gatway as p  where p.sms_id=Param1;
end if;

if(act_mode='paymentupdate') then
update tbl_sms_gatway as p set p.sms_branchid=Param1,p.sms_feedid=Param2,p.sms_username=Param3,p.sms_password=Param4  where p.sms_id=Param10;
end if;
if(act_mode='paymentdelete') then
update tbl_sms_gatway as p set p.sms_status=2 where p.sms_id=Param1;
end if;
END$$

CREATE  PROCEDURE `proc_tearmcond` (IN `act_mode` VARCHAR(50), IN `Param1` VARCHAR(50), IN `Param2` TEXT, IN `Param3` VARCHAR(50), IN `Param4` VARCHAR(50), IN `Param5` VARCHAR(50), IN `Param6` VARCHAR(50), IN `Param7` VARCHAR(50), IN `Param8` VARCHAR(50), IN `Param9` VARCHAR(50), IN `Param10` VARCHAR(50))  BEGIN



if(act_mode='tcview') then

select * from  tbl_tearm where term_status in(1,0);

end if;


if(act_mode='tcinsert') then

INSERT INTO tbl_tearm (`tearm_branchid`, `term_name`, `term_created`, `term_status`) VALUES ( Param1,Param2, NOW(), '1');
select last_insert_id();
end if;

if(act_mode='termpaymentstatus') then
update  tbl_tearm as p set  p.term_status=Param2 where p.term_id=Param1;

end if;

if(act_mode='tearmup') then
select * from  tbl_tearm as p  where p.term_id=Param1;
end if;

if(act_mode='tearmupdate') then
update tbl_tearm as p set p.tearm_branchid=Param1,p.term_name=Param2  where p.term_id=Param10;
end if;
if(act_mode='paymentdelete') then
update tbl_tearm as p set p.term_status=2 where p.term_id=Param1;
end if;
END$$

CREATE  PROCEDURE `proc_tempaddoneadd_v` (IN `act_mode` VARCHAR(50), IN `categories1` VARCHAR(50), IN `addonid` VARCHAR(50), IN `addonprice` VARCHAR(50), IN `addonqty` VARCHAR(50), IN `addonimage` VARCHAR(50), IN `addonname` VARCHAR(50), IN `uniqueid` VARCHAR(50))  BEGIN


if(act_mode='insertaddone') then
INSERT INTO `tempaddoncarttable`(`uniqid`,`addonid`,`addonprice`,`addonqty`,`addonimage`,`addonname`) VALUES (uniqueid,addonid,addonprice,addonqty,addonimage,addonname);
select last_insert_id() as ls;
end if;
if(act_mode='aaddonedisplay') then

select * from tempaddoncarttable where uniqid=uniqueid;
end if;
if(act_mode='selectaddone') then

select * from tempaddoncarttable as t where t.uniqid=uniqueid and t.addonid=addonid;
end if;


if(act_mode='deleteaddone') then

delete  from tempaddoncarttable where tempaddon=addonid;
end if;

if(act_mode='updateaddone111') then
update tempaddoncarttable as t set t.addonprice = addonprice , t.addonqty=addonqty where t.addonid=addonid and t.uniqid=uniqueid;
end if;

if(act_mode='updateaddonedata') then
update tempaddoncarttable as t set t.addonprice=(t.addonprice-(t.addonprice/t.addonqty))   where t.addonid=addonid and t.uniqid=uniqueid;
end if;

if(act_mode='deleteaddones') then

delete  from tempaddoncarttable where addonid=addonid and uniqid=uniqueid;
end if;

if(act_mode='addonselectbyuniqid') then


SELECT GROUP_CONCAT(t.addonid) as addonid ,GROUP_CONCAT(t.addonimage) as addonimage ,GROUP_CONCAT(t.addonname) as addonname ,GROUP_CONCAT(t.addonqty) as addonqty  ,GROUP_CONCAT(t.addonprice) as addonprice FROM tempaddoncarttable as t WHERE t.uniqid=uniqueid;
end if;


if(act_mode='timeslotviewupdate') then
select * from tempaddoncarttable as t where t.addonid=addonid and t.uniqid=uniqueid;
end if;
if(act_mode='updatesesiontimeslot') then
set @a = addonid;
delete  from tempaddoncarttable where addonqty= 1 ;
end if;





if(act_mode='deleteaddonesd') then
delete from tempaddoncarttable where addonqty= @1 ;
end if;


if(act_mode='selectpromoval') then

select * from tempaddoncarttable where  uniqid=uniqueid;
end if;


END$$

CREATE  PROCEDURE `proc_timeslotspackages` (IN `act_mode` VARCHAR(200), IN `Param1` VARCHAR(200), IN `Param2` VARCHAR(200), IN `Param3` VARCHAR(200), IN `Param4` VARCHAR(200), IN `Param5` VARCHAR(200), IN `Param6` VARCHAR(200), IN `Param7` VARCHAR(200), IN `Param8` VARCHAR(200), IN `Param9` VARCHAR(200), IN `Param10` VARCHAR(200), IN `Param11` VARCHAR(200), IN `Param12` VARCHAR(200), IN `Param13` VARCHAR(200), IN `Param14` VARCHAR(200), IN `Param15` VARCHAR(200))  BEGIN


if(act_mode='addTimeslotPackage') then
INSERT INTO `tbl_daily_inventory`(`dailyinventory_branchid`,`dailyinventory_from`,`dailyinventory_to`,`dailyinventory_seats`, `dailyinventory_status`, `dailyinventory_createdon`,  `dailyinventory_minfrom`, `dailyinventory_minto`,  `dailyinventory_date`) VALUES (Param8,Param2,Param4,Param7,'1',now(),Param3,Param5,Param6);
	set @last_id=last_insert_id();
INSERT INTO `tbl_daily_inventorypackages`(`dailyinventorypackages_dailyinventoryid`,
                  `dailyinventorypackages_packagesid`, `status`) VALUES (@last_id,Param1,'1');
											 
end if;

if(act_mode='addTimeslotPackageNew') then
INSERT INTO `tbl_daily_inventory`(`dailyinventory_branchid`,`dailyinventory_from`,`dailyinventory_to`,`dailyinventory_seats`,
                                  `dailyinventory_status`, `dailyinventory_createdon`,  `dailyinventory_minfrom`, `dailyinventory_minto`, 
											 `dailyinventory_date`,dailyinventory_addons)
											  VALUES (Param8,Param2,Param4,Param7,'1',now(),Param3,Param5,Param6,Param10);
											set @last_id=last_insert_id();
											select  @last_id as last_id;
end if;


if(act_mode='ChecksTimeslotPackageNew') then
SELECT count(*) as row_no FROM `tbl_daily_inventory` WHERE `dailyinventory_branchid`=Param8 AND
`dailyinventory_from`=Param2 and `dailyinventory_to`=Param4 and `dailyinventory_minfrom`=Param3
and `dailyinventory_minto`=Param5 and `dailyinventory_date`=Param6 and `dailyinventory_status`!='2';
end if;


if(act_mode='daily_inventorypackagesPackage') then
INSERT INTO `tbl_daily_inventorypackages`(`dailyinventorypackages_dailyinventoryid`,
                  `dailyinventorypackages_packagesid`, `status`) VALUES (Param1,Param2,'1');
end if;


if(act_mode='ViewTimeslotPackage') then

SELECT pm.package_id,pm.package_name,di.dailyinventory_id,dip.dailyinventorypackages_dailyinventoryid,
dip.dailyinventorypackages_id,di.dailyinventory_branchid,di.dailyinventory_from,di.dailyinventory_to,di.dailyinventory_to
,di.dailyinventory_seats,di.dailyinventory_minfrom,di.dailyinventory_minto,di.dailyinventory_date,bm.branch_id,bm.branch_name,dip.`status`,dip.dailyinventorypackages_packagesid
 FROM `tbl_package_master` as pm INNER join tbl_daily_inventorypackages as dip on
 dip.dailyinventorypackages_packagesid=pm.package_id INNER join tbl_daily_inventory as di ON
di.dailyinventory_id =dip.dailyinventorypackages_dailyinventoryid inner join tbl_branch_master as bm on  bm.branch_id=di.dailyinventory_branchid
where dailyinventory_date=CURDATE();										 
end if;

if(act_mode='searchTimeslotPackage') then

SELECT pm.package_id,pm.package_name,di.dailyinventory_id,dip.dailyinventorypackages_dailyinventoryid,
dip.dailyinventorypackages_id,di.dailyinventory_branchid,di.dailyinventory_from,di.dailyinventory_to,di.dailyinventory_to
,di.dailyinventory_seats,di.dailyinventory_minfrom,di.dailyinventory_minto,di.dailyinventory_date,bm.branch_id,bm.branch_name
 FROM `tbl_package_master` as pm INNER join tbl_daily_inventorypackages as dip on
 dip.dailyinventorypackages_packagesid=pm.package_id INNER join tbl_daily_inventory as di ON
di.dailyinventory_id =dip.dailyinventorypackages_dailyinventoryid inner join tbl_branch_master as bm on  bm.branch_id=di.dailyinventory_branchid
where pm.package_status='1'
and di.dailyinventory_status='1' and dip.`status`='1' and di.dailyinventory_date=Param2;
											 
end if;


if(act_mode='EditTimeslotPackage') then
SELECT pm.package_id,pm.package_name,di.dailyinventory_id,dip.dailyinventorypackages_dailyinventoryid,
dip.dailyinventorypackages_packagesid,di.dailyinventory_branchid,di.dailyinventory_from,di.dailyinventory_to
,di.dailyinventory_seats,di.dailyinventory_minfrom,di.dailyinventory_minto,di.dailyinventory_date,bm.branch_id,bm.branch_name,di.dailyinventory_addons
 FROM `tbl_package_master` as pm INNER join tbl_daily_inventorypackages as dip on
 dip.dailyinventorypackages_packagesid=pm.package_id INNER join tbl_daily_inventory as di ON
di.dailyinventory_id =dip.dailyinventorypackages_dailyinventoryid inner join tbl_branch_master as bm on  bm.branch_id=di.dailyinventory_branchid
where pm.package_status='1'
and di.dailyinventory_status='1' and dip.`status`='1' and di.dailyinventory_id=Param1;
end if;

if(act_mode='UpdateTimeslotPackage') then
UPDATE `tbl_daily_inventorypackages` SET `dailyinventorypackages_packagesid`=Param1 where `dailyinventorypackages_id`=Param9;
UPDATE `tbl_daily_inventory` SET `dailyinventory_seats`=Param7,dailyinventory_addons=Param11 where `dailyinventory_id`=Param10;

end if;

if(act_mode='DeleteTimeslotPackage') then
set @checks=(SELECT count(*) as row_no FROM `tbl_daily_inventorypackages` WHERE 
`dailyinventorypackages_dailyinventoryid`=Param10 and `status`!='2');

if(@checks>=2) then

UPDATE `tbl_daily_inventorypackages` SET `status`='2' where `dailyinventorypackages_id`=Param9
and `dailyinventorypackages_dailyinventoryid`=Param10;
else
UPDATE `tbl_daily_inventory` SET `dailyinventory_status`='2' where `dailyinventory_id`=Param10;

end if;

end if;

if(act_mode='ViewCountrys') then
SELECT * FROM `tbl_country` WHERE`counstatus`='1';
end if;

if(act_mode='AddonViewTimeslotPackage') then
SELECT `addon_id`, `addon_name`, `addon_price` FROM `tbl_addon` WHERE addon_status='1';
end if;

if(act_mode='google_login') then
INSERT INTO `tbl_user`(`user_firstname`, `user_lastname`, `user_emailid`, `user_usertype`,
      `oauth_id`, `oauth_link`, `oauth_picture_link`, `oauth_type`,user_status) 
  VALUES (Param2,'',Param1,Param7,Param3,Param4,Param5,Param6,'1');
  select user_id,oauth_id,oauth_type from tbl_user where `oauth_id`=Param3;
end if;

if(act_mode='twitter_email_save')then
update tbl_user set user_emailid=Param2 where user_id=Param1;
end if;

if(act_mode='ChecksGoogleUser') then
SELECT COUNT(oauth_id) as usercount FROM  tbl_user WHERE oauth_id=Param10;
end if;

if(act_mode='google_login_new')then
set @chec=(SELECT COUNT(oauth_id) as usercount FROM  tbl_user WHERE oauth_id=Param10);
if(@chec=0) then
INSERT INTO `tbl_user`(`user_firstname`, `user_lastname`, `user_emailid`, `user_usertype`,
       `oauth_id`, `oauth_link`, `oauth_picture_link`, `oauth_type`) 
  VALUES (Param2,'',Param1,Param7,Param3,Param4,Param5,Param6);	
else
UPDATE `tbl_user` SET `oauth_link`=Param4,oauth_picture_link=Param5 where `oauth_id`=Param10 ;

end if;
end if;


if(act_mode='google_update') then
UPDATE `tbl_user` SET `oauth_link`=Param4,oauth_picture_link=Param5,user_emailid=Param1 where `oauth_id`=Param3;
select user_id,oauth_id,oauth_type,tbl_user.* from tbl_user where `oauth_id`=Param3;
end if;

if(act_mode='AgentEmail') then
SELECT `agent_id`,`agent_email` FROM `tbl_agent_reg`  WHERE `agent_status`='1';
end if;


END$$

CREATE  PROCEDURE `proc_timeslot_s` (IN `act_mode` VARCHAR(50), IN `Param1` VARCHAR(50), IN `Param2` VARCHAR(50), IN `Param3` VARCHAR(50), IN `Param4` VARCHAR(50), IN `Param5` VARCHAR(50), IN `Param6` VARCHAR(50), IN `Param7` VARCHAR(50), IN `Param8` VARCHAR(50), IN `Param9` VARCHAR(50))  BEGIN

if(act_mode = 's_addtimeslot') then
INSERT INTO `tbl_timeslot_branch`(`timeslot_branchid`, `timeslot_from`, `timeslot_to`,
 `timeslot_status`,timeslot_seats,timeslot_minfrom,timeslot_minto,timeslot_packageid,timeslot_addonsid)
 VALUES (Param1,Param2,Param3,'1',Param4,Param5,Param6,Param7,Param8);
end if;

if(act_mode = 's_checktimeslot') then
select count(*) as cou  from tbl_timeslot_branch as t where t.timeslot_branchid=Param1 and t.timeslot_from=Param2 and t.timeslot_to=Param3 and t.timeslot_status=1;
end if;

if(act_mode='delete_commission') then
delete from tbl_commission where 1;
end if;

if(act_mode='edit_commission') then
INSERT INTO `tbl_commission`(`start_ticket_id`, `end_ticket_id`, `commission`) VALUES (Param1,Param2,Param3);
end if;

if(act_mode = 's_viewtimeslot') then
select *,(select group_concat(timeslot_id) from tbl_timeslot_branch as tt where tt.timeslot_branchid=t.branch_id and tt.timeslot_status !=2) as timeslot_id,(select group_concat(timeslot_to) from tbl_timeslot_branch as tt where tt.timeslot_branchid=t.branch_id and tt.timeslot_status !=2) as timeslotto,(select group_concat(timeslot_from) from tbl_timeslot_branch as tt where tt.timeslot_branchid=t.branch_id and tt.timeslot_status !=2) as timeslotfrom,(select group_concat(timeslot_seats) from tbl_timeslot_branch as tt where tt.timeslot_branchid=t.branch_id and tt.timeslot_status !=2) as timeslotseats,(select group_concat(timeslot_minfrom) from tbl_timeslot_branch as tt where tt.timeslot_branchid=t.branch_id and tt.timeslot_status !=2) as timeslot_minfrom,(select group_concat(timeslot_minto) from tbl_timeslot_branch as tt where tt.timeslot_branchid=t.branch_id and tt.timeslot_status !=2) as timeslot_minto from tbl_branch_master as t where t.branch_status=1;
end if;

if(act_mode = 'deletesingletime') then
update tbl_timeslot_branch as t set t.timeslot_status=2 where t.timeslot_branchid=Param1 and t.timeslot_from=Param2 and t.timeslot_to=Param3;
end if;

if(act_mode = 's_viewtimeslotval') then
select * from tbl_timeslot_branch where timeslot_id=Param1;
end if;

if(act_mode = 's_edittimeslot') then
update tbl_timeslot_branch set timeslot_branchid=Param1,timeslot_from=Param2,
 timeslot_to=Param3,timeslot_seats=Param4,timeslot_minfrom=Param5,timeslot_minto=Param6,timeslot_packageid=Param7, 
 timeslot_addonsid=Param8
 
 where timeslot_id=Param9;
end if;


END$$

CREATE  PROCEDURE `proc_viewtimeslot_v` (IN `act_mode` VARCHAR(50), IN `Param1` VARCHAR(50), IN `Param2` VARCHAR(50), IN `Param3` VARCHAR(50), IN `Param4` VARCHAR(50), IN `Param5` VARCHAR(50), IN `Param6` VARCHAR(50), IN `Param7` VARCHAR(50), IN `Param8` VARCHAR(50), IN `Param9` VARCHAR(50))  BEGIN
if(act_mode = 's_viewtimeslot') then
select *,(SELECT sum(t1.packpkg) from `tbl_orderpackage` as t1 WHERE t1.departuretime = t.timeslot_from and t1.departuredate=Param2 and t1.order_status in ('Shipped','Success') ) as coun from `tbl_timeslot_branch` as t where t.timeslot_branchid=Param1 and t.timeslot_status=1;
end if;
if(act_mode = 's_viewtimeslotdata') then
SELECT * from `tbl_orderpackage` WHERE departuretime = Param3;
end if;

if(act_mode = 's_viewtimeslotf') then
select *,(SELECT sum(t1.packpkg) from `tbl_orderpackage` as t1 WHERE t1.departuretime = t.timeslot_from and t1.departuredate=Param2 and t1.departuretime=Param3) as coun from `tbl_timeslot_branch` as t where t.timeslot_branchid=Param1 and t.timeslot_status=1 and t.timeslot_from=Param3;
end if;


if(act_mode = 's_viewtimeslotpackage') then
select *,(SELECT sum(t1.packpkg) from `tbl_orderpackage` as t1 WHERE t1.departuretime = t.timeslot_from and t1.departuredate=Param2  ) as coun ,(SELECT sum(t1.packpkg) from `tbl_orderpackage` as t1 WHERE t1.departuretime = t.timeslot_from and t1.departuredate=Param2 and t1.order_status='Aborted'  ) as Aborted from `tbl_timeslot_branch` as t where t.timeslot_branchid=Param1 and t.timeslot_status=1;
end if;

if(act_mode = 's_viewtimeslotaddones') then
select *,(SELECT count(t1.addonqty) from `tbl_orderaddone` as t1 join `tbl_orderpackage` as t2 on t2.pacorderid=t1.lastid    WHERE t1.addonid = t.addon_id and t2.departuredate=Param2  ) as coun,(SELECT count(t1.addonqty) from `tbl_orderaddone` as t1 join `tbl_orderpackage` as t2 on t2.pacorderid=t1.lastid    WHERE t1.addonid = t.addon_id and t2.departuredate=Param2  and t2.order_status='Aborted' ) as Aborted from `tbl_addon` as t where t.addon_branchid=Param1 and t.addon_status=1;
end if;


if(act_mode = 's_viewtimeslottimepackage') then
select *,(SELECT sum(t1.packpkg) from `tbl_orderpackage` as t1 WHERE t1.departuretime = t.dailyinventory_from and t1.departuredate=Param2 and t1.paymentstatus=1  ) as coun ,(SELECT sum(t1.packpkg) from `tbl_orderpackage` as t1 WHERE t1.departuretime = t.dailyinventory_from and t1.departuredate=Param2 and t1.order_status not in ('Shipped','Success')  ) as Aborted from `tbl_daily_inventory` as t where  t.dailyinventory_status=1 and t.dailyinventory_date=Param3;
end if;

if(act_mode = 's_viewtimeslottimeaddones') then
select *,(SELECT count(t1.addonqty) from `tbl_orderaddone` as t1 join `tbl_orderpackage` as t2 on t2.pacorderid=t1.lastid    WHERE t1.addonid = t.addon_id and t2.departuredate=Param2  ) as coun,(SELECT count(t1.addonqty) from `tbl_orderaddone` as t1 join `tbl_orderpackage` as t2 on t2.pacorderid=t1.lastid    WHERE t1.addonid = t.addon_id and t2.departuredate=Param2  and t2.order_status='Aborted' ) as Aborted from `tbl_addon` as t where t.addon_branchid=Param1 and t.addon_status=1;
end if;

END$$

--
-- Functions
--
CREATE  FUNCTION `fn_credit_debit` (`fn_user_id` INT) RETURNS TEXT CHARSET latin1 BEGIN
#set @credit_debit=(SELECT #     SUM(COALESCE(CASE WHEN `credit_debit_pety` = 'C' THEN `credit_debit_amount` END,0))- SUM(COALESCE(CASE WHEN `credit_debit_pety` = 'D' THEN `credit_debit_amount` END,0)) as amount    
#  FROM tbl_agent_wallet  WHERE `user_id`=fn_user_id# GROUP  #    BY `user_id`);#return @credit_debit;

set @credit_debit=(SELECT total_amount
   FROM tbl_agent_wallet  WHERE `user_id`=fn_user_id and `Status`='1' order by wid desc limit 1);
return @credit_debit;




END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(250) NOT NULL,
  `last_activity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mail_tbl`
--

CREATE TABLE `mail_tbl` (
  `mail_id` int(10) NOT NULL,
  `sender_id` int(10) NOT NULL,
  `receiver_id` int(10) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `compose` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `read_status` varchar(200) NOT NULL,
  `status` varchar(200) NOT NULL DEFAULT 'A'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbladminmenu`
--

CREATE TABLE `tbladminmenu` (
  `id` int(10) NOT NULL,
  `menuname` varchar(255) DEFAULT NULL,
  `parentid` int(10) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `position` varchar(500) DEFAULT NULL,
  `amlevel` int(11) DEFAULT NULL,
  `r_status` enum('Active','Inactive','Delete') DEFAULT 'Active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbladminmenu`
--

INSERT INTO `tbladminmenu` (`id`, `menuname`, `parentid`, `url`, `position`, `amlevel`, `r_status`) VALUES
(13, 'Country', 0, 'admin/geographic/viewcountry', NULL, NULL, 'Inactive'),
(14, 'State', 0, 'admin/geographic/statelist', NULL, NULL, 'Inactive'),
(15, 'City', 0, 'admin/geographic/viewcity', NULL, NULL, 'Inactive'),
(16, 'Location', 0, 'admin/geographic/locationlist', NULL, NULL, 'Inactive'),
(17, 'Promo Code', 0, '', '<i class=\"fa fa-map-marker\"></i>', 4, 'Active'),
(18, 'Add Promo Code', 17, 'admin/promocodes/s_promo', NULL, NULL, 'Active'),
(19, 'View Promo Code', 17, 'admin/promocodes/s_viewpromo', NULL, NULL, 'Active'),
(20, 'Activity', 0, NULL, '<i class=\"fa fa-map-marker\"></i>', 0, 'Inactive'),
(21, 'Add Activity', 20, 'admin/activity/addactivity', NULL, NULL, 'Active'),
(22, 'Branch', 0, NULL, '<i class=\"fa fa-share-alt\"></i>', 0, 'Inactive'),
(23, 'Add Branch', 22, 'admin/branchs/addbranch', NULL, NULL, 'Active'),
(24, 'Company', 0, NULL, '<i class=\"fa fa-building-o\"></i>', 0, 'Inactive'),
(25, 'Add Company', 24, 'admin/companys/s_addCompany', NULL, NULL, 'Active'),
(26, 'Package', 0, NULL, '<i class=\"fa fa-map-marker\"></i>', 0, 'Inactive'),
(27, 'Add Package', 26, 'admin/packages/addPackage', NULL, NULL, 'Active'),
(28, 'Addon', 0, NULL, '<i class=\"fa fa-map-marker\"></i>', 0, 'Inactive'),
(29, 'Add Addon', 28, 'admin/addon/addAddon', NULL, NULL, 'Active'),
(30, 'Time Slot', 0, NULL, '<i class=\"fa fa-clock-o\"></i>', 0, 'Inactive'),
(31, 'Add Timeslot', 30, 'admin/timeslots/addTimeslot', NULL, NULL, 'Active'),
(32, 'Manage Geographic', 61, 'admin/geographic/geomaster', '4', 4, 'Active'),
(34, 'Add Role ', 33, 'admin/managerole/addrole', NULL, NULL, 'Inactive'),
(35, 'Add Employee', 61, 'admin/managerole/addemployee', '2', 2, 'Active'),
(37, 'Role Assign to Employee', 33, 'admin/managerole/assignroles', NULL, NULL, 'Inactive'),
(38, 'Order Management', 0, NULL, '<i class=\"fa fa-cart-arrow-down\"></i>', 2, 'Active'),
(39, 'All Booking List', 38, 'admin/ordercontrol/order', NULL, NULL, 'Active'),
(40, 'Group Booking <br> Management', 0, NULL, '<i class=\"fa fa-group\"></i>', 0, 'Inactive'),
(41, 'Booking Type Master', 40, 'admin/groupbooking/bookingtype', NULL, NULL, 'Active'),
(42, 'Media Gallery', 0, NULL, '<i class=\"fa fa-file-image-o\"></i>', 0, 'Inactive'),
(43, 'Image', 42, 'admin/galary/image', NULL, NULL, 'Active'),
(44, 'Video', 42, 'admin/galary/video', '', 9, 'Active'),
(45, 'Habitat', 42, 'admin/galary/habitat', '', 9, 'Active'),
(46, 'Press Release', 42, 'admin/galary/pressrelease', '', 9, 'Active'),
(47, 'Success Payment', 38, 'admin/ordercontrol/pendingorder', NULL, NULL, 'Active'),
(48, 'Printed Ticket', 38, 'admin/ordercontrol/sucessgorder', NULL, NULL, 'Active'),
(49, 'Registered User', 0, 'admin/user/reguser', '<i class=\"fa fa-user\"></i>', 5, 'Active'),
(51, 'Group Order Management', 0, NULL, '<i class=\"fa fa-group\"></i>', 0, 'Inactive'),
(52, 'Group Success Bookings', 51, 'admin/ordercontrolgp/sucessgordergp', NULL, NULL, 'Inactive'),
(53, 'Group Pending Bookings', 51, 'admin/ordercontrolgp/pendingordergp', NULL, NULL, 'Inactive'),
(54, 'All Group Booking List ', 51, 'admin/ordercontrolgp/ordergp', NULL, NULL, 'Active'),
(56, 'Mail Configuration', 61, 'admin/managemail/viewmail', '3', 3, 'Active'),
(57, 'Inventory Details', 0, NULL, '<i class=\"fa fa-inr\" aria-hidden=\"true\"></i>', 3, 'Active'),
(58, 'Agent Regestration', 70, 'admin/b2b/agent', NULL, NULL, 'Active'),
(59, 'Date wise Inventory', 57, 'admin/inventory/datewiseinventory', NULL, NULL, 'Active'),
(60, 'Inventory Calender', 57, 'admin/inventory/calenderview', NULL, NULL, 'Active'),
(61, 'Master Settings', 0, NULL, '<i class=\"fa fa-group\"></i>', 7, 'Active'),
(62, 'Manage  Master', 61, 'admin/allmanage/managemaster', '1', 1, 'Active'),
(63, 'Print Pending', 38, 'admin/ordercontrol/sucesspendingprinting', NULL, NULL, 'Active'),
(64, 'Dashboard', 0, 'admin/dashboard', '<i class=\"fa fa-globe\"></i>', 1, 'Active'),
(65, 'Commission', 70, 'admin/commission/commissionmaster', NULL, NULL, 'Active'),
(69, 'Agent Management', 70, 'admin/agent/adminmanagement', '', 0, 'Active'),
(70, 'B2B Settings', 0, '', '<i class=\"fa fa-globe\"></i>', 8, 'Active'),
(72, 'Agent Graph', 70, 'admin/graphmanagement/agentgrapg', NULL, NULL, 'Active'),
(73, 'Agent Management Commisiion', 70, 'admin/commission/agentlist', NULL, NULL, 'Active'),
(74, 'Commission Payment Cycle', 70, 'admin/commission/paymenttime', NULL, NULL, 'Active'),
(75, 'Commission Payment History', 70, 'admin/commission/paymenthistory', NULL, NULL, 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `tblassignmenu`
--

CREATE TABLE `tblassignmenu` (
  `assign_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL DEFAULT '0',
  `emp_id` int(11) NOT NULL DEFAULT '0',
  `main_menu` varchar(250) NOT NULL,
  `sub_menu` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblassignmenu`
--

INSERT INTO `tblassignmenu` (`assign_id`, `role_id`, `emp_id`, `main_menu`, `sub_menu`) VALUES
(1, 5, 2, '17', '19'),
(2, 5, 2, '17', '19'),
(3, 9, 2, '1', '32'),
(4, 9, 2, '17', '19'),
(5, 5, 2, '22', '23'),
(6, 9, 2, '20', '21'),
(7, 11, 6, '1', '32'),
(8, 11, 6, '17', '18'),
(9, 11, 6, '17', '19'),
(10, 11, 6, '20', '21'),
(11, 11, 6, '22', '23');

-- --------------------------------------------------------

--
-- Table structure for table `tblassignrole`
--

CREATE TABLE `tblassignrole` (
  `assign_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL DEFAULT '0',
  `emp_id` int(11) NOT NULL DEFAULT '0',
  `main_menu` int(11) NOT NULL,
  `sub_menu` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblassignrole`
--

INSERT INTO `tblassignrole` (`assign_id`, `role_id`, `emp_id`, `main_menu`, `sub_menu`) VALUES
(82, 100, 2, 38, '39'),
(83, 100, 2, 38, '47'),
(84, 100, 2, 38, '48'),
(85, 100, 2, 57, '59'),
(86, 100, 2, 57, '60'),
(87, 100, 2, 38, '63'),
(94, 100, 3, 38, '39'),
(95, 100, 3, 38, '47'),
(96, 100, 3, 38, '48'),
(97, 100, 3, 57, '59'),
(98, 100, 3, 57, '60'),
(99, 100, 3, 38, '63'),
(100, 100, 4, 38, '39'),
(101, 100, 4, 38, '47'),
(102, 100, 4, 38, '48'),
(103, 100, 4, 57, '59'),
(104, 100, 4, 57, '60'),
(105, 100, 4, 38, '63'),
(106, 100, 5, 38, '39'),
(107, 100, 5, 38, '47'),
(108, 100, 5, 38, '48'),
(109, 100, 5, 57, '59'),
(110, 100, 5, 57, '60'),
(111, 100, 5, 38, '63'),
(112, 100, 6, 38, '39'),
(113, 100, 6, 38, '47'),
(114, 100, 6, 38, '48'),
(115, 100, 6, 57, '59'),
(116, 100, 6, 57, '60'),
(117, 100, 6, 38, '63'),
(118, 100, 1, 17, '18'),
(119, 100, 1, 17, '19'),
(120, 100, 1, 33, '35'),
(121, 100, 1, 38, '39'),
(122, 100, 1, 40, '41'),
(123, 100, 1, 38, '47'),
(124, 100, 1, 38, '48'),
(125, 100, 1, 49, '50'),
(126, 100, 1, 55, '56'),
(127, 100, 1, 57, '58'),
(128, 100, 1, 57, '59'),
(129, 100, 1, 57, '60'),
(130, 100, 1, 61, '62'),
(131, 100, 1, 38, '63'),
(132, 100, 7, 17, '18'),
(133, 100, 7, 17, '19'),
(134, 100, 7, 38, '39'),
(135, 100, 7, 38, '47'),
(136, 100, 7, 38, '48'),
(137, 100, 7, 57, '58'),
(138, 100, 7, 57, '59');

-- --------------------------------------------------------

--
-- Table structure for table `tblconfigdetails`
--

CREATE TABLE `tblconfigdetails` (
  `configurationid` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `insertdate` date NOT NULL,
  `modifieddate` date NOT NULL,
  `status` enum('Active','Inactive') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblrole`
--

CREATE TABLE `tblrole` (
  `role_id` int(11) NOT NULL,
  `role_name` varchar(255) DEFAULT NULL,
  `role_status` tinyint(1) NOT NULL DEFAULT '1',
  `role_createdon` datetime DEFAULT CURRENT_TIMESTAMP,
  `role_modifiedon` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_activity`
--

CREATE TABLE `tbl_activity` (
  `activity_id` int(11) NOT NULL,
  `activity_name` varchar(50) DEFAULT NULL,
  `activity_discription` varchar(100) DEFAULT NULL,
  `activity_price` int(11) DEFAULT NULL,
  `activity_image` varchar(500) DEFAULT NULL,
  `activity_location` int(11) DEFAULT NULL,
  `activity_status` tinyint(1) DEFAULT NULL COMMENT '0,1,2',
  `activity_createdon` datetime DEFAULT CURRENT_TIMESTAMP,
  `activity_modifiedon` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_activity_package`
--

CREATE TABLE `tbl_activity_package` (
  `ap_id` int(11) NOT NULL,
  `ap_packageid` int(11) DEFAULT NULL,
  `ap_activityid` varchar(255) DEFAULT NULL,
  `ap_status` tinyint(1) DEFAULT NULL COMMENT '0,1,2',
  `ap_createdon` datetime DEFAULT CURRENT_TIMESTAMP,
  `ap_modifiedon` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_addon`
--

CREATE TABLE `tbl_addon` (
  `addon_id` int(11) NOT NULL,
  `addon_name` varchar(255) DEFAULT NULL,
  `addon_price` int(11) DEFAULT NULL,
  `addon_activityid` int(11) DEFAULT NULL,
  `addon_status` tinyint(4) DEFAULT NULL COMMENT '0,1,2',
  `addon_createdon` datetime DEFAULT CURRENT_TIMESTAMP,
  `addon_modifiedon` datetime DEFAULT NULL,
  `addon_branchid` int(11) NOT NULL,
  `addon_description` text NOT NULL,
  `addon_image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_addon`
--

INSERT INTO `tbl_addon` (`addon_id`, `addon_name`, `addon_price`, `addon_activityid`, `addon_status`, `addon_createdon`, `addon_modifiedon`, `addon_branchid`, `addon_description`, `addon_image`) VALUES
(1, 'Socks', 40, NULL, 1, '2017-07-06 02:49:32', '2017-09-30 08:31:52', 1, 'A pair of sock', 'e4e5980d06eb28833dcf6320e23fc026.png'),
(2, 'Tea', 30, NULL, 1, '2017-07-12 04:35:00', '2017-07-12 05:35:30', 1, 'Cup of tea', '2f1cfe2cf9d10a01a7ae79384ec06edf.png'),
(3, 'Soft Toy', 40, NULL, 1, '2017-11-24 15:25:36', '2017-12-23 13:40:30', 1, 'Soft Toy', '3f68d950808b298465c03f3efc741559.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_adminlogin`
--

CREATE TABLE `tbl_adminlogin` (
  `LoginID` smallint(6) NOT NULL,
  `UserName` varchar(100) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `EmployeeId` smallint(6) DEFAULT NULL,
  `Role` smallint(6) DEFAULT NULL,
  `EmpStatus` char(1) DEFAULT 'A',
  `Modified_By` smallint(6) DEFAULT NULL,
  `Modified_On` datetime DEFAULT NULL,
  `CreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_adminlogin`
--

INSERT INTO `tbl_adminlogin` (`LoginID`, `UserName`, `Password`, `EmployeeId`, `Role`, `EmpStatus`, `Modified_By`, `Modified_On`, `CreatedOn`) VALUES
(1, 'durgesh@chiliadprocons.in', '33b3bd3c5f2b5f6f0517cc73d9e88adf', NULL, NULL, 'A', NULL, NULL, '2016-02-15 18:19:29');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_agent_payment_history`
--

CREATE TABLE `tbl_agent_payment_history` (
  `ph_id` int(11) NOT NULL,
  `ph_agent_id` int(11) DEFAULT NULL,
  `ph_tracking_id` int(11) DEFAULT NULL,
  `ph_order_status` varchar(255) DEFAULT NULL COMMENT '0 = ''failed'', 1=''sucess'' 2= ''pending'' ',
  `ph_status_message` text,
  `ph_mer_amount` varchar(255) DEFAULT NULL,
  `ph_wallet_amount` decimal(10,0) NOT NULL,
  `ph_wid` int(11) DEFAULT NULL COMMENT '1 = 3 =  '' wallet id '', 2 = ''order package id''',
  `ph_paymentmode` varchar(2) DEFAULT NULL COMMENT '1 = ''wallet'', 2 = ''ccavenue'', c = ''wallet + ccavenue'''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_agent_payment_history`
--

INSERT INTO `tbl_agent_payment_history` (`ph_id`, `ph_agent_id`, `ph_tracking_id`, `ph_order_status`, `ph_status_message`, `ph_mer_amount`, `ph_wallet_amount`, `ph_wid`, `ph_paymentmode`) VALUES
(1, 7, 0, '0', 'payment failed', '1605', '1605', 1, '0'),
(2, 7, 0, '0', 'payment failed', '1605', '1605', 2, '0'),
(3, 7, 0, '0', 'payment failed', '1605', '1605', 3, '0'),
(4, 7, 0, '0', 'payment failed', '1605', '1605', 4, '0'),
(5, 7, 0, '0', 'payment failed', '1605', '1605', 6, '0');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_agent_profile`
--

CREATE TABLE `tbl_agent_profile` (
  `agent_id` int(11) NOT NULL,
  `agent_name` int(11) NOT NULL,
  `agent_address` int(11) NOT NULL,
  `agent_city` int(11) NOT NULL,
  `agent_state` int(11) NOT NULL,
  `agent_pincode` int(11) NOT NULL,
  `agent_telephone` int(11) NOT NULL,
  `agent_mobile` int(11) NOT NULL,
  `agent_fax` int(11) NOT NULL,
  `agent_website` int(11) NOT NULL,
  `agent_email` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_agent_reg`
--

CREATE TABLE `tbl_agent_reg` (
  `agent_id` int(11) NOT NULL,
  `agent_agencyname` varchar(255) NOT NULL,
  `agent_office_address` text NOT NULL,
  `agent_city` varchar(255) NOT NULL,
  `agent_state` varchar(255) NOT NULL,
  `agent_pincode` int(255) NOT NULL,
  `agent_telephone` bigint(20) NOT NULL,
  `agent_mobile` bigint(20) NOT NULL,
  `agent_fax` int(255) NOT NULL,
  `agent_website` varchar(255) NOT NULL,
  `agent_email` varchar(255) NOT NULL,
  `agent_primarycontact_name` varchar(255) NOT NULL,
  `agent_primarycontact_email_mobile` varchar(255) NOT NULL,
  `agent_primarycontact_telephone` bigint(20) NOT NULL,
  `agent_secondrycontact_name` varchar(255) NOT NULL,
  `agent_secondrycontact_email_phone` varchar(255) NOT NULL,
  `agent_secondrycontact_telephone` bigint(20) NOT NULL,
  `agent_management_executives_name` varchar(255) NOT NULL,
  `agent_modified` date NOT NULL,
  `agent_s` int(11) NOT NULL,
  `agent_copytan` varchar(255) NOT NULL,
  `agent_copyaddressproof` varchar(255) NOT NULL,
  `agent_emailcurrency` varchar(255) NOT NULL,
  `agent_emailcountry` varchar(255) NOT NULL,
  `agent_username` varchar(255) NOT NULL,
  `agent_password` varchar(255) NOT NULL,
  `agent_createdon` date NOT NULL,
  `agent_status` int(11) NOT NULL DEFAULT '0',
  `agent_management_executives_email_phone` varchar(255) NOT NULL,
  `agent_management_executives_telephone` varchar(255) NOT NULL,
  `agent_branches` varchar(255) NOT NULL,
  `agent_yearofestablism` varchar(255) NOT NULL,
  `agent_organisationtype` varchar(255) NOT NULL,
  `agent_lstno` varchar(255) NOT NULL,
  `agent_cstno` varchar(255) NOT NULL,
  `agent_registrationno` varchar(255) NOT NULL,
  `agent_vatno` varchar(255) NOT NULL,
  `agent_servicetaxno` varchar(255) NOT NULL,
  `agent_tanno` varchar(255) NOT NULL,
  `agent_pfno` varchar(255) NOT NULL,
  `agent_esisno` varchar(255) NOT NULL,
  `agent_officeregistrationno` varchar(255) NOT NULL,
  `agent_exceptiontax` varchar(255) NOT NULL,
  `agent_otherexceptiontax` varchar(255) NOT NULL,
  `agent_bank_benificialname` varchar(255) NOT NULL,
  `agent_intermidiatebankname` varchar(255) NOT NULL,
  `agent_bank_ecs` varchar(255) NOT NULL,
  `agent_copypancart` varchar(255) NOT NULL,
  `agent_copyservicetax` varchar(255) NOT NULL,
  `logintype` int(11) NOT NULL DEFAULT '1',
  `agent_panno` varchar(255) NOT NULL,
  `agent_bank_benificialaccno` varchar(255) DEFAULT NULL,
  `agent_bank_benificialbankname` varchar(255) DEFAULT NULL,
  `bank_benificialaddress` text NOT NULL,
  `bank_benificialifsc` varchar(255) NOT NULL,
  `bank_benificialswiftcode` varchar(255) NOT NULL,
  `bank_benificialibanno` varchar(255) NOT NULL,
  `intermidiatebankswiftcode` varchar(255) NOT NULL,
  `agent_emailstatus` varchar(255) NOT NULL,
  `agent_intermidiatebankaddress` text NOT NULL,
  `countryid` int(11) DEFAULT NULL,
  `stateid` int(11) DEFAULT NULL,
  `cityid` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `locationid` int(11) DEFAULT NULL,
  `bank_creditamount` int(11) NOT NULL,
  `bank_agentcommision` int(11) NOT NULL,
  `agent_bank_benificialbranchname` varchar(255) NOT NULL,
  `agent_title` varchar(255) NOT NULL,
  `agent_firstname` varchar(255) NOT NULL,
  `agent_lastname` varchar(255) NOT NULL,
  `agentprofilecomplete` int(11) NOT NULL,
  `agent_aadhar` varchar(50) NOT NULL,
  `agent_ppid` int(11) NOT NULL,
  `agent_pppermission` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_agent_reg`
--

INSERT INTO `tbl_agent_reg` (`agent_id`, `agent_agencyname`, `agent_office_address`, `agent_city`, `agent_state`, `agent_pincode`, `agent_telephone`, `agent_mobile`, `agent_fax`, `agent_website`, `agent_email`, `agent_primarycontact_name`, `agent_primarycontact_email_mobile`, `agent_primarycontact_telephone`, `agent_secondrycontact_name`, `agent_secondrycontact_email_phone`, `agent_secondrycontact_telephone`, `agent_management_executives_name`, `agent_modified`, `agent_s`, `agent_copytan`, `agent_copyaddressproof`, `agent_emailcurrency`, `agent_emailcountry`, `agent_username`, `agent_password`, `agent_createdon`, `agent_status`, `agent_management_executives_email_phone`, `agent_management_executives_telephone`, `agent_branches`, `agent_yearofestablism`, `agent_organisationtype`, `agent_lstno`, `agent_cstno`, `agent_registrationno`, `agent_vatno`, `agent_servicetaxno`, `agent_tanno`, `agent_pfno`, `agent_esisno`, `agent_officeregistrationno`, `agent_exceptiontax`, `agent_otherexceptiontax`, `agent_bank_benificialname`, `agent_intermidiatebankname`, `agent_bank_ecs`, `agent_copypancart`, `agent_copyservicetax`, `logintype`, `agent_panno`, `agent_bank_benificialaccno`, `agent_bank_benificialbankname`, `bank_benificialaddress`, `bank_benificialifsc`, `bank_benificialswiftcode`, `bank_benificialibanno`, `intermidiatebankswiftcode`, `agent_emailstatus`, `agent_intermidiatebankaddress`, `countryid`, `stateid`, `cityid`, `branch_id`, `locationid`, `bank_creditamount`, `bank_agentcommision`, `agent_bank_benificialbranchname`, `agent_title`, `agent_firstname`, `agent_lastname`, `agentprofilecomplete`, `agent_aadhar`, `agent_ppid`, `agent_pppermission`) VALUES
(7, 'simer', 'test addressdd', 'delhi', 'delhii', 110014, 7042715589, 9810668829, 2147483647, 'http://www.google.com', 'simer@elitehrpractices.com', 'bivek', 'bivek11@gmail.com', 2147483647, 'gggggggggggg', 'simm@gmail.com', 1234567890, 'ggggggggggggg', '0000-00-00', 1, '', 'd0d99bdc6efb62d7e8b17bc06324cdcb.jpg', 'bivek11@gmail.com', 'India', 'simer@elitehrpractices.com', 'MTExMTEx', '2017-06-01', 1, 'mindzbug@gmail.com', '0', 'iii', '6787', 'Proprietorship', '999', 'ytyt', 'yyyyyyyyyyyyyyyyyyyyy', 'yyyyyyttttttt', '29', 'tttttttttttttttttt', 'dgfdfg', 'rtrt', 'yyyyyyyyyy', '35', '0', 'tyrt', 'ttr', 'No', 'ebbcee93c510a7ea1b5329fe6041fe2a.jpg', 'a712fca83a1f48f7fce8abc7feaf4a5f.jpg', 1, '29', '5435835', 'ty', 'Address  333333333333333333333333333333333333333333333333333', '42', 'rrrrrrrrrrrrrrrrrr', 'rrrrrrrrrrrrrrrr', 'trtrt', 'International', 'hdfc', 111, 66, 190, 43, 23, 48300, 0, 'tttttttttt', 'Mr', 'simerjeet', 'singh', 1, '0', 0, ''),
(12, 'durgesh', '', '', '', 0, 0, 9029103838, 0, '', 'durgesh@chiliadprocons.in', '', '', 0, '', '', 0, '', '0000-00-00', 0, '', '', '', '', 'durgesh@chiliadprocons.in', 'ZHVyZ2VzaEAxMjM=', '2017-11-27', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', NULL, NULL, '', '', '', '', '', '', '', 1, 1, 1, 1, 1, 0, 0, '', 'Mr', 'Durgesh', 'Chauhan', 0, '', 0, ''),
(13, '', '', '', '', 0, 0, 0, 0, '', '', '', '', 0, '', '', 0, '', '0000-00-00', 0, '', '', '', '', '', 'NDgxMDYy', '2018-01-05', 0, '', '0', '', '', 'Proprietorship', '', '', '', '', '', '', '', '', '', '', '0', '', '', 'Yes', '', '', 1, '', '', '', '', '', '', '', '', 'International', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', 0, '0', 0, ''),
(14, 'Pappu', 'Pappu', 'Pappu', 'Pappu', 110014, 9810668829, 0, 2147483647, 'ssss.ggg', 'bivek908@gmail.com', 'bivke', 'bivek11@gmail.com', 2147483647, 'dfggdf', 'bivek11@gmail.com', 2147483647, 'rfhfg', '0000-00-00', 0, 'a80cede382f14d9d8b9a8b92a5aa653b.png', '6931a647296a3121722ed93775abe410.png', 'INR', 'India', 'bivek908@gmail.com', 'MTExMTEx', '2018-01-09', 1, 'bivek11@gmail.com', '', '4rt', '4444', 'Proprietorship', 'gdfgd', 'dfgfdgfd', 'fghgfhgf', 'hffgh', 'fhf', 'gfgfd', 'dfgdfg', 'dfgdfh', 'gfhgfhf', 'dfgdf', 'dfgdfged', 'hghjhjh', 'hfghgf', 'Yes', '1509994145286Resume_Pappu.pdf', '3156098648284b513f025324926fe64d.png', 1, 'dfdgdf', 'fgdfgdf', 'dfgdfgf', 'fhfghfg', '564564', 'fgdfgf', 'ghjtygjh', 'gfgfghf', 'Domestic', 'dfgfdgfd', 1, 1, 1, 1, 1, 0, 0, 'fghghjghj', 'Mr', 'yahoo', 'singh', 1, '', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_agent_wallet`
--

CREATE TABLE `tbl_agent_wallet` (
  `wid` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `credit_debit_amount` int(11) DEFAULT NULL,
  `credit_debit_pety` enum('C','D') NOT NULL COMMENT 'C (credit) d( debit)',
  `total_amount` int(11) NOT NULL,
  `Status` smallint(1) NOT NULL COMMENT ' 1 ( Active) 0 ( inactive)',
  `Created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `paymentby` varchar(20) NOT NULL COMMENT 'Ag:agent,Ad:Admin',
  `orderid` int(11) NOT NULL,
  `cclinkenable` int(11) NOT NULL COMMENT '1:enable,0: disable'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_agent_wallet`
--

INSERT INTO `tbl_agent_wallet` (`wid`, `user_id`, `credit_debit_amount`, `credit_debit_pety`, `total_amount`, `Status`, `Created_at`, `paymentby`, `orderid`, `cclinkenable`) VALUES
(1, 7, 2000, 'C', 2000, 1, '2018-01-10 05:55:01', 'Ad', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_bannerimage`
--

CREATE TABLE `tbl_bannerimage` (
  `bannerimage_id` int(11) NOT NULL,
  `bannerimage_top` varchar(255) DEFAULT 'background.jpg',
  `bannerimage_country` int(11) DEFAULT NULL,
  `bannerimage_state` int(11) DEFAULT NULL,
  `bannerimage_city` int(11) DEFAULT NULL,
  `bannerimage_location` int(11) DEFAULT NULL,
  `bannerimage_branch` int(11) DEFAULT NULL,
  `bannerimage_logo` varchar(255) NOT NULL,
  `bannerimage_index` varchar(255) NOT NULL,
  `bannerimage_top1` varchar(255) NOT NULL,
  `bannerimage_top2` varchar(255) NOT NULL,
  `bannerimage_top3` varchar(255) NOT NULL,
  `bannerimage_top4` varchar(255) NOT NULL,
  `bannerimage_mailtype` varchar(255) DEFAULT NULL,
  `bannerimage_subject` varchar(255) DEFAULT NULL,
  `bannerimage_from` varchar(255) DEFAULT NULL,
  `bannerimage_branch_contact` varchar(255) DEFAULT NULL,
  `bannerimage_branch_email` varchar(255) DEFAULT NULL,
  `bannerimage_status` tinyint(4) NOT NULL DEFAULT '1',
  `internethandlingcharge` int(11) NOT NULL,
  `bannerimage_apikey` varchar(255) NOT NULL,
  `bannerimage_gstno` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_bannerimage`
--

INSERT INTO `tbl_bannerimage` (`bannerimage_id`, `bannerimage_top`, `bannerimage_country`, `bannerimage_state`, `bannerimage_city`, `bannerimage_location`, `bannerimage_branch`, `bannerimage_logo`, `bannerimage_index`, `bannerimage_top1`, `bannerimage_top2`, `bannerimage_top3`, `bannerimage_top4`, `bannerimage_mailtype`, `bannerimage_subject`, `bannerimage_from`, `bannerimage_branch_contact`, `bannerimage_branch_email`, `bannerimage_status`, `internethandlingcharge`, `bannerimage_apikey`, `bannerimage_gstno`) VALUES
(1, 'background.jpg', NULL, NULL, NULL, NULL, 1, '376e67e3adb2bd7ca6463fdf0c2f85d2.jpg', '', 'Snow World Mumbai ', 'A Brand of Chiliad Procons Pvt. Ltd', 'Snow World', 'Phoenix Market City, Lower Ground Level 58 - 61, B', NULL, 'Snow World Mumbai', 'customerservice@snowworldmumbai.com', '022-6180 1591 / 92 / 93', 'info@snowworldmumbai.com', 1, 30, 'ff08d81897faf0d072cc62d5b81165f1', 'GSTIN: 27AAFCC7259M1Z3');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_bookingtypemaster`
--

CREATE TABLE `tbl_bookingtypemaster` (
  `bookingtypeid` int(11) NOT NULL,
  `bookingname` varchar(255) DEFAULT NULL,
  `booking_addedon` datetime DEFAULT NULL,
  `booking_modifiedon` datetime DEFAULT NULL,
  `booking_status` int(11) DEFAULT '1',
  `booking_country` int(11) DEFAULT NULL,
  `booking_state` int(11) DEFAULT NULL,
  `booking_city` int(11) DEFAULT NULL,
  `booking_location` int(11) DEFAULT NULL,
  `booking_branchid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_branch_master`
--

CREATE TABLE `tbl_branch_master` (
  `branch_id` int(11) NOT NULL,
  `branch_name` varchar(255) NOT NULL,
  `branch_company` int(255) NOT NULL,
  `branch_location` int(255) NOT NULL,
  `branch_url` varchar(50) DEFAULT 'http://115.124.98.243/~skiindia/',
  `branch_background` varchar(255) DEFAULT NULL,
  `branch_logo` varchar(255) DEFAULT NULL,
  `branch_internet_handling_charge` int(5) DEFAULT NULL,
  `branch_status` tinyint(1) NOT NULL COMMENT '0=inactive,1=active,2=deleted',
  `branch_add` varchar(255) DEFAULT NULL,
  `branch_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `branch_modified` datetime DEFAULT NULL,
  `branch_cronejob` int(11) NOT NULL,
  `branch_hrsrestriction` int(11) NOT NULL,
  `branch_minrestriction` int(11) NOT NULL,
  `branch_cronjobrunvalue` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_branch_master`
--

INSERT INTO `tbl_branch_master` (`branch_id`, `branch_name`, `branch_company`, `branch_location`, `branch_url`, `branch_background`, `branch_logo`, `branch_internet_handling_charge`, `branch_status`, `branch_add`, `branch_created`, `branch_modified`, `branch_cronejob`, `branch_hrsrestriction`, `branch_minrestriction`, `branch_cronjobrunvalue`) VALUES
(1, 'Snow World Mumbai', 1, 1, 'http://192.168.1.65/snowworldnew/', 'f93163ceddd2a9c0b93fa60268c44e1e.jpg', '', 25, 1, 'Phoenix Market City, Lower Ground Level 58 - 61, Between Atrium 2 & 6, 15 L.B.S. Marg, Kamani Jn., Kurla (W), Mumbai 400 070, India.', '2017-07-12 05:33:20', '2018-01-02 13:13:30', 2, 2, 0, 12);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_branch_package`
--

CREATE TABLE `tbl_branch_package` (
  `bp_id` int(11) NOT NULL,
  `bp_packageid` int(11) DEFAULT NULL,
  `bp_branchid` varchar(255) DEFAULT NULL,
  `bp_status` tinyint(1) DEFAULT NULL COMMENT '0,1,2',
  `bp_createdon` datetime DEFAULT CURRENT_TIMESTAMP,
  `bp_modifiedon` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_branch_package`
--

INSERT INTO `tbl_branch_package` (`bp_id`, `bp_packageid`, `bp_branchid`, `bp_status`, `bp_createdon`, `bp_modifiedon`) VALUES
(1, 1, '1', 1, '2017-07-12 05:35:09', NULL),
(2, 2, '1', 1, '2017-11-23 12:53:38', NULL),
(3, 3, '1', 1, '2017-11-30 17:52:28', NULL),
(4, 4, '1', 1, '2017-11-30 17:53:29', NULL),
(5, 5, '1', 1, '2017-12-21 12:56:39', NULL),
(6, 6, '1', 1, '2017-12-21 13:01:21', NULL),
(7, 7, '1', 1, '2017-12-21 13:02:12', NULL),
(8, 8, '1', 1, '2017-12-21 13:02:25', NULL),
(9, 9, '1', 1, '2017-12-21 13:05:49', NULL),
(10, 10, '1', 1, '2017-12-21 13:06:03', NULL),
(11, 11, '1', 1, '2017-12-21 13:08:45', NULL),
(12, 12, '1', 1, '2017-12-23 12:21:00', NULL),
(13, 13, '1', 1, '2017-12-23 12:29:25', NULL),
(14, 14, '1', 1, '2017-12-23 12:30:02', NULL),
(15, 15, '1', 1, '2017-12-23 12:37:27', NULL),
(16, 16, '1', 1, '2017-12-23 13:52:03', NULL),
(17, 17, '1', 1, '2017-12-23 13:56:01', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_city`
--

CREATE TABLE `tbl_city` (
  `cityid` bigint(20) NOT NULL,
  `stateid` bigint(20) NOT NULL,
  `cityname` varchar(100) DEFAULT NULL,
  `cstatus` varchar(50) DEFAULT '0',
  `createdon` datetime DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL,
  `countryid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_city`
--

INSERT INTO `tbl_city` (`cityid`, `stateid`, `cityname`, `cstatus`, `createdon`, `modifiedon`, `countryid`) VALUES
(1, 1, 'Mumbai', '1', '2017-07-12 05:25:18', NULL, 0),
(2, 1, 'Mumbai', '2', '2017-07-12 05:25:44', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_commissioin_payment_date`
--

CREATE TABLE `tbl_commissioin_payment_date` (
  `id` int(11) NOT NULL,
  `commission_time` varchar(50) NOT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_commissioin_payment_date`
--

INSERT INTO `tbl_commissioin_payment_date` (`id`, `commission_time`, `created_on`) VALUES
(1, '4', '2017-12-18 12:51:28');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_commission`
--

CREATE TABLE `tbl_commission` (
  `cid` int(11) NOT NULL,
  `start_ticket_id` varchar(10) NOT NULL,
  `end_ticket_id` varchar(20) NOT NULL,
  `commission` varchar(30) NOT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_commission`
--

INSERT INTO `tbl_commission` (`cid`, `start_ticket_id`, `end_ticket_id`, `commission`, `created_on`) VALUES
(17, '1', '10', '4', '2018-01-05 16:11:55'),
(18, '11', '20', '5', '2018-01-05 16:11:55'),
(19, '21', '30', '6', '2018-01-05 16:11:55'),
(20, '31', '50', '7', '2018-01-05 16:11:56');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_company_master`
--

CREATE TABLE `tbl_company_master` (
  `comp_id` int(11) NOT NULL,
  `comp_name` varchar(255) NOT NULL,
  `comp_status` tinyint(1) NOT NULL COMMENT '0=inactive,1=active,2=deleted',
  `comp_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `comp_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_company_master`
--

INSERT INTO `tbl_company_master` (`comp_id`, `comp_name`, `comp_status`, `comp_created`, `comp_modified`) VALUES
(1, 'Chiliad Procons', 1, '2017-07-12 05:24:37', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contact`
--

CREATE TABLE `tbl_contact` (
  `cont_id` int(11) NOT NULL,
  `cont_fname` varchar(255) DEFAULT NULL,
  `cont_lname` varchar(255) DEFAULT NULL,
  `cont_email` varchar(255) DEFAULT NULL,
  `cont_phone` int(11) DEFAULT NULL,
  `cont_message` text,
  `cont_status` int(11) DEFAULT NULL,
  `cont_addedon` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_contact`
--

INSERT INTO `tbl_contact` (`cont_id`, `cont_fname`, `cont_lname`, `cont_email`, `cont_phone`, `cont_message`, `cont_status`, `cont_addedon`) VALUES
(1, 'ravi', 'gupta', 'ravigupta9867@gmial.com', 2147483647, 'want to do the booking for 2 people, but can\'t abl', 1, '2017-07-28 22:21:12');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_country`
--

CREATE TABLE `tbl_country` (
  `conid` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `code` varchar(50) NOT NULL,
  `counstatus` varchar(100) DEFAULT NULL,
  `createdon` datetime DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_country`
--

INSERT INTO `tbl_country` (`conid`, `name`, `code`, `counstatus`, `createdon`, `modifiedon`) VALUES
(1, 'India', 'IN', '1', '2017-06-20 23:26:50', '2017-07-02 23:52:54');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_customer`
--

CREATE TABLE `tbl_customer` (
  `custid` int(11) NOT NULL,
  `custtype` enum('Corporate','Officer') NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` varchar(10) NOT NULL,
  `createdon` datetime NOT NULL,
  `modifiedon` datetime NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_daily_inventory`
--

CREATE TABLE `tbl_daily_inventory` (
  `dailyinventory_id` int(11) NOT NULL,
  `dailyinventory_branchid` int(11) DEFAULT NULL,
  `dailyinventory_from` varchar(11) DEFAULT NULL,
  `dailyinventory_minfrom` varchar(11) NOT NULL,
  `dailyinventory_to` varchar(11) DEFAULT NULL,
  `dailyinventory_minto` varchar(11) NOT NULL,
  `dailyinventory_seats` int(11) DEFAULT NULL,
  `dailyinventory_addons` varchar(150) NOT NULL,
  `dailyinventory_createdon` datetime DEFAULT CURRENT_TIMESTAMP,
  `dailyinventory_date` date NOT NULL,
  `dailyinventory_modifiedon` datetime DEFAULT NULL,
  `dailyinventory_status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_daily_inventory`
--

INSERT INTO `tbl_daily_inventory` (`dailyinventory_id`, `dailyinventory_branchid`, `dailyinventory_from`, `dailyinventory_minfrom`, `dailyinventory_to`, `dailyinventory_minto`, `dailyinventory_seats`, `dailyinventory_addons`, `dailyinventory_createdon`, `dailyinventory_date`, `dailyinventory_modifiedon`, `dailyinventory_status`) VALUES
(1, 1, '11', '00', '12', '00', 150, '1', '2017-07-05 00:23:45', '2018-01-04', '0000-00-00 00:00:00', 1),
(2, 1, '12', '15', '13', '15', 150, '', '2017-07-05 00:23:45', '2018-01-04', '0000-00-00 00:00:00', 1),
(3, 1, '13', '30', '14', '30', 150, '', '2017-07-05 00:23:45', '2018-01-04', '0000-00-00 00:00:00', 1),
(4, 1, '15', '00', '16', '00', 150, '', '2017-07-05 00:23:45', '2018-01-04', '0000-00-00 00:00:00', 1),
(5, 1, '16', '30', '17', '30', 150, '', '2017-07-05 00:23:45', '2018-01-04', '0000-00-00 00:00:00', 1),
(6, 1, '18', '00', '19', '00', 150, '', '2017-07-05 00:23:45', '2018-01-04', '0000-00-00 00:00:00', 1),
(7, 1, '19', '30', '20', '30', 150, '', '2017-07-05 00:23:45', '2018-01-04', '0000-00-00 00:00:00', 1),
(8, 1, '21', '00', '22', '00', 150, '', '2017-07-15 01:36:55', '2018-01-04', '0000-00-00 00:00:00', 1),
(9, 1, '9', '05', '8', '10', 5, '1', '2017-12-13 11:11:02', '2018-01-04', '0000-00-00 00:00:00', 1),
(10, 1, '14', '08', '11', '10', 7, '1,2,3', '2017-12-13 11:11:02', '2018-01-04', '0000-00-00 00:00:00', 1),
(11, 1, '11', '00', '12', '00', 150, '1', '2017-07-05 00:23:45', '2018-01-05', '0000-00-00 00:00:00', 1),
(12, 1, '12', '15', '13', '15', 150, '', '2017-07-05 00:23:45', '2018-01-05', '0000-00-00 00:00:00', 1),
(13, 1, '13', '30', '14', '30', 150, '', '2017-07-05 00:23:45', '2018-01-05', '0000-00-00 00:00:00', 1),
(14, 1, '15', '00', '16', '00', 150, '', '2017-07-05 00:23:45', '2018-01-05', '0000-00-00 00:00:00', 1),
(15, 1, '16', '30', '17', '30', 150, '', '2017-07-05 00:23:45', '2018-01-05', '0000-00-00 00:00:00', 1),
(16, 1, '18', '00', '19', '00', 150, '', '2017-07-05 00:23:45', '2018-01-05', '0000-00-00 00:00:00', 1),
(17, 1, '19', '30', '20', '30', 150, '', '2017-07-05 00:23:45', '2018-01-05', '0000-00-00 00:00:00', 1),
(18, 1, '21', '00', '22', '00', 150, '', '2017-07-15 01:36:55', '2018-01-05', '0000-00-00 00:00:00', 1),
(19, 1, '9', '05', '8', '10', 5, '1', '2017-12-13 11:11:02', '2018-01-05', '0000-00-00 00:00:00', 1),
(20, 1, '14', '08', '11', '10', 7, '1,2,3', '2017-12-13 11:11:02', '2018-01-05', '0000-00-00 00:00:00', 1),
(21, 1, '11', '00', '12', '00', 150, '1', '2017-07-05 00:23:45', '2018-01-06', '0000-00-00 00:00:00', 1),
(22, 1, '12', '15', '13', '15', 150, '', '2017-07-05 00:23:45', '2018-01-06', '0000-00-00 00:00:00', 1),
(23, 1, '13', '30', '14', '30', 150, '', '2017-07-05 00:23:45', '2018-01-06', '0000-00-00 00:00:00', 1),
(24, 1, '15', '00', '16', '00', 150, '', '2017-07-05 00:23:45', '2018-01-06', '0000-00-00 00:00:00', 1),
(25, 1, '16', '30', '17', '30', 150, '', '2017-07-05 00:23:45', '2018-01-06', '0000-00-00 00:00:00', 1),
(26, 1, '18', '00', '19', '00', 150, '', '2017-07-05 00:23:45', '2018-01-06', '0000-00-00 00:00:00', 1),
(27, 1, '19', '30', '20', '30', 150, '', '2017-07-05 00:23:45', '2018-01-06', '0000-00-00 00:00:00', 1),
(28, 1, '21', '00', '22', '00', 150, '', '2017-07-15 01:36:55', '2018-01-06', '0000-00-00 00:00:00', 1),
(29, 1, '9', '05', '8', '10', 5, '1', '2017-12-13 11:11:02', '2018-01-06', '0000-00-00 00:00:00', 1),
(30, 1, '14', '08', '11', '10', 7, '1,2,3', '2017-12-13 11:11:02', '2018-01-06', '0000-00-00 00:00:00', 1),
(31, 1, '11', '00', '12', '00', 150, '1', '2017-07-05 00:23:45', '2018-01-07', '0000-00-00 00:00:00', 1),
(32, 1, '12', '15', '13', '15', 150, '', '2017-07-05 00:23:45', '2018-01-07', '0000-00-00 00:00:00', 1),
(33, 1, '13', '30', '14', '30', 150, '', '2017-07-05 00:23:45', '2018-01-07', '0000-00-00 00:00:00', 1),
(34, 1, '15', '00', '16', '00', 150, '', '2017-07-05 00:23:45', '2018-01-07', '0000-00-00 00:00:00', 1),
(35, 1, '16', '30', '17', '30', 150, '', '2017-07-05 00:23:45', '2018-01-07', '0000-00-00 00:00:00', 1),
(36, 1, '18', '00', '19', '00', 150, '', '2017-07-05 00:23:45', '2018-01-07', '0000-00-00 00:00:00', 1),
(37, 1, '19', '30', '20', '30', 150, '', '2017-07-05 00:23:45', '2018-01-07', '0000-00-00 00:00:00', 1),
(38, 1, '21', '00', '22', '00', 150, '', '2017-07-15 01:36:55', '2018-01-07', '0000-00-00 00:00:00', 1),
(39, 1, '9', '05', '8', '10', 5, '1', '2017-12-13 11:11:02', '2018-01-07', '0000-00-00 00:00:00', 1),
(40, 1, '14', '08', '11', '10', 7, '1,2,3', '2017-12-13 11:11:02', '2018-01-07', '0000-00-00 00:00:00', 1),
(41, 1, '11', '00', '12', '00', 150, '1', '2017-07-05 00:23:45', '2018-01-08', '0000-00-00 00:00:00', 1),
(42, 1, '12', '15', '13', '15', 150, '', '2017-07-05 00:23:45', '2018-01-08', '0000-00-00 00:00:00', 1),
(43, 1, '13', '30', '14', '30', 150, '', '2017-07-05 00:23:45', '2018-01-08', '0000-00-00 00:00:00', 1),
(44, 1, '15', '00', '16', '00', 150, '', '2017-07-05 00:23:45', '2018-01-08', '0000-00-00 00:00:00', 1),
(45, 1, '16', '30', '17', '30', 150, '', '2017-07-05 00:23:45', '2018-01-08', '0000-00-00 00:00:00', 1),
(46, 1, '18', '00', '19', '00', 150, '', '2017-07-05 00:23:45', '2018-01-08', '0000-00-00 00:00:00', 1),
(47, 1, '19', '30', '20', '30', 150, '', '2017-07-05 00:23:45', '2018-01-08', '0000-00-00 00:00:00', 1),
(48, 1, '21', '00', '22', '00', 150, '', '2017-07-15 01:36:55', '2018-01-08', '0000-00-00 00:00:00', 1),
(49, 1, '9', '05', '8', '10', 5, '1', '2017-12-13 11:11:02', '2018-01-08', '0000-00-00 00:00:00', 1),
(50, 1, '14', '08', '11', '10', 7, '1,2,3', '2017-12-13 11:11:02', '2018-01-08', '0000-00-00 00:00:00', 1),
(51, 1, '11', '00', '12', '00', 150, '1', '2017-07-05 00:23:45', '2018-01-09', '0000-00-00 00:00:00', 1),
(52, 1, '12', '15', '13', '15', 150, '', '2017-07-05 00:23:45', '2018-01-09', '0000-00-00 00:00:00', 1),
(53, 1, '13', '30', '14', '30', 150, '', '2017-07-05 00:23:45', '2018-01-09', '0000-00-00 00:00:00', 1),
(54, 1, '15', '00', '16', '00', 150, '', '2017-07-05 00:23:45', '2018-01-09', '0000-00-00 00:00:00', 1),
(55, 1, '16', '30', '17', '30', 150, '', '2017-07-05 00:23:45', '2018-01-09', '0000-00-00 00:00:00', 1),
(56, 1, '18', '00', '19', '00', 150, '', '2017-07-05 00:23:45', '2018-01-09', '0000-00-00 00:00:00', 1),
(57, 1, '19', '30', '20', '30', 150, '', '2017-07-05 00:23:45', '2018-01-09', '0000-00-00 00:00:00', 1),
(58, 1, '21', '00', '22', '00', 150, '', '2017-07-15 01:36:55', '2018-01-09', '0000-00-00 00:00:00', 1),
(59, 1, '9', '05', '8', '10', 5, '1', '2017-12-13 11:11:02', '2018-01-09', '0000-00-00 00:00:00', 1),
(60, 1, '14', '08', '11', '10', 7, '1,2,3', '2017-12-13 11:11:02', '2018-01-09', '0000-00-00 00:00:00', 1),
(61, 1, '11', '00', '12', '00', 150, '1', '2017-07-05 00:23:45', '2018-01-10', '0000-00-00 00:00:00', 1),
(62, 1, '12', '15', '13', '15', 150, '', '2017-07-05 00:23:45', '2018-01-10', '0000-00-00 00:00:00', 1),
(63, 1, '13', '30', '14', '30', 150, '', '2017-07-05 00:23:45', '2018-01-10', '0000-00-00 00:00:00', 1),
(64, 1, '15', '00', '16', '00', 150, '', '2017-07-05 00:23:45', '2018-01-10', '0000-00-00 00:00:00', 1),
(65, 1, '16', '30', '17', '30', 150, '', '2017-07-05 00:23:45', '2018-01-10', '0000-00-00 00:00:00', 1),
(66, 1, '18', '00', '19', '00', 150, '', '2017-07-05 00:23:45', '2018-01-10', '0000-00-00 00:00:00', 1),
(67, 1, '19', '30', '20', '30', 150, '', '2017-07-05 00:23:45', '2018-01-10', '0000-00-00 00:00:00', 1),
(68, 1, '21', '00', '22', '00', 150, '', '2017-07-15 01:36:55', '2018-01-10', '0000-00-00 00:00:00', 1),
(69, 1, '9', '05', '8', '10', 5, '1', '2017-12-13 11:11:02', '2018-01-10', '0000-00-00 00:00:00', 1),
(70, 1, '14', '08', '11', '10', 7, '1,2,3', '2017-12-13 11:11:02', '2018-01-10', '0000-00-00 00:00:00', 1),
(71, 1, '11', '00', '12', '00', 150, '1', '2017-07-05 00:23:45', '2018-01-11', '0000-00-00 00:00:00', 1),
(72, 1, '12', '15', '13', '15', 150, '', '2017-07-05 00:23:45', '2018-01-11', '0000-00-00 00:00:00', 1),
(73, 1, '13', '30', '14', '30', 150, '', '2017-07-05 00:23:45', '2018-01-11', '0000-00-00 00:00:00', 1),
(74, 1, '15', '00', '16', '00', 150, '', '2017-07-05 00:23:45', '2018-01-11', '0000-00-00 00:00:00', 1),
(75, 1, '16', '30', '17', '30', 150, '', '2017-07-05 00:23:45', '2018-01-11', '0000-00-00 00:00:00', 1),
(76, 1, '18', '00', '19', '00', 150, '', '2017-07-05 00:23:45', '2018-01-11', '0000-00-00 00:00:00', 1),
(77, 1, '19', '30', '20', '30', 150, '', '2017-07-05 00:23:45', '2018-01-11', '0000-00-00 00:00:00', 1),
(78, 1, '21', '00', '22', '00', 150, '', '2017-07-15 01:36:55', '2018-01-11', '0000-00-00 00:00:00', 1),
(79, 1, '9', '05', '8', '10', 5, '1', '2017-12-13 11:11:02', '2018-01-11', '0000-00-00 00:00:00', 1),
(80, 1, '14', '08', '11', '10', 7, '1,2,3', '2017-12-13 11:11:02', '2018-01-11', '0000-00-00 00:00:00', 1),
(81, 1, '11', '00', '12', '00', 150, '1', '2017-07-05 00:23:45', '2018-01-12', '0000-00-00 00:00:00', 1),
(82, 1, '12', '15', '13', '15', 150, '', '2017-07-05 00:23:45', '2018-01-12', '0000-00-00 00:00:00', 1),
(83, 1, '13', '30', '14', '30', 150, '', '2017-07-05 00:23:45', '2018-01-12', '0000-00-00 00:00:00', 1),
(84, 1, '15', '00', '16', '00', 150, '', '2017-07-05 00:23:45', '2018-01-12', '0000-00-00 00:00:00', 1),
(85, 1, '16', '30', '17', '30', 150, '', '2017-07-05 00:23:45', '2018-01-12', '0000-00-00 00:00:00', 1),
(86, 1, '18', '00', '19', '00', 150, '', '2017-07-05 00:23:45', '2018-01-12', '0000-00-00 00:00:00', 1),
(87, 1, '19', '30', '20', '30', 150, '', '2017-07-05 00:23:45', '2018-01-12', '0000-00-00 00:00:00', 1),
(88, 1, '21', '00', '22', '00', 150, '', '2017-07-15 01:36:55', '2018-01-12', '0000-00-00 00:00:00', 1),
(89, 1, '9', '05', '8', '10', 5, '1', '2017-12-13 11:11:02', '2018-01-12', '0000-00-00 00:00:00', 1),
(90, 1, '14', '08', '11', '10', 7, '1,2,3', '2017-12-13 11:11:02', '2018-01-12', '0000-00-00 00:00:00', 1),
(91, 1, '11', '00', '12', '00', 150, '1', '2017-07-05 00:23:45', '2018-01-13', '0000-00-00 00:00:00', 1),
(92, 1, '12', '15', '13', '15', 150, '', '2017-07-05 00:23:45', '2018-01-13', '0000-00-00 00:00:00', 1),
(93, 1, '13', '30', '14', '30', 150, '', '2017-07-05 00:23:45', '2018-01-13', '0000-00-00 00:00:00', 1),
(94, 1, '15', '00', '16', '00', 150, '', '2017-07-05 00:23:45', '2018-01-13', '0000-00-00 00:00:00', 1),
(95, 1, '16', '30', '17', '30', 150, '', '2017-07-05 00:23:45', '2018-01-13', '0000-00-00 00:00:00', 1),
(96, 1, '18', '00', '19', '00', 150, '', '2017-07-05 00:23:45', '2018-01-13', '0000-00-00 00:00:00', 1),
(97, 1, '19', '30', '20', '30', 150, '', '2017-07-05 00:23:45', '2018-01-13', '0000-00-00 00:00:00', 1),
(98, 1, '21', '00', '22', '00', 150, '', '2017-07-15 01:36:55', '2018-01-13', '0000-00-00 00:00:00', 1),
(99, 1, '9', '05', '8', '10', 5, '1', '2017-12-13 11:11:02', '2018-01-13', '0000-00-00 00:00:00', 1),
(100, 1, '14', '08', '11', '10', 7, '1,2,3', '2017-12-13 11:11:02', '2018-01-13', '0000-00-00 00:00:00', 1),
(101, 1, '11', '00', '12', '00', 150, '1', '2017-07-05 00:23:45', '2018-01-14', '0000-00-00 00:00:00', 1),
(102, 1, '12', '15', '13', '15', 150, '', '2017-07-05 00:23:45', '2018-01-14', '0000-00-00 00:00:00', 1),
(103, 1, '13', '30', '14', '30', 150, '', '2017-07-05 00:23:45', '2018-01-14', '0000-00-00 00:00:00', 1),
(104, 1, '15', '00', '16', '00', 150, '', '2017-07-05 00:23:45', '2018-01-14', '0000-00-00 00:00:00', 1),
(105, 1, '16', '30', '17', '30', 150, '', '2017-07-05 00:23:45', '2018-01-14', '0000-00-00 00:00:00', 1),
(106, 1, '18', '00', '19', '00', 150, '', '2017-07-05 00:23:45', '2018-01-14', '0000-00-00 00:00:00', 1),
(107, 1, '19', '30', '20', '30', 150, '', '2017-07-05 00:23:45', '2018-01-14', '0000-00-00 00:00:00', 1),
(108, 1, '21', '00', '22', '00', 150, '', '2017-07-15 01:36:55', '2018-01-14', '0000-00-00 00:00:00', 1),
(109, 1, '9', '05', '8', '10', 5, '1', '2017-12-13 11:11:02', '2018-01-14', '0000-00-00 00:00:00', 1),
(110, 1, '14', '08', '11', '10', 7, '1,2,3', '2017-12-13 11:11:02', '2018-01-14', '0000-00-00 00:00:00', 1),
(111, 1, '11', '00', '12', '00', 150, '1', '2017-07-05 00:23:45', '2018-01-15', '0000-00-00 00:00:00', 1),
(112, 1, '12', '15', '13', '15', 150, '', '2017-07-05 00:23:45', '2018-01-15', '0000-00-00 00:00:00', 1),
(113, 1, '13', '30', '14', '30', 150, '', '2017-07-05 00:23:45', '2018-01-15', '0000-00-00 00:00:00', 1),
(114, 1, '15', '00', '16', '00', 150, '', '2017-07-05 00:23:45', '2018-01-15', '0000-00-00 00:00:00', 1),
(115, 1, '16', '30', '17', '30', 150, '', '2017-07-05 00:23:45', '2018-01-15', '0000-00-00 00:00:00', 1),
(116, 1, '18', '00', '19', '00', 150, '', '2017-07-05 00:23:45', '2018-01-15', '0000-00-00 00:00:00', 1),
(117, 1, '19', '30', '20', '30', 150, '', '2017-07-05 00:23:45', '2018-01-15', '0000-00-00 00:00:00', 1),
(118, 1, '21', '00', '22', '00', 150, '', '2017-07-15 01:36:55', '2018-01-15', '0000-00-00 00:00:00', 1),
(119, 1, '9', '05', '8', '10', 5, '1', '2017-12-13 11:11:02', '2018-01-15', '0000-00-00 00:00:00', 1),
(120, 1, '14', '08', '11', '10', 7, '1,2,3', '2017-12-13 11:11:02', '2018-01-15', '0000-00-00 00:00:00', 1),
(121, 1, '11', '00', '12', '00', 150, '1', '2017-07-05 00:23:45', '2018-01-16', '0000-00-00 00:00:00', 1),
(122, 1, '12', '15', '13', '15', 150, '', '2017-07-05 00:23:45', '2018-01-16', '0000-00-00 00:00:00', 1),
(123, 1, '13', '30', '14', '30', 150, '', '2017-07-05 00:23:45', '2018-01-16', '0000-00-00 00:00:00', 1),
(124, 1, '15', '00', '16', '00', 150, '', '2017-07-05 00:23:45', '2018-01-16', '0000-00-00 00:00:00', 1),
(125, 1, '16', '30', '17', '30', 150, '', '2017-07-05 00:23:45', '2018-01-16', '0000-00-00 00:00:00', 1),
(126, 1, '18', '00', '19', '00', 150, '', '2017-07-05 00:23:45', '2018-01-16', '0000-00-00 00:00:00', 1),
(127, 1, '19', '30', '20', '30', 150, '', '2017-07-05 00:23:45', '2018-01-16', '0000-00-00 00:00:00', 1),
(128, 1, '21', '00', '22', '00', 150, '', '2017-07-15 01:36:55', '2018-01-16', '0000-00-00 00:00:00', 1),
(129, 1, '9', '05', '8', '10', 5, '1', '2017-12-13 11:11:02', '2018-01-16', '0000-00-00 00:00:00', 1),
(130, 1, '14', '08', '11', '10', 7, '1,2,3', '2017-12-13 11:11:02', '2018-01-16', '0000-00-00 00:00:00', 1),
(131, 1, '11', '00', '12', '00', 200, '1,2,3', '2018-01-04 11:41:45', '2018-01-17', NULL, 1),
(132, 1, '12', '15', '13', '15', 200, '1,2', '2018-01-04 11:41:45', '2018-01-17', NULL, 1),
(133, 1, '13', '30', '14', '30', 200, '2,3', '2018-01-04 11:41:45', '2018-01-17', NULL, 1),
(134, 1, '15', '00', '16', '00', 200, '1,2,3', '2018-01-04 11:41:45', '2018-01-17', NULL, 1),
(135, 1, '16', '30', '17', '30', 200, '1,2,3', '2018-01-04 11:41:45', '2018-01-17', NULL, 1),
(136, 1, '18', '00', '19', '00', 200, '1,2,3', '2018-01-04 11:41:46', '2018-01-17', NULL, 1),
(137, 1, '19', '30', '20', '30', 200, '1,2,3', '2018-01-04 11:41:46', '2018-01-17', NULL, 1),
(138, 1, '21', '00', '22', '00', 200, '1,2,3', '2018-01-04 11:41:46', '2018-01-17', NULL, 1),
(139, 1, '11', '00', '12', '00', 200, '1,2,3', '2018-01-04 11:41:46', '2018-01-18', NULL, 1),
(140, 1, '12', '15', '13', '15', 200, '1,2', '2018-01-04 11:41:46', '2018-01-18', NULL, 1),
(141, 1, '13', '30', '14', '30', 200, '2,3', '2018-01-04 11:41:46', '2018-01-18', NULL, 1),
(142, 1, '15', '00', '16', '00', 200, '1,2,3', '2018-01-04 11:41:47', '2018-01-18', NULL, 1),
(143, 1, '16', '30', '17', '30', 200, '1,2,3', '2018-01-04 11:41:47', '2018-01-18', NULL, 1),
(144, 1, '18', '00', '19', '00', 200, '1,2,3', '2018-01-04 11:41:47', '2018-01-18', NULL, 1),
(145, 1, '19', '30', '20', '30', 200, '1,2,3', '2018-01-04 11:41:47', '2018-01-18', NULL, 1),
(146, 1, '21', '00', '22', '00', 200, '1,2,3', '2018-01-04 11:41:47', '2018-01-18', NULL, 1),
(147, 1, '11', '00', '12', '00', 200, '1,2,3', '2018-01-04 11:41:47', '2018-01-19', NULL, 1),
(148, 1, '12', '15', '13', '15', 200, '1,2', '2018-01-04 11:41:48', '2018-01-19', NULL, 1),
(149, 1, '13', '30', '14', '30', 200, '2,3', '2018-01-04 11:41:48', '2018-01-19', NULL, 1),
(150, 1, '15', '00', '16', '00', 200, '1,2,3', '2018-01-04 11:41:48', '2018-01-19', NULL, 1),
(151, 1, '16', '30', '17', '30', 200, '1,2,3', '2018-01-04 11:41:48', '2018-01-19', NULL, 1),
(152, 1, '18', '00', '19', '00', 200, '1,2,3', '2018-01-04 11:41:48', '2018-01-19', NULL, 1),
(153, 1, '19', '30', '20', '30', 200, '1,2,3', '2018-01-04 11:41:49', '2018-01-19', NULL, 1),
(154, 1, '21', '00', '22', '00', 200, '1,2,3', '2018-01-04 11:41:49', '2018-01-19', NULL, 1),
(155, 1, '11', '00', '12', '00', 200, '1,2,3', '2018-01-04 11:41:49', '2018-01-20', NULL, 1),
(156, 1, '12', '15', '13', '15', 200, '1,2', '2018-01-04 11:41:49', '2018-01-20', NULL, 1),
(157, 1, '13', '30', '14', '30', 200, '2,3', '2018-01-04 11:41:49', '2018-01-20', NULL, 1),
(158, 1, '15', '00', '16', '00', 200, '1,2,3', '2018-01-04 11:41:49', '2018-01-20', NULL, 1),
(159, 1, '16', '30', '17', '30', 200, '1,2,3', '2018-01-04 11:41:50', '2018-01-20', NULL, 1),
(160, 1, '18', '00', '19', '00', 200, '1,2,3', '2018-01-04 11:41:50', '2018-01-20', NULL, 1),
(161, 1, '19', '30', '20', '30', 200, '1,2,3', '2018-01-04 11:41:50', '2018-01-20', NULL, 1),
(162, 1, '21', '00', '22', '00', 200, '1,2,3', '2018-01-04 11:41:50', '2018-01-20', NULL, 1),
(163, 1, '11', '00', '12', '00', 200, '1,2,3', '2018-01-04 11:41:50', '2018-01-21', NULL, 1),
(164, 1, '12', '15', '13', '15', 200, '1,2', '2018-01-04 11:41:51', '2018-01-21', NULL, 1),
(165, 1, '13', '30', '14', '30', 200, '2,3', '2018-01-04 11:41:51', '2018-01-21', NULL, 1),
(166, 1, '15', '00', '16', '00', 200, '1,2,3', '2018-01-04 11:41:51', '2018-01-21', NULL, 1),
(167, 1, '16', '30', '17', '30', 200, '1,2,3', '2018-01-04 11:41:51', '2018-01-21', NULL, 1),
(168, 1, '18', '00', '19', '00', 200, '1,2,3', '2018-01-04 11:41:51', '2018-01-21', NULL, 1),
(169, 1, '19', '30', '20', '30', 200, '1,2,3', '2018-01-04 11:41:51', '2018-01-21', NULL, 1),
(170, 1, '21', '00', '22', '00', 200, '1,2,3', '2018-01-04 11:41:52', '2018-01-21', NULL, 1),
(171, 1, '11', '00', '12', '00', 200, '1,2,3', '2018-01-04 11:41:52', '2018-01-22', NULL, 1),
(172, 1, '12', '15', '13', '15', 200, '1,2', '2018-01-04 11:41:52', '2018-01-22', NULL, 1),
(173, 1, '13', '30', '14', '30', 200, '2,3', '2018-01-04 11:41:52', '2018-01-22', NULL, 1),
(174, 1, '15', '00', '16', '00', 200, '1,2,3', '2018-01-04 11:41:52', '2018-01-22', NULL, 1),
(175, 1, '16', '30', '17', '30', 200, '1,2,3', '2018-01-04 11:41:52', '2018-01-22', NULL, 1),
(176, 1, '18', '00', '19', '00', 200, '1,2,3', '2018-01-04 11:41:53', '2018-01-22', NULL, 1),
(177, 1, '19', '30', '20', '30', 200, '1,2,3', '2018-01-04 11:41:53', '2018-01-22', NULL, 1),
(178, 1, '21', '00', '22', '00', 200, '1,2,3', '2018-01-04 11:41:53', '2018-01-22', NULL, 1),
(179, 1, '11', '00', '12', '00', 200, '1,2,3', '2018-01-04 11:41:53', '2018-01-23', NULL, 1),
(180, 1, '12', '15', '13', '15', 200, '1,2', '2018-01-04 11:41:53', '2018-01-23', NULL, 1),
(181, 1, '13', '30', '14', '30', 200, '2,3', '2018-01-04 11:41:54', '2018-01-23', NULL, 1),
(182, 1, '15', '00', '16', '00', 200, '1,2,3', '2018-01-04 11:41:54', '2018-01-23', NULL, 1),
(183, 1, '16', '30', '17', '30', 200, '1,2,3', '2018-01-04 11:41:54', '2018-01-23', NULL, 1),
(184, 1, '18', '00', '19', '00', 200, '1,2,3', '2018-01-04 11:41:54', '2018-01-23', NULL, 1),
(185, 1, '19', '30', '20', '30', 200, '1,2,3', '2018-01-04 11:41:54', '2018-01-23', NULL, 1),
(186, 1, '21', '00', '22', '00', 200, '1,2,3', '2018-01-04 11:41:54', '2018-01-23', NULL, 1),
(187, 1, '11', '00', '12', '00', 200, '1,2,3', '2018-01-04 11:41:55', '2018-01-24', NULL, 1),
(188, 1, '12', '15', '13', '15', 200, '1,2', '2018-01-04 11:41:55', '2018-01-24', NULL, 1),
(189, 1, '13', '30', '14', '30', 200, '2,3', '2018-01-04 11:41:55', '2018-01-24', NULL, 1),
(190, 1, '15', '00', '16', '00', 200, '1,2,3', '2018-01-04 11:41:55', '2018-01-24', NULL, 1),
(191, 1, '16', '30', '17', '30', 200, '1,2,3', '2018-01-04 11:41:56', '2018-01-24', NULL, 1),
(192, 1, '18', '00', '19', '00', 200, '1,2,3', '2018-01-04 11:41:56', '2018-01-24', NULL, 1),
(193, 1, '19', '30', '20', '30', 200, '1,2,3', '2018-01-04 11:41:56', '2018-01-24', NULL, 1),
(194, 1, '21', '00', '22', '00', 200, '1,2,3', '2018-01-04 11:41:56', '2018-01-24', NULL, 1),
(195, 1, '11', '00', '12', '00', 200, '1,2,3', '2018-01-04 11:41:56', '2018-01-25', NULL, 1),
(196, 1, '12', '15', '13', '15', 200, '1,2', '2018-01-04 11:41:57', '2018-01-25', NULL, 1),
(197, 1, '13', '30', '14', '30', 200, '2,3', '2018-01-04 11:41:57', '2018-01-25', NULL, 1),
(198, 1, '15', '00', '16', '00', 200, '1,2,3', '2018-01-04 11:41:57', '2018-01-25', NULL, 1),
(199, 1, '16', '30', '17', '30', 200, '1,2,3', '2018-01-04 11:41:57', '2018-01-25', NULL, 1),
(200, 1, '18', '00', '19', '00', 200, '1,2,3', '2018-01-04 11:41:57', '2018-01-25', NULL, 1),
(201, 1, '19', '30', '20', '30', 200, '1,2,3', '2018-01-04 11:41:57', '2018-01-25', NULL, 1),
(202, 1, '21', '00', '22', '00', 200, '1,2,3', '2018-01-04 11:41:58', '2018-01-25', NULL, 1),
(203, 1, '11', '00', '12', '00', 200, '1,2,3', '2018-01-04 11:41:58', '2018-01-26', NULL, 1),
(204, 1, '12', '15', '13', '15', 200, '1,2', '2018-01-04 11:41:58', '2018-01-26', NULL, 1),
(205, 1, '13', '30', '14', '30', 200, '2,3', '2018-01-04 11:41:58', '2018-01-26', NULL, 1),
(206, 1, '15', '00', '16', '00', 200, '1,2,3', '2018-01-04 11:41:58', '2018-01-26', NULL, 1),
(207, 1, '16', '30', '17', '30', 200, '1,2,3', '2018-01-04 11:41:58', '2018-01-26', NULL, 1),
(208, 1, '18', '00', '19', '00', 200, '1,2,3', '2018-01-04 11:41:59', '2018-01-26', NULL, 1),
(209, 1, '19', '30', '20', '30', 200, '1,2,3', '2018-01-04 11:41:59', '2018-01-26', NULL, 1),
(210, 1, '21', '00', '22', '00', 200, '1,2,3', '2018-01-04 11:41:59', '2018-01-26', NULL, 1),
(211, 1, '11', '00', '12', '00', 200, '1,2,3', '2018-01-04 11:41:59', '2018-01-27', NULL, 1),
(212, 1, '12', '15', '13', '15', 200, '1,2', '2018-01-04 11:41:59', '2018-01-27', NULL, 1),
(213, 1, '13', '30', '14', '30', 200, '2,3', '2018-01-04 11:41:59', '2018-01-27', NULL, 1),
(214, 1, '15', '00', '16', '00', 200, '1,2,3', '2018-01-04 11:42:00', '2018-01-27', NULL, 1),
(215, 1, '16', '30', '17', '30', 200, '1,2,3', '2018-01-04 11:42:00', '2018-01-27', NULL, 1),
(216, 1, '18', '00', '19', '00', 200, '1,2,3', '2018-01-04 11:42:00', '2018-01-27', NULL, 1),
(217, 1, '19', '30', '20', '30', 200, '1,2,3', '2018-01-04 11:42:00', '2018-01-27', NULL, 1),
(218, 1, '21', '00', '22', '00', 200, '1,2,3', '2018-01-04 11:42:00', '2018-01-27', NULL, 1),
(219, 1, '11', '00', '12', '00', 200, '1,2,3', '2018-01-04 11:42:00', '2018-01-28', NULL, 1),
(220, 1, '12', '15', '13', '15', 200, '1,2', '2018-01-04 11:42:01', '2018-01-28', NULL, 1),
(221, 1, '13', '30', '14', '30', 200, '2,3', '2018-01-04 11:42:02', '2018-01-28', NULL, 1),
(222, 1, '15', '00', '16', '00', 200, '1,2,3', '2018-01-04 11:42:02', '2018-01-28', NULL, 1),
(223, 1, '16', '30', '17', '30', 200, '1,2,3', '2018-01-04 11:42:02', '2018-01-28', NULL, 1),
(224, 1, '18', '00', '19', '00', 200, '1,2,3', '2018-01-04 11:42:03', '2018-01-28', NULL, 1),
(225, 1, '19', '30', '20', '30', 200, '1,2,3', '2018-01-04 11:42:03', '2018-01-28', NULL, 1),
(226, 1, '21', '00', '22', '00', 200, '1,2,3', '2018-01-04 11:42:03', '2018-01-28', NULL, 1),
(227, 1, '11', '00', '12', '00', 200, '1,2,3', '2018-01-04 11:42:03', '2018-01-29', NULL, 1),
(228, 1, '12', '15', '13', '15', 200, '1,2', '2018-01-04 11:42:03', '2018-01-29', NULL, 1),
(229, 1, '13', '30', '14', '30', 200, '2,3', '2018-01-04 11:42:03', '2018-01-29', NULL, 1),
(230, 1, '15', '00', '16', '00', 200, '1,2,3', '2018-01-04 11:42:04', '2018-01-29', NULL, 1),
(231, 1, '16', '30', '17', '30', 200, '1,2,3', '2018-01-04 11:42:04', '2018-01-29', NULL, 1),
(232, 1, '18', '00', '19', '00', 200, '1,2,3', '2018-01-04 11:42:04', '2018-01-29', NULL, 1),
(233, 1, '19', '30', '20', '30', 200, '1,2,3', '2018-01-04 11:42:04', '2018-01-29', NULL, 1),
(234, 1, '21', '00', '22', '00', 200, '1,2,3', '2018-01-04 11:42:04', '2018-01-29', NULL, 1),
(235, 1, '11', '00', '12', '00', 200, '1,2,3', '2018-01-04 11:42:04', '2018-01-30', NULL, 1),
(236, 1, '12', '15', '13', '15', 200, '1,2', '2018-01-04 11:42:05', '2018-01-30', NULL, 1),
(237, 1, '13', '30', '14', '30', 200, '2,3', '2018-01-04 11:42:05', '2018-01-30', NULL, 1),
(238, 1, '15', '00', '16', '00', 200, '1,2,3', '2018-01-04 11:42:05', '2018-01-30', NULL, 1),
(239, 1, '16', '30', '17', '30', 200, '1,2,3', '2018-01-04 11:42:05', '2018-01-30', NULL, 1),
(240, 1, '18', '00', '19', '00', 200, '1,2,3', '2018-01-04 11:42:05', '2018-01-30', NULL, 1),
(241, 1, '19', '30', '20', '30', 200, '1,2,3', '2018-01-04 11:42:05', '2018-01-30', NULL, 1),
(242, 1, '21', '00', '22', '00', 200, '1,2,3', '2018-01-04 11:42:05', '2018-01-30', NULL, 1),
(243, 1, '11', '00', '12', '00', 200, '1,2,3', '2018-01-04 11:42:06', '2018-01-31', NULL, 1),
(244, 1, '12', '15', '13', '15', 200, '1,2', '2018-01-04 11:42:06', '2018-01-31', NULL, 1),
(245, 1, '13', '30', '14', '30', 200, '2,3', '2018-01-04 11:42:06', '2018-01-31', NULL, 1),
(246, 1, '15', '00', '16', '00', 200, '1,2,3', '2018-01-04 11:42:06', '2018-01-31', NULL, 1),
(247, 1, '16', '30', '17', '30', 200, '1,2,3', '2018-01-04 11:42:06', '2018-01-31', NULL, 1),
(248, 1, '18', '00', '19', '00', 200, '1,2,3', '2018-01-04 11:42:07', '2018-01-31', NULL, 1),
(249, 1, '19', '30', '20', '30', 200, '1,2,3', '2018-01-04 11:42:07', '2018-01-31', NULL, 1),
(250, 1, '21', '00', '22', '00', 200, '1,2,3', '2018-01-04 11:42:07', '2018-01-31', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_daily_inventorypackages`
--

CREATE TABLE `tbl_daily_inventorypackages` (
  `dailyinventorypackages_id` int(11) NOT NULL,
  `dailyinventorypackages_dailyinventoryid` int(11) DEFAULT NULL,
  `dailyinventorypackages_packagesid` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_daily_inventorypackages`
--

INSERT INTO `tbl_daily_inventorypackages` (`dailyinventorypackages_id`, `dailyinventorypackages_dailyinventoryid`, `dailyinventorypackages_packagesid`, `status`) VALUES
(1, 1, 1, 1),
(2, 1, 2, 1),
(3, 1, 3, 1),
(4, 2, 3, 1),
(5, 3, 3, 1),
(6, 3, 4, 1),
(7, 4, 2, 1),
(8, 4, 3, 1),
(9, 5, 3, 1),
(10, 5, 4, 1),
(11, 6, 3, 1),
(12, 7, 1, 1),
(13, 7, 4, 1),
(14, 8, 2, 1),
(15, 8, 3, 1),
(16, 9, 2, 1),
(17, 10, 1, 1),
(18, 10, 2, 1),
(19, 10, 3, 1),
(20, 10, 4, 1),
(21, 11, 1, 1),
(22, 11, 2, 1),
(23, 11, 3, 1),
(24, 12, 3, 1),
(25, 13, 3, 1),
(26, 13, 4, 1),
(27, 14, 2, 1),
(28, 14, 3, 1),
(29, 15, 3, 1),
(30, 15, 4, 1),
(31, 16, 3, 1),
(32, 17, 1, 1),
(33, 17, 4, 1),
(34, 18, 2, 1),
(35, 18, 3, 1),
(36, 19, 2, 1),
(37, 20, 1, 1),
(38, 20, 2, 1),
(39, 20, 3, 1),
(40, 20, 4, 1),
(41, 21, 1, 1),
(42, 21, 2, 1),
(43, 21, 3, 1),
(44, 22, 3, 1),
(45, 23, 3, 1),
(46, 23, 4, 1),
(47, 24, 2, 1),
(48, 24, 3, 1),
(49, 25, 3, 1),
(50, 25, 4, 1),
(51, 26, 3, 1),
(52, 27, 1, 1),
(53, 27, 4, 1),
(54, 28, 2, 1),
(55, 28, 3, 1),
(56, 29, 2, 1),
(57, 30, 1, 1),
(58, 30, 2, 1),
(59, 30, 3, 1),
(60, 30, 4, 1),
(61, 31, 1, 1),
(62, 31, 2, 1),
(63, 31, 3, 1),
(64, 32, 3, 1),
(65, 33, 3, 1),
(66, 33, 4, 1),
(67, 34, 2, 1),
(68, 34, 3, 1),
(69, 35, 3, 1),
(70, 35, 4, 1),
(71, 36, 3, 1),
(72, 37, 1, 1),
(73, 37, 4, 1),
(74, 38, 2, 1),
(75, 38, 3, 1),
(76, 39, 2, 1),
(77, 40, 1, 1),
(78, 40, 2, 1),
(79, 40, 3, 1),
(80, 40, 4, 1),
(81, 41, 1, 1),
(82, 41, 2, 1),
(83, 41, 3, 1),
(84, 42, 3, 1),
(85, 43, 3, 1),
(86, 43, 4, 1),
(87, 44, 2, 1),
(88, 44, 3, 1),
(89, 45, 3, 1),
(90, 45, 4, 1),
(91, 46, 3, 1),
(92, 47, 1, 1),
(93, 47, 4, 1),
(94, 48, 2, 1),
(95, 48, 3, 1),
(96, 49, 2, 1),
(97, 50, 1, 1),
(98, 50, 2, 1),
(99, 50, 3, 1),
(100, 50, 4, 1),
(101, 51, 1, 1),
(102, 51, 2, 1),
(103, 51, 3, 1),
(104, 52, 3, 1),
(105, 53, 3, 1),
(106, 53, 4, 1),
(107, 54, 2, 1),
(108, 54, 3, 1),
(109, 55, 3, 1),
(110, 55, 4, 1),
(111, 56, 3, 1),
(112, 57, 1, 1),
(113, 57, 4, 1),
(114, 58, 2, 1),
(115, 58, 3, 1),
(116, 59, 2, 1),
(117, 60, 1, 1),
(118, 60, 2, 1),
(119, 60, 3, 1),
(120, 60, 4, 1),
(121, 61, 1, 1),
(122, 61, 2, 1),
(123, 61, 3, 1),
(124, 62, 3, 1),
(125, 63, 3, 1),
(126, 63, 4, 1),
(127, 64, 2, 1),
(128, 64, 3, 1),
(129, 65, 3, 1),
(130, 65, 4, 1),
(131, 66, 3, 1),
(132, 67, 1, 1),
(133, 67, 4, 1),
(134, 68, 2, 1),
(135, 68, 3, 1),
(136, 69, 2, 1),
(137, 70, 1, 1),
(138, 70, 2, 1),
(139, 70, 3, 1),
(140, 70, 4, 1),
(141, 71, 1, 1),
(142, 71, 2, 1),
(143, 71, 3, 1),
(144, 72, 3, 1),
(145, 73, 3, 1),
(146, 73, 4, 1),
(147, 74, 2, 1),
(148, 74, 3, 1),
(149, 75, 3, 1),
(150, 75, 4, 1),
(151, 76, 3, 1),
(152, 77, 1, 1),
(153, 77, 4, 1),
(154, 78, 2, 1),
(155, 78, 3, 1),
(156, 79, 2, 1),
(157, 80, 1, 1),
(158, 80, 2, 1),
(159, 80, 3, 1),
(160, 80, 4, 1),
(161, 81, 1, 1),
(162, 81, 2, 1),
(163, 81, 3, 1),
(164, 82, 3, 1),
(165, 83, 3, 1),
(166, 83, 4, 1),
(167, 84, 2, 1),
(168, 84, 3, 1),
(169, 85, 3, 1),
(170, 85, 4, 1),
(171, 86, 3, 1),
(172, 87, 1, 1),
(173, 87, 4, 1),
(174, 88, 2, 1),
(175, 88, 3, 1),
(176, 89, 2, 1),
(177, 90, 1, 1),
(178, 90, 2, 1),
(179, 90, 3, 1),
(180, 90, 4, 1),
(181, 91, 1, 1),
(182, 91, 2, 1),
(183, 91, 3, 1),
(184, 92, 3, 1),
(185, 93, 3, 1),
(186, 93, 4, 1),
(187, 94, 2, 1),
(188, 94, 3, 1),
(189, 95, 3, 1),
(190, 95, 4, 1),
(191, 96, 3, 1),
(192, 97, 1, 1),
(193, 97, 4, 1),
(194, 98, 2, 1),
(195, 98, 3, 1),
(196, 99, 2, 1),
(197, 100, 1, 1),
(198, 100, 2, 1),
(199, 100, 3, 1),
(200, 100, 4, 1),
(201, 101, 1, 1),
(202, 101, 2, 1),
(203, 101, 3, 1),
(204, 102, 3, 1),
(205, 103, 3, 1),
(206, 103, 4, 1),
(207, 104, 2, 1),
(208, 104, 3, 1),
(209, 105, 3, 1),
(210, 105, 4, 1),
(211, 106, 3, 1),
(212, 107, 1, 1),
(213, 107, 4, 1),
(214, 108, 2, 1),
(215, 108, 3, 1),
(216, 109, 2, 1),
(217, 110, 1, 1),
(218, 110, 2, 1),
(219, 110, 3, 1),
(220, 110, 4, 1),
(221, 111, 1, 1),
(222, 111, 2, 1),
(223, 111, 3, 1),
(224, 112, 3, 1),
(225, 113, 3, 1),
(226, 113, 4, 1),
(227, 114, 2, 1),
(228, 114, 3, 1),
(229, 115, 3, 1),
(230, 115, 4, 1),
(231, 116, 3, 1),
(232, 117, 1, 1),
(233, 117, 4, 1),
(234, 118, 2, 1),
(235, 118, 3, 1),
(236, 119, 2, 1),
(237, 120, 1, 1),
(238, 120, 2, 1),
(239, 120, 3, 1),
(240, 120, 4, 1),
(241, 121, 1, 1),
(242, 121, 2, 1),
(243, 121, 3, 1),
(244, 122, 3, 1),
(245, 123, 3, 1),
(246, 123, 4, 1),
(247, 124, 2, 1),
(248, 124, 3, 1),
(249, 125, 3, 1),
(250, 125, 4, 1),
(251, 126, 3, 1),
(252, 127, 1, 1),
(253, 127, 4, 1),
(254, 128, 2, 1),
(255, 128, 3, 1),
(256, 129, 2, 1),
(257, 130, 1, 1),
(258, 130, 2, 1),
(259, 130, 3, 1),
(260, 130, 4, 1),
(261, 131, 2, 1),
(262, 131, 3, 1),
(263, 131, 4, 1),
(264, 132, 2, 1),
(265, 132, 3, 1),
(266, 133, 2, 1),
(267, 133, 3, 1),
(268, 133, 4, 1),
(269, 134, 2, 1),
(270, 134, 3, 1),
(271, 135, 2, 1),
(272, 135, 3, 1),
(273, 135, 4, 1),
(274, 136, 2, 1),
(275, 136, 3, 1),
(276, 136, 4, 1),
(277, 137, 2, 1),
(278, 137, 3, 1),
(279, 138, 2, 1),
(280, 138, 3, 1),
(281, 139, 2, 1),
(282, 139, 3, 1),
(283, 139, 4, 1),
(284, 140, 2, 1),
(285, 140, 3, 1),
(286, 141, 2, 1),
(287, 141, 3, 1),
(288, 141, 4, 1),
(289, 142, 2, 1),
(290, 142, 3, 1),
(291, 143, 2, 1),
(292, 143, 3, 1),
(293, 143, 4, 1),
(294, 144, 2, 1),
(295, 144, 3, 1),
(296, 144, 4, 1),
(297, 145, 2, 1),
(298, 145, 3, 1),
(299, 146, 2, 1),
(300, 146, 3, 1),
(301, 147, 2, 1),
(302, 147, 3, 1),
(303, 147, 4, 1),
(304, 148, 2, 1),
(305, 148, 3, 1),
(306, 149, 2, 1),
(307, 149, 3, 1),
(308, 149, 4, 1),
(309, 150, 2, 1),
(310, 150, 3, 1),
(311, 151, 2, 1),
(312, 151, 3, 1),
(313, 151, 4, 1),
(314, 152, 2, 1),
(315, 152, 3, 1),
(316, 152, 4, 1),
(317, 153, 2, 1),
(318, 153, 3, 1),
(319, 154, 2, 1),
(320, 154, 3, 1),
(321, 155, 2, 1),
(322, 155, 3, 1),
(323, 155, 4, 1),
(324, 156, 2, 1),
(325, 156, 3, 1),
(326, 157, 2, 1),
(327, 157, 3, 1),
(328, 157, 4, 1),
(329, 158, 2, 1),
(330, 158, 3, 1),
(331, 159, 2, 1),
(332, 159, 3, 1),
(333, 159, 4, 1),
(334, 160, 2, 1),
(335, 160, 3, 1),
(336, 160, 4, 1),
(337, 161, 2, 1),
(338, 161, 3, 1),
(339, 162, 2, 1),
(340, 162, 3, 1),
(341, 163, 2, 1),
(342, 163, 3, 1),
(343, 163, 4, 1),
(344, 164, 2, 1),
(345, 164, 3, 1),
(346, 165, 2, 1),
(347, 165, 3, 1),
(348, 165, 4, 1),
(349, 166, 2, 1),
(350, 166, 3, 1),
(351, 167, 2, 1),
(352, 167, 3, 1),
(353, 167, 4, 1),
(354, 168, 2, 1),
(355, 168, 3, 1),
(356, 168, 4, 1),
(357, 169, 2, 1),
(358, 169, 3, 1),
(359, 170, 2, 1),
(360, 170, 3, 1),
(361, 171, 2, 1),
(362, 171, 3, 1),
(363, 171, 4, 1),
(364, 172, 2, 1),
(365, 172, 3, 1),
(366, 173, 2, 1),
(367, 173, 3, 1),
(368, 173, 4, 1),
(369, 174, 2, 1),
(370, 174, 3, 1),
(371, 175, 2, 1),
(372, 175, 3, 1),
(373, 175, 4, 1),
(374, 176, 2, 1),
(375, 176, 3, 1),
(376, 176, 4, 1),
(377, 177, 2, 1),
(378, 177, 3, 1),
(379, 178, 2, 1),
(380, 178, 3, 1),
(381, 179, 2, 1),
(382, 179, 3, 1),
(383, 179, 4, 1),
(384, 180, 2, 1),
(385, 180, 3, 1),
(386, 181, 2, 1),
(387, 181, 3, 1),
(388, 181, 4, 1),
(389, 182, 2, 1),
(390, 182, 3, 1),
(391, 183, 2, 1),
(392, 183, 3, 1),
(393, 183, 4, 1),
(394, 184, 2, 1),
(395, 184, 3, 1),
(396, 184, 4, 1),
(397, 185, 2, 1),
(398, 185, 3, 1),
(399, 186, 2, 1),
(400, 186, 3, 1),
(401, 187, 2, 1),
(402, 187, 3, 1),
(403, 187, 4, 1),
(404, 188, 2, 1),
(405, 188, 3, 1),
(406, 189, 2, 1),
(407, 189, 3, 1),
(408, 189, 4, 1),
(409, 190, 2, 1),
(410, 190, 3, 1),
(411, 191, 2, 1),
(412, 191, 3, 1),
(413, 191, 4, 1),
(414, 192, 2, 1),
(415, 192, 3, 1),
(416, 192, 4, 1),
(417, 193, 2, 1),
(418, 193, 3, 1),
(419, 194, 2, 1),
(420, 194, 3, 1),
(421, 195, 2, 1),
(422, 195, 3, 1),
(423, 195, 4, 1),
(424, 196, 2, 1),
(425, 196, 3, 1),
(426, 197, 2, 1),
(427, 197, 3, 1),
(428, 197, 4, 1),
(429, 198, 2, 1),
(430, 198, 3, 1),
(431, 199, 2, 1),
(432, 199, 3, 1),
(433, 199, 4, 1),
(434, 200, 2, 1),
(435, 200, 3, 1),
(436, 200, 4, 1),
(437, 201, 2, 1),
(438, 201, 3, 1),
(439, 202, 2, 1),
(440, 202, 3, 1),
(441, 203, 2, 1),
(442, 203, 3, 1),
(443, 203, 4, 1),
(444, 204, 2, 1),
(445, 204, 3, 1),
(446, 205, 2, 1),
(447, 205, 3, 1),
(448, 205, 4, 1),
(449, 206, 2, 1),
(450, 206, 3, 1),
(451, 207, 2, 1),
(452, 207, 3, 1),
(453, 207, 4, 1),
(454, 208, 2, 1),
(455, 208, 3, 1),
(456, 208, 4, 1),
(457, 209, 2, 1),
(458, 209, 3, 1),
(459, 210, 2, 1),
(460, 210, 3, 1),
(461, 211, 2, 1),
(462, 211, 3, 1),
(463, 211, 4, 1),
(464, 212, 2, 1),
(465, 212, 3, 1),
(466, 213, 2, 1),
(467, 213, 3, 1),
(468, 213, 4, 1),
(469, 214, 2, 1),
(470, 214, 3, 1),
(471, 215, 2, 1),
(472, 215, 3, 1),
(473, 215, 4, 1),
(474, 216, 2, 1),
(475, 216, 3, 1),
(476, 216, 4, 1),
(477, 217, 2, 1),
(478, 217, 3, 1),
(479, 218, 2, 1),
(480, 218, 3, 1),
(481, 219, 2, 1),
(482, 219, 3, 1),
(483, 219, 4, 1),
(484, 220, 2, 1),
(485, 220, 3, 1),
(486, 221, 2, 1),
(487, 221, 3, 1),
(488, 221, 4, 1),
(489, 222, 2, 1),
(490, 222, 3, 1),
(491, 223, 2, 1),
(492, 223, 3, 1),
(493, 223, 4, 1),
(494, 224, 2, 1),
(495, 224, 3, 1),
(496, 224, 4, 1),
(497, 225, 2, 1),
(498, 225, 3, 1),
(499, 226, 2, 1),
(500, 226, 3, 1),
(501, 227, 2, 1),
(502, 227, 3, 1),
(503, 227, 4, 1),
(504, 228, 2, 1),
(505, 228, 3, 1),
(506, 229, 2, 1),
(507, 229, 3, 1),
(508, 229, 4, 1),
(509, 230, 2, 1),
(510, 230, 3, 1),
(511, 231, 2, 1),
(512, 231, 3, 1),
(513, 231, 4, 1),
(514, 232, 2, 1),
(515, 232, 3, 1),
(516, 232, 4, 1),
(517, 233, 2, 1),
(518, 233, 3, 1),
(519, 234, 2, 1),
(520, 234, 3, 1),
(521, 235, 2, 1),
(522, 235, 3, 1),
(523, 235, 4, 1),
(524, 236, 2, 1),
(525, 236, 3, 1),
(526, 237, 2, 1),
(527, 237, 3, 1),
(528, 237, 4, 1),
(529, 238, 2, 1),
(530, 238, 3, 1),
(531, 239, 2, 1),
(532, 239, 3, 1),
(533, 239, 4, 1),
(534, 240, 2, 1),
(535, 240, 3, 1),
(536, 240, 4, 1),
(537, 241, 2, 1),
(538, 241, 3, 1),
(539, 242, 2, 1),
(540, 242, 3, 1),
(541, 243, 2, 1),
(542, 243, 3, 1),
(543, 243, 4, 1),
(544, 244, 2, 1),
(545, 244, 3, 1),
(546, 245, 2, 1),
(547, 245, 3, 1),
(548, 245, 4, 1),
(549, 246, 2, 1),
(550, 246, 3, 1),
(551, 247, 2, 1),
(552, 247, 3, 1),
(553, 247, 4, 1),
(554, 248, 2, 1),
(555, 248, 3, 1),
(556, 248, 4, 1),
(557, 249, 2, 1),
(558, 249, 3, 1),
(559, 250, 2, 1),
(560, 250, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_employee`
--

CREATE TABLE `tbl_employee` (
  `emp_tbl_id` int(11) NOT NULL,
  `emp_branch_id` int(11) NOT NULL,
  `emp_name` varchar(50) DEFAULT NULL,
  `emp_main_id` varchar(50) DEFAULT '100',
  `emp_email` varchar(50) DEFAULT NULL,
  `emp_password` varchar(50) DEFAULT NULL,
  `emp_contact` bigint(20) DEFAULT NULL,
  `emp_country` int(11) DEFAULT NULL,
  `emp_state` int(11) DEFAULT NULL,
  `emp_city` int(11) DEFAULT NULL,
  `emp_location` int(11) DEFAULT NULL,
  `emp_status` tinyint(1) DEFAULT '1' COMMENT '0,1,2',
  `emp_createdon` datetime DEFAULT CURRENT_TIMESTAMP,
  `emp_modifiedon` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_employee`
--

INSERT INTO `tbl_employee` (`emp_tbl_id`, `emp_branch_id`, `emp_name`, `emp_main_id`, `emp_email`, `emp_password`, `emp_contact`, `emp_country`, `emp_state`, `emp_city`, `emp_location`, `emp_status`, `emp_createdon`, `emp_modifiedon`) VALUES
(1, 1, 'Paramjit Singh Gupta', '100', 'paramjit@chiliadprocons.in', 'fb5e44527eb0b192bcf5a88130078282', 7045920320, NULL, NULL, NULL, NULL, 1, '2017-07-12 06:09:42', NULL),
(2, 1, 'Rajesh Lad', '100', 'rajesh12mom@gmail.com', 'bf44e33d9745e04551770c7a5a6cdb3b', 9930969882, NULL, NULL, NULL, NULL, 2, '2017-07-12 19:48:32', NULL),
(3, 1, 'Dipesh', '100', 'ptldipesh26@gmail.com', '76b73a45824e89f685552958c5c5ad43', 0, NULL, NULL, NULL, NULL, 1, '2017-07-12 19:52:32', NULL),
(4, 1, 'Abhishek Gangan', '100', 'abhishekgangan72@gmail.com', '167784d36ab99e49738fe6a6a98798b7', 8652613779, NULL, NULL, NULL, NULL, 1, '2017-07-12 19:54:01', NULL),
(5, 1, 'Priyanka Gupta', '100', 'priyankagupta12361@gmail.com', '67d72a3ff25da59be64a5d2ab3e36f07', 0, NULL, NULL, NULL, NULL, 1, '2017-07-12 19:55:10', NULL),
(6, 1, 'Anita Gangawane', '100', 'mumbai@chiliadprocons.in', 'cf0761a0e0eb3fa006dc5dd9844736b0', 9004871082, NULL, NULL, NULL, NULL, 0, '2017-07-12 19:56:53', NULL),
(7, 1, 'bivek', '100', 'bivek@mindztechnology.com', '96e79218965eb72c92a549dd5a330112', 9810668829, NULL, NULL, NULL, NULL, 2, '2017-07-12 23:47:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_groupbooking`
--

CREATE TABLE `tbl_groupbooking` (
  `bookingid` int(11) NOT NULL,
  `bookingType` varchar(255) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `email_address` varchar(255) DEFAULT NULL,
  `phone_number` int(11) DEFAULT NULL,
  `departDate` varchar(20) DEFAULT NULL,
  `sessionTime` varchar(20) DEFAULT NULL,
  `number_people` int(11) DEFAULT NULL,
  `socialGroupRadio` varchar(255) DEFAULT NULL,
  `socialGroupName` varchar(255) DEFAULT NULL,
  `addeddate` datetime DEFAULT NULL,
  `modifieddate` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `stateid` int(11) NOT NULL,
  `cityid` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `locationid` int(11) NOT NULL,
  `countryid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_location`
--

CREATE TABLE `tbl_location` (
  `locationid` int(11) NOT NULL,
  `cityid` int(11) NOT NULL,
  `locationname` varchar(255) NOT NULL,
  `lstatus` varchar(20) NOT NULL COMMENT '0,1,2',
  `createdon` datetime NOT NULL,
  `modifiedon` datetime NOT NULL,
  `countryid` int(11) NOT NULL,
  `stateid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_location`
--

INSERT INTO `tbl_location` (`locationid`, `cityid`, `locationname`, `lstatus`, `createdon`, `modifiedon`, `countryid`, `stateid`) VALUES
(1, 1, 'Kurla', '1', '2017-07-12 00:00:00', '0000-00-00 00:00:00', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_orderaddone`
--

CREATE TABLE `tbl_orderaddone` (
  `orderaddoneid` int(11) NOT NULL,
  `addonename` varchar(255) NOT NULL,
  `addonevisiter` int(11) NOT NULL,
  `addoneprice` float NOT NULL,
  `addonqty` int(11) NOT NULL,
  `lastid` int(11) NOT NULL,
  `addonid` int(11) NOT NULL,
  `Type` enum('Package','Addone') NOT NULL,
  `orderaddon_print_status` tinyint(4) NOT NULL DEFAULT '0',
  `orderaddon_print_quantity` int(11) NOT NULL DEFAULT '0',
  `addon_image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_orderaddone`
--

INSERT INTO `tbl_orderaddone` (`orderaddoneid`, `addonename`, `addonevisiter`, `addoneprice`, `addonqty`, `lastid`, `addonid`, `Type`, `orderaddon_print_status`, `orderaddon_print_quantity`, `addon_image`) VALUES
(1, 'Socks', 2, 40, 1, 1, 1, '', 0, 0, 'e4e5980d06eb28833dcf6320e23fc026.png'),
(2, 'Tea', 2, 30, 1, 1, 2, '', 0, 0, '2f1cfe2cf9d10a01a7ae79384ec06edf.png'),
(3, 'Soft Toy', 2, 40, 1, 1, 3, '', 0, 0, '3f68d950808b298465c03f3efc741559.png'),
(4, 'Socks', 2, 40, 1, 2, 1, '', 0, 0, 'e4e5980d06eb28833dcf6320e23fc026.png'),
(5, 'Tea', 2, 30, 1, 2, 2, '', 0, 0, '2f1cfe2cf9d10a01a7ae79384ec06edf.png'),
(6, 'Soft Toy', 2, 40, 1, 2, 3, '', 0, 0, '3f68d950808b298465c03f3efc741559.png'),
(7, 'Socks', 2, 40, 1, 3, 1, '', 0, 0, 'e4e5980d06eb28833dcf6320e23fc026.png'),
(8, 'Tea', 2, 30, 1, 3, 2, '', 0, 0, '2f1cfe2cf9d10a01a7ae79384ec06edf.png'),
(9, 'Soft Toy', 2, 40, 1, 3, 3, '', 0, 0, '3f68d950808b298465c03f3efc741559.png'),
(10, 'Socks', 2, 40, 1, 4, 1, '', 0, 0, 'e4e5980d06eb28833dcf6320e23fc026.png'),
(11, 'Tea', 2, 30, 1, 4, 2, '', 0, 0, '2f1cfe2cf9d10a01a7ae79384ec06edf.png'),
(12, 'Soft Toy', 2, 40, 1, 4, 3, '', 0, 0, '3f68d950808b298465c03f3efc741559.png'),
(13, 'Socks', 2, 40, 1, 5, 1, '', 0, 0, 'e4e5980d06eb28833dcf6320e23fc026.png'),
(14, 'Tea', 2, 30, 1, 5, 2, '', 0, 0, '2f1cfe2cf9d10a01a7ae79384ec06edf.png'),
(15, 'Soft Toy', 2, 40, 1, 5, 3, '', 0, 0, '3f68d950808b298465c03f3efc741559.png'),
(16, 'Socks', 2, 40, 1, 6, 1, '', 0, 0, 'e4e5980d06eb28833dcf6320e23fc026.png'),
(17, 'Tea', 2, 30, 1, 6, 2, '', 0, 0, '2f1cfe2cf9d10a01a7ae79384ec06edf.png'),
(18, 'Soft Toy', 2, 40, 1, 6, 3, '', 0, 0, '3f68d950808b298465c03f3efc741559.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_orderpackage`
--

CREATE TABLE `tbl_orderpackage` (
  `pacorderid` int(11) NOT NULL,
  `userid` int(11) DEFAULT NULL,
  `billing_by` int(11) NOT NULL,
  `promocodeprice` float DEFAULT NULL,
  `total` float DEFAULT NULL,
  `waletAmount` float DEFAULT NULL,
  `subtotal` int(11) DEFAULT NULL,
  `addedon` datetime DEFAULT NULL,
  `countryid` int(11) DEFAULT NULL,
  `stateid` int(11) DEFAULT NULL,
  `cityid` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `locationid` int(11) DEFAULT NULL,
  `departuredate` varchar(200) DEFAULT NULL,
  `paymentstatus` int(11) DEFAULT '0',
  `paymentmode` varchar(255) DEFAULT 'Pending',
  `ticketid` varchar(255) DEFAULT NULL,
  `tracking_id` varchar(50) DEFAULT NULL,
  `packproductname` tinytext,
  `packimg` tinytext COMMENT 'all package img path in string',
  `package_qty` varchar(100) NOT NULL COMMENT 'all package_id in string',
  `package_price` tinytext NOT NULL COMMENT 'all package_price in string',
  `packpkg` int(11) DEFAULT NULL,
  `packprice` int(11) DEFAULT NULL,
  `orderstatus` int(11) DEFAULT '1',
  `op_ticket_print_status` tinyint(4) DEFAULT '0',
  `op_printed_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `internethandlingcharges` int(11) DEFAULT NULL,
  `op_usertype` varchar(255) DEFAULT 'User',
  `order_status` varchar(50) DEFAULT NULL,
  `status_message` varchar(50) DEFAULT NULL,
  `paymenttype` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `billing_name` varchar(255) DEFAULT NULL,
  `billing_email` varchar(255) DEFAULT NULL,
  `billing_tel` varchar(255) DEFAULT NULL,
  `billing_address` text,
  `billing_city` varchar(255) DEFAULT NULL,
  `billing_state` varchar(255) DEFAULT NULL,
  `billing_zip` varchar(255) DEFAULT NULL,
  `billing_country` varchar(255) DEFAULT NULL,
  `billing_pdf` varchar(255) DEFAULT NULL,
  `ordersucesmail` int(11) DEFAULT NULL,
  `ordermailstatus` int(11) DEFAULT NULL,
  `departuretime` varchar(200) DEFAULT NULL,
  `frommin` varchar(20) DEFAULT NULL,
  `tohrs` varchar(20) DEFAULT NULL,
  `tomin` varchar(20) DEFAULT NULL,
  `txtfromd` varchar(20) DEFAULT NULL,
  `txttod` varchar(20) DEFAULT NULL,
  `p_codename` varchar(255) NOT NULL,
  `op_ordertrackingupdate` int(11) NOT NULL,
  `order_status_date_time` datetime DEFAULT NULL,
  `op_orderjason` int(11) NOT NULL,
  `op_billing_cometoknow` varchar(255) NOT NULL,
  `billing_promocode` varchar(255) NOT NULL,
  `order_statuspayment` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_orderpackage`
--

INSERT INTO `tbl_orderpackage` (`pacorderid`, `userid`, `billing_by`, `promocodeprice`, `total`, `waletAmount`, `subtotal`, `addedon`, `countryid`, `stateid`, `cityid`, `branch_id`, `locationid`, `departuredate`, `paymentstatus`, `paymentmode`, `ticketid`, `tracking_id`, `packproductname`, `packimg`, `package_qty`, `package_price`, `packpkg`, `packprice`, `orderstatus`, `op_ticket_print_status`, `op_printed_date`, `internethandlingcharges`, `op_usertype`, `order_status`, `status_message`, `paymenttype`, `title`, `billing_name`, `billing_email`, `billing_tel`, `billing_address`, `billing_city`, `billing_state`, `billing_zip`, `billing_country`, `billing_pdf`, `ordersucesmail`, `ordermailstatus`, `departuretime`, `frommin`, `tohrs`, `tomin`, `txtfromd`, `txttod`, `p_codename`, `op_ordertrackingupdate`, `order_status_date_time`, `op_orderjason`, `op_billing_cometoknow`, `billing_promocode`, `order_statuspayment`) VALUES
(1, 12, 7, 100, 1605, 1605, 1705, '2018-01-10 11:28:30', 1, 1, 1, 1, 1, '11-01-2018', 1, '0', '5a55ab8616296', 'NULL', 'Combo#Family', 'af264b35318be1024a8eec44cad64b0d.jpg#94a40382f521aaff15c482d582b07ca8.png', '1#1', '1000#545', 2, 1545, 1, 0, '0000-00-00 00:00:00', 50, 'Partner', 'NULL', 'NULL', 'NULL', 'Mr', 'Raju Gupta', 'raju@gmail.com', '9810668829', 'MANTRIPUKHEI,NEAR RPF CAMP,IMPHAL', 'IMPHAL', 'MANIPUR', '323233', 'India', '', 0, 0, '04', '30', '05', '30', 'PM', 'PM', 'RAJU', 0, '2018-01-10 11:28:30', 0, 'test', '', ''),
(2, 13, 7, 100, 1605, 1605, 1705, '2018-01-10 11:30:34', 1, 1, 1, 1, 1, '11-01-2018', 1, '0', '5a55ac02a3751', 'NULL', 'Combo#Family', 'af264b35318be1024a8eec44cad64b0d.jpg#94a40382f521aaff15c482d582b07ca8.png', '1#1', '1000#545', 2, 1545, 1, 0, '0000-00-00 00:00:00', 50, 'Partner', 'NULL', 'NULL', 'NULL', 'Mr', 'Raju Gupta', 'raju@gmail.com', '9810668829', 'MANTRIPUKHEI,NEAR RPF CAMP,IMPHAL', 'IMPHAL', 'MANIPUR', '323233', 'India', '', 0, 0, '04', '30', '05', '30', 'PM', 'PM', 'RAJU', 0, '2018-01-10 11:30:34', 0, 'test', '', ''),
(3, 14, 7, 100, 1605, 1605, 1705, '2018-01-10 12:00:00', 1, 1, 1, 1, 1, '10-01-2018', 1, '0', '5a55b2e8247af', 'NULL', 'Combo#Family', 'af264b35318be1024a8eec44cad64b0d.jpg#94a40382f521aaff15c482d582b07ca8.png', '1#1', '1000#545', 2, 1545, 1, 0, '0000-00-00 00:00:00', 50, 'Partner', 'NULL', 'NULL', 'NULL', 'Mr', 'Raju Gupta', 'raju@gmail.com', '9810668829', 'MANTRIPUKHEI,NEAR RPF CAMP,IMPHAL', 'IMPHAL', 'MANIPUR', '222222', 'India', '', 0, 0, '04', '30', '05', '30', 'PM', 'PM', 'RAJU', 0, '2018-01-10 12:00:00', 0, 'xxx', '', ''),
(4, 3, 7, 100, 1605, 1605, 1705, '2018-01-10 12:07:50', 1, 1, 1, 1, 1, '10-01-2018', 1, '0', '5a55b4bee5400', 'NULL', 'Combo#Family', 'af264b35318be1024a8eec44cad64b0d.jpg#94a40382f521aaff15c482d582b07ca8.png', '1#1', '1000#545', 2, 1545, 1, 0, '0000-00-00 00:00:00', 50, 'Partner', 'NULL', 'NULL', 'NULL', 'Mr', 'Raju Gupta', 'raju@gmail.com', '9810668829', 'MANTRIPUKHEI,NEAR RPF CAMP,IMPHAL', 'IMPHAL', 'MANIPUR', '222222', 'India', '', 0, 0, '04', '30', '05', '30', 'PM', 'PM', 'RAJU', 0, '2018-01-10 12:07:50', 0, 'xxx', '', ''),
(5, 4, 7, 100, 1605, 1605, 1705, '2018-01-10 12:10:16', 1, 1, 1, 1, 1, '10-01-2018', 1, '0', '5a55b550c03be', 'NULL', 'Combo#Family', 'af264b35318be1024a8eec44cad64b0d.jpg#94a40382f521aaff15c482d582b07ca8.png', '1#1', '1000#545', 2, 1545, 1, 0, '0000-00-00 00:00:00', 50, 'Partner', 'NULL', 'NULL', 'NULL', 'Mr', 'Raju Gupta', 'raju@gmail.com', '9810668829', 'MANTRIPUKHEI,NEAR RPF CAMP,IMPHAL', 'IMPHAL', 'MANIPUR', '222222', 'India', '', 0, 0, '04', '30', '05', '30', 'PM', 'PM', 'RAJU', 0, '2018-01-10 12:10:16', 0, 'xxx', '', ''),
(6, 5, 7, 100, 1605, 1605, 1705, '2018-01-10 12:55:38', 1, 1, 1, 1, 1, '10-01-2018', 1, '0', '5a55bff24dd76', 'NULL', 'Combo#Family', 'af264b35318be1024a8eec44cad64b0d.jpg#94a40382f521aaff15c482d582b07ca8.png', '1#1', '1000#545', 2, 1545, 1, 0, '0000-00-00 00:00:00', 50, 'Partner', 'NULL', 'NULL', 'NULL', 'Mr', 'Raju Gupta', 'raju@gmail.com', '9810668829', 'MANTRIPUKHEI,NEAR RPF CAMP,IMPHAL', 'IMPHAL', 'MANIPUR', '777888', 'India', '', 0, 0, '04', '30', '05', '30', 'PM', 'PM', 'RAJU', 0, '2018-01-10 12:55:38', 0, 'FF', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_packagevalue`
--

CREATE TABLE `tbl_packagevalue` (
  `packagevalueid` int(11) NOT NULL,
  `packagenameses` varchar(255) NOT NULL,
  `package_priceses` int(11) NOT NULL,
  `lastidses` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_package_master`
--

CREATE TABLE `tbl_package_master` (
  `package_id` int(11) NOT NULL,
  `package_name` varchar(50) DEFAULT NULL,
  `package_description` text,
  `package_price` float NOT NULL,
  `package_image` varchar(255) NOT NULL,
  `package_for` int(11) NOT NULL,
  `package_createdon` datetime DEFAULT CURRENT_TIMESTAMP,
  `package_modifiedon` datetime DEFAULT NULL,
  `package_status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_package_master`
--

INSERT INTO `tbl_package_master` (`package_id`, `package_name`, `package_description`, `package_price`, `package_image`, `package_for`, `package_createdon`, `package_modifiedon`, `package_status`) VALUES
(1, 'Regular', 'It includes all the Activities (Ice Skating, Snow Fall/Dance, Ski Boarding, Ice Sliding & Types of Sledding), Attractions (Snow Tunnel, Scenic Mountain, Wooden Hut, Roman Culture wall, Igloos & many more) and the Attire (Jackets, Hoods, Gloves and Snow Boots).', 500, '1c73516272f31851c4b2e9c2a09f7957.jpg', 0, '2017-07-12 05:35:09', NULL, 1),
(2, 'school', 'schools (Ice Skating, Snow Fall/Dance, Ski Boarding, Ice Sliding & Types of Sledding), Attractions (Snow Tunnel, Scenic Mountain, Wooden Hut, Roman Culture wall, Igloos & many more) and the Attire (Jackets, Hoods, Gloves and Snow Boots).It includes all the Activities It includes all the Activities (Ice Skating, Snow Fall/Dance, Ski Boarding, Ice Sliding & Types of Sledding), Attractions (Snow Tunnel, Scenic Mountain, Wooden Hut, Roman Culture wall, Igloos & many more) and the Attire (Jackets, Ho', 900, 'bbd8de3922e1259a8db4580d46ab64c7.jpg', 0, '2017-11-23 12:53:38', NULL, 1),
(3, 'Family', 'Family - include 2 adult, 4 childrenIt includes all the Activities (Ice Skating, Snow Fall/Dance, Ski Boarding, Ice Sliding & Types of Sledding), Attractions (Snow Tunnel, Scenic Mountain, Wooden Hut, Roman Culture wall, Igloos & many more) and the Attire (Jackets, Hoods, Gloves and Snow Boots).It includes all the Activities (Ice Skating, Snow Fall/Dance, Ski Boarding, Ice Sliding & Types of Sledding), Attractions (Snow Tunnel, Scenic Mountain, Wooden Hut, Roman Culture wall, Igloos & many more)', 545, '94a40382f521aaff15c482d582b07ca8.png', 0, '2017-11-30 17:52:28', NULL, 1),
(4, 'Combo', 'Combo include 2 adult , 2 childrens (Ice Skating, Snow Fall/Dance, Ski Boarding, Ice Sliding & Types of Sledding), Attractions (Snow Tunnel, Scenic Mountain, Wooden Hut, Roman Culture wall, Igloos & many more) and the Attire (Jackets, Hoods, Gloves and Snow Boots).It includes all the Activities (Ice Skating, Snow Fall/Dance, Ski Boarding, Ice Sliding & Types of Sledding), Attractions (Snow Tunnel, Scenic Mountain, Wooden Hut, Roman Culture wall, Igloos & many more) and the Attire (Jackets, Hoods', 1000, 'af264b35318be1024a8eec44cad64b0d.jpg', 0, '2017-11-30 17:53:29', NULL, 1),
(12, 'fghgfhgf', 'hfghgfhfg', 65465, '40922666d27ad287738aa4a3790b2bae.jpg', 0, '2017-12-23 12:21:00', NULL, 2),
(13, 'hfgjgf', 'hfghfg', 656, '18a51b36739564060632e8135f7a829f.png', 0, '2017-12-23 12:29:25', NULL, 2),
(14, 'ghjghjghjhgjjjhgjgh', 'jhghjghjgngjgjhgjhg', 7656, 'ff6228159aaf6c1e4ecf8d928052dcda.jpeg', 0, '2017-12-23 12:30:02', NULL, 2),
(15, 'fghdfuhg', 'hggfhfgjhjkfhk', 5437, '0e189d4afd9584b0957691fd7ae3893a.png', 0, '2017-12-23 12:37:27', NULL, 2),
(16, 'hgfdhgjhgjgh', 'jghjghjhhjggkfhf', 7000, 'd15d342c54baa859462141b134f3cc91.png', 0, '2017-12-23 13:52:03', NULL, 2),
(17, 'hgujgfohf', '`hjkghkhfjkj', 4455, '3b3cb133fd641eb90a8972257c33d2ad.jpg', 0, '2017-12-23 13:56:01', NULL, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_partner`
--

CREATE TABLE `tbl_partner` (
  `part_id` int(11) NOT NULL,
  `emaiid` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `createdon` datetime DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL,
  `type` enum('Corporate Login','Officer Login') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_payment_gatway`
--

CREATE TABLE `tbl_payment_gatway` (
  `pg_id` int(11) NOT NULL,
  `pg_branchid` int(11) NOT NULL,
  `pg_merchant_id` varchar(50) NOT NULL,
  `pg_access_key` varchar(200) NOT NULL,
  `pg_working_key` varchar(50) NOT NULL,
  `pg_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pg_updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pg_status` tinyint(4) NOT NULL DEFAULT '1',
  `pg_sucess_link` varchar(100) NOT NULL,
  `pg_fail_link` varchar(100) NOT NULL,
  `pg_currency` varchar(50) NOT NULL,
  `pg_language` varchar(50) NOT NULL,
  `pg_prefix` varchar(20) NOT NULL,
  `pg_agent_prefix` varchar(20) NOT NULL,
  `pg_order_sucess_url` varchar(100) NOT NULL,
  `pg_order_fail_url` varchar(100) NOT NULL,
  `pg_wallet_sucess_url` varchar(100) NOT NULL,
  `pg_wallet_fail_url` varchar(100) NOT NULL,
  `pg_prefix_agent_wallet` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_payment_gatway`
--

INSERT INTO `tbl_payment_gatway` (`pg_id`, `pg_branchid`, `pg_merchant_id`, `pg_access_key`, `pg_working_key`, `pg_created`, `pg_updated`, `pg_status`, `pg_sucess_link`, `pg_fail_link`, `pg_currency`, `pg_language`, `pg_prefix`, `pg_agent_prefix`, `pg_order_sucess_url`, `pg_order_fail_url`, `pg_wallet_sucess_url`, `pg_wallet_fail_url`, `pg_prefix_agent_wallet`) VALUES
(1, 1, '87391', 'AVKI70EE70AW83IKWA', '2FB6FC47581E0FDEE8D5529758806A65', '2017-07-03 00:50:47', '2017-07-03 05:06:20', 2, 'http://booking.snowworldmumbai.com/ordersucess', 'http://booking.snowworldmumbai.com/orderfail', 'INR', 'EN', 'snowworldmumbai.', '', '', '', '', '', ''),
(2, 1, '49646', 'AVUS71EG70AF13SUFA', 'B01CEF5819806E9315FF0CADC5D94051', '2017-07-03 04:46:02', '2018-01-05 11:21:40', 1, 'http://192.168.1.65/snowworldnew/ordersucess', 'http://192.168.1.65/snowworldnew/orderfail', 'INR', 'EN', 'Snow', 'Snowagentorder', 'http://192.168.1.65/snowworldnew/agentsucess', 'http://192.168.1.65/snowworldnew/fail', 'http://192.168.1.65/snowworldnew/agentamountwalletsucess', 'http://192.168.1.65/snowworldnew/agentamountwalletfail', 'Snowagentwallet'),
(4, 1, '49648', 'AVUS71EG70AF13SUFC', 'B01CEF5819806E9315FF0CADC5D94052', '2017-12-06 14:35:19', '2017-12-06 14:54:20', 0, 'http://booking.snowworldmumbai.com/ordersucess', 'http://booking.snowworldmumbai.com/orderfail', 'INR', 'EN', 'Snow', 'agent_snow', '', '', '', '', ''),
(10, 1, '6575', 'access key', 'working key', '2018-01-03 14:27:27', '2018-01-03 18:29:33', 0, 'success url', 'fail url', '555', 'Language', 'Prefix', 'Agent Order Prefix', 'order suceess url', 'order fail url', 'wallet suceess url', 'wallet fail url', 'jghjghjghjghjg'),
(11, 1, '765765756', 'gfdgllk', 'gdfgdfgdfg', '2018-01-03 15:26:39', '2018-01-03 18:39:42', 2, 'jjgjkfldjghklj', 'jkhjghklj', '1000', 'ghjdfhgjdfhhgfhgfhgf', 'ghdfgdlkfgjgdfgfdghh', 'jghkjhlgflgfdgfdgffd', 'jhkgjhlj', 'jhkgfjhklfj', 'jkjkhgjfj', 'jhkgfjhkfgkhgfhgfhgfh', 'fgjsdfkflgjldfgdfgdgdfgfgfd');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_promo`
--

CREATE TABLE `tbl_promo` (
  `promo_id` int(11) NOT NULL,
  `p_locationid` int(11) DEFAULT NULL,
  `p_branchid` int(11) DEFAULT NULL,
  `p_codename` varchar(255) DEFAULT NULL,
  `p_type` text,
  `p_discount` int(255) DEFAULT NULL,
  `p_status` tinyint(1) DEFAULT NULL,
  `p_expiry_date` varchar(50) DEFAULT NULL,
  `p_allowed_per_user` int(11) DEFAULT NULL,
  `p_start_date` varchar(50) DEFAULT NULL,
  `p_allowed_times` int(11) DEFAULT NULL,
  `p_createdon` datetime DEFAULT CURRENT_TIMESTAMP,
  `p_modifiedon` datetime DEFAULT NULL,
  `p_logintype` int(11) DEFAULT '0',
  `p_addonedata` text NOT NULL,
  `p_addoneqty` text NOT NULL,
  `p_usertype` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_promo`
--

INSERT INTO `tbl_promo` (`promo_id`, `p_locationid`, `p_branchid`, `p_codename`, `p_type`, `p_discount`, `p_status`, `p_expiry_date`, `p_allowed_per_user`, `p_start_date`, `p_allowed_times`, `p_createdon`, `p_modifiedon`, `p_logintype`, `p_addonedata`, `p_addoneqty`, `p_usertype`) VALUES
(4, NULL, 1, 'V10', 'flat', 10, 1, '2017-12-26', 0, '2017-12-22', 0, '2017-12-21 11:57:54', NULL, 0, '1', '1', 0),
(5, NULL, 1, 'SNOW33', 'flat', 50, 1, '2017-12-31', 0, '2017-12-22', 0, '2017-12-22 13:01:05', NULL, 3, '1,2,3', '1', 0),
(13, NULL, 1, 'KOIMOI', 'flat', 10, 1, '2017-12-28', 0, '2017-12-26', 0, '2017-12-26 14:52:39', NULL, 3, '1,2', '1', 3),
(14, NULL, 1, 'RAJU', 'flat', 100, 1, '2018-02-28', 0, '2017-12-30', 0, '2017-12-30 14:09:48', NULL, 3, '1,2,3', '1', 3);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_promopackage`
--

CREATE TABLE `tbl_promopackage` (
  `promopackage_id` int(11) NOT NULL,
  `promopackagepromo_id` int(11) DEFAULT NULL,
  `promopackagedailyinventory_id` int(11) DEFAULT NULL,
  `promopackage_createddate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `promopackage_timesallowed` int(11) DEFAULT NULL,
  `promopackage_peruserallowed` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_promopackage`
--

INSERT INTO `tbl_promopackage` (`promopackage_id`, `promopackagepromo_id`, `promopackagedailyinventory_id`, `promopackage_createddate`, `promopackage_timesallowed`, `promopackage_peruserallowed`) VALUES
(1, 14, 61, '2018-01-09 05:52:31', 100, 20),
(2, 14, 62, '2018-01-09 05:52:31', 100, 20),
(3, 14, 63, '2018-01-09 05:52:31', 100, 20),
(4, 14, 64, '2018-01-09 05:52:31', 100, 20),
(5, 14, 65, '2018-01-09 05:52:31', 100, 20),
(6, 14, 66, '2018-01-09 05:52:31', 100, 20),
(7, 14, 67, '2018-01-09 05:52:31', 100, 20),
(8, 14, 68, '2018-01-09 05:52:31', 100, 20),
(9, 14, 69, '2018-01-09 05:52:32', 100, 20),
(10, 14, 70, '2018-01-09 05:52:32', 100, 20),
(11, 14, 71, '2018-01-09 05:52:32', 100, 20),
(12, 14, 72, '2018-01-09 05:52:32', 100, 20),
(13, 14, 73, '2018-01-09 05:52:32', 100, 20),
(14, 14, 74, '2018-01-09 05:52:32', 100, 20),
(15, 14, 75, '2018-01-09 05:52:32', 100, 20),
(16, 14, 76, '2018-01-09 05:52:32', 100, 20),
(17, 14, 77, '2018-01-09 05:52:32', 100, 20),
(18, 14, 78, '2018-01-09 05:52:32', 100, 20),
(19, 14, 79, '2018-01-09 05:52:32', 100, 20),
(20, 14, 80, '2018-01-09 05:52:32', 100, 20);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_seoskiindia`
--

CREATE TABLE `tbl_seoskiindia` (
  `seo_id` bigint(20) NOT NULL,
  `seo_pagename` varchar(500) DEFAULT NULL,
  `seo_meta_keyword` text,
  `seo_meta_title` text,
  `seo_meta_desc` text,
  `seo_meta_status` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sms_gatway`
--

CREATE TABLE `tbl_sms_gatway` (
  `sms_id` int(11) NOT NULL,
  `sms_branchid` int(11) NOT NULL,
  `sms_username` varchar(255) NOT NULL,
  `sms_password` varchar(255) NOT NULL,
  `sms_created` datetime NOT NULL,
  `sms_updated` datetime NOT NULL,
  `sms_status` int(11) NOT NULL,
  `sms_feedid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_sms_gatway`
--

INSERT INTO `tbl_sms_gatway` (`sms_id`, `sms_branchid`, `sms_username`, `sms_password`, `sms_created`, `sms_updated`, `sms_status`, `sms_feedid`) VALUES
(1, 43, 'jkjjl', 'klk', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 2, 9089089),
(2, 43, 'saa', 'fgfgdfgd', '2017-07-04 11:08:19', '0000-00-00 00:00:00', 2, 676765),
(3, 1, '9867622549', 'mtpjp', '2017-07-04 01:01:44', '0000-00-00 00:00:00', 2, 356637),
(4, 1, '9022112422', 'snow@123', '2017-07-04 23:13:45', '0000-00-00 00:00:00', 1, 34558110),
(5, 1, '9867622549', 'mtpjp', '2017-07-12 20:07:12', '0000-00-00 00:00:00', 2, 356637);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_state`
--

CREATE TABLE `tbl_state` (
  `stateid` bigint(20) NOT NULL,
  `countryid` int(11) NOT NULL,
  `statename` varchar(50) DEFAULT NULL,
  `statecode` varchar(50) NOT NULL,
  `isdeleted` tinyint(4) DEFAULT NULL,
  `createdon` datetime DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_state`
--

INSERT INTO `tbl_state` (`stateid`, `countryid`, `statename`, `statecode`, `isdeleted`, `createdon`, `modifiedon`) VALUES
(1, 1, 'Maharashtra', 'MH', 1, '2017-07-02 23:53:35', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tearm`
--

CREATE TABLE `tbl_tearm` (
  `term_id` int(11) NOT NULL,
  `term_name` text NOT NULL,
  `term_created` date NOT NULL,
  `term_modified` date NOT NULL,
  `term_status` int(11) NOT NULL,
  `tearm_branchid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_tearm`
--

INSERT INTO `tbl_tearm` (`term_id`, `term_name`, `term_created`, `term_modified`, `term_status`, `tearm_branchid`) VALUES
(1, 'Admission ticket permits admission for one individual and needs to be retained at the respective time slot.', '2017-07-04', '2017-12-30', 1, 1),
(2, 'Kindly report at Booking office 30 minutes prior to session starts to collect tickets.', '2017-07-04', '0000-00-00', 1, 1),
(3, 'Children\'s below 2 years are exempted from ticket charges.', '2017-07-04', '0000-00-00', 1, 1),
(4, 'Children\'s below 10 years and senior citizens need to be accompanied by guardian.', '2017-07-04', '0000-00-00', 1, 1),
(5, 'Total Duration: 1 Hour (Snow Room : 45 mins + Reception : 15 mins)', '2017-07-04', '0000-00-00', 1, 1),
(6, 'Parka Jackets, Gloves, Hood and Boots will be provided with each admission.', '2017-07-04', '0000-00-00', 1, 1),
(7, 'All accessories are not of accurate sizes and are subject to availability.', '2017-07-04', '0000-00-00', 1, 1),
(8, 'Tickets once purchased will not be RETURNED /CANCELLED/ REFUNDED back.', '2017-07-04', '0000-00-00', 1, 1),
(9, 'Eatables and beverages of any kind are not allowed into the snow area.', '2017-07-04', '0000-00-00', 1, 1),
(10, 'The following articles are strictly prohibited: Eatables (Including Gutkha, Pan-Masala, Chewing Gum, Chocolate, Chips), Knife, and Cigarette, Lighter, Match-Box, Firearms and all kind of inflammable objects.', '2017-07-04', '0000-00-00', 1, 1),
(11, 'All images & terminology used in all communication is for creative, descriptive and representation purpose only.', '2017-07-04', '0000-00-00', 1, 1),
(12, 'Ticket package, rates and governing conditions are subject to change without notice.', '2017-07-04', '0000-00-00', 1, 1),
(13, 'Ticket package, rates and governing conditions are subject to change without notice.', '2017-07-04', '0000-00-00', 2, 1),
(14, 'Management reserves the right to cancel any session due to unforeseen circumstances. The Management shall not be liable for any losses/ damages arising with such suspension. No two offers can be clubbed together.', '2017-07-04', '0000-00-00', 1, 1),
(15, 'On the purchase of this ticket the holder confirms that he/she has read, understood and agreed to all the terms and conditions in the theme park and voluntarily assumes all the risks.', '2017-07-04', '0000-00-00', 1, 1),
(16, 'Booking on this website authorizes Snow World Mumbai to contact the customer, on the Phone Number provided, for any booking related communication.', '2017-07-04', '0000-00-00', 1, 1),
(17, 'Any purposeful act to damage the environment, surrounding or assets will be fined according to Managements discretion.', '2017-07-04', '0000-00-00', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_timeslot_branch`
--

CREATE TABLE `tbl_timeslot_branch` (
  `timeslot_id` int(11) NOT NULL,
  `timeslot_branchid` int(11) DEFAULT NULL,
  `timeslot_from` varchar(11) DEFAULT NULL,
  `timeslot_to` varchar(11) DEFAULT NULL,
  `timeslot_seats` int(11) DEFAULT NULL,
  `timeslot_status` tinyint(1) DEFAULT NULL,
  `timeslot_createdon` datetime DEFAULT CURRENT_TIMESTAMP,
  `timeslot_modifiedon` datetime DEFAULT NULL,
  `timeslot_minfrom` varchar(11) DEFAULT NULL,
  `timeslot_minto` varchar(11) DEFAULT NULL,
  `timeslot_date` datetime DEFAULT NULL,
  `timeslot_packageid` varchar(200) DEFAULT NULL,
  `timeslot_addonsid` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_timeslot_branch`
--

INSERT INTO `tbl_timeslot_branch` (`timeslot_id`, `timeslot_branchid`, `timeslot_from`, `timeslot_to`, `timeslot_seats`, `timeslot_status`, `timeslot_createdon`, `timeslot_modifiedon`, `timeslot_minfrom`, `timeslot_minto`, `timeslot_date`, `timeslot_packageid`, `timeslot_addonsid`) VALUES
(1, 1, '18', '20', 45, 2, '2017-12-11 18:44:10', NULL, '19', '17', NULL, '2,4', '2,3'),
(2, 1, '1', '16', 4, 2, '2017-12-11 18:44:10', NULL, '14', '16', NULL, NULL, NULL),
(3, 1, '13', '14', 200, 2, '2017-07-03 00:17:09', NULL, '30', '30', NULL, NULL, NULL),
(4, 1, '15', '16', 200, 2, '2017-07-03 00:17:09', NULL, '00', '00', NULL, NULL, NULL),
(5, 1, '16', '17', 200, 2, '2017-07-03 00:17:09', NULL, '30', '30', NULL, NULL, NULL),
(6, 1, '18', '19', 200, 2, '2017-07-03 00:17:09', NULL, '00', '00', NULL, NULL, NULL),
(7, 1, '19', '20', 200, 2, '2017-07-03 00:17:09', NULL, '30', '30', NULL, NULL, NULL),
(8, 1, '21', '22', 200, 2, '2017-07-03 00:17:09', NULL, '00', '00', NULL, NULL, NULL),
(9, 1, '1', '2', 200, 2, '2017-07-05 00:15:39', NULL, '00', '00', NULL, NULL, NULL),
(10, 1, '2', '3', 200, 2, '2017-07-05 00:15:39', NULL, '00', '00', NULL, NULL, NULL),
(11, 1, '11', '12', 150, 1, '2017-07-05 00:23:45', NULL, '00', '00', NULL, '1,2,3', '1'),
(12, 1, '12', '13', 150, 1, '2017-07-05 00:23:45', NULL, '15', '15', NULL, '3', NULL),
(13, 1, '13', '14', 150, 1, '2017-07-05 00:23:45', NULL, '30', '30', NULL, '3,4', NULL),
(14, 1, '15', '16', 150, 1, '2017-07-05 00:23:45', NULL, '00', '00', NULL, '2,3', NULL),
(15, 1, '16', '17', 150, 1, '2017-07-05 00:23:45', NULL, '30', '30', NULL, '3,4', NULL),
(16, 1, '18', '19', 150, 1, '2017-07-05 00:23:45', NULL, '00', '00', NULL, '3', NULL),
(17, 1, '19', '20', 150, 1, '2017-07-05 00:23:45', NULL, '30', '30', NULL, '1,4', NULL),
(18, 1, '21', '20', 0, 2, '2017-07-05 00:23:45', NULL, '00', '00', NULL, NULL, NULL),
(19, 1, '21', '22', 150, 1, '2017-07-15 01:36:55', NULL, '00', '00', NULL, '2,3', NULL),
(20, 1, '19', '18', 67, 1, '2017-12-13 09:58:27', NULL, '18', '18', NULL, '1,2', '1,2'),
(29, 1, '9', '8', 5, 1, '2017-12-13 11:11:02', NULL, '05', '10', NULL, '2', '1'),
(30, 1, '11', '9', 6, 1, '2017-12-13 11:11:02', NULL, '06', '06', NULL, '2,3', '2,3'),
(31, 1, '14', '11', 7, 1, '2017-12-13 11:11:02', NULL, '08', '10', NULL, '1,2,3,4', '1,2,3');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `user_id` int(11) NOT NULL,
  `user_firstname` varchar(255) DEFAULT NULL,
  `user_lastname` varchar(255) DEFAULT NULL,
  `user_emailid` varchar(255) DEFAULT NULL,
  `user_password` varchar(255) DEFAULT NULL,
  `user_mobileno` varchar(255) DEFAULT NULL,
  `user_status` int(11) DEFAULT NULL,
  `user_createdon` datetime DEFAULT NULL,
  `user_modifiedon` datetime DEFAULT NULL,
  `user_Address` text NOT NULL,
  `user_varified` int(11) NOT NULL DEFAULT '0',
  `user_town` varchar(255) NOT NULL,
  `user_zip` varchar(255) NOT NULL,
  `user_country` varchar(255) NOT NULL,
  `user_title` varchar(20) NOT NULL,
  `user_dob` varchar(20) NOT NULL,
  `user_state` varchar(200) NOT NULL,
  `user_city` varchar(200) NOT NULL,
  `user_pincodes` varchar(200) NOT NULL,
  `user_usertype` varchar(255) NOT NULL DEFAULT 'User',
  `oauth_id` varchar(200) NOT NULL,
  `oauth_provider` enum('','facebook','google','twitter') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `oauth_link` varchar(255) NOT NULL,
  `oauth_picture_link` varchar(200) NOT NULL,
  `oauth_type` varchar(200) NOT NULL,
  `user_regdatafill` int(11) NOT NULL DEFAULT '1',
  `created_by` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `user_firstname`, `user_lastname`, `user_emailid`, `user_password`, `user_mobileno`, `user_status`, `user_createdon`, `user_modifiedon`, `user_Address`, `user_varified`, `user_town`, `user_zip`, `user_country`, `user_title`, `user_dob`, `user_state`, `user_city`, `user_pincodes`, `user_usertype`, `oauth_id`, `oauth_provider`, `oauth_link`, `oauth_picture_link`, `oauth_type`, `user_regdatafill`, `created_by`) VALUES
(4, 'Raju Gupta', '', 'raju@gmail.com', '', '9810668829', 0, '2018-01-10 12:10:16', '0000-00-00 00:00:00', 'MANTRIPUKHEI,NEAR RPF CAMP,IMPHAL', 1, 'IMPHAL', '222222', 'India', 'Mr', '', 'MANIPUR', 'IMPHAL', '222222', 'agent_customer', '', '', '', '', '', 0, '7'),
(5, 'Raju Gupta', '', 'raju@gmail.com', '', '9810668829', 0, '2018-01-10 12:55:38', '0000-00-00 00:00:00', 'MANTRIPUKHEI,NEAR RPF CAMP,IMPHAL', 1, 'IMPHAL', '777888', 'India', 'Mr', '', 'MANIPUR', 'IMPHAL', '777888', 'agent_customer', '', '', '', '', '', 0, '7');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_userlog`
--

CREATE TABLE `tbl_userlog` (
  `userlogid` int(11) NOT NULL,
  `userid` int(11) DEFAULT NULL,
  `logindate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_usertoken`
--

CREATE TABLE `tbl_usertoken` (
  `userlogid` int(11) NOT NULL,
  `createddate` datetime DEFAULT NULL,
  `expirydate` datetime DEFAULT NULL,
  `tokenid` varchar(255) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_commission_detail`
--

CREATE TABLE `tbl_user_commission_detail` (
  `c_id` int(11) NOT NULL,
  `invoice_id` varchar(100) NOT NULL,
  `uid` int(11) NOT NULL,
  `oid` varchar(50) NOT NULL,
  `commission` varchar(100) NOT NULL,
  `order_price` varchar(10) NOT NULL,
  `commission_price` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'P' COMMENT 'p=pending,s=sending',
  `update_status` int(11) DEFAULT '0',
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_on` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user_commission_detail`
--

INSERT INTO `tbl_user_commission_detail` (`c_id`, `invoice_id`, `uid`, `oid`, `commission`, `order_price`, `commission_price`, `status`, `update_status`, `created_on`, `update_on`) VALUES
(1, '', 7, '1', '', '1535', '', 'P', 0, '2018-01-09 15:15:00', ''),
(2, '', 7, '3', '', '1750', '', 'P', 0, '2018-01-09 15:41:06', ''),
(3, '', 7, '3', '', '1750', '', 'P', 0, '2018-01-09 15:42:06', ''),
(4, '', 7, '3', '', '1750', '', 'P', 0, '2018-01-09 15:45:06', ''),
(5, '', 7, '2', '', '540', '', 'P', 0, '2018-01-09 16:33:58', ''),
(6, '', 7, '4', '', '1595', '', 'P', 0, '2018-01-09 17:13:53', ''),
(7, '', 7, '5', '', '610', '', 'P', 0, '2018-01-09 17:44:57', ''),
(8, '', 7, '6', '', '2510', '', 'P', 0, '2018-01-09 17:59:26', ''),
(9, '', 7, '7', '', '1535', '', 'P', 0, '2018-01-09 18:00:00', ''),
(10, '', 14, '10', '', '2510', '', 'P', 0, '2018-01-09 18:19:16', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_promo`
--

CREATE TABLE `tbl_user_promo` (
  `id` int(11) NOT NULL,
  `uid` int(11) DEFAULT NULL,
  `p_id` int(11) DEFAULT NULL,
  `inventory_id` int(11) NOT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user_promo`
--

INSERT INTO `tbl_user_promo` (`id`, `uid`, `p_id`, `inventory_id`, `created_on`) VALUES
(1, 7, 14, 75, '2018-01-10 11:28:30'),
(2, 7, 14, 75, '2018-01-10 11:30:34'),
(3, 7, 14, 65, '2018-01-10 12:00:00'),
(4, 7, 14, 65, '2018-01-10 12:07:51'),
(5, 7, 14, 65, '2018-01-10 12:10:17'),
(6, 7, 14, 65, '2018-01-10 12:55:38');

-- --------------------------------------------------------

--
-- Table structure for table `tempaddoncarttable`
--

CREATE TABLE `tempaddoncarttable` (
  `tempaddon` int(11) NOT NULL,
  `uniqid` varchar(255) NOT NULL,
  `addonid` varchar(11) NOT NULL,
  `addonprice` varchar(11) NOT NULL,
  `addonqty` varchar(11) NOT NULL,
  `addonimage` varchar(255) NOT NULL,
  `addonname` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD KEY `last_activity_idx` (`last_activity`);

--
-- Indexes for table `mail_tbl`
--
ALTER TABLE `mail_tbl`
  ADD PRIMARY KEY (`mail_id`);

--
-- Indexes for table `tbladminmenu`
--
ALTER TABLE `tbladminmenu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblassignmenu`
--
ALTER TABLE `tblassignmenu`
  ADD PRIMARY KEY (`assign_id`);

--
-- Indexes for table `tblassignrole`
--
ALTER TABLE `tblassignrole`
  ADD PRIMARY KEY (`assign_id`);

--
-- Indexes for table `tblconfigdetails`
--
ALTER TABLE `tblconfigdetails`
  ADD PRIMARY KEY (`configurationid`);

--
-- Indexes for table `tblrole`
--
ALTER TABLE `tblrole`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `tbl_activity`
--
ALTER TABLE `tbl_activity`
  ADD PRIMARY KEY (`activity_id`);

--
-- Indexes for table `tbl_activity_package`
--
ALTER TABLE `tbl_activity_package`
  ADD PRIMARY KEY (`ap_id`);

--
-- Indexes for table `tbl_addon`
--
ALTER TABLE `tbl_addon`
  ADD PRIMARY KEY (`addon_id`);

--
-- Indexes for table `tbl_adminlogin`
--
ALTER TABLE `tbl_adminlogin`
  ADD PRIMARY KEY (`LoginID`),
  ADD UNIQUE KEY `UNIQUE KEY` (`UserName`),
  ADD KEY `FK_tbl_emplogin_tbl_employee` (`EmployeeId`);

--
-- Indexes for table `tbl_agent_payment_history`
--
ALTER TABLE `tbl_agent_payment_history`
  ADD PRIMARY KEY (`ph_id`);

--
-- Indexes for table `tbl_agent_profile`
--
ALTER TABLE `tbl_agent_profile`
  ADD PRIMARY KEY (`agent_id`);

--
-- Indexes for table `tbl_agent_reg`
--
ALTER TABLE `tbl_agent_reg`
  ADD PRIMARY KEY (`agent_id`);

--
-- Indexes for table `tbl_agent_wallet`
--
ALTER TABLE `tbl_agent_wallet`
  ADD PRIMARY KEY (`wid`);

--
-- Indexes for table `tbl_bannerimage`
--
ALTER TABLE `tbl_bannerimage`
  ADD PRIMARY KEY (`bannerimage_id`);

--
-- Indexes for table `tbl_bookingtypemaster`
--
ALTER TABLE `tbl_bookingtypemaster`
  ADD PRIMARY KEY (`bookingtypeid`);

--
-- Indexes for table `tbl_branch_master`
--
ALTER TABLE `tbl_branch_master`
  ADD PRIMARY KEY (`branch_id`);

--
-- Indexes for table `tbl_branch_package`
--
ALTER TABLE `tbl_branch_package`
  ADD PRIMARY KEY (`bp_id`);

--
-- Indexes for table `tbl_city`
--
ALTER TABLE `tbl_city`
  ADD PRIMARY KEY (`cityid`),
  ADD KEY `fk_State_idx` (`stateid`);

--
-- Indexes for table `tbl_commissioin_payment_date`
--
ALTER TABLE `tbl_commissioin_payment_date`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_commission`
--
ALTER TABLE `tbl_commission`
  ADD PRIMARY KEY (`cid`);

--
-- Indexes for table `tbl_company_master`
--
ALTER TABLE `tbl_company_master`
  ADD PRIMARY KEY (`comp_id`);

--
-- Indexes for table `tbl_contact`
--
ALTER TABLE `tbl_contact`
  ADD PRIMARY KEY (`cont_id`);

--
-- Indexes for table `tbl_country`
--
ALTER TABLE `tbl_country`
  ADD PRIMARY KEY (`conid`),
  ADD KEY `ttaxm_conid` (`conid`);

--
-- Indexes for table `tbl_customer`
--
ALTER TABLE `tbl_customer`
  ADD PRIMARY KEY (`custid`);

--
-- Indexes for table `tbl_daily_inventory`
--
ALTER TABLE `tbl_daily_inventory`
  ADD PRIMARY KEY (`dailyinventory_id`);

--
-- Indexes for table `tbl_daily_inventorypackages`
--
ALTER TABLE `tbl_daily_inventorypackages`
  ADD PRIMARY KEY (`dailyinventorypackages_id`);

--
-- Indexes for table `tbl_employee`
--
ALTER TABLE `tbl_employee`
  ADD PRIMARY KEY (`emp_tbl_id`);

--
-- Indexes for table `tbl_groupbooking`
--
ALTER TABLE `tbl_groupbooking`
  ADD PRIMARY KEY (`bookingid`);

--
-- Indexes for table `tbl_location`
--
ALTER TABLE `tbl_location`
  ADD PRIMARY KEY (`locationid`);

--
-- Indexes for table `tbl_orderaddone`
--
ALTER TABLE `tbl_orderaddone`
  ADD PRIMARY KEY (`orderaddoneid`);

--
-- Indexes for table `tbl_orderpackage`
--
ALTER TABLE `tbl_orderpackage`
  ADD PRIMARY KEY (`pacorderid`);

--
-- Indexes for table `tbl_packagevalue`
--
ALTER TABLE `tbl_packagevalue`
  ADD PRIMARY KEY (`packagevalueid`);

--
-- Indexes for table `tbl_package_master`
--
ALTER TABLE `tbl_package_master`
  ADD PRIMARY KEY (`package_id`);

--
-- Indexes for table `tbl_partner`
--
ALTER TABLE `tbl_partner`
  ADD PRIMARY KEY (`part_id`);

--
-- Indexes for table `tbl_payment_gatway`
--
ALTER TABLE `tbl_payment_gatway`
  ADD PRIMARY KEY (`pg_id`);

--
-- Indexes for table `tbl_promo`
--
ALTER TABLE `tbl_promo`
  ADD PRIMARY KEY (`promo_id`);

--
-- Indexes for table `tbl_promopackage`
--
ALTER TABLE `tbl_promopackage`
  ADD PRIMARY KEY (`promopackage_id`);

--
-- Indexes for table `tbl_seoskiindia`
--
ALTER TABLE `tbl_seoskiindia`
  ADD PRIMARY KEY (`seo_id`);

--
-- Indexes for table `tbl_sms_gatway`
--
ALTER TABLE `tbl_sms_gatway`
  ADD PRIMARY KEY (`sms_id`);

--
-- Indexes for table `tbl_state`
--
ALTER TABLE `tbl_state`
  ADD PRIMARY KEY (`stateid`),
  ADD KEY `ttaxm_stateid` (`stateid`);

--
-- Indexes for table `tbl_tearm`
--
ALTER TABLE `tbl_tearm`
  ADD PRIMARY KEY (`term_id`);

--
-- Indexes for table `tbl_timeslot_branch`
--
ALTER TABLE `tbl_timeslot_branch`
  ADD PRIMARY KEY (`timeslot_id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `tbl_userlog`
--
ALTER TABLE `tbl_userlog`
  ADD PRIMARY KEY (`userlogid`);

--
-- Indexes for table `tbl_usertoken`
--
ALTER TABLE `tbl_usertoken`
  ADD PRIMARY KEY (`userlogid`);

--
-- Indexes for table `tbl_user_commission_detail`
--
ALTER TABLE `tbl_user_commission_detail`
  ADD PRIMARY KEY (`c_id`);

--
-- Indexes for table `tbl_user_promo`
--
ALTER TABLE `tbl_user_promo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tempaddoncarttable`
--
ALTER TABLE `tempaddoncarttable`
  ADD PRIMARY KEY (`tempaddon`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mail_tbl`
--
ALTER TABLE `mail_tbl`
  MODIFY `mail_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbladminmenu`
--
ALTER TABLE `tbladminmenu`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;
--
-- AUTO_INCREMENT for table `tblassignmenu`
--
ALTER TABLE `tblassignmenu`
  MODIFY `assign_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `tblassignrole`
--
ALTER TABLE `tblassignrole`
  MODIFY `assign_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=139;
--
-- AUTO_INCREMENT for table `tblconfigdetails`
--
ALTER TABLE `tblconfigdetails`
  MODIFY `configurationid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblrole`
--
ALTER TABLE `tblrole`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_activity`
--
ALTER TABLE `tbl_activity`
  MODIFY `activity_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_activity_package`
--
ALTER TABLE `tbl_activity_package`
  MODIFY `ap_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_addon`
--
ALTER TABLE `tbl_addon`
  MODIFY `addon_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_adminlogin`
--
ALTER TABLE `tbl_adminlogin`
  MODIFY `LoginID` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_agent_payment_history`
--
ALTER TABLE `tbl_agent_payment_history`
  MODIFY `ph_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_agent_profile`
--
ALTER TABLE `tbl_agent_profile`
  MODIFY `agent_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_agent_reg`
--
ALTER TABLE `tbl_agent_reg`
  MODIFY `agent_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `tbl_agent_wallet`
--
ALTER TABLE `tbl_agent_wallet`
  MODIFY `wid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `tbl_bannerimage`
--
ALTER TABLE `tbl_bannerimage`
  MODIFY `bannerimage_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_bookingtypemaster`
--
ALTER TABLE `tbl_bookingtypemaster`
  MODIFY `bookingtypeid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_branch_master`
--
ALTER TABLE `tbl_branch_master`
  MODIFY `branch_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_branch_package`
--
ALTER TABLE `tbl_branch_package`
  MODIFY `bp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `tbl_city`
--
ALTER TABLE `tbl_city`
  MODIFY `cityid` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_commissioin_payment_date`
--
ALTER TABLE `tbl_commissioin_payment_date`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_commission`
--
ALTER TABLE `tbl_commission`
  MODIFY `cid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `tbl_company_master`
--
ALTER TABLE `tbl_company_master`
  MODIFY `comp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_contact`
--
ALTER TABLE `tbl_contact`
  MODIFY `cont_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_country`
--
ALTER TABLE `tbl_country`
  MODIFY `conid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_customer`
--
ALTER TABLE `tbl_customer`
  MODIFY `custid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_daily_inventory`
--
ALTER TABLE `tbl_daily_inventory`
  MODIFY `dailyinventory_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=251;
--
-- AUTO_INCREMENT for table `tbl_daily_inventorypackages`
--
ALTER TABLE `tbl_daily_inventorypackages`
  MODIFY `dailyinventorypackages_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=561;
--
-- AUTO_INCREMENT for table `tbl_employee`
--
ALTER TABLE `tbl_employee`
  MODIFY `emp_tbl_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tbl_groupbooking`
--
ALTER TABLE `tbl_groupbooking`
  MODIFY `bookingid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_location`
--
ALTER TABLE `tbl_location`
  MODIFY `locationid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_orderaddone`
--
ALTER TABLE `tbl_orderaddone`
  MODIFY `orderaddoneid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `tbl_orderpackage`
--
ALTER TABLE `tbl_orderpackage`
  MODIFY `pacorderid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_packagevalue`
--
ALTER TABLE `tbl_packagevalue`
  MODIFY `packagevalueid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_package_master`
--
ALTER TABLE `tbl_package_master`
  MODIFY `package_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `tbl_partner`
--
ALTER TABLE `tbl_partner`
  MODIFY `part_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_payment_gatway`
--
ALTER TABLE `tbl_payment_gatway`
  MODIFY `pg_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `tbl_promo`
--
ALTER TABLE `tbl_promo`
  MODIFY `promo_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `tbl_promopackage`
--
ALTER TABLE `tbl_promopackage`
  MODIFY `promopackage_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `tbl_seoskiindia`
--
ALTER TABLE `tbl_seoskiindia`
  MODIFY `seo_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_sms_gatway`
--
ALTER TABLE `tbl_sms_gatway`
  MODIFY `sms_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_state`
--
ALTER TABLE `tbl_state`
  MODIFY `stateid` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_tearm`
--
ALTER TABLE `tbl_tearm`
  MODIFY `term_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `tbl_timeslot_branch`
--
ALTER TABLE `tbl_timeslot_branch`
  MODIFY `timeslot_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_userlog`
--
ALTER TABLE `tbl_userlog`
  MODIFY `userlogid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_usertoken`
--
ALTER TABLE `tbl_usertoken`
  MODIFY `userlogid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_user_commission_detail`
--
ALTER TABLE `tbl_user_commission_detail`
  MODIFY `c_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tbl_user_promo`
--
ALTER TABLE `tbl_user_promo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tempaddoncarttable`
--
ALTER TABLE `tempaddoncarttable`
  MODIFY `tempaddon` int(11) NOT NULL AUTO_INCREMENT;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
