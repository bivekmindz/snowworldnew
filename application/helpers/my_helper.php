<?php


function p($data){
		echo "<pre>";
		print_r($data);
		echo "</pre>";
}



	function pend($data){
		echo "<pre>";
		print_r($data);
		echo "</pre>";
		die();
	}


/*
|--------------------------------------------------------------------------
| CURL FUNCTION POST METHOD THIS WILL READ THE DATA IN JSON FORMAT
|--------------------------------------------------------------------------
*/
	function curlpost($parameters, $path){
		$apiUrl = $path; 
		$curl_handle = curl_init();
		curl_setopt($curl_handle, CURLOPT_URL, $apiUrl);
		curl_setopt($curl_handle, CURLOPT_BUFFERSIZE, 1024);
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($curl_handle, CURLOPT_POST, true);
        curl_setopt($curl_handle, CURLOPT_PROXY, '');
        curl_setopt($curl_handle, CURLOPT_SSLVERSION, 3);
		curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $parameters);
        $response = curl_exec($curl_handle);

               #$info=curl_getinfo($curl_handle);
               #p($response);    
               #print_R($info); 
		
		$newresponse = (object) json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $response), true);	
		//$response  = json_decode($newresponse);
		curl_close($curl_handle);
		return $newresponse;
	}
	
/*
|-----------p---------------------------------------------------------------
| CURL FUNCTION GET METHOD THIS WILL READ THE DATA IN JSON FORMAT
|--------------------------------------------------------------------------
*/
	function curlget($myurl){
	/* */   $curl = curl_init();
                
                $option=array(CURLOPT_RETURNTRANSFER =>true,
                              CURLOPT_URL => $myurl,
                              CURLOPT_USERAGENT =>'Shopotox',
                              CURLOPT_SSL_VERIFYPEER => FALSE,
                              CURLOPT_SSL_VERIFYHOST => FALSE,
                              CURLOPT_PROXY => '',
                              CURLOPT_SSLVERSION => 3
                              );
		// Set some options - we are passing in a useragent too here
		curl_setopt_array($curl,$option);
		// Send the request & save response to $resp
		$response = curl_exec($curl);

               #$info=curl_getinfo($curl_handle);
               #p($response);    
               #print_R($info); 
		
		$newresponse = (object) json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $response), true);	
                #p($newresponse);exit;

		//$response  = json_decode($newresponse);
		curl_close($curl_handle);
		return $newresponse;
	}

function curlgetv($parameters,$path){
		$apiUrl = $path; 
		$curl_handle = curl_init();
		curl_setopt($curl_handle, CURLOPT_URL, $apiUrl);
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER,true);
		curl_setopt($curl_handle, CURLOPT_POST, true);
		curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $parameters);
		$response = curl_exec($curl_handle);
		curl_close($curl_handle);
   		return $response = json_decode($response);
}
	/*
|--------------------------------------------------------------------------
| CURL FUNCTION GET METHOD THIS WILL READ THE DATA IN JSON FORMAT WITH PARAMETERS
|--------------------------------------------------------------------------
*/
	function curlgetp($myurl){
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $myurl);
		curl_setopt($curl, CURLOPT_HTTPGET, 0);
		$response = curl_exec($curl);
		curl_close($curl);
		return $response;
		
	}

  function recursiveRemoveDirectory($directory) {
    foreach (glob("{$directory}/*") as $file) {
        if (is_dir($file)) {
            recursiveRemoveDirectory($file);
        } else {
            unlink($file);
        }
    }
    rmdir($directory);
}
/*function compressImage($ext, $uploadedfile, $path, $actual_image_name, $newwidth, $newheight) {
       
    $src = imagecreatefromjpeg($uploadedfile);
    
    list($width, $height) = getimagesize($uploadedfile);

    $tmp = imagecreatetruecolor($newwidth, $newheight);
    imagecopyresampled($tmp, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

    $t = imagejpeg($tmp, $actual_image_name, 100);

    imagedestroy($tmp);
    
}*/



function compressImage($ext, $uploadedfile, $path, $actual_image_name, $newwidth, $newheight) {
       
    $src = imagecreatefromjpeg($uploadedfile);
    
    list($width, $height) = getimagesize($uploadedfile);

    $tmp = imagecreatetruecolor($newwidth, $newheight);
    imagecopyresampled($tmp, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

    $t = imagejpeg($tmp, $actual_image_name, 100);

    imagedestroy($tmp);
    
}

function compressImagepng($ext, $uploadedfile, $path, $actual_image_name, $newwidth, $newheight) {
       
    $src = imagecreatefrompng($uploadedfile);
    
    list($width, $height) = getimagesize($uploadedfile);

    $tmp = imagecreatetruecolor($newwidth, $newheight);
    imagecopyresampled($tmp, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

    $t = imagejpeg($tmp, $actual_image_name, 100);

    imagedestroy($tmp);
    
}

function compressImageany($ext, $uploadedfile, $path, $actual_image_name, $newwidth, $newheight) {
       
    $src = imagecreatefromgif($uploadedfile);
    
    list($width, $height) = getimagesize($uploadedfile);

    $tmp = imagecreatetruecolor($newwidth, $newheight);
    imagecopyresampled($tmp, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

    $t = imagejpeg($tmp, $actual_image_name, 100);

    imagedestroy($tmp);
    
}



	function img_url(){
		$base_url = base_url();
		$result = $base_url."assets/webapp/images/";
		return $result;
	}

    function proImg_url($imgUrl){
		$base_url = base_url();
		$imgresult = $base_url.'images/hoverimg/'.$imgUrl; 
		return $imgresult;		
	}
	function proimgurl($path=null){
		$base_url =base_url();
		
		if($path==null)
		$result = $base_url;
	    else
	    $result = $base_url.$path;	
		return $result;
	}


	function css_url($urlget=null){
		if($urlget==null)
			$result = base_url()."assets/webapp/css/".$urlget;
			
		else
			$result = base_url()."assets/webapp/css/".$urlget;
		return $result;
	}

	function js_url($urlget=null){
		if($urlget==null)
			$result = base_url()."assets/webapp/js/".$urlget;
			
		else
			$result = base_url()."assets/webapp/js/".$urlget;
		return $result;
	}

	function admin_url(){
		$Admin_base_url = otherbase_url();
		$result = $Admin_base_url."assets/admin/";
		return $result;	
	}
	function vendor_url(){
		$Admin_base_url = otherbase_url();
		$result = $Admin_base_url."assets/vendor/";
		return $result;	
	}

	function api_url(){		
		//$api_url ='http://115.124.125.148/~snowworld/';
                $api_url ='http://192.168.1.65/snowworldnew/';
		$result = $api_url."api/";
		return $result;
	}

	

/*
|--------------------------------------------------------------------------
| ADMIN PANNEL URL CONFIGURATION
|--------------------------------------------------------------------------
|
|  function name is otherbase_url();
|  call this function any where working with admin panel
|
*/

	function otherbase_url(){
		$url = base_url();//"http://beta.royzez.com/";
		return $url;
	}
	
	////CUSTOMIZE RANDOM FUNCTION/////////////// 
	function mt_rand_str ($l, $c = 'abcdefghijklmnopqrstuvwxyz1234567890') {
       for ($s = '', $cl = strlen($c)-1, $i = 0; $i < $l; $s .= $c[mt_rand(0, $cl)], ++$i);
       return $s;
    }


    function expatt($value,$sep){
    	$exp=explode("$sep",$value);
    	return array_filter($exp);
    }

    function logd(){
    	
    	$CI =& get_instance();
    	
    	$data = $CI->session->userdata('snowworld');
    	//p($data); exit;
    	if(!empty($data)){
    		return $data;
    	}else{
    		return false;
    	}
    }


function convertToObject($array) {
        $object = new stdClass();
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $value = convertToObject($value);
            }
            $object->$key=$value;
        }
        return $object;
    }




function xmlrearead(){
$doc = new DOMDocument();
$file =base_url().'category.xml';
$doc->load($file);
$category= array();
$employees = $doc->getElementsByTagName( "root" );
$i=0;
foreach( $employees as $employee )
{
$ParentNames = $employee->getElementsByTagName( "ParentName" );
  $ParentName = $ParentNames->item(0)->nodeValue;

  $catparentids = $employee->getElementsByTagName( "catparentid" );
  $catparentid = $catparentids->item(0)->nodeValue;
  $ChildNames= $employee->getElementsByTagName( "ChildName" );
  $ChildName= $ChildNames->item(0)->nodeValue;
  $childids = $employee->getElementsByTagName( "childid" );
  $childid = $childids->item(0)->nodeValue;
  $catlevels = $employee->getElementsByTagName("catlevel" );
  $catlevel = $catlevels->item(0)->nodeValue;
  $urls = $employee->getElementsByTagName( "url" );
  $url = $urls->item(0)->nodeValue;
  $productss = $employee->getElementsByTagName( "products" );
  $products = $productss->item(0)->nodeValue;
  $CatLogos = $employee->getElementsByTagName( "CatLogo" );
  $CatLogo = $CatLogos->item(0)->nodeValue;
  $CatImageFirsts = $employee->getElementsByTagName( "CatImageFirst" );
  $CatImageFirst = $CatImageFirsts->item(0)->nodeValue;
  $CatImageSeconds = $employee->getElementsByTagName( "CatImageSecond" );
  $CatImageSecond = $CatImageSeconds->item(0)->nodeValue;
  $CatImageThirds = $employee->getElementsByTagName( "CatImageThird" );
  $CatImageThird = $CatImageThirds->item(0)->nodeValue;
  $CatImagealtnames = $employee->getElementsByTagName( "CatImagealtname" );
  $CatImagealtname = $CatImagealtnames->item(0)->nodeValue;

  $category[$i]['ParentName']=$ParentName;
  $category[$i]['catparentid']=$catparentid;
  $category[$i]['CatLogo']=$CatLogo;
  $category[$i]['CatImageFirst']=$CatImageFirst;
  $category[$i]['CatImageSecond']=$CatImageSecond;
  $category[$i]['CatImageThird']=$CatImageThird;
  $category[$i]['CatImagealtname']=str_replace("~","&",$CatImagealtname);
  $category[$i]['ChildName']=str_replace("~","&",$ChildName);
  $category[$i]['childid']=$childid;
  $category[$i]['catlevel']=$catlevel;
 
  $category[$i]['products']=$products; 
  $category[$i]['url']=$url;

  $i++;
  }

  $test= convertToObject($category);
return $test;
  }






	  function menu(){
	
	  	 $response=xmlrearead();
	  	 
	  	 
	  	 $j=1;
	  	$m=null;
	  	if($response!='Something Went Wrong' && $response!=''){
        foreach($response AS $key=>$menulist){
        	
              	if($menulist->catparentid==0){
              	  //$url='';
                   $mainmenu=strtolower(str_replace(' ','-',$menulist->ChildName));

                   if($menulist->catparentid==0) {
              	   	 $url = base_url().$menulist->url;
                   } else if($menulist->products == 0) {
              	   	 $url = base_url().'webapp/category/comingsoon';
                   } else {
              	   	 $url = base_url().'category/'.str_replace('&','en',$mainmenu).'-'.$menulist->childid.'.html';
                   }
					if($menulist->ChildName=='')
					{
						$m.='<li></li>';			
					}else{



$topcaturl= base_url().'maincategory/'.str_replace('&','en',$mainmenu).'-'.$menulist->childid.'.html';
$m.='<li><a href="'.$topcaturl.'"><i><img src="'.base_url().'assets/catimages/'.$menulist->CatLogo.'" /></i>'.ucfirst($menulist->ChildName).'</a>';
						
						/*$m.='<li class="li_coun n'.$j.'"><a href="'.$menulist->url.'"><i><img src="'.base_url().'assets/catimages/'.$menulist->CatLogo.'" /></i>'.ucfirst($menulist->ChildName).'</a>';	*/
						/*$m.='<li><a href="'.base_url().$menulist->url.'"><i><img src="'.base_url().'assets/catimages/'.$menulist->CatLogo.'" /></i>'.ucfirst($menulist->ChildName).'</a>';*/	
						
					}
	                 
	                 //$m.='<li><a href="'.$url.'">'.ucfirst($menulist->ChildName).'</a>';
	                 if($menulist->products > 0) {
	                    $m.='<div class="left_sub hidden-sm hidden-xs"> <div class="col-lg-6 col-md-12">';
	                 }
	                 $child = $menulist->childid;
	                 $m .= '<ul>';
	                 foreach($response AS $skey=>$smenulist){
	                   if($smenulist->catparentid == $child && $smenulist->products > 0 && $menulist->products > 0){
	                   	$smenu=strtolower(str_replace(' ','-',$smenulist->ChildName));
	                   	 $url = base_url().'category/'.str_replace('&','en',$smenu).'-'.$smenulist->childid.'.html';

	                     $m.='<li><a href="'.$url.'">'.ucfirst($smenulist->ChildName).'</a></li>';

	                    $subchild = $smenulist->childid;
	                    /*foreach($response AS $subkey=>$submenulist){
	                      if($submenulist->catparentid == $subchild && $submenulist->products > 0 && $smenulist->products > 0 && $menulist->products > 0){
	                      	$submenu=strtolower(str_replace(' ','-',$submenulist->ChildName));
                              
                			$m.='<li><a href="'.base_url().'category/'.str_replace('&','en',$smenu).'/'.str_replace('&','en',$submenu).'-'.$submenulist->childid.'.html'.'">'.ucfirst($submenulist->ChildName).'</a></li>';
	                     		  
                            //$m.='<li><a href="'.base_url().'category/'.str_replace('&','en',$smenu).'/'.str_replace('&','en',$submenu).'-'.$submenulist->childid.'.html'.'">'.ucfirst($submenulist->ChildName).'</a></li>'; 
                            }
	                    }//end foreach 3rd level.
	                    */
	                   }
	                 }//end foreach 2nd level.
	                $m.='</ul>';

	                /*if($menulist->products > 0) {
	                 	if($menulist->CatImage!='') {
	                 		$imgUrl = img_url().$menulist->CatImage;
	                 		$img='<img src="'.$imgUrl.'" style="width:250px;">';
	                 	} else {
							$img = null;
	                 	}
	                 	$m.='</li><li class="liRight">gdfgdfgdfg'.$img.'</li></ul>';
	                }*/
	                $m.='</div>';
	                if($menulist->catparentid==0){
	                	

	                	/*$m.='<div class="hidden-lg hidden-sm hidden-md hidden-xs">
                                <div class="col-lg-6">
                                	<div class="row">
                                      	<div class="sub_m_banner">';
                                      	
                                      	if(!empty($menulist->CatImageFirst)){
                                          	$m.='<img src="'.base_url().'assets/webapp/images/'.substr($menulist->CatImageFirst,10).'" alt="'.$menulist->CatImagealtname.'" />';
                                        }  	
                                      	
                                      	$m.='</div>
                                    </div>
                                </div>
                                  <div class="col-lg-6">
                                    <div class="sub_m_banner">';
                                    if(!empty($menulist->CatImageSecond)){
                                        $m.='<img src="'.base_url().'assets/webapp/images/'.substr($menulist->CatImageSecond,10).'" />';
                                    }    
                                    $m.='</div>
                                    <div class="sub_m_banner">';
                                    if(!empty($menulist->CatImageThird)){
                                        $m.='<img src="'.base_url().'assets/webapp/images/'.substr($menulist->CatImageThird,10).'" />';
                                    }
                                    $m.='</div>
                                </div>
                            </div>';*/




//--------------------------------- new change in images menu-----------------------------------



//$m.='<div class="hidden-lg hidden-sm hidden-md hidden-xs">
                    $m.='<div class="col-lg-6 hidden-md hidden-xs">
                                <div class="col-lg-12">
                                	<div class="row">
                                      	<div class="sub_m_banner">';
                                      	
                                      	if(!empty($menulist->CatImageFirst)){
                                          	//$m.='<img src="'.base_url().'assets/webapp/images/'.substr($menulist->CatImageFirst,10).'" alt="'.$menulist->CatImagealtname.'" />';
                                            $m.='<img src="'.base_url().'assets/webapp/images/'.$menulist->CatImageFirst.'"  alt="'.$menulist->CatImagealtname.'" />';
                                        }  	
                                      	
                                      	$m.='</div>
                                    </div>
                                </div>';
                                  /*<div class="col-lg-6">
                                    <div class="sub_m_banner">';
                                    if(!empty($menulist->CatImageSecond)){
                                        $m.='<img src="'.base_url().'assets/webapp/images/'.substr($menulist->CatImageSecond,10).'" />';
                                    }    
                                    $m.='</div>
                                    <div class="sub_m_banner">';
                                    if(!empty($menulist->CatImageThird)){
                                        $m.='<img src="'.base_url().'assets/webapp/images/'.substr($menulist->CatImageThird,10).'" />';
                                    }*/
                                    $m.='</div>';
                                /*</div>
                            </div>';*/


//--------------------------------- new change in images menu-----------------------------------



	                }
	                $m.='</div></li>';
              		$j++;
              		if($j==11){
              		break;
              	        }
              		
              	}// if parent loop
              	
           }//end foreach 1st level.
       }
          //$CI = &get_instance();
          //$CI->session->set_userdata('menu',$m);
           return $m;
      }

    function productcount($catid){
      $parameter=array('catid'=>$catid);
      $path = api_url().'headersearch/catProdCount/catid/'.$catid.'/format/json/';
	  $response = curlget($path);
	  $data = $response->totalrow;
	  return $data;
    }
	function wist_count($id)
	{
	  $parameter=array('uid'=>$id);
      $path = api_url().'productlisting/wishlistCount/uid/'.$id.'/format/json/';
      $response = curlget($path); 
      return $response; 
  }
     function catList($cid)
{
      $parameter=array('catid'=>$cid);
      $path = api_url().'productlisting/categoryCount/catid/'.$cid.'/format/json/';
      $response = curlget($path); 
      return $response;         //$response;                                                                                                                                           
    }
    
    function catName($cid){
    	
    $parameter=array('catid'=>$cid);
    $path = api_url().'productlisting/categoryName/catid/'.$cid.'/format/json/';
    $response = curlget($path);
    return $response;  
	}

    function parentcat($cid){
      $path = api_url().'productlisting/parentcatId/catid/'.$cid.'/format/json/';
	  $response = curlget($path);
	  return $response;                                                                                                                                           
    }
 
    function productName($pid){
      $path = api_url().'headersearch/productName/pid/'.$pid.'/format/json/'; 
	  $response = curlget($path);
	  foreach($response as $key=>$pname){
        $pcatid = $pname->ProName;
	  } 
	  return $pcatid;                                                                                                                                           
    }

    function catbyname($pid){
      $catid=0;
      $caturl=null;
      $path = api_url().'headersearch/catNameid/pid/'.$pid.'/format/json/';
	  $response = curlget($path);
	  if($response!='Something Went Wrong')
	  foreach($response as $key=>$cid){
        $catid = $cid->CatId;
        $caturl=$cid->url;
	  } 
	  return $caturl;                                                                                                                                           
    }

    function productCatName($pid){
     $path = api_url().'productlisting/productCatName/pid/'.$pid.'/format/json/'; 
     $response = curlget($path);
	 return $response;                                                                                                                                           
    }

    
    function catBrand($cid){
      $parameter=array('catid'=>$cid);
      $path = api_url().'productlisting/categoryBrand/catid/'.$cid.'/format/json/';
	  $response = curlget($path);
	  if($response=='Something Went Wrong')
	  	$response=null;
	  else
	  	$response=$response;
	  return $response;                                                                                                                                           
    }

    function catPrice($cid){
      $path = api_url().'productlisting/categoryPrice/catid/'.$cid.'/format/json/';
	  $response = curlget($path);
	  return $response;                                                                                                                                           
    }
    function ratingprod($pid){
      $parameter=array('prodid'=>$pid);
      $path = api_url().'headersearch/productrating/prodid/'.$pid.'/format/json/'; 
	  $response = curlget($path);
	  return $response;
    }
    function getFilter($fs){
       $s  = explode("-",$fs);
       return $s;
    }

    function getImgProdetail($proImgDetail){
      $proImg = explode(',',$proImgDetail);
      return $proImg; 
    }
	function totalqnty($qty, $amt){
		$x = $qty * $amt;
		return $x;
	}


      function in_cart_array($x, $productId) {
          foreach ($x as $key => $value) {
             if($value['id'] == $productId){
              return "true";
         	 }
      	  }
      }

      function in_cart_array_vendor($x, $productId) {
          foreach ($x as $key => $value) {
             if($value['id'] == $productId){
              return true;
             }
          }
      }

      function featureMain($arrayData,$productId){
           $data['profeature'] = $arrayData;
           //p($arrayData);exit();

           $rowarray =array();
           foreach ($data['profeature'] as $key => $value) {
           //p($value);

           	  //if(isset($value['FeatureName']))
                          if(isset($value['FeatureName']) && !empty($value['rightvalue']))
			        $rowarray[] = $value['FeatureName'];
           }
           	$x = array_unique($rowarray);

           	$count = 0;
			foreach ($x as $key => $values) {
             
           		echo "<thead>";
                echo "<tr>";
                echo '<th scope="col" colspan="4" align="left">'.$values.'</th>';
                echo '</tr>';
                echo '</thead>';
                echo "<tbody>";
                ///echo "<tr>";
           		foreach ($data['profeature'] as $key => $value) { 
           			$count = $count++;
           			if($count==2){
           				echo "</tr>";
           			}else{
           				echo "<tr>";	
           			}
           			if(!empty($values) && !empty($value['rightvalue']) && $values == $value['FeatureName']){
           				echo '<td>';
           				echo $value['leftmastervalue']; 
           				echo '</td>';
           				echo '<td>';
           				echo $value['rightvalue']; 
           				echo '</td>';
					}
           			
           		}
           		$count = 0;
           		//echo '</tr>';
           		echo '</tdbody>';				
           	}
      }


      function comaseprate($data){
		$value = explode(',', $data);
		return $value;  
		if($value['0'] == ''){
			$value = "";
		}
		//colonseprate($value);
		//return $value;
	}

	function colonseprate($val){
		$final = explode(':', $val);
		return $final;
	}
	
function wishlistlink($act,$userid,$proid){
		
			$path = api_url().'productlisting/addedwishlist/act_mode/'.$act.'/userid/'.$userid.'/proid/'.$proid.'/format/json/';
		$response=curlget($path);
		
		if(isset($response->ProId) && $response!='Something Went Wrong')
			return '<a class="listwhishlis" href="'.base_url().'webapp/memberlogin/myaccount" style="color:red;" wishlistid="'.$proid.'"> <i class="fa fa-heart" style="color:red;"></i></a>';
		else
		
		return '<a class="listwhishlis" data-href="'.$proid.'" id="listwhishlis'.$proid.'" wishlistid="'.$proid.'" style=""> <i class="fa fa-heart"></i></a>';
		
	}

function listwishlistlink($act,$userid,$proid){
		
		$path = api_url().'productlisting/addedwishlist/act_mode/'.$act.'/userid/'.$userid.'/proid/'.$proid.'/format/json/';
		$response=curlget($path);
		
		if(isset($response->ProId) && $response!='Something Went Wrong')
			return '<a class="listwhishlis" href="'.base_url().'webapp/memberlogin/myaccount" style="color:red;" wishlistid="'.$proid.'"> <i class="fa fa-heart" style="color:red;"></i></a>';
		else
			/*return '<li id="liwishlist'.$proid.'"><a class="whishlis" id="wishlist" wishlistid="'.$proid.'"> <i class="fa fa-heart"></i></a></li>';
		*/
		return '<a class="listwhishlis" data-href="'.$proid.'" id="listwhishlis'.$proid.'" wishlistid="'.$proid.'" style=""> <i class="fa fa-heart"></i></a>';
	}	




	function randnumber(){
    srand ((double) microtime() * 1000000);
    $random5 = rand(10000,99999);
    return $random5;
  }

  function tshippingcharge($cartamt){
      //$parameter=array('act_mode'=>'view','p_id'=>0);
      $parameter = array('act_mode'=>'view','row_id'=>'','orderstatus'=>'','order_proid'=>'','order_id'=>'','order_venid'=>'','order_qty'=>'','oldstatus'=>'');
      $path=api_url().'checkoutapi/shippingcharge/format/json/';
      $data=curlpost($parameter,$path);
      //p($data);//exit();
      $shipcharge=number_format($data->shipamt,1,".","");
      /*if($cartamt<=$data->shoppingamt){

      	$shipcharge=$data->shipamt;
      }
      else{
      	$shipcharge=0;
      }*/
      
      return $shipcharge;
  }

  function Totalvalincart($gettotalamount, $shipping, $couponcode,$cartdiscount){
  		if($couponcode=='')
  		{
  			return (($gettotalamount+$shipping) - ($cartdiscount+$couponcode));	
  		}else{
  			return (($gettotalamount+$shipping) - ($cartdiscount));	
  		}
  		
  }

function detailsorder($ordid){
	//p($ordid);exit;
	 $path=api_url().'userapi/orderdetailsview/ordid/'.$ordid.'/format/json/';
	$data=curlget($path);

	return $data;
}


function newurl($name){
	return seoUrl($name);
}
function brandgroup($brandid){
	if($brandid!=''){
		$path=api_url().'productlisting/groupbrandname/?brandid='.$brandid;
		$data=curlget($path);
		$brand=$data->brandname;
	}
	else{
		$brand='';
	}
	return $brand;
}
function confirmurlDetail($urlarray, $proname, $proId ,$revier){
		//return $urlarray;
		$ci 	= &get_instance();
		$ci->load->helper('url');
		unset($urlarray[count($urlarray)-1]);
		
		$trueurl =  implode('-', $urlarray);
		
		$apiUrl  =  newurl($proname);
		
		$apiUrl  =  strtolower($apiUrl);
		if($apiUrl != $trueurl){
          $url = base_url().'product/'.newurl($proname).'-'.$proId.'.html'.$revier;
       header("location:$url") ;        
        //redirect($url);
        exit();
        }/*else if(strcmp($apiUrl, $trueurl) !=0){
        	redirect("webapp/error/");
        }*/
}


function brt(){
	echo "<br/>";
}



function citybaseprice($baseprice,$cityper){
	
	$price=number_format($baseprice+($cityper*$baseprice/100),1,".","");
    return $price;
}


function cityqtyprice($baseprice,$groupprice,$cityper){

	$pricess=number_format(($baseprice+($groupprice*$baseprice/100)),1,".","");

	//$newcityper=ceil($groupprice-$cityper);

	$cityprice=number_format($pricess-$cityper*$pricess/100,1,".","");
	
    return $cityprice;
}



/*function citybaseprice($baseprice,$cityper){
	$price=ceil($baseprice+($cityper*$baseprice/100));
    return $price;
}


function cityqtyprice($baseprice,$groupprice,$cityper){

	$pricess=ceil($baseprice+($groupprice*$baseprice/100));

	//$newcityper=ceil($groupprice-$cityper);

	$cityprice=ceil($pricess-ceil($cityper*$pricess/100));
	
    return $cityprice;
}*/







function seoUrl($string) 
    {
        $string = replaceAll($string);
        //return $string; exit();
        //source: http://stackoverflow.com/a/11330527/1564365
        //Lower case everything
        $string = strtolower($string);
        //Make alphanumeric (removes all other characters)
        $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
        //Clean up multiple dashes or whitespaces
        $string = preg_replace("/[\s-]+/", " ", $string);
        //Convert whitespaces and underscore to dash
        $string = preg_replace("/[\s_]/", "-", $string);
        //$string = preg_replace("/[\s,]/", "-", $string);
        return strtolower($string);
    }

function replaceAll($text) { 
	    $text = strtolower(htmlentities($text)); 
	    $text = str_replace(get_html_translation_table(), "-", $text);
	    $text = str_replace(" ", "-", $text);
	    $text = str_replace("#", "", $text);
	    $text = preg_replace("/[-]+/i", "-", $text);
	    return $text;
}

function trackmyorderbyfedex($awbnumber)
{
if(!empty($awbnumber))
{
    $path1=base_url().'logistic/fedex/ordertrack.php';
    $param=array('trakingnumber'=>$awbnumber,'url'=>'website');
    return  $response = curlpost($param,$path1);
}

    
       
}
function trackmyorderbydelhivery($awbnumber)
{      
	if(!empty($awbnumber)){
	   $api_path = 'http://seller.royzez.com:83/royzez/Order/TracKOrder?waybill='.$awbnumber;
       return $response= curlget($api_path);
    }
 }
if (!function_exists('fun_global')) {

    function fun_global($procname, $params = null) {
        $ci 	= &get_instance();
        $ci->load->model('supper_admin');
        $result = $ci->supper_admin->call_procedure("$procname", $params);
        return $result;
    }


}
if(!function_exists('get_unit_price'))
{
	 function get_unit_price($pid,$qty,$cmid)
	{
         $path=api_url().'orderapi/get_quantity_range/pid/'.$pid.'/act/get_city_disc/cid/'.$cmid.'/format/json/';
	     $qty_city_disc=(array)curlget($path);
        
         $path=api_url().'orderapi/get_quantity_range/pid/'.$pid.'/act/get_sale_price/format/json/';
	     $sale_prc=curlget($path);

         $path=api_url().'orderapi/get_quantity_range/pid/'.$pid.'/act/get_qty_range/format/json/';
	     $qty_range=(array)curlget($path);

	     if($qty_range['scalar']=='Something Went Wrong')
	     {
           
	       if($qty_city_disc['scalar']=='Something Went Wrong')
	       { 
	       	 $unit_price=number_format($sale_prc->prodsellingprice,1,".","");
	       }else{
	       	  $unit_price=number_format(number_format($sale_prc->prodsellingprice,1,".","") + (number_format($sale_prc->prodsellingprice,1,".","") * $qty_city_disc['cityvalue']/100),1,".","");
	       }
	       
	     }else{
	     	  foreach ($qty_range as $key => $value) 
		     { 
		     	if($key=='qty1')
		     	{
		     		$range=explode('to', $value);
		     		if($qty>=$range[0] && $qty<=$range[1])
		     		{
		     			$qtyp=$qty_range['price'];
		     		}
		     		
		     	}

		     	if($key=='qty2')
		     	{
		     		$range=explode('to', $value);
		     		if($qty>=$range[0] && $qty<=$range[1])
		     		{
		     			$qtyp=$qty_range['price2'];
		     		}
		     		
		     	}

		     	if($key=='qty3')
		     	{ 
		     		$range=explode('to', $value);
		     		if($qty>=$range[0])
		     		{
		     			$qtyp=$qty_range['price3'];
		     		}
		     		
		     	}

		     }
		    if($qty_city_disc['scalar']!='Something Went Wrong' && !empty($qtyp))
	       {
	       	   $unit_price=number_format($sale_prc->prodsellingprice,1,".","") + number_format((number_format($sale_prc->prodsellingprice,1,".","") * ($qty_city_disc['cityvalue'])/100),1,".","");
	       	   $unit_price=number_format($unit_price - number_format((number_format($unit_price,1,".","") * ($qtyp)/100),1,".",""),1,".","");

	       }else if($qty_city_disc['scalar']!='Something Went Wrong' && empty($qtyp))
	       {
	       	   $unit_price=number_format($sale_prc->prodsellingprice,1,".","") + number_format((number_format($sale_prc->prodsellingprice,1,".","") * $qty_city_disc['cityvalue']/100),1,".","");
	       }else if($qty_city_disc['scalar']=='Something Went Wrong' && !empty($qtyp))
	       {
	       	$unit_price=number_format($sale_prc->prodsellingprice,1,".","") - number_format(number_format($sale_prc->prodsellingprice,1,".","") * $qtyp/100,1,".","");
	       }else{
	       	   $unit_price=number_format($sale_prc->prodsellingprice,1,".","");
	       }

	     }
	     return $unit_price;
	}
}


function catList2($cid)
{
      $parameter=array('catid'=>$cid);
      $path = api_url().'productlisting/categoryCount2/catid/'.$cid.'/format/json/';
      $response = curlget($path); 
      return $response;         //$response;                                                                                                                                           
    }

function filtercatList2($cid)
{
      $parameter=array('catid'=>$cid);
      $path = api_url().'productlisting/filtercategoryCount2/catid/'.$cid.'/format/json/';
      $response = curlget($path); 
      return $response;         //$response;                                                                                                                                           
    }

function catPricesearch($cid,$token){
    $tokenupdate=str_replace(" ", "+", $token);
    $path = api_url().'productlisting/categoryPricesearch/catid/'.$cid.'/token/'.$tokenupdate.'/format/json/';
    $response = curlget($path);
    return $response;                                                                                                                                           
    }

function catBrand2($cid){
      $parameter = array('act_mode'=>'getbrands','row_id'=>$cid,'pvalue'=>'');
      $path = api_url().'productlisting/categoryBrandes/format/json/';
      
    $response = curlpost($parameter,$path);
    //p($response);exit;
    if($response=='Something Went Wrong')
      $response=null;
    else
      $response=$response;
    return $response;                                                                                                                                           
    }

function snowinquiry($pid)
    {
      
        return '<a href="javascript:void(0);" data-href="'.$pid.'" style="outline:none;" class="inquiry" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil-square-o"></i></a>';

    }


function snowcompare($pid)
  {
      return '<a href="javascript:void(0);" class="Compare" id="compare'.$pid.'" onclick="comsnow('.$pid.')" data-href="'.$pid.'" title="add to Compare" ><i class="fa fa-files-o"></i></a>
      <input type="hidden" id="comproid'.$pid.'" class="comproid'.$pid.'" name="comproid'.$pid.'" value="'.$pid.'">';
  }
//----------start maincatdata----------
    function maincatList2($cid)
{
      $parameter=array('catid'=>$cid);
      $path = api_url().'productlisting/newcategoryCount2/catid/'.$cid.'/format/json/';
      $response = curlget($path); 
      return $response;         //$response;                                                                                                                                           
    }

function cart_items()
{
  $ci   = &get_instance();
  $uid=$ci->session->userdata('snowworldmember')->id;
  $path = api_url().'productlisting/cartcount/userid/'.$uid.'/format/json/';
  $response = curlget($path); 
  return $response->num; 
}

function get_product_details($promapid)
{
  $ci   = &get_instance();
  $parameter = array( 'act_mode'=>'product_details','','p_Id'=>$promapid,'');
  $data=$ci->supper_admin->call_procedureRow('proc_Addtowishlist', $parameter);
  return $data;
}

function truncate_number( $number, $precision = 1) {
    // Are we negative?
    $negative = $number / abs($number);
    // Cast the number to a positive to solve rounding
    $number = abs($number);
    // Calculate precision number for dividing / multiplying
    $precision = pow(10, $precision);
    // Run the math, re-applying the negative value to ensure returns correctly negative / positive
    return floor( $number * $precision ) / $precision * $negative;
}//----------- end maincatdata

function specialre($texts){
  
   //$text='yadav+RAHUL(from2';

    $array_from_to=array('Z1' => '+',
                         'Z2' => '-',
                         'Zy3' => '&',
                         'Z5' => '|',
                         'Z6' => '!',
                         'Z7' => '(',
                         'Z8' => ')',
                         'Z9' => '[',
                         'Zx1' => ']',
                         'Zx2' => '^',
                         'Zx3' => '"',
                         'Zx4' => '*',
                         'Zx5' => '~',
                         'Zx6' => '?',
                         'Zx7' => ':',
                         'Zx8' => "'",
                         'Zx9' => "<",
                         'Zy1' => ">",
                         'Zy2' => "=");
                                     
  $text = strtr($texts,$array_from_to);
   
   return $text;

}
// function encrypt($decrypted) { 

// $output = false;

// 		$encrypt_method = "AES-256-CBC";

// 			 $secret_key = 'asetrdtcb';
// 			 $secret_iv = 'jjedgdfvv';


// 				$key = hash('sha256', $secret_key);


// 				 $iv = substr(hash('sha256', $secret_iv), 0, 16);

// 				$orderid1 = openssl_encrypt($decrypted, $encrypt_method, $key, 0, $iv);
// 				$orderid = base64_encode($orderid1);

// return $orderid ;
// } 

// function decrypt($encrypted) {

// 		$output = false;
// 		$encrypt_method = "AES-256-CBC";
// 		//pls set your unique hashing key
// 		$secret_key = 'asetrdtcb';
// 		$secret_iv = 'jjedgdfvv';

// 		// hash
// 		$key = hash('sha256', $secret_key);

// 		// iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
// 		$iv = substr(hash('sha256', $secret_iv), 0, 16);

		 
// 		$orderid = openssl_decrypt(base64_decode($encrypted), $encrypt_method, $key, 0, $iv);
// 	 // $orderid = base64_decode($orderid1);
// //$orderid=$encrypted;
// return $orderid;
// }

function getsearchprodimage($proid,$promapid){
      $ci = &get_instance();
	  $parameter = array( 'act_mode'=>'get_productimage','row_id'=>$promapid,'feaid'=>$proid,'feavalue'=>'','catid'=>'');
	  $data=$ci->supper_admin->call_procedureRow('proc_feature', $parameter);
	  //p($data);exit;
	  return $data->Image;
}





//--------------------------------- start discount functions ------------------------------

function get_category_names($cat_ids) 
  {
     if($cat_ids)
     {
      
      $ci = &get_instance();
      /*$cat_name_list = $ci->db->query("select tcat.CatId,tcat.CatName from tbl_categorymaster as tcat 
                        inner join tbl_categorymap as tmap 
                       on tcat.CatId=tmap.childid where tmap.parentid=0 and tcat.CatId in ($cat_ids)"); */
        $cat_name_list = $ci->db->query("select catname from tbl_categorymaster where catid in ($cat_ids)");
      
      $cat_name_list =  $cat_name_list->result();
      $cat_name = array();
      foreach($cat_name_list as $cat_name)
      {
        $c_name[] = trim(ucwords(strtolower($cat_name->catname)));
      }
        return implode(', ',$c_name); 
     }
     else
     {
       return "NA";
     }
  }


  function get_product_names($prod_ids) 
  {
     
     if($prod_ids)
     {
      $ci = &get_instance();
      $prod_ids = unserialize($prod_ids);
      
      $all_names = array();
      $i = 0;
      foreach($prod_ids as $value)
      {
        //return $str = "select ProductName from tbl_productmapforlisting where prodtmappid = ".$value['promapid']." and ProId = ".$value['proid'];
        $prod_name_list = $ci->db->query("select ProductName from tbl_productmapforlisting where prodtmappid = ".$value['promapid']." and ProId = ".$value['proid']);
        $prod_name_list =  $prod_name_list->row_array();
        $all_names[$i++] = trim(ucwords(strtolower($prod_name_list['ProductName'])));
      }
      
      return implode(', ',$all_names); 
     }
     else
     {
       return "NA";
     }
  }


  function get_brand_names($brand_ids) 
{
     
     if($brand_ids)
     {
      
      $ci = &get_instance();
      $brand_name_list = $ci->db->query("select brandname from tbl_brand where id in ($brand_ids)");
      $brand_name_list =  $brand_name_list->result();
      $b_name = array();
      foreach($brand_name_list as $brand_name)
      {
        $b_name[] = trim(ucwords(strtolower($brand_name->brandname)));
      }
        return implode(', ',$b_name); 
     }
     else
     {
       return "NA";
     }
  }


//---------------------- end discount functions --------------------------------------------



function get_partialdiscountpercent($wallcoup,$finalprice)
{

  $partialpercent=($wallcoup*100)/$finalprice;

  return $partialpercent;

}

function get_partialdiscountpercentamt($wallcoup,$perpercent)
{

  $partialpercent=($wallcoup*$perpercent)/100;

  return $partialpercent;
}

function comboPrice(){
      $path = api_url().'productlisting/comboPrice/format/json/';
    $response = curlget($path);
    return $response;                      
}

function retailerData($user_id) 
{
    $ci = &get_instance();
    $parameter = array(
    'p_role' => '',
    'p_user' => $user_id,
    'p_menu' => '',
    'p_submenu' => '',
    'act_mode' => 'fetchretailerdetail',
    'start'=>'',
    'total_rows'=>''
    );    
    $response = $ci->supper_admin->call_procedureRow('proc_assignrole',$parameter); 
    return $response;    
}

function retailerTotalOrderAmt($user_id,$start,$end) 
{
    $ci = &get_instance();
    $parameter = array(
    'p_role' => $start,
    'p_user' => $user_id,
    'p_menu' => '',
    'p_submenu' => $end,
    'act_mode' => 'fetchtotalorderamt',
    'start'=>'',
    'total_rows'=>''
    );    
    $response = $ci->supper_admin->call_procedure('proc_assignrole',$parameter);
    $sumTotal=0; $sumTotal1=0; $sumTotal2=0; 
    foreach($response as $val) {
      if($val->incentive_bool=='+') {
        $sumTotal1+=$val->pro_amount*$val->pro_qty;
      }   
      if($val->incentive_bool=='-') {
        $sumTotal2+=$val->pro_amount*$val->pro_qty;
      }   
    }
    $sumTotal = $sumTotal1-$sumTotal2;
    return $sumTotal;    
}

function retailerIncentiveStatus($retailers,$start,$end) 
{
    $retailers=trim($retailers,',');
    $ci = &get_instance();
    $parameter = array(
    'p_role' => $start,
    'p_user' => '',
    'p_menu' => $retailers,
    'p_submenu' => $end,
    'act_mode' => 'fetchincentivestatus',
    'start'=>'',
    'total_rows'=>''
    );    
    $response = $ci->supper_admin->call_procedure('proc_assignrole',$parameter);
    $status = 'pending';
    foreach($response as $val) {  
      if($val->incentive_status=='paid') {
        $status = 'paid';
        break;
      }   
    }
    return $status;    
}

function empIncentive($saleTotal,$empSalary) 
{
    $ci = &get_instance();
    $parameter = array(
    'p_role' => '',
    'p_user' => '',
    'p_menu' => '',
    'p_submenu' => $empSalary,
    'act_mode' => 'fetchempincentives',
    'start' => '',
    'total_rows' => $saleTotal
    );    
    $response = $ci->supper_admin->call_procedureRow('proc_assignrole',$parameter);
    if($response->range_type==1) {
      $incentiveTotal=(($saleTotal*$response->range_incentive)/100);
    } else if($response->range_type==2) {
      $incentiveTotal=$response->range_incentive;
    }
    return $incentiveTotal;    
}

function checkreturnrefund($ordid,$userid) 
{
    $ci = &get_instance();
    $parameterwall=array('act_mode'=>'returnwalletlist','row_id'=>'','wa_orderid'=>$ordid,'wa_userid'=>$userid,'wa_type'=>'','wa_amt'=>'','wa_status'=>'');
    $response = $ci->supper_admin->call_procedureRow('proc_wallet',$parameterwall);
    if($response->countwallet==0) {
      $walletstatus=0;
    } else {
      $walletstatus=1;
    }
    return $walletstatus;    
}

function send_andriod_notification($deviceToken,$message,$notification_type)
{

$server_key="AAAAjzHhnZA:APA91bHP6eciCE1vsJgPjcgqEyk4jmHJf7nmS0SrgWJMxNlnY2-7Xb8AcOnwy7MrF8ZxVhSuNJlIuLDR-qpKXZKMH06n9ufDEkiFCFK-oMtVXo75qJ4NaR_VYmQXuZuQ6TAMbSxEp7Sa";

$data= array(
'message' => $message['message'],
'title' => $message['title'],
'subtitle'  => $message['title'],
'tickerText'=> $message['title'],
'vibrate' => 1,
'sound' => 1,
'notification_type'=>$notification_type
);

$url = 'https://fcm.googleapis.com/fcm/send';
     
$fields = array();
$fields['data'] = $data;
if(is_array($deviceToken))
{
  $fields['registration_ids'] = $deviceToken;
}else{
  $fields['to'] = $deviceToken;
}
//header with content_type api key
$headers = array(
  'Content-Type:application/json',
  'Authorization:key='.$server_key
);
   
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
$result = curl_exec($ch);
if ($result === FALSE) {
  die('FCM Send Error: ' . curl_error($ch));
}
curl_close($ch);
//print_r($result);

}

//CCAVENUE  Crypto

error_reporting(0);

function encrypt($plainText,$key)
{
    $secretKey = hextobin(md5($key));
    $initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
    $openMode = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '','cbc', '');
    $blockSize = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, 'cbc');
    $plainPad = pkcs5_pad($plainText, $blockSize);
    if (mcrypt_generic_init($openMode, $secretKey, $initVector) != -1)
    {
        $encryptedText = mcrypt_generic($openMode, $plainPad);
        mcrypt_generic_deinit($openMode);

    }
    return bin2hex($encryptedText);
}

function decrypt($encryptedText,$key)
{
    $secretKey = hextobin(md5($key));
    $initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
    $encryptedText=hextobin($encryptedText);
    $openMode = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '','cbc', '');
    mcrypt_generic_init($openMode, $secretKey, $initVector);
    $decryptedText = mdecrypt_generic($openMode, $encryptedText);
    $decryptedText = rtrim($decryptedText, "\0");
    mcrypt_generic_deinit($openMode);
    return $decryptedText;

}
//*********** Padding Function *********************

function pkcs5_pad ($plainText, $blockSize)
{
    $pad = $blockSize - (strlen($plainText) % $blockSize);
    return $plainText . str_repeat(chr($pad), $pad);
}

//********** Hexadecimal to Binary function for php 4.0 version ********

function hextobin($hexString)
{
    $length = strlen($hexString);
    $binString="";
    $count=0;
    while($count<$length)
    {
        $subString =substr($hexString,$count,2);
        $packedString = pack("H*",$subString);
        if ($count==0)
        {
            $binString=$packedString;
        }

        else
        {
            $binString.=$packedString;
        }

        $count+=2;
    }
    return $binString;
}

//********** Falcon Apiii ********
function callApi($api_type='', $api_activity='', $api_input='') {
        $data = array();
        $result = http_post_form("https://api.falconide.com/falconapi/web.send.rest", $api_input);
        return $result;
}

function http_post_form($url,$data,$timeout=20) {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url); 
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); 
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout); 
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($ch, CURLOPT_POST, 1); 
        curl_setopt($ch, CURLOPT_RANGE,"1-2000000");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
        curl_setopt($ch, CURLOPT_REFERER, @$_SERVER['REQUEST_URI']);

        $result = curl_exec($ch); 
        $result = curl_error($ch) ? curl_error($ch) : $result;
        curl_close($ch);
        return $result;
}

function getDBDateFormat( $param ){
	
	$date = new DateTime($param );
	return $date->format('Y-m-d');
}

function getFrontendDateFormat( $param ){
	
	$date = new DateTime($param );
	return $date->format('d/m/Y');
}

function getAmPm($param){
	
	return ($param >= 12) ?  'PM' : 'AM';  
}

function trimEachValue($param){
	$param = trim($param);
}

function getTotalPrice( $param ){
	
	return array_sum( $param );
}

function getSumAllArrayElement( $param ){
	
	return array_sum( $param );
}
 
function getAddonTotalPrice($param){
	
	$tmp_price_array = $tmp_qty_array = $data = [];
	 if( count($param) ) {
		 foreach( $param as $k => $v){
			
			 $tmp_price_array[] = $v['addonprice'];
			 $tmp_qty_array[] = $v['addonqty'];
			 
		 }
	 } 
	return $data[] = ['addon_price_array' => $tmp_price_array, 'addon_qty_array' => $tmp_qty_array];
}
// used in print template NOTE addoneprice KEY 
function getAddonTotalPricePrint($param){
	
	$tmp_price_array = $tmp_qty_array = $data = [];
	 if( count($param) ) {
		 foreach( $param as $k => $v){
			 
			 $tmp_price_array[] = $v['addoneprice'];
			 $tmp_qty_array[] = $v['addonqty'];
			 
		 }
	 }
	return $data[] = ['addon_price_array' => $tmp_price_array, 'addon_qty_array' => $tmp_qty_array];
}
// use in addon controller - add all functionality
function getAddonTotalPriceAddALL($param){
	
	$tmp_price_array = $tmp_qty_array = $data = [];
	 if( count($param) ) {
		 foreach( $param as $k => $v){
			
			 $tmp_price_array[] = $v['addon_price'];
			 $tmp_qty_array[] = $v['addon_id'];
			 
		 }
	 } 
	return $data[] = ['addon_price_array' => $tmp_price_array, 'addon_key_array' => $tmp_qty_array];
}

// use in addon controller - add all functionality
function getAddonkeyValue($param){
	
	$tmp_price_array = $tmp_qty_array = $data = [];
	 if( count($param) ) {
		 foreach( $param as $k => $v){
			
			 $tmp_id_array[] = $v['addon_id'];
			 $tmp_name_array[] = $v['addon_name'];
			 $tmp_image_array[] = $v['addon_image'];
			 $tmp_qty_array[] = $v['qty'];
			 $tmp_price_array[] = $v['addon_price'];
			 
		 }
	 } 
	return $data[] = [
		'addon_id_array' => $tmp_id_array,
		'addon_name_array' => $tmp_name_array,
		'addon_image_array' => $tmp_image_array,
		
		'addon_qty_array' => $tmp_qty_array,
		'addon_price_array' => $tmp_price_array
		];
}

function getPackageTotalPrice($param){
	
	$tmp_price_array = $tmp_qty_array = $data = $get_package_name_array = $get_package_desc_array = $get_package_img_array = [];
	
	 if( count($param) ) {
		 foreach( $param as $k => $v){
			 
			 $tmp_price_array[] = $v->package_price;
			 $tmp_qty_array[] = $v->qty;
			 $get_package_name_array[] = $v->package_name;
			 $get_package_desc_array[] = $v->package_description;
			 $get_package_img_array[] = $v->package_image;
		 }
	 }
	
	return $data[] = ['package_price_array' => $tmp_price_array, 'package_qty_array' => $tmp_qty_array, 
					  'package_name_array' => $get_package_name_array, 'package_desc_array' => $get_package_desc_array, 'get_package_img_array' => $get_package_img_array];
}

function calculatePriceByQty( $param, $param1 ){
	
	$tmp = [];
	if( count($param) == count($param1) ){
		
		foreach($param as $k => $v){
			
			$tmp[] = ( (int) $v * (int) $param1[ $k ] );
		}
		return $tmp;
	} 
	
}


function getHandlingChargeByNoOfPerson( $internethandlingcharge , $package_total_qty ) {
	
	return  ( (int) $internethandlingcharge * (int) $package_total_qty );
} 

    
	
function getTimeSlotInArray( $destinationType ){
	
	$trim_data = $hr_min_array = [];
    $trim_data = explode('-',$destinationType );
    array_shift( $trim_data );
	foreach(  $trim_data as $v){
		
		$tmp = explode(':',$v); 
		$hr_min_array[] = trim($tmp[0]);
		$hr_min_array[] = trim($tmp[1]);
	}
    return $hr_min_array;
}

function completeTimeSlot( $timeslot_array ){

		$new_timeslot_array = [];
	
		foreach( $timeslot_array as $k => $v) {
			
					if($k == 0 || $k == 2) {
							
							$new_timeslot_array[] = ( (int) $v >= 12 ) ?  'PM' : 'AM' ;
					}
		}
	
	return $timeslot_array[0] .':'. $timeslot_array[1] .' '. $new_timeslot_array[0] .' '. $timeslot_array[2] .':'. $timeslot_array[3] .' '. $new_timeslot_array[1];
	
}

function completeTimeSlotWithAmPm( $booking_date , $timeslot_array ){

	
		$new_timeslot_array = [];
	
		foreach( $timeslot_array as $k => $v) {
			
					if($k == 0 || $k == 2) {
							
							$new_timeslot_array[] = ( (int) $v >= 12 ) ?  'PM' : 'AM' ;
					}
		}
	
	return  date('g:i A' ,strtotime( $timeslot_array[0] .':'. $timeslot_array[1] ) ) . ' - ' . date('g:i A' ,strtotime( $timeslot_array[2] .':'. $timeslot_array[3] ) ); 
	
}

function completeTimeSlotWithAmPmAgent( $timeslot_array ){
	
	return  date('h:i:A' ,strtotime( $timeslot_array[0] .':'. $timeslot_array[1] ) ) . ' - ' . date('h:i:A' ,strtotime( $timeslot_array[2] .':'. $timeslot_array[3] ) ); 
	
}


?>
