<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Packagesstep extends MX_Controller {

   public function __construct() {
	   
  	$this->load->model("supper_admin");
	$this->load->library('session');
	session_start();
  }


//Select Package  


  public function packagesstep(){


    if( empty($this->session->userdata('txtDepartDate')) )
    {
		
      redirect(base_url());
    }
    else
    {
		
		//Select branch
		$siteurl= base_url();
		$parameterbranch=array(
				  'act_mode' =>'selectbranch',
				  'weburl' =>$siteurl,
				  'type'=>'web',

				  );

        $path=api_url().'selectsiteurl/branch/format/json/'; 
   		$data['branch']=curlpost($parameterbranch,$path); 



		//select banner images
		$parameterbanner=array(
				  'act_mode' =>'selectbannerimages',
				  'branchid' =>$data['branch']->branch_id,
				  'type'=>'web',

				  );

       $path=api_url().'selectsiteurl/banner/format/json/'; 
   	   $data['banner']=curlpost($parameterbanner,$path);    

		//Select Time slot 
	   $parametertimeslot=array(
			  'act_mode' =>'selectsestimeslot',
			  'branchid' =>$data['branch']->branch_id,
			  'destinationType' =>$this->session->userdata('destinationType'),
			  'type'=>'web',
			  );

       $path=api_url().'selecttimesloturl/timeslotses/format/json/'; 
   	   $data['timeslotses'] =curlpost($parametertimeslot,$path);
		
		if(($this->session->userdata['ppid'])>0){

            $parameterpac = array('act_mode' => 'select_packagepartner',
                'branchid' => $this->session->userdata['branch_id'],
                'type' => 'web',
                'packageqty' => '',
                'packageid' => '',
				'txtDepartDate' => getDBDateFormat($this->session->userdata['txtDepartDate'])
            );
        }else{

            $parameterpac = array('act_mode' => 'select_packageuserguest',
                'branchid' => $this->session->userdata['branch_id'],
                'type' => 'web',
                'packageqty' => 'null',
                'packageid' => 'null',
				'txtDepartDate' => getDBDateFormat($this->session->userdata['txtDepartDate'])	  
            );
        }

        $path = api_url()."Packages/packageselect/format/json/";
		$get_selected_package_by_date_timeslot = curlpost($parameterpac,$path);
		
		//p( $get_selected_package_by_date_timeslot );
		
		// unset the no. of visitor when comming back
		if( !empty( $this->session->userdata('package_total_qty') ) ){  //echo 1;
			
			$this->session->unset_userdata('package_total_qty');
		}
		
		if( !empty( $this->session->userdata('final_selected_package_data') ) ){  //echo 2;
			
			$this->session->unset_userdata('final_selected_package_data');
		}	
		

			
			if( !empty( $this->session->userdata('saving_price_promocode') ) ){   

				$this->session->unset_userdata('saving_price_promocode');
			}
			
			if( !empty( $this->session->userdata('total_promo_price') ) ){ 
				
				$this->session->unset_userdata('total_promo_price');
			}
			
			if( !empty( $this->session->userdata('promo_id') ) ){ 
			
				$this->session->unset_userdata('promo_id');				
			}			

			if( !empty( $this->session->userdata('promocode_name') ) ){ 
			
				$this->session->unset_userdata('promocode_name');				
			}									
			if( !empty( $this->session->userdata('saving_price_promocode') ) ){   

				$this->session->unset_userdata('saving_price_promocode');
			}
			
			if( !empty( $this->session->userdata('total_promo_price') ) ){ 
				
				$this->session->unset_userdata('total_promo_price');
			}
			
			if( !empty( $this->session->userdata('promo_id') ) ){ 
			
				$this->session->unset_userdata('promo_id');				
			}			

			if( !empty( $this->session->userdata('promocode_name') ) ){ 
			
				$this->session->unset_userdata('promocode_name');				
			}								
				
		$data['package_total_qty'] =  $this->session->userdata('ddAdult') ? $this->session->userdata('ddAdult') : 1;	
		
		// send to view page
		$data['current_time_slot_limit_value'] =  $this->session->userdata('time_slot_limit_value') ? $this->session->userdata('time_slot_limit_value')->dailyinventory_seats : 0;	
//p( $data['current_time_slot_limit_value'] );		
		$selected_time_slots = explode('-', $this->session->userdata['destinationType']);
		array_shift($selected_time_slots);
		
		$temp = array();
		foreach($selected_time_slots as $val){
			$hr_min_array = explode(':', $val);
			$temp = array_merge($temp, $hr_min_array);
		}

		$temp = array_map('trim',$temp);   
		
		
		$filter_data = [];
		foreach($get_selected_package_by_date_timeslot as $val){
			
			$val = array_map('trim',$val);
			if( $val['dailyinventory_from'] == $temp[0] && 
				$val['dailyinventory_minfrom'] == $temp[1] &&
				$val['dailyinventory_to'] == $temp[2] && 
				$val['dailyinventory_minto'] == $temp[3] ){
				//print_r($val);
				$filter_data[] = $val;
				
			}
		}
		$data['vieww'] = $filter_data;
		
		$this->session->set_userdata('filter_package_by_date_timeslot', $filter_data);

		//continue to 2nd stage
		
		if($this->input->post('submit')=='CONTINUE package'){





			
			
		    $this->session->unset_userdata('addonsval');
			
			$final_selected_package_data =  json_decode( $this->input->post('final_selected_data') );
			
			$tmp_price = $tmp_qty = [];
			

			
			$validate = $final_selected_package_data_validated = [];
			
			foreach( $this->session->userdata('filter_package_by_date_timeslot') as $k => $v ){
				//p($v);
				foreach( $final_selected_package_data as $keys => $values ){
					//p($values);
					// all key value must match therefore 12 
					
							if( count( array_intersect_assoc( $v, (array) $values ) ) >= 12 ) {
								//echo '########################';
								//p( $values );
								 $final_selected_package_data_validated[] = $values;
								//echo '########################';
								array_push($validate, 1);
									
							}else{
								
								array_push($validate, 0);
							}
						
				}
				
			}	
//p($final_selected_package_data_validated); 
//exit;
			foreach( $final_selected_package_data_validated as $v ){
				
				  $tmp_price[] = $v->package_price * $v->qty;
					$tmp_qty[] = $v->qty;
			}
						
			$data['package_total_price'] = getSumAllArrayElement( calculatePriceByQty( $tmp_price, $tmp_qty ) );
			//die;			
            $data = array(
                'uniqid' => uniqid(),
                'packageval' => 1,
                'packageimageval' => "",
                'packagenameval' => "",
                'packageqtyval' => getTotalPrice($tmp_qty) ? getTotalPrice($tmp_qty) : 0,   // getTotalPrice contain intiger element and sum all integer
                'packagepriceval' => getTotalPrice($tmp_price) ? getTotalPrice($tmp_price) : 0,
                'packagedescription' => "ddddddddddd"
            );
			
			//p( $final_selected_package_data_validated );
			// validate and check - harmful attack
			if( array_sum($validate) && ( array_sum($validate) && count($final_selected_package_data) ) ){
				
				$this->session->set_userdata($data);
				$this->session->set_userdata( 'final_selected_package_data', $final_selected_package_data_validated ); // to appy forward on all pages
				header("location:".base_url()."addones");
			}else{
				
				echo '<center>unauthorise access or invalid parameter</center>';
				exit;				
			}			
			
  		}

		 //$this->session->userdata('skiindia')
		$this->load->view("helper/header");
		$this->load->view("helper/topbar",$data);
		$this->load->view("packagesstep",$data);
		$this->load->view("helper/footer");

  	}}


}

?>