<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Packages extends MX_Controller {

    public function __construct() {
        $this->load->model("supper_admin");
		
        $this->load->library('session');
		session_start();
    }

    public function packages(){
    
        if($this->session->userdata('skiindiamember')->id){
            redirect(base_url());
        }else{ 

            $this->session->unset_userdata('ppromo_id');
            $this->session->unset_userdata('ppcodename');
            $this->session->unset_userdata('pp_type');
            $this->session->unset_userdata('pp_addonedata');
            $this->session->unset_userdata('pp_locationid');
            $this->session->unset_userdata('pp_branchid');
            $this->session->unset_userdata('pp_discount');
            $this->session->unset_userdata('pp_status');
            $this->session->unset_userdata('pp_expiry_date');
            $this->session->unset_userdata('pp_allowed_per_user');



            $this->session->unset_userdata('pp_start_date');
            $this->session->unset_userdata('pp_allowed_times');
            $this->session->unset_userdata('pp_createdon');
            $this->session->unset_userdata('pp_modifiedon');
            $this->session->unset_userdata('pp_logintype');
            $this->session->unset_userdata('pp_addoneqty');


            $this->session->unset_userdata('packageval');
            $this->session->unset_userdata('packageimageval');
            $this->session->unset_userdata('packagenameval');
            $this->session->unset_userdata('packageqtyval');
            $this->session->unset_userdata('packagepriceval');
            $this->session->unset_userdata('packagedescription');
            $this->session->unset_userdata('cartpackageimage');
            $this->session->unset_userdata('cartpackagename');
            $this->session->unset_userdata('cartpackageprice');
            $this->session->unset_userdata('cartsubtotal');

            $this->session->unset_userdata('cartaddoneid');
            $this->session->unset_userdata('cartaddoneimage');
            $this->session->unset_userdata('cartaddonname');
            $this->session->unset_userdata('cartaddonqty');
            $this->session->unset_userdata('cartaddonprice');
            $this->session->unset_userdata('orderlastinsertid');

            $this->session->unset_userdata('Promocodeprice');


            $this->session->unset_userdata('billing_name');
            $this->session->unset_userdata('billing_email');
            $this->session->unset_userdata('billing_tel');
            $this->session->unset_userdata('billing_address');
            $this->session->unset_userdata('billing_city');
            $this->session->unset_userdata('billing_state');

            $this->session->unset_userdata('billing_zip');
            $this->session->unset_userdata('billing_country');
			
			// unset all session variable if set
			// p( $this->session->userdata) ;
			
		
			if( !empty( $this->session->userdata('txtDepartDate') ) ){  //echo 7;
								
				$this->session->unset_userdata('txtDepartDate');																	  
			}
			
			if( !empty( $this->session->userdata('destinationType') ) ){  //echo 8;
				
				$this->session->unset_userdata('destinationType');																		
			}			
			
			if( !empty( $this->session->userdata('package_total_qty') ) ){  //echo 1;
				
				$this->session->unset_userdata('package_total_qty'); 	
			}
			
			if( !empty( $this->session->userdata('package_total_price') ) ){ //echo 2;
				
				$this->session->unset_userdata('package_total_price');					
			}
	
			if( !empty( $this->session->userdata('addon_total_price') ) ){   //echo 3;
				
				$this->session->unset_userdata('addon_total_price');									
			}

			if( !empty( $this->session->userdata('addon_total_qty') ) ){ //echo 4;

				$this->session->unset_userdata('addon_total_qty');				
			}

			if( !empty( $this->session->userdata('final_selected_addon_data') ) ){   //echo 5;
				
				$this->session->unset_userdata('final_selected_addon_data');								
			}			
					
			if( !empty( $this->session->userdata('final_cost_package_addon_handling') ) ){ // echo 6;
				
				$this->session->unset_userdata('final_cost_package_addon_handling');					
			}
				
//p( $this->session->userdata );
			
			if( !empty( $this->session->userdata('saving_price_promocode') ) ){   

				$this->session->unset_userdata('saving_price_promocode');
			}
			
			if( !empty( $this->session->userdata('total_promo_price') ) ){ 
				
				$this->session->unset_userdata('total_promo_price');
			}
			
			if( !empty( $this->session->userdata('promo_id') ) ){ 
			
				$this->session->unset_userdata('promo_id');				
			}			

			if( !empty( $this->session->userdata('promocode_name') ) ){ 
			
				$this->session->unset_userdata('promocode_name');				
			}						
// clear last time slot limit if set
			if( !empty( $this->session->userdata('time_slot_limit_value') ) ){ 

			$this->session->unset_userdata('time_slot_limit_value');				
			}			
			//p( $this->session->userdata );
			
            $siteurl= base_url();
            $parameterbranch=array(
                'act_mode' =>'selectbranch',
                'weburl' =>$siteurl,
                'type'=>'web',

            );

            $path=api_url().'selectsiteurl/branch/format/json/';
            $data['branch']=curlpost($parameterbranch,$path);

            $data = array(


                'countryid' => $data['branch']->countryid,
                'stateid' => $data['branch']->stateid,
                'cityid' => $data['branch']->cityid,
                'branch_id' => $data['branch']->branch_id,
                'locationid' => $data['branch']->branch_location
            );
            $this->session->set_userdata($data);
			
			$siteurl= base_url();
            $parameterbranch=array(
                'act_mode' =>'selectbranch',
                'weburl' =>$siteurl,
                'type'=>'web',

            );

            $path=api_url().'selectsiteurl/branch/format/json/';
            $data['branch']=curlpost($parameterbranch,$path);


//select banner images
            $parameterbanner=array(
                'act_mode' =>'selectbannerimages',
                'branchid' =>$data['branch']->branch_id,
                'type'=>'web',

            );

            $path=api_url().'selectsiteurl/banner/format/json/';
            $data['banner']=curlpost($parameterbanner,$path);


//Select Time
            $parametertimeslot=array(
                'act_mode' =>'selecttimeslot',
                'branchid' =>$data['branch']->branch_id,
                'destinationType' =>$this->session->userdata('destinationType'),
                'type'=>'web'
            );

            $path=api_url().'selecttimesloturl/timeslot/format/json/';
            $data['timeslot']=curlpost($parametertimeslot,$path);
//Select Time slot
            $parametertimeslot=array(
                'act_mode' =>'selecttimeslot',
                'branchid' =>$data['branch']->branch_id,
                'destinationType' =>$this->session->userdata('destinationType'),
                'type'=>'web',

            );

            $path12=api_url().'selecttimesloturl/timeslotses/format/json/';
            $data['timeslotses']=curlpost($parametertimeslot,$path12);
            $datenew=date("Y-m-d");
            $parameterdatediff = array( 'act_mode'=>'s_viewtimeslotdate',
                'nextDate'=>$datenew,
                'starttime'=>'',
                'branchid'=>$data['branch']->branch_id,
                'dailyinventory_to' =>'',
                'dailyinventory_seats' =>'',
                'dailyinventory_status' =>'',
                'dailyinventory_createdon' =>'',
                'dailyinventory_modifiedon' =>'',
                'dailyinventory_minfrom' =>'',
                'dailyinventory_minto' =>'',
            );

            $data['s_viewdate'] = $this->supper_admin->call_procedurerow('proc_crone_v',$parameterdatediff);

//Search PACKAGES
            if($_POST['ddAdult']!='')
            {

				$date=explode("-",$this->input->post('txtDepartDate'));
//13-12-2017 0-1-2   2017-12-26
                $dateval =preg_replace('/\s+/', '', $date[2]."-".$date[1]."-".$date[0]);

                $timeexplode=explode("-",$this->input->post('destinationType'));
                $timeexplodevalue=explode(":",$timeexplode[1]);
                $timeexplodevaluetime=$timeexplodevalue[0];

               // p($timeexplodevaluetime);
                $parameter2 = array( 'act_mode'=>'s_viewtimeslotf',
                    'nextDate'=>$dateval,
                    'starttime'=>$timeexplodevaluetime,
                    'branchid'=>$data['branch']->branch_id,
                    'dailyinventory_to' =>$this->input->post('txtDepartDate'),
                    'dailyinventory_seats' =>'',
                    'dailyinventory_status' =>'',
                    'dailyinventory_createdon' =>'',
                    'dailyinventory_modifiedon' =>'',
                    'dailyinventory_minfrom' =>'',
                    'dailyinventory_minto' =>'',
                );

                $data['s_viewtimeslot'] = $this->supper_admin->call_procedurerow('proc_crone_v',$parameter2);
				
				// set current time slot limit
				$this->session->set_userdata( 'time_slot_limit_value', $data['s_viewtimeslot'] );
                
//p($parameter2);                
//p( $data['s_viewtimeslot']);
//echo $data['s_viewtimeslot']->coun;

// echo ($data['s_viewtimeslot']->coun+$this->input->post('ddAdult'))."-----------".$data['s_viewtimeslot']->dailyinventory_seats;

//exit;

                if(($data['s_viewtimeslot']->coun+$this->input->post('ddAdult'))<$data['s_viewtimeslot']->dailyinventory_seats) {
                    $data = array(
                        'uniqid' => uniqid(),
                        'txtDepartDate' => trim($_POST['txtDepartDate']),
                        'destinationType' => trim($_POST['destinationType']),
                        'ddAdult' => trim($_POST['ddAdult'])
                    );

                    $this->session->set_userdata($data);
//pend($this->session->userdata);
                 redirect(base_url().'packagesstep');
                  //  redirect('packagesstep', 'refresh');
                }
                else
                {
                   // redirect(base_url()."packages");
                  //  pend("Hiiii");
                    $data['emsg']=" No Seats Avalilable";
                }


            /*    if(($data['s_viewtimeslot']->coun+$this->input->post('ddAdult'))<=$data['s_viewtimeslot']->dailyinventory_seats)
                {
                    $dateval=explode("/",$_POST['txtDepartDate']);

                    $_POST['txtDepartDate']=$dateval[1]."/".$dateval[0]."/".$dateval[2]."";

                    $data = array(
                        'uniqid' => uniqid(),
                        'txtDepartDate' => trim($_POST['txtDepartDate']),
                        'destinationType' => trim($_POST['destinationType']),
                        'ddAdult' => trim($_POST['ddAdult'])
                    );
					
                    $this->session->set_userdata($data);
//pend($this->session->userdata);

					redirect('packagesstep', 'refresh');

                   // redirect(base_url()."packagesstep");
                }
                else{
					
                    if(($data['s_viewtimeslot']->dailyinventory_seats-$data['s_viewtimeslot']->coun)<=0){
redirect(base_url()."packagesstep");
                     //   $data['emsg']=" No Seats Avalilable";
                    }
                    else{
						redirect(base_url()."packagesstep");
                       // $data['emsg']=" No Seats Avalilable";
                        //$data['emsg']=($data['s_viewtimeslot']->dailyinventory_seats-$data['s_viewtimeslot']->coun)." Seats Avalilable";
                    }     
				}
 */



//header("location:".base_url()."packages?searchid=MQ==");
            }


            //$this->session->userdata('skiindia')
            $this->load->view("helper/header");
            $this->load->view("helper/topbar",$data);
            $this->load->view("packagesfirststage",$data);
            $this->load->view("helper/footerpac");


        }
    }
	
	function getTimeSlotByDate(){

            $siteurl= base_url();
            $parameterbranch=array(
                'act_mode' =>'selectbranch',
                'weburl' =>$siteurl,
                'type'=>'web',

            );

            $path=api_url().'selectsiteurl/branch/format/json/';
            $data['branch']=curlpost($parameterbranch,$path);		
		
            $data = array(
                'countryid' => $data['branch']->countryid,
                'stateid' => $data['branch']->stateid,
                'cityid' => $data['branch']->cityid,
                'branch_id' => $data['branch']->branch_id,
                'locationid' => $data['branch']->branch_location
            );

            $this->session->set_userdata($data);
		
			//Select getTimeSlotByDate
            $parametertimeslot=array(
                'act_mode' => 'getTimeSlotByDate',
                'branchid' => $data['branch']->branch_id ? $data['branch']->branch_id : 1,
                'destinationType' => $this->session->userdata('destinationType') ? $this->session->userdata('destinationType') : 1,
                'type'=>'web',
				'booking_date' => getDBDateFormat($this->input->post('booking_date'))
            );

            $path=api_url().'selecttimesloturl/timeslot/format/json/';
	
            echo json_encode( curlpost($parametertimeslot,$path) );
		
	}


}//end of class
?>