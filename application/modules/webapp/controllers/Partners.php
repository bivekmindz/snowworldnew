<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
class Partners extends MX_Controller {

   public function __construct() {
      $this->load->model("supper_admin");
   $this->load->library('session');
    session_start();
  }
    public function dashboard()
    {   
       
        $this->load->view("agent/helper/header",$data);
        $this->load->view("agent/helper/leftbar",$data);
        $this->load->view("agent/partnerdashboard",$data);
        $this->load->view("agent/helper/footer");

    }

    
    /*public function filesystem($response_data,$session_param){
           $dir_name = "t"; 
              if(!is_dir($dir_name)){  
                mkdir($dir_name, 0777, true);
              }
              $concatstr = $session_param; 
              $file_path = $dir_name . "/temp_$concatstr.txt";
        
              if( file_exists($file_path) ){  //echo 'create fule';

                    file_put_contents($file_path, $response_data, LOCK_EX);

                    return true;

              }else{  
              
                $fh = fopen($file_path,'w');
                if( $fh && is_writable($file_path) ){
                    
                   fwrite( $fh, $response_data );
                   fclose($fh);
                   return true;
                }else{
                    
                   return false;
                }  
              }
    echo 'Successfully updated'; exit;
  }*/

    public function partnerusercreate(){
        if($this->input->post('submit')=='Register'){
            $siteurl= base_url();
            $parameterbranch=array(
                'act_mode' =>'selectbranch',
                'weburl' =>$siteurl,
                'type'=>'web',

            );

            $path=api_url().'selectsiteurl/branch/format/json/';
            $data['branch']=curlpost($parameterbranch,$path);
            $parameterregsel=array(
                'act_mode' =>'selectpartneremail',
                'title' =>'',
                'firstname' =>'',
                'lastname' =>'',
                'row_id' =>'',
                'logintype' =>'',
                'agencyname' =>'',
                'office_address' =>'',
                'city' =>'',
                'state' =>'',
                'pincode' =>'',
                'telephone' =>'',
                'mobile' =>'',
                'fax' =>'',
                'website' =>'',
                'email' =>'',
                'primarycontact_name' =>'',
                'primarycontact_email_mobile' =>'',
                'primarycontact_telephone' =>'',
                'secondrycontact_name' =>'',
                'secondrycontact_email_phone' =>'',
                'secondrycontact_telephone' =>'',
                'management_executives_name' =>'',
                'management_executives_email_phone' =>'',
                'management_executives_telephone' =>'',
                'branches' =>'',

                'yearofestablism' =>'',
                'organisationtype' =>'',
                'lstno' =>'',
                'cstno' =>'',
                'registrationno' =>'',
                'vatno' =>'',
                'panno' =>'',
                'servicetaxno' =>'',
                'tanno' =>'',
                'pfno' =>'',
                'esisno' =>'',
                'officeregistrationno' =>'',
                'exceptiontax' =>'',
                'otherexceptiontax' =>'',
                'bank_benificialname' =>'',
                'bank_benificialaccno' =>'',
                'bank_benificialbankname' =>'',
                'bank_benificialbranchname' =>'',
                'bank_benificialaddress' =>'',
                'bank_benificialifsc' =>'',
                'bank_benificialswiftcode' =>'',
                'bank_benificialibanno' =>'',
                'intermidiatebankname' =>'',
                'intermidiatebankaddress' =>'',

                'intermidiatebankswiftcode' =>'',
                'bank_ecs' =>'',
                'copypancart' => '',
                'copyservicetax' =>'',
                'copytan' =>'',
                'copyaddressproof' =>'',
                'emailstatus' =>'',
                'emailcurrency' =>'',
                'emailcountry' =>'',
                'username' =>$this->input->post('email'),
                'password' =>'',
                'countryid' => $data['branch']->countryid,
                'stateid' => $data['branch']->stateid,
                'cityid' => $data['branch']->cityid,
                'branch_id' => $data['branch']->branch_id,
                'locationid' => $data['branch']->branch_location,
                'bank_creditamount' =>'',
                'bank_agentcommision' =>'',
                'aadhaar_number' =>'',
                'ppid' =>$this->input->post('ppid'),
                'pppermission' =>$this->input->post('pppermission'),
                'type'=>'web',
            );

            $pathsel=api_url().'partnerapi/partnerregister/format/json/';
            $data['regsel']=curlpost($parameterregsel,$pathsel);


            if($data['regsel']->scalar!='Something Went Wrong')
            {
                $data['mesg']="Email Id Already Register";
            }
            else {


                $siteurl= base_url();
                $parameterbranch=array(
                    'act_mode' =>'selectbranch',
                    'weburl' =>$siteurl,
                    'type'=>'web',

                );

                $path=api_url().'selectsiteurl/branch/format/json/';
                $data['branch']=curlpost($parameterbranch,$path);



                //select banner images
                $parameterbanner = array(
                    'act_mode' => 'selectbannerimages',
                    'branchid' => $data['branch']->branch_id,
                    'type' => 'web',

                );

                $path = api_url() . 'selectsiteurl/banner/format/json/';
                $data['banner'] = curlpost($parameterbanner, $path);





                $message_new = '<table width="90%" style="line-height: 28px; font-family: sans-serif;"   >
<tr><td>Congratulations ' . $this->input->post("title").". " . ucfirst($this->input->post("first_name")).'&nbsp;'. $this->input->post("last_name") . ',</td></tr>

<tr><td>You have been successfully registered with  ' . $data['banner']->bannerimage_top3 . '.!</td></tr>

<tr><td>
You have been successfully registered with ' . $data['banner']->bannerimage_top3 . '. You can use ' . $this->input->post("email") . ' as your Login ID to access your ' . $data['banner']->bannerimage_top3 . ' Account online. 
</td></tr>
<tr><td>Please do not disclose/share your password to anyone.</td></tr>

<tr><td>Should you have any queries, please feel free to write to us on '.$data['banner']->bannerimage_branch_email.' or call us on '.$data['banner']->bannerimage_branch_contact.'.</td></tr>
<tr><td>Yours sincerely,<br>
'.$data['banner']->bannerimage_top3.' Team</td></tr>
</table>
';


                $from_email = $data['banner']->bannerimage_from ;
                $to_email = $this->input->post('email');
                //Load email library
                $this->load->library('email');
                $this->email->from($from_email,  $data['banner']->bannerimage_top3 );
                $this->email->reply_to($from_email,  $data['banner']->bannerimage_top3 );
                $this->email->to($to_email);
                $this->email->subject('' . $data['banner']->bannerimage_top3 . ' - Your Registration Details');
                $this->email->message($message_new);
                //Send mail
                $this->email->send();
                //  pend($this->email->print_debugger());


                $parameterreg = array(
                    'act_mode' => 'insertsubpartner',
                    'title' =>$this->input->post('title'),
                    'firstname' =>$this->input->post('first_name'),
                    'lastname' =>$this->input->post('last_name'),
                    'row_id' =>'',
                    'logintype' => '',
                    'agencyname' =>'',
                    'office_address' => '',
                    'city' => '',
                    'state' => '',
                    'pincode' => '',
                    'telephone' => '',
                    'mobile' => $this->input->post('mobile_number'),
                    'fax' => '',
                    'website' => '',
                    'email' => $this->input->post('email'),
                    'primarycontact_name' => '',
                    'primarycontact_email_mobile' => '',
                    'primarycontact_telephone' => '',
                    'secondrycontact_name' => '',
                    'secondrycontact_email_phone' => '',
                    'secondrycontact_telephone' => '',
                    'management_executives_name' => '',
                    'management_executives_email_phone' => '',
                    'management_executives_telephone' => '',
                    'branches' =>'',

                    'yearofestablism' =>'',
                    'organisationtype' =>'',
                    'lstno' => '',
                    'cstno' => '',
                    'registrationno' => '',
                    'vatno' =>'',
                    'panno' => '',
                    'servicetaxno' => '',
                    'tanno' => '',
                    'pfno' => '',
                    'esisno' => '',
                    'officeregistrationno' => '',
                    'exceptiontax' => '',
                    'otherexceptiontax' => '',
                    'bank_benificialname' => '',
                    'bank_benificialaccno' => '',
                    'bank_benificialbankname' => '',
                    'bank_benificialbranchname' => '',
                    'bank_benificialaddress' => '',
                    'bank_benificialifsc' => '',
                    'bank_benificialswiftcode' => '',
                    'bank_benificialibanno' => '',
                    'intermidiatebankname' => '',
                    'intermidiatebankaddress' => '',
                    'intermidiatebankswiftcode' => '',
                    'bank_ecs' => '',
                    'copypancart' =>'',
                    'copyservicetax' => '',
                    'copytan' => '',
                    'copyaddressproof' => '',
                    'emailstatus' => '',
                    'emailcurrency' => '',
                    'emailcountry' => '',
                    'username' => $this->input->post('email'),
                    'password' => base64_encode($this->input->post('password')),

                    'countryid' => $data['branch']->countryid,
                    'stateid' => $data['branch']->stateid,
                    'cityid' => $data['branch']->cityid,
                    'branch_id' => $data['branch']->branch_id,
                    'locationid' => $data['branch']->branch_location,
                    'bank_creditamount' =>'',
                    'bank_agentcommision' =>'',

                    'aadhaar_number' =>$this->input->post('aadhaar_number'),
                    'ppid' =>$this->session->userdata('ppid'),
                    'pppermission' =>$this->input->post('pppermission'),
                    'type' => 'web',
                );

                $path = api_url() . 'partnerapi/partnerregister/format/json/';
                $data['reg'] = curlpost($parameterreg, $path);


              $data['message'] ="Agent Added Sucessfully";
            }}
        $this->load->view("agent/helper/header",$data);
        $this->load->view("agent/helper/leftbar",$data);
        $this->load->view("agent/partnerusercreate",$data);
        $this->load->view("agent/helper/footer");

    }

  
 

}//end of class
?>
