<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
class Partner extends MX_Controller {

   public function __construct() {
      $this->load->model("supper_admin");
   $this->load->library('session');
    session_start();
   }
	
   public function dashboard(){   
        
        $this->load->view("agent/helper/header",$data);
        $this->load->view("agent/helper/leftbar",$data);
        $this->load->view("agent/partnerdashboard",$data);
        $this->load->view("agent/helper/footer");

    }
    // Agent wallet transaction  update
  public function agentwalletsummary(){
	  
	   //p( $this->session->userdata );
	   //p( $this->session->userdata );
	  
	   	if( empty( $this->session->userdata('final_selected_package_data') ) ){
            
			redirect(base_url('/agentpackagesstep'));
			
        }
	   
		$data['payment_flag'] = empty( $this->session->userdata( 'payment_flag' ) ) ? 0 :  array_sum( $this->session->userdata( 'payment_flag' ) ) ;
	   
	   	$data['before_payment_agent_wallet_amount'] = empty( $this->session->userdata( 'before_payment_agent_wallet_amount' ) ) ? 0 : $this->session->userdata( 'before_payment_agent_wallet_amount' ) ;
	   
	    $data['before_payment_total_purchase_amount'] = empty( $this->session->userdata( 'before_payment_total_purchase_amount' ) ) ? 0 : $this->session->userdata( 'before_payment_total_purchase_amount' ) ;

		// wallet
	  	if( ( $data['payment_flag'] == 0 ) ) {
			
			$data['to_payment_amount'] =  abs( $this->session->userdata( 'before_payment_after_wallet_deduction_to_pay_wallet' ) );
			
		}	  
	  
	    // wallet + ccavenue
	   	if( ( $data['payment_flag'] == 3 ) ) {  
			
			$data['to_payment_amount'] =  abs( $this->session->userdata( 'before_payment_after_wallet_deduction_to_pay_ccavenue' ) );
			
		}
		// ccavenue
		if ( $data['payment_flag'] == 2 ) { 
		
			$data['to_payment_amount'] =  abs( $this->session->userdata( 'before_payment_to_pay_ccavenue' ) );
			
		} 
			
	    $data['before_payment_after_wallet_deduction_agent_wallet_amount'] = empty( $this->session->userdata( 'before_payment_after_wallet_deduction_agent_wallet_amount' ) ) ? 0 : $this->session->userdata( 'before_payment_after_wallet_deduction_agent_wallet_amount' ) ;
		
        $this->load->view("agent/helper/header",$data);
        $this->load->view("agent/helper/leftbar",$data);
        $this->load->view("agent/agentwalletsummary",$data);
        $this->load->view("agent/helper/footer");	   	
   }

    //Agent Wallet transaction Amount sucess   update Raju Do not touch this code this is not related to you

    public function agentamountwalletsucess(){

        //echo "hii";

        $siteurl= base_url();
        $parameterbranch=array(
            'act_mode' =>'selectbranch',
            'weburl' =>$siteurl,
            'type'=>'web',

        );

        $path=api_url().'selectsiteurl/branch/format/json/';
        $data['branch']=curlpost($parameterbranch,$path);


        $parameterccgatway=array(
            'act_mode' =>'selectccavenue',
            'branchid' =>$data['branch']->branch_id,
            'type'=>'web',

        );

        $path=api_url().'ccavenue/ccavRequestHandler/format/json/';
        $data['ccavRequestHandler']=curlpost($parameterccgatway,$path);

//p($data['ccavRequestHandler']);
        $workingKey = $data['ccavRequestHandler']->pg_working_key;
        $encResponse = $_POST["encResp"];
        //pend($encResponse);//This is the response sent by the CCAvenue Server
        $rcvdString = decrypt($encResponse, $workingKey);        //Crypto Decryption used as per the specified working key.
        $order_status = "";
        $decryptValues = explode('&', $rcvdString);
        $dataSize = sizeof($decryptValues);
        for ($i = 0; $i < $dataSize; $i++) {
            $information = explode('=', $decryptValues[$i]);
            if ($i == 3) $order_status = $information[1];
        }



        $siteurl = base_url();
        $parameterbranch = array(
            'act_mode' => 'selectbranch',
            'weburl' => $siteurl,
            'type' => 'web',

        );

        $path = api_url() . 'selectsiteurl/branch/format/json/';
        $data['branch'] = curlpost($parameterbranch, $path);
        $parameterbanner = array(
            'act_mode' => 'selectbannerimages',
            'branchid' => $data['branch']->branch_id,
            'type' => 'web',

        );

        $path = api_url() . 'selectsiteurl/banner/format/json/';
        $data['banner'] = curlpost($parameterbanner, $path);


        $parameterccgatway=array(
            'act_mode' =>'selectccavenue',
            'branchid' =>$data['branch']->branch_id,
            'type'=>'web',

        );

        $path=api_url().'ccavenue/ccavRequestHandler/format/json/';
        $data['ccavRequestHandler']=curlpost($parameterccgatway,$path);


        //   p($decryptValues);
        //agentamount_add_new_credit
        //agentamount_add_new_debit

        $arr1 = explode("=", $decryptValues[1]);
        $arr2 = explode("=", $decryptValues[3]);
        $arr3 = explode("=", $decryptValues[8]);
        $arr4 = explode("=", $decryptValues[5]);
        $arr5 = explode("=", $decryptValues[0]);
        $arr6 = explode("=", $decryptValues[35]);
        $arr7 = explode("=", $decryptValues[18]);


        $orderdata = explode($data['ccavRequestHandler']->pg_agent_prefix.'_', $arr5[1]);

        $tracking_id = $arr1[1];

        $order_status = $arr2[1];
        $status_message = $arr3[1];
        $paymentmode = $arr4[1];
        $agent_id = $orderdata[1];
        $agent_amount = $arr6[1];

        $parameter = array('act_mode' => 'agentamount_add_new_credit',
            'Param1' =>  $this->session->userdata['pid'],
            'Param2' => $agent_amount,
            'Param3' => 'C',
            'Param4' => '0',
            'Param5' => 'Ag',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //pend($parameter);
        $response = $this->supper_admin->call_procedurerow('proc_agent_s', $parameter);

        $parameter = array('act_mode' => 'agentamount_add_new_history',
            'Param1' =>  $this->session->userdata['pid'],
            'Param2' => $tracking_id,
            'Param3' => $order_status,
            'Param4' => $status_message,
            'Param5' =>$paymentmode,
            'Param6' => $agent_amount,
            'Param7' => $response->last_id,
            'Param8' => '',
            'Param9' => '');

        $response = $this->supper_admin->call_procedurerow('proc_agent_s', $parameter);



        $mess = '<table width="90%" style="line-height: 28px; font-family: sans-serif;" >
	<tr><td>Payment Sucess at '.$data['banner']->bannerimage_top3 .' !</td></tr>

	<tr><td>Greetings from '.$data['banner']->bannerimage_top3 .'</td></tr>

	<tr><td>Payment Sucess</td></tr>
	<tr><td> join the fun at '.$data['banner']->bannerimage_top3 .'!</td></tr>

	<tr><td>Yours sincerely,<br>
	' . $data['banner']->bannerimage_top3 . ' Team</td></tr>
	</table>
	';
        $from_email = $data['banner']->bannerimage_from;
        $from = $from_email;
        $fromname = $data['banner']->bannerimage_top3;
        $to =  $arr7[1]; //Recipients list (semicolon separated)
        $api_key = $data['banner']->bannerimage_apikey;
        $subject = ('' . $data['banner']->bannerimage_top3 . ' - Your Request for Agent Sucess');


        $content =$mess;

        $mail1=array();
        $mail1['subject']= ($subject);
        $mail1['fromname']= ($fromname);
        $mail1['api_key'] = $api_key;
        $mail1['from'] = $from;
        $mail1['content']= ($content);
        $mail1['recipients']= $to;
        $apiresult = callApi(@$api_type,@$action,$mail1);
        redirect("agentsucesswallet");
    }
    //Agent Wallet transaction Amount sucess page  update Raju Do not touch this code this is not related to you
    public function agentsucesswallet(){

        $parameter = array('act_mode' => 'agentamount_select_new_history',
            'Param1' =>  $this->session->userdata['pid'],
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' =>'',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');

        $date['agentdatapayment'] = $this->supper_admin->call_procedurerow('proc_agent_s', $parameter);
//p( $date['agentdatapayment'] );   data not relevent
        $parameterreg = array(
            'act_mode' => 'selectsubpartner',
            'title' =>'',
            'firstname' =>'',
            'lastname' =>'',
            'row_id' =>'',
            'logintype' => '',
            'agencyname' =>'',
            'office_address' => '',
            'city' => '',
            'state' => '',
            'pincode' => '',
            'telephone' => '',
            'mobile' => '',
            'fax' => '',
            'website' => '',
            'email' => '',
            'primarycontact_name' => '',
            'primarycontact_email_mobile' => '',
            'primarycontact_telephone' => '',
            'secondrycontact_name' => '',
            'secondrycontact_email_phone' => '',
            'secondrycontact_telephone' => '',
            'management_executives_name' => '',
            'management_executives_email_phone' => '',
            'management_executives_telephone' => '',
            'branches' =>'',

            'yearofestablism' =>'',
            'organisationtype' =>'',
            'lstno' => '',
            'cstno' => '',
            'registrationno' => '',
            'vatno' =>'',
            'panno' => '',
            'servicetaxno' => '',
            'tanno' => '',
            'pfno' => '',
            'esisno' => '',
            'officeregistrationno' => '',
            'exceptiontax' => '',
            'otherexceptiontax' => '',
            'bank_benificialname' => '',
            'bank_benificialaccno' => '',
            'bank_benificialbankname' => '',
            'bank_benificialbranchname' => '',
            'bank_benificialaddress' => '',
            'bank_benificialifsc' => '',
            'bank_benificialswiftcode' => '',
            'bank_benificialibanno' => '',
            'intermidiatebankname' => '',
            'intermidiatebankaddress' => '',
            'intermidiatebankswiftcode' => '',
            'bank_ecs' => '',
            'copypancart' =>'',
            'copyservicetax' => '',
            'copytan' => '',
            'copyaddressproof' => '',
            'emailstatus' => '',
            'emailcurrency' => '',
            'emailcountry' => '',
            'username' => '',
            'password' => '',

            'countryid' => $data['branch']->countryid,
            'stateid' => $data['branch']->stateid,
            'cityid' => $data['branch']->cityid,
            'branch_id' => $data['branch']->branch_id,
            'locationid' => $data['branch']->branch_location,
            'bank_creditamount' =>($this->session->userdata('ppid') != 0) ? $this->session->userdata('ppid') : '',
            'bank_agentcommision' =>'',

            'aadhaar_number' =>'',
            'ppid' =>'',
            'pppermission' =>'',
            'type' => 'web',
        );


        $path = api_url() . 'partnerapi/partnerregisterpartner/format/json/';
        $data['reg'] = curlpost($parameterreg, $path);
// p( $data['reg'] );   data not relevent


        $this->load->view("agent/helper/header",$data);
        $this->load->view("agent/helper/leftbar",$data);
        $this->load->view("agent/agentwaletsucess",$data);
        $this->load->view("agent/helper/footer");
    }
    //Agent Wallet transaction Amount Fail  update Raju Do not touch this code this is not related to you
 public function agentamountwalletfail(){

     $siteurl= base_url();
     $parameterbranch=array(
         'act_mode' =>'selectbranch',
         'weburl' =>$siteurl,
         'type'=>'web',

     );

     $path=api_url().'selectsiteurl/branch/format/json/';
     $data['branch']=curlpost($parameterbranch,$path);


     $parameterccgatway=array(
         'act_mode' =>'selectccavenue',
         'branchid' =>$data['branch']->branch_id,
         'type'=>'web',

     );

     $path=api_url().'ccavenue/ccavRequestHandler/format/json/';
     $data['ccavRequestHandler']=curlpost($parameterccgatway,$path);

//p($data['ccavRequestHandler']);
     $workingKey = $data['ccavRequestHandler']->pg_working_key;
     $encResponse = $_POST["encResp"];
     //pend($encResponse);//This is the response sent by the CCAvenue Server
     $rcvdString = decrypt($encResponse, $workingKey);        //Crypto Decryption used as per the specified working key.
     $order_status = "";
     $decryptValues = explode('&', $rcvdString);
     $dataSize = sizeof($decryptValues);
     for ($i = 0; $i < $dataSize; $i++) {
         $information = explode('=', $decryptValues[$i]);
         if ($i == 3) $order_status = $information[1];
     }



     $siteurl = base_url();
     $parameterbranch = array(
         'act_mode' => 'selectbranch',
         'weburl' => $siteurl,
         'type' => 'web',

     );

     $path = api_url() . 'selectsiteurl/branch/format/json/';
     $data['branch'] = curlpost($parameterbranch, $path);
     $parameterbanner = array(
         'act_mode' => 'selectbannerimages',
         'branchid' => $data['branch']->branch_id,
         'type' => 'web',

     );

     $path = api_url() . 'selectsiteurl/banner/format/json/';
     $data['banner'] = curlpost($parameterbanner, $path);


     $parameterccgatway=array(
         'act_mode' =>'selectccavenue',
         'branchid' =>$data['branch']->branch_id,
         'type'=>'web',

     );

     $path=api_url().'ccavenue/ccavRequestHandler/format/json/';
     $data['ccavRequestHandler']=curlpost($parameterccgatway,$path);

     /*
     Array
     (
         [0] => order_id=agent_snow_7
         [1] => tracking_id=106310685039
         [2] => bank_ref_no=null
         [3] => order_status=Aborted
         [4] => failure_message=
         [5] => payment_mode=null
         [6] => card_name=null
         [7] => status_code=
         [8] => status_message=I have second thoughts about making this payment
         [9] => currency=INR
         [10] => amount=100.0
         [11] => billing_name=simerjeet singh
         [12] => billing_address=test addressdd
         [13] => billing_city=delhi
         [14] => billing_state=delhii
         [15] => billing_zip=110014
         [16] => billing_country=India
         [17] => billing_tel=9810668829
         [18] => billing_email=simer@elitehrpractices.com
         [19] => delivery_name=
         [20] => delivery_address=
         [21] => delivery_city=
         [22] => delivery_state=
         [23] => delivery_zip=
         [24] => delivery_country=
         [25] => delivery_tel=
         [26] => merchant_param1=
         [27] => merchant_param2=
         [28] => merchant_param3=
         [29] => merchant_param4=
         [30] => merchant_param5=
         [31] => vault=N
         [32] => offer_type=null
         [33] => offer_code=null
         [34] => discount_value=0.0
         [35] => mer_amount=100.0
         [36] => eci_value=
         [37] => retry=null
         [38] => response_code=
         [39] => billing_notes=
         [40] => trans_date=null
         [41] => bin_country=
     )
     */


     $arr1 = explode("=", $decryptValues[1]);
     $arr2 = explode("=", $decryptValues[3]);
     $arr3 = explode("=", $decryptValues[8]);
     $arr4 = explode("=", $decryptValues[5]);
     $arr5 = explode("=", $decryptValues[0]);
     $arr6 = explode("=", $decryptValues[35]);
     $arr7 = explode("=", $decryptValues[18]);


     $orderdata = explode($data['ccavRequestHandler']->pg_agent_prefix.'_', $arr5[1]);

     $tracking_id = $arr1[1];

     $order_status = $arr2[1];
     $status_message = $arr3[1];
     $paymentmode = $arr4[1];
     $agent_id = $orderdata[1];
     $agent_amount = $arr6[1];



     $parameter = array('act_mode' => 'agentamount_add_new_history',
         'Param1' =>  $this->session->userdata['pid'],
         'Param2' => $tracking_id,
         'Param3' => $order_status,
         'Param4' => $status_message,
         'Param5' =>$paymentmode,
         'Param6' => $agent_amount,
         'Param7' => '',
         'Param8' => '',
         'Param9' => '');

     $response = $this->supper_admin->call_procedurerow('proc_agent_s', $parameter);



     $mess = '<table width="90%" style="line-height: 28px; font-family: sans-serif;" >
<tr><td>You missed the FUN at '.$data['banner']->bannerimage_top3 .' !</td></tr>

<tr><td>Greetings from '.$data['banner']->bannerimage_top3 .'</td></tr>

<tr><td>Oops! By Some Reason You Missed the Payment</td></tr>
<tr><td>Book now and join the fun at '.$data['banner']->bannerimage_top3 .'!</td></tr>

<tr><td>Yours sincerely,<br>
' . $data['banner']->bannerimage_top3 . ' Team</td></tr>
</table>
';
     $from_email = $data['banner']->bannerimage_from;
     $from = $from_email;
     $fromname = $data['banner']->bannerimage_top3;
     $to =  $arr7[1]; //Recipients list (semicolon separated)
     $api_key = $data['banner']->bannerimage_apikey;
     $subject = ('' . $data['banner']->bannerimage_top3 . ' - Your Request for Agent Cancellation');


     $content =$mess;

     $mail1=array();
     $mail1['subject']= ($subject);
     $mail1['fromname']= ($fromname);
     $mail1['api_key'] = $api_key;
     $mail1['from'] = $from;
     $mail1['content']= ($content);
     $mail1['recipients']= $to;
     $apiresult = callApi(@$api_type,@$action,$mail1);

     redirect("agentwalletfail");
 }
    //Agent Wallet transaction Amount Fail   update Raju Do not touch this code this is not related to you

    public function agentwalletfail(){
    $parameter = array('act_mode' => 'agentamount_select_new_history',
        'Param1' =>  $this->session->userdata['pid'],
        'Param2' => '',
        'Param3' => '',
        'Param4' => '',
        'Param5' =>'',
        'Param6' => '',
        'Param7' => '',
        'Param8' => '',
        'Param9' => '');

    $date['agentdatapayment'] = $this->supper_admin->call_procedurerow('proc_agent_s', $parameter);


    $parameterreg = array(
        'act_mode' => 'selectsubpartner',
        'title' =>'',
        'firstname' =>'',
        'lastname' =>'',
        'row_id' =>'',
        'logintype' => '',
        'agencyname' =>'',
        'office_address' => '',
        'city' => '',
        'state' => '',
        'pincode' => '',
        'telephone' => '',
        'mobile' => '',
        'fax' => '',
        'website' => '',
        'email' => '',
        'primarycontact_name' => '',
        'primarycontact_email_mobile' => '',
        'primarycontact_telephone' => '',
        'secondrycontact_name' => '',
        'secondrycontact_email_phone' => '',
        'secondrycontact_telephone' => '',
        'management_executives_name' => '',
        'management_executives_email_phone' => '',
        'management_executives_telephone' => '',
        'branches' =>'',

        'yearofestablism' =>'',
        'organisationtype' =>'',
        'lstno' => '',
        'cstno' => '',
        'registrationno' => '',
        'vatno' =>'',
        'panno' => '',
        'servicetaxno' => '',
        'tanno' => '',
        'pfno' => '',
        'esisno' => '',
        'officeregistrationno' => '',
        'exceptiontax' => '',
        'otherexceptiontax' => '',
        'bank_benificialname' => '',
        'bank_benificialaccno' => '',
        'bank_benificialbankname' => '',
        'bank_benificialbranchname' => '',
        'bank_benificialaddress' => '',
        'bank_benificialifsc' => '',
        'bank_benificialswiftcode' => '',
        'bank_benificialibanno' => '',
        'intermidiatebankname' => '',
        'intermidiatebankaddress' => '',
        'intermidiatebankswiftcode' => '',
        'bank_ecs' => '',
        'copypancart' =>'',
        'copyservicetax' => '',
        'copytan' => '',
        'copyaddressproof' => '',
        'emailstatus' => '',
        'emailcurrency' => '',
        'emailcountry' => '',
        'username' => '',
        'password' => '',

        'countryid' => $data['branch']->countryid,
        'stateid' => $data['branch']->stateid,
        'cityid' => $data['branch']->cityid,
        'branch_id' => $data['branch']->branch_id,
        'locationid' => $data['branch']->branch_location,
        'bank_creditamount' =>($this->session->userdata('ppid') != 0) ? $this->session->userdata('ppid') : '',
        'bank_agentcommision' =>'',

        'aadhaar_number' =>'',
        'ppid' =>'',
        'pppermission' =>'',
        'type' => 'web',
    );

    $path = api_url() . 'partnerapi/partnerregisterpartner/format/json/';
    $data['reg'] = curlpost($parameterreg, $path);


    $this->load->view("agent/helper/header",$data);
    $this->load->view("agent/helper/leftbar",$data);
    $this->load->view("agent/agentwalletfail",$data);
    $this->load->view("agent/helper/footer");
}


    //Agent Amount sucess
   public function agentamountsucess(){

      /*
	   * agent order payment response
	   * 	success handler for wallet payment (1) 
	   *    success handler for ccavenue payment (2)
	   *    success handler for wallet + ccavenue payment (3)	    
	   */
	   
	   $agent_order_last_insert_id = '' ;

	   	if( empty( $this->session->userdata('final_selected_package_data') ) ) {
            
			redirect( base_url('/agentpackagesstep') );
        }
	   
	   	if( empty( $this->session->userdata('agent_order_last_insert_id') ) ){
			
			echo 'you order request failed ... please try again.' ;
		
		} else {
			
		 	$agent_order_last_insert_id = $this->session->userdata('agent_order_last_insert_id');
			
			$payment_flag = array_sum( $this->session->userdata('payment_flag') );
		}
	   
	   
		$siteurl= base_url();
        $parameterbranch=array(
            'act_mode' =>'selectbranch',
            'weburl' =>$siteurl,
            'type'=>'web',

        );

        $path=api_url().'selectsiteurl/branch/format/json/';
        $data['branch']=curlpost($parameterbranch,$path);


        $parameterccgatway=array(
            'act_mode' =>'selectccavenue',
            'branchid' =>$data['branch']->branch_id,
            'type'=>'web',

        );

        $path=api_url().'ccavenue/ccavRequestHandler/format/json/';
        $data['ccavRequestHandler']=curlpost($parameterccgatway,$path);
	   
	   
	   /*  $data['ccavRequestHandler']
	   
			stdClass Object
			(
				[pg_id] => 2
				[pg_branchid] => 1
				[pg_merchant_id] => 49646
				[pg_access_key] => AVUS71EG70AF13SUFA
				[pg_working_key] => B01CEF5819806E9315FF0CADC5D94051
				[pg_created] => 2017-07-03 04:46:02
				[pg_updated] => 2018-01-04 15:56:35
				[pg_status] => 1
				[pg_sucess_link] => http://192.168.1.65/snowworldnew/ordersucess
				[pg_fail_link] => http://192.168.1.65/snowworldnew/orderfail
				[pg_currency] => INR
				[pg_language] => EN
				[pg_prefix] => Snow
				[pg_agent_prefix] => Snowagentorder
				[pg_order_sucess_url] => http://192.168.1.65/snowworldnew/agentsucess
				[pg_order_fail_url] => http://192.168.1.65/snowworldnew/fail
				[pg_wallet_sucess_url] => http://192.168.1.65/snowworldnew/agentwalletsucess
				[pg_wallet_fail_url] => http://192.168.1.65/snowworldnew/agentwalletfail
				[pg_prefix_agent_wallet] => Snowagentwallet
			)
	*/
	   
        $workingKey = $data['ccavRequestHandler']->pg_working_key;
	   
        $encResponse = $_POST["encResp"];
	   
        //pend($encResponse);//This is the response sent by the CCAvenue Server
	   
        $rcvdString = decrypt($encResponse, $workingKey);        //Crypto Decryption used as per the specified working key.
	   
        $order_status = "";
	   
        $decryptValues = explode('&', $rcvdString);
	   
        $dataSize = sizeof($decryptValues);
	   
        for ($i = 0; $i < $dataSize; $i++) {
			
            $information = explode('=', $decryptValues[$i]);
			
            if ($i == 3) $order_status = $information[1];
        }
        
  		$siteurl = base_url();
        $parameterbranch = array(
            'act_mode' => 'selectbranch',
            'weburl' => $siteurl,
            'type' => 'web',

        );

        $path = api_url() . 'selectsiteurl/branch/format/json/';
        $data['branch'] = curlpost($parameterbranch, $path);
         $parameterbanner = array(
            'act_mode' => 'selectbannerimages',
            'branchid' => $data['branch']->branch_id,
            'type' => 'web',

        );

        $path = api_url() . 'selectsiteurl/banner/format/json/';
        $data['banner'] = curlpost($parameterbanner, $path);

        
		$parameterccgatway=array(
			'act_mode' =>'selectccavenue',
			'branchid' =>$data['branch']->branch_id,
			'type'=>'web',

		);

		$path=api_url().'ccavenue/ccavRequestHandler/format/json/';
		$data['ccavRequestHandler']=curlpost($parameterccgatway,$path);

      
     		//p($decryptValues);
            //agentamount_add_new_credit
            //agentamount_add_new_debit

       /* agnet Mail start here and order update and  */
       $data['orderlastinsertid'] = $this->session->userdata['agent_order_last_insert_id'];
       $orderdisplay = array('act_mode' => 'select_order',
           'orderid' => $data['orderlastinsertid'],
            'type' => 'web', );
       $path = api_url() . "Ordersucess/selectorder/format/json/";
       $data['orderdisplaydataval'] = curlpost($orderdisplay, $path);
      // $arr = (array)$data['orderdisplaydataval'];

       $date_array1 = explode("-", $data['orderdisplaydataval']->addedon); // split the array
       $var_day1 = $date_array1[2]; //day seqment
       $var_month1 = $date_array1[1]; //month segment
       $var_year1 = $date_array1[0]; //year segment
       $new_date_format1 = strtotime("$var_year1-$var_month1-$var_day1"); // join them together

       $d1 = date(' jS F Y', $new_date_format1);


       $date = '19:24:15 ';
       $d2 = date('h:i:s a ');

       $date_array = explode("/", $data['orderdisplaydataval']->departuredate); // split the array
       $var_day = $date_array[0]; //day seqment
       $var_month = $date_array[1]; //month segment
       $var_year = rtrim($date_array[2]," "); //year segment
       $new_date_format = strtotime("2017-$var_month-$var_day"); // join them together

       $input = ("$var_year$var_month$var_day");

       $d3 = date("D", strtotime($input)) . "\n";


       //$d4= date(' jS F Y', $new_date_format);

       $row['date'] = trim($var_year, " ") . "-" . $var_month . "-" . $var_day; // this is for example only - comment out when tested
       $d4 = date("F j, Y", strtotime($row['date']));

//echo date("H:i:s") . "\n";)


       $to_email = $data['orderdisplaydataval']->billing_email;
       $rrrval = $data['orderdisplaydataval']->pacorderid;
       $rproduct = $data['orderdisplaydataval']->packproductname;
       $parametertearms = array(
           'act_mode' => 'selecttearms',
           'branchid' => $data['branch']->branch_id,
           'type' => 'web',

       );

       $path = api_url() . 'selectsiteurl/bannern/format/json/';
       $data['tearmsgatway'] = curlpost($parametertearms, $path);
       $mess = '<table width="90%" style="line-height: 28px; font-family: sans-serif;" >
<tr><td>Dear  ' . $data['orderdisplaydataval']->billing_name . ',</td></tr>

<tr><td>Greetings from ' . $data['banner']->bannerimage_top3 . '.!</td></tr>

<tr><td>
Thank you for choosing our Services. The  details are as follows:
Transaction ID:' . $data['orderdisplaydataval']->ticketid . '
</td></tr>
<tr><td>All guests are requested to report 30 minutes prior to the Session Time and collect your Entry Pass from the Ticket Counter.</td></tr>

<tr><td>We request the guest to carry a print out of this E &minus; voucher if possible. Should you have any queries, please feel free to write to us on ' . $data['banner']->bannerimage_branch_email . ' or call us on ' . $data['banner']->bannerimage_branch_contact . '.</td></tr>


<tr><td>We look forward to welcoming you the next time you visit us at ' . $data['banner']->bannerimage_top3 . '.</td></tr>




<tr><td>Yours sincerely,<br>
' . $data['banner']->bannerimage_top3 . ' Team</td></tr>
</table>';

       $date_array1 = explode("-", $data['orderdisplaydataval']->departuredate); // split the array
       $var_day1 = $date_array1[2]; //day seqment
       $var_month1 = $date_array1[1]; //month segment
       $var_year1 = rtrim($date_array1[0]," "); //year segment
       $new_date_format1 = strtotime("$var_year1-$var_month1-$var_day1"); // join them together


       $d1 = date(' jS M Y', strtotime($data['orderdisplaydataval']->departuredate) );
       $data[ 'd1' ] = $d1;

       $date = '19:24:15 ';
       $d2 = date('h:i:s a ');

       $date_array = explode("/", $data['orderdisplaydataval']->departuredate); // split the array
       $var_day = $date_array[0]; //day seqment
       $var_month = $date_array[1]; //month segment
       $var_year = rtrim($date_array[2]," "); //year segment
       $new_date_format = strtotime( date('Y-m-d', strtotime($data['orderdisplaydataval']->addedon) ) ); // join them together

       $input = ("$var_year$var_month$var_day");

       $d3 = date( "D", strtotime( date('Y-m-d', strtotime($data['orderdisplaydataval']->departuredate) ) ) ) . "\n";
       $data['d3'] = $d3 ;


       $prepare_time_slot_from_date = $data['orderdisplaydataval']->departuretime .':'.$data['orderdisplaydataval']->frommin .' '.$data['orderdisplaydataval']->txtfromd .' - '.
           $data['orderdisplaydataval']->tohrs.':'.$data['orderdisplaydataval']->tomin.' '.$data['orderdisplaydataval']->txttod;
       $data['prepare_time_slot_from_date'] =$prepare_time_slot_from_date;

       $d4 = date(' jS M Y', $new_date_format);
       $data['d4'] = $d4;


       $message_pdf = '<html>
<head>
    <title> Booking</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
    <!-- Save for Web Slices (Untitled-1) -->
    <table width="953" height="967" cellpadding="0" cellspacing="0"  style="border:solid 1px #37ace1;font-family:Arial, Helvetica, sans-serif;color:#555;">
    <tr>
    <td colspan="3">
    <table width="953">
    <tr>
    <td border="0" width="250" valign="top" style="padding-top:20px;padding-left:20px;"><img src="' . base_url("assets/admin/images/") . "/" . $data['banner']->bannerimage_logo . '" alt="" width="220" ></td>
    <td border="0" width="700" style="text-align:center;">
    <table width="800" style="margin:20px 20px;line-height:25px;">
    <tr>
    <td style="color: #37ACE1;font-weight:700;padding:0px 0px;font-size:29px;text-align:center;">
    <strong>' . $data['banner']->bannerimage_top1 . '</strong>
                
                </td>
                </tr>
                <tr><td style="padding:8px 0px 5px;font-size:18px;font-weight:700;"><strong>' . $data['banner']->bannerimage_top2 . '</strong></td></tr>
                
                <tr><td style="color: #444;font-weight:700;padding:0px 0px 0px;font-size:22px;"><strong>' . $data['banner']->bannerimage_top4 . '</strong></td></tr>
               <tr><td style="color: #444;font-weight:700;padding:0px 0px 0px;font-size:22px;"><strong>' . $data['banner']->bannerimage_gstno . '</strong></td></tr>
                </table>
                </td>
                
                </tr>
                </table>
                </td>
                
    </tr>
    <tr><td colspan="3" style="color: #37ACE1;font-weight:600;padding:8px 0px 8px;font-size:25px;text-align:center"><strong>Booking Confirmation Details</strong></td></tr>
    <tr><td colspan="3" style="border-top:dashed 3px #37ace1;"><td></tr>
      <tr>
      <td colspan="3">
      <table width="1000" cellpadding="0" cellspacing="0" style="margin:15px 0;">
      <tr>
      <td width="475">
      <table style="margin:35px 35px 35px 35px;">
      <tr>
      <td style="line-height:25px;">
                    <h3 style="font-size:23px;">Venue Details</h3>
                    
                    </td>
                    </tr>
                    <tr><td style="font-weight:normal;line-height:23px;font-size:18px;"> ' . $data["branch"]->branch_add . '</td></tr>
                    <tr><td style="font-size:18px;"><span style="color: rgb(79, 193, 242);">Email :</span> ' . $data['banner']->bannerimage_branch_email . '</td></tr>
                    <tr><td style="font-size:18px;"><span style="color: rgb(79, 193, 242);">Phone :</span> ' . $data['banner']->bannerimage_branch_contact . '</td></tr>
                    
                </table>
                </td>
      <td style="border-left:dashed 3px #37ace1;"></td>
      <td width="475">
      <table style="margin:35px 35px 35px 35px;">
      <tr>
      <td style="line-height:25px;">
                    <h3 style="font-size:23px;">Guest Details</h3>
                    
                    </td>
                    </tr>   
                    <tr><td style="font-weight:normal;line-height:23px;font-size:18px;">' . $data['orderdisplaydataval']->billing_name . ' <br>'.$data['orderdisplaydataval']->billing_address.' </td></tr>
                    <tr><td style="font-size:18px;"><span style="color: rgb(79, 193, 242);">Email :</span> ' . $data['orderdisplaydataval']->billing_email . '</td></tr>
                    <tr><td style="font-size:18px;"><span style="color: rgb(79, 193, 242);">Phone :</span> ' . $data['orderdisplaydataval']->billing_tel . '</td></tr>
                </table>
                </td>
      </tr>
      <tr>
      <td colspan="3">
       <table width="1000" cellpadding="0" cellspacing="0" style="margin:15px 0px;">
       <tr>
       <td colspan="3" style="border-top:dashed 3px #37ace1;"></td>
       </tr>
       <tr>
       <td><table width="475" style="margin:15px 0;border-right:dashed 3px #37ace1;">
                
                <tr><td style="margin-bottom:0;font-size:15px;margin-top:5px;text-align:center;font-weight:bold;">Booking No.</td>
                <td style="margin-bottom:0;font-size:15px;margin-top:5px;text-align:center;font-weight:bold;">Booking Date.</td></tr>
                
                <tr><td style="text-align:center;margin-top:5px;font-size:13px;">' . $data['orderdisplaydataval']->ticketid . '</td>
                <td style="text-align:center;margin-top:5px;font-size:13px;">' . $d4 . '</td></tr></table></td>
                <td width="3"></td>
                <td><table width="475" style="margin:15px 0;">
                
                <tr><td style="margin-bottom:0;font-size:15px;margin-top:5px;text-align:center;font-weight:bold;">Visit Date.</td>
                <td style="margin-bottom:0;font-size:15px;margin-top:5px;text-align:center;font-weight:bold;">Session Time</td></tr>
                <tr><td style="text-align:center;margin-top:5px;font-size:13px;">' . $d1 . '</td>
                <td style="text-align:center;margin-top:5px;font-size:13px;">' . $data['prepare_time_slot_from_date'] . '</td></tr></table></td>
       </tr>
       <tr>
       <td colspan="3" style="border-top:dashed 3px #37ace1;"></td>
       </tr>
       </table>
      </td></tr>
      </table>
      </td>
      </tr>
      <tr>
      <td colspan="3">
        <table width="860" cellpadding="0" cellspacing="0" style="border:solid 1px #37ace1;font-family:Arial, Helvetica, sans-serif;color:#555;margin:0px 50px 10px 50px; ">
        <tr>
        <td width="215" style="background:#444;text-align:center;font-size:18px;color:#fff;padding:15px 0px;font-weight:bold;">
        Items
</td>
        <td width="215" style="background:#444;text-align:center;font-size:18px;color:#fff;padding:15px 0px;font-weight:bold;">Qty
</td>
        <td width="215" style="background:#444;text-align:center;font-size:18px;color:#fff;padding:15px 0px;font-weight:bold;">Cost</td>
        <td width="215" style="background:#444;text-align:center;font-size:18px;color:#fff;padding:15px 0px;font-weight:bold;">Total</td>
        </tr>
       
        ';


       $message_pdf1 = $package_total_price = $addon_total_price_with_quantity = "";
       $total_array = $addon_data = [];
       $total = 0;

       $package_name_array = explode('#', $data['orderdisplaydataval']->packproductname );
       $package_img_array = explode('#', $data['orderdisplaydataval']->packimg );
       $package_qty_array = explode('#', $data['orderdisplaydataval']->package_qty );
       $package_price_array = explode('#', $data['orderdisplaydataval']->package_price );

       $package_total_price = $total_array[] = getSumAllArrayElement( calculatePriceByQty( explode('#', $data['orderdisplaydataval']->package_price ), explode('#', $data['orderdisplaydataval']->package_qty ) ) );

       $addon_data =  getAddonTotalPricePrint( $data['orderaddonedisplaydata'] ) ;
       //p( $addon_data['addon_price_array'] );
//p($addon_data);
       $data['addon_price_with_quantity'] = 0;
       $addon_total_price_with_quantity = $total_array[] =  getSumAllArrayElement( calculatePriceByQty( $addon_data['addon_price_array'], $addon_data['addon_qty_array'] ) );
//p( $addon_total_price_with_quantity );
       $data['addon_price_with_quantity'] = $addon_total_price_with_quantity;
//p( $data['orderdisplaydataval']->internethandlingcharges );
       $total_array[] = $data['orderdisplaydataval']->internethandlingcharges;
       //echo '<pre>';
       //print_r($data['orderdisplaydataval']);
       $total = ($data['orderdisplaydataval']->promocodeprice) ?   ( getSumAllArrayElement( $total_array ) - $data['orderdisplaydataval']->promocodeprice )  :  getSumAllArrayElement( $total_array );
//p($total_array);
       foreach( $package_name_array as $ky => $val){

           $message_pdf1 .= '<tr><td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px">
       ' . $val . ' Package
</td>
        <td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px">' . ( $package_qty_array[ $ky ] ) . '
</td>
        <td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px"> ₹ ' . ( $package_price_array[ $ky ] ) . '</td>
        <td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px"> ₹ ' . ( $package_qty_array[ $ky ] * $package_price_array[ $ky ] ) . '</td>
        </tr>';
       }

///p($data['orderaddonedisplaydata']);
       foreach ($data['orderaddonedisplaydata'] as $a => $b) {
           if ($b != 'Something Went Wrong') {
               $message_pdf1 .= '<tr><td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px">
        ' . $b["addonename"] . '
</td><td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px">' . $b["addonqty"] . '
</td>
        <td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px"> ₹ ' . $b["addoneprice"] . '</td>
        <td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px"> ₹ ' . ($b["addoneprice"] * $b["addonqty"]) . '</td>
        </tr>';
           }
       }

       $message_pdf2 = '<tr>
        <td colspan="4">
        <table width="760" cellpadding="0" cellspacing="0" style="border-top:solid 1px #37ace1;font-family:Arial, Helvetica, sans-serif;color:#555;margin:10px 50px 10px 50px; ">
        <tr>
        <td width="380" style="text-align:left;font-size:18px;color:#000;padding:10px 0px 10px 40px">Price:</td>
        <td width="380" style="text-align:right;font-size:18px;color:#000;padding:10px 40px 10px 0px"> ₹ ' . ($package_total_price + $addon_total_price_with_quantity) . '</td>
        </tr>
        </table>
        </td>
        </tr>
        
        </table>
      </td>
      </tr>
      
      <tr>
      <td colspan="3">
      <table width="860" cellpadding="0" cellspacing="0" style="border:solid 1px #37ace1;font-family:Arial, Helvetica, sans-serif;color:#555;margin:0px 50px 10px 50px; ">
     <tr><td width="430" style="text-align:left;font-size:18px;color:#000;padding:10px 0px 10px 40px">Discount Amount</td><td width="430" style="text-align:right;font-size:18px;color:#000;padding:10px 40px 10px 0px"> ₹ ' . $data['orderdisplaydataval']->promocodeprice . '</td></tr>
      </table>
      </td>
      </tr>
      
      <tr>
      <td colspan="3">
      <table width="860" cellpadding="0" cellspacing="0" style="border:solid 1px #37ace1;font-family:Arial, Helvetica, sans-serif;color:#555;margin:0px 50px 10px 50px; ">
     <tr><td width="430" style="text-align:left;font-size:18px;color:#000;padding:10px 0px 10px 40px">Internet Handling Charges</td><td width="430" style="text-align:right;font-size:18px;color:#000;padding:10px 40px 10px 0px"> ₹ ' . $data['orderdisplaydataval']->internethandlingcharges . '</td></tr>
      </table>
      </td>
      </tr>
      <tr>
      <td colspan="3">
      <table width="860" cellpadding="0" cellspacing="0" style="border:solid 1px #37ace1;font-family:Arial, Helvetica, sans-serif;color:#555;margin:0px 50px 30px 50px; ">
     <tr><td width="430" style="text-align:left;font-size:18px;color:#000;padding:10px 0px 10px 40px">Total</td><td width="430" style="text-align:right;font-size:18px;color:#000;padding:10px 40px 10px 0px"> ₹ ' . $total . '</td></tr>
      </table>
      </td>
      </tr>   
      <tr>
     <td colspan="3">
      <table width="475" style="margin:15px 0px 15px 50px;">
      <tr><td style="font-size:17px;font-weight:bold;color:#000;padding:4px 0px;"></td></tr>
      <tr><td style="font-size:17px;font-weight:bold;color:#000;padding:4px 0px;">PAN No. : AAFFC9989B</td></tr>
      <tr><td style="font-size:17px;font-weight:bold;color:#000;padding:4px 0px;">Encl. Terms and Conditions</td></tr>
      </table>
     </td>
      </tr>
    </table>
    <table width="953" cellpadding="0" cellspacing="0"  style="border:solid 1px #37ace1;font-family:Arial, Helvetica, sans-serif;color:#555;">
    <tr>
   <td>
    <table>
     <tr>
     <td width="250" valign="top" style="padding:15px 0px 0px 15px;"><img src="' . base_url("assets/admin/images/") . "/" . $data['banner']->bannerimage_logo . '" alt="" width="220"></td>
     <td style="text-align:center;">
    <table width="453" style="margin:20px 20px;line-height:25px;">
    <tr>
    <td width="453" style="color: #37ACE1;font-weight:700;padding:0px 0px;font-size:29px;text-align:center;">
    <strong>' . $data['banner']->bannerimage_top1 . '</strong>
                
                </td>
                </tr>
                <tr><td style="padding:8px 0px 5px;font-size:18px;font-weight:700;"><strong>' . $data['banner']->bannerimage_top2 . '</strong></td></tr>
               
               
                <tr><td style="color: #37ACE1;font-weight:600;padding:15px 0px 0px;font-size:25px;"><strong>Terms and Conditions</strong></td></tr>
                </table>
                </td>
                 <td width="250" valign="top" style="padding:15px 15px 0px 0px;"><img src="' . base_url("assets/admin/images/") . "/" . $data['banner']->bannerimage_logo . '" alt="" width="220"></td>
                </tr>
                <tr>
                <td colspan="3">
                <table width="940" style="margin-left:50px;margin-top:10px;margin-bottom:80px;color:#000;font-size:16px;line-height:25px;">';


       $i=1;foreach ($data['tearmsgatway'] as $key => $value11) {
           $message_pdf2 .= '
   <tr>
                                    <td width="30" valign="top">'.$i.'.</td>
                                    <td width="900" valign="top">'.$value11['term_name'].'</td>
                                </tr>';
           $i++;}
       $message_pdf2 .= '
   
    
    <tr>
    <td width="30" valign="top">&nbsp;</td>
    <td width="900" valign="top">
    <table style="font-size:16px;line-height:25px;">
    
    </table>
    </td>
    </tr>
    
    
    </table>
                </td>
                </tr>
                </table>
     </td>
    
     </tr>
     </table>

   </td>
    </tr>
    
    
   
    </table>
    <!-- End Save for Web Slices -->
</body>
</html>';


       $pdf_name = time() . "download.pdf";
       base_url("assets/admin/pdfstore/" . $pdf_name);
       $this->load->helper('file');
        $pdfFilePath = FCPATH . "assets/admin/pdfstore/" . $pdf_name;

       $this->load->library('m_pdf');

       $this->m_pdf->pdf->WriteHTML($message_pdf . $message_pdf1 . $message_pdf2);
       $a = $this->m_pdf->pdf->Output($pdfFilePath, 'F');


       $from_email = $data['banner']->bannerimage_from;

    //  p($pdfFilePath."kkkkkkkkkkk");

       //Load email library
       $this->load->library('email');
       $this->email->from($from_email, $data['banner']->bannerimage_top3);
       $this->email->reply_to($from_email, $data['banner']->bannerimage_top3);
       // $this->email->reply_to($from_email, $data['banner']->bannerimage_top3);
       $this->email->to($to_email);
       $this->email->cc($from_email);
       // $this->email->to($from_email);
       $this->email->$data['banner']->bannerimage_apikey;
       $this->email->subject('' . $data['banner']->bannerimage_top3 . ' - Booking Confirmation Voucher ');

       $this->email->message($mess);

       $this->email->attach(FCPATH . "assets/admin/pdfstore/" . $pdf_name);
       $this->email->send();
     //  show_error($this->email->print_debugger());
       //var_dump($this->email->send()); exit;
       //Send mail
       $orderpdfupdate = array('act_mode' => 'orderpdfupdatesucess',
           'orderid' => $data['orderlastinsertid'],
           'tracking_id' => $pdf_name,
           'order_status' => '',
           'status_message' => '',
           'paymentmode' => '',
           'paymentstatus' => '',
           'ordersucesmail' => '',
           'ordermailstatus' => '',
           'type' => 'web',
       );

       $path = api_url() . "Ordersucess/selectorderdata/format/json/";
       $data['orderpdf'] = curlpost($orderpdfupdate, $path);



    //   pend($this->session->userdata('agent_order_last_insert_id'));


/* agnet payemnt start here */
	   		$total_promo_price = '';
				
			$final_selected_package_data_array = getPackageTotalPrice( $this->session->userdata( 'final_selected_package_data' ) );
	   
	   			$total_promo_price	= $this->session->userdata( 'total_promo_price' ) ;
	   
	   			$user_id = $this->session->userdata( 'user_id' );
				
	   			// empty array required as per procedure
				 $prepare_proc_param_package_addon_promocode = [

					    'param' => '' ,
						'param1' => '' ,
						'param2' => '' ,
						'param3' => '' ,
						'param4' => '' ,
						'param5' => '' ,
						'param6' => '' , 
						'param7' => '' , 
						'param8' => '' ,
						'param9' => '' ,
						'param10' => '' ,
						'param11' => '' ,    
						'param12' => '' ,
						'param13' => '' ,
						'param14' => '' ,
						'param15' => ''  
				 		];

	   			// empty array required as per procedure	   
				$prepare_proc_param_billingid_orderid_userid_urls = [

						'param16' => '' ,
						'param17' => '' ,
						'param18' => '' ,
						'param19' => '' ,
						'param20' => '' ,
						'param21' => '' ,
						'param22' => '' ,
						'param23' => '' ,
						'param24' => '' ,
						'param25' => '' ,
						'param26' => '' ,
						'param27' => '' ,
						'param28' => '' ,
						'param29' => '' ,
						'param30' => '' ,
						'param31' => '' ,
						'param32' => '' ,
						'param33' => '' ,
						'param34' => '' ,
						'param35' => '' ,
						'param36' => '' 
						];			
	   
	   			// empty array required as per procedure	   
					$prepare_proc_param_future_param = [ 
						'param37' => '',    // wallet amount
						'param38' => '',      // subtotal
						'param39' => '',
						'param40' => '',
						
						'param41' => '',
						'param42' => '' ,
						'param43' => '' ,
						'param44' => '' ,
						'param45' => '' ,
						'param46' => '' ,
						'param47' => '' ,
						'param48' => '' ,
						'param49' => '' ,
						'param50' => '' ,
						
						'param51' => '' ,
						// used later 
						'param52' => '' ,
						'param53' => '' ,
						'param54' => '' ,
						'param55' => '' ,
						'param56' => '' ,
						'param57' => '' ,
						'param58' => '' ,
						'param59' => '' ,
						'param60' => '' ,
						
						'param61' => '' ,
						'param62' => '' ,
						'param63' => 'wallet',
						'param64' => empty( $this->session->userdata( 'total_promo_price' ) ) ? $this->session->userdata( 'before_payment_total_purchase_amount' ) : $this->session->userdata( 'total_promo_price' ) ,
						'param65' => $this->session->userdata( 'user_id' ) 
						];
	   
	   
					$response_object = $agent_commission_inserted_id = '';	
	   
					$agent_payment_param  = array_merge( $prepare_proc_param_package_addon_promocode, $prepare_proc_param_billingid_orderid_userid_urls, $prepare_proc_param_future_param ) ;
	   
//					   if( $user_id ) {

						  /*
						   * insert agent commission details 
						   */
								$agent_payment_param[ 'param' ] = 'insert_agentcommission_details' ;
						   		$agent_payment_param[ 'param32' ] = ( $agent_order_last_insert_id ) ? $agent_order_last_insert_id : 0 ;
								$agent_payment_param[ 'param8' ] = empty( $this->session->userdata( 'total_promo_price' ) ) ? $this->session->userdata( 'before_payment_total_purchase_amount' ) : $this->session->userdata( 'total_promo_price' ) ;						   

								$response_object = $this->supper_admin->call_procedureRow( 'proc_agent_payment', $agent_payment_param );

								$agent_commission_inserted_id = $response_object->lastinsert_agentcommission_id;					   
						   
//					   	}	   
	   
/* agent end here	     */
	   
	   // $this->session->userdata );
	   // exit;
/*
			$arr1 = explode("=", $decryptValues[1]);
			$arr2 = explode("=", $decryptValues[3]);
			$arr3 = explode("=", $decryptValues[8]);
			$arr4 = explode("=", $decryptValues[5]);
			$arr5 = explode("=", $decryptValues[0]);
			$arr6 = explode("=", $decryptValues[35]);       
			$arr7 = explode("=", $decryptValues[18]);        
*/

		   $tracking_id = $order_status = $status_message = '' ;
		   $paymentmode = $agent_id = $agent_amount = '' ;
	   	   $agent_wallet_last_insert_id = '' ;
	   
	   	   $session_to_pay_wallet = $session_agent_wallet_amount = $session_to_pay_ccavenue = '' ;
	   
		   $tracking_id_wallet = $agent_order_last_insert_id ;  //order id - order package	  	   
	   
			// here
		   //$orderdata = explode( $data['ccavRequestHandler']->pg_agent_prefix.'_', $arr5[1] );

		   $payment_flag_int = $payment_flag ; 
	   
	   	   $agent_id_wallet = $user_id;
	   
		/*
		*  wallet  transaction
		*/	   		   	   
	   
		   if ( ( $payment_flag_int == 0 ) ) {
			   
			   $credit_debit_flag_wallet = 'D';
			   $tracking_id_wallet = $agent_order_last_insert_id ;
			   $order_status_wallet = 1 ;
			   $status_message_wallet = 'payment successful';
			   $paymentmode_wallet = 'wallet';
			   $agent_amount_wallet = $this->session->userdata('before_payment_after_wallet_deduction_to_pay_wallet');
			   $agent_id_wallet = $user_id;
			   
			   // required for history table entry
			   
	   			// $agent_id_wallet = '' ;
		   		$tracking_id = '' ;   // get from ccavenue 
	   			$order_status = $order_status_wallet ; # current order  0 = failed,1 = success, 2 = pending
		   		$status_message = $status_message_wallet ; // current order message
		   		$paymentmode = $paymentmode_wallet ;  // current order payment mode
		   		$agent_amount = $agent_amount_wallet ;  // current order amount
		   		$agent_wallet_last_insert_id = $agent_order_last_insert_id ;  // current order table last insert id
		   		$session_to_pay_wallet = $agent_amount_wallet ;  // to pay by wallet amount    
			   
			   $parameter = array('act_mode' => 'agentamount_add_new_debit',
							'Param1' => $agent_id_wallet,
							'Param2' => $to_pay_wallet,
							'Param3' => $credit_debit_flag_wallet,
							'Param4' => '0',
							'Param5' => 'Ag',
							'Param6' => '',
							'Param7' => '',
							'Param8' => '',
							'Param9' => '');
					// pend($parameter);
			   
				$response = $this->supper_admin->call_procedurerow('proc_agent_s', $parameter); 			   
			   
			   ( isset( $response->last_id ) && ( $response->last_id ) ) ?  $agent_wallet_last_insert_id =  $response->last_id : 0 ;
		   }
	   
		/*
		* 	ccavenue + wallet  transaction
		*/	   		   
 	
	   
	   	  if ( $payment_flag_int == 3 ) {

			   $credit_debit_flag_wallet = 'D';

			  //p( $this->session->userdata );
			   $session_to_pay_wallet = $this->session->userdata('before_payment_after_wallet_deduction_to_pay_wallet');
			   $session_agent_wallet_amount = $this->session->userdata('before_payment_after_wallet_deduction_agent_wallet_amount') ;
			   $session_to_pay_ccavenue = $this->session->userdata('before_payment_after_wallet_deduction_to_pay_ccavenue') ; 
			  
			   //$order_status_wallet = 'payment successful';
			  
			   $status_message_wallet = 'payment successful';
			  
			   $paymentmode_wallet_ccavenue = '3';
			   $to_pay_wallet = $session_to_pay_wallet ;
			   $agent_amount_wallet = $session_agent_wallet_amount ;
			   $total_purchase_amount = $session_to_pay_ccavenue ;
			   
				
				$tracking_id =  $arr1[1] ? $arr1[1].'/'.$tracking_id_wallet : $tracking_id_wallet ;    // two in one
				$order_status = $arr2[1] ? $arr2[1].'/'.$order_status_wallet : $order_status_wallet ;   // two in one
				$status_message = $arr3[1] ? $arr3[1].'/'.$status_message_wallet : $status_message_wallet ; // two in one
				$paymentmode = $arr4[1] ? $arr4[1] . '/'. $paymentmode_wallet_ccavenue : $paymentmode_wallet_ccavenue ; // two in one
				$agent_id = $orderdata[1] ? $orderdata[1] : '' ;
			  
				$agent_amount = ( $arr6[1] || ( $session_to_pay_wallet && $session_to_pay_ccavenue ) ) ? ( $arr6[1] + $session_to_pay_wallet + $session_to_pay_ccavenue ) : 0 ;	//		   
			  

			    // required for history table entry
			  
	   			// $agent_id_wallet = '' ;
		   		$tracking_id = '' ;   // get from ccavenue 
	   			$order_status = 1 ; # current order  0 = failed,1 = success, 2 = pending
		   		$status_message = $status_message_wallet ; // current order message
		   		$paymentmode = $paymentmode_wallet ;  // current order payment mode
		   		$agent_amount = ( $to_pay_wallet + $session_to_pay_ccavenue ) ;  // current order amount
		   		$agent_wallet_last_insert_id = $agent_order_last_insert_id ;  // current order table last insert id
		   		$session_to_pay_wallet = $to_pay_wallet ;  // to pay by wallet amount
			  
			  
			   $parameter = array('act_mode' => 'agentamount_add_new_debit',
							'Param1' => $agent_id_wallet,
							'Param2' => $to_pay_wallet,
							'Param3' => $credit_debit_flag_wallet,
							'Param4' => '0',
							'Param5' => 'Ag',
							'Param6' => '',
							'Param7' => '',
							'Param8' => '',
							'Param9' => '');
			  
				$response = $this->supper_admin->call_procedurerow('proc_agent_s', $parameter); 
			  
			  	( isset( $response->last_id ) && ( $response->last_id ) ) ?  $agent_wallet_last_insert_id =  $response->last_id : 0 ;

		   }
	   
		/*
		* 	only ccavenue transaction
		*/	   	
	   	  if ( $payment_flag_int == 2 ) {

			    $credit_debit_flag_wallet = 'D'; 
				//p( $this->session->userdata ); 
			  
			    $session_to_pay_ccavenue = $this->session->userdata('before_payment_to_pay_ccavenue') ;
			  
				$paymentmode_wallet_ccavenue = '2';
			  
			    //$agent_id_wallet = $user_id;
				
				$tracking_id = isset( $arr1[1] ) ? $arr1[1].'/'.$tracking_id_wallet : $tracking_id_wallet ;    // two in one  // get from ccavenue 
				$order_status = isset( $arr2[1] ) ? $arr2[1].'/'.$order_status_wallet : $order_status_wallet ;   // two in one  # current order  0 = failed,1 = success, 2 = pending
				$status_message = isset( $arr3[1] ) ? $arr3[1].'/'.$status_message_wallet : $status_message_wallet ; // two in one  // current order message
				$paymentmode = isset( $arr4[1] ) ? $arr4[1].'/'.$paymentmode_wallet_ccavenue : $paymentmode_wallet_ccavenue ; // two in one  // current order payment mode
				$agent_id = isset( $orderdata[1] ) ? $orderdata[1] : '' ; 
				$agent_amount = isset( $arr6[1] ) ? ( $arr6[1] ) : $session_to_pay_ccavenue ;	//  // current order amount
			  
				// required for history table entry
		   		$agent_wallet_last_insert_id = $agent_order_last_insert_id ;  // current order table last insert id
		   		$session_to_pay_wallet = $session_to_pay_ccavenue ;  // to pay by wallet amount			  
			  
		   }	 

		/*
		*	Agent history is saved for all successfull orders
		*
		*/
	   
	   //if( $response->last_id ) { 
		   
		 $parameter = array(
			 		'act_mode' => 'agentamount_add_new_history' ,
					'Param1' => $agent_id_wallet ,
					'Param2' => $tracking_id ,
					'Param3' => $order_status ,
					'Param4' => $status_message ,
					'Param5' => $paymentmode ,
					'Param6' => ( $agent_amount ) ? $agent_amount : 0 ,
					'Param7' => ( $agent_wallet_last_insert_id ) ?  $agent_wallet_last_insert_id : $agent_order_last_insert_id ,
					'Param8' => ( $session_to_pay_wallet ) ? $session_to_pay_wallet : 0 ,
					'Param9' => ''
		 			);
	   
		 		$response = $this->supper_admin->call_procedurerow( 'proc_agent_s', $parameter );
	   
		 //p( $parameter ); exit;
	   
	   //}


//p( $agent_payment_param ) ; exit;
	   






	   
	    // unset all user data start here
        if( !empty( $this->session->userdata('package_total_qty') ) ){  //echo 1;
            
            $this->session->unset_userdata('package_total_qty');
        }
		
        if( !empty( $this->session->userdata('package_total_price') ) ){  //echo 1;
            
            $this->session->unset_userdata('package_total_price');
        }		
        
        if( !empty( $this->session->userdata('final_selected_package_data') ) ){  echo 1;
            
            $this->session->unset_userdata('final_selected_package_data');
        }   
		   
		if( !empty( $this->session->userdata('final_selected_addon_data') ) ){   echo 2;

		   $this->session->unset_userdata('final_selected_addon_data');
		}
		
		if( ! empty( $this->session->userdata( 'addon_total_price' ) ) ) {

			$this->session->unset_userdata( 'addon_total_price' );
		}						

		if( ! empty( $this->session->userdata( 'addon_total_qty' ) ) ) {

			$this->session->unset_userdata( 'addon_total_qty' );
		}		
		   
		   
		// sessioon destroy start here 
		if( !empty( $this->session->userdata('orderlastinsertid') ) ){

			$this->session->unset_userdata('orderlastinsertid');

		}		

		if( ! empty( $this->session->userdata( 'before_payment_agent_wallet_amount' ) ) ) {

			$this->session->unset_userdata( 'before_payment_agent_wallet_amount' );
		}

		if( ! empty( $this->session->userdata( 'before_payment_total_purchase_amount' ) ) ) {

			$this->session->unset_userdata( 'before_payment_total_purchase_amount' );
		}

		if( ! empty( $this->session->userdata( 'before_payment_after_wallet_deduction_to_pay_ccavenue' ) ) ) {

			$this->session->unset_userdata( 'before_payment_after_wallet_deduction_to_pay_ccavenue' );
		}

		if( ! empty( $this->session->userdata( 'before_payment_after_wallet_deduction_to_pay_wallet' ) ) ) {

			$this->session->unset_userdata( 'before_payment_after_wallet_deduction_to_pay_wallet' );
		}

		if( ! empty( $this->session->userdata( 'before_payment_after_wallet_deduction_agent_wallet_amount' ) ) ) {

			$this->session->unset_userdata( 'before_payment_after_wallet_deduction_agent_wallet_amount' );
		}

		if( ! empty( $this->session->userdata( 'payment_flag' ) ) ) {

			$this->session->unset_userdata( 'payment_flag' );
		}			
		// sessioon destroy end here
		
		// promo variable unset start here 
		if( ! empty( $this->session->userdata( 'total_promo_price' ) ) ) {

			$this->session->unset_userdata( 'total_promo_price' );
		}	

		if( ! empty( $this->session->userdata( 'promo_id' ) ) ) {

			$this->session->unset_userdata( 'promo_id' );
		}			

		if( ! empty( $this->session->userdata( 'saving_price_promocode' ) ) ) {

			$this->session->unset_userdata( 'saving_price_promocode' );
		}				

		if( ! empty( $this->session->userdata( 'promocode_name' ) ) ) {

			$this->session->unset_userdata( 'promocode_name' );
		}				

		if( ! empty( $this->session->userdata( 'handling_charge_with_no_of_person' ) ) ) {

			$this->session->unset_userdata( 'handling_charge_with_no_of_person' );
		}						

		if( ! empty( $this->session->userdata( 'final_cost_package_addon_handling' ) ) ) {

			$this->session->unset_userdata( 'final_cost_package_addon_handling' );
		}	   
	    // unset all user data end here
	   
		 redirect("agentsucess");
}
    //Agent Amount Fail
    
 public function agentamountfail(){  // not in used 
	 
    	$siteurl= base_url();
        $parameterbranch=array(
            'act_mode' =>'selectbranch',
            'weburl' =>$siteurl,
            'type'=>'web',

        );

        $path=api_url().'selectsiteurl/branch/format/json/';
        $data['branch']=curlpost($parameterbranch,$path);


        $parameterccgatway=array(
            'act_mode' =>'selectccavenue',
            'branchid' =>$data['branch']->branch_id,
            'type'=>'web',

        );

        $path=api_url().'ccavenue/ccavRequestHandler/format/json/';
        $data['ccavRequestHandler']=curlpost($parameterccgatway,$path);

//p($data['ccavRequestHandler']);
        $workingKey = $data['ccavRequestHandler']->pg_working_key;
        $encResponse = $_POST["encResp"];
        //pend($encResponse);//This is the response sent by the CCAvenue Server
        $rcvdString = decrypt($encResponse, $workingKey);        //Crypto Decryption used as per the specified working key.
        $order_status = "";
        $decryptValues = explode('&', $rcvdString);
        $dataSize = sizeof($decryptValues);
        for ($i = 0; $i < $dataSize; $i++) {
            $information = explode('=', $decryptValues[$i]);
            if ($i == 3) $order_status = $information[1];
        }
     

        
        $siteurl = base_url();
        $parameterbranch = array(
            'act_mode' => 'selectbranch',
            'weburl' => $siteurl,
            'type' => 'web',

        );

        $path = api_url() . 'selectsiteurl/branch/format/json/';
        $data['branch'] = curlpost($parameterbranch, $path);
         $parameterbanner = array(
            'act_mode' => 'selectbannerimages',
            'branchid' => $data['branch']->branch_id,
            'type' => 'web',

        );

        $path = api_url() . 'selectsiteurl/banner/format/json/';
        $data['banner'] = curlpost($parameterbanner, $path);

        
		$parameterccgatway=array(
			'act_mode' =>'selectccavenue',
			'branchid' =>$data['branch']->branch_id,
			'type'=>'web',

		);

		$path=api_url().'ccavenue/ccavRequestHandler/format/json/';
		$data['ccavRequestHandler']=curlpost($parameterccgatway,$path);
        
/*
Array
(
    [0] => order_id=agent_snow_7
    [1] => tracking_id=106310685039
    [2] => bank_ref_no=null
    [3] => order_status=Aborted
    [4] => failure_message=
    [5] => payment_mode=null
    [6] => card_name=null
    [7] => status_code=
    [8] => status_message=I have second thoughts about making this payment
    [9] => currency=INR
    [10] => amount=100.0
    [11] => billing_name=simerjeet singh
    [12] => billing_address=test addressdd
    [13] => billing_city=delhi
    [14] => billing_state=delhii
    [15] => billing_zip=110014
    [16] => billing_country=India
    [17] => billing_tel=9810668829
    [18] => billing_email=simer@elitehrpractices.com
    [19] => delivery_name=
    [20] => delivery_address=
    [21] => delivery_city=
    [22] => delivery_state=
    [23] => delivery_zip=
    [24] => delivery_country=
    [25] => delivery_tel=
    [26] => merchant_param1=
    [27] => merchant_param2=
    [28] => merchant_param3=
    [29] => merchant_param4=
    [30] => merchant_param5=
    [31] => vault=N
    [32] => offer_type=null
    [33] => offer_code=null
    [34] => discount_value=0.0
    [35] => mer_amount=100.0
    [36] => eci_value=
    [37] => retry=null
    [38] => response_code=
    [39] => billing_notes=
    [40] => trans_date=null
    [41] => bin_country=
)
*/


        $arr1 = explode("=", $decryptValues[1]);
        $arr2 = explode("=", $decryptValues[3]);
        $arr3 = explode("=", $decryptValues[8]);
        $arr4 = explode("=", $decryptValues[5]);
		$arr5 = explode("=", $decryptValues[0]);
		$arr6 = explode("=", $decryptValues[35]);       
		$arr7 = explode("=", $decryptValues[18]);        
     
     
     $orderdata = explode($data['ccavRequestHandler']->pg_agent_prefix.'_', $arr5[1]);

        $tracking_id = $arr1[1];

        $order_status = $arr2[1];
        $status_message = $arr3[1];
        $paymentmode = $arr4[1];
        $agent_id = $orderdata[1];
          $agent_amount = $arr6[1];
          
          
          
     $parameter = array('act_mode' => 'agentamount_add_new_history',
                'Param1' =>  $this->session->userdata['pid'],
                'Param2' => $tracking_id,
                'Param3' => $order_status,
                'Param4' => $status_message,
                'Param5' =>$paymentmode,
                'Param6' => $agent_amount,
                'Param7' => '',
                'Param8' => '',
                'Param9' => '');
          
    $response = $this->supper_admin->call_procedurerow('proc_agent_s', $parameter);
       
          
          
     $mess = '<table width="90%" style="line-height: 28px; font-family: sans-serif;" >
<tr><td>You missed the FUN at '.$data['banner']->bannerimage_top3 .' !</td></tr>

<tr><td>Greetings from '.$data['banner']->bannerimage_top3 .'</td></tr>

<tr><td>Oops! By Some Reason You Missed the Payment</td></tr>
<tr><td>Book now and join the fun at '.$data['banner']->bannerimage_top3 .'!</td></tr>

<tr><td>Yours sincerely,<br>
' . $data['banner']->bannerimage_top3 . ' Team</td></tr>
</table>
';       
    $from_email = $data['banner']->bannerimage_from;
$from = $from_email;
$fromname = $data['banner']->bannerimage_top3;
$to =  $arr7[1]; //Recipients list (semicolon separated)
$api_key = $data['banner']->bannerimage_apikey;
$subject = ('' . $data['banner']->bannerimage_top3 . ' - Your Request for Agent Cancellation');
 

$content =$mess;

$mail1=array();
$mail1['subject']= ($subject);                                                                       
$mail1['fromname']= ($fromname);                                                             
$mail1['api_key'] = $api_key;
$mail1['from'] = $from;
$mail1['content']= ($content);
$mail1['recipients']= $to;
$apiresult = callApi(@$api_type,@$action,$mail1);

redirect("agentfail");
	 
}
	
	
	
public function agentfail(){
	
			/* save agent purchase details ( commission )  start here  */

					//p( $this->session->userdata );
		
				   /* agnet Mail start here and order update and  */
			$data = [];

			$agent_order_id = $agent_order_amount = $paymentmode = '' ;

			$user_id = $path = '' ; 

			$agent_id_wallet = 	$tracking_id = 	$order_status = $status_message = $paymentmode = $agent_amount = '' ;
			
			$agent_wallet_last_insert_id = $session_to_pay_wallet = '';

			$data['orderlastinsertid'] = $this->session->userdata['agent_order_last_insert_id'];

			$user_id = $this->session->userdata( 'user_id' );

			$orderdisplay = array('act_mode' => 'select_order',
					   'orderid' => $data['orderlastinsertid'],
						'type' => 'web', );

					   $path = api_url() . "Ordersucess/selectorder/format/json/";

					   $data['orderdisplaydataval'] = curlpost( $orderdisplay, $path );


				if( isset( $data['orderdisplaydataval']->pacorderid ) ) {

					$paymentmode = $data['paymentmode'] = $data['orderdisplaydataval']->paymentmode ;		

					$agent_order_amount = $data['agent_order_amount'] = $data['orderdisplaydataval']->total ;

					$agent_order_id = $data['agent_order_id'] = $data['orderdisplaydataval']->pacorderid ;
					
					$session_to_pay_wallet = $data['session_to_pay_wallet'] =  $data['orderdisplaydataval']->waletAmount ;
					

					$agent_id_wallet = $user_id ;
					
						switch( $paymentmode ){
								
							case 1 :	
								
							break;	
								
							case 2 :
								
							break;	
								
							case 3 :									
								
							break;	
						
						}

					$tracking_id = '' ;

					$order_status = $data['order_status'] =  0 ;

					$status_message = $data['status_message'] = 'payment failed' ;

					//$paymentmode = $paymentmode ;    // to get by database

					$agent_amount = $data['agent_amount'] = ( $agent_order_amount ) ? $agent_order_amount : 0 ;

					$agent_wallet_last_insert_id = ( $agent_order_id ) ?  $agent_order_id : 0 ;

					$session_to_pay_wallet = $data['session_to_pay_wallet'] = ( $session_to_pay_wallet ) ? $session_to_pay_wallet : 0 ;									

							$parameter = array(
								'act_mode' => 'agentamount_add_new_history' ,
								'Param1' => $agent_id_wallet   ,
								'Param2' => $tracking_id ,
								'Param3' => $order_status ,
								'Param4' => $status_message ,
								'Param5' => $paymentmode ,
								'Param6' => ( $agent_amount ) ? $agent_amount : 0 ,
								'Param7' => ( $agent_wallet_last_insert_id ) ?  $agent_wallet_last_insert_id : $agent_order_last_insert_id ,
								'Param8' => $session_to_pay_wallet ,
								'Param9' => ''
								);
//p( $parameter );
							$response = $this->supper_admin->call_procedurerow( 'proc_agent_s', $parameter );					

				} else {
					
					log_message('info', 'Some variable did not contain a value');
				} 	
				
		//p( $data['orderdisplaydataval'] );
		// p( $response );
	    // exit;
		
/* save agent purchase details ( commission ) end here  */		
	
	
	/*	
		$this->load->view("agent/helper/header",$data);
		$this->load->view("agent/helper/leftbar",$data);
		$this->load->view("agentamountfail",$data);
		$this->load->view("agent/helper/footer");	
	
	*/
	
	
	
	
	
	
	
	
	
	
	
	
	
	
    
     $parameter = array('act_mode' => 'agentamount_select_new_history',
                'Param1' =>  $this->session->userdata['pid'],
                'Param2' => '',
                'Param3' => '',
                'Param4' => '',
                'Param5' =>'',
                'Param6' => '',
                'Param7' => '',
                'Param8' => '',
                'Param9' => '');
          
    $date['agentdatapayment'] = $this->supper_admin->call_procedurerow('proc_agent_s', $parameter);
    

    $parameterreg = array(
        'act_mode' => 'selectsubpartner',
        'title' =>'',
        'firstname' =>'',
        'lastname' =>'',
        'row_id' =>'',
        'logintype' => '',
        'agencyname' =>'',
        'office_address' => '',
        'city' => '',
        'state' => '',
        'pincode' => '',
        'telephone' => '',
        'mobile' => '',
        'fax' => '',
        'website' => '',
        'email' => '',
        'primarycontact_name' => '',
        'primarycontact_email_mobile' => '',
        'primarycontact_telephone' => '',
        'secondrycontact_name' => '',
        'secondrycontact_email_phone' => '',
        'secondrycontact_telephone' => '',
        'management_executives_name' => '',
        'management_executives_email_phone' => '',
        'management_executives_telephone' => '',
        'branches' =>'',

        'yearofestablism' =>'',
        'organisationtype' =>'',
        'lstno' => '',
        'cstno' => '',
        'registrationno' => '',
        'vatno' =>'',
        'panno' => '',
        'servicetaxno' => '',
        'tanno' => '',
        'pfno' => '',
        'esisno' => '',
        'officeregistrationno' => '',
        'exceptiontax' => '',
        'otherexceptiontax' => '',
        'bank_benificialname' => '',
        'bank_benificialaccno' => '',
        'bank_benificialbankname' => '',
        'bank_benificialbranchname' => '',
        'bank_benificialaddress' => '',
        'bank_benificialifsc' => '',
        'bank_benificialswiftcode' => '',
        'bank_benificialibanno' => '',
        'intermidiatebankname' => '',
        'intermidiatebankaddress' => '',
        'intermidiatebankswiftcode' => '',
        'bank_ecs' => '',
        'copypancart' =>'',
        'copyservicetax' => '',
        'copytan' => '',
        'copyaddressproof' => '',
        'emailstatus' => '',
        'emailcurrency' => '',
        'emailcountry' => '',
        'username' => '',
        'password' => '',

        'countryid' => $data['branch']->countryid,
        'stateid' => $data['branch']->stateid,
        'cityid' => $data['branch']->cityid,
        'branch_id' => $data['branch']->branch_id,
        'locationid' => $data['branch']->branch_location,
        'bank_creditamount' =>($this->session->userdata('ppid') != 0) ? $this->session->userdata('ppid') : '',
        'bank_agentcommision' =>'',

        'aadhaar_number' =>'',
        'ppid' =>'',
        'pppermission' =>'',
        'type' => 'web',
    );


    $path = api_url() . 'partnerapi/partnerregisterpartner/format/json/';
    $data['reg'] = curlpost($parameterreg, $path);
	
		/* Render booking details */

		$data['orderlastinsertid'] = $this->session->userdata['agent_order_last_insert_id'];

		$user_id = $this->session->userdata( 'user_id' );

		$orderdisplay = array('act_mode' => 'select_order',
				   'orderid' => $data['orderlastinsertid'],
					'type' => 'web', );

				   $path = api_url() . "Ordersucess/selectorder/format/json/";

				   $data['orderdisplaydataval'] = curlpost( $orderdisplay, $path );	
		
		// $data['orderdisplaydataval'] = is_object( $data['orderdisplaydataval'] ) ? (array) $data['orderdisplaydataval'] : [] ;
		
         $prepare_time_slot_from_date = $data['orderdisplaydataval']->departuretime .':'.$data['orderdisplaydataval']->frommin .' '.$data['orderdisplaydataval']->txtfromd .' - '.
             $data['orderdisplaydataval']->tohrs.':'.$data['orderdisplaydataval']->tomin.' '.$data['orderdisplaydataval']->txttod;
		
		$data['prepare_time_slot_from_date'] =$prepare_time_slot_from_date;		
		

        $date_array1 = explode("-", $data['orderdisplaydataval']->addedon); // split the array
        $var_day1 = $date_array1[2]; //day seqment
        $var_month1 = $date_array1[1]; //month segment
        $var_year1 = $date_array1[0]; //year segment
        $new_date_format1 = strtotime("$var_year1-$var_month1-$var_day1"); // join them together

        $d1 = date(' jS F Y', $new_date_format1);


        $date = '19:24:15 ';
        $d2 = date('h:i:s a ');

        $date_array = explode("-", $data['orderdisplaydataval']->departuredate); // split the array
        $var_day = $date_array[0]; //day seqment
        $var_month = $date_array[1]; //month segment
        $var_year = rtrim($date_array[2]," "); //year segment
        $new_date_format = strtotime("2017-$var_month-$var_day"); // join them together

        $input = ("$var_year$var_month$var_day");

        $d3 = date("D", strtotime($input)) . "\n";


        //$d4= date(' jS F Y', $new_date_format);

        $row['date'] = trim($var_year, " ") . "-" . $var_month . "-" . $var_day; // this is for example only - comment out when tested
        $d4 = date("F j, Y", strtotime( $data['orderdisplaydataval']->departuredate ));
 $data['d4'] = $d4;
		 $data['d1'] = $d1;
		 $data['d2'] = $d2;
		 $data['d3'] = $d3;
// Addone Type
        $orderaddone = array('act_mode' => 'select_addone',
            'orderid' => $this->session->userdata('orderlastinsertid'),
            'type' => 'web',

        );
        $path2 = api_url() . "Ordersucess/selectaddone/format/json/";
        $data['orderaddonedisplaydata'] = curlpost($orderaddone, $path2);
         $total_array = $addon_data = [];
        $total = 0;
        
        $package_name_array = explode('#', $data['orderdisplaydataval']->packproductname );
        $package_img_array = explode('#', $data['orderdisplaydataval']->packimg );
        $package_qty_array = explode('#', $data['orderdisplaydataval']->package_qty );
        $package_price_array = explode('#', $data['orderdisplaydataval']->package_price );

        $package_total_price = $total_array[] = getSumAllArrayElement( calculatePriceByQty( explode('#', $data['orderdisplaydataval']->package_price ), explode('#', $data['orderdisplaydataval']->package_qty ) ) );       
        
        $addon_data =  getAddonTotalPricePrint( $data['orderaddonedisplaydata'] ) ; 
         //p( $addon_data['addon_price_array'] );
//p($addon_data);
        $data['addon_price_with_quantity'] = 0;
        $addon_total_price_with_quantity = $total_array[] =  getSumAllArrayElement( calculatePriceByQty( $addon_data['addon_price_array'], $addon_data['addon_qty_array'] ) );
//p( $addon_total_price_with_quantity );        
        $data['addon_price_with_quantity'] = $addon_total_price_with_quantity;	
	
	
			$siteurl = base_url();

			$parameterbranch = array(
				'act_mode' => 'selectbranch',
				'weburl' => $siteurl,
				'type' => 'web',

			);

			$path = api_url() . 'selectsiteurl/branch/format/json/';
			$data['branch'] = curlpost($parameterbranch, $path);	
	
		/* Render booking details */
	

        $this->load->view("agent/helper/header",$data);
        $this->load->view("agent/helper/leftbar",$data);
        $this->load->view("agentamountfail",$data);
        $this->load->view("agent/helper/footer");
	
	// good design agentfail template
}
    
    

    
    
    public function agentsucess(){
    //Select branch
        $siteurl = base_url();
        $parameterbranch = array(
            'act_mode' => 'selectbranch',
            'weburl' => $siteurl,
            'type' => 'web',

        );

        $path = api_url() . 'selectsiteurl/branch/format/json/';
        $data['branch'] = curlpost($parameterbranch, $path);

		$parametertearms = array(
            'act_mode' => 'selecttearms',
            'branchid' => $data['branch']->branch_id,
            'type' => 'web',

        );

        $path = api_url() . 'selectsiteurl/bannern/format/json/';
        $data['tearmsgatway'] = curlpost($parametertearms, $path);
		 $parameter = array('act_mode' => 'agentamount_select_new_history',
					'Param1' =>  $this->session->userdata['pid'],
					'Param2' => '',
					'Param3' => '',
					'Param4' => '',
					'Param5' =>'',
					'Param6' => '',
					'Param7' => '',
					'Param8' => '',
					'Param9' => '');

		$date['agentdatapayment'] = $this->supper_admin->call_procedurerow('proc_agent_s', $parameter);
//p( $date['agentdatapayment'] );   data not relevent
		$parameterreg = array(
			'act_mode' => 'selectsubpartner',
			'title' =>'',
			'firstname' =>'',
			'lastname' =>'',
			'row_id' =>'',
			'logintype' => '',
			'agencyname' =>'',
			'office_address' => '',
			'city' => '',
			'state' => '',
			'pincode' => '',
			'telephone' => '',
			'mobile' => '',
			'fax' => '',
			'website' => '',
			'email' => '',
			'primarycontact_name' => '',
			'primarycontact_email_mobile' => '',
			'primarycontact_telephone' => '',
			'secondrycontact_name' => '',
			'secondrycontact_email_phone' => '',
			'secondrycontact_telephone' => '',
			'management_executives_name' => '',
			'management_executives_email_phone' => '',
			'management_executives_telephone' => '',
			'branches' =>'',

			'yearofestablism' =>'',
			'organisationtype' =>'',
			'lstno' => '',
			'cstno' => '',
			'registrationno' => '',
			'vatno' =>'',
			'panno' => '',
			'servicetaxno' => '',
			'tanno' => '',
			'pfno' => '',
			'esisno' => '',
			'officeregistrationno' => '',
			'exceptiontax' => '',
			'otherexceptiontax' => '',
			'bank_benificialname' => '',
			'bank_benificialaccno' => '',
			'bank_benificialbankname' => '',
			'bank_benificialbranchname' => '',
			'bank_benificialaddress' => '',
			'bank_benificialifsc' => '',
			'bank_benificialswiftcode' => '',
			'bank_benificialibanno' => '',
			'intermidiatebankname' => '',
			'intermidiatebankaddress' => '',
			'intermidiatebankswiftcode' => '',
			'bank_ecs' => '',
			'copypancart' =>'',
			'copyservicetax' => '',
			'copytan' => '',
			'copyaddressproof' => '',
			'emailstatus' => '',
			'emailcurrency' => '',
			'emailcountry' => '',
			'username' => '',
			'password' => '',

			'countryid' => $data['branch']->countryid,
			'stateid' => $data['branch']->stateid,
			'cityid' => $data['branch']->cityid,
			'branch_id' => $data['branch']->branch_id,
			'locationid' => $data['branch']->branch_location,
			'bank_creditamount' =>($this->session->userdata('ppid') != 0) ? $this->session->userdata('ppid') : '',
			'bank_agentcommision' =>'',

			'aadhaar_number' =>'',
			'ppid' =>'',
			'pppermission' =>'',
			'type' => 'web',
		);


		$path = api_url() . 'partnerapi/partnerregisterpartner/format/json/';
		$data['reg'] = curlpost($parameterreg, $path);
 //p( $data['reg'] );   //data not relevent
		
	//	p( $this->session->userdata ); exit;
		$data['orderlastinsertid'] = $this->session->userdata['agent_order_last_insert_id'];

		$user_id = $this->session->userdata( 'user_id' );

		$orderdisplay = array('act_mode' => 'select_order',
				   'orderid' => $data['orderlastinsertid'],
					'type' => 'web', );

				   $path = api_url() . "Ordersucess/selectorder/format/json/";

				   $data['orderdisplaydataval'] = curlpost( $orderdisplay, $path );	
		
		// $data['orderdisplaydataval'] = is_object( $data['orderdisplaydataval'] ) ? (array) $data['orderdisplaydataval'] : [] ;
		
         $prepare_time_slot_from_date = $data['orderdisplaydataval']->departuretime .':'.$data['orderdisplaydataval']->frommin .' '.$data['orderdisplaydataval']->txtfromd .' - '.
             $data['orderdisplaydataval']->tohrs.':'.$data['orderdisplaydataval']->tomin.' '.$data['orderdisplaydataval']->txttod;
		
		$data['prepare_time_slot_from_date'] =$prepare_time_slot_from_date;		
		

        $date_array1 = explode("-", $data['orderdisplaydataval']->addedon); // split the array
        $var_day1 = $date_array1[2]; //day seqment
        $var_month1 = $date_array1[1]; //month segment
        $var_year1 = $date_array1[0]; //year segment
        $new_date_format1 = strtotime("$var_year1-$var_month1-$var_day1"); // join them together

        $d1 = date(' jS F Y', $new_date_format1);


        $date = '19:24:15 ';
        $d2 = date('h:i:s a ');

        $date_array = explode("-", $data['orderdisplaydataval']->departuredate); // split the array
        $var_day = $date_array[0]; //day seqment
        $var_month = $date_array[1]; //month segment
        $var_year = rtrim($date_array[2]," "); //year segment
        $new_date_format = strtotime("2017-$var_month-$var_day"); // join them together

        $input = ("$var_year$var_month$var_day");

        $d3 = date("D", strtotime($input)) . "\n";


        //$d4= date(' jS F Y', $new_date_format);

        $row['date'] = trim($var_year, " ") . "-" . $var_month . "-" . $var_day; // this is for example only - comment out when tested
        $d4 = date("F j, Y", strtotime( $data['orderdisplaydataval']->departuredate ));
 $data['d4'] = $d4;
		 $data['d1'] = $d1;
		 $data['d2'] = $d2;
		 $data['d3'] = $d3;
// Addone Type
        $orderaddone = array('act_mode' => 'select_addone',
            'orderid' => $this->session->userdata('orderlastinsertid'),
            'type' => 'web',

        );
        $path2 = api_url() . "Ordersucess/selectaddone/format/json/";
        $data['orderaddonedisplaydata'] = curlpost($orderaddone, $path2);
         $total_array = $addon_data = [];
        $total = 0;
        
        $package_name_array = explode('#', $data['orderdisplaydataval']->packproductname );
        $package_img_array = explode('#', $data['orderdisplaydataval']->packimg );
        $package_qty_array = explode('#', $data['orderdisplaydataval']->package_qty );
        $package_price_array = explode('#', $data['orderdisplaydataval']->package_price );

        $package_total_price = $total_array[] = getSumAllArrayElement( calculatePriceByQty( explode('#', $data['orderdisplaydataval']->package_price ), explode('#', $data['orderdisplaydataval']->package_qty ) ) );       
        
        $addon_data =  getAddonTotalPricePrint( $data['orderaddonedisplaydata'] ) ; 
         //p( $addon_data['addon_price_array'] );
//p($addon_data);
        $data['addon_price_with_quantity'] = 0;
        $addon_total_price_with_quantity = $total_array[] =  getSumAllArrayElement( calculatePriceByQty( $addon_data['addon_price_array'], $addon_data['addon_qty_array'] ) );
//p( $addon_total_price_with_quantity );        
        $data['addon_price_with_quantity'] = $addon_total_price_with_quantity;

        $this->load->view("agent/helper/header",$data);
        $this->load->view("agent/helper/leftbar",$data);
        $this->load->view("agent/agentsucess",$data);
        $this->load->view("agent/helper/footer");
	}
    

 
   
//partnerregister update   page
public function agentform1()
{

   
    if($this->input->post('submit')=='Send Pin')

    {
    $smsphnon = $this->input->post('umob');

    $num = rand(1111, 9999);
    $parameter =array('act_mode'=>'chmobileotp',
        'row_id'=>'',
        'p_userid'=>'',
        'p_email'=>$this->session->userdata['pemailid'],
        'p_mobilenum'=>$smsphnon,
        'p_mas'=>'');
    $path1=api_url().'partnerapi/chmobileotp/format/json/';
    $record=curlpost($parameter,$path1);
   // p($record);exit();
        $smsmsgg = urlencode('Hi User, your OTP Password is ' . $num . ' for Mobile Number Verification.');

        $sms_url = "http://bulkpush.mytoday.com/BulkSms/SingleMsgApi?feedid=356637&username=9867622549&password=mtpjp&To=" . $this->input->post('umob') . "&Text=" . $smsmsgg;
        $sms = file_get_contents($sms_url);

        $data = array(
            'agentmobile' => $this->input->post('umob'),
            'p_email' => $this->session->userdata['pemailid'],
            'agentotp' => $num,
        );

        $this->session->set_userdata($data);

        header("Location:agentformverify");

    }
 $this->load->view("helper/header",$data);
    $this->load->view("helper/topbar",$data);
    $this->load->view("agent/form1",$data);
    $this->load->view("helper/footer");

}
    public function agentformverify()
    {

        if($this->input->post('submit')=='Send Pin')

        {

            if($this->input->post('umob')==$this->session->userdata('agentotp'))

            {

                $smsphnon = $this->session->userdata('agentmobile');
                $parameter =array('act_mode'=>'chmobileotpinsert',
                    'row_id'=>'',
                    'p_userid'=>'',
                    'p_email'=>$this->session->userdata['pemailid'],
                    'p_mobilenum'=>$smsphnon,
                    'p_mas'=>'');
                $path1=api_url().'partnerapi/chmobileotp/format/json/';
                $record=curlpost($parameter,$path1);

               header("location:agentform2");
            }
            else{

                $data['message']="Invalid Promocode";
            }

        }

        $this->load->view("helper/header",$data);
        $this->load->view("helper/topbar",$data);
        $this->load->view("agent/agentformverify",$data);
        $this->load->view("helper/footer");

    }



    public function agentform2()
    {


        if($this->input->post('Submit')=='Submit') {
            $parameterreg = array(
                'act_mode' => 'insertpartnerstep1',
                'title' => '',
                'firstname' => '',
                'lastname' => '',
                'row_id' => $this->session->userdata['pid'],
                'logintype' => $this->input->post('logintype'),
                'emailstatus' => $this->input->post('emailstatus'),
                'emailcurrency' => $this->input->post('emailcurrency'),
                'emailcountry' => $this->input->post('emailcountry'),

                'agencyname' => $this->input->post('agencyname'),
                'office_address' => $this->input->post('office_address'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'pincode' => $this->input->post('pincode'),
                'telephone' => $this->input->post('telephone'),
                'mobile' => '',
                'website' => $this->input->post('website'),
                'fax' => $this->input->post('fax'),
                'email' => $this->session->userdata['pemailid'],
                'primarycontact_name' => $this->input->post('primarycontact_name'),
                'primarycontact_email_mobile' => $this->input->post('primarycontact_email_mobile'),
                'primarycontact_telephone' => $this->input->post('primarycontact_telephone'),
                'secondrycontact_name' => $this->input->post('secondrycontact_name'),
                'secondrycontact_email_phone' => $this->input->post('secondrycontact_email_phone'),
                'secondrycontact_telephone' => $this->input->post('secondrycontact_telephone'),
                'management_executives_name' => $this->input->post('management_executives_name'),
                'management_executives_email_phone' => $this->input->post('management_executives_email_phone'),
                'management_executives_telephone' => $this->input->post('management_executives_telephone	'),
                'branches' => $this->input->post('branches'),

                'yearofestablism' => $this->input->post('yearofestablism'),
                'organisationtype' => $this->input->post('organisationtype'),
                'lstno' => $this->input->post('lstno'),
                'cstno' => $this->input->post('cstno'),
                'registrationno' => $this->input->post('registrationno'),
                'vatno' => $this->input->post('vatno'),
                'panno' => $this->input->post('panno'),
                'servicetaxno' => $this->input->post('servicetaxno'),
                'tanno' => $this->input->post('tanno'),
                'pfno' => $this->input->post('pfno'),
                'esisno' => $this->input->post('esisno'),
                'officeregistrationno' => $this->input->post('officeregistrationno'),
                'exceptiontax' => $this->input->post('exceptiontax'),
                'otherexceptiontax' => $this->input->post('otherexceptiontax'),
                'bank_benificialname' => $this->input->post('bank_benificialname'),
                'bank_benificialaccno' => $this->input->post('bank_benificialaccno'),
                'bank_benificialbankname' => $this->input->post('bank_benificialbankname'),
                'bank_benificialbranchname' => $this->input->post('bank_benificialbranchname'),
                'bank_benificialaddress' => $this->input->post('bank_benificialaddress'),
                'bank_benificialifsc' => $this->input->post('bank_benificialifsc'),
                'bank_benificialswiftcode' => $this->input->post('bank_benificialswiftcode'),
                'bank_benificialibanno' => $this->input->post('bank_benificialibanno'),
                'intermidiatebankname' => $this->input->post('intermidiatebankname'),
                'intermidiatebankaddress' => $this->input->post('intermidiatebankaddress'),

                'intermidiatebankswiftcode' => $this->input->post('intermidiatebankswiftcode'),
                'bank_ecs' => $this->input->post('bank_ecs'),
                'copypancart' => $pancart,
                'copyservicetax' => $servicetax,
                'copytan' => $tan,
                'copyaddressproof' => $addressproof,

                'username' => $this->session->userdata['pemailid'],
                'password' => base64_encode($this->input->post('password')),

                'countryid' => $data['branch']->countryid,
                'stateid' => $data['branch']->stateid,
                'cityid' => $data['branch']->cityid,
                'branch_id' => $data['branch']->branch_id,
                'locationid' => $data['branch']->branch_location,
                'bank_creditamount' => '',
                'bank_agentcommision' => '',
                'aadhaar_number' =>$this->input->post('aadhaar_number'),
                'ppid' =>$this->input->post('ppid'),
                'pppermission' =>$this->input->post('pppermission'),


                'type' => 'web',
            );

            $path = api_url() . 'partnerapi/partnerregister/format/json/';
            $data['reg'] = curlpost($parameterreg, $path);
header("location:agentform3");

        }


        $this->load->view("helper/header",$data);
        $this->load->view("helper/topbar",$data);
        $this->load->view("agent/form2",$data);
        $this->load->view("helper/footer");

    }
    public function agentform3()
    {


        if($this->input->post('Submit')=='Submit')
        {
            $parameterreg = array(
                'act_mode' => 'insertpartnerstep2',
                'title' => '',
                'firstname' => '',
                'lastname' => '',
                'row_id' => $this->session->userdata['pid'],
                'logintype' => $this->input->post('logintype'),
                'emailstatus' => $this->input->post('emailstatus'),
                'emailcurrency' => $this->input->post('emailcurrency'),
                'emailcountry' => $this->input->post('emailcountry'),

                'agencyname' => $this->input->post('agencyname'),
                'office_address' => $this->input->post('office_address'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'pincode' => $this->input->post('pincode'),
                'telephone' => $this->input->post('telephone'),
                'mobile' => '',
                'website' => $this->input->post('website'),
                'fax' => $this->input->post('fax'),
                'email' => $this->session->userdata['pemailid'],
                'primarycontact_name' => $this->input->post('primarycontact_name'),
                'primarycontact_email_mobile' => $this->input->post('primarycontact_email_mobile'),
                'primarycontact_telephone' => $this->input->post('primarycontact_telephone'),
                'secondrycontact_name' => $this->input->post('secondrycontact_name'),
                'secondrycontact_email_phone' => $this->input->post('secondrycontact_email_phone'),
                'secondrycontact_telephone' => $this->input->post('secondrycontact_telephone'),
                'management_executives_name' => $this->input->post('management_executives_name'),
                'management_executives_email_phone' => $this->input->post('management_executives_email_phone'),
                'management_executives_telephone' => $this->input->post('management_executives_telephone	'),
                'branches' => $this->input->post('branches'),

                'yearofestablism' => $this->input->post('yearofestablism'),
                'organisationtype' => $this->input->post('organisationtype'),
                'lstno' => $this->input->post('lstno'),
                'cstno' => $this->input->post('cstno'),
                'registrationno' => $this->input->post('registrationno'),
                'vatno' => $this->input->post('vatno'),
                'panno' => $this->input->post('panno'),
                'servicetaxno' => $this->input->post('servicetaxno'),
                'tanno' => $this->input->post('tanno'),
                'pfno' => $this->input->post('pfno'),
                'esisno' => $this->input->post('esisno'),
                'officeregistrationno' => $this->input->post('officeregistrationno'),
                'exceptiontax' => $this->input->post('exceptiontax'),
                'otherexceptiontax' => $this->input->post('otherexceptiontax'),
                'bank_benificialname' => $this->input->post('bank_benificialname'),
                'bank_benificialaccno' => $this->input->post('bank_benificialaccno'),
                'bank_benificialbankname' => $this->input->post('bank_benificialbankname'),
                'bank_benificialbranchname' => $this->input->post('bank_benificialbranchname'),
                'bank_benificialaddress' => $this->input->post('bank_benificialaddress'),
                'bank_benificialifsc' => $this->input->post('bank_benificialifsc'),
                'bank_benificialswiftcode' => $this->input->post('bank_benificialswiftcode'),
                'bank_benificialibanno' => $this->input->post('bank_benificialibanno'),
                'intermidiatebankname' => $this->input->post('intermidiatebankname'),
                'intermidiatebankaddress' => $this->input->post('intermidiatebankaddress'),

                'intermidiatebankswiftcode' => $this->input->post('intermidiatebankswiftcode'),
                'bank_ecs' => $this->input->post('bank_ecs'),
                'copypancart' => $pancart,
                'copyservicetax' => $servicetax,
                'copytan' => $tan,
                'copyaddressproof' => $addressproof,

                'username' => $this->session->userdata['pemailid'],
                'password' => base64_encode($this->input->post('password')),

                'countryid' => $data['branch']->countryid,
                'stateid' => $data['branch']->stateid,
                'cityid' => $data['branch']->cityid,
                'branch_id' => $data['branch']->branch_id,
                'locationid' => $data['branch']->branch_location,
                'bank_creditamount' => '',
                'bank_agentcommision' => '',
'aadhaar_number' =>$this->input->post('aadhaar_number'),
                'ppid' =>$this->input->post('ppid'),
                'pppermission' =>$this->input->post('pppermission'),
                'type' => 'web',
            );

            $path = api_url() . 'partnerapi/partnerregister/format/json/';
            $data['reg'] = curlpost($parameterreg, $path);
            header("location:agentform4");
        }
        $this->load->view("helper/header",$data);
        $this->load->view("helper/topbar",$data);
        $this->load->view("agent/form3",$data);
        $this->load->view("helper/footer");

    }

    public function agentform4()
    {

        if($this->input->post('Submit')=='Submit')
        {
            $parameterreg = array(
                'act_mode' => 'insertpartnerstep4',
                'title' => '',
                'firstname' => '',
                'lastname' => '',
                'row_id' => $this->session->userdata['pid'],
                'logintype' => $this->input->post('logintype'),
                'emailstatus' => $this->input->post('emailstatus'),
                'emailcurrency' => $this->input->post('emailcurrency'),
                'emailcountry' => $this->input->post('emailcountry'),

                'agencyname' => $this->input->post('agencyname'),
                'office_address' => $this->input->post('office_address'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'pincode' => $this->input->post('pincode'),
                'telephone' => $this->input->post('telephone'),
                'mobile' => '',
                'website' => $this->input->post('website'),
                'fax' => $this->input->post('fax'),
                'email' => $this->session->userdata['pemailid'],
                'primarycontact_name' => $this->input->post('primarycontact_name'),
                'primarycontact_email_mobile' => $this->input->post('primarycontact_email_mobile'),
                'primarycontact_telephone' => $this->input->post('primarycontact_telephone'),
                'secondrycontact_name' => $this->input->post('secondrycontact_name'),
                'secondrycontact_email_phone' => $this->input->post('secondrycontact_email_phone'),
                'secondrycontact_telephone' => $this->input->post('secondrycontact_telephone'),
                'management_executives_name' => $this->input->post('management_executives_name'),
                'management_executives_email_phone' => $this->input->post('management_executives_email_phone'),
                'management_executives_telephone' => $this->input->post('management_executives_telephone	'),
                'branches' => $this->input->post('branches'),

                'yearofestablism' => $this->input->post('yearofestablism'),
                'organisationtype' => $this->input->post('organisationtype'),
                'lstno' => $this->input->post('lstno'),
                'cstno' => $this->input->post('cstno'),
                'registrationno' => $this->input->post('registrationno'),
                'vatno' => $this->input->post('vatno'),
                'panno' => $this->input->post('panno'),
                'servicetaxno' => $this->input->post('servicetaxno'),
                'tanno' => $this->input->post('tanno'),
                'pfno' => $this->input->post('pfno'),
                'esisno' => $this->input->post('esisno'),
                'officeregistrationno' => $this->input->post('officeregistrationno'),
                'exceptiontax' => $this->input->post('exceptiontax'),
                'otherexceptiontax' => $this->input->post('otherexceptiontax'),
                'bank_benificialname' => $this->input->post('bank_benificialname'),
                'bank_benificialaccno' => $this->input->post('bank_benificialaccno'),
                'bank_benificialbankname' => $this->input->post('bank_benificialbankname'),
                'bank_benificialbranchname' => $this->input->post('bank_benificialbranchname'),
                'bank_benificialaddress' => $this->input->post('bank_benificialaddress'),
                'bank_benificialifsc' => $this->input->post('bank_benificialifsc'),
                'bank_benificialswiftcode' => $this->input->post('bank_benificialswiftcode'),
                'bank_benificialibanno' => $this->input->post('bank_benificialibanno'),
                'intermidiatebankname' => $this->input->post('intermidiatebankname'),
                'intermidiatebankaddress' => $this->input->post('intermidiatebankaddress'),

                'intermidiatebankswiftcode' => $this->input->post('intermidiatebankswiftcode'),
                'bank_ecs' => $this->input->post('bank_ecs'),
                'copypancart' => $pancart,
                'copyservicetax' => $servicetax,
                'copytan' => $tan,
                'copyaddressproof' => $addressproof,

                'username' => $this->session->userdata['pemailid'],
                'password' => base64_encode($this->input->post('password')),

                'countryid' => $data['branch']->countryid,
                'stateid' => $data['branch']->stateid,
                'cityid' => $data['branch']->cityid,
                'branch_id' => $data['branch']->branch_id,
                'locationid' => $data['branch']->branch_location,
                'bank_creditamount' => '',
                'bank_agentcommision' => '',

'aadhaar_number' =>$this->input->post('aadhaar_number'),
                'ppid' =>$this->input->post('ppid'),
                'pppermission' =>$this->input->post('pppermission'),
                'type' => 'web',
            );

            $path = api_url() . 'partnerapi/partnerregister/format/json/';
            $data['reg'] = curlpost($parameterreg, $path);
           header("location:agentform5");
        }

        $this->load->view("helper/header",$data);
        $this->load->view("helper/topbar",$data);
        $this->load->view("agent/form4",$data);
        $this->load->view("helper/footer");

    }
    public function agentform5()
    {

        if($this->input->post('Submit')=='Submit')
        {
            $parameterreg = array(
                'act_mode' => 'insertpartnerstep5',
                'title' => '',
                'firstname' => '',
                'lastname' => '',
                'row_id' => $this->session->userdata['pid'],
                'logintype' => $this->input->post('logintype'),
                'emailstatus' => $this->input->post('emailstatus'),
                'emailcurrency' => $this->input->post('emailcurrency'),
                'emailcountry' => $this->input->post('emailcountry'),

                'agencyname' => $this->input->post('agencyname'),
                'office_address' => $this->input->post('office_address'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'pincode' => $this->input->post('pincode'),
                'telephone' => $this->input->post('telephone'),
                'mobile' => '',
                'website' => $this->input->post('website'),
                'fax' => $this->input->post('fax'),
                'email' => $this->session->userdata['pemailid'],
                'primarycontact_name' => $this->input->post('primarycontact_name'),
                'primarycontact_email_mobile' => $this->input->post('primarycontact_email_mobile'),
                'primarycontact_telephone' => $this->input->post('primarycontact_telephone'),
                'secondrycontact_name' => $this->input->post('secondrycontact_name'),
                'secondrycontact_email_phone' => $this->input->post('secondrycontact_email_phone'),
                'secondrycontact_telephone' => $this->input->post('secondrycontact_telephone'),
                'management_executives_name' => $this->input->post('management_executives_name'),
                'management_executives_email_phone' => $this->input->post('management_executives_email_phone'),
                'management_executives_telephone' => $this->input->post('management_executives_telephone	'),
                'branches' => $this->input->post('branches'),

                'yearofestablism' => $this->input->post('yearofestablism'),
                'organisationtype' => $this->input->post('organisationtype'),
                'lstno' => $this->input->post('lstno'),
                'cstno' => $this->input->post('cstno'),
                'registrationno' => $this->input->post('registrationno'),
                'vatno' => $this->input->post('vatno'),
                'panno' => $this->input->post('panno'),
                'servicetaxno' => $this->input->post('servicetaxno'),
                'tanno' => $this->input->post('tanno'),
                'pfno' => $this->input->post('pfno'),
                'esisno' => $this->input->post('esisno'),
                'officeregistrationno' => $this->input->post('officeregistrationno'),
                'exceptiontax' => $this->input->post('exceptiontax'),
                'otherexceptiontax' => $this->input->post('otherexceptiontax'),
                'bank_benificialname' => $this->input->post('bank_benificialname'),
                'bank_benificialaccno' => $this->input->post('bank_benificialaccno'),
                'bank_benificialbankname' => $this->input->post('bank_benificialbankname'),
                'bank_benificialbranchname' => $this->input->post('bank_benificialbranchname'),
                'bank_benificialaddress' => $this->input->post('bank_benificialaddress'),
                'bank_benificialifsc' => $this->input->post('bank_benificialifsc'),
                'bank_benificialswiftcode' => $this->input->post('bank_benificialswiftcode'),
                'bank_benificialibanno' => $this->input->post('bank_benificialibanno'),
                'intermidiatebankname' => $this->input->post('intermidiatebankname'),
                'intermidiatebankaddress' => $this->input->post('intermidiatebankaddress'),

                'intermidiatebankswiftcode' => $this->input->post('intermidiatebankswiftcode'),
                'bank_ecs' => $this->input->post('bank_ecs'),
                'copypancart' => $pancart,
                'copyservicetax' => $servicetax,
                'copytan' => $tan,
                'copyaddressproof' => $addressproof,

                'username' => $this->session->userdata['pemailid'],
                'password' => base64_encode($this->input->post('password')),

                'countryid' => $data['branch']->countryid,
                'stateid' => $data['branch']->stateid,
                'cityid' => $data['branch']->cityid,
                'branch_id' => $data['branch']->branch_id,
                'locationid' => $data['branch']->branch_location,
                'bank_creditamount' => '',
                'bank_agentcommision' => '',
'aadhaar_number' =>$this->input->post('aadhaar_number'),
                'ppid' =>$this->input->post('ppid'),
                'pppermission' =>$this->input->post('pppermission'),
                'type' => 'web',
            );

            $path = api_url() . 'partnerapi/partnerregister/format/json/';
            $data['reg'] = curlpost($parameterreg, $path);

        header("location:agentform6");
        }

        $this->load->view("helper/header",$data);
        $this->load->view("helper/topbar",$data);
        $this->load->view("agent/form5",$data);
        $this->load->view("helper/footer");

    }

    public function agentform6()
    {

        if($this->input->post('Submit')=='Submit')
        {




            $configUpload['upload_path']    = 'assets/admin/images/partner';              #the folder placed in the root of project
            $configUpload['allowed_types']  = 'gif|jpg|png|bmp|jpeg';       #allowed types description
            $configUpload['max_size']       = '0';                          #max size
            $configUpload['max_width']      = '0';                          #max width
            $configUpload['max_height']     = '0';                          #max height
            $configUpload['encrypt_name']   = true;                         #encrypt name of the uploaded file
            $this->load->library('upload', $configUpload);

            $this->upload->do_upload('copypancart');
           // echo $this->upload->display_errors();

            $pancart=($this->upload->data('copypancart')['file_name']);

            
            $this->upload->do_upload('copyservicetax');
            $servicetax=($this->upload->data('copyservicetax')['file_name']);
            $this->upload->do_upload('copytan');
            $tan=($this->upload->data('copytan')['file_name']);
            $this->upload->do_upload('copyaddressproof');
           $addressproof=($this->upload->data('copyaddressproof')['file_name']);




            $parameterreg = array(
                'act_mode' => 'insertpartnerstep6',
                'title' => '',
                'firstname' => '',
                'lastname' => '',
                'row_id' => $this->session->userdata['pid'],
                'logintype' => $this->input->post('logintype'),
                'emailstatus' => $this->input->post('emailstatus'),
                'emailcurrency' => $this->input->post('emailcurrency'),
                'emailcountry' => $this->input->post('emailcountry'),

                'agencyname' => $this->input->post('agencyname'),
                'office_address' => $this->input->post('office_address'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'pincode' => $this->input->post('pincode'),
                'telephone' => $this->input->post('telephone'),
                'mobile' => '',
                'website' => $this->input->post('website'),
                'fax' => $this->input->post('fax'),
                'email' => $this->session->userdata['pemailid'],
                'primarycontact_name' => $this->input->post('primarycontact_name'),
                'primarycontact_email_mobile' => $this->input->post('primarycontact_email_mobile'),
                'primarycontact_telephone' => $this->input->post('primarycontact_telephone'),
                'secondrycontact_name' => $this->input->post('secondrycontact_name'),
                'secondrycontact_email_phone' => $this->input->post('secondrycontact_email_phone'),
                'secondrycontact_telephone' => $this->input->post('secondrycontact_telephone'),
                'management_executives_name' => $this->input->post('management_executives_name'),
                'management_executives_email_phone' => $this->input->post('management_executives_email_phone'),
                'management_executives_telephone' => $this->input->post('management_executives_telephone	'),
                'branches' => $this->input->post('branches'),

                'yearofestablism' => $this->input->post('yearofestablism'),
                'organisationtype' => $this->input->post('organisationtype'),
                'lstno' => $this->input->post('lstno'),
                'cstno' => $this->input->post('cstno'),
                'registrationno' => $this->input->post('registrationno'),
                'vatno' => $this->input->post('vatno'),
                'panno' => $this->input->post('panno'),
                'servicetaxno' => $this->input->post('servicetaxno'),
                'tanno' => $this->input->post('tanno'),
                'pfno' => $this->input->post('pfno'),
                'esisno' => $this->input->post('esisno'),
                'officeregistrationno' => $this->input->post('officeregistrationno'),
                'exceptiontax' => $this->input->post('exceptiontax'),
                'otherexceptiontax' => $this->input->post('otherexceptiontax'),
                'bank_benificialname' => $this->input->post('bank_benificialname'),
                'bank_benificialaccno' => $this->input->post('bank_benificialaccno'),
                'bank_benificialbankname' => $this->input->post('bank_benificialbankname'),
                'bank_benificialbranchname' => $this->input->post('bank_benificialbranchname'),
                'bank_benificialaddress' => $this->input->post('bank_benificialaddress'),
                'bank_benificialifsc' => $this->input->post('bank_benificialifsc'),
                'bank_benificialswiftcode' => $this->input->post('bank_benificialswiftcode'),
                'bank_benificialibanno' => $this->input->post('bank_benificialibanno'),
                'intermidiatebankname' => $this->input->post('intermidiatebankname'),
                'intermidiatebankaddress' => $this->input->post('intermidiatebankaddress'),

                'intermidiatebankswiftcode' => $this->input->post('intermidiatebankswiftcode'),
                'bank_ecs' => $this->input->post('bank_ecs'),
                'copypancart' => $pancart,
                'copyservicetax' => $servicetax,
                'copytan' => $tan,
                'copyaddressproof' => $addressproof,

                'username' => $this->session->userdata['pemailid'],
                'password' => base64_encode($this->input->post('password')),

                'countryid' => $data['branch']->countryid,
                'stateid' => $data['branch']->stateid,
                'cityid' => $data['branch']->cityid,
                'branch_id' => $data['branch']->branch_id,
                'locationid' => $data['branch']->branch_location,
                'bank_creditamount' => '',
                'bank_agentcommision' => '',

'aadhaar_number' =>$this->input->post('aadhaar_number'),
                'ppid' =>$this->input->post('ppid'),
                'pppermission' =>$this->input->post('pppermission'),
                'type' => 'web',
            );

            $path = api_url() . 'partnerapi/partnerregister/format/json/';
            $data['reg'] = curlpost($parameterreg, $path);

            header("location:partnerlogin");
        }


        $this->load->view("helper/header",$data);
        $this->load->view("helper/topbar",$data);
        $this->load->view("agent/form6",$data);
        $this->load->view("helper/footer");

    }
    public function agentform7()
    {
        $this->load->view("helper/header",$data);
        $this->load->view("helper/topbar",$data);
        $this->load->view("agent/form7",$data);
        $this->load->view("helper/footer");

    }

public function partnerlogout(){

    $this->session->sess_destroy();
    redirect(base_url('partnerlogin'));
}
  //partnerlogin  page
public function partnerlogin(){
	
    if ($this->session->userdata['ppid'] > 0) {
    redirect(base_url()."partnerdashboard");
		
  }else{
    $parameterseo=array('act_mode'=>'viewseotags','rowid'=>19);
     $data['seotags']=$this->supper_admin->call_procedureRow('proc_siteconfig',$parameterseo);

  if($this->input->post('submit')=='LOGIN'){

      $parameter=array(
          'act_mode' =>'partnerlogincorporate',
         'pusername' =>$this->input->post('emailmob'),
          'ppassword'=>base64_encode($this->input->post('pwd')),
         'type'=>'web',
          );

      $parameterofficer=array(
          'act_mode' =>'partnerloginofficer',
         'pusername' =>$this->input->post('emailmob'),
          'ppassword'=>base64_encode($this->input->post('pwd')),
         'type'=>'web',
          );

      $path=api_url().'partnerapi/partnerlogin/format/json/';

      $responsecorporate=curlpost($parameter,$path);
      $responseofficer=curlpost($parameterofficer,$path);


      if($responsecorporate->scalar!='Something Went Wrong' && !empty($responsecorporate) && isset($responsecorporate))
         {



if($responsecorporate->agentprofilecomplete!=0)
{
 $data = array(
     'ppid' => $responsecorporate->agent_id,
     'pemailid' => $responsecorporate->agent_username,
     'pid' => $responsecorporate->agent_id,
     'user_id' => $responsecorporate->agent_id,

     'utype' => "partner",


);
$this->session->set_userdata($data);

   redirect(base_url()."partnerdashboard");
}
else
{
    redirect(base_url()."agentform1");
}

         }

elseif($responseofficer->scalar!='Something Went Wrong' && !empty($responseofficer) && isset($responseofficer))
         {

         	$data = array(
				 'ppid' => $responseofficer->agent_id,
				 'user_id' => $responseofficer->agent_id,
				 'pemailid' => $responseofficer->agent_username,
				 'pid' => $responseofficer->agent_id,
				 'utype' => "partner"
				);
			 
			$this->session->set_userdata($data);


             if($responseofficer->agentprofilecomplete!=0){
				 
				 redirect(base_url()."partnerdashboard");
				 
			 }else{
				 
                 redirect(base_url()."agentform1");
             }
			 
        }else{
			 
		   $this->session->set_flashdata('message', 'Invalid login details');
		}

    }

      $siteurl= base_url();
      $parameterbranch=array(
          'act_mode' =>'selectbranch',
          'weburl' =>$siteurl,
          'type'=>'web',

      );

      $path=api_url().'selectsiteurl/branch/format/json/';
      $data['branch']=curlpost($parameterbranch,$path);

      $this->load->view("helper/header",$data);
      $this->load->view("helper/topbar",$data);
 $this->load->view("partnerlogin",$data);
     $this->load->view("helper/footer");
  }
}




//partnerregister update   page
public function agentregistrationupdate(){
    if ($this->session->userdata['ppid'] > 0) {
        redirect(base_url()."partnerdashboard");
    }
  else{
    $parameterseo=array('act_mode'=>'viewseotags','rowid'=>14);
     $data['seotags']=$this->supper_admin->call_procedureRow('proc_siteconfig',$parameterseo);

  if($this->input->post('submit')=='submit'){

    //  p($_POST);




      $siteurl= base_url();
      $parameterbranch=array(
          'act_mode' =>'selectbranch',
          'weburl' =>$siteurl,
          'type'=>'web',

      );

      $path=api_url().'selectsiteurl/branch/format/json/';
      $data['branch']=curlpost($parameterbranch,$path);











      $configUpload['upload_path']    = 'assets/admin/images/partner';              #the folder placed in the root of project
      $configUpload['allowed_types']  = 'gif|jpg|png|bmp|jpeg';       #allowed types description
      $configUpload['max_size']       = '0';                          #max size
      $configUpload['max_width']      = '0';                          #max width
      $configUpload['max_height']     = '0';                          #max height
      $configUpload['encrypt_name']   = true;                         #encrypt name of the uploaded file
      $this->load->library('upload', $configUpload);

      $this->upload->do_upload('copypancart');
      $this->upload->do_upload('copyservicetax');
      $this->upload->do_upload('copytan');
      $this->upload->do_upload('copyaddressproof');
      $pancart=($this->upload->data('copypancart')['file_name']);
      $servicetax=($this->upload->data('copyservicetax')['file_name']);
      $tan=($this->upload->data('copytan')['file_name']);
      $addressproof=($this->upload->data('copyaddressproof')['file_name']);






      $parameterregsel=array(
          'act_mode' =>'selectpartneremail',
           'row_id' =>'',
          'logintype' =>'',
          'agencyname' =>'',
          'office_address' =>'',
          'city' =>'',
          'state' =>'',
          'pincode' =>'',
          'telephone' =>'',
          'mobile' =>'',
          'fax' =>'',
          'website' =>'',
          'email' =>'',
          'primarycontact_name' =>'',
          'primarycontact_email_mobile' =>'',
          'primarycontact_telephone' =>'',
          'secondrycontact_name' =>'',
          'secondrycontact_email_phone' =>'',
          'secondrycontact_telephone' =>'',
          'management_executives_name' =>'',
          'management_executives_email_phone' =>'',
          'management_executives_telephone' =>'',
          'branches' =>'',

          'yearofestablism' =>'',
          'organisationtype' =>'',
          'lstno' =>'',
          'cstno' =>'',
          'registrationno' =>'',
          'vatno' =>'',
          'panno' =>'',
          'servicetaxno' =>'',
          'tanno' =>'',
          'pfno' =>'',
          'esisno' =>'',
          'officeregistrationno' =>'',
          'exceptiontax' =>'',
          'otherexceptiontax' =>'',
          'bank_benificialname' =>'',
          'bank_benificialaccno' =>'',
          'bank_benificialbankname' =>'',
          'bank_benificialbranchname' =>'',
          'bank_benificialaddress' =>'',
          'bank_benificialifsc' =>'',
          'bank_benificialswiftcode' =>'',
          'bank_benificialibanno' =>'',
          'intermidiatebankname' =>'',
          'intermidiatebankaddress' =>'',

          'intermidiatebankswiftcode' =>'',
          'bank_ecs' =>'',
          'copypancart' => '',
          'copyservicetax' =>'',
          'copytan' =>'',
          'copyaddressproof' =>'',
          'emailstatus' =>'',
          'emailcurrency' =>'',
          'emailcountry' =>'',
          'username' =>$this->input->post('username'),
          'password' =>'',
          'countryid' => $data['branch']->countryid,
          'stateid' => $data['branch']->stateid,
          'cityid' => $data['branch']->cityid,
          'branch_id' => $data['branch']->branch_id,
          'locationid' => $data['branch']->branch_location,
            'bank_creditamount' =>'',
              'bank_agentcommision' =>'',
              'aadhaar_number' =>'',
          'ppid' =>$this->input->post('ppid'),
          'pppermission' =>$this->input->post('pppermission'),
          'type'=>'web',
      );

      $pathsel=api_url().'partnerapi/partnerregister/format/json/';
      $data['regsel']=curlpost($parameterregsel,$pathsel);


if($data['regsel']->scalar!='Something Went Wrong')
{
   $data['mesg']="Email Id Already Register";
}
else {











    $parameterreg = array(
        'act_mode' => 'insertpartner',
         'row_id' =>'',
        'logintype' => $this->input->post('logintype'),
        'agencyname' => $this->input->post('agencyname'),
        'office_address' => $this->input->post('office_address'),
        'city' => $this->input->post('city'),
        'state' => $this->input->post('state'),
        'pincode' => $this->input->post('pincode'),
        'telephone' => $this->input->post('telephone'),
        'mobile' => $this->input->post('mobile'),
        'fax' => $this->input->post('fax'),
        'website' => $this->input->post('website'),
        'email' => $this->input->post('email'),
        'primarycontact_name' => $this->input->post('primarycontact_name'),
        'primarycontact_email_mobile' => $this->input->post('primarycontact_email_mobile'),
        'primarycontact_telephone' => $this->input->post('primarycontact_telephone'),
        'secondrycontact_name' => $this->input->post('secondrycontact_name'),
        'secondrycontact_email_phone' => $this->input->post('secondrycontact_email_phone'),
        'secondrycontact_telephone' => $this->input->post('secondrycontact_telephone'),
        'management_executives_name' => $this->input->post('management_executives_name'),
        'management_executives_email_phone' => $this->input->post('management_executives_email_phone'),
        'management_executives_telephone' => $this->input->post('management_executives_telephone	'),
        'branches' => $this->input->post('branches'),

        'yearofestablism' => $this->input->post('yearofestablism'),
        'organisationtype' => $this->input->post('organisationtype'),
        'lstno' => $this->input->post('lstno'),
        'cstno' => $this->input->post('cstno'),
        'registrationno' => $this->input->post('registrationno'),
        'vatno' => $this->input->post('vatno'),
        'panno' => $this->input->post('panno'),
        'servicetaxno' => $this->input->post('servicetaxno'),
        'tanno' => $this->input->post('tanno'),
        'pfno' => $this->input->post('pfno'),
        'esisno' => $this->input->post('esisno'),
        'officeregistrationno' => $this->input->post('officeregistrationno'),
        'exceptiontax' => $this->input->post('exceptiontax'),
        'otherexceptiontax' => $this->input->post('otherexceptiontax'),
        'bank_benificialname' => $this->input->post('bank_benificialname'),
        'bank_benificialaccno' => $this->input->post('bank_benificialaccno'),
        'bank_benificialbankname' => $this->input->post('bank_benificialbankname'),
        'bank_benificialbranchname' => $this->input->post('bank_benificialbranchname'),
        'bank_benificialaddress' => $this->input->post('bank_benificialaddress'),
        'bank_benificialifsc' => $this->input->post('bank_benificialifsc'),
        'bank_benificialswiftcode' => $this->input->post('bank_benificialswiftcode'),
        'bank_benificialibanno' => $this->input->post('bank_benificialibanno'),
        'intermidiatebankname' => $this->input->post('intermidiatebankname'),
        'intermidiatebankaddress' => $this->input->post('intermidiatebankaddress'),

        'intermidiatebankswiftcode' => $this->input->post('intermidiatebankswiftcode'),
        'bank_ecs' => $this->input->post('bank_ecs'),
        'copypancart' => $pancart,
        'copyservicetax' => $servicetax,
        'copytan' => $tan,
        'copyaddressproof' => $addressproof,
        'emailstatus' => $this->input->post('emailstatus'),
        'emailcurrency' => $this->input->post('emailcurrency'),
        'emailcountry' => $this->input->post('emailcountry'),
        'username' => $this->input->post('username'),
        'password' => base64_encode($this->input->post('password')),

        'countryid' => $data['branch']->countryid,
        'stateid' => $data['branch']->stateid,
        'cityid' => $data['branch']->cityid,
        'branch_id' => $data['branch']->branch_id,
        'locationid' => $data['branch']->branch_location,
          'bank_creditamount' =>'',
              'bank_agentcommision' =>'',
'aadhaar_number' =>$this->input->post('aadhaar_number'),
        'ppid' =>$this->input->post('ppid'),
        'pppermission' =>$this->input->post('pppermission'),
        'type' => 'web',
    );

    $path = api_url() . 'partnerapi/partnerregister/format/json/';
    $data['reg'] = curlpost($parameterreg, $path);
     $data['mesg'] ="Agent Added Sucessfully";
}}
      $siteurl= base_url();
      $parameterbranch=array(
          'act_mode' =>'selectbranch',
          'weburl' =>$siteurl,
          'type'=>'web',

      );

      $path=api_url().'selectsiteurl/branch/format/json/';
      $data['branch']=curlpost($parameterbranch,$path);
      $this->load->view("helper/header",$data);
      $this->load->view("helper/topbar",$data);
 $this->load->view("agentregistrationupdate",$data);
     $this->load->view("helper/footer");
  }
}



    //partnerregister update   page
    public function agentregistration(){
        if ($this->session->userdata['ppid'] > 0) {
            redirect(base_url()."partnerdashboard");
        }
        else{
            $parameterseo=array('act_mode'=>'viewseotags','rowid'=>14);
            $data['seotags']=$this->supper_admin->call_procedureRow('proc_siteconfig',$parameterseo);

            if($this->input->post('submit')=='Register'){
                 $siteurl= base_url();
                  $parameterbranch=array(
                    'act_mode' =>'selectbranch',
                    'weburl' =>$siteurl,
                    'type'=>'web',

                );

                $path=api_url().'selectsiteurl/branch/format/json/';
                $data['branch']=curlpost($parameterbranch,$path);
                    $parameterregsel=array(
                    'act_mode' =>'selectpartneremail',
                        'title' =>'',
                        'firstname' =>'',
                        'lastname' =>'',
                    'row_id' =>'',
                    'logintype' =>'',
                    'agencyname' =>'',
                    'office_address' =>'',
                    'city' =>'',
                    'state' =>'',
                    'pincode' =>'',
                    'telephone' =>'',
                    'mobile' =>'',
                    'fax' =>'',
                    'website' =>'',
                    'email' =>'',
                    'primarycontact_name' =>'',
                    'primarycontact_email_mobile' =>'',
                    'primarycontact_telephone' =>'',
                    'secondrycontact_name' =>'',
                    'secondrycontact_email_phone' =>'',
                    'secondrycontact_telephone' =>'',
                    'management_executives_name' =>'',
                    'management_executives_email_phone' =>'',
                    'management_executives_telephone' =>'',
                    'branches' =>'',

                    'yearofestablism' =>'',
                    'organisationtype' =>'',
                    'lstno' =>'',
                    'cstno' =>'',
                    'registrationno' =>'',
                    'vatno' =>'',
                    'panno' =>'',
                    'servicetaxno' =>'',
                    'tanno' =>'',
                    'pfno' =>'',
                    'esisno' =>'',
                    'officeregistrationno' =>'',
                    'exceptiontax' =>'',
                    'otherexceptiontax' =>'',
                    'bank_benificialname' =>'',
                    'bank_benificialaccno' =>'',
                    'bank_benificialbankname' =>'',
                    'bank_benificialbranchname' =>'',
                    'bank_benificialaddress' =>'',
                    'bank_benificialifsc' =>'',
                    'bank_benificialswiftcode' =>'',
                    'bank_benificialibanno' =>'',
                    'intermidiatebankname' =>'',
                    'intermidiatebankaddress' =>'',

                    'intermidiatebankswiftcode' =>'',
                    'bank_ecs' =>'',
                    'copypancart' => '',
                    'copyservicetax' =>'',
                    'copytan' =>'',
                    'copyaddressproof' =>'',
                    'emailstatus' =>'',
                    'emailcurrency' =>'',
                    'emailcountry' =>'',
                    'username' =>$this->input->post('email'),
                    'password' =>'',
                    'countryid' => $data['branch']->countryid,
                    'stateid' => $data['branch']->stateid,
                    'cityid' => $data['branch']->cityid,
                    'branch_id' => $data['branch']->branch_id,
                    'locationid' => $data['branch']->branch_location,
                    'bank_creditamount' =>'',
                    'bank_agentcommision' =>'',
                    'aadhaar_number' =>'',
                        'ppid' =>$this->input->post('ppid'),
                        'pppermission' =>$this->input->post('pppermission'),
                    'type'=>'web',
                );

                $pathsel=api_url().'partnerapi/partnerregister/format/json/';
                $data['regsel']=curlpost($parameterregsel,$pathsel);


                if($data['regsel']->scalar!='Something Went Wrong')
                {
                    $data['mesg']="Email Id Already Register";
                }
                else {


                    $siteurl= base_url();
                    $parameterbranch=array(
                        'act_mode' =>'selectbranch',
                        'weburl' =>$siteurl,
                        'type'=>'web',

                    );

                    $path=api_url().'selectsiteurl/branch/format/json/';
                    $data['branch']=curlpost($parameterbranch,$path);



                    //select banner images
                    $parameterbanner = array(
                        'act_mode' => 'selectbannerimages',
                        'branchid' => $data['branch']->branch_id,
                        'type' => 'web',

                    );

                    $path = api_url() . 'selectsiteurl/banner/format/json/';
                    $data['banner'] = curlpost($parameterbanner, $path);





                    $message_new = '<table width="90%" style="line-height: 28px; font-family: sans-serif;"   >
<tr><td>Congratulations ' . $this->input->post("title").". " . ucfirst($this->input->post("first_name")).'&nbsp;'. $this->input->post("last_name") . ',</td></tr>

<tr><td>You have been successfully registered with  ' . $data['banner']->bannerimage_top3 . '.!</td></tr>

<tr><td>
You have been successfully registered with ' . $data['banner']->bannerimage_top3 . '. You can use ' . $this->input->post("email") . ' as your Login ID to access your ' . $data['banner']->bannerimage_top3 . ' Account online. 
</td></tr>
<tr><td>Please do not disclose/share your password to anyone.</td></tr>

<tr><td>Should you have any queries, please feel free to write to us on '.$data['banner']->bannerimage_branch_email.' or call us on '.$data['banner']->bannerimage_branch_contact.'.</td></tr>
<tr><td>Yours sincerely,<br>
'.$data['banner']->bannerimage_top3.' Team</td></tr>
</table>
';


                    $from_email = $data['banner']->bannerimage_from ;
                    $to_email = $this->input->post('email');
                    //Load email library
                    $this->load->library('email');
                    $this->email->from($from_email,  $data['banner']->bannerimage_top3 );
                    $this->email->reply_to($from_email,  $data['banner']->bannerimage_top3 );
                    $this->email->to($to_email);
                    $this->email->subject('' . $data['banner']->bannerimage_top3 . ' - Your Registration Details');
                    $this->email->message($message_new);
                    //Send mail
                    $this->email->send();
                  //  pend($this->email->print_debugger());


                    $parameterreg = array(
                        'act_mode' => 'insertpartnerstartstage',
                        'title' =>$this->input->post('title'),
                        'firstname' =>$this->input->post('first_name'),
                        'lastname' =>$this->input->post('last_name'),
                        'row_id' =>'',
                        'logintype' => '',
                        'agencyname' =>'',
                        'office_address' => '',
                        'city' => '',
                        'state' => '',
                        'pincode' => '',
                        'telephone' => '',
                        'mobile' => $this->input->post('mobile_number'),
                        'fax' => '',
                        'website' => '',
                        'email' => $this->input->post('email'),
                        'primarycontact_name' => '',
                        'primarycontact_email_mobile' => '',
                        'primarycontact_telephone' => '',
                        'secondrycontact_name' => '',
                        'secondrycontact_email_phone' => '',
                        'secondrycontact_telephone' => '',
                        'management_executives_name' => '',
                        'management_executives_email_phone' => '',
                        'management_executives_telephone' => '',
                        'branches' =>'',

                        'yearofestablism' =>'',
                        'organisationtype' =>'',
                        'lstno' => '',
                        'cstno' => '',
                        'registrationno' => '',
                        'vatno' =>'',
                        'panno' => '',
                        'servicetaxno' => '',
                        'tanno' => '',
                        'pfno' => '',
                        'esisno' => '',
                        'officeregistrationno' => '',
                        'exceptiontax' => '',
                        'otherexceptiontax' => '',
                        'bank_benificialname' => '',
                        'bank_benificialaccno' => '',
                        'bank_benificialbankname' => '',
                        'bank_benificialbranchname' => '',
                        'bank_benificialaddress' => '',
                        'bank_benificialifsc' => '',
                        'bank_benificialswiftcode' => '',
                        'bank_benificialibanno' => '',
                        'intermidiatebankname' => '',
                        'intermidiatebankaddress' => '',
                        'intermidiatebankswiftcode' => '',
                        'bank_ecs' => '',
                        'copypancart' =>'',
                        'copyservicetax' => '',
                        'copytan' => '',
                        'copyaddressproof' => '',
                        'emailstatus' => '',
                        'emailcurrency' => '',
                        'emailcountry' => '',
                        'username' => $this->input->post('email'),
                        'password' => base64_encode($this->input->post('password')),

                        'countryid' => $data['branch']->countryid,
                        'stateid' => $data['branch']->stateid,
                        'cityid' => $data['branch']->cityid,
                        'branch_id' => $data['branch']->branch_id,
                        'locationid' => $data['branch']->branch_location,
                        'bank_creditamount' =>'',
                        'bank_agentcommision' =>'',

'aadhaar_number' =>$this->input->post('aadhaar_number'),
                        'ppid' =>$this->input->post('ppid'),
                        'pppermission' =>$this->input->post('pppermission'),
                        'type' => 'web',
                    );

                    $path = api_url() . 'partnerapi/partnerregister/format/json/';
                    $data['reg'] = curlpost($parameterreg, $path);


                    $data = array(

                        'pemailid' => $this->input->post('email'),
                        'pid' => $data['reg']->lid,

                    );
    $this->session->set_userdata($data);


                 header("location:agentform1");
                  //  $data['mesg'] ="Agent Added Sucessfully";
                }}
            $siteurl= base_url();
            $parameterbranch=array(
                'act_mode' =>'selectbranch',
                'weburl' =>$siteurl,
                'type'=>'web',

            );

            $path=api_url().'selectsiteurl/branch/format/json/';
            $data['branch']=curlpost($parameterbranch,$path);
            $this->load->view("helper/header",$data);
            $this->load->view("helper/topbar",$data);
            $this->load->view("agentregistration",$data);
            $this->load->view("helper/footer");
        }
    }


    public function agentforget(){
        if ($this->session->userdata['pid'] > 0) {
            redirect(base_url()."partnerdashboard");
        }
else {
    $siteurl= base_url();
    $parameterbranch=array(
        'act_mode' =>'selectbranch',
        'weburl' =>$siteurl,
        'type'=>'web',
    );

    $path=api_url().'selectsiteurl/branch/format/json/';
    $data['branch']=curlpost($parameterbranch,$path);

    $new_pass = base64_encode(rand(000000,999999));


    $parameterupdatepassword_newpass = array(
        'act_mode' => 'forgetpassword_newpassagent',
        'email' => $this->input->post('emailmob'),
        'Param3' => $new_pass,
        'type' => 'web'
    );
//p($parameterupdatepassword_newpass);
    $path_newpass = api_url() . 'userapi/updatepassword/format/json/';
    $data['updateacc_new'] = curlpost($parameterupdatepassword_newpass, $path_newpass);

    $parameterupdatepassword = array(
        'act_mode' => 'forgetpasswordagent',
        'email' => $this->input->post('emailmob'),
        'type' => 'web'
    );

    $path = api_url() . 'userapi/updatepassword/format/json/';
    $data['updateacc'] = curlpost($parameterupdatepassword, $path);




    //pend($data['updateacc_new']);

    $message_new = '<table width="90%" style="line-height: 28px; font-family: sans-serif;"   >
<tr><td>Congratulations ' . $data['updateacc']->agent_agencyname.',</td></tr>
<tr><td>Greetings from ' . $data['banner']->bannerimage_top3 . '..!</td></tr>
<tr><td>You have received this communication in response to your request for your ' . $data['banner']->bannerimage_top3 . '. Account password to be sent to you via e-mail and sms.</td></tr>
<tr><td>Your temporary password is: '.base64_decode($data['updateacc']->agent_password).' </td></tr>
<tr><td>Please use the password exactly as it appears above.</td></tr>
<tr><td>We request you immediately change your password by logging on to '.base_url().'</td></tr>
<tr><td>Should you have any queries, please feel free to write to us on '.$data['banner']->bannerimage_branch_email.' or call us on '.$data['banner']->bannerimage_branch_contact.'.</td></tr>
<tr><td>Yours sincerely,<br>
'.$data['banner']->bannerimage_top3.' Team</td></tr>
</table>
';


    $from_email = $data['banner']->bannerimage_from ;
    $to_email = $this->input->post('emailmob');
    //Load email library
    $this->load->library('email');
    $this->email->from($from_email,  $data['banner']->bannerimage_top3 );
    $this->email->reply_to($from_email,  $data['banner']->bannerimage_top3 );
    $this->email->subject('' . $data['banner']->bannerimage_top3 . ' - Your Request For Password Reset');
    $this->email->message($message_new);
    //Send mail
    $this->email->send();
    //pend($this->email->print_debugger());


    $this->load->view("helper/header",$data);
    $this->load->view("helper/topbar",$data);
    $this->load->view("agentforget",$data);
    $this->load->view("helper/footer");
}
}

//update partner profile   updatepartnerrprofile
public function updatepartnerrprofile(){


    if ($this->input->post('submit')) {


        $configUpload['upload_path'] = 'assets/admin/images/partner';              #the folder placed in the root of project
        $configUpload['allowed_types'] = 'gif|jpg|png|bmp|jpeg';       #allowed types description
        $configUpload['max_size'] = '0';                          #max size
        $configUpload['max_width'] = '0';                          #max width
        $configUpload['max_height'] = '0';                          #max height
        $configUpload['encrypt_name'] = true;                         #encrypt name of the uploaded file
        $this->load->library('upload', $configUpload);
        if ($_FILES['copypancart']['name'] != '') {
            $this->upload->do_upload('copypancart');
            $pancart = ($this->upload->data('copypancart')['file_name']);
        } else {
            $pancart = $_POST['copypancartdata'];
        }
        if ($_FILES['copyservicetax']['name'] != '') {
            $this->upload->do_upload('copyservicetax');
            $servicetax = ($this->upload->data('copyservicetax')['file_name']);
        } else {
            $servicetax = $_POST['copyservicetaxdata'];
        }
        if ($_FILES['copytan']['name'] != '') {
            $this->upload->do_upload('copytan');
            $tan = ($this->upload->data('copytan')['file_name']);
        }

        else {
            $tan = $_POST['copytandata'];
        }


        if ($_FILES['copyaddressproof']['name'] != '') {
            $this->upload->do_upload('copyaddressproof');
            $addressproof = ($this->upload->data('copyaddressproof')['file_name']);
        } else {
            $addressproof = $_POST['copyaddressproofdata'];
        }


        $parameterregupdate = array(
            'act_mode' => 'updatepartner',
            'title' =>$this->input->post('title'),
            'firstname' =>$this->input->post('firstname'),
            'lastname' =>$this->input->post('lastname'),
            'row_id' => $this->session->userdata['ppid'],


            'logintype' => $this->input->post('logintype'),
            'agencyname' => $this->input->post('agencyname'),
            'office_address' => $this->input->post('office_address'),
            'city' => $this->input->post('city'),
            'state' => $this->input->post('state'),
            'pincode' => $this->input->post('pincode'),
            'telephone' => $this->input->post('telephone'),
            'mobile' => $this->input->post('mobile'),
            'fax' => $this->input->post('fax'),
            'website' => $this->input->post('website'),
            'email' => $this->input->post('email'),
            'primarycontact_name' => $this->input->post('primarycontact_name'),
            'primarycontact_email_mobile' => $this->input->post('primarycontact_email_mobile'),
            'primarycontact_telephone' => $this->input->post('primarycontact_telephone'),
            'secondrycontact_name' => $this->input->post('secondrycontact_name'),
            'secondrycontact_email_phone' => $this->input->post('secondrycontact_email_phone'),
            'secondrycontact_telephone' => $this->input->post('secondrycontact_telephone'),
            'management_executives_name' => $this->input->post('management_executives_name'),
            'management_executives_email_phone' => $this->input->post('management_executives_email_phone'),
            'management_executives_telephone' => $this->input->post('management_executives_telephone	'),
            'branches' => $this->input->post('branches'),

            'yearofestablism' => $this->input->post('yearofestablism'),
            'organisationtype' => $this->input->post('organisationtype'),
            'lstno' => $this->input->post('lstno'),
            'cstno' => $this->input->post('cstno'),
            'registrationno' => $this->input->post('registrationno'),
            'vatno' => $this->input->post('vatno'),
            'panno' => $this->input->post('panno'),
            'servicetaxno' => $this->input->post('servicetaxno'),
            'tanno' => $this->input->post('tanno'),
            'pfno' => $this->input->post('pfno'),
            'esisno' => $this->input->post('esisno'),
            'officeregistrationno' => $this->input->post('officeregistrationno'),
            'exceptiontax' => $this->input->post('exceptiontax'),
            'otherexceptiontax' => $this->input->post('otherexceptiontax'),
            'bank_benificialname' => $this->input->post('bank_benificialname'),
            'bank_benificialaccno' => $this->input->post('bank_benificialaccno'),
            'bank_benificialbankname' => $this->input->post('bank_benificialbankname'),
            'bank_benificialbranchname' => $this->input->post('bank_benificialbranchname'),
            'bank_benificialaddress' => $this->input->post('bank_benificialaddress'),
            'bank_benificialifsc' => $this->input->post('bank_benificialifsc'),
            'bank_benificialswiftcode' => $this->input->post('bank_benificialswiftcode'),
            'bank_benificialibanno' => $this->input->post('bank_benificialibanno'),
            'intermidiatebankname' => $this->input->post('intermidiatebankname'),
            'intermidiatebankaddress' => $this->input->post('intermidiatebankaddress'),

            'intermidiatebankswiftcode' => $this->input->post('intermidiatebankswiftcode'),
            'bank_ecs' => $this->input->post('bank_ecs'),
            'copypancart' => $pancart,
            'copyservicetax' => $servicetax,
            'copytan' => $tan,
            'copyaddressproof' => $addressproof,
            'emailstatus' => $this->input->post('emailstatus'),
            'emailcurrency' => $this->input->post('emailcurrency'),
            'emailcountry' => $this->input->post('emailcountry'),
            'username' => $this->input->post('username'),
            'password' => base64_encode($this->input->post('password')),

            'countryid' => '',
            'stateid' => '',
            'cityid' => '',
            'branch_id' => $this->input->post('username'),
            'locationid' => '',
            'bank_creditamount' => $this->input->post('bank_creditamount'),
            'bank_agentcommision' => $this->input->post('bank_agentcommision'),
            'bank_agentcommision' => $this->input->post('bank_agentcommision'),
            'aadhaar_number' =>$this->input->post('aadhaar_number'),

            'ppid' =>$this->input->post('ppid'),
            'pppermission' =>$this->input->post('pppermission'),
        );

        $response['regselupdate'] = $this->supper_admin->call_procedure('proc_partnerregister_v', $parameterregupdate);

        $data['emsg'] = "Edit Sucessfully";

       // header("Location:agent");

    }



    $parametersel = array(


        'act_mode' => 'selectpartnerdata',
        'title' =>'',
        'firstname' =>'',
        'lastname' =>'',
        'row_id' => $this->session->userdata['ppid'],

        'logintype' => '',
        'agencyname' => '',
        'office_address' => '',
        'city' => '',
        'state' => '',
        'pincode' => '',
        'telephone' => '',
        'mobile' => '',
        'fax' => '',
        'website' => '',
        'email' => '',
        'primarycontact_name' => '',
        'primarycontact_email_mobile' => '',
        'primarycontact_telephone' => '',
        'secondrycontact_name' => '',
        'secondrycontact_email_phone' => '',
        'secondrycontact_telephone' => '',
        'management_executives_name' => '',
        'management_executives_email_phone' => '',
        'management_executives_telephone' => '',
        'branches' => '',

        'yearofestablism' => '',
        'organisationtype' => '',
        'lstno' => '',
        'cstno' => '',
        'registrationno' => '',
        'vatno' => '',
        'panno' => '',
        'servicetaxno' => '',
        'tanno' => '',
        'pfno' => '',
        'esisno' => '',
        'officeregistrationno' => '',
        'exceptiontax' => '',
        'otherexceptiontax' => '',
        'bank_benificialname' => '',
        'bank_benificialaccno' => '',
        'bank_benificialbankname' => '',
        'bank_benificialbranchname' => '',
        'bank_benificialaddress' => '',
        'bank_benificialifsc' => '',
        'bank_benificialswiftcode' => '',
        'bank_benificialibanno' => '',
        'intermidiatebankname' => '',
        'intermidiatebankaddress' => '',

        'intermidiatebankswiftcode' => '',
        'bank_ecs' => '',
        'copypancart' => '',
        'copyservicetax' => '',
        'copytan' => '',
        'copyaddressproof' => '',
        'emailstatus' => '',
        'emailcurrency' => '',
        'emailcountry' => '',
        'username' => '',
        'password' => '',

        'countryid' => '',
        'stateid' => '',
        'cityid' => '',
        'branch_id' => '',
        'locationid' => '',
        'bank_creditamount' => '',
        'bank_agentcommision' => '',
        'aadhaar_number' =>'',
        'ppid' =>$this->input->post('ppid'),
        'pppermission' =>$this->input->post('pppermission'),
       
    );

 $data['agentviewwdata'] = $this->supper_admin->call_procedurerow('proc_partnerregister_v', $parametersel);




    //$path = api_url() . 'partnerapi/partnerregister/format/json/';
    //$data['agentviewwdata'] = curlpost($parametersel, $path);


    $this->load->view("agent/helper/header",$data);
    $this->load->view("agent/helper/leftbar",$data);
    $this->load->view("agent/updatepartnerrprofile",$data);
    $this->load->view("agent/helper/footer");
}

    public function changepartnerpassword(){


        if ($this->input->post('submit') == 'changePswrd') {
            $this->form_validation->set_rules('currentpassword', 'Enter Current  password ', 'required');
            $this->form_validation->set_rules('newpassword', 'Enter  password ', 'required');
          $this->form_validation->set_rules('confirmpassword', 'Enter Confirm  password ', 'required');


            if ($this->input->post('confirmpassword') != $this->input->post('newpassword')) {

                $this->form_validation->set_rules('confirmpassword', ' Password Not Match', 'required');
            }



            if ($this->form_validation->run() != FALSE) {

                $parametercheckpasswrd = array(
                    'act_mode' => 'partncheckpassword',
                    'userid' => $this->session->userdata['ppid'],
                    'currentpassword' => $this->input->post('currentpassword'),
                    'newpassword' => '',
                    'confirmpassword' => '',

                    'type' => 'web',
                );

                $path = api_url() . 'userapi/usercheckchangepassword/format/json/';
                $data['checkpasswrd'] = curlpost($parametercheckpasswrd, $path);
              
//echo (base64_decode($data['checkpasswrd']->agent_password)."---".$this->input->post('currentpassword');
                if ((base64_decode($data['checkpasswrd']->agent_password)) == $this->input->post('currentpassword')) {

                    $parameter = array(
                        'act_mode' => 'partnchangepassword',
                        'userid' => $this->session->userdata['ppid'],
                        'currentpassword' => $this->input->post('currentpassword'),
                        'newpassword' => $this->input->post('newpassword'),
                        'confirmpassword' => $this->input->post('confirmpassword'),
                        'type' => 'web',
                    );

                    $path = api_url() . 'userapi/userchangepassword/format/json/';
                    $data['userlog'] = curlpost($parameter, $path);

                    $data['message'] = "Password Change Sucessfully";

                } else {


                    $data['message'] = " Invalid Password ";

                }

            }
        }



        $this->load->view("agent/helper/header",$data);
        $this->load->view("agent/helper/leftbar",$data);
        $this->load->view("agent/changepartnerpassword",$data);
        $this->load->view("agent/helper/footer");
    }


     public function order_sess(){
		 
        if ($_POST['Clear'] == 'Show All Records')
        {
            $this->session->unset_userdata('order_filter');
        }

        $a = explode(":", $this->input->post('filter_date_session'));
        $b = explode(" ", $this->input->post('filter_date_session'));

        if ($b['1'] == 'PM') {
            $session_time = $a['0'] + 12;
        } else {
            $session_time = $a['0'];
        }

        $a = $this->input->post('filter_branch');
        $b = $this->input->post('filter_status');
        $c = $this->input->post('filter_tim');
        $d = $this->input->post('filter_sta');
        $branchids = implode(',', $a);
        $filterstatus = implode(',', $b);
        $filtertime = implode(',', $c);
        $filtersta = implode(',', $d);

        $array = array('branchids' => $branchids,
        'filter_name' => $this->input->post('filter_name'),
        'filter_email' => $this->input->post('filter_email'),
        'filter_ticket' => $this->input->post('filter_ticket'),
        'filter_mobile' => $this->input->post('filter_mobile'),
        'filter_payment' => $this->input->post('filter_payment'),
        'filter_date_booking_from' => $this->input->post('filter_date_booking_from'),
        'filter_date_booking_to' => $this->input->post('filter_date_booking_to'),
        'filter_date_session_from' => $this->input->post('filter_date_session_from'),
        'filter_date_session_to' => $this->input->post('filter_date_session_to'),
        'filter_status' => $filterstatus,
        'filter_date_printed_from' => $this->input->post('filter_date_printed_from'),
        'filter_date_printed_to' => $this->input->post('filter_date_printed_to'),
        'filter_date_ticket' => $this->input->post('filter_date_ticket'),
        'filter_date_session' => $session_time,
        'filter_tim' => $filtertime,
        'filter_sta' => $filtersta
        );

        $this->session->set_userdata('order_filter', $array);
        redirect('order');
    }


 //Show Order List Of Agent
 public function order(){ 
     if($this->session->userdata('order_filter'))
       { 
                 $parameter1 = array('act_mode' => 'S_vieworderagent',
                'Param1' => $this->session->userdata('order_filter')['branchids'],
                'Param2' => $this->session->userdata('order_filter')['filter_name'],
                'Param3' => $this->session->userdata('order_filter')['filter_email'],
                'Param4' => $this->session->userdata('order_filter')['filter_ticket'],
                'Param5' => $this->session->userdata('order_filter')['filter_mobile'],
                'Param6' => $this->session->userdata('order_filter')['filter_payment'],
                'Param7' => $this->session->userdata('order_filter')['filter_date_booking_from'],
                'Param8' => $this->session->userdata('order_filter')['filter_date_booking_to'],
                'Param9' => $this->session->userdata('order_filter')['filter_date_session_from'],
                'Param10' => $this->session->userdata('order_filter')['filter_date_session_to'],
                'Param11' => $this->session->userdata('order_filter')['filter_status'],
                'Param12' => $this->session->userdata('order_filter')['filter_date_printed_from'],
                'Param13' => $this->session->userdata('order_filter')['filter_date_printed_to'],
                'Param14' => $this->session->userdata('order_filter')['filter_date_ticket'],
                'Param15' => $this->session->userdata('order_filter')['filter_date_session'],
                'Param16' => $this->session->userdata('order_filter')['filter_tim'],
                'Param17' => $this->session->userdata('order_filter')['filter_sta'],
                'Param18' => '',
                'Param19' =>$this->session->userdata('ppid')
                 );
            foreach($parameter1 as $key=>$val){
                if($parameter1[$key] == '')
                {
                    $parameter1[$key] =-1;
                }
            }
            $response['vieww_order'] = $this->supper_admin->call_procedure('proc_order_filter_v', $parameter1);
            $this->session->unset_userdata('order_filter');


        }else{
		   
            $parameter1 = array(
				'act_mode' => 'S_vieworder_new',
                'Param1' => '',
                'Param2' => '',
                'Param3' => '',
                'Param4' => '',
                'Param5' => '',
                'Param6' => '',
                'Param7' => '',
                'Param8' => '',
                'Param9' => $this->session->userdata('ppid') 
            ); 
      
            $response['vieww_order'] = $this->supper_admin->call_procedure('proc_order_partner', $parameter1);
     }

        $parameter_time = array('act_mode' => 'branch_list_for_filter',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' =>'',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' =>'',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',
            'Param16' => '',
            'Param17' => '',
            'Param18' => '',
            'Param19' => '');
        $response['vieww_time'] = $this->supper_admin->call_procedure('proc_order_filter_s', $parameter_time);

        $parameter_status = array('act_mode' => 'status_list_for_filter',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' =>'',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' =>'',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',
            'Param16' => '',
            'Param17' => '',
            'Param18' => '',
            'Param19' => '');
        $response['vieww_status'] = $this->supper_admin->call_procedure('proc_order_filter_s',$parameter_status);
//pend($response['vieww_status']);
        $parameter2 = array( 'act_mode'=>'s_viewbranch',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        $response['s_viewbranch'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter2);

        
        $siteurl = base_url();
        $parameterbranch = array(
            'act_mode' => 'selectbranch',
            'weburl' => $siteurl,


        );
        $response['branch'] = $this->supper_admin->call_procedurerow('proc_select_branch_v', $parameterbranch);
//select banner images
        $parameterbanner = array(
            'act_mode' => 'selectbannerimages',
            'branchid' =>  $response['branch']->branch_id,


        );
        $response['banner'] = $this->supper_admin->call_procedurerow('proc_select_banner_v', $parameterbanner);
	 
     	$parametertearms = array(
            'act_mode' => 'selecttearms',
            'branchid' => $response['branch']->branch_id,


        );
        $response['tearmsgatway'] = $this->supper_admin->call_procedure('proc_select_banner_v', $parametertearms);

        $this->load->view("agent/helper/header",$response);
        $this->load->view("agent/helper/leftbar",$response);
        $this->load->view("agent/order",$response);
        $this->load->view("agent/helper/footer");
    }

    public function updateamount(){

        if ($this->session->userdata['ppid'] < 0) {
            redirect(base_url()."partnerdashboard");
        }
        else {
            $siteurl= base_url();
            $parameterbranch=array(
                'act_mode' =>'selectbranch',
                'weburl' =>$siteurl,
                'type'=>'web',
            );

            $path=api_url().'selectsiteurl/branch/format/json/';
            $data['branch']=curlpost($parameterbranch,$path);


            $userdisplay = array('act_mode'=>'select_partner',
                'userid'=>$this->session->userdata['ppid'],
                'type'=>'web',

            );
            $path3 = api_url()."Ordersucess/selectuser/format/json/";
            $data['memuser']= curlpost($userdisplay,$path3);
            $parameterccgatway=array(
                'act_mode' =>'selectccavenue',
                'branchid' =>$data['branch']->branch_id,
                'type'=>'web',

            );

            $path=api_url().'ccavenue/ccavRequestHandler/format/json/';
            $data['ccavRequestHandler']=curlpost($parameterccgatway,$path);
            $walletselect = array('act_mode' => 'selectwalletdata',
                'orderid' => '',
                'tracking_id' => '',
                'order_status' => '',
                'status_message' => '',
                'paymentmode' => '',
                'paymentstatus' => '',
                'ordersucesmail' => '',
                'ordermailstatus' => '',
                'agent_id' => '',
                'cred_debet' => '',
                'amount' => '',
                'bankagentcommision' => '',
                'type' => 'web',

            );

            $path = api_url() . "Ordersucess/selectwalletdata/format/json/";
            $data['orderwalletselect'] = curlpost($walletselect, $path);


      $parameter = array('act_mode' => 'agentamount_show',
            'Param1' => $this->session->userdata('ppid'),
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
      // p($parameter);exit();
        $data['agent_show'] = $this->supper_admin->call_procedurerow('proc_agent_s', $parameter);
       // p($data['agent_show']);exit();

           

 if($this->input->post('submit')=='cartses') {
     $data = array(
         'tid' => '12345',
         'title' => $this->input->post('title'),
         'billing_name' => $this->input->post('billing_name'),
         'billing_email' => $this->input->post('billing_email'),
         'billing_tel' => $this->input->post('billing_tel'),
         'billing_address' => $this->input->post('billing_address'),
         'billing_city' => $this->input->post('billing_city'),
         'billing_state' => $this->input->post('billing_state'),
         'billing_zip' => $this->input->post('billing_zip'),
         'billing_country' => 'India',
         'merchant_id' => $data['ccavRequestHandler']->pg_merchant_id,
         'order_id' => $data['ccavRequestHandler']->pg_prefix . '_' . $this->session->userdata('ppid'),
         'order_idval' => $this->session->userdata('ppid'),
         'user_id' => $this->session->userdata('ppid'),
         'amount' =>  $this->input->post('amount'),
         'currency' => $data['ccavRequestHandler']->pg_currency,
         'redirect_url' => $data['ccavRequestHandler']->pg_sucess_link,
         'cancel_url' => $data['ccavRequestHandler']->pg_fail_link,
         'language' => $data['ccavRequestHandler']->pg_language,

     );
     $this->session->set_userdata('ccavenue_param', $data);
     redirect(base_url()."ccavRequestHandler");
 }
            $this->load->view("agent/helper/header",$data);
            $this->load->view("agent/helper/leftbar",$data);
            $this->load->view("agent/updateamount",$data);
            $this->load->view("agent/helper/footer");
        }
    }


public function partners(){

    $parameterreg = array(
        'act_mode' => 'selectsubpartner',
        'title' =>'',
        'firstname' =>'',
        'lastname' =>'',
        'row_id' =>'',
        'logintype' => '',
        'agencyname' =>'',
        'office_address' => '',
        'city' => '',
        'state' => '',
        'pincode' => '',
        'telephone' => '',
        'mobile' => '',
        'fax' => '',
        'website' => '',
        'email' => '',
        'primarycontact_name' => '',
        'primarycontact_email_mobile' => '',
        'primarycontact_telephone' => '',
        'secondrycontact_name' => '',
        'secondrycontact_email_phone' => '',
        'secondrycontact_telephone' => '',
        'management_executives_name' => '',
        'management_executives_email_phone' => '',
        'management_executives_telephone' => '',
        'branches' =>'',

        'yearofestablism' =>'',
        'organisationtype' =>'',
        'lstno' => '',
        'cstno' => '',
        'registrationno' => '',
        'vatno' =>'',
        'panno' => '',
        'servicetaxno' => '',
        'tanno' => '',
        'pfno' => '',
        'esisno' => '',
        'officeregistrationno' => '',
        'exceptiontax' => '',
        'otherexceptiontax' => '',
        'bank_benificialname' => '',
        'bank_benificialaccno' => '',
        'bank_benificialbankname' => '',
        'bank_benificialbranchname' => '',
        'bank_benificialaddress' => '',
        'bank_benificialifsc' => '',
        'bank_benificialswiftcode' => '',
        'bank_benificialibanno' => '',
        'intermidiatebankname' => '',
        'intermidiatebankaddress' => '',
        'intermidiatebankswiftcode' => '',
        'bank_ecs' => '',
        'copypancart' =>'',
        'copyservicetax' => '',
        'copytan' => '',
        'copyaddressproof' => '',
        'emailstatus' => '',
        'emailcurrency' => '',
        'emailcountry' => '',
        'username' => '',
        'password' => '',

        'countryid' => $data['branch']->countryid,
        'stateid' => $data['branch']->stateid,
        'cityid' => $data['branch']->cityid,
        'branch_id' => $data['branch']->branch_id,
        'locationid' => $data['branch']->branch_location,
        'bank_creditamount' =>($this->session->userdata('ppid') != 0) ? $this->session->userdata('ppid') : '',
        'bank_agentcommision' =>'',

        'aadhaar_number' =>'',
        'ppid' =>'',
        'pppermission' =>'',
        'type' => 'web',
    );


    $path = api_url() . 'partnerapi/partnerregisterpartner/format/json/';
    $data['reg'] = curlpost($parameterreg, $path);

    $this->load->view("agent/helper/header",$data);
    $this->load->view("agent/helper/leftbar",$data);
    $this->load->view("agent/partners",$data);
    $this->load->view("agent/helper/footer");

}

public function agentedit()
{

    if($this->input->post('submit'))

    {



        $parameterregupdate=array(
            'act_mode' => 'updateagentpartner',
            'title' => $this->input->post('title'),
            'firstname' => $this->input->post('first_name'),
            'lastname' => $this->input->post('last_name'),
            'row_id' => ($_GET['edid']),
            'logintype' => '',
            'agencyname' => '',
            'office_address' => '',
            'city' => '',
            'state' => '',
            'pincode' => '',
            'telephone' => '',
            'mobile' => '',
            'fax' => '',
            'website' => '',
            'email' => '',
            'primarycontact_name' => '',
            'primarycontact_email_mobile' => '',
            'primarycontact_telephone' => '',
            'secondrycontact_name' => '',
            'secondrycontact_email_phone' => '',
            'secondrycontact_telephone' => '',
            'management_executives_name' => '',
            'management_executives_email_phone' =>'',
            'management_executives_telephone' => '',
            'branches' => '',
            'yearofestablism' => '',
            'organisationtype' => '',
            'lstno' =>'',
            'cstno' => '',
            'registrationno' =>'',
            'vatno' => '',
            'panno' => '',
            'servicetaxno' => '',
            'tanno' => '',
            'pfno' => '',
            'esisno' => '',
            'officeregistrationno' => '',
            'exceptiontax' => '',
            'otherexceptiontax' => '',
            'bank_benificialname' => '',
            'bank_benificialaccno' => '',
            'bank_benificialbankname' => '',
            'bank_benificialbranchname' => '',
            'bank_benificialaddress' => '',
            'bank_benificialifsc' => '',
            'bank_benificialswiftcode' => '',
            'bank_benificialibanno' => '',
            'intermidiatebankname' => '',
            'intermidiatebankaddress' => '',
            'intermidiatebankswiftcode' => '',
            'bank_ecs' => '',
            'copypancart' => '',
            'copyservicetax' => '',
            'copytan' => '',
            'copyaddressproof' => '',
            'emailstatus' => '',
            'emailcurrency' => '',
            'emailcountry' => '',
            'username' =>'',
            'password' => '',
            'countryid' => '',
            'stateid' => '',
            'cityid' => '',
            'branch_id' => '',
            'locationid' => '',
            'bank_creditamount' => '',
            'bank_agentcommision' => '',
            'aadhaar_number' =>'',
            'ppid' =>'',
            'pppermission' =>'',
        );

        $data['regselupdate'] =   $this->supper_admin->call_procedure('proc_partnerregister_v', $parameterregupdate);

        $data['emsg'] ="Added Sucessfully";

     header("Location:partners");

    }



    $parametersel=array(
		'act_mode' => 'selectpartnerdata',
        'title' => '',
        'firstname' => '',
        'lastname' => '',
        'row_id' =>  ($_GET['edid']),
        'logintype' => '',
        'agencyname' => '',
        'office_address' => '',
        'city' => '',
        'state' => '',
        'pincode' => '',
        'telephone' => '',
        'mobile' => '',
        'fax' => '',
        'website' => '',
        'email' => '',
        'primarycontact_name' => '',
        'primarycontact_email_mobile' => '',
        'primarycontact_telephone' => '',
        'secondrycontact_name' => '',
        'secondrycontact_email_phone' => '',
        'secondrycontact_telephone' => '',
        'management_executives_name' => '',
        'management_executives_email_phone' =>'',
        'management_executives_telephone' => '',
        'branches' => '',
'yearofestablism' => '',
        'organisationtype' => '',
        'lstno' =>'',
        'cstno' => '',
        'registrationno' =>'',
        'vatno' => '',
        'panno' => '',
        'servicetaxno' => '',
        'tanno' => '',
        'pfno' => '',
        'esisno' => '',
        'officeregistrationno' => '',
        'exceptiontax' => '',
        'otherexceptiontax' => '',
        'bank_benificialname' => '',
        'bank_benificialaccno' => '',
        'bank_benificialbankname' => '',
        'bank_benificialbranchname' => '',
        'bank_benificialaddress' => '',
        'bank_benificialifsc' => '',
        'bank_benificialswiftcode' => '',
        'bank_benificialibanno' => '',
        'intermidiatebankname' => '',
        'intermidiatebankaddress' => '',
'intermidiatebankswiftcode' => '',
        'bank_ecs' => '',
        'copypancart' => '',
        'copyservicetax' => '',
        'copytan' => '',
        'copyaddressproof' => '',
        'emailstatus' => '',
        'emailcurrency' => '',
        'emailcountry' => '',
        'username' =>'',
        'password' => '',
'countryid' => '',
        'stateid' => '',
        'cityid' => '',
        'branch_id' => '',
        'locationid' => '',
        'bank_creditamount' => '',
        'bank_agentcommision' => '',
        'aadhaar_number' =>'',
        'ppid' =>'',
        'pppermission' =>'',

    );

 //$path = api_url() . 'partnerapi/partnerregisterpartner/format/json/';
  //  $data['agentviewwdata'] = curlpost($parameterreg, $path);

   $data['agentviewwdata'] =   $this->supper_admin->call_procedurerow('proc_partnerregister_v', $parametersel);




    $this->load->view("agent/helper/header",$data);
    $this->load->view("agent/helper/leftbar",$data);
    $this->load->view("agent/partnersedit",$data);
    $this->load->view("agent/helper/footer");

}


public function agentstatus(){
    $rowid = $this->uri->segment(4);
    $status = $this->uri->segment(5);
    $act_mode = $status == '1' ? 'activeandinactive' : 'inactiveandinactive';
    $parameter = array('act_mode' => $act_mode,
        'title' => '',
        'firstname' => '',
        'lastname' => '',
        'row_id' => $rowid,
        'logintype' => '',
        'agencyname' => '',
        'office_address' => '',
        'city' => '',
        'state' => '',
        'pincode' => '',
        'telephone' => '',
        'mobile' => '',
        'fax' => '',
        'website' => '',
        'email' => '',
        'primarycontact_name' => '',
        'primarycontact_email_mobile' => '',
        'primarycontact_telephone' => '',
        'secondrycontact_name' => '',
        'secondrycontact_email_phone' => '',
        'secondrycontact_telephone' => '',
        'management_executives_name' => '',
        'management_executives_email_phone' =>'',
        'management_executives_telephone' => '',
        'branches' => '',

        'yearofestablism' => '',
        'organisationtype' => '',
        'lstno' =>'',
        'cstno' => '',
        'registrationno' =>'',
        'vatno' => '',
        'panno' => '',
        'servicetaxno' => '',
        'tanno' => '',
        'pfno' => '',
        'esisno' => '',
        'officeregistrationno' => '',
        'exceptiontax' => '',
        'otherexceptiontax' => '',
        'bank_benificialname' => '',
        'bank_benificialaccno' => '',
        'bank_benificialbankname' => '',
        'bank_benificialbranchname' => '',
        'bank_benificialaddress' => '',
        'bank_benificialifsc' => '',
        'bank_benificialswiftcode' => '',
        'bank_benificialibanno' => '',
        'intermidiatebankname' => '',
        'intermidiatebankaddress' => '',

        'intermidiatebankswiftcode' => '',
        'bank_ecs' => '',
        'copypancart' => '',
        'copyservicetax' => '',
        'copytan' => '',
        'copyaddressproof' => '',
        'emailstatus' => '',
        'emailcurrency' => '',
        'emailcountry' => '',
        'username' =>'',
        'password' => '',

        'countryid' => '',
        'stateid' => '',
        'cityid' => '',
        'branch_id' => '',
        'locationid' => '',
        'bank_creditamount' => '',
        'bank_agentcommision' => '',
        'aadhaar_number' =>'',
        'ppid' =>'',
        'pppermission' =>'',
    );
    //pend($parameter);
    $response = $this->supper_admin->call_procedure('proc_partnerregister_v', $parameter);


}
    public function partnerusercreate(){
        if($this->input->post('submit')=='Register'){
            $siteurl= base_url();
            $parameterbranch=array(
                'act_mode' =>'selectbranch',
                'weburl' =>$siteurl,
                'type'=>'web',

            );

            $path=api_url().'selectsiteurl/branch/format/json/';
            $data['branch']=curlpost($parameterbranch,$path);
            $parameterregsel=array(
                'act_mode' =>'selectpartneremail',
                'title' =>'',
                'firstname' =>'',
                'lastname' =>'',
                'row_id' =>'',
                'logintype' =>'',
                'agencyname' =>'',
                'office_address' =>'',
                'city' =>'',
                'state' =>'',
                'pincode' =>'',
                'telephone' =>'',
                'mobile' =>'',
                'fax' =>'',
                'website' =>'',
                'email' =>'',
                'primarycontact_name' =>'',
                'primarycontact_email_mobile' =>'',
                'primarycontact_telephone' =>'',
                'secondrycontact_name' =>'',
                'secondrycontact_email_phone' =>'',
                'secondrycontact_telephone' =>'',
                'management_executives_name' =>'',
                'management_executives_email_phone' =>'',
                'management_executives_telephone' =>'',
                'branches' =>'',

                'yearofestablism' =>'',
                'organisationtype' =>'',
                'lstno' =>'',
                'cstno' =>'',
                'registrationno' =>'',
                'vatno' =>'',
                'panno' =>'',
                'servicetaxno' =>'',
                'tanno' =>'',
                'pfno' =>'',
                'esisno' =>'',
                'officeregistrationno' =>'',
                'exceptiontax' =>'',
                'otherexceptiontax' =>'',
                'bank_benificialname' =>'',
                'bank_benificialaccno' =>'',
                'bank_benificialbankname' =>'',
                'bank_benificialbranchname' =>'',
                'bank_benificialaddress' =>'',
                'bank_benificialifsc' =>'',
                'bank_benificialswiftcode' =>'',
                'bank_benificialibanno' =>'',
                'intermidiatebankname' =>'',
                'intermidiatebankaddress' =>'',

                'intermidiatebankswiftcode' =>'',
                'bank_ecs' =>'',
                'copypancart' => '',
                'copyservicetax' =>'',
                'copytan' =>'',
                'copyaddressproof' =>'',
                'emailstatus' =>'',
                'emailcurrency' =>'',
                'emailcountry' =>'',
                'username' =>$this->input->post('email'),
                'password' =>'',
                'countryid' => $data['branch']->countryid,
                'stateid' => $data['branch']->stateid,
                'cityid' => $data['branch']->cityid,
                'branch_id' => $data['branch']->branch_id,
                'locationid' => $data['branch']->branch_location,
                'bank_creditamount' =>'',
                'bank_agentcommision' =>'',
                'aadhaar_number' =>'',
                'ppid' =>$this->input->post('ppid'),
                'pppermission' =>$this->input->post('pppermission'),
                'type'=>'web',
            );

            $pathsel=api_url().'partnerapi/partnerregister/format/json/';
            $data['regsel']=curlpost($parameterregsel,$pathsel);


            if($data['regsel']->scalar!='Something Went Wrong')
            {
                $data['mesg']="Email Id Already Register";
            }
            else {


                $siteurl= base_url();
                $parameterbranch=array(
                    'act_mode' =>'selectbranch',
                    'weburl' =>$siteurl,
                    'type'=>'web',

                );

                $path=api_url().'selectsiteurl/branch/format/json/';
                $data['branch']=curlpost($parameterbranch,$path);



                //select banner images
                $parameterbanner = array(
                    'act_mode' => 'selectbannerimages',
                    'branchid' => $data['branch']->branch_id,
                    'type' => 'web',

                );

                $path = api_url() . 'selectsiteurl/banner/format/json/';
                $data['banner'] = curlpost($parameterbanner, $path);





                $message_new = '<table width="90%" style="line-height: 28px; font-family: sans-serif;"   >
<tr><td>Congratulations ' . $this->input->post("title").". " . ucfirst($this->input->post("first_name")).'&nbsp;'. $this->input->post("last_name") . ',</td></tr>

<tr><td>You have been successfully registered with  ' . $data['banner']->bannerimage_top3 . '.!</td></tr>

<tr><td>
You have been successfully registered with ' . $data['banner']->bannerimage_top3 . '. You can use ' . $this->input->post("email") . ' as your Login ID to access your ' . $data['banner']->bannerimage_top3 . ' Account online. 
</td></tr>
<tr><td>Please do not disclose/share your password to anyone.</td></tr>

<tr><td>Should you have any queries, please feel free to write to us on '.$data['banner']->bannerimage_branch_email.' or call us on '.$data['banner']->bannerimage_branch_contact.'.</td></tr>
<tr><td>Yours sincerely,<br>
'.$data['banner']->bannerimage_top3.' Team</td></tr>
</table>
';


                $from_email = $data['banner']->bannerimage_from ;
                $to_email = $this->input->post('email');
                //Load email library
                $this->load->library('email');
                $this->email->from($from_email,  $data['banner']->bannerimage_top3 );
                $this->email->reply_to($from_email,  $data['banner']->bannerimage_top3 );
                $this->email->to($to_email);
                $this->email->subject('' . $data['banner']->bannerimage_top3 . ' - Your Registration Details');
                $this->email->message($message_new);
                //Send mail
                $this->email->send();
                //  pend($this->email->print_debugger());


                $parameterreg = array(
                    'act_mode' => 'insertsubpartner',
                    'title' =>$this->input->post('title'),
                    'firstname' =>$this->input->post('first_name'),
                    'lastname' =>$this->input->post('last_name'),
                    'row_id' =>'',
                    'logintype' => '',
                    'agencyname' =>'',
                    'office_address' => '',
                    'city' => '',
                    'state' => '',
                    'pincode' => '',
                    'telephone' => '',
                    'mobile' => $this->input->post('mobile_number'),
                    'fax' => '',
                    'website' => '',
                    'email' => $this->input->post('email'),
                    'primarycontact_name' => '',
                    'primarycontact_email_mobile' => '',
                    'primarycontact_telephone' => '',
                    'secondrycontact_name' => '',
                    'secondrycontact_email_phone' => '',
                    'secondrycontact_telephone' => '',
                    'management_executives_name' => '',
                    'management_executives_email_phone' => '',
                    'management_executives_telephone' => '',
                    'branches' =>'',

                    'yearofestablism' =>'',
                    'organisationtype' =>'',
                    'lstno' => '',
                    'cstno' => '',
                    'registrationno' => '',
                    'vatno' =>'',
                    'panno' => '',
                    'servicetaxno' => '',
                    'tanno' => '',
                    'pfno' => '',
                    'esisno' => '',
                    'officeregistrationno' => '',
                    'exceptiontax' => '',
                    'otherexceptiontax' => '',
                    'bank_benificialname' => '',
                    'bank_benificialaccno' => '',
                    'bank_benificialbankname' => '',
                    'bank_benificialbranchname' => '',
                    'bank_benificialaddress' => '',
                    'bank_benificialifsc' => '',
                    'bank_benificialswiftcode' => '',
                    'bank_benificialibanno' => '',
                    'intermidiatebankname' => '',
                    'intermidiatebankaddress' => '',
                    'intermidiatebankswiftcode' => '',
                    'bank_ecs' => '',
                    'copypancart' =>'',
                    'copyservicetax' => '',
                    'copytan' => '',
                    'copyaddressproof' => '',
                    'emailstatus' => '',
                    'emailcurrency' => '',
                    'emailcountry' => '',
                    'username' => $this->input->post('email'),
                    'password' => base64_encode($this->input->post('password')),

                    'countryid' => $data['branch']->countryid,
                    'stateid' => $data['branch']->stateid,
                    'cityid' => $data['branch']->cityid,
                    'branch_id' => $data['branch']->branch_id,
                    'locationid' => $data['branch']->branch_location,
                    'bank_creditamount' =>'',
                    'bank_agentcommision' =>'',

                    'aadhaar_number' =>$this->input->post('aadhaar_number'),
                    'ppid' =>$this->session->userdata('ppid'),
                    'pppermission' =>$this->input->post('pppermission'),
                    'type' => 'web',
                );

                $path = api_url() . 'partnerapi/partnerregister/format/json/';
                $data['reg'] = curlpost($parameterreg, $path);


              $data['message'] ="Agent Added Sucessfully";
            }}
        $this->load->view("agent/helper/header",$data);
        $this->load->view("agent/helper/leftbar",$data);
        $this->load->view("agent/partnerusercreate",$data);
        $this->load->view("agent/helper/footer");

    }
    public function mywallet(){

        $edid=$this->session->userdata['ppid'];
       // pend($edid);

        $orderwalletselect = array('act_mode' => 'orderwalletselectagentamount',
            'orderid' => '',
            'tracking_id' => '',
            'order_status' => '',
            'status_message' => '',
            'paymentmode' => '',
            'paymentstatus' => '',
            'ordersucesmail' =>'',
            'ordermailstatus' => '',
            'agent_id' => $this->session->userdata['ppid'],
            'cred_debet' =>'',
            'amount' => '',
            'bankagentcommision' => '',
            'type' => 'web',

        );

        $path = api_url() . "Ordersucess/selectwalletdatadispall/format/json/";
        $data['orderwalletselect'] = curlpost($orderwalletselect, $path);

        $parameter = array('act_mode' => 'agentamount_new',
            'Param1' => $edid,
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');

        $data['agent_his'] = (array)$this->supper_admin->call_procedure('proc_agent_s', $parameter);
       // pend( $response['agent_his']);
        $parameter = array('act_mode' => 'agent_last_amount',
            'Param1' => $edid,
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');

        $data['agent_left_amt'] = (array)$this->supper_admin->call_procedureRow('proc_agent_s', $parameter);

        //pend($data['agent_last_amt']);



        $this->load->view("agent/helper/header",$data);
        $this->load->view("agent/helper/leftbar",$data);
        $this->load->view("agent/mywallet",$data);
        $this->load->view("agent/helper/footer");
    }




  public function dashboardlist(){ 
   if($this->input->post('search') == 'Search'){
         $paramater = array(
            'act_mode'=>'count_order_by_month_date',
            'Param1'=>date('Y-m-d',strtotime($this->input->post('datepicker1'))),
            'Param2'=>date('Y-m-d',strtotime($this->input->post('datepicker2'))),
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>''
        );

        $data['transamount'] = $this->supper_admin->call_procedure('proc_order_s',$paramater);
       //p($response['dataselect']);exit;
       }else{
     $param=array(
            'act_mode'=>'Total_order_agent',
            'Param1'=>$this->session->userdata['ppid'],
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>''
            );
        $data['orderdata']= $this->supper_admin->call_procedureRow('proc_order_s',$param);

        $param=array(
            'act_mode'=>'last_transaction_amount',
            'Param1'=>$this->session->userdata['ppid'],
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>''
            );
        $data['lastamount']= $this->supper_admin->call_procedureRow('proc_order_s',$param);

         $param=array(
            'act_mode'=>'getmonthtransationorder',
            'Param1'=>$this->session->userdata['ppid'],
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>''
            );
        $data['transamount']= $this->supper_admin->call_procedure('proc_order_s',$param);

         $param=array(
            'act_mode'=>'getmonthtransationordercount',
            'Param1'=>$this->session->userdata['ppid'],
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>''
            );
        $data['transamountcount']= $this->supper_admin->call_procedure('proc_order_s',$param);

         $param=array(
            'act_mode'=>'usertotalamount',
            'Param1'=>$this->session->userdata['ppid'],
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>''
            );
        $data['totaluseramount']= $this->supper_admin->call_procedureRow('proc_order_s',$param);

             $param=array(
            'act_mode'=>'lastcreditamount',
            'Param1'=>$this->session->userdata['ppid'],
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>''
            );
        $data['creditamount']= $this->supper_admin->call_procedureRow('proc_order_s',$param);
        }
        $this->load->view("agent/helper/header",$data);
        $this->load->view("agent/helper/leftbar",$data);
        $this->load->view("agent/partnerdashboards",$data);
        $this->load->view("agent/helper/footer");
  }
  
    //Promocode Process
	// check & validate promocode here

    public function checkpromocode(){


        $promocode = $this->input->post('promocode');
        $txtDepartDate =$this->session->userdata['txtDepartDate'];
        $uid = $this->input->post('uid');
        $totalhourmint=   getTimeSlotInArray($this->session->userdata['destinationType']);
        $txfromhrs = $totalhourmint[0];
        $txtfrommin =  $totalhourmint[1];
        $txttohrs =  $totalhourmint[2];
        $txttomin = $totalhourmint[3];
        $txtfromd = $totalhourmint[4];
        $txttod = $this->input->post('txttod');
        $type = $this->input->post('type');
        $totalprice = $this->session->userdata['final_cost_package_addon_handling'];


        $cartaddoneiddata = $this->input->post('cartaddoneiddata');
        $checkpromo = array('act_mode'=>'checkpromocode',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>$promocode,
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'',
            'Param10'=>'',
            'Param11'=>'',
            'Param12'=>'',
        );
        $checkPromocode = $this->supper_admin->call_procedurerow('proc_promocode_s', $checkpromo);
        if($checkPromocode->cnt>0){
            // Check Start date and end date exits promocode
            $checkpromodate = array('act_mode'=>'datevalidation',
                'Param1'=>'',
                'Param2'=>'',
                'Param3'=>date('Y-m-d',strtotime($txtDepartDate)),
                'Param4'=>'',
                'Param5'=>'',
                'Param6'=>'',
                'Param7'=>'',
                'Param8'=>'',
                'Param9'=>'',
                'Param10'=>'',
                'Param11'=>'',
                'Param12'=>'',
            );
            $datepromocode = $this->supper_admin->call_procedurerow('proc_promocode_s', $checkpromodate);
            if($datepromocode->pcnt>0){
                //Check user type exit;
                $parameter1 = array('act_mode'=>'get_user_detail_agent',
                    'Param1'=>'',
                    'Param2'=>$uid,
                    'Param3'=>'',
                    'Param4'=>'',
                    'Param5'=>'',
                    'Param6'=>'',
                    'Param7'=>'',
                    'Param8'=>'',
                    'Param9'=>'',
                    'Param10'=>'',
                    'Param11'=>'',
                    'Param12'=>'',
                );

                $getudetail = $this->supper_admin->call_procedurerow('proc_promocode_s', $parameter1);
                $promocode1 = array('act_mode'=>'checkpromode_user_type',
                    'Param1'=>'',
                    'Param2'=>'',
                    'Param3'=>$promocode,
                    'Param4'=>'',
                    'Param5'=>'',
                    'Param6'=>'',
                    'Param7'=>'',
                    'Param8'=>'',
                    'Param9'=>'',
                    'Param10'=>'',
                    'Param11'=>'',
                    'Param12'=>'',
                );
                $promocodedetail = $this->supper_admin->call_procedurerow('proc_promocode_s', $promocode1);
                //Check Promo User Type Exits  
              /*  p($getudetail->user_usertype);  p($promocodedetail->p_usertype);exit;*/
                $checkusertype = $this->checkusertype('Agent',$promocodedetail->p_usertype);
                if($checkusertype==1){
                    //Check Promo date time exits
                    $promocodedate = array('act_mode'=>'checkpromode_date_time',
                        'Param1'=>  '',
                        'Param2'=>  '',
                        'Param3'=>  date('Y-m-d',strtotime($txtDepartDate)),
                        'Param4'=>  '',
                        'Param5'=>  '',
                        'Param6'=>  $promocodedetail->promo_id,
                        'Param7'=>  $txfromhrs,//"4"
                        'Param8'=>  $txtfrommin,//"04"
                        'Param9'=>  $txttohrs,//"8"
                        'Param10'=> $txttomin,//"08"
                        'Param11'=> '',
                        'Param12'=> '',
                    );
                    $promocodedatetime = $this->supper_admin->call_procedurerow('proc_promocode_s', $promocodedate);
                    if(!empty($promocodedatetime->promopackagedailyinventory_id)){
                        //Check promocode count by based inventry id
                        $promocode1 = array('act_mode'=>'checkpromocodecount_inventery',
                            'Param1'=>'',
                            'Param2'=>'',
                            'Param3'=>$promocodedetail->promo_id,
                            'Param4'=>'',
                            'Param5'=>$promocodedatetime->promopackagedailyinventory_id,
                            'Param6'=>'',
                            'Param7'=>'',
                            'Param8'=>'',
                            'Param9'=>'',
                            'Param10'=>'',
                            'Param11'=>'',
                            'Param12'=>'',
                        );
                        $getcountpromocode = $this->supper_admin->call_procedurerow('proc_promocode_s', $promocode1);
                        if(($promocodedatetime->promopackage_timesallowed>$getcountpromocode->cntpromo) || ($promocodedatetime->promopackage_timesallowed==0)){
                            //Check user promocode count by based inventry id
                            $promocodeuser = array('act_mode'=>'checkpromocodecount_user_inventery',
                                'Param1'=>'',
                                'Param2'=>'',
                                'Param3'=>$promocodedetail->promo_id,
                                'Param4'=>$uid,
                                'Param5'=>$promocodedatetime->promopackagedailyinventory_id,
                                'Param6'=>'',
                                'Param7'=>'',
                                'Param8'=>'',
                                'Param9'=>'',
                                'Param10'=>'',
                                'Param11'=>'',
                                'Param12'=>'',
                            );
                            $getcountpromocodeuser = $this->supper_admin->call_procedurerow('proc_promocode_s', $promocodeuser);
                            if(($promocodedatetime->promopackage_peruserallowed>$getcountpromocodeuser->cntpromouid) || ($promocodedatetime->promopackage_peruserallowed==0)){
                                //echo 'dcsdcs';
                                $getflatprice = $this->getpromocodepriceflat($totalprice,$promocodedetail->p_type,$promocodedetail->p_discount);

                                $getpromocodeprice = $this->getpromocodepricecart($getflatprice,$cartaddoneiddata,$promocodedetail->p_addonedata);

                                /* $promocodeuser = array('act_mode'=>'getaddonlist',
                                          'Param1'=>'',
                                          'Param2'=>'',
                                          'Param3'=>$promocodedetail->p_addonedata,
                                          'Param4'=>'',
                                          'Param5'=>'',
                                          'Param6'=>'',
                                          'Param7'=>'',
                                          'Param8'=>'',
                                          'Param9'=>'',
                                          'Param10'=>'',
                                          'Param11'=>'',
                                          'Param12'=>'',
                                       );
                               $getcountpromocodeuser = $this->supper_admin->call_procedure('proc_promocode_s', $promocodeuser);
                                 foreach($getcountpromocodeuser as $pvalue){
                                  $dataList[] = $pvalue->addon_name;
                                 }

                                  $addon = implode(',',$dataList);
                                  $discountype = $promocodedetail->p_type;
                                  $dicountvalue = $promocodedetail->p_discount;
                                  $getpromocodeprice = $getpromocodeprice;
                                  //echo '<p>Free: - </p>(<span>'.$addon.'</span>),'.$discountype.'-'.$dicountvalue;
                                 echo $getpromocodeprice;

                                 }*/

                                // echo $getpromocodeprice;
                                //saving price
                                $savingpromoprice= $totalprice-$getpromocodeprice;
                                //Total Price
                                $data = array(
                                    'total_promo_price' => $getpromocodeprice,
                                    'promo_id' =>$promocodedetail->promo_id,
                                    'saving_price_promocode' =>$savingpromoprice,
                                    'promocode_name' =>$promocodedetail->p_codename
                                );
                                //p($data);
                                $this->session->set_userdata($data);
                                echo 1;
                            }else{
                                echo 0;
                            }
                        }else
                            echo 0;
                    }else{
                        echo 0;
                    }
                }else{
                    echo 0;
                } }else{
                echo 0;
            } }else{
            echo 0;
        }
    }




    public function getpromocodepricecart($flatprice,$cardid,$addonpromid){

        //Get addon price
        $getcardexpprice =explode(',', $cardid);
        $getpromoexpprice =explode(',', $addonpromid);
        foreach($getpromoexpprice as $value){
            if(in_array($value, $getcardexpprice)){
                $promocodeuser = array('act_mode'=>'getaddonlist',
                    'Param1'=>'',
                    'Param2'=>'',
                    'Param3'=>$value,
                    'Param4'=>'',
                    'Param5'=>'',
                    'Param6'=>'',
                    'Param7'=>'',
                    'Param8'=>'',
                    'Param9'=>'',
                    'Param10'=>'',
                    'Param11'=>'',
                    'Param12'=>'',
                );
                $getcountpromocodeuser = $this->supper_admin->call_procedurerow('proc_promocode_s', $promocodeuser);
                $getaddonprices += $getcountpromocodeuser->addon_price;
            }
        }
        $getaddonprice  = ($getaddonprices) ? $getaddonprices : 0;
        $finalprice = $flatprice-$getaddonprices;
        return $finalprice;
    }

    public function checkusertype($usertype,$promotype){
        if($promotype ==3 && $usertype =='Agent'){
            return 1;
        }

    }

    public function getpromocodepriceflat($price,$discountype,$discount_param){
        $getpromoprice='';
        $agent_discount_value = trim( $discount_param );
        if( $discountype == 'flat' ){
            $getpromoprice = ( $price - $agent_discount_value );
            return  $getpromoprice;
        }

        if($discountype =='percent'){

            $getpromoprice = ($price * ($agent_discount_value / 100));
            $getpromoprice = ($price - $getpromoprice);
            return $getpromoprice;
        }
    }

  
 

}//end of class
?>
