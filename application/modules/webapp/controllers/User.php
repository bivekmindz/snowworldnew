<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User extends MX_Controller
{

    public function __construct()
    {
        $this->load->model("supper_admin");
        $this->load->library('session');
         session_start();
        
    }

//userregister Sucess
    public function userregister()
    {
        $parameter=array(
            'act_mode'=>'ViewCountrys',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'',
            'Param10'=>'',
            'Param11'=>'',
            'Param12'=>'',
            'Param13'=>'',
            'Param14'=>'',
            'Param15'=>'',
        );


        $data['Countrys'] = $this->supper_admin->call_procedure('proc_timeslotspackages',$parameter);
        if ($this->session->userdata['skiindia'] > 0) {
            redirect(base_url());
        } else {
            $this->load->library('session');
            $parameterseo = array('act_mode' => 'viewseotags', 'rowid' => 14);
            $data['seotags'] = $this->supper_admin->call_procedureRow('proc_siteconfig', $parameterseo);

            if (($_POST['submit'] == 'Register')) {
               /* $this->form_validation->set_rules('title', 'Select Title ', 'required');
                $this->form_validation->set_rules('first_name', 'Enter First Name', 'required');
                $this->form_validation->set_rules('last_name', 'Enter Last Name ', 'required');
                $this->form_validation->set_rules('mobile_number', 'Enter Mobile No', 'required');
                $this->form_validation->set_rules('email', 'Enter Email id ', 'required');
                $this->form_validation->set_rules('password', 'Enter Password', 'required');*/
                // $this->form_validation->set_rules('cpassword', 'Enter Confirm password ', 'required');
                $this->form_validation->set_rules('cpassword', 'Confirm Password', 'required|matches[password]');

              //  pend("Hiii");
                if ($this->form_validation->run() != FALSE) {

//check user
                    $parameter9 = array(
                        'act_mode' => 'checkuser',
                        'email' => $this->input->post('email'),
                        'type' => 'web',
                    );

                    $path = api_url() . 'userapi/usercheck/format/json/';
                    $data['checkuser'] = curlpost($parameter9, $path);


                    if ($data['checkuser']->scalar == '0') {



                        //Select branch
                        $siteurl= base_url();
                        $parameterbranch=array(
                            'act_mode' =>'selectbranch',
                            'weburl' =>$siteurl,
                            'type'=>'web',

                        );

                        $path=api_url().'selectsiteurl/branch/format/json/';
                        $data['branch']=curlpost($parameterbranch,$path);



                        //select banner images
                        $parameterbanner = array(
                            'act_mode' => 'selectbannerimages',
                            'branchid' => $data['branch']->branch_id,
                            'type' => 'web',

                        );

                        $path = api_url() . 'selectsiteurl/banner/format/json/';
                        $data['banner'] = curlpost($parameterbanner, $path);


                          $parametersms = array(
                            'act_mode' => 'selectsms',
                            'branchid' => $data['branch']->branch_id,
                            'type' => 'web',

                        );

                        $path = api_url() . 'selectsiteurl/banner/format/json/';
                        $data['smsgatway'] = curlpost($parametersms, $path);






                        $message_new = '<table width="90%" style="line-height: 28px; font-family: sans-serif;"   >
<tr><td>Congratulations ' . $this->input->post("title").". " . ucfirst($this->input->post("first_name")).'&nbsp;'. $this->input->post("last_name") . ',</td></tr>

<tr><td>You have been successfully registered with  ' . $data['banner']->bannerimage_top3 . '.!</td></tr>

<tr><td>
You have been successfully registered with ' . $data['banner']->bannerimage_top3 . '. You can use ' . $this->input->post("email") . ' as your Login ID to access your ' . $data['banner']->bannerimage_top3 . ' Account online. 
</td></tr>
<tr><td>Please do not disclose/share your password to anyone.</td></tr>

<tr><td>Should you have any queries, please feel free to write to us on '.$data['banner']->bannerimage_branch_email.' or call us on '.$data['banner']->bannerimage_branch_contact.'.</td></tr>
<tr><td>Yours sincerely,<br>
'.$data['banner']->bannerimage_top3.' Team</td></tr>
</table>
';


                        $from_email = $data['banner']->bannerimage_from ;
                        $to_email = $this->input->post('email');
                        //Load email library
                        $this->load->library('email');
                        $this->email->from($from_email,  $data['banner']->bannerimage_top3 );
                        $this->email->reply_to($from_email,  $data['banner']->bannerimage_top3 );
                        $this->email->to($to_email);
                        $this->email->subject('' . $data['banner']->bannerimage_top3 . ' - Your Registration Details');
                        $this->email->message($message_new);
                        //Send mail
                      //  $this->email->send();
                        //pend($this->email->print_debugger());






$from =  $data['banner']->bannerimage_from ;
$fromname = $data['banner']->bannerimage_top3;
$to = $this->input->post('email');
$api_key = $data['banner']->bannerimage_apikey;
$subject = ('' . $data['banner']->bannerimage_top3 . ' - Your Registration Details');
 

$content =$message_new;

$mail1=array();
$mail1['subject']= ($subject);                                                                       
$mail1['fromname']= ($fromname);                                                             
$mail1['api_key'] = $api_key;
$mail1['from'] = $from;
$mail1['content']= ($message_new);
$mail1['recipients']= $to;
$apiresult = callApi(@$api_type,@$action,$mail1);

//echo trim($apiresult);


                        //Register
                        $parameter6=array(
                            'act_mode'=>'memregister',
                            'Param1'=>$this->input->post('regtitle'),
                            'Param2'=>$this->input->post('fname'),
                            'Param3'=>$this->input->post('lastName'),
                            'Param4'=>$this->input->post('mobileno'),
                            'Param5'=>$this->input->post('email'),
                            'Param6'=>base64_encode($this->input->post('password')),
                            'Param7'=>$this->input->post('reg_address'),
                            'Param8'=>$this->input->post('reg_city'),
                            'Param9'=>$this->input->post('reg_state'),
                            'Param10'=>$this->input->post('reg_zip'),
                            'Param11'=>$this->input->post('reg_country'),
                            'Param12'=>'',
                            'Param13'=>'',
                            'Param14'=>'',
                            'Param15'=>'',
                        );


                        $data['register'] = $this->supper_admin->call_procedure('proc_memberregister_v',$parameter6);

                        $arr = (array)$data['register'];
                        // Userlog
                        $parameter4 = array(
                            'act_mode' => 'memuserlog',
                            'user_id' => $arr[0]->ls,
                            'type' => 'web',
                        );

                        $path = api_url() . 'userapi/userlog/format/json/';
                        $data['userlog'] = curlpost($parameter4, $path);


                        $data = array(
                            'skiindia' => $arr[0]->ls,
                        );

                        $this->session->set_userdata($data);

                        $data['skiindases'] = ($arr[0]->ls);


                        /*sms api*/
                        $data = urlencode("Thank you for registering with us. You can use your email-id ".$this->input->post('email')." for managing your ".$data['banner']->bannerimage_top3." account.");
                        $sms_url = "http://bulkpush.mytoday.com/BulkSms/SingleMsgApi?feedid=".$data['smsgatway']->sms_feedid."&username=".$data['smsgatway']->sms_username."&password=".$data['smsgatway']->sms_password."&To=".$this->input->post('mobile_number')."&Text=".$data;
                        $sms = file_get_contents($sms_url);
                        //die();
                        /*sms apcvbi*/


                        redirect(base_url() . "myaccount");

                    } else {

                        $data['emsg'] = "Email Id Already Registered";
                        $this->session->set_flashdata('message', 'Email Id Already Registered');


                    }

                }


            }
            //Select branch
            $siteurl= base_url();
            $parameterbranch=array(
                'act_mode' =>'selectbranch',
                'weburl' =>$siteurl,
                'type'=>'web',

            );

            $path=api_url().'selectsiteurl/branch/format/json/';
            $data['branch']=curlpost($parameterbranch,$path);


            $this->load->view("helper/header",$data);
            $this->load->view("helper/topbar",$data);
            $this->load->view("userregister", $data);
            $this->load->view("helper/footer");


        }
    }


//userlogin  
    public function userlogin()
    {

        if ($this->session->userdata['skiindia'] > 0) {
            redirect(base_url());
        } else {
            $parameterseo = array('act_mode' => 'viewseotags', 'rowid' => 14);
            $data['seotags'] = $this->supper_admin->call_procedureRow('proc_siteconfig', $parameterseo);


            if (($this->input->post('submit') == 'Login')) {
                $this->form_validation->set_rules('emailmob', 'Enter Email id ', 'required');
                $this->form_validation->set_rules('pwd', 'Enter Password', 'required');

                if ($this->form_validation->run() != FALSE) {
                    //Login
                    $parameter3 = array(
                        'act_mode' => 'memlogin',
                        'username' => $this->input->post('emailmob'),
                        'password' => $this->input->post('pwd'),
                        'type' => 'web',

                    );

                    $path = api_url() . 'userapi/userlogin/format/json/';
                    $data['login'] = curlpost($parameter3, $path);

                    if ($data['login']->scalar == 'Something Went Wrong') {
                        $data['emsg'] = "Invalid Login";
                        $this->session->set_flashdata('message', 'Invalid Login');
                    } else {
// Userlog
                        $parameter4 = array(
                            'act_mode' => 'memuserlog',
                            'user_id' => $data['login']->user_id,
                            'type' => 'web',
                        );

                        $path = api_url() . 'userapi/userlog/format/json/';
                        $data['userlog'] = curlpost($parameter4, $path);


                        $data = array(
                            'skiindia' => $data['login']->user_id,
                        );

                        $this->session->set_userdata($data);

                        $data['skiindases'] = ($data['login']->user_id);

                        header("location:" . base_url() . "myaccount");
                    }
                }

            }
            //Select branch
            $siteurl= base_url();
            $parameterbranch=array(
                'act_mode' =>'selectbranch',
                'weburl' =>$siteurl,
                'type'=>'web',

            );

            $path=api_url().'selectsiteurl/branch/format/json/';
            $data['branch']=curlpost($parameterbranch,$path);


            $this->load->view("helper/header",$data);
            $this->load->view("helper/topbar",$data);
            $this->load->view("userlogin", $data);
            $this->load->view("helper/footer");


        }
    }


    //emailverified
    public function emailverified()
    {
        if ($this->session->userdata('skiindiamember')->id) {
            redirect(base_url() . "home");
        } else {
            $parameterseo = array('act_mode' => 'viewseotags', 'rowid' => 5);
            $data['seotags'] = $this->supper_admin->call_procedureRow('proc_siteconfig', $parameterseo);


            $parameter = array(
                'act_mode' => 'emailverified',
                'email' => $this->input->get('emailid'),

                'type' => 'web',

            );

            $path = api_url() . 'userapi/useremailverified/format/json/';
            $data['emver'] = curlpost($parameter, $path);
            $arr = (array)$data['emver'];

            if ($data['emver']->scalar != 'Something Went Wrong') {

                $parameter = array(
                    'act_mode' => 'emailverifiedupdate',
                    'email' => $this->input->get('emailid'),
                    'type' => 'web',

                );

                $path = api_url() . 'userapi/useremailverifiedupdate/format/json/';
                $data['emverupdate'] = curlpost($parameter, $path);
                header("Location:emailverifiedsucess");
            } else {
                redirect(base_url());
            }


            //$this->load->view("landingpage",$data);
            //    $this->load->view("helper/footer");
        }
    }

//emailverified Sucess
    public function emailverifiedsucess()
    {
        if ($this->session->userdata('skiindiamember')->id) {
            redirect(base_url() . "home");
        } else {
            $parameterseo = array('act_mode' => 'viewseotags', 'rowid' => 14);
            $data['seotags'] = $this->supper_admin->call_procedureRow('proc_siteconfig', $parameterseo);


            $this->load->view("helper/header");
            $this->load->view("emailverified", $data);
            $this->load->view("helper/footer");


        }
    }

//forgetpassword Sucess
    public function forgetpassword()
    {
        if ($this->session->userdata('skiindiamember')->id) {
            redirect(base_url() . "home");
        } else {
            $parameterseo = array('act_mode' => 'viewseotags', 'rowid' => 14);
            $data['seotags'] = $this->supper_admin->call_procedureRow('proc_siteconfig', $parameterseo);

            if ($this->input->post('submit') == 'Forget Password') {

                $new_pass = base64_encode(rand(000000,999999));


                $parameterupdatepassword_newpass = array(
                    'act_mode' => 'forgetpassword_newpass',
                    'email' => $this->input->post('emailmob'),
                    'Param3' => $new_pass,
                    'type' => 'web'
                );
//p($parameterupdatepassword_newpass);
                $path_newpass = api_url() . 'userapi/updatepassword/format/json/';
                $data['updateacc_new'] = curlpost($parameterupdatepassword_newpass, $path_newpass);
                //pend($data['updateacc_new']);

                $parameterupdatepassword = array(
                    'act_mode' => 'forgetpassword',
                    'email' => $this->input->post('emailmob'),
                    'type' => 'web'
                );

                $path = api_url() . 'userapi/updatepassword/format/json/';
                $data['updateacc'] = curlpost($parameterupdatepassword, $path);


                if (($data['updateacc']->scalar) != 'Something Went Wrong') {


                    /*send sms api start*/
                    /* $sms_url = "http://bulkpush.mytoday.com/BulkSms/SingleMsgApi?feedid=356637&username=9867622549&password=mtpjp&To=".$data['updateacc']->user_mobileno."&Text='Your password reset request has been processed\. Your temporary password is .".$passworddta.". Please login and change your password as soon as possible.";
                     $sms = file_get_contents($sms_url, null);*/
                    //pend($sms);
                    /*send sms api finish*/


                    //pend($data['updateacc']);


                    //Select branch
                    $siteurl= base_url();
                    $parameterbranch=array(
                        'act_mode' =>'selectbranch',
                        'weburl' =>$siteurl,
                        'type'=>'web',

                    );

                    $path=api_url().'selectsiteurl/branch/format/json/';
                    $data['branch']=curlpost($parameterbranch,$path);



                    //select banner images
                    $parameterbanner = array(
                        'act_mode' => 'selectbannerimages',
                        'branchid' => $data['branch']->branch_id,
                        'type' => 'web',

                    );

                    $path = api_url() . 'selectsiteurl/banner/format/json/';
                    $data['banner'] = curlpost($parameterbanner, $path);

//pend($data['banner']);


                    $message_new = '<table width="90%" style="line-height: 28px; font-family: sans-serif;"   >
<tr><td>Congratulations ' . $data['updateacc']->user_title.". " .$data['updateacc']->user_firstname.'&nbsp;'.$data['updateacc']->user_lastname . ',</td></tr>
<tr><td>Greetings from ' . $data['banner']->bannerimage_top3 . '..!</td></tr>
<tr><td>You have received this communication in response to your request for your ' . $data['banner']->bannerimage_top3 . '. Account password to be sent to you via e-mail and sms.</td></tr>
<tr><td>Your temporary password is: '.base64_decode($data['updateacc']->user_password).' </td></tr>
<tr><td>Please use the password exactly as it appears above.</td></tr>
<tr><td>We request you immediately change your password by logging on to '.base_url().'</td></tr>
<tr><td>Should you have any queries, please feel free to write to us on '.$data['banner']->bannerimage_branch_email.' or call us on '.$data['banner']->bannerimage_branch_contact.'.</td></tr>
<tr><td>Yours sincerely,<br>
'.$data['banner']->bannerimage_top3.' Team</td></tr>
</table>
';


                    $from_email = $data['banner']->bannerimage_from ;
                    $to_email = $this->input->post('email');
                    //Load email library
                    $this->load->library('email');
                    $this->email->from($from_email,  $data['banner']->bannerimage_top3 );
                    $this->email->reply_to($from_email,  $data['banner']->bannerimage_top3 );
                    $this->email->to($data['updateacc']->user_emailid);
                    $this->email->subject('' . $data['banner']->bannerimage_top3 . ' - Your Request For Password Reset');
                    $this->email->message($message_new);
                    //Send mail
                    $this->email->send();
                    //pend($this->email->print_debugger());

                    $data['updateacc']->user_password. "<br>";


                    $var_sms = base64_decode($data['updateacc']->user_password);

                    //pend($data['updateacc']);
                    //$var_sms = "hhasdasdasdasdh";
                    /*sms forget*/
                    $mde=urlencode('Your password reset request has been processed. Your temporary password is '.$var_sms.'. Please login and change your password as soon as possible.');

                    $sms_url = "http://bulkpush.mytoday.com/BulkSms/SingleMsgApi?feedid=356637&username=9867622549&password=mtpjp&To=".$data['updateacc']->user_mobileno."&Text=".$mde;



                    $sms = file_get_contents($sms_url);
                    $data['message'] = "Password Send To Email Id";
                } else {

                    $data['message'] = 'You Are Not Registered User';
                }

            }

//Select branch
            $siteurl= base_url();
            $parameterbooking=array(
                'act_mode' =>'selectbranch',
                'weburl' =>$siteurl,
                'type'=>'web',

            );

            $path=api_url().'selectsiteurl/branch/format/json/';
            $data['branch']=curlpost($parameterbooking,$path);


            $this->load->view("helper/header",$data);
            $this->load->view("helper/topbar",$data);
            $this->load->view("forgetpassword", $data);
            $this->load->view("helper/footer");


        }
    }


    public function logout()
    {
        $this->session->sess_destroy();

        redirect(base_url());

    }

//myaccount
    public function myaccount()
    {
       // p($this->session->userdata);
        if ($this->session->userdata('skiindia')=='') {
            redirect(base_url() . "packages");
        } else {


            if ($this->input->post('submit') == 'profile') {

                $parameterupdateacc = array(
                    'act_mode' => 'updateuser',
                    'userid' => $this->session->userdata['skiindia'],
                    'firstName' => $this->input->post('firstName'),
                    'lastName' => $this->input->post('lastName'),
                    'myprofiledob' => $this->input->post('myprofiledob'),
                    'email' => $this->input->post('email'),
                    'mobile' => $this->input->post('mobile'),
                    'address' => $this->input->post('address'),
                    'countryName' => $this->input->post('countryName'),
                    'stateName' => $this->input->post('stateName'),
                    'cityName' => $this->input->post('cityName'),
                    'pincode' => $this->input->post('pincode'),

                    'type' => 'web',
                );

                $path = api_url() . 'userapi/updateacc/format/json/';
                $data['updateacc'] = curlpost($parameterupdateacc, $path);

                $data['message'] = "Account Change Sucessfully";
            }

            $parameterseo = array('act_mode' => 'viewseotags', 'rowid' => 6);
            $data['seotags'] = $this->supper_admin->call_procedureRow('proc_siteconfig', $parameterseo);


            if ($this->input->post('submit') == 'changePswrd') {


                $this->form_validation->set_rules('currentpassword', 'Enter Current  password ', 'required');
                $this->form_validation->set_rules('newpassword', 'Enter  password ', 'required');
                $this->form_validation->set_rules('confirmpassword', 'Enter Confirm  password ', 'required');


                if ($this->input->post('confirmpassword') != $this->input->post('newpassword')) {
                    $this->form_validation->set_rules('confirmpassword', ' Password Not Match', 'required');
                }
                if ($this->form_validation->run() != FALSE) {
                    $parametercheckpasswrd = array(
                        'act_mode' => 'memecheckpassword',
                        'userid' => $this->session->userdata['skiindia'],
                        'currentpassword' => $this->input->post('currentpassword'),
                        'newpassword' => '',
                        'confirmpassword' => '',

                        'type' => 'web',
                    );

                    $path = api_url() . 'userapi/usercheckchangepassword/format/json/';
                    $data['checkpasswrd'] = curlpost($parametercheckpasswrd, $path);
                    if ((base64_decode($data['checkpasswrd']->user_password)) == $this->input->post('currentpassword')) {


                        $parameter = array(
                            'act_mode' => 'memechangepassword',
                            'userid' => $this->session->userdata['skiindia'],
                            'currentpassword' => $this->input->post('currentpassword'),
                            'newpassword' => $this->input->post('newpassword'),
                            'confirmpassword' => $this->input->post('confirmpassword'),
                            'type' => 'web',
                        );

                        $path = api_url() . 'userapi/userchangepassword/format/json/';
                        $data['userlog'] = curlpost($parameter, $path);

                        $data['message'] = "Password Change Sucessfully";

                    } else {
                        $data['message'] = "Password not attached in this mail id ";

                    }

                }
            }


            $parameterselectdata = array(
                'act_mode' => 'selectuser',
                'userid' => $this->session->userdata['skiindia'],
                'firstName' => $this->input->post('firstName'),
                'lastName' => $this->input->post('lastName'),
                'myprofiledob' => $this->input->post('myprofiledob'),
                'email' => $this->input->post('email'),
                'mobile' => $this->input->post('mobile'),
                'address' => $this->input->post('address'),
                'countryName' => $this->input->post('countryName'),
                'stateName' => $this->input->post('stateName'),
                'cityName' => $this->input->post('cityName'),
                'pincode' => $this->input->post('pincode'),

                'type' => 'web',
            );

            $path = api_url() . 'userapi/selectacc/format/json/';
            $data['selectacc'] = curlpost($parameterselectdata, $path);


            $parameter = array(
                'act_mode' => 'memusersesiid',
                'userid' => $this->session->userdata['skiindia'],
                'type' => 'web',
            );

            $path = api_url() . 'userapi/usersesion/format/json/';
            $data['memuser'] = curlpost($parameter, $path);


            $parameterorder = array(
                'act_mode' => 'selectorderbooking',
                'userid' => $this->session->userdata['skiindia'],
                'type' => 'web',
            );

            $path = api_url() . 'userapi/userorderbooking/format/json/';
            $data['selectaccountorder'] = curlpost($parameterorder, $path);


            $parameterordercompses = array(
                'act_mode' => 'selectorderbookingcompses',
                'userid' => $this->session->userdata['skiindia'],
                'type' => 'web',
            );

            $path = api_url() . 'userapi/userorderbooking/format/json/';
            $data['selectordercompses'] = curlpost($parameterordercompses, $path);


//Select branch
            $siteurl = base_url();
            $parameterbranch = array(
                'act_mode' => 'selectbranch',
                'weburl' => $siteurl,
                'type' => 'web',

            );

            $path = api_url() . 'selectsiteurl/branch/format/json/';
            $data['branch'] = curlpost($parameterbranch, $path);


//select banner images
            $parameterbanner = array(
                'act_mode' => 'selectbannerimages',
                'branchid' => $data['branch']->branch_id,
                'type' => 'web',

            );

            $path = api_url() . 'selectsiteurl/banner/format/json/';
            $data['banner'] = curlpost($parameterbanner, $path);

            $parametertearms = array(
                'act_mode' => 'selecttearms',
                'branchid' => $data['branch']->branch_id,
);

            $data['tearmsgatway'] = $this->supper_admin->call_procedure('proc_select_banner_v', $parametertearms);

            $this->load->view("helper/header");
            $this->load->view("helper/topbar", $data);
            $this->load->view("myaccount", $data);
            $this->load->view("helper/footer");


        }
    }

    public function saveemailbytwitter()
    {
        $userid=$this->session->userdata['skiindia'];
        $parameter=array(
            'act_mode'=>'twitter_email_save',
            'Param1'=>$userid,
            'Param2'=>$this->input->post('email_id'),
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'',
            'Param10'=>'',
            'Param11'=>'',
            'Param12'=>'',
            'Param13'=>'',
            'Param14'=>'',
            'Param15'=>''
        );

      //  pend($parameter);
        $responses = $this->supper_admin->call_procedureRow('proc_timeslotspackages',$parameter);
        echo $this->input->post('email_id');

    }
//myaccount
    public function cancelorder()
    {

//Select branch
        $siteurl = base_url();
        $parameterbranch = array(
            'act_mode' => 'selectbranch',
            'weburl' => $siteurl,
            'type' => 'web',

        );

        $path = api_url() . 'selectsiteurl/branch/format/json/';
        $data['branch'] = curlpost($parameterbranch, $path);


//select banner images
        $parameterbanner = array(
            'act_mode' => 'selectbannerimages',
            'branchid' => $data['branch']->branch_id,
            'type' => 'web',

        );

        $path = api_url() . 'selectsiteurl/banner/format/json/';
        $data['banner'] = curlpost($parameterbanner, $path);


        $from_email = $data['banner']->bannerimage_from ;
        $parameterorderselect = array(
            'act_mode' => 'selectorderbookingdata',
            'userid' => base64_decode($_GET['pacid']),
            'type' => 'web',
        );

        $path = api_url() . 'userapi/userorderbooking/format/json/';
        $data['selectaccountorder'] = curlpost($parameterorderselect, $path);
        $arr = (array)$data['selectaccountorder'];
//p( $data['selectaccountorder']);


        $message_new = '<table width="90%" style="line-height: 28px; font-family: sans-serif;"   >
<tr><td>From Mr/Ms ' . $arr[0]['billing_name'] . ',</td></tr>


<tr><td> The Transaction id is canceled '.$arr[0]['tracking_id'].'</td></tr>

<tr><td>Yours sincerely,<br>
'.$data['banner']->bannerimage_top3.' Team</td></tr>
</table>
';



        $message_new1 = '<table width="90%" style="line-height: 28px; font-family: sans-serif;"   >
<tr><td>Dear ' .  $arr[0]['billing_name'] . ',</td></tr>

<tr><td>Greetings from ' . $data['banner']->bannerimage_top3 . '.!</td></tr>


<tr><td>Should you have any queries, please feel free to write to us on '.$data['banner']->bannerimage_branch_email.' or call us on '.$data['banner']->bannerimage_branch_contact.'.</td></tr>
<tr><td>Yours sincerely,<br>
'.$data['banner']->bannerimage_top3.' Team</td></tr>
</table>
';


        $from_email = $data['banner']->bannerimage_from ;
        $to_email = $this->input->post('email');
        //Load email library
        $this->load->library('email');
        $this->email->to($from_email,  $data['banner']->bannerimage_top3 );
        $this->email->reply_to($from_email,  $data['banner']->bannerimage_top3 );
        $this->email->from($arr[0]['billing_email']);
        $this->email->subject('' . $data['banner']->bannerimage_top3 . ' - Your Acknowledgement for Request for Cancellation of Bookings');
        $this->email->message($message_new1);
        //Send mail
        $this->email->send();




        $this->email->from($from_email,  $data['banner']->bannerimage_top3 );
        $this->email->reply_to($from_email,  $data['banner']->bannerimage_top3 );
        $this->email->to($arr[0]['billing_email']);
        $this->email->subject('' . $data['banner']->bannerimage_top3 . ' - Your Acknowledgement for Request for Cancellation of Bookings');
        $this->email->message($message_new1);
        //Send mail
        $this->email->send();




        //pend($this->email->print_debugger());


        $parameterorder = array(
            'act_mode' => 'cancelorderbooking',
            'userid' => base64_decode($_GET['pacid']),
            'type' => 'web',
        );

        $path = api_url() . 'userapi/userorderbooking/format/json/';
        $data['selectaccountorder'] = curlpost($parameterorder, $path);

        header("location:myaccount");

    }
}//end of class
?>