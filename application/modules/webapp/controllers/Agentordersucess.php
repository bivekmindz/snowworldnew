<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Agentordersucess extends MX_Controller
{

    public function __construct()
    {
  
        $this->load->model("supper_admin");
        $this->load->library('session');

    }

     //Agent Order Success
    public function agentordersucesswallets()
    {
         
        $this->load->library('session');
//$this->session->sess_destroy();
        $parameterseo = array('act_mode' => 'viewseotags', 'rowid' => 14);
        $data['seotags'] = $this->supper_admin->call_procedureRow('proc_siteconfig', $parameterseo);
        $data['agentid'] = $this->session->userdata['ppid'];


        $siteurl= base_url();
        $parameterbranch=array(
            'act_mode' =>'selectbranch',
            'weburl' =>$siteurl,
            'type'=>'web',

        );

        $path=api_url().'selectsiteurl/branch/format/json/';
        $data['branch']=curlpost($parameterbranch,$path);


        $parameterccgatway=array(
            'act_mode' =>'selectccavenue',
            'branchid' =>$data['branch']->branch_id,
            'type'=>'web',

        );

        $path=api_url().'ccavenue/ccavRequestHandler/format/json/';
        $data['ccavRequestHandler']=curlpost($parameterccgatway,$path);


        $workingKey = $data['ccavRequestHandler']->pg_working_key;
        $encResponse = $_POST["encResp"];
        //pend($encResponse);//This is the response sent by the CCAvenue Server
        $rcvdString = decrypt($encResponse, $workingKey);        //Crypto Decryption used as per the specified working key.
        $order_status = "";
        $decryptValues = explode('&', $rcvdString);
        $dataSize = sizeof($decryptValues);
        for ($i = 0; $i < $dataSize; $i++) {
            $information = explode('=', $decryptValues[$i]);
            if ($i == 3) $order_status = $information[1];
        }

//echo "<pre>";
//print_r($information);


        $arr1 = explode("=", $decryptValues[1]);
        $arr2 = explode("=", $decryptValues[3]);
        $arr3 = explode("=", $decryptValues[8]);
        $arr4 = explode("=", $decryptValues[5]);
        $arr5 = explode("=", $decryptValues[10]);


        $tracking_id = $arr1[1];

        $order_status = $arr2[1];
        $status_message = $arr3[1];
        $paymentmode = $arr4[1];
        $paymentamount = $arr5[1];


// Ticket Type
        // User Display
        $agentdisplay = array('act_mode' => 'select_partner',
            'userid' => $this->session->userdata['ppid'],
            'type' => 'web',

        );
        $path3 = api_url() . "Ordersucess/selectuser/format/json/";
        $data['agentdisplaydata'] = curlpost($agentdisplay, $path3);


        $siteurl = base_url();
        $parameterbranch = array(
            'act_mode' => 'selectbranch',
            'weburl' => $siteurl,
            'type' => 'web',

        );

        $path = api_url() . 'selectsiteurl/branch/format/json/';
        $data['branch'] = curlpost($parameterbranch, $path);


        $parametertimeslot = array(
            'act_mode' => 'selectsestimeslot',
            'branchid' => $data['branch']->branch_id,
            'destinationType' => $this->session->userdata('destinationType'),
            'type' => 'web',

        );

        $path = api_url() . 'selecttimesloturl/timeslotses/format/json/';
        $data['timeslotses'] = curlpost($parametertimeslot, $path);


//Select branch
        $siteurl = base_url();
        $parameterbranch = array(
            'act_mode' => 'selectbranch',
            'weburl' => $siteurl,
            'type' => 'web',

        );

        $path = api_url() . 'selectsiteurl/branch/format/json/';
        $data['branch'] = curlpost($parameterbranch, $path);


//select banner images
        $parameterbanner = array(
            'act_mode' => 'selectbannerimages',
            'branchid' => $data['branch']->branch_id,
            'type' => 'web',

        );

        $path = api_url() . 'selectsiteurl/banner/format/json/';
        $data['banner'] = curlpost($parameterbanner, $path);


// Order walletupdate
        $orderamountupdate = array('act_mode' => 'orderwalletupdatesucess',
            'orderid' => '',
            'tracking_id' => $tracking_id,
            'order_status' => $order_status,
            'status_message' => $status_message,
            'paymentmode' => "$paymentmode",
            'paymentstatus' => 1,
            'ordersucesmail' => 1,
            'ordermailstatus' => 1,
            'agent_id' => $this->session->userdata['ppid'],
            'cred_debet' => 'Debit',
            'amount' => $paymentamount,
            'bankagentcommision' => 0,
            'type' => 'web',
        );

        $path = api_url() . "Ordersucess/selectwalletdata/format/json/";
        $data['orderwalletupdate'] = curlpost($orderamountupdate, $path);
        $from_email = $data['banner']->bannerimage_from;
        $mess = '<table width="90%" style="line-height: 28px; font-family: sans-serif;" >
       <tr><td>Dear  ' . preg_replace('/[^A-Za-z0-9\-]/', '', $data['agentdisplaydata']->agent_email) . ',</td></tr>
       <tr><td>Greetings from ' . $data['banner']->bannerimage_top3 . '.!</td></tr>
       <tr><td>
       Thank you for choosing our Services. 
       </td></tr>
       <tr><td>We look forward to welcoming you the next time you visit us at ' . $data['banner']->bannerimage_top3 . '.</td></tr>
       <tr><td>Yours sincerely,<br>
       '.$data['banner']->bannerimage_top3 . ' Team</td></tr>
       </table>';
        $to_email = $data['agentdisplaydata']->agent_username;

        //Load email library
        $this->load->library('email');
        $this->email->from($from_email, $data['banner']->bannerimage_top3);
        $this->email->reply_to($from_email, $data['banner']->bannerimage_top3);
        $this->email->to($to_email);
        $this->email->subject('' . $data['banner']->bannerimage_top3 . ' - Confirmation Voucher ');
        $this->email->message($mess);
        //Send mail
        $this->email->send();
        $orderagentupdate = array('act_mode' => 'orderpaymentupdatedata',
            'orderid' => $this->session->userdata['orderlastinsertid'],
            'type' => 'web',
        );
        $path = api_url() . "Ordersucess/selectorder/format/json/";
        $data['ordermaildataupdate'] = curlpost($orderagentupdate, $path);
        header("location:updateamount?emsg=sucess");
    }


}//end of class
?>