<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Addones extends MX_Controller {

    public function __construct() {
        $this->load->model("supper_admin");
        $this->load->library('session');
        session_start();
    }
    //addones page
    public function addones(){

        if($this->session->userdata('packageval')==''){

            redirect(base_url());
        }
        else{
//$this->session->sess_destroy();
            //Select Time slot
//Select branch
            $siteurl= base_url();
            $parameterbranch=array(
                'act_mode' =>'selectbranch',
                'weburl' =>$siteurl,
                'type'=>'web',

            );

            $path=api_url().'selectsiteurl/branch/format/json/';
            $data['branch']=curlpost($parameterbranch,$path);
			
//select banner images
            $parameterbanner=array(
                'act_mode' =>'selectbannerimages',
                'branchid' =>$data['branch']->branch_id,
                'type'=>'web',

            );

            $path=api_url().'selectsiteurl/banner/format/json/';
            $data['banner']=curlpost($parameterbanner,$path);


            $parametertimeslot=array(
                'act_mode' =>'selectsestimeslot',
                'branchid' =>$data['branch']->branch_id,
                'destinationType' =>$this->session->userdata('destinationType'),
                'type'=>'web',

            );

            $path = api_url().'selecttimesloturl/timeslotses/format/json/';
            $data['timeslotses'] = curlpost($parametertimeslot,$path);
			
			// dynamic data as per packages 
			//p($this->session->userdata('final_selected_package_data')); 
			//p( $this->session->userdata('packageval') ) ;
			//$data['final_selected_package_data'] = $this->session->userdata('final_selected_package_data');

            $data = array(
				
                'timeslot_from' =>   $data['timeslotses']->timeslot_from,
                'timeslot_to' =>  $data['timeslotses']->timeslot_to,
                'timeslot_seats' =>  $data['timeslotses']->timeslot_seats,
                'timeslot_minfrom' => $data['timeslotses']->timeslot_minfrom,
                'timeslot_minto' =>  $data['timeslotses']->timeslot_minto,

            );
            $this->session->set_userdata($data);


			
/*
            if($this->input->post('delete')!='')

            {


                $datadelete = array(
                    'act_mode' =>'deleteaddone',
                    'categories1' => '',
                    'addonid' => $this->input->post('delete'),
                    'addonprice' => '',
                    'addonqty' =>'',
                    'addonimage' => '',
                    'addonname' => '',
                    'uniqueid' => '',
                    'type' => 'web'
                );
                $pathdelete= api_url()."Cart/deleteaddonscartses_select/format/json/";
                $data['addonscartdelete']= curlpost($datadelete,$pathdelete);


            }
*/	
//  not in used start  //
/*		
            if($_POST['categories1']){
				

                for($i=0;$i<=count($_POST['categories1']);$i++){
					
                    if($_POST['categories1'][$i]!=0)
                    {
                        //echo $_POST['addonid'][$i];
                        $data13[$i] = array(
                            'act_mode' =>'selectaddone',
                            'categories1' => '',
                            'addonid' => $_POST['addonid'][$i],
                            'addonprice' =>'',
                            'addonqty' => '',
                            'addonimage' => '',
                            'addonname' => '',
                            'uniqueid' => $this->session->userdata['uniqid'],
                            'type' =>'web');
                        $pathaddone13 = api_url()."Cart/insertaddonscartses_select/format/json/";
                        $data['addonscartselectd']= curlpost($data13[$i],$pathaddone13);


                        $arr = (array)$data['addonscartselectd'];




                        if ($arr[0]['addonid'] == $_POST['addonid'][$i])
                        {

                            $data[$i] = array(
                                'act_mode' =>'updateaddone111',
                                'categories1' => $_POST['categories1'][$i],
                                'addonid' => $_POST['addonid'][$i],
                                'addonprice' => $_POST['addonprice'][$i],
                                'addonqty' => $_POST['addonqty'][$i],
                                'addonimage' => $_POST['addonimage'][$i],
                                'addonname' => $_POST['addonname'][$i],
                                'uniqueid' => $this->session->userdata['uniqid'],
                                'type' =>'web'
                            );

                            $pathaddone= api_url()."Cart/insertaddonscartses_select/format/json/";
                            $data['addonscart']= curlpost($data[$i],$pathaddone);

                            // p($data[$i]);



                        }
                        else{
							
							

                            $data[$i] = array(
                                'act_mode' =>'insertaddone',
                                'categories1' => $_POST['categories1'][$i],
                                'addonid' => $_POST['addonid'][$i],
                                'addonprice' => $_POST['addonprice'][$i],
                                'addonqty' => $_POST['addonqty'][$i],
                                'addonimage' => $_POST['addonimage'][$i],
                                'addonname' => $_POST['addonname'][$i],
                                'uniqueid' => $this->session->userdata['uniqid'],
                                'type' =>'web'
                            );
							
                            $pathaddone= api_url()."Cart/insertaddonscartses_select/format/json/";
                            $data['addonscart']= curlpost($data[$i],$pathaddone);
                            // p($data['addonscart']);
                            // p($data[$i]);

                        }

                    }

                }

            }
*/
//  not in used  end here //			

//  not in used 
            //p($obj);
			/*
            $dataaddonedisplay = array(
                'act_mode' =>'aaddonedisplay',
                'categories1' => '',
                'addonid' => '',
                'addonprice' => '',
                'addonqty' =>'',
                'addonimage' => '',
                'addonname' =>'',
                'uniqueid' => $this->session->userdata['uniqid'],
                'type' => 'web'
            );
            $pathdisplay= api_url()."Cart/displayaddonscartses_select/format/json/";
            $data['addonsdisplay'] = curlpost($dataaddonedisplay,$pathdisplay);
			*/
//  not in used end here 
	

			
			// MASTER ADD ON DATA
			// display addons parameters
            $parameterbranch        = array('act_mode'=>'addonscartses_package',
                'branch_id'=>$this->session->userdata['branch_id'],
                'type'=>'web',
            );
            $pathbranch= api_url()."Cart/getaddonscartses_select/format/json/";
            $data['addonscart'] = curlpost($parameterbranch,$pathbranch);
			
			$final_selected_addon_data = $final_selected_package_data_validated = $validate = [];
			
			
			if( !empty( $this->session->userdata('saving_price_promocode') ) ){   

				$this->session->unset_userdata('saving_price_promocode');
			}
			
			if( !empty( $this->session->userdata('total_promo_price') ) ){ 
				
				$this->session->unset_userdata('total_promo_price');
			}
			
			if( !empty( $this->session->userdata('promo_id') ) ){ 
			
				$this->session->unset_userdata('promo_id');				
			}			

			if( !empty( $this->session->userdata('promocode_name') ) ){ 
			
				$this->session->unset_userdata('promocode_name');				
			}									
			
			if( !empty( $this->input->post( 'final_selected_addon_data' ) ) ){ 
				
				$final_selected_addon_data = json_decode($this->input->post( 'final_selected_addon_data' ) ); 
				
				if( count( json_decode($this->input->post( 'final_selected_addon_data' ) ) ) ) {
					
					//p( $data['addonscart'] );
					foreach( $data['addonscart'] as $kkk => $vvv ){
						
						foreach( $final_selected_addon_data as $kkkk => $vvvv){
							
							if( count( array_intersect_assoc( $vvv, (array) $vvvv ) ) >= 10 ) {
								//echo '########################';
								//p( $values );
								 //$vvvv->addon_price = ( $vvvv->addon_price * $vvvv->qty );
									
								 $final_selected_package_data_validated[] = (array) $vvvv;
								//echo '########################';
								array_push($validate, 1);
									
							}else{
								
								array_push($validate, 0);
							}
						} 
						
					}
					//p( $final_selected_package_data_validated );
					//exit;
					if( array_sum($validate) && ( array_sum($validate) == count($final_selected_addon_data) ) ){
						
						$this->session->set_userdata( 'final_selected_addon_data', $data['addonsdisplay'] = count( $final_selected_package_data_validated ) ? $final_selected_package_data_validated : [] );
						
					}else{

						echo '<center>unauthorise access or invalid parameter</center>';
						exit;				
					}	
					
				}
			}else{ 
				
				if( !empty( $this->session->userdata( 'final_selected_addon_data') ) ) { 

					$this->session->unset_userdata( 'final_selected_addon_data' );
				}			
				  
			}			
			//p( $final_selected_package_data_validated ); exit;
//p($data['addonscart']);			
			
			// addon data manipulation start here	
			
			/*
			if( !empty( $this->session->userdata( 'final_selected_addon_data') ) ) { //echo 1;
				
				$this->session->unset_userdata( 'final_selected_addon_data' );
			}
			*/
			
			
			$grand_total = []; // final price 
			$data['addon_total_price'] = $data['addon_total_qty'] = $data['package_total_price'] = $data['package_total_qty'] = 0;
			
			//$addon_price_qty_array = getAddonTotalPrice( $data['addonsdisplay'] );
			$addon_price_qty_array = getAddonkeyValue( $data['addonsdisplay'] );
//p( $addon_price_qty_array );
			//exit;
			
			if( count($addon_price_qty_array) ){
				
				$data['addon_total_price'] = $grand_total[] = getSumAllArrayElement( $addon_price_qty_array['addon_price_array'] );
				$data['addon_total_qty'] = getSumAllArrayElement( $addon_price_qty_array['addon_qty_array'] );
				
			}	
			

			
			// addon data manipulation end here
			
			// package data manipulation start here
			$data['tmp_array'] = $this->session->userdata('final_selected_package_data');
			 
			$package_price_qty_array = getPackageTotalPrice( $this->session->userdata('final_selected_package_data') );

			$data['package_total_price'] =  $package_price_qty_array['package_price_array'];

			$data['package_total_qty'] = $package_price_qty_array['package_qty_array'];
			
			//$data['package_total_qty_no_visitor'] = $data['package_total_qty'];
			
			$grand_total[] = $data['package_total_price'] = getSumAllArrayElement( calculatePriceByQty( $data['package_total_price'], $data['package_total_qty'] ) );
			
			// no of visitors revised
			$data['package_total_qty'] = getSumAllArrayElement( $data['package_total_qty'] );
			
// add all start here
						
		//	$addon_price_qty_array = getAddonTotalPriceAddALL( $data['addonscart'] );

			
		//	if( count($addon_price_qty_array) ){
		//		$data['addon_total_price'] = $grand_total[] = getSumAllArrayElement( $addon_price_qty_array['addon_price_array'] );
		//		$data['addon_total_qty'] = getSumAllArrayElement( $addon_price_qty_array['addon_qty_array'] );
		//	}				
			
// add all end here
			
			// add to session
			
			$this->session->set_userdata( [ 'package_total_qty' =>  $data['package_total_qty'], 
										   'package_total_price' => $data['package_total_price']
 										  ] );
				

			$data['package_addon_total'] = getSumAllArrayElement( $grand_total ) ? getSumAllArrayElement( $grand_total ) : 0;
			// package data manipulation end here
			
            $parameterseo=array('act_mode'=>'viewseotags','rowid'=>1);
            $data['seotags'] = $this->supper_admin->call_procedureRow('proc_siteconfig',$parameterseo);



            if($this->input->post('submit')=='cartses')
            {
				$package_addon_total_price = '';
				// p( $data['addonsdisplay'] ); 
				// p( getAddonkeyValue( $data['addonsdisplay'] ) ); 
				
				$get_addon_key_value = getAddonkeyValue( $data['addonsdisplay'] );
				
				$addon_total_price =  getSumAllArrayElement( calculatePriceByQty( $get_addon_key_value['addon_qty_array'], $get_addon_key_value['addon_price_array'] ) );
				
				if( $addon_total_price ){
					
						$this->session->set_userdata( [ 
										   'package_addon_total_price' => ( (float) $data['package_addon_total'] + (float) $addon_total_price ) ,
										   'addon_total_price' => $addon_total_price,
										   'addon_total_qty' => getSumAllArrayElement( $get_addon_key_value['addon_price_array'] ) 
 										  ] );
					}
				
                $data = array(
					
					'cartpackageimage' => $_POST['cartpackageimage'],
                    'cartpackagename' => $_POST['cartpackagename'],
                    'cartpackageprice' => $data['package_total_price'],
                    'cartsubtotal' => ( (float) $data['package_addon_total'] + (float) $addon_total_price ),

                    'cartaddoneid' =>  implode(',',$get_addon_key_value['addon_id_array'] ),
                    'cartaddoneimage' =>  implode(',',$get_addon_key_value['addon_image_array']),
                    'cartaddonname' =>  implode(',',$get_addon_key_value['addon_name_array']),
                    'cartaddonqty' =>  implode(',',$get_addon_key_value['addon_qty_array']),
                    'cartaddonprice' =>  implode(',',$get_addon_key_value['addon_price_array']),

                );
				// p( $data );
				// exit;	
				
                $this->session->set_userdata($data);
//pend($this->session->userdata);
                if($this->session->userdata('skiindia')>0){
					
                    redirect(base_url()."summary");
                }elseif($this->session->userdata('skiindia_guest')>0){
					
                    redirect(base_url()."summary");
                }else{
					
                    redirect(base_url()."step4");
                }

            }

            $this->load->view("helper/header");
            $this->load->view("helper/topbar",$data);
            $this->load->view("addones",$data);
            $this->load->view("helper/footer");
        }
    }


    public function step4(){
		
		//p( $this->session->userdata );
		
        if($this->session->userdata('packageval')==''){
            redirect(base_url());
        } else {
            //Select branch
            $siteurl= base_url();
            $parameterbranch=array(
                'act_mode' =>'selectbranch',
                'weburl' =>$siteurl,
                'type'=>'web',

            );

            $path=api_url().'selectsiteurl/branch/format/json/';
            $data['branch']=curlpost($parameterbranch,$path);



//select banner images
            $parameterbanner=array(
                'act_mode' =>'selectbannerimages',
                'branchid' =>$data['branch']->branch_id,
                'type'=>'web',

            );

            $path=api_url().'selectsiteurl/banner/format/json/';
            $data['banner']=curlpost($parameterbanner,$path);




            $parametertimeslot=array(
                'act_mode' =>'selectsestimeslot',
                'branchid' =>$data['branch']->branch_id,
                'destinationType' =>$this->session->userdata('destinationType'),
                'type'=>'web',

            );

            $path=api_url().'selecttimesloturl/timeslotses/format/json/';
            $data['timeslotses']=curlpost($parametertimeslot,$path);


//continue Login
            if($this->input->post('submit')=='Sign In')
            {

                //Login
                $parameter3=array(
                    'act_mode' =>'memlogin',
                    'username' =>$this->input->post('emailmob'),
                    'password'=>$this->input->post('pwd'),
                    'type'=>'web',

                );

                $path=api_url().'userapi/userlogin/format/json/';
                $data['login']=curlpost($parameter3,$path);



                if($data['login']->scalar=='Something Went Wrong')
                {

                    $data['message']="Invalid Login";
                    //header("location:".base_url()."step4");
                }
                else
                {
			
					

                    // Userlog
                    $parameter4=array(
                        'act_mode' =>'memuserlog',
                        'user_id' =>$data['login']->user_id,
                        'type'=>'web',
                    );

                    $path=api_url().'userapi/userlog/format/json/';
                    $data['userlog']=curlpost($parameter4,$path);


                    $data = array(
                        'skiindia' => $data['login']->user_id,
						 'user_regdatafill' =>$data['login']->user_regdatafill,
                    );

                    $this->session->set_userdata($data);

                    $data['skiindases'] =($data['login']->user_id);


                    header("location:".base_url()."summary");
                }

//echo ($this->session->userdata->skiindia);

            }

//continue Register
            if($this->input->post('submit')=='Sign up')
            {
                //check user
                $parameter9=array(
                    'act_mode' =>'checkuser',
                    'email'=>$this->input->post('email'),
                    'type'=>'web',
                );

                $path=api_url().'userapi/usercheck/format/json/';
                $data['checkuser']=curlpost($parameter9,$path);


                if($data['checkuser']->scalar=='0')
                {
                    $this->form_validation->set_rules('fname', 'Enter First Name', 'required');
                    $this->form_validation->set_rules('lastName', 'Enter Last Name ', 'required');
                    $this->form_validation->set_rules('mobileno', 'Enter Mobile No', 'required');
                    $this->form_validation->set_rules('email', 'Enter Email id ', 'required');
                    $this->form_validation->set_rules('password', 'Enter Password', 'required');                 $this->form_validation->set_rules('cpassword', 'Confirm Password', 'required|matches[password]');



                    if ($this->form_validation->run() != FALSE) {







                        //Select branch
                        $siteurl= base_url();
                        $parameterbranch=array(
                            'act_mode' =>'selectbranch',
                            'weburl' =>$siteurl,
                            'type'=>'web',

                        );

                        $path=api_url().'selectsiteurl/branch/format/json/';
                        $data['branch']=curlpost($parameterbranch,$path);



                        //select banner images
                        $parameterbanner = array(
                            'act_mode' => 'selectbannerimages',
                            'branchid' => $data['branch']->branch_id,
                            'type' => 'web',

                        );

                        $path = api_url() . 'selectsiteurl/banner/format/json/';
                        $data['banner'] = curlpost($parameterbanner, $path);

//pend($data['banner']);
                        $parametersms = array(
                            'act_mode' => 'selectsms',
                            'branchid' => $data['branch']->branch_id,
                            'type' => 'web',

                        );

                        $path = api_url() . 'selectsiteurl/banner/format/json/';
                        $data['smsgatway'] = curlpost($parametersms, $path);




                        $message_new = '<table width="96%" style="line-height: 28px; font-family: sans-serif;" >
<tr><td>Dear '.$data['updateacc']->user_title.' '. ucfirst($data['updateacc']->user_firstname).' '.$data['updateacc']->user_lastname.',</td></tr>

<tr><td>Greetings from ' . $data['banner']->bannerimage_top3 . '.!</td></tr>

<tr><td>
You have received this communication in response to your request for your ' . $data['banner']->bannerimage_top3 . ' Account password to be sent to you via e-mail and sms.
</td></tr>
<tr><td>Your temporary password is: '.base64_decode($data['updateacc']->user_password).' </td></tr>
<tr><td>Please use the password exactly as it appears above.</td></tr>


<tr><td>Use this password to login to your ' . $data['banner']->bannerimage_top3 . ' account.</td></tr>

<tr><td>We request you immediately change your password by logging on to '.base_url().'</td></tr>


<tr><td>Should you have any queries, please feel free to write to us on '.$data['banner']->bannerimage_branch_email.' or call us on '.$data['banner']->bannerimage_branch_contact.'.</td></tr>



<tr><td>&nbsp;</td></td>
<tr><td>&nbsp;</td></td>
<tr><td>Yours sincerely,<br>
'.$data['banner']->bannerimage_top3.' Team</td></tr>
</table>
';


                        $from_email = $data['banner']->bannerimage_from ;;
                        $to_email = $this->input->post('email');
                        //Load email library
                        $this->load->library('email');
                        $this->email->from($from_email,  $data['banner']->bannerimage_top3 );
                        $this->email->to($to_email);
                        $this->email->subject('' . $data['banner']->bannerimage_top3 . ' - Your Registration Details');
                        $this->email->message($message_new);
                        //Send mail
                        $this->email->send();
                        //pend($this->email->print_debugger());
                        $emaildata=$this->input->post('email');
                        $mob=$this->input->post('mobileno');
                        $registersms=urlencode("Thank you for registering with us. You can use your email-id ".$emaildata." for managing your ".$data['banner']->bannerimage_top3." account.");
                        /*sms api*/
                        $sms_url = "http://bulkpush.mytoday.com/BulkSms/SingleMsgApi?feedid=".$data['smsgatway']->sms_feedid."&username=".$data['smsgatway']->sms_username."&password=".$data['smsgatway']->sms_password."&To=".$this->input->post('mobile_number')."&Text=".$data;
                        $sms = file_get_contents($sms_url);
                        /*sms api*/











                        //Register
                        $parameter6=array(
                            'act_mode' =>'memregister',
                            'title' =>'',
                            'fname' =>$this->input->post('firstName'),
                            'lastName'=>$this->input->post('lastName'),
                            'mobileno' =>$this->input->post('mobileno'),
                            'email'=>$this->input->post('email'),
                            'password' =>$this->input->post('password'),
                            'type'=>'web',

                        );

                        $path=api_url().'userapi/userregister/format/json/';
                        $data['register']=curlpost($parameter6,$path);

                        $arr = (array)$data['register'];

// Userlog
                        $parameter4=array(
                            'act_mode' =>'memuserlog',
                            'user_id' =>$arr[0]['ls'],
                            'type'=>'web',
                        );

                        $path=api_url().'userapi/userlog/format/json/';
                        $data['userlog']=curlpost($parameter4,$path);


                        $data = array(
                            'skiindia' => $arr[0]['ls'],
                        );

                        $this->session->set_userdata($data);

                        $data['skiindases'] =($arr[0]['ls']);
                        header("location:".base_url()."summary");
                    }}
                else
                {
                    $data['msg'] ="Email Already Exist";
                    // header("location:".base_url()."step4");
                }
            }






          $parameter=array(
                        'act_mode'=>'ViewCountrys',
                        'Param1'=>'',
                        'Param2'=>'',
                        'Param3'=>'',
                        'Param4'=>'',
                        'Param5'=>'',
                        'Param6'=>'',
                        'Param7'=>'',
                        'Param8'=>'',
                        'Param9'=>'',
                        'Param10'=>'',
                        'Param11'=>'',
                        'Param12'=>'',
                        'Param13'=>'',
                        'Param14'=>'',
                        'Param15'=>'',
                    );

 
$data['Countrys'] = $this->supper_admin->call_procedure('proc_timeslotspackages',$parameter);
       
       $parameter = array('act_mode'=>'select_order',
        'orderid'=>$this->session->userdata('orderlastinsertid'),
        'type'=>'web',

        );
        $path1 = api_url()."Cart/getpayment_package/format/json/";
        $data['paymentpac']= curlpost($parameter,$path1);


            $this->load->view("helper/header");
            $this->load->view("helper/topbar",$data);
            $this->load->view("loginregister",$data);
            $this->load->view("helper/footer");
        }
    }

    public function summary(){
        print_r($_POST);
        die();
        if($this->session->userdata('packageval')==''){
            redirect(base_url());
        } else {
//Select branch
            $siteurl= base_url();
            $parameterbranch=array(
                'act_mode' =>'selectbranch',
                'weburl' =>$siteurl,
                'type'=>'web',

            );

            $path=api_url().'selectsiteurl/branch/format/json/';
            $data['branch']=curlpost($parameterbranch,$path);



//select banner images
            $parameterbanner=array(
                'act_mode' =>'selectbannerimages',
                'branchid' =>$data['branch']->branch_id,
                'type'=>'web',

            );

            $path=api_url().'selectsiteurl/banner/format/json/';
            $data['banner']=curlpost($parameterbanner,$path);

//Select Time slot
            $parametertimeslot=array(
                'act_mode' =>'selectsestimeslot',
                'branchid' =>$data['branch']->branch_id,
                'destinationType' =>$this->session->userdata('destinationType'),
                'type'=>'web',

            );

            $path=api_url().'selecttimesloturl/timeslotses/format/json/';
            $data['timeslotses']=curlpost($parametertimeslot,$path);


            $data['internethandlingcharge']=($data['branch']->branch_internet_handling_charge);
//remove promocode

            if($_POST['removepromocode']=='removepromocode')
            {
                if($this->session->userdata('pp_addonedata')!='')
                {

                    $promodate=explode(",",$this->session->userdata('pp_addonedata'));

                    foreach($promodate as $v)
                    {

                        $parameter4 = array('act_mode' => 'test_del',
                            'Param1' => $v,
                            'Param2' => $this->session->userdata['uniqid'],
                            'Param3' => '',
                            'Param4' => '',
                            'Param5' => '',
                            'Param6' => '',
                            'Param7' => '',
                            'Param8' => '',
                            'Param9' => '');
                        //pend($parameter);
                        $response['vieww_n'] = $this->supper_admin->call_procedurerow('proc_addon_s', $parameter4);




                    }
                    $parameter4 = array('act_mode' => 'addonselectbyuniqid',
                        'categories1' => '',
                        'addonid' => '',
                        'addonprice' =>'',
                        'addonqty' => '',
                        'addonimage' => '',
                        'addonname' => '',
                        'uniqueid' => $this->session->userdata['uniqid'],);

                    $response['selectpromoval'] = $this->supper_admin->call_procedurerow('proc_tempaddoneadd_v', $parameter4);



                    $data = array(


                        'cartaddoneid' =>  $response['selectpromoval']->addonid,
                        'cartaddoneimage' =>  $response['selectpromoval']->addonimage,
                        'cartaddonname' =>  $response['selectpromoval']->addonname,
                        'cartaddonqty' =>  $response['selectpromoval']->addonqty,
                        'cartaddonprice' =>  $response['selectpromoval']->addonprice

                    );

                    $this->session->set_userdata($data);

                    $this->session->unset_userdata('ppromo_id');
                    $this->session->unset_userdata('ppcodename');
                    $this->session->unset_userdata('pp_type');
                    $this->session->unset_userdata('pp_addonedata');
                    $this->session->unset_userdata('pp_locationid');
                    $this->session->unset_userdata('pp_branchid');
                    $this->session->unset_userdata('pp_discount');
                    $this->session->unset_userdata('pp_status');
                    $this->session->unset_userdata('pp_expiry_date');
                    $this->session->unset_userdata('pp_allowed_per_user');



                    $this->session->unset_userdata('pp_start_date');
                    $this->session->unset_userdata('pp_allowed_times');
                    $this->session->unset_userdata('pp_createdon');
                    $this->session->unset_userdata('pp_modifiedon');
                    $this->session->unset_userdata('pp_logintype');
                    $this->session->unset_userdata('pp_addoneqty');
                    redirect("summary");

                }
                else{
                    $this->session->unset_userdata('ppromo_id');
                    $this->session->unset_userdata('ppcodename');
                    $this->session->unset_userdata('pp_type');
                    $this->session->unset_userdata('pp_addonedata');
                    $this->session->unset_userdata('pp_locationid');
                    $this->session->unset_userdata('pp_branchid');
                    $this->session->unset_userdata('pp_discount');
                    $this->session->unset_userdata('pp_status');
                    $this->session->unset_userdata('pp_expiry_date');
                    $this->session->unset_userdata('pp_allowed_per_user');



                    $this->session->unset_userdata('pp_start_date');
                    $this->session->unset_userdata('pp_allowed_times');
                    $this->session->unset_userdata('pp_createdon');
                    $this->session->unset_userdata('pp_modifiedon');
                    $this->session->unset_userdata('pp_logintype');
                    $this->session->unset_userdata('pp_addoneqty');
                    $this->session->unset_userdata('internethandlingcharge');


                    redirect("summary");
                }






            }
// Promocode
            if($_POST['promoFormdata']!='')
            {


                $epromodate=explode("/",$this->session->userdata('txtDepartDate'));
                $promodatevalda=($epromodate[1]."/".$epromodate[0]."/".$epromodate[2]);


                $parameter         = array('act_mode'=>'promoselect_package','promovalue'=>$_POST['promoFormdata'],'locationid'=>$this->session->userdata['locationid'],
                    'branch_id'=>$this->session->userdata['branch_id'],
                    'userid'=>$promodatevalda,
                );
                $path = api_url()."Cart/getpromoselect_package/format/json/";
                $data['promocode']= curlpost($parameter,$path);

                if($data['promocode']->scalar=='Something Went Wrong'){ $data['msg']="Invalid Promocode"; }
                else
                {

                    $dateses=explode("/",$data['promocode']->p_expiry_date);
                    $datepromo=explode("/",rtrim($this->session->userdata('txtDepartDate')," "));




                    $date2=date_create($dateses[2]."-".$dateses[0]."-".$dateses[1]);
                    $date1=date_create($datepromo[2]."-".$datepromo[1]."-".$datepromo[0]);
                    $diff=date_diff($date1,$date2);
                    $datesesval= $diff->format("%R%a");


                    if($datesesval>=0) {




                        if ($data['promocode']->p_addonedata != '') {


//p($this->session->userdata);
                            $promodate = explode(",", $data['promocode']->p_addonedata);

                            foreach ($promodate as $v) {

                                $data13 = array(
                                    'act_mode' => 'selectaddone',
                                    'categories1' => '',
                                    'addonid' => $v,
                                    'addonprice' => '',
                                    'addonqty' => '',
                                    'addonimage' => '',
                                    'addonname' => '',
                                    'uniqueid' => $this->session->userdata['uniqid'],
                                    'type' => 'web');
                                $pathaddone13 = api_url() . "Cart/insertaddonscartses_select/format/json/";
                                $data['addonscartselectd'] = curlpost($data13, $pathaddone13);


                                $arr = (array)$data['addonscartselectd'];

                                if ($data['addonscartselectd']->scalar != "Something Went Wrong") {
                                    $parameter4 = array('act_mode' => 'addonviewupdate',
                                        'Param1' => $v,
                                        'Param2' => '',
                                        'Param3' => '',
                                        'Param4' => '',
                                        'Param5' => '',
                                        'Param6' => '',
                                        'Param7' => '',
                                        'Param8' => '',
                                        'Param9' => '');
                                    //pend($parameter);
                                    $response['vieww'] = $this->supper_admin->call_procedurerow('proc_addon_s', $parameter4);

                                    $data[$i] = array(
                                        'act_mode' => 'updateaddonedata',
                                        'categories1' => $response['vieww']->addon_id,
                                        'addonid' => $response['vieww']->addon_id,
                                        'addonprice' => $response['vieww']->addon_price,
                                        'addonqty' => '',
                                        'addonimage' => $response['vieww']->addon_image,
                                        'addonname' => $response['vieww']->addon_name,
                                        'uniqueid' => $this->session->userdata['uniqid'],
                                        'type' => 'web'
                                    );

                                    $pathaddone = api_url() . "Cart/insertaddonscartses_select/format/json/";
                                    $data['addonscart'] = curlpost($data[$i], $pathaddone);

                                } else {


                                    $parameter4 = array('act_mode' => 'addonviewupdate',
                                        'Param1' => $v,
                                        'Param2' => '',
                                        'Param3' => '',
                                        'Param4' => '',
                                        'Param5' => '',
                                        'Param6' => '',
                                        'Param7' => '',
                                        'Param8' => '',
                                        'Param9' => '');
                                    //pend($parameter);
                                    $response['vieww'] = $this->supper_admin->call_procedurerow('proc_addon_s', $parameter4);

                                    $data = array(
                                        'act_mode' => 'insertaddone',
                                        'categories1' => $response['vieww']->addon_id,
                                        'addonid' => $response['vieww']->addon_id,
                                        'addonprice' => 0,
                                        'addonqty' => 1,
                                        'addonimage' => $response['vieww']->addon_image,
                                        'addonname' => $response['vieww']->addon_name,
                                        'uniqueid' => $this->session->userdata['uniqid'],
                                        'type' => 'web'
                                    );

                                    $pathaddone = api_url() . "Cart/insertaddonscartses_select/format/json/";
                                    $data['addonscart'] = curlpost($data, $pathaddone);


                                }


                            }

                            $data1 = array(
                                'act_mode' => 'addonselectbyuniqid',
                                'categories1' => '',
                                'addonid' => '',
                                'addonprice' => '',
                                'addonqty' => '',
                                'addonimage' => '',
                                'addonname' => '',
                                'uniqueid' => $this->session->userdata['uniqid'],

                            );

                            $data['vieww1'] = $this->supper_admin->call_procedurerow('proc_tempaddoneadd_v', $data1);

                            $data = array(


                                'cartaddoneid' => $data['vieww1']->addonid,
                                'cartaddoneimage' => $data['vieww1']->addonimage,
                                'cartaddonname' => $data['vieww1']->addonname,
                                'cartaddonqty' => $data['vieww1']->addonqty,
                                'cartaddonprice' => $data['vieww1']->addonprice,

                            );
                            $this->session->set_userdata($data);
                            $siteurl = base_url();
                            $parameterbranch = array(
                                'act_mode' => 'selectbranch',
                                'weburl' => $siteurl,
                                'type' => 'web',

                            );

                            $path = api_url() . 'selectsiteurl/branch/format/json/';
                            $data['branch'] = curlpost($parameterbranch, $path);

                            $parameter = array('act_mode' => 'promoselect_package', 'promovalue' => $_POST['promoFormdata'], 'locationid' => $this->session->userdata['locationid'],
                                'branch_id' => $this->session->userdata['branch_id'],
                                'userid' => $this->session->userdata['userid'],
                            );
                            $path = api_url() . "Cart/getpromoselect_package/format/json/";
                            $data['promocode'] = curlpost($parameter, $path);

                            $data = array(
                                'ppromo_id' => $data['promocode']->promo_id,
                                'ppcodename' => $data['promocode']->p_codename,
                                'pp_type' => $data['promocode']->p_type,
                                'pp_addonedata' => $data['promocode']->p_addonedata,
                                'pp_locationid' => $data['promocode']->p_locationid,
                                'pp_branchid' => $data['promocode']->p_branchid,
                                'pp_discount' => $data['promocode']->p_discount,
                                'pp_status' => $data['promocode']->p_status,
                                'pp_expiry_date' => $data['promocode']->p_expiry_date,
                                'pp_allowed_per_user' => $data['promocode']->p_allowed_per_user,
                                'pp_start_date' => $data['promocode']->p_start_date,
                                'pp_allowed_times' => $data['promocode']->p_allowed_times,
                                'pp_createdon' => $data['promocode']->p_createdon,
                                'pp_modifiedon' => $data['promocode']->p_modifiedon,
                                'pp_logintype' => $data['promocode']->p_logintype,
                                'pp_addoneqty' => $data['promocode']->p_addoneqty,
                                'internethandlingcharge' => $data['branch']->branch_internet_handling_charge,

                            );
                            $this->session->set_userdata($data);

                            redirect("summary");
                            //  echo "dddd"; exit;


                        } else {
                            $originalDate = $this->session->userdata('txtDepartDate');
                            $Date = explode("/", $this->session->userdata('txtDepartDate'));
                            $newDate = $Date[1] . "/" . $Date[0] . "/" . $Date[2];
                            if ($data['promocode']->p_logintype == '0' or $data['promocode']->p_logintype == '1') {

//echo $newDate."---".$data['promocode']->p_start_date."---".$data['promocode']->p_expiry_date;

                           

                                    //Complete Promo Select
                                    $parametercompuser = array('act_mode' => 'promoselectcompuser_package', 'promovalue' => $_POST['promoFormdata'], 'locationid' => '',
                                        'branch_id' => '',
                                        'userid' => '',
                                    );
                                    $pathcompuser = api_url() . "Cart/getpromoselect_packageuser/format/json/";
                                    $data['promocompuser'] = curlpost($parametercompuser, $pathcompuser);


                                    if ($data['promocompuser']->num < $data['promocode']->p_allowed_times) {
//Userwise Promo select

                                        $parameteruser = array('act_mode' => 'promoselectuser_package', 'promovalue' => $_POST['promoFormdata'], 'locationid' => '',
                                            'branch_id' => '',
                                            'userid' => $this->session->userdata('skiindia'),
                                        );
                                        $pathuser = api_url() . "Cart/getpromoselect_packageuser/format/json/";
                                        $data['promocodeuser'] = curlpost($parameteruser, $pathuser);


                                        $data = array(
                                            'ppromo_id' => $data['promocode']->promo_id,
                                            'ppcodename' => $data['promocode']->p_codename,
                                            'pp_type' => $data['promocode']->p_type,
                                            'pp_addonedata' => $data['promocode']->p_addonedata,
                                            'pp_locationid' => $data['promocode']->p_locationid,
                                            'pp_branchid' => $data['promocode']->p_branchid,
                                            'pp_discount' => $data['promocode']->p_discount,
                                            'pp_status' => $data['promocode']->p_status,
                                            'pp_expiry_date' => $data['promocode']->p_expiry_date,
                                            'pp_allowed_per_user' => $data['promocode']->p_allowed_per_user,
                                            'pp_start_date' => $data['promocode']->p_start_date,
                                            'pp_allowed_times' => $data['promocode']->p_allowed_times,
                                            'pp_createdon' => $data['promocode']->p_createdon,
                                            'pp_modifiedon' => $data['promocode']->p_modifiedon,
                                            'pp_logintype' => $data['promocode']->p_logintype,
                                            'pp_addoneqty' => $data['promocode']->p_addoneqty,
                                            'internethandlingcharge' => $data['branch']->branch_internet_handling_charge,

                                        );
                                        $this->session->set_userdata($data);


                                  

                                    // echo($this->session->userdata('txtDepartDate'));
                                } else {
                             
                                    $data['msg'] = "Invalid Promocode";
                                }
                            } else {

                                $data['msg'] = "Invalid Promocode";
                            }
                        }

                    }   else
                    {
                        $data['msg'] = "Invalid Promocode";
                    }
                    // pend($data['promocode']->p_addonedata);

                }



            }
            else
            {

                $data['msg']="Promo Not Add";
            }


            if($_POST['submit']=='final add')
            {


 $data = array(

                  
                    'totalvalue' =>  $_POST['total'],

                );
                $this->session->set_userdata($data);

                $parameterunique         = array('act_mode'=>'selectbooking',
                    'subtotal'=>'',
                    'discountamount'=>'',
                    'total'=>'',
                    'countryid'=>'',
                    'stateid'=>'',
                    'cityid'=>'',
                    'branch_id'=>'',
                    'locationid'=>'',
                    'userid'=>'',
                    'txtDepartDate1'=>'',
                    'txtDepartdata'=>'',
                    'paymentmode'=>'',
                    'ticketid'=>$this->session->userdata['uniqid'],
                    'packproductname'=>'',
                    'packimg'=>'',
                    'packpkg'=>'',
                    'packprice'=>'',
                    'promocodeprice'=>'',
                    'internethandlingcharges'=>'',
                    'txtfrommin'=>'',
                    'txttohrs'=>'',
                    'txttomin'=>'',
                    'txtfromd'=>'',
                    'txttod'=>'',
                    'p_codename'=>'',
                    'p_usertype'=>'',
                    'type'=>'web',

                );

                $path = api_url()."Cart/getorderadd_package/format/json/";
                $data['packageunique']= curlpost($parameterunique,$path);


                if($this->session->userdata('pid')>0)
                {
                    $_POST['p_usertype']="Agent";

                }
                elseif($this->session->userdata('skiindia')>0)
                {
                    $_POST['p_usertype']="User";

                }
                else{
                    $_POST['p_usertype']="Guest";

                }


                if($data['packageunique']->scalar=='Something Went Wrong')
                {


                    $parameter         = array('act_mode'=>'orderpackage_insert',
                        'subtotal'=>$_POST['subtotal'],
                        'discountamount'=>($_POST['discountamount']),
                        'total'=>$_POST['total'],
                        'countryid'=>$this->session->userdata['countryid'],
                        'stateid'=>$this->session->userdata['stateid'],
                        'cityid'=>$this->session->userdata['cityid'],
                        'branch_id'=>$this->session->userdata['branch_id'],
                        'locationid'=>$this->session->userdata['locationid'],
                        'userid'=>$this->session->userdata('skiindia'),
                        'txtDepartDate1'=>$_POST['txtDepartDate1'],
                        'txtDepartdata'=>$_POST['txfromhrs'],
                        'paymentmode'=>'NULL',
                        'ticketid'=>$this->session->userdata['uniqid'],
                        'packproductname'=>$this->session->userdata['cartpackagename'],
                        'packimg'=>$this->session->userdata['packageimageval'],
                        'packpkg'=>$this->session->userdata['packageqtyval'],
                        'packprice'=>$this->session->userdata['packagepriceval'],
                        'promocodeprice'=>$_POST['discountamount'],
                        'internethandlingcharges'=>$_POST['internethandlingcharges'],
                        'txtfrommin'=>$_POST['txtfrommin'],
                        'txttohrs'=>$_POST['txttohrs'],
                        'txttomin'=>$_POST['txttomin'],
                        'txtfromd'=>$_POST['txtfromd'],
                        'txttod'=>$_POST['txttod'],
                        'p_codename'=>$_POST['p_codename'],
                        'p_usertype'=>$_POST['p_usertype'],

                        'type'=>'web',

                    );

                    $path = api_url()."Cart/getorderadd_package/format/json/";
                    $data['packageadd']= curlpost($parameter,$path);
                }
                else
                {

                    $parameter         = array('act_mode'=>'orderpackage_update',
                        'subtotal'=>$_POST['subtotal'],
                        'discountamount'=>($_POST['discountamount']),
                        'total'=>$_POST['total'],
                        'countryid'=>$this->session->userdata['countryid'],
                        'stateid'=>$this->session->userdata['stateid'],
                        'cityid'=>$this->session->userdata['cityid'],
                        'branch_id'=>$this->session->userdata['branch_id'],
                        'locationid'=>$this->session->userdata['locationid'],
                        'userid'=>$this->session->userdata('skiindia'),
                        'txtDepartDate1'=>$_POST['txtDepartDate1'],
                        'txtDepartdata'=>$_POST['txtDepartdata'],
                        'paymentmode'=>'NULL',
                        'ticketid'=>$this->session->userdata['uniqid'],
                        'packproductname'=>$this->session->userdata['cartpackagename'],
                        'packimg'=>$this->session->userdata['packageimageval'],
                        'packpkg'=>$this->session->userdata['packageqtyval'],
                        'packprice'=>$this->session->userdata['packagepriceval'],
                        'promocodeprice'=>$_POST['discountamount'],
                        'internethandlingcharges'=>$_POST['internethandlingcharges'],
                        'txtfrommin'=>$_POST['txtfrommin'],
                        'txttohrs'=>$_POST['txttohrs'],
                        'txttomin'=>$_POST['txttomin'],
                        'txtfromd'=>$_POST['txtfromd'],
                        'txttod'=>$_POST['txttod'],
                        'p_codename'=>$_POST['p_codename'],
                        'type'=>'web',

                    );

                    $path = api_url()."Cart/getorderadd_package/format/json/";
                    $data['packageadd']= curlpost($parameter,$path);

                }




                $arr = (array)$data['packageadd'];

                for( $i= 0 ; $i < (count($_POST['ccartaddonqty'])) ; $i++ ) {

                    //echo "hello";
                    $parameter1 = array('act_mode'=>'orderaddone_insert',

                        'addonnameses'=>$_POST['ccartaddonname'][$i],
                        'addonquantityses'=>$_POST['ccartaddonqty'][$i],
                        'addonidses'=>$_POST['addonid'][$i],
                        'lastidses'=>$arr[0]['last_insert_id()'],
                        'addonpriceses'=>$_POST['ccartaddonprice'][$i],
                        'addonidses'=>$_POST['ccaddonid'][$i],
                        'ccartpackageimage'=>$_POST['ccartpackageimage'][$i],

                    );
                    $path1 = api_url()."Cart/getaddonadd_package/format/json/";
                    $data['promoaddoneadd']= curlpost($parameter1,$path1);


                }

                //promoadd


                $parameterpromo = array('act_mode'=>'orderaddone_insert',
                    'promoname'=>$this->session->userdata['ppcodename'],

                    'lastidses'=>$arr[0]['last_insert_id()'],


                );
                $response['vieww_n'] = $this->supper_admin->call_procedurerow('proc_promodata_s', $parameterpromo);


                $data = array(
                    'orderlastinsertid' => $arr[0]['last_insert_id()'],
                );

                $this->session->set_userdata($data);

                $data['skiindases'] =($data['login']->user_id);

                if($this->session->userdata('skiindia')>0)
                {
                    redirect(base_url()."payment");
                }
                elseif($this->session->userdata('ppid')>0)
                {
                    redirect(base_url()."paymentagent");
                }
                else
                {
                    redirect(base_url()."payment");
                }



                // header("location:".base_url()."payment");




            }
            $data['internethandlingcharge']=($data['branch']->branch_internet_handling_charge);
            $this->load->view("helper/header");
            $this->load->view("helper/topbar",$data);
            $this->load->view("summary",$data);
            $this->load->view("helper/footer");
        }}


    public function payment(){
		
//Select Time slot
        if($this->session->userdata('packageval')==''){
            redirect(base_url());
        } else {
			
//Select branch
            $siteurl= base_url();
            $parameterbranch=array(
                'act_mode' =>'selectbranch',
                'weburl' =>$siteurl,
                'type'=>'web',

            );

            $path=api_url().'selectsiteurl/branch/format/json/';
            $data['branch']=curlpost($parameterbranch,$path);



            $parameterccgatway=array(
                'act_mode' =>'selectccavenue',
                'branchid' =>$data['branch']->branch_id,
                'type'=>'web',

            );

            $path=api_url().'ccavenue/ccavRequestHandler/format/json/';
            $data['ccavRequestHandler']=curlpost($parameterccgatway,$path);

//select banner images
            $parameterbanner=array(
                'act_mode' =>'selectbannerimages',
                'branchid' =>$data['branch']->branch_id,
                'type'=>'web',

            );

            $path=api_url().'selectsiteurl/banner/format/json/';
            $data['banner']=curlpost($parameterbanner,$path);




            $parameter=array(
                'act_mode' =>'memusersesiid',
                'userid' =>($this->session->userdata['skiindia']>0) ? $this->session->userdata['skiindia'] : $this->session->userdata['skiindia_guest'],
                'type'=>'web',
            );

            $path=api_url().'userapi/usersesion/format/json/';
            $data['memuser']=curlpost($parameter,$path);




            $parametertimeslot=array(
                'act_mode' =>'selectsestimeslot',
                'branchid' =>$data['branch']->branch_id,
                'destinationType' =>$this->session->userdata('destinationType'),
                'type'=>'web',

            );

            $path=api_url().'selecttimesloturl/timeslotses/format/json/';
            $data['timeslotses']=curlpost($parametertimeslot,$path);

			
			if( ! empty( trim($this->session->userdata('final_cost_package_addon_handling')) ) ){
				$data['final_cost_package_addon_handling'] = $this->session->userdata('final_cost_package_addon_handling') ? $this->session->userdata('final_cost_package_addon_handling') : 0;	
			}

            $parameter = array('act_mode'=>'select_order',

                'orderid'=>$this->session->userdata('orderlastinsertid'),
                'type'=>'web',

            );
            $path1 = api_url()."Cart/getpayment_package/format/json/";
            $data['paymentpac']= curlpost($parameter,$path1);

          $parameter=array(
                        'act_mode'=>'ViewCountrys',
                        'Param1'=>'',
                        'Param2'=>'',
                        'Param3'=>'',
                        'Param4'=>'',
                        'Param5'=>'',
                        'Param6'=>'',
                        'Param7'=>'',
                        'Param8'=>'',
                        'Param9'=>'',
                        'Param10'=>'',
                        'Param11'=>'',
                        'Param12'=>'',
                        'Param13'=>'',
                        'Param14'=>'',
                        'Param15'=>'',
                    );

 
			$data['Countrys'] = $this->supper_admin->call_procedure('proc_timeslotspackages',$parameter);





            $this->load->view("helper/header");
            $this->load->view("helper/topbar",$data);
            $this->load->view("payment",$data);
            $this->load->view("helper/footer");
        }}

    public function paymentagent(){
//Select Time slot
        if($this->session->userdata('packageval')==''){
            redirect(base_url());
        } else {
//Select branch
            $siteurl= base_url();
            $parameterbranch=array(
                'act_mode' =>'selectbranch',
                'weburl' =>$siteurl,
                'type'=>'web',

            );

            $path=api_url().'selectsiteurl/branch/format/json/';
            $data['branch']=curlpost($parameterbranch,$path);



//select banner images
            $parameterbanner=array(
                'act_mode' =>'selectbannerimages',
                'branchid' =>$data['branch']->branch_id,
                'type'=>'web',

            );

            $path=api_url().'selectsiteurl/banner/format/json/';
            $data['banner']=curlpost($parameterbanner,$path);

            $parameterccgatway=array(
                'act_mode' =>'selectccavenue',
                'branchid' =>$data['branch']->branch_id,
                'type'=>'web',

            );

            $path=api_url().'ccavenue/ccavRequestHandler/format/json/';
            $data['ccavRequestHandler']=curlpost($parameterccgatway,$path);


            $parameter=array(
                'act_mode' =>'patrnusersesiid',
                'userid' =>$this->session->userdata['ppid'],
                'type'=>'web',
            );

            $path=api_url().'userapi/usersesion/format/json/';
            $data['memuser']=curlpost($parameter,$path);



            $parametertimeslot=array(
                'act_mode' =>'selectsestimeslot',
                'branchid' =>$data['branch']->branch_id,
                'destinationType' =>$this->session->userdata('destinationType'),
                'type'=>'web',

            );

            $path=api_url().'selecttimesloturl/timeslotses/format/json/';
            $data['timeslotses']=curlpost($parametertimeslot,$path);


            $parameter = array('act_mode'=>'select_order',

                'orderid'=>$this->session->userdata('orderlastinsertid'),
                'type'=>'web',

            );
            $path1 = api_url()."Cart/getpayment_package/format/json/";
            $data['paymentpac']= curlpost($parameter,$path1);

            $this->load->view("helper/header");
            $this->load->view("helper/topbar",$data);
            $this->load->view("paymentagent",$data);
            $this->load->view("helper/footer");
        }}

    public function adduser(){





        // Userlog
        $parameter4=array(
            'act_mode' =>'orderuserupdate',
            'user_id' =>$this->input->post('user_id'),
            'order_id'=>$this->input->post('order_idval'),
            'title' =>$this->input->post('title'),

            'billing_name' =>$this->input->post('billing_name'),
            'billing_email' =>$this->input->post('billing_email'),
            'billing_tel'=>$this->input->post('billing_tel'),
            'billing_address' =>$this->input->post('billing_address'),
            'billing_city'=>$this->input->post('billing_city'),
            'billing_state' =>$this->input->post('billing_state'),

            'billing_zip'=>$this->input->post('billing_zip'),
            'billing_country' =>$this->input->post('billing_country'),

            'type'=>'web',
        );

        $path=api_url().'userapi/userorderupdate/format/json/';
        $data['userregister']=curlpost($parameter4,$path);


    }

    public function addagentwalet(){





        // Userlog
        $parameter4=array(
            'act_mode' =>'orderagentupdate',
            'user_id' =>$this->input->post('user_id'),
            'order_id'=>$this->input->post('order_idval'),
            'title' =>$this->input->post('title'),

            'billing_name' =>$this->input->post('billing_namewallet'),
            'billing_email' =>$this->input->post('billing_emailwallet'),
            'billing_tel'=>$this->input->post('billing_telwallet'),
            'billing_address' =>$this->input->post('billing_addresswallet'),
            'billing_city'=>$this->input->post('billing_citywallet'),
            'billing_state' =>$this->input->post('billing_statewallet'),

            'billing_zip'=>$this->input->post('billing_zipwallet'),
            'billing_country' =>$this->input->post('billing_countrywallet'),

            'type'=>'web',
        );

        $path=api_url().'userapi/userorderupdate/format/json/';
        $data['userregister']=curlpost($parameter4,$path);


    }
    public function addagent(){





        // Userlog
        $parameter4=array(
            'act_mode' =>'orderagentupdate',
            'user_id' =>$this->input->post('user_id'),
            'order_id'=>$this->input->post('order_idval'),
            'title' =>$this->input->post('title'),

            'billing_name' =>$this->input->post('billing_name'),
            'billing_email' =>$this->input->post('billing_email'),
            'billing_tel'=>$this->input->post('billing_tel'),
            'billing_address' =>$this->input->post('billing_address'),
            'billing_city'=>$this->input->post('billing_city'),
            'billing_state' =>$this->input->post('billing_state'),

            'billing_zip'=>$this->input->post('billing_zip'),
            'billing_country' =>$this->input->post('billing_country'),

            'type'=>'web',
        );

        $path=api_url().'userapi/userorderupdate/format/json/';
        $data['userregister']=curlpost($parameter4,$path);


    }

 //Redirect to summary page
  public function summaryredirect(){

     header("location:summary");
    }

 public function guestregistered(){
      $parameter6=array(
           'act_mode' =>'guestregister',
            'title' =>$this->input->post('title'),

            'billing_name' =>$this->input->post('billing_name'),
            'billing_email' =>$this->input->post('billing_email'),
            'billing_tel'=>$this->input->post('billing_tel'),
            'billing_address' =>$this->input->post('billing_address'),
            'billing_city'=>$this->input->post('billing_city'),
            'billing_state' =>$this->input->post('billing_state'),
            'billing_zip'=>$this->input->post('billing_zip'),
            'billing_country' =>$this->input->post('billing_country'),
            'type'=>'web',
        );

        $path =api_url().'userapi/guestregister/format/json/';
        $data['register']=curlpost($parameter6,$path);
         $arr = (array)$data['register'];
         $data = array(
          'skiindia_guest' => $arr[0]['ls'],
     );

    $this->session->set_userdata($data);

 }


    public function addguest(){


//pend($data['register']);


        $arr = (array)$data['register'];

// Userlog
        $parameter4=array(
            'act_mode' =>'orderguestupdate',
            'user_id' =>$arr[0]['ls'],
            'order_id'=>$this->input->post('order_idval'),
            'title' =>$this->input->post('title'),

            'billing_name' =>$this->input->post('billing_name'),
            'billing_email' =>$this->input->post('billing_email'),
            'billing_tel'=>$this->input->post('billing_tel'),
            'billing_address' =>$this->input->post('billing_address'),
            'billing_city'=>$this->input->post('billing_city'),
            'billing_state' =>$this->input->post('billing_state'),

            'billing_zip'=>$this->input->post('billing_zip'),
            'billing_country' =>$this->input->post('billing_country'),

            'type'=>'web',
        );
        //p($parameter4);

        $path=api_url().'userapi/guestorderupdate/format/json/';
        $data['userlog']=curlpost($parameter4,$path);

//pend($data['userlog']);




        //$data = array(
        //    'skiindia' => $arr[0]['ls'],

        //        );

        //    $this->session->set_userdata($data);

        //  $data['skiindases'] = ($arr[0]['ls']);





    }

}//end of class
?>