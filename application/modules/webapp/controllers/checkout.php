<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class checkout extends MX_Controller {

    public function __construct() {
        $this->load->model("supper_admin");
        $this->load->library('session');

    }
    //Index page
//Checkout Step1 and add adons
    public function step1(){
        $parameterseo=array('act_mode'=>'viewseotags','rowid'=>3);
        $data['seotags']=$this->supper_admin->call_procedureRow('proc_siteconfig',$parameterseo);

        //loading session library
        $this->load->library('session');
        if($this->session->userdata('skiindiamember')->id){
            redirect(base_url());
        }
        else
        {
            if(isset($_POST['packageid'])!='') {
                $packageid=(implode(',',$_POST['packageid']));
                $data = array(
                    'ticketid' => $packageid,
                );

                $this->session->set_userdata($data);
            }

            if($_POST['submit']=='CONTINUE')
            {

                $data = array(
                    'package_name' => implode(',',$_POST['package_name']),
                    'ddAdult' => implode(',',$_POST['ddAdult']),
                    'package_price' => implode(',',$_POST['package_price']) ,
                    'total_price' => $_POST['total_price'],
                    'addon_nameses' => implode(',',$_POST['addon_nameval']),
                    'addon_idses' => implode(',',$_POST['addon_id']),
                    'addon_quantityses' => implode(',',$_POST['addon_quantity']),
                    'addon_priceses' => implode(',',$_POST['addon_price'])
                );

                $this->session->set_userdata($data);
                header("location:step2");
            }
            if($_POST['submit']=='Update Quantity')
            {

                if($_POST['ddAdult']=='0'){ $_POST['ddAdult']= "1";} else {$_POST['ddAdult']=$_POST['ddAdult'];}
                if($_POST['addon_quantity']=='0'){ $_POST['addon_quantity']= "1";} else {$_POST['addon_quantity']=$_POST['addon_quantity'];}


                $data = array(

                    'package_name' => implode(',',$_POST['package_name']),
                    'ddAdult' => implode(',',$_POST['ddAdult']),
                    'package_price' => implode(',',$_POST['package_price']) ,
                    'total_price' => $_POST['total_price'],
                    'addon_quantityses' => implode(',',$_POST['addon_quantity']),
                    'addon_nameses' => implode(',',$_POST['addon_nameval']),
                    'addon_idses' => implode(',',$_POST['addon_id']),
                    'addon_quantity' => implode(',',$_POST['addon_quantity']),
                    'addon_priceses' => implode(',',$_POST['addon_price'])
                );

                $this->session->set_userdata($data);
            }



            if($_POST['submit']=='select Addon')
            {

                $data = array(
                    'addonid' => ($_POST['selctaddon']),

                );
                $this->session->set_userdata( $data);
            }


            $counter = 0;
            foreach($this->session->userdata('addonid') as $count => $value)
            {

//$delsms=(explode("deleteaddone-",$_POST['submit']));



                if(($_POST['submit']=='deleteaddone-'.$value))
                {

                    $delsms=(explode("deleteaddone-",$_POST['submit']));



                    $filter = $this->session->userdata('addonid');
                    $index = array_search($delsms[1], $filter);
                    unset($filter[$index]);
//$this->session->unset_userdata('addonid',$index); 
                    $this->session->set_userdata('addonid', $filter);


                }
//addone value in session 
                if($this->session->userdata['addonid'][$counter]!=''){
                    $parameter2        = array('act_mode'=>'addonscartses_package','addon_id'=>$this->session->userdata['addonid'][$counter]);

                    $path2= api_url()."Cart/getaddonscartses_select/format/json/";
                    $data['addonscartses'][]= curlpost($parameter2,$path2);

                }$counter++;
            }

            $ticketidval= (explode(",",$this->session->userdata('ticketid')));


            foreach($ticketidval as $count1 => $value1)
            {
//echo $value1;
                $parameter         = array('act_mode'=>'ticketcart_package','branch_id'=>'','packageid'=>$value1);
                $path = api_url()."Cart/getpackagecart_select/format/json/";
                $data['cartpackage'][]= curlpost($parameter,$path);


            }
//pend($data['cartpackage']);

            $parameter1         = array('act_mode'=>'addonscart_package','Param1'=>$this->session->userdata['branch_id'],'Param2'=>'','Param3'=>'','Param4'=>'','Param5'=>'','Param6'=>'','Param7'=>'','Param8'=>'','Param9'=>'');
            $path1= api_url()."Cart/getaddonscart_select/format/json/";
            $data['addonscart']= curlpost($parameter1,$path1);

            $this->load->view("helper/header");

            $this->load->view("step1",$data);
            $this->load->view("helper/footer");
        }

    }

//Checkout Step2 display data and add promo code
    public function step2(){

        if($this->session->userdata('skiindiamember')->id){
            redirect(base_url());
        }
        else
        {
            $parameterseo=array('act_mode'=>'viewseotags','rowid'=>4);
            $data['seotags']=$this->supper_admin->call_procedureRow('proc_siteconfig',$parameterseo);
//print_r($this->session->userdata);  exit;

            if($_POST['promoFormdata']!='')
            {

                $parameter         = array('act_mode'=>'promoselect_package','promovalue'=>$_POST['promoFormdata'],'locationid'=>$this->session->userdata['locationid']);
                $path = api_url()."Cart/getpromoselect_package/format/json/";
                $data['promocode']= curlpost($parameter,$path);

                if($data['promocode']->scalar=='Something Went Wrong'){
                    $this->session->set_flashdata('message', 'Invalid Promo code.');
                    // redirect("step2");

                }else { }
            }
            if(($this->input->post('submit'))=='CONTINUE')
            {
//print_r($this->session->userdata); 'package_name'=>$this->session->userdata['package_name'], 'package_price'=>$this->session->userdata['package_price']


                $parameter         = array('act_mode'=>'orderpackage_insert',
                    'subtotal'=>$_POST['subtotal'],
                    'discountamount'=>$_POST['discountamount'],
                    'total'=>$_POST['total'],
                    'countryid'=>$this->session->userdata['countryid'],
                    'stateid'=>$this->session->userdata['stateid'],
                    'cityid'=>$this->session->userdata['cityid'],
                    'branch_id'=>$this->session->userdata['branch_id'],
                    'locationid'=>$this->session->userdata['locationid'],
                    'userid'=>'0',
                    'txtDepartDate1'=>$_POST['txtDepartDate1'],
                    'txtDepartdata'=>$_POST['txtDepartdata'],

                    'ticketid'=>$this->session->userdata['ticketidticketid']

                );

                $path = api_url()."Cart/getorderadd_package/format/json/";
                $data['promocodeadd']= curlpost($parameter,$path);

                $arr = (array)$data['promocodeadd'];

                $package_nameval= (explode(",",$this->session->userdata('package_name')));
                $package_priceval= (explode(",",$this->session->userdata('package_price')));

                for( $j= 0 ; $j < (count($package_nameval)) ; $j++ ) {
                    $parameter2 = array('act_mode'=>'orderpackagedata_insert',
                        'packagenameses'=>$package_nameval[$j],
                        'package_priceses'=>$package_priceval[$j],
                        'lastidses'=>$arr[0]['last_insert_id()'],


                    );
                    $path = api_url()."Cart/insertorderpackage_data/format/json/";
                    $data['promopackageoneadd']= curlpost($parameter2,$path);
                }


                $addonqtyval= (explode(",",$this->session->userdata('addon_quantityses')));
                $addon_nameval= (explode(",",$this->session->userdata('addon_nameses')));
                $addon_idval= (explode(",",$this->session->userdata('addon_idses')));
                $addon_priceval= (explode(",",$this->session->userdata('addon_priceses')));

                for( $i= 0 ; $i < (count($addonqtyval)) ; $i++ ) {

                    //echo "hello";
                    $parameter1 = array('act_mode'=>'orderaddone_insert',
                        'addonquantityses'=>$addonqtyval[$i],
                        'addonnameses'=>$addon_nameval[$i],
                        'addonidses'=>$addon_idval[$i],
                        'lastidses'=>$arr[0]['last_insert_id()'],
                        'addonpriceses'=>$addon_priceval[$i],

                    );
                    $path = api_url()."Cart/getaddonadd_package/format/json/";
                    $data['promoaddoneadd']= curlpost($parameter1,$path);


                }
                $data = array(
                    'lastidses' => $arr[0]['last_insert_id()'],
                    'total'=>$_POST['total'],

                );

                $this->session->set_userdata($data);
                header("location:step3");
            }


            $this->load->view("helper/header");
            $this->load->view("helper/message");
            $this->load->view("step2",$data);
            $this->load->view("helper/footer");
        }

    }






}//end of class
?>