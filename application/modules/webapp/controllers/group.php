<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class group extends MX_Controller {

   public function __construct() {
      $this->load->model("supper_admin");
   $this->load->library('session'); 
  }
  //Index page
public function groupbooking(){
if($this->session->userdata('skiindiamember')->id){
    redirect(base_url()."home");
  }
  else{
    $parameterseo=array('act_mode'=>'viewseotags','rowid'=>7);
     $data['seotags']=$this->supper_admin->call_procedureRow('proc_siteconfig',$parameterseo);

   $siteurl= base_url();
$parameterbranch=array(
          'act_mode' =>'selectbranch',
          'weburl' =>$siteurl,
        'type'=>'web',
         
          );

       $path=api_url().'selectsiteurl/branch/format/json/'; 
   $data['branch']=curlpost($parameterbranch,$path); 



if($_POST['Submit']=='Submit')
{

 $this->form_validation->set_rules('bookingType', 'Select Booking Type', 'required');
  $this->form_validation->set_rules('full_name', 'Enter Full Name', 'required');
   $this->form_validation->set_rules('email_address', 'Enter Email Id', 'required');
  $this->form_validation->set_rules('phone_number', 'Enter Phone Number', 'required');
   $this->form_validation->set_rules('departDate', 'Select Date', 'required');
  $this->form_validation->set_rules('sessionTime', 'Select Time', 'required');
 $this->form_validation->set_rules('number_people', 'Enter Number of people', 'required');

  

   if ($this->form_validation->run() != FALSE) {
$parametergroupbooking=array(
          'act_mode' =>'insertgroups',
          'bookingType' =>$_POST['bookingType'],
          'full_name' =>$_POST['full_name'],
          'email_address' =>$_POST['email_address'],
          'phone_number' =>$_POST['phone_number'],
          'departDate' =>$_POST['departDate'],
          'sessionTime' =>$_POST['sessionTime'],
          'number_people' =>$_POST['number_people'],
          'socialGroupRadio' =>$_POST['socialGroupRadio'],
          'socialGroupName' =>$_POST['socialGroupName'],
         'stateid' => $data['branch']->stateid,
          'cityid' => $data['branch']->cityid,
          'branch_id' => $data['branch']->branch_id,
          'locationid' => $data['branch']->branch_location,
           'countryid' => $data['branch']->countryid,
        'type'=>'web',
         
          );
  $path=api_url().'booking/bookingadded/format/json/';

  /*sms api*/
$sms_url = "http://bulkpush.mytoday.com/BulkSms/SingleMsgApi?feedid=356637&username=9867622549&password=mtpjp&To=".$_POST['phone_number']."&Text='Booked Sucessfully.'";
    $sms = file_get_contents($sms_url, null);
    // pend($sms);
    /*sms api*/




       $to_email = "bivek@mindztechnology.com";
       $from_email = $this->input->post('email_address');
       $mess='<table>
   <tr><td></td><td></td></tr>
    <tr><td>First Name</td><td>'.$_POST['full_name'].'</td></tr>
	  <tr><td>Email</td><td>'.$_POST['email_address'].'</td></tr>
	   <tr><td>Phone</td><td>'.$_POST['phone_number'].'</td></tr>
   </table>';
       //Load email library
       $this->load->library('email');
       $this->email->from($from_email, 'skiindia');
      $this->email->reply_to($from_email, 'skiindia');
       $this->email->to($to_email);
       $this->email->subject('Contact');
       $this->email->message($mess);
       //Send mail
       $this->email->send();
       $this->email->print_debugger();

 
   $data['bookingadded']=curlpost($parametergroupbooking,$path);
       $this->session->set_flashdata('message', 'Thank you.! We will contact you shortly!');
       $data['message']='Thank you.! We will contact you shortly!';  }
}

$parameterbookingtype=array(
          'act_mode' =>'selectbookingtype',
           'countryid' => $data['branch']->countryid,
             'stateid' => $data['branch']->stateid,
              'cityid' => $data['branch']->cityid,
               'branch_id' => $data['branch']->branch_id,
                'locationid' => $data['branch']->branch_location,
               'type'=>'web',
          );

       $path=api_url().'booking/bookingtype/format/json/'; 
   $data['bookingtype']=curlpost($parameterbookingtype,$path);

   $this->load->view("helper/header",$data);
  $this->load->view("helper/topbar",$data);
 $this->load->view("groupbooking",$data);
     $this->load->view("helper/footerpac");
  }
}




}//end of class
?>