<h1>Dashboard</h1>
<div class="arrow-ri"><span><i class="fa fa-home"></i></span></div>
<div class="show-data">
    <h1>Showing data for 21-06-2017</h1>
</div>
<div class="row">
    <div class="all-invoeic">
        <div class="col-md-12">
            <div class="left_bar">
                <div class="col-md-4">
                    <div class="box1">
                        <p>User Total Amount</p>
                        <h1>0</h1>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box2">
                        <p>Last Transaction Amount Date</p>
                        <h1>0</h1>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box1">
                        <p>Wallet Amount</p>
                        <h1>0</h1>
                    </div>
                </div>
            </div>
            <div class="left_bar">
                <div class="col-md-4">
                    <div class="box1">
                        <p>Today Ticket Sold</p>
                        <h1>0</h1>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box2">
                        <p>Commision</p>
                        <h1>0</h1>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box1">
                        <p>Quote</p>
                        <h1>00000000</h1>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-md-12 col-xs-12">
        <div class="transit-detail">
            <div class="row">
                <div class="col-lg-10 col-md-10 col-xs-12">
                    <h1>Transaction Detail</h1>
                </div>
                <div class="col-lg-2 col-md-2 col-xs-12 text-right">
                    <button type="button" class="view_btn"> View</button>
                </div>
            </div>
            <div class="trans_table">
                <table>
                    <tr>
                        <th>Usear Name</th>
                        <th>Packege / Addons</th>
                        <th>Totals</th>
                        <th>Payment By</th>
                    </tr>
                    <tr>
                        <td>Sandeep Kumar</td>
                        <td>Standerd Packege</td>
                        <td>30000.00</td>
                        <td>Sandeep Kumar</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<!--  <div class="row">
    <div class="col-md-12">
        <div class="mostresch">
            <h1>MOST RECENT BOOKING : <span>BOOKING DATE : </span></h1>
            <div class="re">
                <div class="col-md-3">
                    <div class="visidta">
                        <div class="panel-body">
                            <div class="widget-summary">
                                <div class="visidta1">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <div class="widget-summary-col">
                                    <h4 class="title">Visit Date</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="visidta visidta2">
                        <div class="panel-body">
                            <div class="widget-summary">
                                <div class="visidta1 visidta2">
                                    <i class="fa fa-group"></i>
                                </div>
                                <div class="widget-summary-col">
                                    <h4 class="title">No. Of Visitors</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="visidta visidta3">
                        <div class="panel-body">
                            <div class="widget-summary">
                                <div class="visidta1 visidta3">
                                    <i class="fa fa-rupee"></i>
                                </div>
                                <div class="widget-summary-col">
                                    <h4 class="title">Amount</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="visidta visidta4">
                        <div class="panel-body">
                            <div class="widget-summary">
                                <div class="visidta1 visidta4">
                                    <i class="fa fa-barcode"></i>
                                </div>
                                <div class="widget-summary-col">
                                    <h4 class="title">Promo Code</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->
<style>
.all-invoeic .left_bar .box1 p{
margin: 15px 0 0 0;
}
.all-invoeic .left_bar .box2 p{
margin: 15px 0 0 0;
}
.transit-detail{
width: 100%; float: left;
margin: 15px 0 0 0;
}
.transit-detail .view_btn{
padding: 10px 20px;
background: #2b303a;
color: #fff;
border: none;
font-size: 16px;
margin: 15px 0 0 0;
}
.transit-detail h1{
font-size: 31px;
}
.trans_table{
width: 100%; float: left;
margin: 20px 0 0 0;
}
.trans_table{
width: 100%; float: left;
}
.trans_table table{
width: 100%;
float: left;
border-collapse: collapse;
}
.trans_table table th {
border: 1px solid #dddddd;
text-align: left;
padding: 8px;
font-weight: 600;
font-size: 16px;
color: #393939;
}
.trans_table table td{
border: 1px solid #dddddd;
text-align: left;
padding: 8px;
color: #2b2a2a;
background: #f9f9f9;
}
</style>