<style>
.modal-header {padding: 25px;}
th{ text-align: center !important;}
.nav-tabs{border:none;}
.cancele{ width: 100%; float: left;
padding:0px 0px 0px 0px;}
.cancele a{background-color: #335f9b;
padding: 10px 10px 10px 10px;
border-radius: 5px;
color: #fff;
font-weight: 800;}

</style>
<style type="text/css">
#mask {
position: absolute;
left: 0;
top: 0;
z-index: 9000;
background-color: rgba(0,0,0,0.75);
display: none;
}
#boxes .window {
position: absolute;
left: 0;
top: 0;
width: 440px;
height: 200px;
display: none;
z-index: 9999;
padding: 20px;
border-radius: 15px;
text-align: center;
}
.subs-for{margin: 15px 0;}
.subs-for input[type="text"]{
  width: 85%; float: left;
}
.subs-for input[type="button"]{
    background: #4bbbec;
    color: #fff;
    text-transform: capitalize;
    font-size: 18px;
    border: none;
    padding: 4px 10px;
    line-height: 27px;
    /* float: left; */
    margin: 0 0 0 10px;
}
.subs-for input[type="button"]:focus, .subs-for input[type="text"]:focus{
  outline: none;
}
.subs-for lable{
display: block;
text-align: left;
margin: 0 0 10px 0;
}
#boxes #dialog {
width: 575px;
height: 155px;
padding: 10px;
background-color: #ffffff;
font-family: 'Segoe UI Light', sans-serif;
font-size: 15pt;
}
#popupfoot {
font-size: 16pt;
position: absolute;
bottom: 0px;
width: 250px;
left: 250px;
}
</style>
<script>
function savemail(e)
{
var email = $('#emailid').val();
//alert(email);
var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
if(email=="")
{
$(".emailmsg").html("Please enter your email.").css({ 'color': 'red' });
return false;
}
else if(!emailReg.test(email))
{
$(".emailmsg").html("Enter a valid email address.").css({ 'color': 'red' });
return false;
}
else
{
$(".emailmsg").html(" ").css({ 'color': 'red' });
}
$.ajax({
url: '<?php echo base_url();?>webapp/User/saveemailbytwitter',
type: 'POST',
data: { 'email_id': email },
success: function(data) {
//alert(data);
$(".emailmsg").html("Thanks for submiting emailid").css({ 'color': 'green' });
window.location.reload();
//                 $('#email').val=data;
//$('.window').hide();
//$('#mask').hide();
//return false;
}
});
}
</script>
<script type="text/javascript">
$(document).ready(function() {
  if($('#email').val()=='')
  {
      
var id = '#dialog';
var maskHeight = $(document).height();
var maskWidth = $(window).width();
$('#mask').css({'width': maskWidth, 'height': maskHeight});
$('#mask').fadeIn(500);
$('#mask').fadeTo("slow", 0.9);
var winH = $(window).height();
var winW = $(window).width();
$(id).css('top', winH / 2 - $(id).height() / 2);
$(id).css('left', winW / 2 - $(id).width() / 2);
$(id).fadeIn(2000);
}
//   $('.window .close').click(function (e) {
//
//   e.preventDefault();
//   $('#mask').hide();
//
//   $('.window').hide();
//
//   });
// $('#mask').click(function () {
// $(this).hide();
// $('.window').hide();
// });
});
</script>
<script type="text/javascript">
function printDiv(divName) {
var printContents = document.getElementById(divName).innerHTML;
var originalContents = document.body.innerHTML;
document.body.innerHTML = printContents;
window.print();
document.body.innerHTML = originalContents;
}
</script>
<div class="container margin_30">
  <div class="register box_style_1">
    <h3>
    <i class="icon-user"></i> My Account<span class="login_status">Welcome,
      <?php echo $selectacc->user_firstname; ?>  <?php echo $selectacc->user_lastname; ?> | <span style="display: inline-block;">
        <form id="j_idt27" name="j_idt27" method="post" action="" enctype="application/x-www-form-urlencoded">
          <input type="hidden" name="j_idt27" value="j_idt27" />
          <a id="j_idt27:j_idt28" href="<?php echo base_url(); ?>logout" class="ui-commandlink ui-widget" onclick="PrimeFaces.ab({s:'j_idt27:j_idt28'});return false;">Log Out</a><input type="hidden" name="javax.faces.ViewState" id="j_id1:javax.faces.ViewState:5" value="1036046285632908855:-5943212799464085644" autocomplete="off" />
        </form>
      </span>
    </span>
    </h3>
    
    <ul class="nav nav-tabs" id="tabs" >
      <li class="active"><a href="#my_bookings" data-toggle="tab">My
      Bookings</a></li>
      <li><a href="#profile" data-toggle="tab">Profile</a></li>
      <li><a href="#password" data-toggle="tab">Edit Password</a></li>
    </ul>
    <div class="tab-content"><html xmlns="http://www.w3.org/1999/xhtml">
      <div class="tab-pane active" id="my_bookings">
        <div class="panel-group" id="accordion">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
              <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Bookings<i class="indicator icon-angle-circled-up pull-right"></i></a>
              </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in">
              <div class="panel-body text-center">
                <div class="order_summary">
                  <div class="order_titile">
                    
                    
                    <div class="col-lg-3 col-md-3"><div class="text_d">Package</div></div>
                    
                    <div class="col-lg-2 col-md-2"><div class="text_d">Total Price</div></div>
                    <div class="col-lg-2 col-md-2"><div class="text_d">Addones</div></div>
                    <div class="col-lg-2 col-md-2"><div class="text_d">Print</div></div>
                    
                  </div>
                  <?php foreach ($selectaccountorder as $key => $value) { if($value>0) { ?>
                  
                  <div class="summary_product">
                    
                    
                    
                    
                    
                    
                    <div class="col-lg-3 col-md-3">
                      <div class="product_text">
                        <div class="product_p"> <?php echo $value['pacorderid']; ?>  <?php echo (implode(',',explode('#',$value['packproductname']))); ?> </div>
                        
                      </div>
                    </div>
                    <div class="col-lg-2 col-md-2">
                      <div class="product_text">
                        <div class="product_rs"><i class="icon-rupee" aria-hidden="true"></i><?php echo $value['total']; ?></div>
                      </div>
                    </div>
                    <div class="col-lg-2 col-md-2"><?php
                      $ex = explode(',', $value['addon']);
                      
                      $ex1 = explode(',', $value['addonquantity']);
                      $ex2 = explode(',', $value['addon_image']);
                      $ex3 = explode(',', $value['addonqty']);
                      $ex4 = explode(',', $value['addonprice']);
                      
                      /*  foreach ($ex as $key => $a) {
                      
                      if ($a == "") {
                      echo "N/A";
                      continue;
                      }*/
                      /*else {*/
                      if($ex1[0]>0) {
                      ?>
                      <a href="" data-toggle="modal" data-target="#login<?php echo $value['pacorderid']; ?>">
                      Addons</a><?php
                      } else { echo "N/A";}
                      /*}
                      }*/
                      
                      
                      
                      ?>
                      
                      <div id="login<?php echo $value['pacorderid']; ?>" class="modal fade" role="dialog">
                        <div class="modal-dialog" style="width:550px;">
                          <!-- Modal content-->
                          <div class="modal-content">
                            <div class="modal-content">
                              
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">×</button>
                                <div class="model_header">
                                  <div>
                                    <table width="100%" style="line-height: 70px;">
                                      
                                      <tr><th>Image </th> <th>Name</th> <th>Quantity</th> <th>Price</th> </tr>
                                      <?php  for($i=0;$i<count($ex1); $i++) {// p($ex1); ?>
                                      <tr><td>  <?php  if( $ex2[$i]!='') { ?>
                                        <img src="assets/admin/images/<?php echo $ex2[$i]; ?>" class="img-responsive" style="margin: 0 auto;width: 57px;">
                                        <?php } else { ?>
                                        <img src="img/1463897752358.jpeg" class="img-responsive">
                                        <?php } ?> </td> <td> <?php echo $ex[$i]; ?></td> <td><?php echo $ex3[$i]; ?></td> <td><i class="icon-rupee" aria-hidden="true"></i><?php echo $ex4[$i]; ?></td> </tr>
                                        <?php } ?>
                                      </table>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-2 col-md-2">
                        <a href="#" onclick="printDiv('printableArea<?= $value['pacorderid']; ?>')"><img src="<?= base_url('assets/admin/images/pdf.png')?> " alt="PDF" style="width: 33px;"> </a>  </div>
                      </div>
                      <?php } } ?>
                      <?php foreach ($selectaccountorder as $key => $value) { if($value>0) {?>
                      <div id="printableArea<?= $value['pacorderid']; ?>" style="display:none" >
                        <?php
                        $date_array1 = explode("-",  $value['addedon']); // split the array
                        $var_day1 = $date_array1[2]; //day seqment
                        $var_month1 = $date_array1[1]; //month segment
                        $var_year1 = rtrim($date_array1[0]," "); //year segment
                        $new_date_format1 = strtotime("$var_year1-$var_month1-$var_day1"); // join them together
                        $d1 = date(' jS M Y', strtotime($value['addedon']) );
                        //$data[ 'd1' ] = $d1;
                        $date = '19:24:15 ';
                        $d2 = date('h:i:s a ');
                        $date_array = explode("/", $value['departuredate']); // split the array
                        $var_day = $date_array[0]; //day seqment
                        $var_month = $date_array[1]; //month segment
                        $var_year = rtrim($date_array[2]," "); //year segment
                        $new_date_format = strtotime( date('Y-m-d', strtotime($value['departuredate']) ) ); // join them together
                        $input = ("$var_year$var_month$var_day");
                        //p( date('Y-m-d', strtotime($data['orderdisplaydataval']->departuredate) ) );
                        $d3 = date( "D", strtotime( date('Y-m-d', strtotime($value['departuredate']) ) ) ) . "\n";
                        $data['d3'] = $d3 ;
                        //p( $data['orderdisplaydataval'] );
                        $prepare_time_slot_from_date = $value['departuretime'] .':'.$value['frommin'] .' '.$value['txtfromd'] .' - '.
                        $value['tohrs'].':'.$value['tomin'].' '.$value['txttod'];
                        $data['prepare_time_slot_from_date'] = $prepare_time_slot_from_date;
                          //  completeTimeSlot( getTimeSlotInArray( $this->session->userdata('destinationType') ) )
                        $d4 = date(' jS M Y', $new_date_format);
                        //$data['d4'] = $d4;
                        ?>
                        <table width="953" height="967" cellpadding="0" cellspacing="0"  style="border:solid 1px #37ace1;font-family:Arial, Helvetica, sans-serif;color:#555;">
                          <tr>
                            <td colspan="3">
                              <table width="953">
                                <tr>
                                  <td  width="350" valign="top" style="padding:40px 0px 0px 15px !important;">
                                  <img src="<?php echo base_url()?>assets/admin/images/<?php echo $banner->bannerimage_logo; ?>"  alt=""></td>
                                  <td border="0" width="800" style="text-align:center;">
                                    <table width="600" style="margin:20px 20px;line-height:25px;">
                                      <tr>
                                        <td style="color: #37ACE1;font-weight:700;padding:0px 0px;font-size:29px;text-align:center;">
                                          <strong><?php echo $banner->bannerimage_top1; ?></strong>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td style="padding:8px 0px 5px;font-size:18px;font-weight:700;"><strong><?php echo $banner->bannerimage_top2; ?> </strong></td>
                                      </tr>
                                      <tr>
                                        <td style="padding:4px 0px;font-size:22px;font-weight:700;"><strong><?php echo $banner->bannerimage_top3; ?> </strong></td>
                                      </tr>
                                      <tr>
                                        <td style="color: #444;font-weight:700;padding:0px 0px 0px;font-size:22px;"><strong><?php echo $banner->bannerimage_top4; ?> </strong></td>
                                      </tr>
                                      <tr>
                                        <td style="color: #444;font-weight:700;padding:0px 0px 0px;font-size:22px;"><strong><?php echo $banner->bannerimage_gstno; ?> </strong></td>
                                      </tr>
                                      <tr>
                                        <td style="color: #37ACE1;font-weight:600;padding:15px 0px 0px;font-size:25px;"><strong>Booking Confirmation Details</strong></td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                        <tr><td  style="border-top:dashed 3px #37ace1;"><td></tr>
                        <tr>
                          <td colspan="3">
                            <table width="1000" cellpadding="0" cellspacing="0" style="margin:15px 0;">
                              <tr>
                                <td width="475">
                                  <table style="margin:35px 35px 35px 35px;">
                                    <tr>
                                      <td style="line-height:25px;">
                                        <h3 style="font-size:23px;">Venue Details</h3>
                                      </td>
                                    </tr>
                                    <tr><td style="font-weight:normal;line-height:23px;font-size:18px;"><?php echo $branch->branch_add; ?></td></tr>
                                    <tr><td style="font-size:18px;"><span style="color: rgb(79, 193, 242);">Email :</span><?php echo $banner->bannerimage_branch_email; ?></td></tr>
                                    <tr><td style="font-size:18px;"><span style="color: rgb(79, 193, 242);">Phone :</span><?php echo $banner->bannerimage_branch_contact; ?></td></tr>
                                  </table>
                                </td>
                                <td style="border-left:dashed 3px #37ace1;"></td>
                                <td width="475">
                                  <table style="margin:35px 35px 35px 35px;">
                                    <tr>
                                      <td style="line-height:25px;">
                                        <h3 style="font-size:23px;">Guest Details</h3>
                                      </td>
                                    </tr>
                                    <tr><td style="font-weight:normal;line-height:23px;font-size:18px;"><?php  echo $value['billing_name']; ?> <br></td></tr>
                                    <tr><td style="font-size:18px;"><span style="color: rgb(79, 193, 242);">Email :</span> <?php echo $value['billing_email']; ?></td></tr>
                                    <tr><td style="font-size:18px;"><span style="color: rgb(79, 193, 242);">Phone :</span> <?php echo $value['billing_tel']; ?></td></tr>
                                  </table>
                                </td>
                              </tr>
                              <tr>
                                <td colspan="3">
                                  <table width="953" cellpadding="0" cellspacing="0" style="margin:15px 0px;">
                                    <tr>
                                      <td colspan="3" style="border-top:dashed 3px #37ace1;"></td>
                                    </tr>
                                    <tr>
                                      <td><table width="475" style="margin:15px 0;border-right:dashed 3px #37ace1;">
                                        <tr>
                                          <td style="margin-bottom:0;font-size:15px;margin-top:5px;text-align:center;font-weight:bold;">Booking No.</td>
                                          <td style="margin-bottom:0;font-size:15px;margin-top:5px;text-align:center;font-weight:bold;">Booking Date.</td></tr>
                                          <tr>
                                            <td style="text-align:center;margin-top:5px;font-size:13px;"><?php  echo $value['ticketid']; ?> </td>
                                            <td style="text-align:center;margin-top:5px;font-size:13px;"><?php  echo $d1; ?></td></tr></table></td>
                                            <td width="3"></td>
                                            <td>
                                              <table width="475" style="margin:15px 0;">
                                                <tr>
                                                  <td style="margin-bottom:0;font-size:15px;margin-top:5px;text-align:center;font-weight:bold;">Visit Date.</td>
                                                  <td style="margin-bottom:0;font-size:15px;margin-top:5px;text-align:center;font-weight:bold;">Session Time</td>
                                                </tr>
                                                <tr>
                                                  <td style="text-align:center;margin-top:5px;font-size:13px;"><?php  echo $d4; ?>.</td>
                                                  <td style="text-align:center;margin-top:5px;font-size:13px;"><?php echo $prepare_time_slot_from_date;  ?></td>
                                                </tr>
                                              </table>
                                            </td>
                                          </tr>
                                          <tr>
                                            <td colspan="3" style="border-top:dashed 3px #37ace1;"></td>
                                          </tr>
                                        </table>
                                      </td></tr>
                                    </table>
                                  </td>
                                </tr>
                                <tr>
                                  <td colspan="3">
                                    <table width="860" cellpadding="0" cellspacing="0" style="border:solid 1px #37ace1;font-family:Arial, Helvetica, sans-serif;color:#555;margin:0px 50px 10px 50px; ">
                                      <tr>
                                        <td width="215" style="background:#444;text-align:center;font-size:18px;color:#fff;padding:15px 0px;font-weight:bold;">
                                          Items
                                        </td>
                                        <td width="215" style="background:#444;text-align:center;font-size:18px;color:#fff;padding:15px 0px;font-weight:bold;">Qty
                                        </td>
                                        <td width="215" style="background:#444;text-align:center;font-size:18px;color:#fff;padding:15px 0px;font-weight:bold;">Cost</td>
                                        <td width="215" style="background:#444;text-align:center;font-size:18px;color:#fff;padding:15px 0px;font-weight:bold;">Total</td>
                                      </tr>
                                      <?php   //p($orderdisplaydataval);
                                      $total_array = [];
                                      $addon_total_price_with_quantity = $package_total_price = '';
                                      $package_name_array = explode('#',$value['packproductname'] );
                                      $package_img_array = explode('#', $value['packimg'] );
                                      $package_qty_array = explode('#', $value['package_qty'] );
                                      $package_price_array = explode('#',$value['package_price']);
                                      $package_total_price = $total_array[] = getSumAllArrayElement( calculatePriceByQty( explode('#', $value['package_price'] ), explode('#', $value['package_qty'] ) ) );
                                      foreach( $package_name_array as $ky => $val){
                                      ?>
                                      <tr>
                                        <td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px"><?php echo $val . ' Package'; ?> </td>
                                        <td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px"><?php echo $package_qty_array[ $ky ];  ?></td>
                                        <td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px"><?php echo $package_price_array[ $ky ]; ?></td>
                                        <td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px"><?php echo '₹ ' . ( $package_qty_array[ $ky ] * $package_price_array[ $ky ] ) ?> </td>
                                      </tr>
                                      <?php } ?>
                                      <?php
                                      //p($orderaddonedisplaydata);
                                      // $addon_data = getAddonTotalPricePrint( $orderaddonedisplaydata ) ;
                                      //p( $addon_data['addon_price_array'] );
                                      //p($addon_data);
                                      //p($total_array);
                                      $total_array[] = $value['internethandlingcharges'];
                                            //      p($addon_total_price_with_quantity);
                                      // $addon_price_array = $addon_data->addon_price_array;
                                      // $addon_qty_array = $addon_data->addon_qty_array;
                                      $addon_array = explode(',', $value['addon']) ;
                                      $addonquantity_array = explode(',',$value['addonquantity'] ) ;
                                      $addonprice_array = explode(',', $value['addonprice']) ;
                                      $addon_total_price_with_quantity = $total_array[] = getSumAllArrayElement( $addonprice_array );
                                      $total = ($value['promocodeprice']) ?   ( getSumAllArrayElement( $total_array ) - $value['promocodeprice'])  :  getSumAllArrayElement( $total_array );
                                      foreach ($addon_array as $a => $b) {
                                      if($b>0){
                                      if($b!='Something Went Wrong' ){
                                      ?>
                                      <tr><td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px"><?php echo $b; ?></td>
                                      <td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px"><?php echo $addonquantity_array[ $a ]; ?></td>
                                      <td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px"><span><i class="icon-rupee" aria-hidden="true"></i></span><?php echo ( $addonprice_array[ $a ] / $addonquantity_array[ $a ] ); ?></td>
                                      <td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px"><span><i class="icon-rupee" aria-hidden="true"></i></span><?php echo $addonprice_array[ $a ] ; ?></td>
                                    </tr>
                                    <?php  }} } ?>
                                    <tr>
                                      <td colspan="4">
                                        <table width="760" cellpadding="0" cellspacing="0" style="border-top:solid 1px #37ace1;font-family:Arial, Helvetica, sans-serif;color:#555;margin:10px 50px 10px 50px; ">
                                          <tr>
                                            <td width="380" style="text-align:left;font-size:18px;color:#000;padding:10px 0px 10px 40px">Price:</td>
                                            <td width="380" style="text-align:right;font-size:18px;color:#000;padding:10px 40px 10px 0px"><span><i class="icon-rupee" aria-hidden="true"></i></span><?php echo ( $package_total_price + $addon_total_price_with_quantity ); ?></td>
                                          </tr>
                                        </table>
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                              <tr>
                                <td colspan="3">
                                  <table width="860" cellpadding="0" cellspacing="0" style="border:solid 1px #37ace1;font-family:Arial, Helvetica, sans-serif;color:#555;margin:0px 50px 10px 50px; ">
                                    <tr><td width="430" style="text-align:left;font-size:18px;color:#000;padding:10px 0px 10px 40px">Discount Amount</td><td width="430" style="text-align:right;font-size:18px;color:#000;padding:10px 40px 10px 0px"><span><i class="icon-rupee" aria-hidden="true"></i></span><?php echo $value['promocodeprice']; ?></td></tr>
                                  </table>
                                </td>
                              </tr>
                              <tr>
                                <td colspan="3">
                                  <table width="860" cellpadding="0" cellspacing="0" style="border:solid 1px #37ace1;font-family:Arial, Helvetica, sans-serif;color:#555;margin:0px 50px 10px 50px; ">
                                    <tr><td width="430" style="text-align:left;font-size:18px;color:#000;padding:10px 0px 10px 40px">Internet Handling Charges</td><td width="430" style="text-align:right;font-size:18px;color:#000;padding:10px 40px 10px 0px"><span><i class="icon-rupee" aria-hidden="true"></i></span><?php echo $value['internethandlingcharges'] ; ?></td></tr>
                                  </table>
                                </td>
                              </tr>
                              <tr>
                                <td colspan="3">
                                  <table width="860" cellpadding="0" cellspacing="0" style="border:solid 1px #37ace1;font-family:Arial, Helvetica, sans-serif;color:#555;margin:0px 50px 30px 50px; ">
                                    <tr><td width="430" style="text-align:left;font-size:18px;color:#000;padding:10px 0px 10px 40px">Total</td><td width="430" style="text-align:right;font-size:18px;color:#000;padding:10px 40px 10px 0px"><span><i class="icon-rupee" aria-hidden="true"></i></span><?php echo $total; ?></td></tr>
                                  </table>
                                </td>
                              </tr>
                              <tr>
                                <td colspan="3">
                                  <table width="475" style="margin:15px 0px 15px 50px;">
                                    <tr><td style="font-size:17px;font-weight:bold;color:#000;padding:4px 0px;"></td></tr>
                                    <tr><td style="font-size:17px;font-weight:bold;color:#000;padding:4px 0px;">PAN No. : AAFFC9989B</td></tr>
                                    <tr><td style="font-size:17px;font-weight:bold;color:#000;padding:4px 0px;">Encl. Terms and Conditions</td></tr>
                                  </table>
                                </td>
                              </tr>
                            </table>
                            <br>
                            <br>  <br>  <br>  <br>  <br>
                            <table width="953" cellpadding="0" cellspacing="0"  style="border:solid 1px #37ace1;font-family:Arial, Helvetica, sans-serif;color:#555;">
                              <tr>
                                <td>
                                  <table>
                                    <tr>
                                      <td width="150" valign="top" style="padding:15px 0px 0px 15px;"><img src="assets/admin/images/<?php echo $banner->bannerimage_logo; ?>"  alt="" width="220"></td>
                                      <td style="text-align:center;">
                                        <table width="653" style="margin:20px 20px;line-height:25px;">
                                          <tr>
                                            <td width="653" style="color: #37ACE1;font-weight:700;padding:0px 0px;font-size:29px;text-align:center;">
                                              <strong><?php echo $banner->bannerimage_top1; ?></strong>
                                            </td>
                                          </tr>
                                          <tr><td style="padding:8px 0px 5px;font-size:18px;font-weight:700;"><strong><?php echo $banner->bannerimage_top2; ?></strong></td></tr>
                                          <tr><td style="color: #37ACE1;font-weight:600;padding:15px 0px 0px;font-size:25px;"><strong>Terms and Conditions</strong></td></tr>
                                        </table>
                                      </td>
                                      <td width="150" valign="top" style="padding:15px 15px 0px 0px;"><img src="assets/admin/images/<?php echo $banner->bannerimage_logo; ?>"  alt="" width="220"></td>
                                    </tr>
                                    <tr>
                                      <td colspan="3">
                                        <table width="940" style="margin-left:50px;margin-top:10px;margin-bottom:80px;color:#000;font-size:16px;line-height:25px;">
                                          <?php  $j=1;foreach ($tearmsgatway as $key => $value11) {  ?>
                                          <tr>
                                            <td width="30" valign="top"><?php echo  $j; ?>.</td>
                                            <td width="900" valign="top"><?php echo $value11->term_name; ?></td>
                                          </tr>
                                          <?php  $j++;} ?>
                                          <tr>
                                            <td width="30" valign="top">&nbsp;</td>
                                            <td width="900" valign="top">
                                              <table style="font-size:16px;line-height:25px;">
                                              </table>
                                            </td>
                                          </tr>
                                        </table>
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                            </table>
                          </div>
                          <?php }} ?>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title">
                      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Completed
                        Sessions<i class="indicator icon-angle-circled-down pull-right"></i>
                      </a>
                      </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse">
                      <div class="panel-body text-center"><div class="order_summary">
                        <div class="order_titile">
                          
                          
                          <div class="col-lg-3 col-md-3"><div class="text_d">Package</div></div>
                          
                          <div class="col-lg-2 col-md-2"><div class="text_d">Total Price</div></div>
                          
                          
                        </div>
                        <?php foreach ($selectordercompses as $key => $value) {
                        if($value>0) {
                        ?>
                        
                        <div class="summary_product">
                          <div class="col-lg-3 col-md-3">
                            <div class="product_text">
                              <div class="product_p"> <?php echo $value['pacorderid']; ?>  <?php echo implode(',', explode('#',$value['packproductname'])); ?> </div>
                              
                            </div>
                          </div>
                          <div class="col-lg-2 col-md-2">
                            <div class="product_text">
                              <div class="product_rs"><i class="icon-rupee" aria-hidden="true"></i><?php echo $value['total']; ?></div>
                            </div>
                          </div>
                          <div class="col-lg-2 col-md-2"><?php
                            
                            $ex = explode(',', $value['addon']);
                            
                            $ex1 = explode(',', $value['addonquantity']);
                            $ex2 = explode(',', $value['addon_image']);
                            $ex3 = explode(',', $value['addonqty']);
                            $ex4 = explode(',', $value['addonprice']);
                            /*foreach ($ex as $key => $a) {
                            
                            if ($a == "") {
                            echo "N/A";
                            continue;
                            }
                            else {*/ ?>
                            <a href="" data-toggle="modal" data-target="#login<?php echo $value['pacorderid']; ?>">Addons</a><?php
                            /*  }
                            }*/
                            
                            ?>
                            
                            <div id="login<?php echo $value['pacorderid']; ?>" class="modal fade" role="dialog">
                              <div class="modal-dialog" style="width:550px;">
                                <!-- Modal content-->
                                <div class="modal-content">
                                  <div class="modal-content">
                                    
                                    
                                    
                                    
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal">×</button>
                                      
                                      <div class="model_header">
                                        
                                        <div>
                                          <table width="100%" style="line-height: 70px;">
                                            
                                            <tr><th>Image </th> <th>Name</th> <th>Quantity</th> <th>Price</th> </tr>
                                            
                                            <?php  for($i=0;$i<count($ex1); $i++) { ?>
                                            
                                            <tr><td>  <?php  if( $ex2[$i]!='') { ?>
                                              <img src="assets/admin/images/<?php echo $ex2[$i]; ?>" class="img-responsive" style="margin: 0 auto;width: 57px;">
                                              <?php } else { ?>
                                              <img src="img/1463897752358.jpeg" class="img-responsive">
                                              <?php } ?> </td> <td> <?php echo $ex[$i]; ?></td> <td><?php echo $ex3[$i]; ?></td> <td><i class="icon-rupee" aria-hidden="true"></i><?php echo $ex4[$i]; ?></td> </tr>
                                              

                                              
                                              <?php } ?>
                                              
                                            </table>
                                          </div>

                                          
                                        </div>

                                      </div>
                                    </div>
                                    
                                  </div>
                                </div>
                              </div>
                              
                            </div>
                          </div>
                          <?php }} ?>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </html><html xmlns="http://www.w3.org/1999/xhtml">
            <div class="tab-pane" id="profile">
              <form id="loginform" name="loginform" method="post" action="" enctype="">
                <input type="hidden" name="loginform" value="loginform" />
                <span id="loginform:updateMyProfile">
                  <div class="row">
                    <div class="col-md-12">
                      
                      
                      <div class="form-group">
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>First Name</label>
                              <input name="firstName" type="text" value="<?php echo $selectacc->user_firstname; ?>" placeholder="Enter Your First Name" pattern="[A-Za-z\s]+" class="form-control" role="textbox" aria-disabled="false" aria-readonly="false" required title="First Name should only contain  Alphabet. e.g. John">
                            </div>
                          </div>
                          
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Last Name</label>
                              <input  name="lastName" type="text" value="<?php echo $selectacc->user_lastname; ?>" placeholder="Enter Your Last Name"  pattern="[A-Za-z\s]+" class="form-control" role="textbox" aria-disabled="false" aria-readonly="false" title="Last Name should only contain  Alphabet. e.g. John">
                            </div>
                          </div>
                        </div>
                        
                        
                        
                        
                        
                        
                      </div>
                      
                      
                      
                      <div class="form-group">
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Date of Birth</label>
                              <input class="date-pick form-control formmm1" data-date-format="dd/mm/yyyy" type="text" id="datepicker1" name="myprofiledob" placeholder="Enter Date Of Birth"  value="<?php echo $selectacc->user_dob; ?>"  required title="Enter Date Of Birth">
                            </div>
                          </div>
                          
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Email Address</label>
                              <input name="email" type="text" id="email"  value="<?php echo $selectacc->user_emailid; ?>" placeholder="Enter Your Email Address" class="form-control" role="textbox" aria-disabled="false" aria-readonly="false" required readonly>
                            </div>
                          </div>
                        </div>
                        
                      </div>
                      
                      
                      <div class="form-group">
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Mobile Number</label>
                              <input required name="mobile" type="text" value="<?php echo $selectacc->user_mobileno; ?>" placeholder="Enter Your Mobile Number" minlength="10" maxlength="10" class="form-control" role="textbox" aria-disabled="false" aria-readonly="false"  pattern="^\d{10}$" title="Your Phone Number Should Only Contain  Number. e.g. 9999999999" onkeypress="return isNumberKey(event)" >
                            </div>
                          </div>
                        </div>
                        
                      </div>
                      
                      
                      <div class="form-group">
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                              <label>Address</label>
                              <textarea required  name="address" cols="20" rows="3" placeholder="Enter you Address" class="form-control" role="textbox" aria-disabled="false" aria-readonly="false" aria-multiline="true"><?php echo $selectacc->user_Address; ?></textarea>
                            </div>
                          </div>
                        </div>
                        
                      </div>
                      
                      
                      <div class="form-group">
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Country</label>
                              <input required  name="countryName" type="text" class="form-control" autocomplete="off" placeholder="Enter Your Country" value="<?php echo $selectacc->user_country; ?>" role="textbox" aria-disabled="false" aria-readonly="false" aria-autocomplete="listbox" pattern="[A-Za-z\s]+"  title="Country Name should only contain  letters. e.g. India" >
                            </div>
                          </div>
                          
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>State</label>
                              <input  required name="stateName" type="text" class="form-control" autocomplete="off" placeholder="Enter Your State" value="<?php echo $selectacc->user_state; ?>"  role="textbox" aria-disabled="false" aria-readonly="false" required  pattern="[A-Za-z\s]+"  title="State Name should only contain  letters. e.g. India">
                            </div>
                          </div>
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>City</label>
                              <input required  name="cityName" type="text" class="form-control" autocomplete="off" placeholder="Enter Your City" value="<?php echo $selectacc->user_city; ?>" role="textbox" aria-disabled="false" aria-readonly="false" aria-autocomplete="listbox" required  pattern="[A-Za-z\s]+"  title="City Name should only contain  letters. e.g. India">
                            </div>
                          </div>
                          
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Pin Code</label>
                              <input required  name="pincode" type="text" value="<?php echo $selectacc->user_pincodes; ?>" placeholder="Enter Your Pin Code" minlength="6" maxlength="6"class="form-control" role="textbox" aria-disabled="false" aria-readonly="false" required  onkeypress="return isNumberKey(event)"

                              >
                            </div>
                          </div>
                        </div>
                      </div>
                      
                      
                      
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <button  name="submit" class="btn_1"  type="submit" role="button" aria-disabled="false" value="profile"><span class="ui-button-text ui-c">Submit</span></button>
                      
                      
                    </div>
                  </div></span>
                </form>
              </div>
            </html><html xmlns="http://www.w3.org/1999/xhtml">
            <div class="tab-pane" id="password">
              <form id="change_password_form" name="change_password_form" method="post" action="" enctype="">
                <input type="hidden" name="change_password_form" value="change_password_form" />
                <span id="change_password_form:changePassword">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Current Password</label>
                        <input required name="currentpassword" type="password" class="form-control" placeholder="Enter Your Current Password" role="textbox" aria-disabled="false" aria-readonly="false" maxlength="8">
                      </div>
                      <div class="form-group">
                        <label>New Password</label><input  name="newpassword" type="password" class="form-control" placeholder="Enter Your New Password" role="textbox" aria-disabled="false" aria-readonly="false" maxlength="8">
                      </div>
                      <div class="form-group">
                        <label>Confirm Password</label><input  name="confirmpassword" type="password" class="form-control" placeholder="Enter Your Confirm Password" role="textbox" aria-disabled="false" aria-readonly="false" required maxlength="8">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <button  name="submit" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only btn_1"  type="submit" role="button" aria-disabled="false" value="changePswrd"><span class="ui-button-text ui-c">Change Password</span></button>
                    </div>
                    <div class="col-md-6"><div id="change_password_form:j_idt97" class="ui-messages ui-widget" aria-live="polite"></div>
                  </div>
                </div></span>
              </form>
            </div>
          </div>
        </div>
      </html>
    </div>
  </div>
</div>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<div id="boxes">
  <div id="dialog" class="window">
    <div class="form-group subs-for">
      <lable>Enter your email</lable>
      <input type="text" name="emailid" id="emailid" required autofocus />
      <input type="button" value="submit"  onclick="savemail(this.value);"/>
      <label for=""  class="emailmsg" style="float: left;"></label>
    </div>
  </div>
  <div id="mask"></div>
</div>

<script>
    $(function() {
        $.noConflict();
        $("#datepicker1").datepicker({
            dateFormat: 'dd-mm-yy',
            // minDate: 0,
            maxDate: "-1M "
        });
    });

    function isNumberKey(evt){
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }


</script>