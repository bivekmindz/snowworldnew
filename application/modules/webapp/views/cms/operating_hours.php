

<div class="container margin_30">
    <div class="col-md-12 box_style_1">
      <h3 class="scifiheader"><i class="icon-clock"></i> Operating Hours</h3>
      <div class="col-md-4 table-responsive">
              <table id="operation" class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <th>Start Time</th>
                      <th>End Time</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>11:00 AM</td>
                      <td>12:30 PM</td>
                    </tr>
                    <tr>
                      <td>01:00 PM</td>
                      <td>02:30 PM</td>
                    </tr>
                    <tr>
                      <td>03:00 PM</td>
                      <td>04:30 PM</td>
                    </tr>
                    <tr>
                      <td>05:00 PM</td>
                      <td>06:30 PM</td>
                    </tr>
                    <tr>
                      <td>07:00 PM</td>
                      <td>08:30 PM</td>
                    </tr>
                    <tr>
                      <td>09:00 PM</td>
                      <td>10:30 PM</td>
                    </tr> 
                  </tbody>
                </table>     
            </div>
            
            <div class="col-md-4">
              <div class="box_style_1">
                    <h3 class="scifiheader"><i class="icon-ticket"></i> Ticket Price</h3>
                    <p class="footer_address">Ticket Type: Regular<br>Ticket Cost: <i class="icon-rupee"></i> 1,150/- (<i class="icon-rupee"></i> 1,000 + Taxes)</p>
                    <h3 class="scifiheader"><i class="icon-clock"></i> Total Duration</h3>
                    <p class="footer_address">1 Hour​ 30 mins​ <br>(Snow Room : ​1 hour​ + Reception : ​30 mins)</p>
                </div> 
            </div>
            
            <div class="col-md-4"> 
                <div class="box_style_2">
          <i class="icon-phone"></i>
          <h4>Need help? Call us</h4>
          <a href="tel://01202595150" class="phone">120 259 51 50</a>
          <a href="tel://01202595151" class="phone">120 259 51 51</a>
          <a href="tel://01202595152" class="phone">120 259 51 52</a>
          <a href="tel://01202595153" class="phone">120 259 51 53</a>
          <a href="tel://01202595154" class="phone">120 259 51 54</a>
          <small>Monday to Sunday 11.00am - 10.00pm</small>
        </div>
            </div><!-- End col-md-4 -->
      </div>      
    </div>  
    