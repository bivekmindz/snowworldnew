<?php
if(!empty($_POST))
{
   $this->session->unset_userdata('sesregister');
}
?>
    <div class="wrapper fix_mar" style="position: relative;">
        <div class="navigation">
            <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12" style="position: inherit;">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-5 col-xs-6">
                        <div class="row_left">
                            <ul>
                                <li style="width:100%;"><a class="ceteg" href="#">Top categories</a></li>
                            </ul>
                        </div>

                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-7 col-xs-6" style="position: inherit;">
                        <ul>

                              <li class="mobile_h"><a href="<?php echo base_url();?>creditpurchase">Credit Purchase</a></li>
                            <!-- <li><a href="#">Special Deals</a></li> -->
                            <li><a href="<?php echo base_url();?>allcategory">More Categories</a>
                            <li><a href="<?php echo base_url();?>allbrands">Shop By Brands</a>
                            <li><a href="<?php echo base_url();?>stationery">Stationery</a></li>
                            <li><a href="<?php echo base_url();?>combo/combolist">Super Saver</a></li>
                          </li>
                            

                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="wrapper" style="position:relative;">
        <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12">
            <div class="row index_n">
                <?php
                    include("helper/nav.php"); //Left navigation
                ?>
                <div class="col-lg-9 col-md-12 col-sm-12">
                    <div class="row_left2">

                       <!-- Slider Div -->
                        <div class="slider">
                            <!--<img src="images/banner33.jpg" />-->

                            <div id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 1020px; height: 450px; overflow: hidden; visibility: hidden;">
                                <!-- Loading Screen -->
                                <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
                                    <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
                                    <div style="position:absolute;display:block;background:url('<?php echo base_url();?>assets/webapp/img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
                                </div>
                                <div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 1020px; height: 450px; overflow: hidden;">
                                <?php foreach ($mainslide as $key => $value) { ?>
                                    <div data-p="112.50" style="display: none;">
                                        <a href="<? echo  $value['t_path']?>"><img data-u="image" src="<?php echo base_url();?>assets/webapp/img/<?php echo $value['t_imgname']?>" /></a>
                                    </div> 
                                <?php } ?>
                                </div>
                                <!-- Bullet Navigator -->
                                <div data-u="navigator" class="jssorb05" style="bottom:16px;right:16px;" data-autocenter="1">
                                    <!-- bullet navigator item prototype -->
                                    <div data-u="prototype" style="width:16px;height:16px;"></div>
                                </div>
                                <!-- Arrow Navigator -->
                                <span data-u="arrowleft" class="jssora12l" style="top:0px;left:0px;width:30px;height:46px;" data-autocenter="2"></span>
                                <span data-u="arrowright" class="jssora12r" style="top:0px;right:0px;width:30px;height:46px;" data-autocenter="2"></span>
                                <a href="http://www.jssor.com" style="display:none">Slideshow Maker</a>
                            </div>
                            <script>
                                jssor_1_slider_init();
                            </script>
                        </div>
                        <!-- End Slider Div -->
                    </div>

                </div>
            </div>
            <div class="row_left2">
                <div class="div_sep2" style="margin-top:10px;">

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><h1 class="b_selec">snowworld Selection</h1></div>
                    <div style="float:left;width:100%">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                            <div class="row_right">
                                <div class="slice_b">
                                    <a class="browse_n" href="<?php echo $selectimgone->t_path;?>">Browse Product</a>
                                    <div class="p_name"><h3><?php echo $selectimgone->t_heading;?><br /><span><?php echo $selectimgone->t_subheading;?></span></h3></div>
                                    <a href="<?php echo $selectimgone->t_path;?>">
                                        <img src="<?php echo base_url();?>assets/webapp/images/<?php echo $selectimgone->t_imgname;?>" alt="<?php echo $selectimgone->t_imgaltname; ?>" />
                                    </a>
                                </div>

                            </div>

                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <div class="">
                                <div class="slice_a">
                                    <a class="browse_n" href="<?php echo $selectimgtwo->t_path;?>">Browse Product</a>
                                    <div class="p_name"><h3><?php echo $selectimgtwo->t_heading;?><br /><span><?php echo $selectimgtwo->t_subheading;?></span></h3></div>
                                    <a href="<?php echo $selectimgtwo->t_path;?>">
                                        <img src="<?php echo base_url();?>assets/webapp/images/<?php echo $selectimgtwo->t_imgname;?>" alt="<?php echo $selectimgtwo->t_imgaltname; ?>" />
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                            <div class="row_left">
                                <div class="slice_b">
                                    <a class="browse_n" href="<?php echo $selectimgthree->t_path;?>">Browse Product</a>
                                    <div class="p_name"><h3><?php echo $selectimgthree->t_heading;?><br /><span><?php echo $selectimgthree->t_subheading;?></span></h3></div>
                                    <a href="<?php echo $selectimgthree->t_path;?>">
                                        <img src="<?php echo base_url();?>assets/webapp/images/<?php echo $selectimgthree->t_imgname;?>" alt="<?php echo $selectimgthree->t_imgaltname; ?>" />
                                    </a>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div style="float:left;width:100%;margin-top:4px">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                           <a href="<?php echo $selectimgfour->t_path;?>">
                            <img src="<?php echo base_url();?>assets/webapp/images/<?php echo $selectimgfour->t_imgname;?>" alt="<?php echo $selectimgfour->t_imgaltname; ?>" class="img-responsive" />
                            </a>
                        </div>
                    </div>


                </div>
            </div>

            


        </div>
        <div class="col-lg-2 hidden-md hidden-sm hidden-xs">
            <div class="row">
                <div class="right_side_b">
                <?php foreach ($rightban as $key => $value) { ?>
                    <a href="<?php echo $value['t_path'];?>"> <img src="<?php echo base_url();?>assets/webapp/images/<?php echo $value['t_imgname'];?>" alt="<?php echo $value['t_imgaltname']; ?>" /></a>
                <?php } ?>
                </div>
            </div>
        </div>
        <div class="div_sep_p">
            <div id="main">
            <div class="stick_nav_vis"></div>
            <div class="col-lg-1 hidden-md hidden-sm hidden-xs">
            <div class="stick_nav">
            <?php $j=0; foreach ($iconimg as $key => $value) {  ?>
            <div class="lef_icon n<?php echo $key;?>">
            <a href="javascript:void();" onclick="$('#div_id<?php echo $key;?>').animatescroll({ scrollSpeed: 100 });">
               </a></div>
            <?php $j++; 
            if($j==10){
                break;
                } } ?>
                <!-- <img src="<?php echo base_url();?>assets/webapp/images/lef.jpg" class="lef_icon" /> -->
                </div>
            </div>
            <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12">
            <div class="st_close"></div>
                <div class="row_sty">
               
                    <!-- <div class="div_sep white2">
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="row">
                                        <div class="categ_n">
                                            <ul>
                                                <li class="cat_n"><a href="#">Electronic Toys</a></li>
                                                <li><a href="#">Electronic Toys</a></li>
                                                <li><a href="#">Electronic Toys</a></li>
                                                <li><a href="#">Electronic Toys</a></li>
                                                <li><a href="#">Electronic Toys</a></li>
                                                <li><a href="#">Electronic Toys</a></li>
                                                <li><a href="#">Electronic Toys</a></li>
                                                <li><a href="#">Electronic Toys</a></li>
                                                <li><a href="#">Electronic Toys</a></li>
                                                <li><a href="#">Electronic Toys</a></li>
                                                <li><a href="#">Electronic Toys</a></li>
                                                <li><a href="#">Electronic Toys</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-8">
                                    <div class="row">
                                        <div class="cat_img">
                                            <div class="slides">

                                                <ul>

                                                    <li class="slide">
                                                        <img src="<?php echo base_url();?>assets/webapp/images/cate_img.jpg" />
                                                       
                                                    </li>
                                                    <li class="slide">

                                                        <img src="<?php echo base_url();?>assets/webapp/images/cate_img.jpg" />
                                                    </li>
                                                    <li class="slide">
                                                        <img src="<?php echo base_url();?>assets/webapp/images/cate_img.jpg" />
                                                     
                                                    </li>

                                                </ul>
                                            </div>
                                            <div class="btn-bar">
                                                <div id="buttons"><a id="prev" class="fa fa-arrow-left" href="#"></a><a id="next" href="#" class="fa fa-arrow-right"></a> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="other_pro">
                                    <ul>
                                        <li>
                                            <a href="#">
                                                <div class="text_b">
                                                    Chicco Billy Bigwheels
                                                    <span> Billy Bigwheels</span>
                                                </div>
                                                <div class="img_pro">
                                                    <img src="<?php echo base_url();?>assets/webapp/images/pro_img.jpg" />
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <div class="text_b">
                                                    Chicco Billy Bigwheels
                                                    <span> Billy Bigwheels</span>
                                                </div>
                                                <div class="img_pro">
                                                    <img src="<?php echo base_url();?>assets/webapp/images/pro_img.jpg" />
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <div class="text_b">
                                                    Chicco Billy Bigwheels
                                                    <span> Billy Bigwheels</span>
                                                </div>
                                                <div class="img_pro">
                                                    <img src="<?php echo base_url();?>assets/webapp/images/pro_img.jpg" />
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <div class="text_b">
                                                    Chicco Billy Bigwheels
                                                    <span> Billy Bigwheels</span>
                                                </div>
                                                <div class="img_pro">
                                                    <img src="<?php echo base_url();?>assets/webapp/images/pro_img.jpg" />
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <div class="text_b">
                                                    Chicco Billy Bigwheels
                                                    <span> Billy Bigwheels</span>
                                                </div>
                                                <div class="img_pro">
                                                    <img src="<?php echo base_url();?>assets/webapp/images/pro_img.jpg" />
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <div class="text_b">
                                                    Chicco Billy Bigwheels
                                                    <span> Billy Bigwheels</span>
                                                </div>
                                                <div class="img_pro">
                                                    <img src="<?php echo base_url();?>assets/webapp/images/pro_img.jpg" />
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div> -->
                
                    

                 <?php 
                        $o=0;
                        foreach ($sectionmasterview as $key => $value) {
                         $o++;
                         
               //$mainCatUrl = base_url().'category/'.str_replace('&','en',$value['catname']).'-'.$value['catid'].'.html';
               $mainCatUrl = base_url().$value['url'];
                 ?>
                   <div class="page_div_item" id="div_box<?php echo $key;?>">
                   <div class="link_clk" id="div_id<?php echo $key;?>"></div>
                    <div class="item_div div_sep white2">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                    <div class="row">
                                        <div class="categ_n">
                                            <ul>
                                                <li class="cat_n"><a href="<?=$mainCatUrl?>"><?php echo $value['catname'];?></a></li>
                                                <?php 
                                                foreach ($sectiondetailview as $k => $val) { 

                                                $subCatUrl = base_url().'category/'.strtolower(str_replace(" ","-",str_replace('&','en',$val['imgurl']))).'-'.$val['imgname'].'.html';

                                                if($value['sid']==$val['sid'] && $val['mediatype']!='slider' && $val['mediatype']!='banner'){
                                                ?>    
                                                <li><a href="<?=$subCatUrl?>"><?php echo $val['imgurl'];?></a></li>

                                                <?php } } ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                    <div class="row">
                                        <div class="cat_img">
                                            <div id="myCarousel<?php echo $o;?>" class="carousel slide">
                                                <!-- Indicators -->
                                                <ol class="carousel-indicators">
                                                    <li data-target="#myCarousel<?php echo $o;?>" data-slide-to="0" class="active"></li>
                                                    <li data-target="#myCarousel<?php echo $o;?>" data-slide-to="1"></li>
                                                    <li data-target="#myCarousel<?php echo $o;?>" data-slide-to="2"></li>
                                                      <li data-target="#myCarousel<?php echo $o;?>" data-slide-to="3"></li>
                                                </ol>

                                                <!-- Wrapper for Slides -->
                                                <div class="carousel-inner">
                                                 <?php $cnt = 1; 
                                                foreach ($sectiondetailview as $k => $val) { 
                                                if($value['sid']==$val['sid'] && $val['mediatype']=='slider'){
                                                ?> 
                                                    <div class="item <?php echo $cnt == 1 ? 'active' : ''; ?>">
                                                        <!-- Set the first background image using inline CSS below. -->
                                                        <div class="fill"> <a href="<?php echo $val['imgurl'];?>">
                                                        <img src="<?php echo base_url();?>assets/sectionimages/<?php echo $val['imgname'];?>" alt="<?php echo $val['imgaltname']; ?>" /></div>
                                                     <!--   <div class="carousel-caption">
                                                            <h2>Caption 1</h2>
                                                        </div>-->
                                                    </div>
                                                  
                                                  <?php $cnt++; } } ?>
                                                </div>

                                                <!-- Controls -->
                                                <a class="left carousel-control" href="#myCarousel<?php echo $o;?>" data-slide="prev">
                                                    <span class="icon-prev"></span>
                                                </a>
                                                <a class="right carousel-control" href="#myCarousel<?php echo $o;?>" data-slide="next">
                                                    <span class="icon-next"></span>
                                                </a>

                                            </div>
                                          <!--  <div class="slides<?=$o?>">

                                                <ul>
                                                <?php 
                                                foreach ($sectiondetailview as $k => $val) { 
                                                if($value['sid']==$val['sid'] && $val['mediatype']=='slider'){
                                                ?> 

                                                <li class="slide">
                                                    <a href="<?php echo $val['imgurl'];?>">
                                                        <img src="<?php echo base_url();?>assets/sectionimages/<?php echo $val['imgname'];?>" />
                                                    </a>
                                                </li>        

                                                <?php } } ?>
                                                   
                                                </ul>
                                            </div>
                                            <div class="btn-bar">
                                                <div id="buttons"><a id="prev<?=$o?>" class="fa fa-arrow-left" href="#"></a><a id="next<?=$o?>" href="#" class="fa fa-arrow-right"></a> </div>
                                            </div>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 hidden-sm hidden-xs">
                            <div class="row">
                                <div class="other_pro">
                                    <ul>

                                    <?php 
                                        foreach ($sectiondetailview as $k => $val) { 
                                        if($value['sid']==$val['sid'] && $val['mediatype']=='banner'){
                                    ?>
                                        <li>
                                            <a href="<?php echo $val['imgurl'];?>">
                                                <div class="text_b">
                                                    <?php echo $val['imghead'];?>
                                                    <span> <?php echo $val['imgsubhead'];?></span>
                                                </div>
                                                <div class="img_pro">
                                                    <img src="<?php echo base_url();?>assets/sectionimages/<?php echo $val['imgname'];?>" />
                                                </div>
                                            </a>
                                        </li>

                                    <?php } } ?>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div id="vi_sty<?php echo $key;?>" class="item_n" title="n<?php echo $key;?>"></div>
                        <div class="item_b s<?php echo $key;?>"></div>
                    </div>
                    </div>
                    

 <!-- <style type="text/css">
        .slides<?=$o?> {
            overflow: hidden;
            position: relative;
            width: 100%;
            height: auto;
        }

        .btn-bar {
            float: right;
            padding-bottom: 6px;
            position:absolute;
            top:48%;
            width:100%;
            display:none;
        }
        .cat_img:hover .btn-bar{
            display:block;

        }

            .btn-bar a {
                color: #fff;
                font-size: 19px;
                margin-right: 5px;
                text-decoration:none;
                padding:10px 10px;
                background:#6F6F6F;
            }
            .btn-bar a#next<?=$o?>{
                position:absolute;
                right:-5px;
            }
            .btn-bar a#prev<?=$o?>{
                position:absolute;
                    left: 0px;
            }

        .slides<?=$o?> ul {
            list-style: none;
            width: 100%;
            height: auto;
            margin: 0;
            padding: 0;
            position: relative;
        }

        .slides<?=$o?> li {
            width: 100%;
            height: auto;
            float: left;
            position: relative;
        }
    </style>


    <script>
        function randomIntFromInterval(min,max){
            return Math.floor(Math.random()*(max-min+1)+min);
        }
        $(document).ready(function () {
            //rotation speed and timer
            var speed = randomIntFromInterval(3000,6000);

            var run = setInterval(rotate, speed);
            var slides = $('.slide');
            var container = $('.slides<?=$o?> ul');
            var elm = container.find(':first-child').prop("tagName");
            var item_width = container.width();
            var previous = 'prev<?=$o?>'; //id of previous button
            var next = 'next<?=$o?>'; //id of next button
            slides.width(item_width); //set the slides to the correct pixel width
            container.parent().width(item_width);
            container.width(slides.length * item_width); //set the slides container to the correct total width
            container.find(elm + ':first').before(container.find(elm + ':last'));
            resetSlides();


            //if user clicked on prev button

            $('#buttons a').click(function (e) {
                //slide the item

                if (container.is(':animated')) {
                    return false;
                }
                if (e.target.id == previous) {
                    container.stop().animate({
                        'left': 0
                    }, 1500, function () {
                        container.find(elm + ':first').before(container.find(elm + ':last'));
                        resetSlides();
                    });
                }

                if (e.target.id == next) {
                    container.stop().animate({
                        'left': item_width * -2
                    }, 1500, function () {
                        container.find(elm + ':last').after(container.find(elm + ':first'));
                        resetSlides();
                    });
                }

                //cancel the link behavior
                return false;

            });

            //if mouse hover, pause the auto rotation, otherwise rotate it
            container.parent().mouseenter(function () {
                clearInterval(run);
            }).mouseleave(function () {
                run = setInterval(rotate, speed);
            });


            function resetSlides() {
                //and adjust the container so current is in the frame
                container.css({
                    'left': -1 * item_width
                });
            }

        });
        //a simple function to click next link
        //a timer will call this function, and the rotation will begin

        function rotate() {
            $('#next<?=$o?>').click();
        }
    </script>-->
 <script src="<?php echo base_url();?>/assets/js/jquery.js"></script>
    
    <!-- Bootstrap Core JavaScript -->
   
    <script src="<?php echo base_url();?>/assets/webapp/bootstrap/js/bootstrap.min.js"></script>

    <!-- Script to Activate the Carousel -->
    <script>
        $('.carousel').carousel({
            interval: 5000 //changes the speed
        })
    </script>
                    <?php } ?>


                    <!-- <div class="div_sep white2">
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="row">
                                        <div class="categ_n">
                                            <ul>
                                                
                                                <li><a href="#"><img src="<?php echo base_url();?>assets/webapp/images/c_logo.jpg" /></a></li>
                                                
                                                <li><a href="#"><img src="<?php echo base_url();?>assets/webapp/images/c_logo2.jpg" /></a></li>
                                                <li><a href="#"><img src="<?php echo base_url();?>assets/webapp/images/c_logo3.jpg" /></a></li>
                                                <li><a href="#"><img src="<?php echo base_url();?>assets/webapp/images/c_logo4.jpg" /></a></li>
                                                <li><a href="#"><img src="<?php echo base_url();?>assets/webapp/images/c_logo.jpg" /></a></li>
                                                <li><a href="#"><img src="<?php echo base_url();?>assets/webapp/images/c_logo2.jpg" /></a></li>
                                                <li><a href="#"><img src="<?php echo base_url();?>assets/webapp/images/c_logo3.jpg" /></a></li>
                                                
                                                
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-8">
                                    <div class="row">
                                        <div class="cat_img">
                                            <img src="<?php echo base_url();?>assets/webapp/images/cate_img.jpg" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="other_pro">
                                    <ul>
                                        <li>
                                            <a href="#">
                                                <div class="text_b">
                                                    Chicco Billy Bigwheels
                                                    <span> Billy Bigwheels</span>
                                                </div>
                                                <div class="img_pro">
                                                    <img src="<?php echo base_url();?>assets/webapp/images/pro_img.jpg" />
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <div class="text_b">
                                                    Chicco Billy Bigwheels
                                                    <span> Billy Bigwheels</span>
                                                </div>
                                                <div class="img_pro">
                                                    <img src="<?php echo base_url();?>assets/webapp/images/pro_img.jpg" />
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <div class="text_b">
                                                    Chicco Billy Bigwheels
                                                    <span> Billy Bigwheels</span>
                                                </div>
                                                <div class="img_pro">
                                                    <img src="<?php echo base_url();?>assets/webapp/images/pro_img.jpg" />
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <div class="text_b">
                                                    Chicco Billy Bigwheels
                                                    <span> Billy Bigwheels</span>
                                                </div>
                                                <div class="img_pro">
                                                    <img src="<?php echo base_url();?>assets/webapp/images/pro_img.jpg" />
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <div class="text_b">
                                                    Chicco Billy Bigwheels
                                                    <span> Billy Bigwheels</span>
                                                </div>
                                                <div class="img_pro">
                                                    <img src="<?php echo base_url();?>assets/webapp/images/pro_img.jpg" />
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <div class="text_b">
                                                    Chicco Billy Bigwheels
                                                    <span> Billy Bigwheels</span>
                                                </div>
                                                <div class="img_pro">
                                                    <img src="<?php echo base_url();?>assets/webapp/images/pro_img.jpg" />
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div> -->
                
                </div>
            </div>
            <div class="col-lg-1 col-md-1"></div>
            </div>
        </div>

       
        <div class="div_sep_p hidden-xs hidden-sm">
            <div class="col-lg-1 col-md-1">
              
            </div>
            <div class="col-lg-10 col-md-10">
                <div class="row">
                    <div class="div_sep white2" style="border:none;margin-top:40px;">
                        <div class="col-lg-12">
                            <div class="tabs">
                                <ul class="tab_ul">
                                    <li data-tab="regis">Registration & Login<span class="tab_ar"></span></li>
                                    <li data-tab="produ_sel">Product selection<span class="tab_ar"></span></li>
                                    <li data-tab="cart_ch">Cart check out<span class="tab_ar"></span></li>
                                    <li data-tab="pay_op">Payment options<span class="tab_ar"></span></li>
                                    <li data-tab="Ship">Shipping<span class="tab_ar"></span></li>
                                    <li data-tab="compl">Completed<span class="tab_ar"></span></li>
                                </ul>
                            </div>
                            <div class="login_tab" id="regis">
                            <div class="col-lg-12">
                            	<div class="row">
                            		<div class="tab_cont">
                                		<img src="<?php echo base_url();?>assets/webapp/images/register.jpg" class="img-responsive" style="margin:auto;"/>
                            		</div>
                                </div>
                            </div>
                            
                            
                           </div>
                           <div class="login_tab" id="produ_sel">
                            <div class="col-lg-12">
                            	<div class="row">
                            		<div class="tab_cont">
                                		<img src="<?php echo base_url();?>assets/webapp/images/product_selection.jpg" class="img-responsive" style="margin:auto;"/>
                            		</div>
                                </div>
                            </div>
                            
                            
                           </div>
                           <div class="login_tab" id="cart_ch">
                            <div class="col-lg-12">
                            	<div class="row">
                            		<div class="tab_cont">
                                		<img src="<?php echo base_url();?>assets/webapp/images/checkout.jpg" class="img-responsive" style="margin:auto;"/>
                            		</div>
                                </div>
                            </div>
                            
                            
                           </div>
                           <div class="login_tab" id="pay_op">
                            <div class="col-lg-12">
                            	<div class="row">
                            		<div class="tab_cont">
                                		<img src="<?php echo base_url();?>assets/webapp/images/payment1.jpg" class="img-responsive" style="margin:auto;"/>
                            		</div>
                                </div>
                            </div>
                            
                            
                           </div>
                           <div class="login_tab" id="Ship">
                            <div class="col-lg-12">
                            	<div class="row">
                            		<div class="tab_cont">
                                		<img src="<?php echo base_url();?>assets/webapp/images/shpping.jpg" class="img-responsive" style="margin:auto;"/>
                            		</div>
                                </div>
                            </div>
                            
                            
                           </div>
                           <div class="login_tab" id="compl">
                            <div class="col-lg-12">
                            	<div class="row">
                            		<div class="tab_cont">
                                		<img src="<?php echo base_url();?>assets/webapp/images/completed.jpg" class="img-responsive" style="margin:auto;"/>
                            		</div>
                                </div>
                            </div>
                            
                            
                           </div>
                     
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="col-lg-1 col-md-1"></div>
        </div>
    </div>

    <?php //p($sectiondetailview);exit;
//p($this->session->all_userdata());//exit();
if($this->session->userdata('sesregister')=='registeruser'){ ?>
  
    <div class="add_coment_canc" style="width:338px;">
    <p>Please Complete Your Personal Information</p>
    <form method="post" action="<?php echo base_url(); ?>webapp/webapp/subscribe" id="compt_pro_frm">
    <p style="font-size:12px;">
    <input type="hidden" name="mode" value="123">
    <input type="checkbox" name="subs" value="<?php echo $this->session->userdata('snowworldmember')->compemailid; ?>" style="float:left;margin-right:5px;"> Allow promotional messages & email permission</p>
    <input type="submit" class="comen_su" value="Do it Now">
    <a href="javascript:void(0);" class="comen_ca" onclick="cancel_session_data()">Do it Later</a>
    </form>
    </div>
    <div class="overlay"></div>    
    
    
<?php
}
if(!empty($this->session->userdata('popupon')) && $this->session->userdata('popupon')=='on'){ ?>
  
    <div class="add_coment_canc" style="width:470px;left: 41%;">
    <p>We accept only advance payment and shipping charges are extra.</p>
    <p style="font-size:12px;">
    <a href="javascript:void(0);" class="comen_ca" style="float: right;" onclick="location.href=location.href">OK</a>
    </div>
    <div class="overlay"></div>    
    
    
<?php
}
$this->session->unset_userdata('popupon');
?>
    
    <script type="text/javascript">
    function cancel_session_data()
    {
       $('#compt_pro_frm').attr('action','');
       $('#compt_pro_frm').submit();
    }
    </script>   
   
   <script type="text/javascript">

        $(document).ready(function(){
          $(".ceteg").click(function(){
           $("#index_nv").toggleClass("index_sm_nav");
          });
          $("#regis").fadeIn();
       $(".tab_ul li").click(function(){
       	var op_tab = $(this).attr("data-tab");
       	$(".tab_ul li").removeClass("active_t");
       	$(this).addClass("active_t");
       	$(".login_tab").hide();
       	//alert();
       	$("#"+op_tab).fadeIn();
       });
        });
         
    </script>
    <style type="text/css">
   .login_tab{
   	display: none;
   }
 
    </style>