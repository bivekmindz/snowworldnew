   
    <!-- THE SHOWBIZ STLYES -->
    <link rel="stylesheet" type="text/css" href="showbizpro/css/settings.css" media="screen" />
    <!-- get jQuery from the google apis -->
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.js"></script>    
    <!-- THE SHOWBIZ JS FILES  -->
    <script type="text/javascript" src="showbizpro/js/jquery.themepunch.plugins.min.js"></script>
    <script type="text/javascript" src="showbizpro/js/jquery.themepunch.showbizpro2.min.js"></script>
    <script src="js/ga-code.js"></script>
</head>


    
  
    
    
    
    <div class="container margin_30">
        <hr>        
            <div class="showbiz-navigation center sb-nav-dark">
                <a id="showbiz_left_4" class="sb-navigation-left"><i class="icon-left-open"></i></a>
                <a id="showbiz_right_4" class="sb-navigation-right"><i class="icon-right-open"></i></a>
                <div class="sbclear"></div>
            </div>

            <div class="divide20"></div>

            <div id="habitat" class="showbiz-container darkbg">

                <!--    THE PORTFOLIO ENTRIES   -->
                <div class="showbiz" data-left="#showbiz_left_4" data-right="#showbiz_right_4" id="sbiz2288" style="user-select: none; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); touch-action: none;">
                    <!-- THE OVERFLOW HOLDER CONTAINER, DONT REMOVE IT !! -->
                    <div class="overflowholder" style="height: 313px;">
                        <!-- LIST OF THE ENTRIES -->
                        <ul class="showbiz-drag-mouse" style="width: 4652px; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -0.00333, 0, 0, 0, 1); height: 313px; left: -965px;">

                            <!-- AN ENTRY HERE WITH PREDEFINED SHOWCASE SKIN-->
                            <li class="sb-showcase-skin" style="width: 173px;">

                                        <!-- THE MEDIA HOLDER HERE -->
                                        <div class="mediaholder ">
                                            <div class="mediaholder_innerwrap">
                                                <img alt="Frozo" src="img/habitat/frozo.png">
                                            </div>
                                        </div><!-- END OF MEDIA CONTAINER -->

                                        <!-- SOME ALWAYS VISIBLE CONTENT -->
                                        <div class="detailholder">
                                            <h4 class="showbiz-title txt-center"><a href="#">Frozo</a></h4>
                                            <div class="divide5"></div>
                                            <p class="txt-center">He's the coolest youngster among the lot.</p>
                                        </div><!-- END OF DEATIL CONTAINER -->

                                        <!-- THE REVEAL CONTAINER - OPENING IN FULLWIDTH -->
                                        <div class="reveal_container tofullwidth">

                                            <!-- THE REVEL HIDDEN / VISIBLE CONTAINER -->
                                            <div class="reveal_wrapper">
                                                <!-- THE HEIGHT ADJUSTER CONTAINER -->
                                                <div class="heightadjuster table">

                                                    <!-- TABLE CONSTRUCT FOR LEFT / RIGHT CONTENTS -->
                                                    <div class="table-cell onethird">
                                                            <img alt="Frozo" src="img/habitat/frozo.png" width="100%">
                                                    </div>

                                                    <!-- CONTENT IN TABLE -->
                                                    <div class="table-cell pl20">
                                                        <h3 class="showbiz-title large">Frozo <span class="sub_title">The Rock Star</span></h3>
                                                        <div class="divide20"></div>
                                                        <p>He's the coolest youngster among the lot. He's got the coolest and Swankiest hairstyle, a Mohawk. His charm and youthfulness can baffle any girl around the corner. He got groovy skates and goes to College on those. You are definite to find Frozo in any of the rock shows in the Snow Village. At times you would see skating on an electric guitar. </p>
                                                        <div class="divide20"></div>
                                                        <!--<p class="orange bigger bolder">Special Offer:</p>
                                                        <div class="showbiz-price"><span class="currency">$</span><span class="number">25.49</span></div>
                                                        <a class="showbiz-button">Purchase Now</a>-->
                                                    </div><!-- END OF CONTENT  -->

                                                </div><!-- END OF HEIGHT ADJUSTER CONTAINER -->
                                            </div><!-- END OF REVEAL HIDDEN/VISIBLE CONTAINER -->

                                            <!-- THE CLOSER / OPENER FUNCTION -->
                                            <a class="reveal_opener opener_big_grey">
                                                <span class="openme">Show Details</span>
                                                <span class="closeme">Close</span>
                                            </a><!-- END OF CLOSER / OPENER -->

                                        </div><!-- END OF THE REVEAL CONTAINER -->
                                </li><!-- END OF ENTRY -->

                        <!-- AN ENTRY HERE -->
                            <li class="sb-showcase-skin" style="width: 173px;">
                                        <div class="mediaholder ">
                                            <div class="mediaholder_innerwrap">
                                                <img alt="Waddles" src="img/habitat/waddles.png">
                                            </div>
                                        </div>


                                        <!-- SOME ALWAYS VISIBLE CONTENT -->
                                        <div class="detailholder">
                                            <h4 class="showbiz-title txt-center"><a href="#">Waddles</a></h4>
                                            <div class="divide5"></div>
                                            <p class="txt-center">Tall and confused is how you'll see him always.</p>
                                        </div>

                                        <!-- THE REVEAL CONTAINER - OPENING IN FULLWIDTH -->
                                        <div class="reveal_container tofullwidth">

                                            <!-- THE REVEL HIDDEN / VISIBLE CONTAINER -->
                                            <div class="reveal_wrapper">
                                                <!-- THE HEIGHT ADJUSTER CONTAINER -->
                                                <div class="heightadjuster table">

                                                    <!-- TABLE CONSTRUCT FOR LEFT / RIGHT CONTENTS -->
                                                    <div class="table-cell onethird">
                                                            <img alt="Waddles" src="img/habitat/waddles.png" width="100%">
                                                    </div>

                                                    <!-- CONTENT IN TABLE -->
                                                    <div class="table-cell pl20">
                                                        <h3 class="showbiz-title large">Waddles <span class="sub_title">The Skinny Stud</span></h3>
                                                        <div class="divide20"></div>
                                                        <p>His hair always sways even with the slightest breeze.  Tall and confused is how you'll see him always. He is filled with questions and doubts that seldom people can answer. He finds everything boring around him. One of the common things he says "Why waste time fishing when I can have it from a Tuna Can "</p>
                                                        <div class="divide20"></div>
                                                        
                                                    </div><!-- END OF CONTENT  -->

                                                </div><!-- END OF HEIGHT ADJUSTER CONTAINER -->
                                            </div><!-- END OF REVEAL HIDDEN/VISIBLE CONTAINER -->

                                            <!-- THE CLOSER / OPENER FUNCTION -->
                                            <a class="reveal_opener opener_big_grey">
                                                <span class="openme">Show Details</span>
                                                <span class="closeme">Close</span>
                                            </a><!-- END OF CLOSER / OPENER -->

                                        </div><!-- END OF THE REVEAL CONTAINER -->
                            </li>

                            <!-- AN ENTRY HERE -->
                            <li class="sb-showcase-skin" style="width: 173px;">
                                        <div class="mediaholder ">
                                            <div class="mediaholder_innerwrap">
                                                <img alt="Tip" src="img/habitat/tip.png">
                                            </div>
                                        </div>


                                        <!-- SOME ALWAYS VISIBLE CONTENT -->
                                        <div class="detailholder">
                                            <h4 class="showbiz-title txt-center"><a href="#">Tip</a></h4>
                                            <div class="divide5"></div>
                                            <p class="txt-center">The most loved ones among everyone</p>
                                        </div>

                                        <!-- THE REVEAL CONTAINER - OPENING IN FULLWIDTH -->
                                        <div class="reveal_container tofullwidth">

                                            <!-- THE REVEL HIDDEN / VISIBLE CONTAINER -->
                                            <div class="reveal_wrapper">
                                                <!-- THE HEIGHT ADJUSTER CONTAINER -->
                                                <div class="heightadjuster table">

                                                    <!-- TABLE CONSTRUCT FOR LEFT / RIGHT CONTENTS -->
                                                    <div class="table-cell onethird">
                                                            <img alt="Tip" src="img/habitat/tip.png" width="100%">
                                                    </div>

                                                    <!-- CONTENT IN TABLE -->
                                                    <div class="table-cell pl20">
                                                        <h3 class="showbiz-title large">Tip <span class="sub_title">The Cutie Heart</span></h3>
                                                        <div class="divide20"></div>
                                                        <p>They are known to spread love. The most loved ones among everyone. They are Small, Tiny &amp; cuddly. Their common sight is " Laughing &amp; Rolling on the floor.” ROFL suits them pretty well. There are moments when they laugh, roll and form a giant ice ball and destroy an Ice Hockey Match.</p>
                                                        <div class="divide20"></div>
                                                    </div><!-- END OF CONTENT  -->

                                                </div><!-- END OF HEIGHT ADJUSTER CONTAINER -->
                                            </div><!-- END OF REVEAL HIDDEN/VISIBLE CONTAINER -->

                                            <!-- THE CLOSER / OPENER FUNCTION -->
                                            <a class="reveal_opener opener_big_grey">
                                                <span class="openme">Show Details</span>
                                                <span class="closeme">Close</span>
                                            </a><!-- END OF CLOSER / OPENER -->

                                        </div><!-- END OF THE REVEAL CONTAINER -->

                            </li>


                            <!-- AN ENTRY HERE -->
                            <li class="sb-showcase-skin" style="width: 173px;">
                                        <div class="mediaholder ">
                                            <div class="mediaholder_innerwrap">
                                                <img alt="Yowie" src="img/habitat/yowie.png">
                                            </div>
                                        </div>


                                        <!-- SOME ALWAYS VISIBLE CONTENT -->
                                        <div class="detailholder">
                                            <h4 class="showbiz-title txt-center"><a href="#">Yowie</a></h4>
                                            <div class="divide5"></div>
                                            <p class="txt-center">He is always so annoyed of the penguins making noise</p>
                                        </div>

                                        <!-- THE REVEAL CONTAINER - OPENING IN FULLWIDTH -->
                                        <!-- END OF THE REVEAL CONTAINER -->
                            <div class="reveal_container tofullwidth">

                                            <!-- THE REVEL HIDDEN / VISIBLE CONTAINER -->
                                            <div class="reveal_wrapper" style="opacity: 0; top: 0%; height: 0%; visibility: visible;">
                                                <!-- THE HEIGHT ADJUSTER CONTAINER -->
                                                <div class="heightadjuster table">

                                                    <!-- TABLE CONSTRUCT FOR LEFT / RIGHT CONTENTS -->
                                                    <div class="table-cell onethird">
                                                            <img alt="Yowie" src="img/habitat/yowie.png" width="100%">
                                                    </div>

                                                    <!-- CONTENT IN TABLE -->
                                                    <div class="table-cell pl20">
                                                        <h3 class="showbiz-title large">Yowie <span class="sub_title">The Giant Yeti</span></h3>
                                                        <div class="divide20"></div>
                                                        <p>Tall , Lazy &amp; huge. His hand is his strength. He is often seen fishing or maybe even having a Sauna Bath in the hot water springs of the Ice Mountain. He is always so annoyed of the penguins making noise and troubling him that he tries to catch hold of them.</p>
                                                        <div class="divide20"></div>
                                                    </div><!-- END OF CONTENT  -->

                                                </div><!-- END OF HEIGHT ADJUSTER CONTAINER -->
                                            </div><!-- END OF REVEAL HIDDEN/VISIBLE CONTAINER -->

                                            <!-- THE CLOSER / OPENER FUNCTION -->
                                            <a class="reveal_opener opener_big_grey">
                                                <span class="openme">Show Details</span>
                                                <span class="closeme">Close</span>
                                            </a><!-- END OF CLOSER / OPENER -->

                                        </div></li>

                        <!-- AN ENTRY HERE -->
                            <li class="sb-showcase-skin" style="width: 173px;">
                                        <div class="mediaholder ">
                                            <div class="mediaholder_innerwrap">
                                                <img alt="Toe" src="img/habitat/toe.png">
                                            </div>
                                        </div>


                                        <!-- SOME ALWAYS VISIBLE CONTENT -->
                                        <div class="detailholder">
                                            <h4 class="showbiz-title txt-center"><a href="#">Toe</a></h4>
                                            <div class="divide5"></div>
                                            <p class="txt-center">The most loved ones among everyone</p>
                                        </div>

                                        <!-- THE REVEAL CONTAINER - OPENING IN FULLWIDTH -->
                                        <!-- END OF THE REVEAL CONTAINER -->

                            <div class="reveal_container tofullwidth">

                                            <!-- THE REVEL HIDDEN / VISIBLE CONTAINER -->
                                            <div class="reveal_wrapper" style="opacity: 0; top: 0%; height: 0%; visibility: visible;">
                                                <!-- THE HEIGHT ADJUSTER CONTAINER -->
                                                <div class="heightadjuster table">

                                                    <!-- TABLE CONSTRUCT FOR LEFT / RIGHT CONTENTS -->
                                                    <div class="table-cell onethird">
                                                            <img alt="Toe" src="img/habitat/toe.png" width="100%">
                                                    </div>

                                                    <!-- CONTENT IN TABLE -->
                                                    <div class="table-cell pl20">
                                                        <h3 class="showbiz-title large">Toe <span class="sub_title">The Cutie Heart</span></h3>
                                                        <div class="divide20"></div>
                                                        <p>They are known to spread love. The most loved ones among everyone. They are Small, Tiny &amp; cuddly. Their common sight is " Laughing &amp; Rolling on the floor.” ROFL suits them pretty well. There are moments when they laugh, roll and form a giant ice ball and destroy an Ice Hockey Match.</p>
                                                        <div class="divide20"></div>
                                                    </div><!-- END OF CONTENT  -->

                                                </div><!-- END OF HEIGHT ADJUSTER CONTAINER -->
                                            </div><!-- END OF REVEAL HIDDEN/VISIBLE CONTAINER -->

                                            <!-- THE CLOSER / OPENER FUNCTION -->
                                            <a class="reveal_opener opener_big_grey">
                                                <span class="openme">Show Details</span>
                                                <span class="closeme">Close</span>
                                            </a><!-- END OF CLOSER / OPENER -->

                                        </div></li>

                            <!-- NEW ENTRY HERE -->
                            <li class="sb-showcase-skin" style="width: 173px;">

                                        <!-- THE MEDIA HOLDER HERE -->
                                        <div class="mediaholder ">
                                            <div class="mediaholder_innerwrap">
                                                <img alt="Polka" src="img/habitat/polka.png">
                                            </div>
                                        </div><!-- END OF MEDIA CONTAINER -->

                                        <!-- SOME ALWAYS VISIBLE CONTENT -->
                                        <div class="detailholder">
                                            <h4 class="showbiz-title txt-center"><a href="#">Polka</a></h4>
                                            <div class="divide5"></div>
                                            <p class="txt-center">She has her own fashion Line</p>
                                        </div><!-- END OF DEATIL CONTAINER -->

                                        <!-- THE REVEAL CONTAINER - OPENING IN FULLWIDTH -->
                                        
                                <div class="reveal_container tofullwidth">

                                            <!-- THE REVEL HIDDEN / VISIBLE CONTAINER -->
                                            <div class="reveal_wrapper" style="opacity: 0; top: 0%; height: 0%; visibility: visible;">
                                                <!-- THE HEIGHT ADJUSTER CONTAINER -->
                                                <div class="heightadjuster table">

                                                    <!-- TABLE CONSTRUCT FOR LEFT / RIGHT CONTENTS -->
                                                    <div class="table-cell onethird">
                                                            <img alt="Polka" src="img/habitat/polka.png">
                                                    </div>

                                                    <!-- CONTENT IN TABLE -->
                                                    <div class="table-cell pl20">
                                                        <h3 class="showbiz-title large">Polka <span class="sub_title">The Stylista</span></h3>
                                                        <div class="divide20"></div>
                                                        <p>A middle aged lady , who thinks there's no one more stylish than her. She considers herself to be a fashion police. She has her own fashion Line called " Polka ". She Walks around with an attitude filled look with sunglasses &amp; Fashionista Bags Swaying her booties.</p>
                                                        <div class="divide20"></div>                                                        
                                                    </div><!-- END OF CONTENT  -->

                                                </div><!-- END OF HEIGHT ADJUSTER CONTAINER -->
                                            </div><!-- END OF REVEAL HIDDEN/VISIBLE CONTAINER -->

                                            
                                            <a class="reveal_opener opener_big_grey">
                                                <span class="openme">Show Details</span>
                                                <span class="closeme">Close</span>
                                            </a>

                                        </div></li>
                        <li class="sb-showcase-skin" style="width: 173px;">

                                        <!-- THE MEDIA HOLDER HERE -->
                                        <div class="mediaholder ">
                                            <div class="mediaholder_innerwrap">
                                                <img alt="Frozo" src="img/habitat/frozo.png">
                                            </div>
                                        </div><!-- END OF MEDIA CONTAINER -->

                                        <!-- SOME ALWAYS VISIBLE CONTENT -->
                                        <div class="detailholder">
                                            <h4 class="showbiz-title txt-center"><a href="#">Frozo</a></h4>
                                            <div class="divide5"></div>
                                            <p class="txt-center">He's the coolest youngster among the lot.</p>
                                        </div><!-- END OF DEATIL CONTAINER -->

                                        <!-- THE REVEAL CONTAINER - OPENING IN FULLWIDTH -->
                                        <!-- END OF THE REVEAL CONTAINER -->
                                <div class="reveal_container tofullwidth">

                                            <!-- THE REVEL HIDDEN / VISIBLE CONTAINER -->
                                            <div class="reveal_wrapper" style="opacity: 0; top: 0%; height: 0%; visibility: visible;">
                                                <!-- THE HEIGHT ADJUSTER CONTAINER -->
                                                <div class="heightadjuster table">

                                                    <!-- TABLE CONSTRUCT FOR LEFT / RIGHT CONTENTS -->
                                                    <div class="table-cell onethird">
                                                            <img alt="Frozo" src="img/habitat/frozo.png" width="100%">
                                                    </div>

                                                    <!-- CONTENT IN TABLE -->
                                                    <div class="table-cell pl20">
                                                        <h3 class="showbiz-title large">Frozo <span class="sub_title">The Rock Star</span></h3>
                                                        <div class="divide20"></div>
                                                        <p>He's the coolest youngster among the lot. He's got the coolest and Swankiest hairstyle, a Mohawk. His charm and youthfulness can baffle any girl around the corner. He got groovy skates and goes to College on those. You are definite to find Frozo in any of the rock shows in the Snow Village. At times you would see skating on an electric guitar. </p>
                                                        <div class="divide20"></div>
                                                        <!--<p class="orange bigger bolder">Special Offer:</p>
                                                        <div class="showbiz-price"><span class="currency">$</span><span class="number">25.49</span></div>
                                                        <a class="showbiz-button">Purchase Now</a>-->
                                                    </div><!-- END OF CONTENT  -->

                                                </div><!-- END OF HEIGHT ADJUSTER CONTAINER -->
                                            </div><!-- END OF REVEAL HIDDEN/VISIBLE CONTAINER -->

                                            <!-- THE CLOSER / OPENER FUNCTION -->
                                            <a class="reveal_opener opener_big_grey">
                                                <span class="openme">Show Details</span>
                                                <span class="closeme">Close</span>
                                            </a><!-- END OF CLOSER / OPENER -->

                                        </div></li><li class="sb-showcase-skin" style="width: 173px;">
                                        <div class="mediaholder ">
                                            <div class="mediaholder_innerwrap">
                                                <img alt="Waddles" src="img/habitat/waddles.png">
                                            </div>
                                        </div>


                                        <!-- SOME ALWAYS VISIBLE CONTENT -->
                                        <div class="detailholder">
                                            <h4 class="showbiz-title txt-center"><a href="#">Waddles</a></h4>
                                            <div class="divide5"></div>
                                            <p class="txt-center">Tall and confused is how you'll see him always.</p>
                                        </div>

                                        <!-- THE REVEAL CONTAINER - OPENING IN FULLWIDTH -->
                                        <div class="reveal_container tofullwidth">

                                            <!-- THE REVEL HIDDEN / VISIBLE CONTAINER -->
                                            <div class="reveal_wrapper">
                                                <!-- THE HEIGHT ADJUSTER CONTAINER -->
                                                <div class="heightadjuster table">

                                                    <!-- TABLE CONSTRUCT FOR LEFT / RIGHT CONTENTS -->
                                                    <div class="table-cell onethird">
                                                            <img alt="Waddles" src="img/habitat/waddles.png" width="100%">
                                                    </div>

                                                    <!-- CONTENT IN TABLE -->
                                                    <div class="table-cell pl20">
                                                        <h3 class="showbiz-title large">Waddles <span class="sub_title">The Skinny Stud</span></h3>
                                                        <div class="divide20"></div>
                                                        <p>His hair always sways even with the slightest breeze.  Tall and confused is how you'll see him always. He is filled with questions and doubts that seldom people can answer. He finds everything boring around him. One of the common things he says "Why waste time fishing when I can have it from a Tuna Can "</p>
                                                        <div class="divide20"></div>
                                                        
                                                    </div><!-- END OF CONTENT  -->

                                                </div><!-- END OF HEIGHT ADJUSTER CONTAINER -->
                                            </div><!-- END OF REVEAL HIDDEN/VISIBLE CONTAINER -->

                                            <!-- THE CLOSER / OPENER FUNCTION -->
                                            <a class="reveal_opener opener_big_grey">
                                                <span class="openme">Show Details</span>
                                                <span class="closeme">Close</span>
                                            </a><!-- END OF CLOSER / OPENER -->

                                        </div><!-- END OF THE REVEAL CONTAINER -->
                            </li><li class="sb-showcase-skin" style="width: 173px;">
                                        <div class="mediaholder ">
                                            <div class="mediaholder_innerwrap">
                                                <img alt="Tip" src="img/habitat/tip.png">
                                            </div>
                                        </div>


                                        <!-- SOME ALWAYS VISIBLE CONTENT -->
                                        <div class="detailholder">
                                            <h4 class="showbiz-title txt-center"><a href="#">Tip</a></h4>
                                            <div class="divide5"></div>
                                            <p class="txt-center">The most loved ones among everyone</p>
                                        </div>

                                        <!-- THE REVEAL CONTAINER - OPENING IN FULLWIDTH -->
                                        <!-- END OF THE REVEAL CONTAINER -->

                            <div class="reveal_container tofullwidth">

                                            <!-- THE REVEL HIDDEN / VISIBLE CONTAINER -->
                                            <div class="reveal_wrapper" style="opacity: 0; top: 0%; height: 0%; visibility: visible;">
                                                <!-- THE HEIGHT ADJUSTER CONTAINER -->
                                                <div class="heightadjuster table">

                                                    <!-- TABLE CONSTRUCT FOR LEFT / RIGHT CONTENTS -->
                                                    <div class="table-cell onethird">
                                                            <img alt="Tip" src="img/habitat/tip.png" width="100%">
                                                    </div>

                                                    <!-- CONTENT IN TABLE -->
                                                    <div class="table-cell pl20">
                                                        <h3 class="showbiz-title large">Tip <span class="sub_title">The Cutie Heart</span></h3>
                                                        <div class="divide20"></div>
                                                        <p>They are known to spread love. The most loved ones among everyone. They are Small, Tiny &amp; cuddly. Their common sight is " Laughing &amp; Rolling on the floor.” ROFL suits them pretty well. There are moments when they laugh, roll and form a giant ice ball and destroy an Ice Hockey Match.</p>
                                                        <div class="divide20"></div>
                                                    </div><!-- END OF CONTENT  -->

                                                </div><!-- END OF HEIGHT ADJUSTER CONTAINER -->
                                            </div><!-- END OF REVEAL HIDDEN/VISIBLE CONTAINER -->

                                            <!-- THE CLOSER / OPENER FUNCTION -->
                                            <a class="reveal_opener opener_big_grey">
                                                <span class="openme">Show Details</span>
                                                <span class="closeme">Close</span>
                                            </a><!-- END OF CLOSER / OPENER -->

                                        </div></li><li class="sb-showcase-skin" style="width: 173px;">
                                        <div class="mediaholder ">
                                            <div class="mediaholder_innerwrap">
                                                <img alt="Yowie" src="img/habitat/yowie.png">
                                            </div>
                                        </div>


                                        <!-- SOME ALWAYS VISIBLE CONTENT -->
                                        <div class="detailholder">
                                            <h4 class="showbiz-title txt-center"><a href="#">Yowie</a></h4>
                                            <div class="divide5"></div>
                                            <p class="txt-center">He is always so annoyed of the penguins making noise</p>
                                        </div>

                                        <!-- THE REVEAL CONTAINER - OPENING IN FULLWIDTH -->
                                        <div class="reveal_container tofullwidth">

                                            <!-- THE REVEL HIDDEN / VISIBLE CONTAINER -->
                                            <div class="reveal_wrapper">
                                                <!-- THE HEIGHT ADJUSTER CONTAINER -->
                                                <div class="heightadjuster table">

                                                    <!-- TABLE CONSTRUCT FOR LEFT / RIGHT CONTENTS -->
                                                    <div class="table-cell onethird">
                                                            <img alt="Yowie" src="img/habitat/yowie.png" width="100%">
                                                    </div>

                                                    <!-- CONTENT IN TABLE -->
                                                    <div class="table-cell pl20">
                                                        <h3 class="showbiz-title large">Yowie <span class="sub_title">The Giant Yeti</span></h3>
                                                        <div class="divide20"></div>
                                                        <p>Tall , Lazy &amp; huge. His hand is his strength. He is often seen fishing or maybe even having a Sauna Bath in the hot water springs of the Ice Mountain. He is always so annoyed of the penguins making noise and troubling him that he tries to catch hold of them.</p>
                                                        <div class="divide20"></div>
                                                    </div><!-- END OF CONTENT  -->

                                                </div><!-- END OF HEIGHT ADJUSTER CONTAINER -->
                                            </div><!-- END OF REVEAL HIDDEN/VISIBLE CONTAINER -->

                                            <!-- THE CLOSER / OPENER FUNCTION -->
                                            <a class="reveal_opener opener_big_grey">
                                                <span class="openme">Show Details</span>
                                                <span class="closeme">Close</span>
                                            </a><!-- END OF CLOSER / OPENER -->

                                        </div><!-- END OF THE REVEAL CONTAINER -->
                            </li><li class="sb-showcase-skin" style="width: 173px;">
                                        <div class="mediaholder ">
                                            <div class="mediaholder_innerwrap">
                                                <img alt="Toe" src="img/habitat/toe.png">
                                            </div>
                                        </div>


                                        <!-- SOME ALWAYS VISIBLE CONTENT -->
                                        <div class="detailholder">
                                            <h4 class="showbiz-title txt-center"><a href="#">Toe</a></h4>
                                            <div class="divide5"></div>
                                            <p class="txt-center">The most loved ones among everyone</p>
                                        </div>

                                        <!-- THE REVEAL CONTAINER - OPENING IN FULLWIDTH -->
                                        <div class="reveal_container tofullwidth">

                                            <!-- THE REVEL HIDDEN / VISIBLE CONTAINER -->
                                            <div class="reveal_wrapper">
                                                <!-- THE HEIGHT ADJUSTER CONTAINER -->
                                                <div class="heightadjuster table">

                                                    <!-- TABLE CONSTRUCT FOR LEFT / RIGHT CONTENTS -->
                                                    <div class="table-cell onethird">
                                                            <img alt="Toe" src="img/habitat/toe.png" width="100%">
                                                    </div>

                                                    <!-- CONTENT IN TABLE -->
                                                    <div class="table-cell pl20">
                                                        <h3 class="showbiz-title large">Toe <span class="sub_title">The Cutie Heart</span></h3>
                                                        <div class="divide20"></div>
                                                        <p>They are known to spread love. The most loved ones among everyone. They are Small, Tiny &amp; cuddly. Their common sight is " Laughing &amp; Rolling on the floor.” ROFL suits them pretty well. There are moments when they laugh, roll and form a giant ice ball and destroy an Ice Hockey Match.</p>
                                                        <div class="divide20"></div>
                                                    </div><!-- END OF CONTENT  -->

                                                </div><!-- END OF HEIGHT ADJUSTER CONTAINER -->
                                            </div><!-- END OF REVEAL HIDDEN/VISIBLE CONTAINER -->

                                            <!-- THE CLOSER / OPENER FUNCTION -->
                                            <a class="reveal_opener opener_big_grey">
                                                <span class="openme">Show Details</span>
                                                <span class="closeme">Close</span>
                                            </a><!-- END OF CLOSER / OPENER -->

                                        </div><!-- END OF THE REVEAL CONTAINER -->

                            </li><li class="sb-showcase-skin" style="width: 173px;">

                                        <!-- THE MEDIA HOLDER HERE -->
                                        <div class="mediaholder ">
                                            <div class="mediaholder_innerwrap">
                                                <img alt="Polka" src="img/habitat/polka.png">
                                            </div>
                                        </div><!-- END OF MEDIA CONTAINER -->

                                        <!-- SOME ALWAYS VISIBLE CONTENT -->
                                        <div class="detailholder">
                                            <h4 class="showbiz-title txt-center"><a href="#">Polka</a></h4>
                                            <div class="divide5"></div>
                                            <p class="txt-center">She has her own fashion Line</p>
                                        </div><!-- END OF DEATIL CONTAINER -->

                                        <!-- THE REVEAL CONTAINER - OPENING IN FULLWIDTH -->
                                        <div class="reveal_container tofullwidth">

                                            <!-- THE REVEL HIDDEN / VISIBLE CONTAINER -->
                                            <div class="reveal_wrapper">
                                                <!-- THE HEIGHT ADJUSTER CONTAINER -->
                                                <div class="heightadjuster table">

                                                    <!-- TABLE CONSTRUCT FOR LEFT / RIGHT CONTENTS -->
                                                    <div class="table-cell onethird">
                                                            <img alt="Polka" src="img/habitat/polka.png">
                                                    </div>

                                                    <!-- CONTENT IN TABLE -->
                                                    <div class="table-cell pl20">
                                                        <h3 class="showbiz-title large">Polka <span class="sub_title">The Stylista</span></h3>
                                                        <div class="divide20"></div>
                                                        <p>A middle aged lady , who thinks there's no one more stylish than her. She considers herself to be a fashion police. She has her own fashion Line called " Polka ". She Walks around with an attitude filled look with sunglasses &amp; Fashionista Bags Swaying her booties.</p>
                                                        <div class="divide20"></div>                                                        
                                                    </div><!-- END OF CONTENT  -->

                                                </div><!-- END OF HEIGHT ADJUSTER CONTAINER -->
                                            </div><!-- END OF REVEAL HIDDEN/VISIBLE CONTAINER -->

                                            
                                            <a class="reveal_opener opener_big_grey">
                                                <span class="openme">Show Details</span>
                                                <span class="closeme">Close</span>
                                            </a>

                                        </div>
                                </li><li class="sb-showcase-skin" style="width: 173px;">

                                        <!-- THE MEDIA HOLDER HERE -->
                                        <div class="mediaholder ">
                                            <div class="mediaholder_innerwrap">
                                                <img alt="Frozo" src="img/habitat/frozo.png">
                                            </div>
                                        </div><!-- END OF MEDIA CONTAINER -->

                                        <!-- SOME ALWAYS VISIBLE CONTENT -->
                                        <div class="detailholder">
                                            <h4 class="showbiz-title txt-center"><a href="#">Frozo</a></h4>
                                            <div class="divide5"></div>
                                            <p class="txt-center">He's the coolest youngster among the lot.</p>
                                        </div><!-- END OF DEATIL CONTAINER -->

                                        <!-- THE REVEAL CONTAINER - OPENING IN FULLWIDTH -->
                                        <div class="reveal_container tofullwidth">

                                            <!-- THE REVEL HIDDEN / VISIBLE CONTAINER -->
                                            <div class="reveal_wrapper">
                                                <!-- THE HEIGHT ADJUSTER CONTAINER -->
                                                <div class="heightadjuster table">

                                                    <!-- TABLE CONSTRUCT FOR LEFT / RIGHT CONTENTS -->
                                                    <div class="table-cell onethird">
                                                            <img alt="Frozo" src="img/habitat/frozo.png" width="100%">
                                                    </div>

                                                    <!-- CONTENT IN TABLE -->
                                                    <div class="table-cell pl20">
                                                        <h3 class="showbiz-title large">Frozo <span class="sub_title">The Rock Star</span></h3>
                                                        <div class="divide20"></div>
                                                        <p>He's the coolest youngster among the lot. He's got the coolest and Swankiest hairstyle, a Mohawk. His charm and youthfulness can baffle any girl around the corner. He got groovy skates and goes to College on those. You are definite to find Frozo in any of the rock shows in the Snow Village. At times you would see skating on an electric guitar. </p>
                                                        <div class="divide20"></div>
                                                        <!--<p class="orange bigger bolder">Special Offer:</p>
                                                        <div class="showbiz-price"><span class="currency">$</span><span class="number">25.49</span></div>
                                                        <a class="showbiz-button">Purchase Now</a>-->
                                                    </div><!-- END OF CONTENT  -->

                                                </div><!-- END OF HEIGHT ADJUSTER CONTAINER -->
                                            </div><!-- END OF REVEAL HIDDEN/VISIBLE CONTAINER -->

                                            <!-- THE CLOSER / OPENER FUNCTION -->
                                            <a class="reveal_opener opener_big_grey">
                                                <span class="openme">Show Details</span>
                                                <span class="closeme">Close</span>
                                            </a><!-- END OF CLOSER / OPENER -->

                                        </div><!-- END OF THE REVEAL CONTAINER -->
                                </li><li class="sb-showcase-skin" style="width: 173px;">
                                        <div class="mediaholder ">
                                            <div class="mediaholder_innerwrap">
                                                <img alt="Waddles" src="img/habitat/waddles.png">
                                            </div>
                                        </div>


                                        <!-- SOME ALWAYS VISIBLE CONTENT -->
                                        <div class="detailholder">
                                            <h4 class="showbiz-title txt-center"><a href="#">Waddles</a></h4>
                                            <div class="divide5"></div>
                                            <p class="txt-center">Tall and confused is how you'll see him always.</p>
                                        </div>

                                        <!-- THE REVEAL CONTAINER - OPENING IN FULLWIDTH -->
                                        <div class="reveal_container tofullwidth">

                                            <!-- THE REVEL HIDDEN / VISIBLE CONTAINER -->
                                            <div class="reveal_wrapper">
                                                <!-- THE HEIGHT ADJUSTER CONTAINER -->
                                                <div class="heightadjuster table">

                                                    <!-- TABLE CONSTRUCT FOR LEFT / RIGHT CONTENTS -->
                                                    <div class="table-cell onethird">
                                                            <img alt="Waddles" src="img/habitat/waddles.png" width="100%">
                                                    </div>

                                                    <!-- CONTENT IN TABLE -->
                                                    <div class="table-cell pl20">
                                                        <h3 class="showbiz-title large">Waddles <span class="sub_title">The Skinny Stud</span></h3>
                                                        <div class="divide20"></div>
                                                        <p>His hair always sways even with the slightest breeze.  Tall and confused is how you'll see him always. He is filled with questions and doubts that seldom people can answer. He finds everything boring around him. One of the common things he says "Why waste time fishing when I can have it from a Tuna Can "</p>
                                                        <div class="divide20"></div>
                                                        
                                                    </div><!-- END OF CONTENT  -->

                                                </div><!-- END OF HEIGHT ADJUSTER CONTAINER -->
                                            </div><!-- END OF REVEAL HIDDEN/VISIBLE CONTAINER -->

                                            <!-- THE CLOSER / OPENER FUNCTION -->
                                            <a class="reveal_opener opener_big_grey">
                                                <span class="openme">Show Details</span>
                                                <span class="closeme">Close</span>
                                            </a><!-- END OF CLOSER / OPENER -->

                                        </div><!-- END OF THE REVEAL CONTAINER -->
                            </li><li class="sb-showcase-skin" style="width: 173px;">
                                        <div class="mediaholder ">
                                            <div class="mediaholder_innerwrap">
                                                <img alt="Tip" src="img/habitat/tip.png">
                                            </div>
                                        </div>


                                        <!-- SOME ALWAYS VISIBLE CONTENT -->
                                        <div class="detailholder">
                                            <h4 class="showbiz-title txt-center"><a href="#">Tip</a></h4>
                                            <div class="divide5"></div>
                                            <p class="txt-center">The most loved ones among everyone</p>
                                        </div>

                                        <!-- THE REVEAL CONTAINER - OPENING IN FULLWIDTH -->
                                        <div class="reveal_container tofullwidth">

                                            <!-- THE REVEL HIDDEN / VISIBLE CONTAINER -->
                                            <div class="reveal_wrapper">
                                                <!-- THE HEIGHT ADJUSTER CONTAINER -->
                                                <div class="heightadjuster table">

                                                    <!-- TABLE CONSTRUCT FOR LEFT / RIGHT CONTENTS -->
                                                    <div class="table-cell onethird">
                                                            <img alt="Tip" src="img/habitat/tip.png" width="100%">
                                                    </div>

                                                    <!-- CONTENT IN TABLE -->
                                                    <div class="table-cell pl20">
                                                        <h3 class="showbiz-title large">Tip <span class="sub_title">The Cutie Heart</span></h3>
                                                        <div class="divide20"></div>
                                                        <p>They are known to spread love. The most loved ones among everyone. They are Small, Tiny &amp; cuddly. Their common sight is " Laughing &amp; Rolling on the floor.” ROFL suits them pretty well. There are moments when they laugh, roll and form a giant ice ball and destroy an Ice Hockey Match.</p>
                                                        <div class="divide20"></div>
                                                    </div><!-- END OF CONTENT  -->

                                                </div><!-- END OF HEIGHT ADJUSTER CONTAINER -->
                                            </div><!-- END OF REVEAL HIDDEN/VISIBLE CONTAINER -->

                                            <!-- THE CLOSER / OPENER FUNCTION -->
                                            <a class="reveal_opener opener_big_grey">
                                                <span class="openme">Show Details</span>
                                                <span class="closeme">Close</span>
                                            </a><!-- END OF CLOSER / OPENER -->

                                        </div><!-- END OF THE REVEAL CONTAINER -->

                            </li><li class="sb-showcase-skin" style="width: 173px;">
                                        <div class="mediaholder ">
                                            <div class="mediaholder_innerwrap">
                                                <img alt="Yowie" src="img/habitat/yowie.png">
                                            </div>
                                        </div>


                                        <!-- SOME ALWAYS VISIBLE CONTENT -->
                                        <div class="detailholder">
                                            <h4 class="showbiz-title txt-center"><a href="#">Yowie</a></h4>
                                            <div class="divide5"></div>
                                            <p class="txt-center">He is always so annoyed of the penguins making noise</p>
                                        </div>

                                        <!-- THE REVEAL CONTAINER - OPENING IN FULLWIDTH -->
                                        <div class="reveal_container tofullwidth">

                                            <!-- THE REVEL HIDDEN / VISIBLE CONTAINER -->
                                            <div class="reveal_wrapper">
                                                <!-- THE HEIGHT ADJUSTER CONTAINER -->
                                                <div class="heightadjuster table">

                                                    <!-- TABLE CONSTRUCT FOR LEFT / RIGHT CONTENTS -->
                                                    <div class="table-cell onethird">
                                                            <img alt="Yowie" src="img/habitat/yowie.png" width="100%">
                                                    </div>

                                                    <!-- CONTENT IN TABLE -->
                                                    <div class="table-cell pl20">
                                                        <h3 class="showbiz-title large">Yowie <span class="sub_title">The Giant Yeti</span></h3>
                                                        <div class="divide20"></div>
                                                        <p>Tall , Lazy &amp; huge. His hand is his strength. He is often seen fishing or maybe even having a Sauna Bath in the hot water springs of the Ice Mountain. He is always so annoyed of the penguins making noise and troubling him that he tries to catch hold of them.</p>
                                                        <div class="divide20"></div>
                                                    </div><!-- END OF CONTENT  -->

                                                </div><!-- END OF HEIGHT ADJUSTER CONTAINER -->
                                            </div><!-- END OF REVEAL HIDDEN/VISIBLE CONTAINER -->

                                            <!-- THE CLOSER / OPENER FUNCTION -->
                                            <a class="reveal_opener opener_big_grey">
                                                <span class="openme">Show Details</span>
                                                <span class="closeme">Close</span>
                                            </a><!-- END OF CLOSER / OPENER -->

                                        </div><!-- END OF THE REVEAL CONTAINER -->
                            </li><li class="sb-showcase-skin" style="width: 173px;">
                                        <div class="mediaholder ">
                                            <div class="mediaholder_innerwrap">
                                                <img alt="Toe" src="img/habitat/toe.png">
                                            </div>
                                        </div>


                                        <!-- SOME ALWAYS VISIBLE CONTENT -->
                                        <div class="detailholder">
                                            <h4 class="showbiz-title txt-center"><a href="#">Toe</a></h4>
                                            <div class="divide5"></div>
                                            <p class="txt-center">The most loved ones among everyone</p>
                                        </div>

                                        <!-- THE REVEAL CONTAINER - OPENING IN FULLWIDTH -->
                                        <div class="reveal_container tofullwidth">

                                            <!-- THE REVEL HIDDEN / VISIBLE CONTAINER -->
                                            <div class="reveal_wrapper">
                                                <!-- THE HEIGHT ADJUSTER CONTAINER -->
                                                <div class="heightadjuster table">

                                                    <!-- TABLE CONSTRUCT FOR LEFT / RIGHT CONTENTS -->
                                                    <div class="table-cell onethird">
                                                            <img alt="Toe" src="img/habitat/toe.png" width="100%">
                                                    </div>

                                                    <!-- CONTENT IN TABLE -->
                                                    <div class="table-cell pl20">
                                                        <h3 class="showbiz-title large">Toe <span class="sub_title">The Cutie Heart</span></h3>
                                                        <div class="divide20"></div>
                                                        <p>They are known to spread love. The most loved ones among everyone. They are Small, Tiny &amp; cuddly. Their common sight is " Laughing &amp; Rolling on the floor.” ROFL suits them pretty well. There are moments when they laugh, roll and form a giant ice ball and destroy an Ice Hockey Match.</p>
                                                        <div class="divide20"></div>
                                                    </div><!-- END OF CONTENT  -->

                                                </div><!-- END OF HEIGHT ADJUSTER CONTAINER -->
                                            </div><!-- END OF REVEAL HIDDEN/VISIBLE CONTAINER -->

                                            <!-- THE CLOSER / OPENER FUNCTION -->
                                            <a class="reveal_opener opener_big_grey">
                                                <span class="openme">Show Details</span>
                                                <span class="closeme">Close</span>
                                            </a><!-- END OF CLOSER / OPENER -->

                                        </div><!-- END OF THE REVEAL CONTAINER -->

                            </li><li class="sb-showcase-skin" style="width: 173px;">

                                        <!-- THE MEDIA HOLDER HERE -->
                                        <div class="mediaholder ">
                                            <div class="mediaholder_innerwrap">
                                                <img alt="Polka" src="img/habitat/polka.png">
                                            </div>
                                        </div><!-- END OF MEDIA CONTAINER -->

                                        <!-- SOME ALWAYS VISIBLE CONTENT -->
                                        <div class="detailholder">
                                            <h4 class="showbiz-title txt-center"><a href="#">Polka</a></h4>
                                            <div class="divide5"></div>
                                            <p class="txt-center">She has her own fashion Line</p>
                                        </div><!-- END OF DEATIL CONTAINER -->

                                        <!-- THE REVEAL CONTAINER - OPENING IN FULLWIDTH -->
                                        <div class="reveal_container tofullwidth">

                                            <!-- THE REVEL HIDDEN / VISIBLE CONTAINER -->
                                            <div class="reveal_wrapper">
                                                <!-- THE HEIGHT ADJUSTER CONTAINER -->
                                                <div class="heightadjuster table">

                                                    <!-- TABLE CONSTRUCT FOR LEFT / RIGHT CONTENTS -->
                                                    <div class="table-cell onethird">
                                                            <img alt="Polka" src="img/habitat/polka.png">
                                                    </div>

                                                    <!-- CONTENT IN TABLE -->
                                                    <div class="table-cell pl20">
                                                        <h3 class="showbiz-title large">Polka <span class="sub_title">The Stylista</span></h3>
                                                        <div class="divide20"></div>
                                                        <p>A middle aged lady , who thinks there's no one more stylish than her. She considers herself to be a fashion police. She has her own fashion Line called " Polka ". She Walks around with an attitude filled look with sunglasses &amp; Fashionista Bags Swaying her booties.</p>
                                                        <div class="divide20"></div>                                                        
                                                    </div><!-- END OF CONTENT  -->

                                                </div><!-- END OF HEIGHT ADJUSTER CONTAINER -->
                                            </div><!-- END OF REVEAL HIDDEN/VISIBLE CONTAINER -->

                                            
                                            <a class="reveal_opener opener_big_grey">
                                                <span class="openme">Show Details</span>
                                                <span class="closeme">Close</span>
                                            </a>

                                        </div>
                                </li><li class="sb-showcase-skin" style="width: 173px;">

                                        <!-- THE MEDIA HOLDER HERE -->
                                        <div class="mediaholder ">
                                            <div class="mediaholder_innerwrap">
                                                <img alt="Frozo" src="img/habitat/frozo.png">
                                            </div>
                                        </div><!-- END OF MEDIA CONTAINER -->

                                        <!-- SOME ALWAYS VISIBLE CONTENT -->
                                        <div class="detailholder">
                                            <h4 class="showbiz-title txt-center"><a href="#">Frozo</a></h4>
                                            <div class="divide5"></div>
                                            <p class="txt-center">He's the coolest youngster among the lot.</p>
                                        </div><!-- END OF DEATIL CONTAINER -->

                                        <!-- THE REVEAL CONTAINER - OPENING IN FULLWIDTH -->
                                        <div class="reveal_container tofullwidth">

                                            <!-- THE REVEL HIDDEN / VISIBLE CONTAINER -->
                                            <div class="reveal_wrapper">
                                                <!-- THE HEIGHT ADJUSTER CONTAINER -->
                                                <div class="heightadjuster table">

                                                    <!-- TABLE CONSTRUCT FOR LEFT / RIGHT CONTENTS -->
                                                    <div class="table-cell onethird">
                                                            <img alt="Frozo" src="img/habitat/frozo.png" width="100%">
                                                    </div>

                                                    <!-- CONTENT IN TABLE -->
                                                    <div class="table-cell pl20">
                                                        <h3 class="showbiz-title large">Frozo <span class="sub_title">The Rock Star</span></h3>
                                                        <div class="divide20"></div>
                                                        <p>He's the coolest youngster among the lot. He's got the coolest and Swankiest hairstyle, a Mohawk. His charm and youthfulness can baffle any girl around the corner. He got groovy skates and goes to College on those. You are definite to find Frozo in any of the rock shows in the Snow Village. At times you would see skating on an electric guitar. </p>
                                                        <div class="divide20"></div>
                                                        <!--<p class="orange bigger bolder">Special Offer:</p>
                                                        <div class="showbiz-price"><span class="currency">$</span><span class="number">25.49</span></div>
                                                        <a class="showbiz-button">Purchase Now</a>-->
                                                    </div><!-- END OF CONTENT  -->

                                                </div><!-- END OF HEIGHT ADJUSTER CONTAINER -->
                                            </div><!-- END OF REVEAL HIDDEN/VISIBLE CONTAINER -->

                                            <!-- THE CLOSER / OPENER FUNCTION -->
                                            <a class="reveal_opener opener_big_grey">
                                                <span class="openme">Show Details</span>
                                                <span class="closeme">Close</span>
                                            </a><!-- END OF CLOSER / OPENER -->

                                        </div><!-- END OF THE REVEAL CONTAINER -->
                                </li><li class="sb-showcase-skin" style="width: 173px;">
                                        <div class="mediaholder ">
                                            <div class="mediaholder_innerwrap">
                                                <img alt="Waddles" src="img/habitat/waddles.png">
                                            </div>
                                        </div>


                                        <!-- SOME ALWAYS VISIBLE CONTENT -->
                                        <div class="detailholder">
                                            <h4 class="showbiz-title txt-center"><a href="#">Waddles</a></h4>
                                            <div class="divide5"></div>
                                            <p class="txt-center">Tall and confused is how you'll see him always.</p>
                                        </div>

                                        <!-- THE REVEAL CONTAINER - OPENING IN FULLWIDTH -->
                                        <div class="reveal_container tofullwidth">

                                            <!-- THE REVEL HIDDEN / VISIBLE CONTAINER -->
                                            <div class="reveal_wrapper">
                                                <!-- THE HEIGHT ADJUSTER CONTAINER -->
                                                <div class="heightadjuster table">

                                                    <!-- TABLE CONSTRUCT FOR LEFT / RIGHT CONTENTS -->
                                                    <div class="table-cell onethird">
                                                            <img alt="Waddles" src="img/habitat/waddles.png" width="100%">
                                                    </div>

                                                    <!-- CONTENT IN TABLE -->
                                                    <div class="table-cell pl20">
                                                        <h3 class="showbiz-title large">Waddles <span class="sub_title">The Skinny Stud</span></h3>
                                                        <div class="divide20"></div>
                                                        <p>His hair always sways even with the slightest breeze.  Tall and confused is how you'll see him always. He is filled with questions and doubts that seldom people can answer. He finds everything boring around him. One of the common things he says "Why waste time fishing when I can have it from a Tuna Can "</p>
                                                        <div class="divide20"></div>
                                                        
                                                    </div><!-- END OF CONTENT  -->

                                                </div><!-- END OF HEIGHT ADJUSTER CONTAINER -->
                                            </div><!-- END OF REVEAL HIDDEN/VISIBLE CONTAINER -->

                                            <!-- THE CLOSER / OPENER FUNCTION -->
                                            <a class="reveal_opener opener_big_grey">
                                                <span class="openme">Show Details</span>
                                                <span class="closeme">Close</span>
                                            </a><!-- END OF CLOSER / OPENER -->

                                        </div><!-- END OF THE REVEAL CONTAINER -->
                            </li><li class="sb-showcase-skin" style="width: 173px;">
                                        <div class="mediaholder ">
                                            <div class="mediaholder_innerwrap">
                                                <img alt="Tip" src="img/habitat/tip.png">
                                            </div>
                                        </div>


                                        <!-- SOME ALWAYS VISIBLE CONTENT -->
                                        <div class="detailholder">
                                            <h4 class="showbiz-title txt-center"><a href="#">Tip</a></h4>
                                            <div class="divide5"></div>
                                            <p class="txt-center">The most loved ones among everyone</p>
                                        </div>

                                        <!-- THE REVEAL CONTAINER - OPENING IN FULLWIDTH -->
                                        <div class="reveal_container tofullwidth">

                                            <!-- THE REVEL HIDDEN / VISIBLE CONTAINER -->
                                            <div class="reveal_wrapper">
                                                <!-- THE HEIGHT ADJUSTER CONTAINER -->
                                                <div class="heightadjuster table">

                                                    <!-- TABLE CONSTRUCT FOR LEFT / RIGHT CONTENTS -->
                                                    <div class="table-cell onethird">
                                                            <img alt="Tip" src="img/habitat/tip.png" width="100%">
                                                    </div>

                                                    <!-- CONTENT IN TABLE -->
                                                    <div class="table-cell pl20">
                                                        <h3 class="showbiz-title large">Tip <span class="sub_title">The Cutie Heart</span></h3>
                                                        <div class="divide20"></div>
                                                        <p>They are known to spread love. The most loved ones among everyone. They are Small, Tiny &amp; cuddly. Their common sight is " Laughing &amp; Rolling on the floor.” ROFL suits them pretty well. There are moments when they laugh, roll and form a giant ice ball and destroy an Ice Hockey Match.</p>
                                                        <div class="divide20"></div>
                                                    </div><!-- END OF CONTENT  -->

                                                </div><!-- END OF HEIGHT ADJUSTER CONTAINER -->
                                            </div><!-- END OF REVEAL HIDDEN/VISIBLE CONTAINER -->

                                            <!-- THE CLOSER / OPENER FUNCTION -->
                                            <a class="reveal_opener opener_big_grey">
                                                <span class="openme">Show Details</span>
                                                <span class="closeme">Close</span>
                                            </a><!-- END OF CLOSER / OPENER -->

                                        </div><!-- END OF THE REVEAL CONTAINER -->

                            </li><li class="sb-showcase-skin" style="width: 173px;">
                                        <div class="mediaholder ">
                                            <div class="mediaholder_innerwrap">
                                                <img alt="Yowie" src="img/habitat/yowie.png">
                                            </div>
                                        </div>


                                        <!-- SOME ALWAYS VISIBLE CONTENT -->
                                        <div class="detailholder">
                                            <h4 class="showbiz-title txt-center"><a href="#">Yowie</a></h4>
                                            <div class="divide5"></div>
                                            <p class="txt-center">He is always so annoyed of the penguins making noise</p>
                                        </div>

                                        <!-- THE REVEAL CONTAINER - OPENING IN FULLWIDTH -->
                                        <div class="reveal_container tofullwidth">

                                            <!-- THE REVEL HIDDEN / VISIBLE CONTAINER -->
                                            <div class="reveal_wrapper">
                                                <!-- THE HEIGHT ADJUSTER CONTAINER -->
                                                <div class="heightadjuster table">

                                                    <!-- TABLE CONSTRUCT FOR LEFT / RIGHT CONTENTS -->
                                                    <div class="table-cell onethird">
                                                            <img alt="Yowie" src="img/habitat/yowie.png" width="100%">
                                                    </div>

                                                    <!-- CONTENT IN TABLE -->
                                                    <div class="table-cell pl20">
                                                        <h3 class="showbiz-title large">Yowie <span class="sub_title">The Giant Yeti</span></h3>
                                                        <div class="divide20"></div>
                                                        <p>Tall , Lazy &amp; huge. His hand is his strength. He is often seen fishing or maybe even having a Sauna Bath in the hot water springs of the Ice Mountain. He is always so annoyed of the penguins making noise and troubling him that he tries to catch hold of them.</p>
                                                        <div class="divide20"></div>
                                                    </div><!-- END OF CONTENT  -->

                                                </div><!-- END OF HEIGHT ADJUSTER CONTAINER -->
                                            </div><!-- END OF REVEAL HIDDEN/VISIBLE CONTAINER -->

                                            <!-- THE CLOSER / OPENER FUNCTION -->
                                            <a class="reveal_opener opener_big_grey">
                                                <span class="openme">Show Details</span>
                                                <span class="closeme">Close</span>
                                            </a><!-- END OF CLOSER / OPENER -->

                                        </div><!-- END OF THE REVEAL CONTAINER -->
                            </li><li class="sb-showcase-skin" style="width: 173px;">
                                        <div class="mediaholder ">
                                            <div class="mediaholder_innerwrap">
                                                <img alt="Toe" src="img/habitat/toe.png">
                                            </div>
                                        </div>


                                        <!-- SOME ALWAYS VISIBLE CONTENT -->
                                        <div class="detailholder">
                                            <h4 class="showbiz-title txt-center"><a href="#">Toe</a></h4>
                                            <div class="divide5"></div>
                                            <p class="txt-center">The most loved ones among everyone</p>
                                        </div>

                                        <!-- THE REVEAL CONTAINER - OPENING IN FULLWIDTH -->
                                        <div class="reveal_container tofullwidth">

                                            <!-- THE REVEL HIDDEN / VISIBLE CONTAINER -->
                                            <div class="reveal_wrapper">
                                                <!-- THE HEIGHT ADJUSTER CONTAINER -->
                                                <div class="heightadjuster table">

                                                    <!-- TABLE CONSTRUCT FOR LEFT / RIGHT CONTENTS -->
                                                    <div class="table-cell onethird">
                                                            <img alt="Toe" src="img/habitat/toe.png" width="100%">
                                                    </div>

                                                    <!-- CONTENT IN TABLE -->
                                                    <div class="table-cell pl20">
                                                        <h3 class="showbiz-title large">Toe <span class="sub_title">The Cutie Heart</span></h3>
                                                        <div class="divide20"></div>
                                                        <p>They are known to spread love. The most loved ones among everyone. They are Small, Tiny &amp; cuddly. Their common sight is " Laughing &amp; Rolling on the floor.” ROFL suits them pretty well. There are moments when they laugh, roll and form a giant ice ball and destroy an Ice Hockey Match.</p>
                                                        <div class="divide20"></div>
                                                    </div><!-- END OF CONTENT  -->

                                                </div><!-- END OF HEIGHT ADJUSTER CONTAINER -->
                                            </div><!-- END OF REVEAL HIDDEN/VISIBLE CONTAINER -->

                                            <!-- THE CLOSER / OPENER FUNCTION -->
                                            <a class="reveal_opener opener_big_grey">
                                                <span class="openme">Show Details</span>
                                                <span class="closeme">Close</span>
                                            </a><!-- END OF CLOSER / OPENER -->

                                        </div><!-- END OF THE REVEAL CONTAINER -->

                            </li><li class="sb-showcase-skin" style="width: 173px;">

                                        <!-- THE MEDIA HOLDER HERE -->
                                        <div class="mediaholder ">
                                            <div class="mediaholder_innerwrap">
                                                <img alt="Polka" src="img/habitat/polka.png">
                                            </div>
                                        </div><!-- END OF MEDIA CONTAINER -->

                                        <!-- SOME ALWAYS VISIBLE CONTENT -->
                                        <div class="detailholder">
                                            <h4 class="showbiz-title txt-center"><a href="#">Polka</a></h4>
                                            <div class="divide5"></div>
                                            <p class="txt-center">She has her own fashion Line</p>
                                        </div><!-- END OF DEATIL CONTAINER -->

                                        <!-- THE REVEAL CONTAINER - OPENING IN FULLWIDTH -->
                                        <div class="reveal_container tofullwidth">

                                            <!-- THE REVEL HIDDEN / VISIBLE CONTAINER -->
                                            <div class="reveal_wrapper">
                                                <!-- THE HEIGHT ADJUSTER CONTAINER -->
                                                <div class="heightadjuster table">

                                                    <!-- TABLE CONSTRUCT FOR LEFT / RIGHT CONTENTS -->
                                                    <div class="table-cell onethird">
                                                            <img alt="Polka" src="img/habitat/polka.png">
                                                    </div>

                                                    <!-- CONTENT IN TABLE -->
                                                    <div class="table-cell pl20">
                                                        <h3 class="showbiz-title large">Polka <span class="sub_title">The Stylista</span></h3>
                                                        <div class="divide20"></div>
                                                        <p>A middle aged lady , who thinks there's no one more stylish than her. She considers herself to be a fashion police. She has her own fashion Line called " Polka ". She Walks around with an attitude filled look with sunglasses &amp; Fashionista Bags Swaying her booties.</p>
                                                        <div class="divide20"></div>                                                        
                                                    </div><!-- END OF CONTENT  -->

                                                </div><!-- END OF HEIGHT ADJUSTER CONTAINER -->
                                            </div><!-- END OF REVEAL HIDDEN/VISIBLE CONTAINER -->

                                            
                                            <a class="reveal_opener opener_big_grey">
                                                <span class="openme">Show Details</span>
                                                <span class="closeme">Close</span>
                                            </a>

                                        </div>
                                </li></ul>
                        <div class="sbclear"></div>
                    </div> 
                    <div class="sbclear"></div>
                </div>
            </div>
        
        <hr>    
    </div>
    
    
 <script type="text/javascript">
    jQuery(document).ready(function() 
    {
        jQuery.noConflict(true);
        jQuery('#habitat').showbizpro({
            dragAndScroll:"on",
            visibleElementsArray:[6,3,2,1],
            carousel:"on",
            speed:800,
            easing:"Back.easeOut"
        });
    });
</script>