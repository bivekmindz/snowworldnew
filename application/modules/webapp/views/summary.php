<section class="step_payment">
   <div class="container main-container">
   <div id="default-screen" class="content-part">
   <span id="updateFullPage">
   <section class="row block" id="ticketsAddOnesBlock">
      <div id="searchResult">
         <span id="ticketsAddOnsOpen"></span>
         <span id="step1Done">
            <div id="ticketsAddOnsDone" class="ticketsAddOnsDone doneBlock aboveAll">
               <div class="stepHeader stepDone stepDoneHere relative row" id="ticketsAddOnsInfo">
                  <?php require_once('helper/sessiondatetimenoofvisiter.php'); ?>
                  <form id="j_idt135" name="j_idt135" method="post" action="" enctype="application/x-www-form-urlencoded">
                     <input type="hidden" name="j_idt135" value="j_idt135">
                     <a id="j_idt135:step1Edit" href="packages" class="ui-commandlink ui-widget edit" onclick="">
                     <span class="icon-pencil"></span></a>
                     <input type="hidden" name="javax.faces.ViewState" value="4914754771058361795:4671725937336393998"
                        autocomplete="off">
                  </form>
                  <h2 class="relative">
                     <span class="steps"><span class="stepNum">Step
                     1</span></span>
                  </h2>
                  <p class="steps-title">Search
                  </p>
               </div>
            </div>
         </span>
      </div>
   </section>
   <section class="row block" id="ticketsAddOnesBlock">
      <div id="searchResult">
         <span id="ticketsAddOnsOpen"></span>
         <span id="step1Done">
            <div id="ticketsAddOnsDone" class="ticketsAddOnsDone doneBlock aboveAll">
               <div class="stepHeader stepDone stepDoneHere relative row" id="ticketsAddOnsInfo">
                  <?php require_once('helper/ticketcost.php'); ?>
                  <form id="j_idt135" name="j_idt135" method="post" action="" enctype="application/x-www-form-urlencoded">
                     <input type="hidden" name="j_idt135" value="j_idt135">
                     <a id="j_idt135:step1Edit" href="packagesstep" class="ui-commandlink ui-widget edit" onclick="">
                     <span class="icon-pencil"></span></a>
                     <input type="hidden" name="javax.faces.ViewState" value="4914754771058361795:4671725937336393998"
                        autocomplete="off">
                  </form>
                  <h2 class="relative">
                     <span class="steps"><span class="stepNum">Step
                     2</span></span>
                  </h2>
                  <p class="steps-title">Package
                  </p>
               </div>
            </div>
         </span>
      </div>
   </section>
   <section class="row block" id="ticketsAddOnesBlock">
      <div id="searchResult">
         <span id="ticketsAddOnsOpen"></span>
         <span id="step1Done">
            <div id="ticketsAddOnsDone" class="ticketsAddOnsDone doneBlock aboveAll">
               <div class="stepHeader stepDone stepDoneHere relative row" id="ticketsAddOnsInfo">
                  <?php require_once('helper/addonscost.php'); ?>
                  <form id="j_idt135" name="j_idt135" method="post" action="" enctype="application/x-www-form-urlencoded">
                     <input type="hidden" name="j_idt135" value="j_idt135">
                     <a id="" href="addones" class="ui-commandlink ui-widget edit" onclick="">
                     <span class="icon-pencil"></span></a>
                     <input type="hidden" name="" value="" autocomplete="off">
                  </form>
                  <h2 class="relative">
                     <span class="steps"><span class="stepNum">Step
                     3</span></span>
                  </h2>
                  <p class="steps-title">Addons
                  </p>
               </div>
            </div>
         </span>
      </div>
   </section>
   <!-- <form action="" method="post"> -->
   <section class="row block" id="ticketsAddOnesBlock">
      <div id="searchResult">
         <span id="ticketsAddOnsOpen">
            <div id="ticketsAddOnsOpen" class="openBlock aboveAll">
               <span id="step1Header">
                  <div class="stepHeader stepOpenHere row stepOpen">
                     <p class="fRight subHead">Would this be all?</p>
                     <h2 class="relative">
                        <span class="steps"><span class="stepNum">Step
                        4</span></span>
                     </h2>
                     <p class="steps-title Summary-ski">Summary</p>
                  </div>
               </span>
               <span id="refreshStep1Panel">
                  <span id="step1AddonDisp">
                     <div id="TicketSummaryBlock" class="infoBlock relative addOnsBlock">
                        <span id="updateStep2">
                           <div class="summaryTitle">
                              <div class="row">
                                 <div class="col-xs-12 col-md-4 width1001" style="padding:0;">
                                    <div>
                                       Booking Date: <span>
                                       <?php
                                          $date_array1 = explode("-", date("d-M-Y")); // split the array
                                          $var_day1 = $date_array1[0]; //day seqment
                                          $var_month1 = $date_array1[1]; //month segment
                                          $var_year1 = $date_array1[2]; //year segment
                                          $new_date_format1 = strtotime("$var_year1-$var_month1-$var_day1"); // join them together
                                          $mydate = date("Y-M-d");
                                          echo  date('D');										//echo substr(date('l', strtotime($mydate));
                                          $input1 = ("$var_year1$var_month1$var_day1");
                                          echo date(' jS M Y', $new_date_format1);
                                          ?>(<?php
                                          $date = '19:24:15 ';
                                          echo date('h:i:s A');
                                          //echo date("H:i:s") . "\n"; ?>)
                                       </span>
                                    </div>
                                 </div>
                                 <div class="col-xs-12 col-md-3 width1001" style="padding:0;">
                                    <div>
                                       Date of visit: <span><?php
                                          $trim_data = explode('-',$this->session->userdata('destinationType') );array_shift( $trim_data );
                                          echo date(' jS M Y', strtotime( $this->session->userdata('txtDepartDate') ) ); ?></span>
                                    </div>
                                 </div>
                                 <div class="col-xs-12 col-md-3 width1001" style="padding:0;">
                                    Session Time: 
                                    <span>
                                       <?php $variabletotal=0; ?>
                                       <?php
                                          // to show only on front end - not to pass in the db
                                          echo completeTimeSlotWithAmPm( date('Y-m-d', strtotime( $this->session->userdata('txtDepartDate') ) ) , getTimeSlotInArray( $this->session->userdata('destinationType') ) )
                                          ?>
                                       <?php //echo implode('-', $trim_data ) ; ?>
                                       <div>
                                          <input type="hidden" name="txfromhrs" id="txfromhrs" value="<?php  echo count($to_render_array) ? $to_render_array[0] : ''; ?>">
                                          <input type="hidden" name="txtfrommin"  id= "txtfrommin" value="<?php echo count($to_render_array) ? $to_render_array[1] : ''; ?>">
                                          <input type="hidden" name="txttohrs"  id= "txttohrs" value="<?php echo count($to_render_array) ? $to_render_array[2] : ''; ?>">
                                          <input type="hidden" name="txttomin" id="txttomin" value="<?php echo count($to_render_array) ? $to_render_array[3] : ''; ?>">
                                          <input type="hidden" name="txtfromd" id="txtfromd" value="<?php echo $this->session->userdata('txtDepartDate'); ?> ">
                                          <input type="hidden" name="txttod" id="txttod" value="<?php echo $apmvar1; ?> ">
                                       </div>
                                 </div>
                                 <div class="col-xs-12 col-md-2 width1001" style="padding:0;">
                                 <div>
                                 Tracking ID: <span><?php echo($this->session->userdata('uniqid')); ?></span>
                                 </div>
                                 </div>
                              </div>
                           </div>
                           <div class="ticketsWithAddOns">
                           <?php foreach( $final_selected_package_data as $v ){  ?>
                           <!-- loop start here -->
                           <div class="row ticketdescription mart20">
                           <div class="col-xs-1">
                           <?php if (($this->session->userdata('cartpackageimage')) != '') { ?>
                           <img style="max-width: 65px;" src="assets/admin/images/<?php echo $v->package_image; ?>">
                           <?php } else { ?>
                           <img style="max-width: 65px;" src="img/1463897752358.jpeg">
                           <?php } ?>
                           </div>
                           <div class="col-xs-4">
                           <div class="ticket-type"><?php echo $v->package_name; ?>
                           </div>
                           </div>
                           <div class="col-xs-3 txtcenter visitorsCount">
                           No. Of Visitors: <span><?php echo ($v->qty) ? $v->qty : 0 ; ?></span>
                           </div>
                           <div class="col-xs-4 txtright">
                           <p class="reupespps">
                           <span>
                           <i class="icon-rupee" aria-hidden="true"></i>
                           </span>
                           <?php echo ( (int) $v->package_price * (int) $v->qty ) ? ( (int) $v->package_price * (int) $v->qty ) : 0; ?>
                           </p>
                           </div>
                           </div>
                           <?php } ?>
                           <!-- loop end here -->
                           <?php
                              $cartaddoneimage = explode(',', $this->session->userdata('cartaddoneimage'));
                              $cartaddonname = explode(',', $this->session->userdata('cartaddonname'));
                              $cartaddonqty = explode(',', $this->session->userdata('cartaddonqty'));
                              $cartaddonprice = explode(',', $this->session->userdata('cartaddonprice'));
                              $cartaddoneid = explode(',', $this->session->userdata('cartaddoneid'));
                              $valaddoneqty = 0;
                              $valadd = explode(",", $this->session->userdata('cartaddonqty'));
                              ?>
                           <input type="hidden" id="cartaddoneiddata" name="cartaddoneid" value="<?php echo $this->session->userdata('cartaddoneid') ?>">
                           <?php
                              foreach ($valadd as $key => $value) {
                              $valaddoneqty += $value;
                              }
                              if (($valaddoneqty) > 0) {
                              for ($i = 0; $i <= (count($cartaddonqty) - 1); $i++) {
                              ?>
                           <div class="row ticketdescription mart20">
                           <div class="col-xs-1">
                           <?php if (($this->session->userdata('cartaddoneimage')) != '') { ?>
                           <img style="max-width: 65px;" src="assets/admin/images/<?php echo $cartaddoneimage[$i]; ?>">
                           <?php } else { ?>
                           <img style="max-width: 65px;" src="img/1463897752358.jpeg">
                           <?php } ?>
                           <input type="hidden" name="ccaddonid[]" value="<?php echo $cartaddoneid[$i]; ?>">
                           <input type="hidden" name="ccartpackageimage[]"value="<?php echo $cartaddoneimage[$i]; ?>">
                           </div>
                           <div class="col-xs-4">
                           <div class="ticket-type"><?php echo $cartaddonname[$i]; ?></div>
                           <input type="hidden" name="ccartaddonname[]" value="<?php echo $cartaddonname[$i]; ?>">
                           </div>
                           <div class="col-xs-3 txtcenter visitorsCount">
                           Qty: <span><?php echo $cartaddonqty[$i]; ?></span>
                           <input type="hidden" name="ccartaddonqty[]" value="<?php echo $cartaddonqty[$i]; ?>">
                           </div>
                           <div class="col-xs-4 txtright">
                           <p class="reupespps">
                           <span>
                           <i class="icon-rupee" aria-hidden="true"></i></span>
                           <?php $variabletotal+= ( $cartaddonprice[$i] * $cartaddonqty[$i] ); echo ( $cartaddonprice[$i] * $cartaddonqty[$i] ); ?> </p>
                           <input type="hidden" name="ccartaddonprice[]" id="ccartaddonprice" value="<?php echo $cartaddonprice[$i]; ?>">
                           </div>
                           </div>
                           <?php }
                              }
                              ?>
                           <div class="row ticketTotalPrice">
                           <div class="col-lg-5 col-md-5 col-xs-12" >
                           <div class="ticket-type">Internet Handling Charges </div>
                           </div>
                           <div class="col-xs-3 txtcenter" >
                           <div class="">
                           No of Visitors : <?php echo $package_total_qty; ?>
                           </div>
                           </div>
                           <div class="col-xs-4 txtright" style="padding-right:6px;">
                           <p class="reupespps marhhe">
                           <span><i class="icon-rupee" aria-hidden="true"></i></span>
                           <?php  //echo $intchar; ?>
                           <input type="hidden" name="internethandlingcharges" value='<?php echo $internethandlingcharge_revised; ?>'>
                           <?php echo $internethandlingcharge_revised; ?>
                           </p>
                           </div>
                           </div>
                           <?php
                              if($this->session->userdata('total_promo_price') !='')
                              {
                              ?>
                           <div class="row ticketTotalPrice">
                           <div class="col-lg-5 col-md-5 col-xs-12" style="padding-left: 20px;">
                           <div class="ticket-type">Promocode </div>
                           </div>
                           <div class="col-lg-3 col-md-3 col-xs-12 text-center">
                           	<span>(Promocode Applied: "<?php echo  $this->session->userdata('promocode_name'); ?>")
                           	</span>
                           </div>
                           <div class="col-xs-4 txtright">
                           <p class="reupespps marhhe ">
                           <span>
                           <i class="icon-rupee"
                              aria-hidden="true"></i></span>-<?php echo $this->session->userdata('saving_price_promocode') ?>
                           </p>
                           <input type="hidden" name="p_codename" value="<?php echo $discountamount ; ?>">
                           <input type="hidden" name="promo_codename" value="<?php echo  $this->session->userdata('ppcodename'); ?>">
                           </div>
                           </div>
                           <?php
                              }elseif ($promocode->scalar == 'Something Went Wrong' or $msg == 'Max Allowed Exceed' or $msg == 'Invalid Promocode' or $msg == 'Promo Not Add') {
                              	
                              }else {
                              	if ($msg == 'Max Allowed Exceed') {
                              	}else {
                              ?>
                           <div class="row ticketTotalPrice">
                           <div class="col-xs-6" style="padding-left: 20px;">
                           <div class="ticket-type">Promocode <span>(Promocode Applied: "<?php echo  $this->session->userdata('ppcodename'); ?>")</span></div>
                           </div>
                           <div class="col-xs-6 txtright">
                           <p class="reupespps ">
                           <span>
                           <i class="icon-rupee"
                              aria-hidden="true"></i></span>-<?php if ($this->session->userdata('pp_type') == 'flat') {
                              echo $discountamount = ($this->session->userdata('pp_discount')*$this->session->userdata('packageqtyval'));
                              } else {
                              echo $discountamount = (($discountamount = $this->session->userdata('pp_discount') * $this->session->userdata('cartsubtotal')) / 100);
                              } ?></p>
                           <input type="hidden" name="p_codename" value="<?php echo $discountamount ; ?>">
                           <input type="hidden" name="promo_codename" value="<?php echo  $this->session->userdata('ppcodename'); ?>">               </div>
                           <?php } } ?>
                           <div class="row ticketTotalPrice">
                           <div class="col-xs-6" style="padding-left: 20px;">
                           <div class="ticket-type">Total</div>
                           </div>
                           <div class="col-xs-6 txtright" style="padding-right:6px;">
                           <p class="reupespps marhhe">
                           <span><i class="icon-rupee"
                              aria-hidden="true"></i></span><?php echo !empty($this->session->userdata('total_promo_price')) ? $this->session->userdata('total_promo_price') : $grand_total; ?></p>
                           </div>
                           </div>
                           <div class="row " id="promopricehide">
                           <div class="col-xs-6" style="padding-left: 20px;">
                           <div class="ticket-type">Promocode price</div>
                           </div>
                           <div class="col-xs-6 txtright" style="padding-right:6px;">
                           <p class="reupespps marhhe">
                           <span><i class="icon-rupee"
                              aria-hidden="true"></i></span><span id="promoprice"></span></p>
                           </div>
                           </div>
                           <div class="row">
                           <?php if ($promocode->scalar == 'Something Went Wrong' or $msg == 'Max Allowed Exceed' or $msg == 'Invalid Promocode'){ ?>
                           <div style="padding-top: 10px;"></div>
                           <form id="promoForm" name="promoForm" method="post" action="" enctype="application/x-www-form-urlencoded">
                           <input type="hidden" name="promoForm" value="promoForm">
                           <span id="promoForm:dispPromoPanel">
                           <div class="ticketsWithAddOns">
                           <div class="row">
                           <div class="txtright">
                           <div id="promocode" class="col-xs-12">
                           <div class="addOnsListBlock">
                           <div id="promoCode" class="widget-container ticketDetailsList">
                           <div class="widget-content">
                           <div class="addOnsListContainer">
                           <div class="row">
                           <?php if ($promocode->scalar == 'Something Went Wrong' or $msg == 'Max Allowed Exceed' or $msg == 'Invalid Promocode') { ?>
                           <div class="flashmsg"
                              style="text-align: left;padding-left: 59px;">  <?php echo "Invalid Promo code."; ?> </div>
                           <?php } ?>
                           <div class="col-xs-12">
                           </div>
                           </div>
                           <!--              Sachin         TESTING  -->
                           <div class="col-xs-8 confirm-order">
                           <div class="promocode"><input name="promoFormdata" type="text"
                              role="textbox"
                              placeholder="Enter Your Promo Code:"></div>
                           <p class="help-block"></p>
                           </div>
                           </div>
                           <div class="col-xs-4 calculate-btn"><button id="calculatePromo"
                              name="calculatePromo"
                              class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only btn btn-custom btn-prime"
                              role="button"
                              aria-disabled="false">
                           <span class="ui-button-text ui-c">Calculate</span></button>
                           </div>
                           <!--                 END               -->
                           <!-- <div class="col-xs-8 confirm-order">
                              <div class="promocode"><input name="promoFormdata" type="text"
                              role="textbox"
                              placeholder="Enter Your Promo Code:"></div>
                              	
                              	<p class="help-block"></p>
                              </div>
                              </div>
                              <div class="col-xs-4 calculate-btn"><button id="promoForm:calculatePromo"
                              name="promoForm:calculatePromo"
                              class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only btn btn-custom btn-prime"
                              type="submit" role="button"
                              aria-disabled="false">
                              <span class="ui-button-text ui-c">Calculate</span></button>
                              </div> -->
                           <div class="clearfix"></div>
                           </div>
                           </div>
                           </div>
                           <div><label></label>
                           </div>
                           </div>
                           </div>
                           </div>
                           </div>
                           </div>
                           </span></form>
                           <?php } else  { ?>
                           <div class="txtright cleck_mor">
                           <?php if ($this->session->userdata('ppcodename') != '') { ?>
                           <button id="promoForm:calculatePromo" name="removepromocode"
                              class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only btn btn-custom btn-prime"
                              type="submit" role="button" aria-disabled="false" value="removepromocode">
                           <span class="ui-button-text ui-c">Remove Promocode</span></button>
                           <?php } else { ?>
                           <?php if($this->session->userdata('total_promo_price') !=''){
                              ?>
                           <a onclick="myFunction_reset()" id='promodata'>Disable Promocode</a>
                           <?php } else{ ?>
                           <a onclick="myFunction()" id='promodata'>Have a promo code to redeem?
                           Click here</a><?php } ?>
                           <?php } ?>
                           </div>
                           <div id="promodatalist">
                           <div id="myDIV" style="display:none">
                           <!-- checkpromoForm -->
                           <!--          Sachin      -->
                           <div class="col-lg-10 col-md-10 col-xs-12 confirm-order">
                           <div class="promocode">
                           <input name="promocode" id="promocode" type="text"
                              role="textbox" placeholder="Enter Your Promo Code:"></div>
                           <span id="promocodemessage"></span>
                           <p class="help-block"></p>
                           </div>
                           <div class="col-lg-2 col-md-2  col-xs-12 calculate-btn text-center">
                           <button id="calculatePromo" onclick="checkpromoForm();" name="calculatePromo" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only btn btn-custom btn-prime"
                              type="submit" role="button"
                              aria-disabled="false">
                           <span class="ui-button-text ui-c">Calculate</span></button>
                           </div>
                           </div>
                           </div>
                           <input type="hidden" value="<?php echo ($this->session->userdata('skiindia')) ? $this->session->userdata('skiindia') :  $this->session->userdata('skiindia_guest') ?>" id="uid" name="uid">
                           <input type="hidden" value="<?php echo ($this->session->userdata('skiindia')) ? 'User' :  'Guest' ?>" id="type" name="type">
                           <!--                   END                 -->
                           <!--
                              <form id="promoForm" name="promoForm" method="post" action="" onclick="return checkpromoForm();" enctype="application/x-www-form-urlencoded">
                              	<input type="hidden" name="promoForm" value="promoForm">
                              	<span id="promoForm:dispPromoPanel">
                              							<div class="ticketsWithAddOns">
                              									<div class="row">
                              											<div class="txtright">
                              													<div id="promocode" class="col-xs-12">
                              													<div class="addOnsListBlock">
                              															<div id="promoCode" class="widget-container ticketDetailsList">
                              																	<div class="widget-content">
                              																			<div class="addOnsListContainer">
                              																					<div class="row">
                              											<?php //if ($promocode->scalar == 'Something Went Wrong') {
                                 //echo "Invalid Promo code.";
                                 } ?>
                              											<div class="col-xs-12">
                              											</div>
                              										</div>
                              										<div class="col-xs-8 confirm-order">
                              											<div class="promocode"><input name="promoFormdata" type="text"
                              												role="textbox"
                              											placeholder="Enter Your Promo Code:"></div>
                              											
                              											<p class="help-block"></p>
                              										</div>
                              									</div>
                              									<div class="col-xs-4 calculate-btn">
                              										<button id="promoForm:calculatePromo"  name="promoForm:calculatePromo" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only btn btn-custom btn-prime"
                              										type="submit" role="button"
                              										aria-disabled="false">
                              										<span class="ui-button-text ui-c">Calculate</span></button>
                              									</div>
                              									<div class="clearfix"></div>
                              								</div>
                              							</div>
                              						</div>
                              						<div><label></label>
                              					</div>
                              					
                              				</div>
                              			</div>
                              		</div>
                              	</div>
                              </div>
                              <div class="row ticketPromoCode" style="margin-right: 155px !important;">
                              </div>
                              </span>
                              <input type="hidden" name="javax.faces.ViewState" value="" # autocomplete="off"></form> -->
                           <!--    </div> -->
                           <?php // } ?>
                           <script>
                              function myFunction() {
                              var x = document.getElementById('myDIV');
                              var y = document.getElementById('promodata');
                              if (x.style.display === 'none') {
                              x.style.display = 'block';
                              y.style.display = 'none';
                              } else {
                              x.style.display = 'none';
                              }
                              }
                           </script>
                           <form action ="" method="post">
                           <div class="row">
                           <div class="col-xs-12 txtcenter">
                           <div class="continue">
                           <input type="hidden" name="subtotal" value="<?php echo $variabletotal; ?>">
                           <input type="hidden" name="discountamount" value="<?php if ($discountamount != '') {
                              echo $discountamount;
                              } else {
                              echo "0";
                              } ?>">
                           <input type="hidden" name="total" id="amount_s1"
                              value="<?php echo $variabletotal - $discountamount + $taxvalue + $intchar; ?>">
                           <input type="hidden" id="txtDepartDate1" name="txtDepartDate1"
                              value="<?php echo $this->session->userdata['txtDepartDate']; ?>">
                           <input type="hidden"  id="txtDepartdata" name="txtDepartdata"
                              value="<?php echo $this->session->userdata['destinationType']; ?>">
                           <button id="ordsummery" onclick="return myFunction_1()" name="submit" value="final add"
                              class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only btn btn-custom btn-prime"
                              type="submit" role="button" aria-disabled="false"><span
                              class="ui-button-text ui-c">CONTINUE TO CHECKOUT</span></button>
                           <input type="hidden" name="" value="" autocomplete="off">
                           </a>
                           </div>
                           </div>
                           </div>
                           </form>
                           </span>
                           </div>
                        </span>
                  </span>
                  </div>
               </span>
               </div>
   </section>
   <!--
      </form> -->
   <script>
      function myFunction_1() {
      //console.log('hi');
      //alert('test');return false;
      var str3 = $("#amount_s1").val();
      var str4 = "<?php echo $variabletotal - $discountamount + $taxvalue + $intchar; ?>";
      if (str3 != str4) {
      alert('stay away from our code');
      return false;
      }
      }
   </script>
   <!-- 	<section id="paymentBlock" class="row block relative"><span id="beforePanel3">
      <div id="summaryClose" class="stepHeader stepClose row summaryClose closeBlock">
      	<p class="fRight subHead"></p>
      	<h2 class="relative">
      	<span class="steps"><span class="stepNum">Step 5</span></span>
      	</h2>
      	<p class="steps-title">Payment</p>
      	</div></span><span id="step3open"></span>
      </section> -->
   </span>
   </div>
   </div>
</section>
<script>
   $('#promopricehide').css("display","none");
   $('#hidepromoname').hide();
   function checkpromoForm(){
   var promocode     =      $('#promocode').val();
   var txtDepartDate =      $('#txtDepartDate1').val();
   var uid           =      $('#uid').val();
   var txfromhrs     =      $('#txfromhrs').val();
   var txtfrommin    =      $('#txtfrommin').val();
   var txttohrs      =      $('#txttohrs').val();
   var txttomin      =      $('#txttomin').val();
   var txtfromd      =      $('#txtfromd').val();
   var txttod        =      $('#txttod').val();
   var type        =      $('#type').val();
   var totalprice        =      $('#amount_s1').val();
   var cartaddoneiddata        =      $('#cartaddoneiddata').val();
   $.ajax({
   url: '<?php echo base_url()?>webapp/Summary/checkpromocode',
   type: "POST",
   data: {'promocode': promocode,'txtDepartDate': txtDepartDate,'uid': uid,'txfromhrs': txfromhrs,
   'txtfrommin': txtfrommin,'txttohrs': txttohrs,'txttomin': txttomin,'txtfromd': txtfromd,'txttod': txttod,'type':type,'totalprice':totalprice,'cartaddoneiddata':cartaddoneiddata},
   success: function(data){//console.log(data);return false;
   if(data == 0){
   $('#promocodemessage').html('Invalid promocode').css("background-color:red");
   }else{
   location.reload();
   }
   }
   });
   }
   //Session Unset
   function myFunction_reset(){
   $.ajax({
   url: '<?php echo base_url()?>webapp/Summary/resetsession',
   type: "POST",
   success: function(data){
   window.location.href  ='<?php echo base_url().'summary' ?>';
   }
   });
   }
</script>
<style type="text/css">
   #promodatalist{
   width: 100%; float: left; margin: 0 0 10px 0;
   }
   #promodatalist .ui-button{ margin: 10px 0 0 0; }	
</style>