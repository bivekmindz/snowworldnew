<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="<?php echo base_url()?>assets/js/jquery-1.9.1.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/moment.js"></script>
<!-- <script src="<?php echo base_url()?>assets/js/date.js"></script> -->

<style>
    input.error{border:1px solid red!important;}
    select.error{border:1px solid red!important;}
    textarea.error{border:1px solid red!important;}
    .error{border:1px solid red;}
    label.error{border:0px solid red!important; color:red; font-weight: normal; display:inline; }
    .sessionerror{border:0px solid red!important; color:red; font-weight: normal; }
    .guest-details input[type=text], select, textarea{ color:#fff;}
</style>

<style>
	.nav-tabs>li{ width:100%;}

	{    left: 100%;
		top: 50%;
		border: solid transparent;
		content: " ";
		height: 0;
		width: 0;
		position: absolute;
		pointer-events: none;
		border-color: rgba(77,77,77,0);
		border-left-color: #4d4d4d;
		border-width: 25px;
		margin-top: -25px;
	}


	.nav-tabs>li>a{ padding: 11px 13px 11px 13px;}

	.nav-tabs>li {
		background-color: #c4c4c4;
		border-right: 1px solid #aaaaaa;
	}

	.nav-tabs>li.active>a, .nav-tabs>li.active>a:hover, .nav-tabs>li.active>a:focus {
		color: #ffffff;
		background-color: #335f9b;
		position:relative;
	}

	.tab-content{ background-color:inherit;}


	label {
		color: #999;
		float:left;
	}

	.googel1 {
		background-color: #DF4A32 !important;
	}

	.linkedin1 {
		background-color: #0177B5 !important;
	}
	.nav-tabs>li.active::after {
		position: absolute;
		right: 0px;
		content: '';
		top: 0;
		bottom: 0;
		position: absolute;
		right: -15px;
		content: '';
		bottom: 0;
		width: 0;
		height: 0;
		border-top: 23px solid transparent;
		border-bottom: 23px solid transparent;
		border-left: 15px solid #335f9b;
	}


</style>

<style>
	input.error{border:1px solid red!important;}
	select.error{border:1px solid red!important;}
	textarea.error{border:1px solid red!important;}
	.error{border:1px solid red;}
	label.error{border:0px solid red!important; color:red; font-weight: normal; display:inline; }
	.sessionerror{border:0px solid red!important; color:red; font-weight: normal; }
</style>

<!--Start Compare part      ng-app="snowWorldApp" ng-controller="packageCtrl" -->
<section class="step_payment" >
    <div class="container main-container" >
		
      <div id="default-screen" class="content-part"><span id="updateFullPage">

      <section class="row block" id="ticketsAddOnesBlock">
        <div id="searchResult"><span id="ticketsAddOnsOpen">
          <div id="ticketsAddOnsOpen" class="openBlock aboveAll"><span id="step1Header">
            <div class="stepHeader stepOpenHere row stepOpen">
              <p class="fRight subHead">We are glad you are here. </p>
                <h2 class="relative">
                  <span class="steps"><span class="stepNum">Step 1</span></span>
                </h2>

                  <p class="steps-title title1">Search</p>
				
			
                <!-- jQuery Form Validation code -->
				
                <!--Start Compare part-->
				
                </div></span>
			  <span id="refreshStep1Panel">
				  <span id="step1AddonDisp">

                <div id="AddOnsBlock" class="infoBlock relative addOnsBlock">
                  <div class="ticketsWithAddOns">
                    <div class="row  mart20">

                      <div class="order_titile">

                        <form  role="form" name="excursionSearchForm" onsubmit="return validateModifySearch()" action="packages" method="post">

                          <div class="col-md-12 col-md-offset-0">
                              <div class="flashmsg"><?php echo $emsg; ?></div>


                            <div class="row">
								
                              <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                                <div class="form-group formmm animated fadeInUp">
									
                                  <input id="datepicker12" data-date-format="dd/mm/yyyy" class="date-pick form-control formmm" name="txtDepartDate" type="text" autocomplete="off" value="<?php //echo date("d-m-Y"); ?>" placeholder="Select Date">
                                  <span class="input-icon1">
									  <i class="icon-calendar"></i>
									</span>
                                </div>
                              </div>

                           <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                                <div class="form-group  animated fadeInUp  ">


                                  <select class="form-control formmm" name="destinationType" id="sessionType1">
									  
                                    <option value="" disabled="disabled" selected="selected">Select Time</option>
                                  </select>
									
                                <span class="input-icon1"><i class="icon-clock"></i></span>
                              </div>
                            </div>


                            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                              <div class="form-group formmm animated fadeInUp hide">
                                <input type="number" max="200" min="1" class="form-control formmm" id="inputVisitor1" name="ddAdult" placeholder="No. of visitor" value="1">
                                <?php //echo $this->session->userdata('ddAdult'); ?> 
                                <span class="input-icon1"><i class="icon-adult"></i></span>
                              </div>
                            </div>



                            <div class="col-lg-3 col-md-3 bookbtn">
                              <div class="form-group formmm fadeInUp">
													
                               <input type="submit" value="Search"  class="animated fadeInUp btn_full" id="submit-booking">
                               <span class="animated fadeInUp input-icon_nobg"><i class="icon-search"></i></span>
                             </div>
                           </div>



                         </div>

                       </div>  </form>       </div>

                     </div>




                   </div>
                 </div>



               </span></span>
             </div></span>

           </div>
         </section>




         <section class="row block relative" id="summaryBlock">
          <div id="searchSummary"><span id="beforePanel2">
            <div class="stepHeader stepClose row summaryClose closeBlock" id="summaryClose" style="display: block;">
              <p class="fRight subHead"></p>
              <h2 class="relative">
                <span class="steps"><span class="stepNum">Step
                  2</span></span>
                </h2>
                <p class="steps-title title2">Package</p>
              </div></span><span id="step2open"></span><span id="step2Completed"></span>


            </div>
          </section>




          <section id="paymentBlock" class="row block relative"><span id="beforePanel3">
            <div id="summaryClose" class="stepHeader stepClose row summaryClose closeBlock">
              <p class="fRight subHead"></p>
              <h2 class="relative">
                <span class="steps"><span class="stepNum">Step 3</span></span>
              </h2>
              <p class="steps-title title3">Addons</p>
            </div></span><span id="step3open"></span>

          </section>

                <?php if($this->session->userdata('skiindia')>0) {?>

                    <section id="paymentBlock" class="row block relative"><span id="beforePanel3">
            <div id="summaryClose" class="stepHeader stepClose row summaryClose closeBlock">
              <p class="fRight subHead"></p>
              <h2 class="relative">
                <span class="steps"><span class="stepNum">Step 4</span></span>
              </h2>
              <p class="steps-title title4">Summary</p>
            </div></span><span id="step3open"></span>

          </section>

                    <section id="paymentBlock" class="row block relative"><span id="beforePanel3">
            <div id="summaryClose" class="stepHeader stepClose row summaryClose closeBlock">
              <p class="fRight subHead"></p>
              <h2 class="relative">
                <span class="steps"><span class="stepNum">Step 5</span></span>
              </h2>
              <p class="steps-title title5">Payment</p>
            </div></span><span id="step3open"></span>

          </section>



                <?php } else { ?>

                    <section id="paymentBlock" class="row block relative"><span id="beforePanel3">
            <div id="summaryClose" class="stepHeader stepClose row summaryClose closeBlock">
              <p class="fRight subHead"></p>
              <h2 class="relative">
                <span class="steps"><span class="stepNum">Step 4</span></span>
              </h2>
              <p class="steps-title title4">Login / Register</p>
            </div></span><span id="step3open"></span>

          </section>


                    <section id="paymentBlock" class="row block relative"><span id="beforePanel3">
            <div id="summaryClose" class="stepHeader stepClose row summaryClose closeBlock">
              <p class="fRight subHead"></p>
              <h2 class="relative">
                <span class="steps"><span class="stepNum">Step 5</span></span>
              </h2>
              <p class="steps-title title5">Summary</p>
            </div></span><span id="step3open"></span>

          </section>

                    <section id="paymentBlock" class="row block relative"><span id="beforePanel3">
            <div id="summaryClose" class="stepHeader stepClose row summaryClose closeBlock">
              <p class="fRight subHead"></p>
              <h2 class="relative">
                <span class="steps"><span class="stepNum">Step 6</span></span>
              </h2>
              <p class="steps-title title6">Payment</p>
            </div></span><span id="step3open"></span>

          </section>

                <?php } ?>


        </span>
        </div>
    </div>

</section>


<script type="text/javascript">
    function validateModifySearch(){
            var txtDepartDate=document.excursionSearchForm.txtDepartDate.value.trim();
            var destinationType=document.excursionSearchForm.destinationType.value;

              if (txtDepartDate==null || txtDepartDate=="" || txtDepartDate.length == 0){
              alert("Date can't be blank");
              return false;
              }

              if (destinationType==null || destinationType==""){

              alert(txtDepartDate+"Booking Time can't be blank");
              return false;
              }
              
            var ddAdult=document.excursionSearchForm.ddAdult.value;
            var getEXP = destinationType.split('-');
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth(); //January is 0!
            var yyyy = today.getFullYear();
            var hour12 = today.getHours();
            var min1 = today.getMinutes();
            es = getEXP[1].split(':');
            var gethours = es[0].trim()-hour12;
            var getminut ='';
            if(es[1].trim()> min1){
             getminut = es[1].trim()-min1;
              }
              if(es[1].trim()< min1){
            getminut = min1-es[1].trim();
              }
            var totalgetHM = gethours+':'+getminut;
            var hours = today.getHours();
            var currentampm = hours >= 12 ? 'pm' : 'am';
            var ampm = es[0] >= 12 ? 'pm' : 'am';
//--------------- Code---------------------//
          var today = new Date();
          var dd = today.getDate();
          var mm = today.getMonth()+1; //January is 0!

          var yyyy = today.getFullYear();

          if(dd<10){
            dd='0'+dd;
          }
          if(mm<10){
            mm='0'+mm;
          }

          var today = dd+'-'+mm+'-'+yyyy;
          today = today.trim();

          var start = destinationType;
          var date = new Date();
          var hour1 = date.getHours();
          var min1 = date.getMinutes();

          var end = hour1+":"+min1;
          e = start.split(':');
          s = end.split(':');
          min = e[1]-s[1];
          hour_carry = 0;
          if(min < 0){
            min += 60;
            hour_carry += 1;
          }
          hour = e[0]-s[0]-hour_carry;
          min = ((min/60)*100).toString();
          diff = hour ;
    
//--------------- End---------------------//
      

        if (ddAdult==null || ddAdult==""){
            alert("No of person cannot  be blank");
            return false;
        }

          var sec_num = parseInt(this, 10); // don't forget the second param


//$branch->branch_minrestriction
        if(today >= txtDepartDate){ 
         //if(ampm == currentampm){ 
           if(hour12 < es[0]){ 
            if(gethours < <?php echo $branch->branch_hrsrestriction; ?>){  
                alert("Sorry, Online booking is allowed only <?php echo $branch->branch_hrsrestriction; ?> hours <?php echo $branch->branch_minrestriction; ?> minut prior to the session time");
                return false;
          }
          if(gethours == <?php echo $branch->branch_hrsrestriction; ?>){  
             if(getminut <= <?php echo $branch->branch_minrestriction; ?>){ 
                alert("Sorry, Online booking is allowed only <?php echo $branch->branch_hrsrestriction; ?> hours <?php echo $branch->branch_minrestriction; ?> minut prior to the session time");
                return false;
              }
          }
         
      }else{
         alert("Sorry, Online booking is allowed only <?php echo $branch->branch_hrsrestriction; ?> hours <?php echo $branch->branch_minrestriction; ?> minut prior to the session time");
                return false;
      }
    }
  }

    
</script>

<script>
	  $( function() {
		  $.noConflict();
		  $( "#datepicker12" ).datepicker({ minDate: 0, maxDate: "+1M ", dateFormat: 'dd-mm-yy'});
		  
		  $('#datepicker12').change(function(){
			  
				var request = $.ajax({
				  url: "get-time-slot-by-date",
				  method: "POST",
				  data: { 'booking_date' : $('#datepicker12').val() }
				  
				});

				request.done(function( response ) {
					
					response = JSON.parse(response);
					console.log(response);
					var option_html = '';
					
						if(response.status){
							var data_array = JSON.parse(response.data);
							data_array.forEach(function(value, key){
								option_html += '<option value="'+value.dailyinventory_id + '-' + + value.dailyinventory_from + ' : ' + value.dailyinventory_minfrom  + '-' + value.dailyinventory_to + ' : ' + value.dailyinventory_minto +'">' + value.dailyinventory_from + ' : ' + value.dailyinventory_minfrom  + '<b> - </b>' + value.dailyinventory_to + ' : ' + value.dailyinventory_minto + '</option>';							
							});
						}
				  
				  	$('#sessionType1').html( option_html );
				});

				request.fail(function( jqXHR, textStatus ) {
				  console.log( "Request failed: " + jqXHR + '--' + textStatus );
				});
			  
		  });
		  
		  
	  });
	
	
	
	/*
		var app = angular.module('snowWorldApp', []);
		app.controller('packageCtrl', function($scope, $element, $http) {

			console.log( moment().format('DD/MM/YYYY') );
			
			$scope.getAvailableTimeSlots = function(){
				alert();
			}
			
		});
		
		
	*/
</script>




