<p class="fRight subHead">
    Session Date:
    <?php
	
    $trim_data = explode('-',$this->session->userdata('destinationType') );
    array_shift( $trim_data );
    echo date(' jS F Y', strtotime( $this->session->userdata('txtDepartDate') ) ); ?>
    | Session Time:
    <?php 
	// to show only on front end - not to pass in the db
	echo completeTimeSlotWithAmPm( date('Y-m-d', strtotime( $this->session->userdata('txtDepartDate') ) ) , getTimeSlotInArray( $this->session->userdata('destinationType') ) )
	?>
    | No. of Visiters: <?php echo $this->session->userdata('package_total_qty') ? $this->session->userdata('package_total_qty') : $package_total_qty ; ?>
</p>