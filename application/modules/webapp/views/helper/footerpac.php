<footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <h3>Find Us <i class="icon-location-inv"></i></h3>
                    <ul id="contact_details_footer">
                        <li><strong>FREEZING RAINS AT SNOW WORLD</strong><sup>®</sup><br>                        
                        <img class="image_add" src="img/phoenix.png" alt="Phoenix Market City">
                        <p class="footer_address">Phoenix Market City, Lower Ground Level 58 - 61, Between Atrium 2 &amp; 6, 15 L.B.S. Marg, Kamani Jn., Kurla (W), Mumbai 400 070, India.</p></li>
                        <li><i class="icon-phone"></i><a href="tel://02261801591"> 022-61801591</a> / <a href="tel://02261801592"> 022-61801592</a></li>
                        <li><i class="icon-mail"></i><a href="mailto:info@snowworldmumbai.com"> info@snowworldmumbai.com</a></li>
                    </ul>  
                </div>
                <div class="col-md-2">
                    <h3>Snow World<sup>®</sup></h3>
                    <ul>
                        <li><a href="http://www.snowworldmumbai.com/about_us.html">About Us</a></li>
                        <li><a href="http://www.snowworldmumbai.com/careers.html">Careers</a></li>
                        <li><a href="http://www.snowworldmumbai.com/safety_rules.html">Safety Rules</a></li>
                        <li><a href="http://www.snowworldmumbai.com/privacy_policy.html">Privacy Policy</a></li>
                        <li><a href="http://www.snowworldmumbai.com/terms_conditions.html">Terms &amp; Conditions</a></li>
                    </ul>
                </div>                
                <div class="col-md-2">
                    <h3>Plan your Visit <i class="icon-briefcase"></i></h3>
                    <ul>
                        <li><a href="http://www.snowworldmumbai.com/operating_hours.html">Operating Hours</a></li>
                        <li><a href="http://www.snowworldmumbai.com/tickets.html">Tickets Price</a></li>
                        <li><a href="http://www.snowworldmumbai.com/faqs.html">FAQ's</a></li>
						<li><a href="<?php echo base_url();?>bookingquery">Booking Query</a></li>             
						<li><a href="<?php echo base_url();?>partnerlogin">Partner</a></li>
                        <li><a href="http://chiliadprocons.in/" target="_blank">Visit Corporate Site</a></li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <h3>Get directions <i class="icon-location-3"></i></h3>
                    <p>Get direction from your destination.</p>
						<form id="get_direction" method="get" action="https://www.google.co.in/maps/place/" target="_blank">
                        <div class="form-group">
                            <input id="saddr" name="saddr" value="" placeholder="Enter your location" class="form-control-1">
                            <input type="hidden" name="daddr" value="snow+world+mumbai"/>
                        </div>
                            <input type="submit" value="Show on map" class="btn_1 white">
                    	</form>
                </div>
            </div><!-- End row -->
            <div class="row">
                <div class="col-md-12">
                    <div id="social_footer">
                        <ul>
                            <li><a href="https://www.facebook.com/SnowWorldMumbai/" target="_blank"><i class="icon-facebook"></i></a></li>
                            <li><a href="https://twitter.com/SnowWorldMumbai/" target="_blank"><i class="icon-twitter"></i></a></li>
                            <li><a href="https://plus.google.com/+SnowWorldMumbai-CP/" target="_blank"><i class="icon-google"></i></a></li>
                            <li><a href="https://instagram.com/snowworldmumbai/" target="_blank"><i class="icon-instagram"></i></a></li>
                            <li><a href="https://www.youtube.com/user/SnowWorldMumbai/" target="_blank"><i class="icon-youtube-play"></i></a></li>
                        </ul>
                        <p>© Snow World Mumbai 2017</p>
                    </div>
                </div>
            </div>  <!-- End row -->
        </div> <!-- End container -->
    </footer> <!-- End footer -->
    
<div id="toTop"></div><!-- Back to top button -->

<script src="<?php echo base_url();?>js/jquery-1.11.2.min.js"></script>
<script src="<?php echo base_url();?>js/common_scripts_min.js"></script>
<script src="<?php echo base_url();?>js/functions.js"></script>
<script src="<?php echo base_url();?>assets/validate.js"></script>
<!-- Specific scripts -->
<script src="<?php echo base_url();?>js/jquery.validate.min.js"></script>

</body>
</html>