<style type="text/css">

.regiter_step{ width: 100%; float: left; background-color: azure; }
.regiter_step .stpe_con_all{ width: 100%; float: left; padding: 20px 0px 20px 0px;}

.regiter_step .stpe_con_all .left_contyi{ width: 100%; float: left; }
.regiter_step .stpe_con_all .left_contyi ul{ width: 100%; float:left;    list-style: none;}
.regiter_step .stpe_con_all .left_contyi ul li{    padding: 3px;
    float: left;
    width: 100%;}
.regiter_step .stpe_con_all .left_contyi ul li a{       text-decoration: none;
    border-left: 3px solid #d40735;
    background-color: #2657a6;
    padding: 10px 15px 10px 15px;
    color: #fff;
    font-weight: 700;
    width: 100%;
    float: left;}

	.acty{color: #fff !important;
    background-color: #d40735 !important;border-left: 3px solid #6fc27f !important;}

.regiter_step .stpe_con_all .left_contyi ul li a:hover{ color:#fff; background-color:#d40735; border-left: 3px solid #d40735;}

.regiter_step .stpe_con_all .right_contyi{ width:100%; float:left; background-color:#fff;  }
.regiter_step .stpe_con_all .right_contyi .heading_tab{ width:100%; float:left;     padding: 0px 20px 10px 20px; border-bottom:1px dotted #CCC;}
.regiter_step .stpe_con_all .right_contyi .heading_tab h1{ width:100%; float:left;     font-size: large; }
.regiter_step .stpe_con_all .right_contyi .heading_tab h1 span{color: #2657a6; font-size: medium; }

.regiter_step .stpe_con_all .right_contyi .verified_num{ width:100%; float:left; border-left:2px solid; padding: 10px 20px 10px 20px;}
.regiter_step .stpe_con_all .right_contyi .verified_num h1{font-size: large;}
.send_pin{ width:100%; float:left;}
.send_pin a {   background-color: #2657a6;
    padding: 7px 15px 7px 15px;

    line-height: 35px;
    border-radius: 2px;
    color: #fff;}

	.regiter_step .stpe_con_all .right_contyi .verified_num p{ width:100%; float:left;    line-height: 27px;
    padding-top: 15px;}

</style>

<section class="regiter_step">
	<div class="container">
		
		<div class="row">
		<div class="stpe_con_all">
		<div class="col-md-4">
			<div class="left_contyi">
				<ul>
					<li><a href="agentform1" class="acty"> Identity Verification</a></li>
					<li><a href="">Seller Details </a></li>
					<li><a href="">Official Details</a></li>
					<li><a href="">Statutory Requirements</a></li>
					<li><a href="">Bank Details</a></li>
					<li><a href="">Documents Required</a></li>
					
				</ul>

			</div>
		</div>

		<div class="col-md-8">

		<div class="right_contyi">
			<div class="heading_tab">
            <h1>Registration Process <span>( Just 4 more steps until your account is setup )</span></h1>
            </div>
            
            
            <div class="verified_num">
            <h1>Verify your mobile number</h1>
            
            <form action="" method="post">
            <div class="form-group">
                    	<div class="row">
							<div class="col-md-6">
                                <input type="tel" name="umob" class="form-control-1" placeholder="Enter Your Phone Number" pattern="^\d{10}$" required title="Your Phone Number Should Only Contain  Letters. e.g. 9999999999" maxlength="10">
                       </div>
                            <div class="col-md-6">

                            <div class="send_pin">
                                <input type="submit" name="submit" class="btn_1" value="Send Pin">

                       
                        </div>
                       
                            </div>
                    	</div>
                    </div>
                </form>
                    
                    <p>We will send out a verification code by text message.</p>

            </div>
		</div>
			
		</div>

		</div>
			
		</div>
	</div>
</section>