<style type="text/css">

    .regiter_step {
        width: 100%;
        float: left;
        background-color: azure;
    }

    .regiter_step .stpe_con_all {
        width: 100%;
        float: left;
        padding: 20px 0px 20px 0px;
    }

    .regiter_step .stpe_con_all .left_contyi {
        width: 100%;
        float: left;
    }

    .regiter_step .stpe_con_all .left_contyi ul {
        width: 100%;
        float: left;
        list-style: none;
    }

    .regiter_step .stpe_con_all .left_contyi ul li {
        padding: 3px;
        float: left;
        width: 100%;
    }

    .regiter_step .stpe_con_all .left_contyi ul li a {
        text-decoration: none;
        border-left: 3px solid #d40735;
        background-color: #2657a6;
        padding: 10px 15px 10px 15px;
        color: #fff;
        font-weight: 700;
        width: 100%;
        float: left;
    }

    .acty {
        color: #fff !important;
        background-color: #d40735 !important;
        border-left: 3px solid #6fc27f !important;
    }

    .regiter_step .stpe_con_all .left_contyi ul li a:hover {
        color: #fff;
        background-color: #d40735;
        border-left: 3px solid #d40735;
    }

    .regiter_step .stpe_con_all .right_contyi {
        width: 100%;
        float: left;
        background-color: #fff;
    }

    .regiter_step .stpe_con_all .right_contyi .heading_tab {
        width: 100%;
        float: left;
        padding: 0px 20px 10px 20px;
        border-bottom: 1px dotted #CCC;
    }

    .regiter_step .stpe_con_all .right_contyi .heading_tab h1 {
        width: 100%;
        float: left;
        font-size: large;
    }

    .regiter_step .stpe_con_all .right_contyi .heading_tab h1 span {
        color: #2657a6;
        font-size: medium;
    }

    .regiter_step .stpe_con_all .right_contyi .form_details {
        width: 100%;
        float: left;
        padding: 25px 20px 10px 20px;
    }

    .regiter_step .stpe_con_all .right_contyi .form_details .form-group {
        width: 100%;
        float: left;
    }

    .regiter_step .stpe_con_all .right_contyi .form_details .form-group select {
        min-height: 35px;
    }

    .regiter_step .stpe_con_all .right_contyi .form_details .next_button {
        width: 100%;
        float: left;
        padding: 20px 0px 20px 0px;
        text-align: center;
    }

    .regiter_step .stpe_con_all .right_contyi .form_details .next_button a {

        background-color: #2657a6;
        padding: 7px 15px 7px 15px;
        line-height: 35px;
        border-radius: 2px;
        color: #fff;
    }

</style>

<section class="regiter_step">
    <div class="container">

        <div class="row">
            <div class="stpe_con_all">
                <div class="col-md-4">
                    <div class="left_contyi">
                        <ul>
                            <li><a href=""> Identity Verification</a></li>
                            <li><a href="agentform2" class="acty">Seller Details </a></li>
                            <li><a href="">Official Details</a></li>
                            <li><a href="">Statutory Requirements</a></li>
                            <li><a href="">Bank Details</a></li>
                            <li><a href="">Documents Required</a></li>

                        </ul>

                    </div>
                </div>

                <div class="col-md-8">

                    <div class="right_contyi">
                        <div class="heading_tab">
                            <h1>Seller Details </h1>
                        </div>
<form action="" method="post" enctype="">

                        <div class="form_details">
                            <div class="form-group">

                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Login Type</label>
                                        <select name="logintype" class="form-control-1" required="">
                                            <option value="1">Corporate Login</option>
                                            <option value="2">Officer Login</option>

                                        </select></div>
                                    <div class="col-md-6">
                                        <label>Country:</label>
                                        <input type="text" name="emailcountry" class="form-control-1"
                                               placeholder="Enter Your Country" pattern="[A-Za-z\s]+" required=""
                                               title="Enter Your Country" maxlength="50">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">

                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Agency Name</label>

                                        <input type="text" name="agencyname" class="form-control-1"
                                               placeholder="Enter Agency Name" required=""
                                               title="Agency Name should only contain  letters. e.g. John"
                                               maxlength="60">

                                    </div>
                                    <div class="col-md-6">
                                        <label>Registered / Head Office Address </label>
                                        <input type="text" name="office_address" class="form-control-1"
                                               placeholder="Enter Office Address" required=""
                                               title="Enter Registered / Head Office Address " maxlength="60">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>City</label>
                                        <input type="text" name="city" class="form-control-1" placeholder="Enter City"
                                               pattern="[A-Za-z\s]+" required=""
                                               title="City Name should only contain  letters. e.g. Delhi"
                                               maxlength="60">

                                    </div>
                                    <div class="col-md-6">
                                        <label>State</label>
                                        <input type="text" name="state" class="form-control-1" placeholder="Enter State"
                                               pattern="[A-Za-z\s]+" required=""
                                               title="State Name should only contain  letters. e.g. Delhi"
                                               maxlength="60">

                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Pincode</label>
                                        <input type="text" name="pincode" class="form-control-1"
                                               placeholder="Enter Your Pincode" pattern="^\d{6}$" required=""
                                               title="Your Pincode Should Only Contain  Number. e.g. 110011"
                                               maxlength="10">
                                    </div>
                                    <div class="col-md-6">
                                        <label>Telephone</label>
                                        <input type="tel" name="telephone" class="form-control-1"
                                               placeholder="Enter Your Telephone Number" pattern="^\d{10}$" required=""
                                               title="Your Phone Number Should Only Contain  Number. e.g. 9999999999"
                                               maxlength="20">
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Website</label>
                                        <input type="text" name="website" class="form-control-1"
                                               placeholder="Enter Your Website" maxlength="60">
                                    </div>
                                    <div class="col-md-6">
                                        <label>Fax</label>
                                        <input type="text" name="fax" class="form-control-1"
                                               placeholder="Enter Your Fax Number"
                                               title="Your Fax Number Should Only Contain  Number. e.g. 9999999999"
                                               maxlength="20" pattern="^\d{10}$">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Status</label>
                                        <select name="emailstatus" class="form-control-1">
                                            <option value="International">International</option>
                                            <option value="Domestic">Domestic</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Currency in which payments will be made:</label>
                                        <input type="text" name="emailcurrency" class="form-control-1"
                                               placeholder="Enter Your Currency" required="" title="Enter Your Currency"
                                               maxlength="50">
                                    </div>
                                </div>
                            </div>


                            <div class="next_button">
                                <input type="submit" value="Submit" name="Submit" class="btn_1">
                            </div>

                        </div>
</form>
                    </div>

                </div>

            </div>

        </div>
    </div>
</section>