<?php
   echo '';
   ?>
<script src = "https://code.jquery.com/jquery-1.10.2.js"></script>
<script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"
   ></script> <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/boo
   tstrap.min.js"></script> 
<div class="container">
   <div class="row">
      <div class="col-lg-3 col-md-3 col-xs-12"></div>
      <div class="col-lg-6
         col-md-6 col-xs-12">
         <div class="order_success">
            <div class="order_success_logo"></div>
            <div
               class="order_success_text text-center">
               <span class="success_icon"> 
               	<i class="fa fa-check-circle"></i>
               </span>
               <h2>congratulations</h2>
               <p>Your order has been successfully placed
               </p>
            </div>
            <div
               class="order_success_detail">
               <div class="row">
                  <div class="col-lg-6 col-md-6 col-xs-12">
                     <div
                        class="order-id ods-ct"> Your order Id is
                        <span class="spf">#00221100</span>                             
                     </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-xs-12">
                     <div class="payed_amount ods-ct text-right">
                        Total Paid Amount is <span class="spf"><i class="fa fa-rupee"></i>3000</span>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-lg-12 col-xs-12">
                     <div class="billing_address">
                        <h4>Shipping
                           Address -
                        </h4>
                        <ul>
                           <li><span class="bill_text-left">Name</span><span class="bill_text-right">
                              Simerjeet singh</span>
                           </li>
                           <li>
                           	<span class="bill_text-left">Billing Mobile</span>
                           	<span class="bill_text-right">9810668829</span>
                           </li>
                           <li>
                           	<span class="bill_text-left">Email Address</span>
                           	<span class="bill_text-right">simer@elitehrpractices.com</span>
                           </li>
                           <li>
                           	<span class="bill_text-left">Address</span>
                           	<span class="bill_text-right">Janak Puri</span>
                           	<span class="city bill-add"> New Delhi,</span>
                           	<span class="bill-add"> Delhi 110058</span>
                           	<span class="bill-add"> Delhi</span></li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-lg-3 col-md-3 col-xs-12"></div>
   </div>
</div>
<style type="text/css">
   .order_success{
   width: 100%;
   float: left;
   margin: 70px 0 0 0;
   border: 1px solid #e8e8e8;
   background: #fdfdfd;
   padding: 15px;
   }
   .success_icon{
   width: 100%;
   float: left;
   }
   .success_icon .fa{
   font-size: 100px;
   color: #71ca90;
   line-height: 110px;
   }
   h2{
   width: 100%;
   float: left;
   margin: 15px 0;
   text-transform: capitalize;
   color: #0098DA;
   font-weight: 700;
   }
   .order_success_detail{
   width: 100%;
   float: left;
   margin: 15px 0;
   border-top: 1px solid #ddd;
   padding: 15px 0 0 0;
   }
   .spf{
   font-size: 18px;
   color: #0098DA;
   line-height: 22px;
   padding: 0 0 0 10px;
   box-sizing: border-box;
   font-weight: 600;
   }
   .ods-ct{
   font-size: 16px;
   line-height: 22px;
   color: #393939;
   }
   .billing_address{
   width: 100%;
   margin: 15px 0 0 0;
   float: left;
   }
   ul{
   padding: 0px;
   }
   ul li{
   list-style-type: none;
   margin: 0 0 10px 0;
   }
   .billing_address h4{
  width: 100%;
    float: left;
    color: #71ca90;
    padding: 0 0 10px 0;
    border-bottom: 1px solid #a3d8b5;
    font-size: 20px;
   }
   .bill_text-left{width: 25%; position: relative;padding: 0 15px 0 0;box-sizing: border-box;float: left; font-weight: 600;}
     .bill_text-left:after{    position: absolute;
    right: 10px;
    top: 0px;
    content: ":";
    font-size: 16px;
    font-weight: bold;
    color: #3939396;
     }
</style>