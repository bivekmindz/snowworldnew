<ul id="main-menu" class="main-menu">
    <li><a href="partnerdashboard"> <i class="entypo-gauge"></i><span class="title"> Dashboard</span> </a></li>
     <li><a href="partnerbookingsearch"><i class="entypo-pencil"></i><span class="title">Booking Pannel</span></a></li>
                    
                  
    <!--  <li class="has-sub">
        <a href="#"><i class="entypo-mail"></i><span class="title">Dashboard Management</span></a>
        <ul>
            <li> <a href="partnerdashboard"><i class="entypo-pencil"></i><span class="title">Dashboard</span></a> </li>
            <li> <a href="agentgrapg"><i class="entypo-pencil"></i><span class="title">Agnet Comparison Graph</span></a> </li>
            
        </ul>
    </li>
    <li class="active opened active has-sub">
        <a href="partnerbookingsearch"><i class="entypo-gauge"></i><span class="title">Booking Pannel</span></a>
    </li> -->
    <li class="has-sub">
        <a href="#"><i class="entypo-book"></i><span class="title">Transactions</span></a>
        <ul>
            <li> <a href="updateamount"><i class="entypo-inbox"></i><span class="title">Add Payment</span></a> </li>
            <li> <a href="mywallet"><i class="entypo-pencil"></i><span class="title">WALLET HISTORY</span></a> </li>
            <li> <a href="order"><i class="entypo-pencil"></i><span class="title">Order</span></a> </li>
        </ul>
    </li>
  <!--   <li class="has-sub">
        <a href="#"><i class="entypo-layout"></i><span class="title">Booking Summary</span></a>
        <ul>
            <li> <a href="#"><span class="title">Dashboard</span></a> </li>
            <li> <a href="#"><span class="title">Statistics</span></a> </li>
        </ul>
    </li> -->
    <li class="has-sub">
        <a href="#"><i class="entypo-thumbs-up"></i><span class="title">Agency Management</span></a>
        <ul>
            <li> <a href="partners"><i class="entypo-user"></i><span class="title">Manage User</span></a> </li>
            <li> <a href="updatepartnerrprofile"><i class="entypo-vcard"></i><span class="title">Update Profile</span></a> </li>
             <li> <a href="agentmail"><i class="entypo-mail"></i><span class="title">Mail</span></a> </li>


            <li> <a href="changepartnerpassword"><i class="entypo-lock"></i><span class="title">Change Password</span></a> </li>
        </ul>
    </li>
    <li class="has-sub">
        <a href="partnerlogout"><i class="entypo-logout"></i><span class="title">Logout</span></a>
    </li>
    
</ul>
</div>
</div>
<div class="main-content">
<div class="row">
<!-- Profile Info and Notifications -->
<div class="col-md-4 col-sm-8 clearfix">
    <ul class="user-info pull-left pull-none-xsm">
        <!-- Profile Info -->
        <li class="profile-info dropdown">
            <!-- add class "pull-right" if you want to place this from right --> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> My Account  </a>
            <ul class="dropdown-menu">
                <!-- Reverse Caret -->
                <li class="caret"></li>
                <!-- Profile sub-links -->
                <li> <a href="partnerbookingsearch"> <i class="entypo-user"></i> Booking Pannel </a> </li>
                <li> <a href="updateamount"> <i class="entypo-mail"></i> Add Payment </a> </li>
                <li> <a href="mywallet"> <i class="entypo-calendar"></i>My Wallet </a> </li>
                <li> <a href="order"> <i class="entypo-clipboard"></i> Order </a> </li>
                <li> <a href="partners"> <i class="entypo-mail"></i> Manage User </a> </li>
                <li> <a href="updatepartnerrprofile"> <i class="entypo-calendar"></i>Accounting </a> </li>
                <li> <a href="changepartnerpassword"> <i class="entypo-clipboard"></i> Change Password </a> </li>
                <li> <a href="partnerlogout"> <i class="entypo-clipboard"></i>Logout</a> </li>
            </ul>
        </li>
    </ul>



</div>
<div class="col-md-4 col-sm-4">&nbsp;</div>
<div class="col-md-4 col-sm-4 clearfix hidden-xs">
        <ul class="list-inline links-list pull-right">         
          <li> <a href="">Log Out <i class="entypo-logout right"></i> </a> </li>
        </ul>
      </div>
      </div>
<hr />