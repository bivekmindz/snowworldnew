<form id="loginform" name="loginform" class="form_pa" method="post" action="" enctype="">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-6">
            </div>
            <div class="col-lg-6">
                <div class="top_headadin1">
                    <a href=""><i class="entypo-left-open"></i>  Back</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class='flashmsg'>
                <?php echo validation_errors(); ?>
                <?php
                echo $message;
                if($this->session->flashdata('message')){
                    echo $this->session->flashdata('message');
                }

                ?>
            </div>
            <div class="add-form">
                <h1> Basic Information</h1>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Title</label>
                       <?php echo $agentviewwdata->user_title; ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>First Name</label>
                        <?php echo $agentviewwdata->user_firstname; ?>    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label>Email Address</label>
                        <?php echo $agentviewwdata->user_emailid; ?>  </div>
                </div>
              <div class="col-md-6">
                    <div class="form-group">
                        <label>Address</label>
                        <?php echo $agentviewwdata->user_Address; ?>    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Town</label>
                        <?php echo $agentviewwdata->user_town; ?>    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Counry</label>
                        <?php echo $agentviewwdata->user_country; ?>    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>State</label>
                        <?php echo $agentviewwdata->user_state; ?>   </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>City</label>
                        <?php echo $agentviewwdata->user_city; ?>  </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Pincode</label>
                        <?php echo $agentviewwdata->user_pincodes; ?>  </div>
                </div>
    <!--   <div class="serach_bar">
          <input type="submit" name="submit" id="submit" onclick="return functi()" value="Edit Partner" class="btn btn-lg btn-primary">
                    </div> -->
                <div class="line-do"></div>
            </div>
        </div>

</form>
