   
  <script src="<?php echo base_url()?>assets/js/jquery-1.9.1.js"></script>
  <script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
  
  <!-- jQuery Form Validation code -->
  <style>
    input.error{border:1px solid red!important;}
    select.error{border:1px solid red!important;}
    textarea.error{border:1px solid red!important;}
    .error{border:1px solid red;}
    label.error{border:0px solid red!important; color:red; font-weight: normal; display:inline; }
    .sessionerror{border:0px solid red!important; color:red; font-weight: normal; }
  </style>

  <script language='javascript'>
  
  // When the browser is ready...
  $(document).ready(function() {
  
  // Setup form validation on the #register-form element
    $("#addCont").validate({
        // Specify the validation rules

          rules: {
            emailmob     : "required",
            acctype      : "required",
            pwd          : {
                              required  : true,
                              minlength : 6
                           },
            
        },
        // Specify the validation error messages
        messages: {
            emailmob      : "Email  is required",
            acctype       : "Choose Account type",
            pwd           : {
                              required: "Password is required",
                              minlength: "Your password must be at least 6 characters long"
                            },
            
        },
        submitHandler: function(form) {
            form.submit();
        }
    });

  });
  
</script>

<!--Start Compare part-->

<section class="step_paymentlogin" >
<div class="container margin_30">
		<div id="partner_login_form" class="login box_style_1">
			<h3>
				<i class="icon-users"></i> Partner Login Form
			</h3>
			<form id="login" method="post" name="loginPage" >
				<div class="row">
					<div class="col-md-12">
           <div class="sessionerror"><?php echo $this->session->flashdata('message'); ?></div>
						<div class="radio">

							<label class="radio-inline"><input type="radio" name="acctype" id="acctype" checked="checked" value="Corporate Login">Corporate Login </label> <label class="radio-inline"><input type="radio" id="acctype" name="acctype" value="Officer Login">Officer Login </label>
						</div>
						<div class="form-group">
							<label>User Name</label>
                            <input type="email" name="emailmob" class="form-control-1" placeholder="Enter Your Email" required title="The input is not a valid email address" maxlength="50">
                          </div>
						<div class="form-group">
							<label>Password</label>   <input type="password" name="pwd" class="form-control-1" placeholder="Enter Your Password" required title="The input is not a valid Password" maxlength="35" min="6">

                        </div>
					
				</div>
				<div class="row">
					<div class="col-md-4">
					 
					  <input type="submit" id="submit" name="submit" value="LOGIN" class="btn_1">
					</div>

				</div>
			</form>
			<div class="row">
				<div class="col-md-12">
					<p style="margin: 10px 0 0 0 !important;">
						<a href="agentregistration" >Click here</a> to For B2B registration form.
                        <a href="agentforget" >Forgot Password</a>

				</div>
			</div>
		</div>

		
	</div>
</section>

  
  
  