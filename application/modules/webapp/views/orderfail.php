<style>
.doller_left p {
    color: #000!important;
}
</style>
<div class="failure container-fluid">
    
    
    <div class="container">
    <div class="row">
    
    <div class="bg_fail">
    
    <div class="col-md-12">
    
    <div class="all_content">
    <div class="heading_top">
    
    <h1><span>SORRY FOR THE INCONVENIENCE. YOUR BOOKING IS FAILED</span></h1>
    </div>
    
    <div class="p_class">
    
    <p>
<span>Please note : </span>Below is your booking ID for all communication with Snow world. Please save and store the ID mentioned.</p>
    </div>
	<?php 
//p( $orderdisplaydataval );

	?>    
    <div class="heading_tab_inner"><h5>Booking Details - <?php

            $prepare_time_slot_from_date = $orderdisplaydataval->departuretime .':'.$orderdisplaydataval->frommin .' '.$orderdisplaydataval->txtfromd .' - '.
                $orderdisplaydataval->tohrs.':'.$orderdisplaydataval->tomin.' '.$orderdisplaydataval->txttod;


            $prepare_complete_time_slot = [ $orderdisplaydataval->departuretime,
                $orderdisplaydataval->frommin,
                $orderdisplaydataval->tohrs,
                $orderdisplaydataval->tomin,
            ];
            $prepare_complete_time_slot = array_map( 'trim', $prepare_complete_time_slot );

            echo date(' jS M Y', strtotime( $orderdisplaydataval->addedon ) );

            ?> </h5></div>
    
    <div class="content_mid">
    
    <div class="payment_details_main"> 
                      
                        <div class=" col-lg-9 col-md-9 col-sm-6 col-xs-6 review_content ">
                          <div class="top_head_bar">
                            <h4>Ticket Type: <?php echo str_replace('#', '/', $orderdisplaydataval->packproductname );?></h4>							  
                          </div>
                          <div class="scifiheader"><h4>Booking ID:  <?php  echo($orderdisplaydataval->ticketid);  ?></h4></div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                          <div class="doller_left">

                            <p></p>
                          </div>
                        </div>
                      </div>
                      
                      
                      
                      
                      <div class="full_width package_table_section">
                        <div class="col-lg-6 col-md-6 border_right">
                          <div class="payment_table_package">
                            <table class="table">
                              <tbody>
                              <tr>
                                <td>Session Date</td>
                                <td>:</td>
                                <td>
                                    <?php echo date(' jS M Y', strtotime($orderdisplaydataval->departuredate) ); ?> (<?php echo date('h:i:s A');?>)

								  
								  
								  </td>
                              </tr>
								  
								  
                              <tr>
                                <td>Start Time</td>
                                <td>:</td>
                                <td> <?php echo $prepare_time_slot_from_date;  ?> </td>
                              </tr>
                              <tr>
                                <td><?php if($orderdisplaydataval->packpkg>1) { ?>Visitors<?php } else { ?>Visitor<?php } ?></td>
                                <td>:</td>
                                <td><?php  echo($orderdisplaydataval->packpkg );  ?></td>
                              </tr>
                              <tr>
                                <td>Addons</td>
                                <td>:</td>
                                <td><?php 
foreach($orderaddonedisplaydata as $key =>$value){ 
 if($value!='Something Went Wrong' ){
                            echo($value['addonename']."&nbsp;x&nbsp;".$value['addonevisiter']."<br>");
                         } else { echo "N/A";} } 
                          ?></td>
                              </tr>                              
                            </tbody></table>
                          </div> 
                        </div>
                      
                        <div class="col-lg-6 col-md-6">                         
                          <div class="payment_table_package">
                            <table>
                              <tbody>
                              <tr>
                                <td>Ticket price</td>
                                <td> <span><i class="icon-rupee" aria-hidden="true"></i></span><?php echo ($orderdisplaydataval->packprice); ?>
                                </td>
                              </tr>
                              <tr>
                                <td>Addons</td>
                               
                                <td><span><i class="icon-rupee" aria-hidden="true"></i></span><?php 
$addonetotal=0;foreach($orderaddonedisplaydata as $key =>$value){
 if($value!='Something Went Wrong' ){

                    $addonetotal +=  ($value['addoneprice']);
                      }
                      else{ $addonetotal= 0;}
                          }  echo $addonetotal;
                          ?></td>
                              </tr>    
                             
                              <tr>
                                <td>Discount (Promocode)</td>
                                <td><span><i class="icon-rupee" aria-hidden="true"></i></span> - <?php echo ($orderdisplaydataval->promocodeprice); ?>
 </td>
                              </tr>

                              <tr>
                                <td>Internet Handling Charge</td>
                                <td><span><i class="icon-rupee" aria-hidden="true"></i></span> <?php echo ($orderdisplaydataval->internethandlingcharges); ?>
 </td>
                              </tr>
                              <tr class="total_row">
                                <td>Payable Price</td>
                                <td>

 <span><i class="icon-rupee" aria-hidden="true"></i></span> <?php echo ($orderdisplaydataval->total); ?>

</td>
                              </tr>
                            </tbody></table>
                          </div>
                        </div>
                      </div>
    
    </div>
    
  <div class="full_width information_section">
                    <div class="information_title scifiheader"> Customer Information </div>
                    <div class="full_width information_table_main">
                      <div class="col-lg-6 col-md-6 border_right">
                        <div class="payment_table_package conyy">
                          <table class="table">
                            <tbody>
                            <tr>
                              <td>Name :</td>
                              <td><?php  echo($orderdisplaydataval->billing_name);  ?></td>
                            </tr>
                            <tr>
                              <td>Contact :</td>
                              <td><?php  echo($orderdisplaydataval->billing_tel);  ?></td>
                            </tr>
                            <tr>
                              <td>Email :</td>
                              <td><?php  echo($orderdisplaydataval->billing_email );  ?></td>
                            </tr>
                          </tbody></table>
                        </div>
                      </div>
                      <div class="col-lg-6 col-md-6 border_right">
                        <div class="payment_table_package conyy">
                          <table class="table">
                            <tbody><tr>
                              <td>Address :</td>
                              <td><?php   if(($orderdisplaydataval->billing_address)!='') {  echo $orderdisplaydataval->billing_address ; } else { echo "N/A";} ?></td>
                            </tr>
                            <tr>
                              <td>Town/City :</td>
                              <td><?php if(($orderdisplaydataval->billing_city)!='') {  echo($orderdisplaydataval->billing_city ); } else { echo "N/A";} ?></td>
                            </tr>
                            <tr>
                              <td>Zip Code : </td>
                              <td><?php if(($orderdisplaydataval->billing_zip)!='') {  echo($orderdisplaydataval->billing_zip ); } else { echo "N/A";} ?></td>
                            </tr>
                            <tr>
                              <td>Country :</td>
                              <td><?php if($orderdisplaydataval->billing_country!='') {  echo($orderdisplaydataval->billing_country ); } else { echo "N/A";} ?></td>
                            </tr>
                          </tbody></table>
                        </div>
                      </div>
                    </div>
                  </div>
          
          <div class="full_width information_table_main">
                      <div class="booking_text t_align_c">
                        <span>Venue Address</span>
                        <p class="scifiheader"><?php echo ($branch->branch_add); ?>.</p>
                      </div>
                    </div> 
            
    
    </div>
    
    
    
    
    </div>
    
    </div>
    </div>
    
    </div>
    
    
    </div> 
  