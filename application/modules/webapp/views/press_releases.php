
    <div class="container margin_30">
        <div class="main_title">
            <h2>Press Releases</h2>
        </div>
        <hr>
        <div class="row magnific-gallery">
            <div class="col-md-3 col-sm-3">
                <a HREF="img/press/slide1.jpg"><img SRC="img/press/1.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/press/slide2.jpg"><img SRC="img/press/2.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/press/slide3.jpg"><img SRC="img/press/3.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/press/slide4.jpg"><img SRC="img/press/4.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/press/slide5.jpg"><img SRC="img/press/5.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/press/slide6.jpg"><img SRC="img/press/6.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/press/slide7.jpg"><img SRC="img/press/7.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/press/slide8.jpg"><img SRC="img/press/8.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/press/slide9.jpg"><img SRC="img/press/9.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/press/slide10.jpg"><img SRC="img/press/10.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/press/slide11.jpg"><img SRC="img/press/11.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/press/slide12.jpg"><img SRC="img/press/12.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/press/slide13.jpg"><img SRC="img/press/13.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/press/slide14.jpg"><img SRC="img/press/14.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/press/slide15.jpg"><img SRC="img/press/15.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/press/slide16.jpg"><img SRC="img/press/16.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/press/slide17.jpg"><img SRC="img/press/17.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/press/slide18.jpg"><img SRC="img/press/18.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/press/slide19.jpg"><img SRC="img/press/19.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/press/slide20.jpg"><img SRC="img/press/20.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/press/slide21.jpg"><img SRC="img/press/21.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/press/slide22.jpg"><img SRC="img/press/22.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/press/slide23.jpg"><img SRC="img/press/23.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/press/slide24.jpg"><img SRC="img/press/24.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/press/slide25.jpg"><img SRC="img/press/25.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/press/slide26.jpg"><img SRC="img/press/26.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/press/slide27.jpg"><img SRC="img/press/27.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/press/slide28.jpg"><img SRC="img/press/28.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/press/slide29.jpg"><img SRC="img/press/29.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/press/slide30.jpg"><img SRC="img/press/30.jpg" alt="" class="img-responsive styled"></a>
            </div>
            
            <div class="col-md-3 col-sm-3">
                <a HREF="img/press/slide31.jpg"><img SRC="img/press/31.jpg" alt="" class="img-responsive styled"></a>
            </div>
        </div><!-- End row -->
        
        <hr>    
    </div><!-- End container -->
    
   