<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Inventory extends MX_Controller
{
    public function __construct()
    {
        $this->load->model("supper_admin");
        $this->load->helper('my_helper');
        $this->load->helper('adminmenu_helper');
       $this->load->library('session');
    }



    /*ADD,VIEW,UPDATE COMPANY BY zzz*/
    public function datewiseinventory()
    {
if(getMemberId()==1)

{

}
else
{
  redirect('admin/login/dashboard?empid='.$_GET['empid'].'&uid='.$_GET['uid'].'');
}
        $parameter2 = array('act_mode' => 's_viewbranch',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        $response['s_viewbranch'] = $this->supper_admin->call_procedure('proc_packages_s', $parameter2);


        if ($this->input->post('submit')) {
            $parameter2 = array('act_mode' => 's_viewtimeslot',
                'Param1' => $this->input->post('branchids'),
                'Param2' => $this->input->post('departDate'),
                'Param3' => $this->input->post('field_name1'),
                'Param4' => $this->input->post('minute_name1'),
                'Param5' => $this->input->post('field_name2'),
                'Param6' => $this->input->post('minute_name2'),
                'Param7' => '',
                'Param8' => '',
                'Param9' => '');
            $response['s_viewtimeslot'] = $this->supper_admin->call_procedure('proc_viewtimeslot_v', $parameter2);

            $arr = (array)$response['s_viewtimeslot'];
        }


        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('inventory/datewisevisitors', $response);

    }


    public function calenderview()
    {

if(getMemberId()==1)

{

}
else
{
  redirect('admin/login/dashboard?empid='.$_GET['empid'].'&uid='.$_GET['uid'].'');
}
        $parameter2 = array('act_mode' => 's_viewbranch',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        $response['s_viewbranch'] = $this->supper_admin->call_procedure('proc_packages_s', $parameter2);


        if ($this->input->post('submit')) {
            $a = $this->input->post('update_branchids');
            $this->session->set_userdata('adminbranchid', $a);
            $this->session->set_userdata('adminbranchid', $a);
            $response['arr'] = $a;

            $parameter2 = array('act_mode' => 's_viewbranch',
                'Param1' => '',
                'Param2' => '',
                'Param3' => '',
                'Param4' => '',
                'Param5' => '',
                'Param6' => '',
                'Param7' => '',
                'Param8' => '',
                'Param9' => '');
            $response['s_viewbranch'] = $this->supper_admin->call_procedure('proc_packages_s', $parameter2);

        }

        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('inventory/mainview', $response);
    }

    public function threshold()
    {
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('inventory/threshold', $response);
    }

    public function event()
    {
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('inventory/event', $response);
    }

    public function category()
    {
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('inventory/category', $response);
    }

    public function checkdate()
    {
        $date = $this->input->post('date');
        $xx = explode('-', $date);
        //pend($xx);
        //die;
        //$month =   sprintf("%02d", $this->input->post('month'));

        //$year =   $this->input->post('year');
        $dd = $xx['2'] . "/" . $xx['1'] . "/" . $xx['0'];
       //echo $this->session->userdata('adminbranchid');
       // $dd = '01/06/2017';


        $parameter2 = array('act_mode' => 'promo_view',
            'Param1' => $this->session->userdata('adminbranchid'),
            'Param2' => $dd,
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
      // p($parameter2);
        $response['timeslot_total'] = (array)$this->supper_admin->call_procedure('proc_inventory_s', $parameter2);
     //p($response['timeslot_total']);

    /*   $parameter21 = array('act_mode' => 'promo_view1',
            'Param1' => $this->session->userdata('adminbranchid'),
            'Param2' => $dd,
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
       //p($parameter21);
        $response['timeslot_total1'] = (array)$this->supper_admin->call_procedure('proc_inventory_s', $parameter21);
        pend($response['timeslot_total1']);*/


        $parameter3 = array('act_mode' => 'order_addon_view',
            'Param1' => $this->session->userdata('adminbranchid'),
            'Param2' => $dd,
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        $response['order_table'] = (array)$this->supper_admin->call_procedure('proc_inventory_s', $parameter3);
        //p($response['order_table']);
        $timeslot_master_Array = array();
        foreach ($response['timeslot_total'] as $a) {
            $timeslot_master_Array[] = ['total_seat' => $a->total_Seat, 'time_start_from' => $a->tfrom, 'booked_seat' => 0];
            //p($a);
        }
        //p($timeslot_master_Array);

        $package_qty = explode(',', $response['order_table']['0']->package_qty);
        $package_time = explode(',', $response['order_table']['0']->package_time);
        //p($package_time);
        //p($package_qty);
        foreach ($package_time as $key => $package_time1) {
            //echo $package_time1[$package_time1];
            foreach ($timeslot_master_Array as $k => &$v) {

                //p($package_time1);
                if ($package_time1 == $v['time_start_from']) {
                    //echo $v['time_start_from'] . "<br>";
                    $v['booked_seat'] = $v['booked_seat'] + $package_qty[$key];
                }


                //$v['booked_seat'] = $v['booked_seat'] + 1;
                //break;

            }
            //break;

        }
        //p($timeslot_master_Array);
        $my_var = "";

        foreach ($timeslot_master_Array as $v11) {
            //echo $v11['total_seat'];
            //die;
            if ($v11['total_seat'] <= $v11['booked_seat']) {
                $my_var = 'threshold';
                echo $my_var;
                die();
               // echo $v11['total_seat'];
            }
}


foreach ($timeslot_master_Array as $v11) {
            if ($v11['total_seat'] > $v11['booked_seat'] && $v11['booked_seat'] != 0) {
                $my_var = 'normal';
                 echo $my_var;
                //echo $v11['total_seat'];
die();
            }
           }
 foreach ($timeslot_master_Array as $v11) {
            if ($v11['booked_seat'] == '0') {
                $my_var = 'available';
            }
}
        
             
        



        echo $my_var;


    }


    public function sess_date()
    {
        $a = (string)$this->input->post('date');
        $this->session->set_userdata('admindateid', $a);
       // print_r($this->session->userdata);
        echo "set";
    }


    public function package()
    {
        $response['branchids'] = $this->session->userdata('adminbranchid');
        $respdate = $this->session->userdata('admindateid');
        $responcevalue = explode("-", $respdate);
        $response['departDate'] = "" . $responcevalue[2] . "/" . $responcevalue[1] . "/" . $responcevalue[0] . "";
        $parameter2 = array('act_mode' => 's_viewtimeslottimepackage',
            'Param1' => $response['branchids'],
            'Param2' => $response['departDate'],
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        $response['s_viewtimeslot'] = $this->supper_admin->call_procedure('proc_viewtimeslot_v', $parameter2
        );
        $arr = (array)$response['s_viewtimeslot'];
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('inventory_v/package', $response);
    }

    public function addons()
    {
        $response['branchids'] = $this->session->userdata('adminbranchid');
        $respdate = $this->session->userdata('admindateid');
        $responcevalue = explode("-", $respdate);
        $response['departDate'] = "" . $responcevalue[2] . "/" . $responcevalue[1] . "/" . $responcevalue[0] . "";
        $parameter2 = array('act_mode' => 's_viewtimeslottimeaddones',
            'Param1' => $response['branchids'],
            'Param2' => $response['departDate'],
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        $response['s_viewtimeslotaddones'] = $this->supper_admin->call_procedure('proc_viewtimeslot_v', $parameter2


        );

        $arr = (array)$response['s_viewtimeslot'];


        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('inventory_v/addons', $response);
    }


    public function thresholdd()
    {

        $response['branchids'] = $this->session->userdata('adminbranchid');
        $respdate = $this->session->userdata('admindateid');
        $responcevalue = explode("-", $respdate);

        $response['departDate'] = "" . $responcevalue[2] . "/" . $responcevalue[1] . "/" . $responcevalue[0] . "";

        $parameter1 = array('act_mode' => 's_viewtimeslottimepackage',
            'Param1' => $response['branchids'],
            'Param2' => $response['departDate'],
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        $response['s_viewtimeslot'] = $this->supper_admin->call_procedure('proc_viewtimeslot_v', $parameter1


        );

        $parameter2 = array('act_mode' => 's_viewtimeslotaddones',
            'Param1' => $response['branchids'],
            'Param2' => $response['departDate'],
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        $response['s_viewtimeslotaddones'] = $this->supper_admin->call_procedure('proc_viewtimeslot_v', $parameter2


        );

        $arr = (array)$response['s_viewtimeslot'];


        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('inventory_v/threshold', $response);

    }

    public function update_inv()
    {
        if($this->input->post('submit'))
        {
            $id_1 = $this->input->post('dailydate_id');
            $date_1 =$this->input->post('pre_date');

           foreach($id_1 as $a=>$b) {
               $parameter2 = array('act_mode' => 'insert_dailydate',
                   'Param1' => $b,
                   'Param2' => $date_1[$a],
                   'Param3' => '',
                   'Param4' => '',
                   'Param5' => '',
                   'Param6' => '',
                   'Param7' => '',
                   'Param8' => '',
                   'Param9' => '');
               $response['timeslot_total'] = (array)$this->supper_admin->call_procedure('proc_inventory_s', $parameter2);
           }
        }
        $response['branchids'] = $this->session->userdata('adminbranchid');
        $respdate = $this->session->userdata('admindateid');
        $responcevalue = explode("-", $respdate);
       // 13-12-2017
        //2017-12-13   13/12/2017
       //pend($respdate);

        $response['departDate'] = "" . $responcevalue[2] . "-" . $responcevalue[1] . "-" . $responcevalue[0] . "";

        $parameter2 = array('act_mode' => 's_viewtimeslottimepackage',
            'Param1' => $response['branchids'],
            'Param2' => $response['departDate'],
            'Param3' => $respdate,
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        $response['s_viewtimeslot'] = $this->supper_admin->call_procedure('proc_viewtimeslot_v', $parameter2
        );


        $arr = (array)$response['s_viewtimeslot'];
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('inventory_v/update_inv', $response);
    }
    
    public function s_print()
    {

        $a = array('1'=>'2','2'=> '3','asdas'=>'5','asd');
    echo "<pre>";
    var_dump($a);

        
    }


}// end class
