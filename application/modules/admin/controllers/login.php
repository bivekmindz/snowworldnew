<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Login extends MX_Controller{

  public function __construct() {
    
    $this->load->model("supper_admin");
    $this->load->helper('my_helper');
     $this->load->library('session');
    
  }
  
//............. DEFAULT FUNCTION ............... //
  public function index(){
    $result = null;
    if($this->input->post('submit')){


      $this->form_validation->set_rules('email', 'Email','trim|required|xss_clean|valid_email');
      $this->form_validation->set_rules('password', 'Password','trim|required|max_length[20]|xss_clean');
      if($this->form_validation->run() == true){
       $parameter = array(
                    'useremail'    => $this->input->post('email'),
                    'userpassword' => md5($this->input->post('password')),
                    'act_mode'     => 'login',
                    'row_id'       => '');

       $data['result'] = $this->supper_admin->call_procedureRow('proc_Adminlogin', $parameter);

       if(isset($data['result']->LoginID) && $data['result']->LoginID >0){

 $this->session->set_userdata('snowworldval',$data['result']);
           $this->session->set_userdata('snowworldadmin',$data['result']);
          //$this->session->set_userdata('test','yahoo');

    // pend($this->session->all_userdata());
 redirect('admin/login/dashboard?empid='. base64_encode(($this->session->userdata('snowworldadmin')->EmployeeId)).'&uid='. base64_encode($this->session->userdata('snowworldadmin')->LoginID));
			  }
        else
          $result['result']='Invalid Login !!';
      }
	  }
    $this->load->view("user/login",$result);
  }

//.............  User Logout ............... //  
  public function logout(){
   $this->session->sess_destroy();
    //$this->session->unset_userdata('snowworld');
    redirect('admin/login');
  }

//.............  User Dashboard ............... //  
   public function dashboard()
   {
//       $paramater = array(
//           'act_mode'=>'count_order_by_month',
//           'Param1'=>'',
//           'Param2'=>'',
//           'Param3'=>'',
//           'Param4'=>'',
//           'Param5'=>'',
//           'Param6'=>'',
//           'Param7'=>'',
//           'Param8'=>'',
//           'Param9'=>''
//       );
//       //pend($param);
//       $response['data'] = $this->supper_admin->call_procedure('proc_order_s',$paramater);
//      // p($paramater);
//       pend($response['data']);
//       $this->load->view('helper/header');
//       $this->load->view('helper/nav');
//       $this->load->view('user/dashboard');
      redirect('admin/dashboard/index');

      //pend($this->session->userdata('snowworld'));
//       $this->load->view('helper/header');
//       $this->load->view('helper/nav');
//        $this->load->view('user/dashboard');
      // pend($this->session->all_userdata());
    }
   
//.............  Website Configuration ............... //  
   public function siteconfig(){
      $parameter         = array('act_mode'=>'viewsiteconfig','row_id'=>'');
      $data['viewsdata'] = $this->supper_admin->call_procedure('proc_siteconfig', $parameter); 
      $this->load->view('helper/header');
      $this->load->view('user/siteconfig',$data);
      $this->load->view('helper/footer');
    
  }

//.............  Update Website Configuration ............... //  
  public function updatesiteconfig(){


      $rowid = $this->uri->segment(4);
      //p($_POST);exit();

      if($_POST){
        $sitevalue   = $this->input->post('sitevalue');
        $sitevalueid = $this->input->post('sitevalueid');
        $parameter1  = array('act_mode'=>'updateconfigdata','row_id'=>$sitevalueid,'qty'=>'','pay_status'=>$sitevalue);
        $response['updatesite'] = $this->supper_admin->call_procedure('proc_orderflow', $parameter1);
        $this->session->set_flashdata('message', 'Data Update Successfully');
        redirect('admin/login/siteconfig');

      }//end if.

      $parameter         = array('act_mode'=>'viewupdateconfig','row_id'=>$rowid);
      $data['viewsdata'] = $this->supper_admin->call_procedureRow('proc_siteconfig', $parameter); 
      $this->load->view('helper/header');
      $this->load->view('user/updatesiteconfig',$data);
      $this->load->view('helper/footer');
    
  }

//.............  Unique Admin Password ............... //  
  public function adminUniquepassword() {


       $allsession    = $this->session->userdata('snowworld');
       $userid        = $allsession->LoginID;
       $useremail     = $allsession->UserName;
       
       $valuepassword = $_POST['oldpasswod'];

       $parameter = array(
                     'act_mode'    => 'checkpassword',
                     'n_email'     => $useremail,
                     'oldpassword' => md5($valuepassword),
                     'row_id'      => $userid
                    );
        
        $result['value'] = $this->supper_admin->supper_admin->call_procedureRow('proc_adminpasswordcheck', $parameter);
        //p($result['value']); exit;
        echo json_encode($result['value']);
  }

//.............  Update Admin Password ............... //  
  public function adminupdatepassword(){

    $this->load->view('helper/header');
     $this->load->view('helper/nav');

   //p($this->session->all_userdata()); exit;
    $allsession   = $this->session->userdata('snowworldadmin');
    $userid       = $allsession->LoginID;
    $useremail    = $allsession->UserName;
    
  //echo $userid."/ ".$useremail; exit;

    if ($this->input->post('submit')) {
        //p($_POST); exit;
        $valuepassword        = $this->input->post('old_password');
        $confirmvaluepassword = $this->input->post('confirm_password');
        
        $parameter            = array(
                                  'act_mode'    => 'updatepassword',
                                  'n_email'     => $useremail,
                                  'oldpassword' => md5($confirmvaluepassword),
                                  'row_id'      => $userid
                                );

       //p($parameter); exit;

        $data = $this->supper_admin->supper_admin->call_procedureRow('proc_adminpasswordcheck', $parameter);
           //p($data ); exit;
        if ($data->adminemailcount == 0) {
            $this->session->set_flashdata('message', 'Password Update Successfully!');
            redirect('admin/login/adminupdatepassword');
        }
      }  
     $this->load->view('adminupdatepassword');
     //$this->load->view('helper/footer');
  }
public function dashboardview()
    {

       $actmode =   $this->uri->segment(4); 
        


         $parameter = array('act_mode'=>$actmode,'row_id'  => '');

         $data['orderofday'] = $this->supper_admin->supper_admin->call_procedure('proc_dashboardview', $parameter);

         

        //p($data['orderofday']);
        $this->load->view('helper/header');
         $this->load->view('order/dashboard_view',$data);
    }

}//end class
?>