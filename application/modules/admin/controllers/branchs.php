<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Branchs extends MX_Controller
{

    public function __construct()
    {
        $this->load->model("supper_admin");
        $this->load->library('session');
        $this->load->library('upload');


        //$this->userfunction->loginAdminvalidation();
    }

    public function addbranchupdate($a)
    {

        if ($this->input->post('submit')) {
            /*p($_FILES);
            p($_POST);
            die();*/
            $this->form_validation->set_rules('locationid', 'locationid', 'required');
            $this->form_validation->set_rules('companyid', 'companyid', 'required');
            $this->form_validation->set_rules('branch_name', 'branch_name', 'required');
            $this->form_validation->set_rules('branch_internethandle', 'branch_internethandle', 'required');
            $this->form_validation->set_rules('branch_url', 'branch_url', 'required');
            $this->form_validation->set_rules('branch_addr', 'branch_addr', 'required');

            if ($this->form_validation->run() != FALSE) {
                //p($uploadedDetails);
                if ($_FILES['mail_image_background']['name'] == '') {
                    $parameter11 = array('act_mode' => 's_addbranch_update',
                        'Param1' => $this->input->post('locationid'),
                        'Param2' => $this->input->post('companyid'),
                        'Param3' => $this->input->post('branch_name'),
                        'Param4' => '',
                        'Param5' => $this->input->post('mail_image_background_hidden'),
                        'Param6' => $this->input->post('branch_internethandle'),
                        'Param7' => $this->input->post('branch_url'),
                        'Param8' => $this->input->post('branch_addr'),
                        'Param9' => base64_decode($a),
                        'Param10' => $this->input->post('cronejob'),
                        'Param11' => $this->input->post('hrsrestriction'),
                        'Param12' => $this->input->post('minrestriction'),
                        'Param13' => $this->input->post('cronjobrunvalue'),
                        'Param14' => '',
                        'Param15' => '',


                    );
                    //pend($parameter11);
                    $response = $this->supper_admin->call_procedure('proc_branch_s', $parameter11);
                    redirect("admin/branchs/addbranch?empid=" . $_GET['empid'] . "&uid=" . str_replace(".html", "", $_GET['uid']) . "");


                } else {
                    $configUpload1['upload_path'] = './assets/admin/images';              #the folder placed in the root of project
                    $configUpload1['allowed_types'] = 'gif|jpg|png|bmp|jpeg';       #allowed types description
                    $configUpload1['max_size'] = '0';                          #max size
                    $configUpload1['max_width'] = '0';                          #max width
                    $configUpload1['max_height'] = '0';                          #max height
                    $configUpload1['encrypt_name'] = true;                         #encrypt name of the uploaded file
                    $this->load->library('upload', $configUpload1);                  #init the upload class
                    if (!$this->upload->do_upload('mail_image_background')) {
                        $uploadedDetails1 = $this->upload->display_errors();
                        $this->session->set_flashdata('message', $uploadedDetails1);
                    } else {
                        $uploadedDetails1 = $this->upload->data();
                        //p($uploadedDetails1);
                        $parameter11 = array(
                            'act_mode' => 's_addbranch_update',
                            'Param1' => $this->input->post('locationid'),
                            'Param2' => $this->input->post('companyid'),
                            'Param3' => $this->input->post('branch_name'),
                            'Param4' => '',
                            'Param5' => $uploadedDetails1['file_name'],
                            'Param6' => $this->input->post('branch_internethandle'),
                            'Param7' => $this->input->post('branch_url'),
                            'Param8' => $this->input->post('branch_addr'),
                            'Param9' => base64_decode($a),
                            'Param10' => $this->input->post('cronejob'),
                            'Param11' => $this->input->post('hrsrestriction'),
                            'Param12' => $this->input->post('minrestriction'),
                            'Param13' => $this->input->post('cronjobrunvalue'),
                            'Param14' => '',
                            'Param15' => '',);
                        //pend($parameter11);
                        $response = $this->supper_admin->call_procedure('proc_branch_s', $parameter11);
                        $this->session->set_flashdata('message', 'inserted sucessfully');
                        redirect("admin/branchs/addbranch?empid=" . $_GET['empid'] . "&uid=" . str_replace(".html", "", $_GET['uid']) . "");

                    }
                }


            } else {
                $this->session->set_flashdata('message', validation_errors());
                redirect("admin/branchs/addbranch?empid=" . $_GET['empid'] . "&uid=" . str_replace(".html", "", $_GET['uid']) . "");

            }
        }


        $parameter1 = array('act_mode' => 's_viewcompany',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
        );
        $response['vieww_company'] = $this->supper_admin->call_procedure('proc_location_v', $parameter1);
        $parameter2 = array('act_mode' => 's_viewbranch',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',);
        $response['s_viewbranch'] = $this->supper_admin->call_procedure('proc_branch_s', $parameter2);
        //pend($response['s_viewbranch']);
        $parameter = array('act_mode' => 'viewcountry', 'row_id' => '', 'counname' => '', 'coucode' => '', 'commid' => '');
        $response['vieww'] = $this->supper_admin->call_procedure('proc_geographic', $parameter);
        $parameter2 = array('act_mode' => 's_viewbranch_update',
            'Param1' => base64_decode($a),
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',);
        $response['s_viewbranch'] = $this->supper_admin->call_procedure('proc_branch_s', $parameter2);
        $parameter4 = array('act_mode' => 'viewlocation', 'row_id' => '', 'counname' => '', 'coucode' => '', 'commid' => '');
        $response['vieww_location'] = $this->supper_admin->call_procedure('proc_geographic', $parameter4);
        //p($response['s_viewbranch']);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('branch/addbranchupdate', $response);

    }

    /*Add branch by zzz*/
    public function addbranch()
    {
        if ($this->input->post('submit'))
        {
            $this->form_validation->set_rules('locationid', 'locationid', 'required');
            $this->form_validation->set_rules('companyid', 'companyid', 'required');
            $this->form_validation->set_rules('branch_name', 'branch_name', 'required');
            $this->form_validation->set_rules('branch_internethandle', 'branch_internethandle', 'required');
           // $this->form_validation->set_rules('branch_logo_image', 'branch_logo_image', 'required');
            $this->form_validation->set_rules('branch_url', 'branch_url', 'required');
            $this->form_validation->set_rules('branch_addr', 'branch_addr', 'required');
            $this->form_validation->set_rules('cronejob', 'branch_addr', 'required');
            $this->form_validation->set_rules('hrsrestriction', 'branch_addr', 'required');

            if ($this->form_validation->run() != FALSE)
            {
                $data1 = $this->do_upload('branch_logo_image');
                $img1   = $data1['name']['upload_data']['file_name'];
                $data2 = $this->do_upload('mail_image_background');
                $img2   = $data2['name']['upload_data']['file_name'];

                        $parameter11 = array(
                            'act_mode' => 's_addbranch',
                            'Param1' => $this->input->post('locationid'),
                            'Param2' => $this->input->post('companyid'),
                            'Param3' => $this->input->post('branch_name'),
                            'Param4' => $img1,
                            'Param5' => $img2,
                            'Param6' => $this->input->post('branch_internethandle'),
                            'Param7' => $this->input->post('branch_url'),
                            'Param8' => $this->input->post('branch_addr'),
                            'Param9' => '',
                            'Param10' => $this->input->post('cronejob'),
                            'Param11' => $this->input->post('hrsrestriction'),
                            'Param12' => $this->input->post('minrestriction'),
                            'Param13' => $this->input->post('cronjobrunvalue'),
                            'Param14' => '',
                            'Param15' => ''
                        );

                        $response = $this->supper_admin->call_procedure('proc_branch_s', $parameter11);

                    }
            $this->session->set_flashdata('message', 'inserted sucessfully');
        }



        $parameter1 = array('act_mode' => 's_viewcompany',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        $response['vieww_company'] = $this->supper_admin->call_procedure('proc_location_v', $parameter1);
        $parameter2 = array('act_mode' => 's_viewbranch',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',);
        $response['s_viewbranch'] = $this->supper_admin->call_procedure('proc_branch_s', $parameter2);
        //pend($response['s_viewbranch']);
        $parameter = array('act_mode' => 'viewcountry', 'row_id' => '', 'counname' => '', 'coucode' => '', 'commid' => '');
        $response['vieww'] = $this->supper_admin->call_procedure('proc_geographic', $parameter);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('branch/addbranch', $response);

    }




      public function addbranch_old_5_may()
    {
        if ($this->input->post('submit')) {
            //p($_FILES);
            //p($_POST);
            $this->form_validation->set_rules('locationid', 'locationid', 'required');
            $this->form_validation->set_rules('companyid', 'companyid', 'required');
            $this->form_validation->set_rules('branch_name', 'branch_name', 'required');
            $this->form_validation->set_rules('branch_internethandle', 'branch_internethandle', 'required');
            $this->form_validation->set_rules('branch_url', 'branch_url', 'required');
            $this->form_validation->set_rules('branch_addr', 'branch_addr', 'required');
            if ($this->form_validation->run() != FALSE) {
                $configUpload['upload_path'] = './assets/admin/images';              #the folder placed in the root of project
                $configUpload['allowed_types'] = 'gif|jpg|png|bmp|jpeg';       #allowed types description
                $configUpload['max_size'] = '0';                          #max size
                $configUpload['max_width'] = '0';                          #max width
                $configUpload['max_height'] = '0';                          #max height
                $configUpload['encrypt_name'] = true;                         #encrypt name of the uploaded file
                $this->load->library('upload', $configUpload);                  #init the upload class
                if (!$this->upload->do_upload('branch_logo_image')) {
                    $uploadedDetails = $this->upload->display_errors();
                    $this->session->set_flashdata('message', $uploadedDetails);
                } else {
                    $uploadedDetails    = $this->upload->data();
                    //p($uploadedDetails);
                    $configUpload1['upload_path'] = './assets/admin/images';              #the folder placed in the root of project
                    $configUpload1['allowed_types'] = 'gif|jpg|png|bmp|jpeg';       #allowed types description
                    $configUpload1['max_size'] = '0';                          #max size
                    $configUpload1['max_width'] = '0';                          #max width
                    $configUpload1['max_height'] = '0';                          #max height
                    $configUpload1['encrypt_name'] = true;                         #encrypt name of the uploaded file
                    $this->load->library('upload', $configUpload1);                  #init the upload class
                    if (!$this->upload->do_upload('mail_image_background')) {
                        $uploadedDetails1 = $this->upload->display_errors();
                        $this->session->set_flashdata('message', $uploadedDetails1);
                    }
                    else {
                        $uploadedDetails1    = $this->upload->data();
                        //p($uploadedDetails1);
                        $parameter11 = array('act_mode' => 's_addbranch',
                            'Param1' => $this->input->post('locationid'),
                            'Param2' => $this->input->post('companyid'),
                            'Param3' => $this->input->post('branch_name'),
                            'Param4' => $uploadedDetails['file_name'],
                            'Param5' => $uploadedDetails1['file_name'],
                            'Param6' => $this->input->post('branch_internethandle'),
                            'Param7' => $this->input->post('branch_url'),
                            'Param8' => $this->input->post('branch_addr'),
                            'Param9' => '',
                            'Param10' => '',
                            'Param11' => '',
                            'Param12' => '',
                            'Param13' => '',
                            'Param14' => '',
                            'Param15' => '',);
                        //pend($parameter11);
                        $response = $this->supper_admin->call_procedure('proc_branch_s', $parameter11);
                        $this->session->set_flashdata('message', 'inserted sucessfully');
                    }
                    }
                }
            }


        $parameter1 = array('act_mode' => 's_viewcompany',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        $response['vieww_company'] = $this->supper_admin->call_procedure('proc_location_v', $parameter1);
        $parameter2 = array('act_mode' => 's_viewbranch',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',);
        $response['s_viewbranch'] = $this->supper_admin->call_procedure('proc_branch_s', $parameter2);
        //pend($response['s_viewbranch']);
        $parameter = array('act_mode' => 'viewcountry', 'row_id' => '', 'counname' => '', 'coucode' => '', 'commid' => '');
        $response['vieww'] = $this->supper_admin->call_procedure('proc_geographic', $parameter);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('branch/addbranch', $response);

    }
    
 /*Add branch by zzz*/
    public function addbranch_old1()
    {
        if ($this->input->post('submit')) {
            //p($_FILES);
            //p($_POST);
            $this->form_validation->set_rules('locationid', 'locationid', 'required');
            $this->form_validation->set_rules('companyid', 'companyid', 'required');
            $this->form_validation->set_rules('branch_name', 'branch_name', 'required');
            $this->form_validation->set_rules('branch_internethandle', 'branch_internethandle', 'required');
            $this->form_validation->set_rules('branch_url', 'branch_url', 'required');
            if ($this->form_validation->run() != FALSE) {
                $configUpload['upload_path'] = './assets/admin/images';              #the folder placed in the root of project
                $configUpload['allowed_types'] = 'gif|jpg|png|bmp|jpeg';       #allowed types description
                $configUpload['max_size'] = '0';                          #max size
                $configUpload['max_width'] = '0';                          #max width
                $configUpload['max_height'] = '0';                          #max height
                $configUpload['encrypt_name'] = true;                         #encrypt name of the uploaded file
                $this->load->library('upload', $configUpload);                  #init the upload class
                if (!$this->upload->do_upload('branch_logo_image')) {
                    $uploadedDetails = $this->upload->display_errors();
                    $this->session->set_flashdata('message', $uploadedDetails);
                } else {
                    $uploadedDetails    = $this->upload->data();
                    //p($uploadedDetails);
                    $configUpload1['upload_path'] = './assets/admin/images';              #the folder placed in the root of project
                    $configUpload1['allowed_types'] = 'gif|jpg|png|bmp|jpeg';       #allowed types description
                    $configUpload1['max_size'] = '0';                          #max size
                    $configUpload1['max_width'] = '0';                          #max width
                    $configUpload1['max_height'] = '0';                          #max height
                    $configUpload1['encrypt_name'] = true;                         #encrypt name of the uploaded file
                    $this->load->library('upload', $configUpload1);                  #init the upload class
                    if (!$this->upload->do_upload('mail_image_background')) {
                        $uploadedDetails1 = $this->upload->display_errors();
                        $this->session->set_flashdata('message', $uploadedDetails1);
                    }
                    else {
                        $uploadedDetails1    = $this->upload->data();
                        //p($uploadedDetails1);
                        $parameter11 = array('act_mode' => 's_addbranch',
                            'Param1' => $this->input->post('locationid'),
                            'Param2' => $this->input->post('companyid'),
                            'Param3' => $this->input->post('branch_name'),
                            'Param4' => $uploadedDetails['file_name'],
                            'Param5' => $uploadedDetails1['file_name'],
                            'Param6' => $this->input->post('branch_internethandle'),
                            'Param7' => $this->input->post('branch_url'),
                            'Param8' => '',
                            'Param9' => '',
                            'Param10' => '',
                            'Param11' => '',
                            'Param12' => '',
                            'Param13' => '',
                            'Param14' => '',
                            'Param15' => '',);
                        //pend($parameter11);
                        $response = $this->supper_admin->call_procedure('proc_branch_s', $parameter11);
                        $this->session->set_flashdata('message', 'inserted sucessfully');
                    }
                    }
                }
            }


        $parameter1 = array('act_mode' => 's_viewcompany',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        $response['vieww_company'] = $this->supper_admin->call_procedure('proc_location_v', $parameter1);
        $parameter2 = array('act_mode' => 's_viewbranch',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',);
        $response['s_viewbranch'] = $this->supper_admin->call_procedure('proc_branch_s', $parameter2);
        //pend($response['s_viewbranch']);
        $parameter = array('act_mode' => 'viewcountry', 'row_id' => '', 'counname' => '', 'coucode' => '', 'commid' => '');
        $response['vieww'] = $this->supper_admin->call_procedure('proc_geographic', $parameter);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('branch/addbranch', $response);

    }
    /*Add branch by zzz*/
    public function addbranch_old()
    {
        if ($this->input->post('submit')) {
            $this->form_validation->set_rules('locationid', 'locationid', 'required');
            $this->form_validation->set_rules('companyid', 'companyid', 'required');
            $this->form_validation->set_rules('branch_name', 'branch_name', 'required');
            if ($this->form_validation->run() != FALSE) {

                $parameter11 = array('act_mode' => 's_addbranch',
                    'Param1' => $this->input->post('locationid'),
                    'Param2' => $this->input->post('companyid'),
                    'Param3' => $this->input->post('branch_name'),
                    'Param4' => '',
                    'Param5' => '',
                    'Param6' => '',
                    'Param7' => '',
                    'Param8' => '',
                    'Param9' => '',
                    'Param10' => '',
                    'Param11' => '',
                    'Param12' => '',
                    'Param13' => '',
                    'Param14' => '',
                    'Param15' => '',);
                //pend($parameter);
                $response = $this->supper_admin->call_procedure('proc_branch_s', $parameter11);
                $this->session->set_flashdata('message', 'inserted sucessfully');

            }
        }

        $parameter1 = array('act_mode' => 's_viewcompany',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        $response['vieww_company'] = $this->supper_admin->call_procedure('proc_location_v', $parameter1);
        $parameter2 = array('act_mode' => 's_viewbranch',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',);
        $response['s_viewbranch'] = $this->supper_admin->call_procedure('proc_branch_s', $parameter2);
        //pend($response['s_viewbranch']);
        $parameter = array('act_mode' => 'viewcountry', 'row_id' => '', 'counname' => '', 'coucode' => '', 'commid' => '');
        $response['vieww'] = $this->supper_admin->call_procedure('proc_geographic', $parameter);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('branch/addbranch', $response);

    }

    /*DELETE BRANCH BY zzz*/
    public function branchdelete1()
    {
        $parameter = array('act_mode' => 'delete_branch',
            'Param1' => $this->uri->segment('4'),
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',);
        //pend($parameter);
        $response['vieww'] = $this->supper_admin->call_procedure('proc_branch_s', $parameter);
        redirect("admin/branchs/addbranch?empid=".$_GET['empid']."&uid=".str_replace(".html","",$_GET['uid'])."");

    }

    /*UPDATE BRANCH BY zzz*/
    public function updatebranch()
    {
        echo $this->input->post('update_branch_id');
        echo $this->input->post('locationid_update');
        echo $this->input->post('companyid_update');
        echo $this->input->post('branch_name_update');

        if ($this->input->post('submit')) {
            $this->form_validation->set_rules('locationid_update', 'locationid', 'required');
            $this->form_validation->set_rules('companyid_update', 'companyid', 'required');
            $this->form_validation->set_rules('branch_name_update', 'branch_name', 'required');
            if ($this->form_validation->run() != FALSE) {

                $parameter11 = array('act_mode' => 's_updatebranch',
                    'Param1' => $this->input->post('locationid_update'),
                    'Param2' => $this->input->post('companyid_update'),
                    'Param3' => $this->input->post('branch_name_update'),
                    'Param4' => $this->input->post('update_branch_id'),
                    'Param5' => '',
                    'Param6' => '',
                    'Param7' => '',
                    'Param8' => '',
                    'Param9' => '',
                    'Param10' => '',
                    'Param11' => '',
                    'Param12' => '',
                    'Param13' => '',
                    'Param14' => '',
                    'Param15' => '',);
                //pend($parameter);
                $response = $this->supper_admin->call_procedure('proc_branch_s', $parameter11);
                $this->session->set_flashdata('message', 'updated sucessfully');
                redirect("admin/branchs/addbranch?empid=".$_GET['empid']."&uid=".str_replace(".html","",$_GET['uid'])."");

            }
            else {
                $this->session->set_flashdata('message', 'Not updated please try again');
                redirect("admin/branchs/addbranch?empid=".$_GET['empid']."&uid=".str_replace(".html","",$_GET['uid'])."");
            }
        }
    }
    
     public  function branchstatus($a,$b)
    {
        $status =  base64_decode($b)== 1 ? 0 : 1;
        $parameter1 = array('act_mode' => 'branchstatus',
            'Param1' => base64_decode($a),
            'Param2' => $status,
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',);
        //pend($parameter1);
        $response['vieww'] = $this->supper_admin->call_procedure('proc_branch_s', $parameter1);
        redirect("admin/branchs/addbranch?empid=".$_GET['empid']."&uid=".str_replace(".html","",$_GET['uid'])."");

    }

    public function image_upload($field_name)
    {

        $config['upload_path']    = './assets/admin/images';
        $config['allowed_types']  = 'gif|jpg|png|bmp|jpeg';
        $config['max_size']       = '0';
        $config['max_width'] = '0';                          #max width
        $config['max_height'] = '0';                          #max height
        $config['encrypt_name'] = true;
        $config['file_name']      = time().$_FILES[$field_name]['name'];

        $this->load->library('upload', $config);

        $this->upload->initialize($config);

        if ( ! $this->upload->do_upload($field_name)){
            $data['error']       = array('error' => $this->upload->display_errors());
        } else
        {
            $data['name']        = array('upload_data' => $this->upload->data());
        }
        return $data;
    }

    public function do_upload($field_name)
    {
        $config['upload_path'] = './assets/admin/images';
        $config['allowed_types'] = 'png|jpg|jpeg|gif|png|pdf';
        $config['max_size'] = '10000000';
        $config['file_name']  = time().$_FILES[$field_name]['name'];

        $this->upload->initialize($config);
        if ( ! $this->upload->do_upload($field_name)){
            $data['error'] = array('error' => $this->upload->display_errors());

        } else {
            $data['name'] = array('upload_data' => $this->upload->data());
            // p($data['name']);exit();
        }
        return $data;
    }



}// end class
?>