<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Payment extends MX_Controller
{
    public function __construct()
    {
        $this->load->model("supper_admin");
        $this->load->helper('my_helper');
    }

    /*ADD,VIEW,UPDATE Sms BY zzz*/
    public function tearmcondtion()
    { 
        if($this->input->post('submit'))
        {
            $this->form_validation->set_rules('tcname', 'tcname', 'required');


            if ($this->form_validation->run() != FALSE)
            {
                $parameter1 = array( 'act_mode'=>'tcinsert',
                    'Param1'=>$this->input->post('branchids'),
                    'Param2'=>$this->input->post('tcname'),
                    'Param3'=>'',
                    'Param4'=>'',
                    'Param5'=>'',
                    'Param6'=>'',
                    'Param7'=>'',
                    'Param8'=>'',
                    'Param9'=>'',
                    'Param10' =>'');
                $response = $this->supper_admin->call_procedure('proc_tearmcond',$parameter1);

            }
            else{
                $this->session->set_flashdata('message',validation_errors());
            }
        }


        $parameter2 = array( 'act_mode'=>'s_viewbranch',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        $response['s_viewbranch'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter2);

        $parameter3 = array( 'act_mode'=>'tcview',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'','Param10'=>'');
        $response['s_viewpayment'] = $this->supper_admin->call_procedure('proc_tearmcond',$parameter3);


        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('gateway/termcond',$response);

    }


    public function termstatus()
    {
        $a = base64_decode($this->uri->segment('5')) == 1 ? 0 : 1;

        $parameter3 = array( 'act_mode'=>'termpaymentstatus',
            'Param1'=>base64_decode($this->uri->segment('4')),
            'Param2'=>$a,
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'','Param10'=>'');
        $response = $this->supper_admin->call_procedure('proc_tearmcond',$parameter3);
        redirect("admin/Payment/tearmcondtion?empid=".$_GET['empid']."&uid=".str_replace(".html","",$_GET['uid'])."");


    }



    public function teatmcondde()
    {

        $parameter3 = array( 'act_mode'=>'paymentdelete',
            'Param1'=>base64_decode($this->uri->segment('4')),
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'',
            'Param10'=>'');
        $response = $this->supper_admin->call_procedure('proc_tearmcond',$parameter3);
        //p($parameter3);
        //pend($response);
        redirect("admin/Payment/tearmcondtion?empid=".$_GET['empid']."&uid=".str_replace(".html","",$_GET['uid'])."");


    }


    /*ADD,VIEW,UPDATE Sms BY zzz*/
    public function smsgateway()
    {
        if($this->input->post('submit'))
        {
            $this->form_validation->set_rules('feedid', 'feedid', 'required');
            $this->form_validation->set_rules('username', 'username', 'required');
            $this->form_validation->set_rules('password', 'password', 'required');

            if ($this->form_validation->run() != FALSE)
            {
                $parameter1 = array( 'act_mode'=>'smsinsert',
                    'Param1'=>$this->input->post('branchids'),
                    'Param2'=>$this->input->post('username'),
                    'Param3'=>$this->input->post('password'),
                    'Param4'=>$this->input->post('feedid'),
                    'Param5'=>'',
                    'Param6'=>'',
                    'Param7'=>'',
                    'Param8'=>'',
                    'Param9'=>'',
                    'Param10' =>'');
                $response = $this->supper_admin->call_procedure('proc_smsgatway',$parameter1);
              
            }
            else{
                $this->session->set_flashdata('message',validation_errors());
            }
        }


        $parameter2 = array( 'act_mode'=>'s_viewbranch',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        $response['s_viewbranch'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter2);

        $parameter3 = array( 'act_mode'=>'smsview',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'','Param10'=>'');
        $response['s_viewpayment'] = $this->supper_admin->call_procedure('proc_smsgatway',$parameter3);

        //pend($response['s_viewpayment']);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('gateway/smsgateway',$response);

    }

    public function smsgatwaystatus()
    {
        $a = base64_decode($this->uri->segment('5')) == 1 ? 0 : 1;

        $parameter3 = array( 'act_mode'=>'smspaymentstatus',
            'Param1'=>base64_decode($this->uri->segment('4')),
            'Param2'=>$a,
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'','Param10'=>'');
        $response = $this->supper_admin->call_procedure('proc_smsgatway',$parameter3);
        redirect("admin/Payment/smsgateway?empid=".$_GET['empid']."&uid=".str_replace(".html","",$_GET['uid'])."");


    }
    public function smspaymentup()
    {

        $parameter3 = array( 'act_mode'=>'paymentup',
            'Param1'=>base64_decode($this->uri->segment('4')),
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'',
            'Param10'=>'');
        $response['paymentup'] = (array)$this->supper_admin->call_procedure('proc_smsgatway',$parameter3);

        if($this->input->post('submit'))
        {
            $this->form_validation->set_rules('branchids', 'branchids', 'required');
            $this->form_validation->set_rules('feedid', 'feedid', 'required');
            $this->form_validation->set_rules('username', 'username', 'required');
            $this->form_validation->set_rules('password', 'password', 'required');

            if ($this->form_validation->run() != FALSE)
            {
                $parameter1 = array( 'act_mode'=>'paymentupdate',
                    'Param1'=>$this->input->post('branchids'),
                    'Param2'=>$this->input->post('feedid'),
                    'Param3'=>$this->input->post('username'),
                    'Param4'=>$this->input->post('password'),
                    'Param5'=>'',
                    'Param6'=>'',
                    'Param7'=>'',
                    'Param8'=>'',
                    'Param9'=>'',
                    'Param10' =>base64_decode($this->uri->segment('4')));
                $response = $this->supper_admin->call_procedure('proc_smsgatway',$parameter1);
                $this->session->set_flashdata('message','Updated sucessfully');
                redirect("admin/Payment/smsgateway?empid=".$_GET['empid']."&uid=".str_replace(".html","",$_GET['uid'])."");

            }
            else{
                $this->session->set_flashdata('message',validation_errors());
            }
        }


        $parameter2 = array( 'act_mode'=>'s_viewbranch',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        $response['s_viewbranch'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter2);


        //pend($response['s_viewpayment']);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('gateway/smspaymentup',$response);

    }
    public function smspaymentde()
    {

        $parameter3 = array( 'act_mode'=>'paymentdelete',
            'Param1'=>base64_decode($this->uri->segment('4')),
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'',
            'Param10'=>'');
        $response = $this->supper_admin->call_procedure('proc_smsgatway',$parameter3);
        //p($parameter3);
        //pend($response);
        redirect("admin/Payment/smsgateway?empid=".$_GET['empid']."&uid=".str_replace(".html","",$_GET['uid'])."");

    }

    /*ADD,VIEW,UPDATE COMPANY BY zzz*/
    public function gateway()
    {
        if($this->input->post('submit'))
        {
            $this->form_validation->set_rules('branchids', 'branchids', 'required');
            $this->form_validation->set_rules('merchant_id', 'merchant_id', 'required');
            $this->form_validation->set_rules('access_key', 'access_key', 'required');
            $this->form_validation->set_rules('working_key', 'working_key', 'required');
            $this->form_validation->set_rules('sucess_url', 'sucess_url', 'required');
            $this->form_validation->set_rules('fail_url', 'fail_url', 'required');
            $this->form_validation->set_rules('currency_nam', 'currency_nam', 'required');
            $this->form_validation->set_rules('language_nam', 'language_nam', 'required');$this->form_validation->set_rules('prefix_nam', 'prefix_nam', 'required');
            if ($this->form_validation->run() != FALSE)
            {
                $parameter1 = array( 'act_mode'=>'paymentinsert',
                    'Param1'=>$this->input->post('branchids'),
                    'Param2'=>$this->input->post('merchant_id'),
                    'Param3'=>$this->input->post('working_key'),
                    'Param4'=>$this->input->post('sucess_url'),
                    'Param5'=>$this->input->post('fail_url'),
                    'Param6'=>$this->input->post('currency_nam'),
                    'Param7'=>$this->input->post('language_nam'),
                    'Param8'=>$this->input->post('access_key'),
                    'Param9'=>$this->input->post('prefix_nam'),
                    'Param10' =>'');
                $response = $this->supper_admin->call_procedure('proc_payment_s',$parameter1);

            }
            else{
                $this->session->set_flashdata('message',validation_errors());
            }
        }


        $parameter2 = array( 'act_mode'=>'s_viewbranch',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        $response['s_viewbranch'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter2);

        $parameter3 = array( 'act_mode'=>'paymentview',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'','Param10'=>'');
        $response['s_viewpayment'] = $this->supper_admin->call_procedure('proc_payment_s',$parameter3);

        //pend($response['s_viewpayment']);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('gateway/gateway',$response);

    }

    public function paymentstatus()
    {
        $a = base64_decode($this->uri->segment('5')) == 1 ? 0 : 1;

        $parameter3 = array( 'act_mode'=>'paymentstatus',
            'Param1'=>base64_decode($this->uri->segment('4')),
            'Param2'=>$a,
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'','Param10'=>'');
        $response = $this->supper_admin->call_procedure('proc_payment_s',$parameter3);
        //p($parameter3);
        //pend($response);
        redirect("admin/Payment/gateway?empid=".$_GET['empid']."&uid=".str_replace(".html","",$_GET['uid'])."");


    }

    public function paymentde()
    {

        $parameter3 = array( 'act_mode'=>'paymentdelete',
            'Param1'=>base64_decode($this->uri->segment('4')),
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'',
        'Param10'=>'');
        $response = $this->supper_admin->call_procedure('proc_payment_s',$parameter3);
        //p($parameter3);
        //pend($response);
        redirect("admin/Payment/gateway?empid=".$_GET['empid']."&uid=".str_replace(".html","",$_GET['uid'])."");

    }



    public function paymentup()
    {

        $parameter3 = array( 'act_mode'=>'paymentup',
            'Param1'=>base64_decode($this->uri->segment('4')),
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'',
            'Param10'=>'');
        $response['paymentup'] = (array)$this->supper_admin->call_procedure('proc_payment_s',$parameter3);
        //p($parameter3);
        if($this->input->post('submit'))
        {
            $this->form_validation->set_rules('branchids', 'branchids', 'required');
            $this->form_validation->set_rules('merchant_id', 'merchant_id', 'required');
            $this->form_validation->set_rules('access_key', 'access_key', 'required');
            $this->form_validation->set_rules('working_key', 'working_key', 'required');
            $this->form_validation->set_rules('sucess_url', 'sucess_url', 'required');
            $this->form_validation->set_rules('fail_url', 'fail_url', 'required');
            $this->form_validation->set_rules('currency_nam', 'currency_nam', 'required');
            $this->form_validation->set_rules('language_nam', 'language_nam', 'required');$this->form_validation->set_rules('prefix_nam', 'prefix_nam', 'required');
            if ($this->form_validation->run() != FALSE)
            {
                $parameter1 = array( 'act_mode'=>'paymentupdate',
                    'Param1'=>$this->input->post('branchids'),
                    'Param2'=>$this->input->post('merchant_id'),
                    'Param3'=>$this->input->post('working_key'),
                    'Param4'=>$this->input->post('sucess_url'),
                    'Param5'=>$this->input->post('fail_url'),
                    'Param6'=>$this->input->post('currency_nam'),
                    'Param7'=>$this->input->post('language_nam'),
                    'Param8'=>$this->input->post('access_key'),
                    'Param9'=>$this->input->post('prefix_nam'),
                    'Param10' =>base64_decode($this->uri->segment('4')));
                $response = $this->supper_admin->call_procedure('proc_payment_s',$parameter1);
                $this->session->set_flashdata('message','Updated sucessfully');
                redirect("admin/Payment/gateway?empid=".$_GET['empid']."&uid=".str_replace(".html","",$_GET['uid'])."");

            }
            else{
                $this->session->set_flashdata('message',validation_errors());
            }
        }


        $parameter2 = array( 'act_mode'=>'s_viewbranch',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        $response['s_viewbranch'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter2);


        //pend($response['s_viewpayment']);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('gateway/gatewayup',$response);

    }

    /*DELETE COMPANY BY zzz*/


}// end class
?>