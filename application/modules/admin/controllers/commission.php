<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Commission extends MX_Controller
{
    public function __construct()
    {
        $this->load->model("supper_admin");
        $this->load->helper('my_helper');


 $this->load->library('session');


    }

    public  function commissionmaster(){


        if($this->input->post('submit'))
        {

            $start = $this->input->post('start');
            $end = $this->input->post('end');
            $commissionid = $this->input->post('commissionid');
             $parameter1 = array( 'act_mode'=>'delete_commission',
                    'Param1'=>'',
                    'Param2'=>'',
                    'Param3'=>'',
                    'Param4'=>'',
                    'Param5'=>'',
                    'Param6'=>'',
                    'Param7'=>'',
                    'Param8'=>'',
                    'Param9'=>''
                    );
              $response = $this->supper_admin->call_procedure('proc_timeslot_s',$parameter1);

                $a =0;
                foreach ($commissionid as $key => $value) {
                    if($value){
                $parameter1 = array( 'act_mode'=>'edit_commission',
                    'Param1'=>$start[$a],
                    'Param2'=>$end[$a],
                    'Param3'=>$value,
                    'Param4'=>'',
                    'Param5'=>'',
                    'Param6'=>'',
                    'Param7'=>'',
                    'Param8'=>'',
                    'Param9'=>$this->input->post('updateid'));
                $response = $this->supper_admin->call_procedure('proc_timeslot_s',$parameter1);
               
                 }
               $a++;
                }///exit;
            redirect("admin/commission/commissionmaster?empid=".$_GET['empid']."&uid=".str_replace(".html","",$_GET['uid'])."");
        }


        $parameter2 = array( 'act_mode'=>'get_commission_list',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        $response['commission'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter2);

        
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('timeslot/editcommission',$response);
    }

    public function agentlist()
    { 
     
        if(getMemberId()!=1)
        {
         redirect('admin/login/dashboard?empid='.$_GET['empid'].'&uid='.$_GET['uid'].'');
        }
   
        $parameterregsel=array(
            'act_mode' => 'selectpartner',
            'title' => '',
            'firstname' => '',
            'lastname' => '',
            'row_id' => '',

            'logintype' => '',
            'agencyname' => '',
            'office_address' => '',
            'city' => '',
            'state' => '',
            'pincode' => '',
            'telephone' => '',
            'mobile' => '',
            'fax' => '',
            'website' => '',
            'email' => '',
            'primarycontact_name' => '',
            'primarycontact_email_mobile' => '',
            'primarycontact_telephone' => '',
            'secondrycontact_name' => '',
            'secondrycontact_email_phone' => '',
            'secondrycontact_telephone' => '',
            'management_executives_name' => '',
            'management_executives_email_phone' =>'',
            'management_executives_telephone' => '',
            'branches' => '',

            'yearofestablism' => '',
            'organisationtype' => '',
            'lstno' =>'',
            'cstno' => '',
            'registrationno' =>'',
            'vatno' => '',
            'panno' => '',
            'servicetaxno' => '',
            'tanno' => '',
            'pfno' => '',
            'esisno' => '',
            'officeregistrationno' => '',
            'exceptiontax' => '',
            'otherexceptiontax' => '',
            'bank_benificialname' => '',
            'bank_benificialaccno' => '',
            'bank_benificialbankname' => '',
            'bank_benificialbranchname' => '',
            'bank_benificialaddress' => '',
            'bank_benificialifsc' => '',
            'bank_benificialswiftcode' => '',
            'bank_benificialibanno' => '',
            'intermidiatebankname' => '',
            'intermidiatebankaddress' => '',

            'intermidiatebankswiftcode' => '',
            'bank_ecs' => '',
            'copypancart' => '',
            'copyservicetax' => '',
            'copytan' => '',
            'copyaddressproof' => '',
            'emailstatus' => '',
            'emailcurrency' => '',
            'emailcountry' => '',
            'username' =>'',
            'password' => '',

            'countryid' => '',
            'stateid' => '',
            'cityid' => '',
            'branch_id' => '',
            'locationid' => '',
            'bank_creditamount' => '',
            'bank_agentcommision' => '',
            'aadhaar_number' =>$this->input->post('aadhaar_number'),
            'ppid' =>$this->input->post('ppid'),
            'pppermission' =>$this->input->post('pppermission'),
        );
        $response['agentvieww'] =   $this->supper_admin->call_procedure('proc_partnerregister_v', $parameterregsel);
        $dataList = array();
    
       foreach($response['agentvieww']  as $value){
        $parameter2 = array( 'act_mode'=>'getorderlistagent',
            'Param1'=>$value->agent_id,
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        $walletprice = $this->supper_admin->call_procedureRow('proc_packages_s',$parameter2); 
    
        /*Get Commission Price*/
        $getcommission =$this->user_commission($walletprice->cntorder);

             $parameter2 = array( 'act_mode'=>'getwalletamount',
            'Param1'=>$value->agent_id,
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        $data['walletprices'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter2);
        /*-----End Commission ----*/
         $list['agent_id'] = $value->agent_id;
         $list['agent_agencyname'] = $value->agent_agencyname;
         $list['agent_username'] = $value->agent_username;
         $list['agent_mobile'] = $value->agent_mobile;
         $list['agent_email'] = $value->agent_email;
         $list['bank_agentcommision'] = $value->bank_agentcommision;
         $list['credit_debit'] = $value->credit_debit;
         $list['agent_status'] = $value->agent_status;
        // $list['wallet_price'] = $walletprices->total_amount;
         $list['per_amount'] = $getcommission;

         $dataList[] = $list;
       }//p($dataList);
        $data['agentvieww'] = $dataList;

        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('timeslot/agent_commissinon', $data);
    }


 public function agentDetail(){
     $parameter2 = array( 
        'act_mode'=>'getwalletamount_total_history_list_detail',
        'Param1'=>$this->input->post('agentids'),
        'Param2'=>'',
        'Param3'=>'',
        'Param4'=>'',
        'Param5'=>'',
        'Param6'=>'',
        'Param7'=>'',
        'Param8'=>'',
        'Param9'=>'');
    $walletprices_history_detail= $this->supper_admin->call_procedure('proc_packages_s',$parameter2);
     $var .= '<table border ="2"><th>Order ID</th><th>Order Price</th><th>Order per commission</th><th>Commission per Price</th></tr>';
    foreach($walletprices_history_detail as $value){
     $var .= 
     '<tr><td>'.$value->oid.'</td>
       <td>'.$value->order_price.'</td>
       <td>'.$value->commission.' '.'%'.'</td>
       <td>'.$value->commission_price.'</td></tr>';
       $totalPrice += $value->commission_price;
    }
    $var .='<tr style="background-color:green"><td colspan="3">Total Commission</td><td>'.$totalPrice.'</td></tr></table><input type="hidden" value="'.$totalPrice.'" name="" id="totalamnt">';
     echo $var;
 }

  public function savedetailcommission(){

     $parameter = array('act_mode' => 'agentamount_add_new_credit',
                'Param1' => $this->input->post('agentids'),
                'Param2' => $this->input->post('totalamnt'),
                'Param3' => 'C',
                'Param4' => '0',
                'Param5' => 'Ad',
                'Param6' => '',
                'Param7' => '',
                'Param8' => '',
                'Param9' => ''
             );
           
    $response = $this->supper_admin->call_procedurerow('proc_agent_s', $parameter);
     $parameter_payment = array( 'act_mode'=>'update_status_commission',
        'Param1'=>$this->input->post('agentids'),
        'Param2'=>'',
        'Param3'=>'',
        'Param4'=>'',
        'Param5'=>'',
        'Param6'=>'',
        'Param7'=>'',
        'Param8'=>'',
        'Param9'=>'');
 
    $update_payment = $this->supper_admin->call_procedureRow('proc_packages_s',$parameter_payment);
    echo 1;          
  }

 public function paymenttime(){
      
        if($_POST['submit'] == 'Update'){
        $parameter_payment = array( 'act_mode'=>'update_commission_payment',
            'Param1'=>'',
            'Param2'=>$this->input->post('commisstiontime'),
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
     
        $update_payment = $this->supper_admin->call_procedureRow('proc_packages_s',$parameter_payment); 
      }
       $parameter2 = array( 'act_mode'=>'get_commission_payment',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        $data['commission'] = $this->supper_admin->call_procedureRow('proc_packages_s',$parameter2); 
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('timeslot/commisionpaymentsycle', $data);

 }
 
  public function paymenthistory(){

     if(getMemberId()!=1)
        {
         redirect('admin/login/dashboard?empid='.$_GET['empid'].'&uid='.$_GET['uid'].'');
        }
   
        $parameterregsel=array(
            'act_mode' => 'selectpartner_histry',
            'title' => '',
            'firstname' => '',
            'lastname' => '',
            'row_id' => '',

            'logintype' => '',
            'agencyname' => '',
            'office_address' => '',
            'city' => '',
            'state' => '',
            'pincode' => '',
            'telephone' => '',
            'mobile' => '',
            'fax' => '',
            'website' => '',
            'email' => '',
            'primarycontact_name' => '',
            'primarycontact_email_mobile' => '',
            'primarycontact_telephone' => '',
            'secondrycontact_name' => '',
            'secondrycontact_email_phone' => '',
            'secondrycontact_telephone' => '',
            'management_executives_name' => '',
            'management_executives_email_phone' =>'',
            'management_executives_telephone' => '',
            'branches' => '',

            'yearofestablism' => '',
            'organisationtype' => '',
            'lstno' =>'',
            'cstno' => '',
            'registrationno' =>'',
            'vatno' => '',
            'panno' => '',
            'servicetaxno' => '',
            'tanno' => '',
            'pfno' => '',
            'esisno' => '',
            'officeregistrationno' => '',
            'exceptiontax' => '',
            'otherexceptiontax' => '',
            'bank_benificialname' => '',
            'bank_benificialaccno' => '',
            'bank_benificialbankname' => '',
            'bank_benificialbranchname' => '',
            'bank_benificialaddress' => '',
            'bank_benificialifsc' => '',
            'bank_benificialswiftcode' => '',
            'bank_benificialibanno' => '',
            'intermidiatebankname' => '',
            'intermidiatebankaddress' => '',

            'intermidiatebankswiftcode' => '',
            'bank_ecs' => '',
            'copypancart' => '',
            'copyservicetax' => '',
            'copytan' => '',
            'copyaddressproof' => '',
            'emailstatus' => '',
            'emailcurrency' => '',
            'emailcountry' => '',
            'username' =>'',
            'password' => '',

            'countryid' => '',
            'stateid' => '',
            'cityid' => '',
            'branch_id' => '',
            'locationid' => '',
            'bank_creditamount' => '',
            'bank_agentcommision' => '',
            'aadhaar_number' =>$this->input->post('aadhaar_number'),
            'ppid' =>$this->input->post('ppid'),
            'pppermission' =>$this->input->post('pppermission'),
        );
        $response['agentvieww'] =   $this->supper_admin->call_procedure('proc_partnerregister_v', $parameterregsel);
        $dataList = array();
       foreach($response['agentvieww']  as $value){
        $parameter2 = array( 'act_mode'=>'getorderlistagent',
            'Param1'=>$value->agent_id,
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        $walletprice = $this->supper_admin->call_procedureRow('proc_packages_s',$parameter2); 
    
        /*Get Commission Price*/
        $getcommission =$this->user_commission($walletprice->cntorder);

             $parameter2 = array( 'act_mode'=>'getwalletamount_history',
            'Param1'=>$value->agent_id,
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        $data['walletprices'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter2);
        /*-----End Commission ----*/
         $list['agent_id'] = $value->agent_id;
         $list['agent_agencyname'] = $value->agent_agencyname;
         $list['agent_username'] = $value->agent_username;
         $list['agent_mobile'] = $value->agent_mobile;
         $list['agent_email'] = $value->agent_email;
         $list['bank_agentcommision'] = $value->bank_agentcommision;
         $list['credit_debit'] = $value->credit_debit;
         $list['agent_status'] = $value->agent_status;
        // $list['wallet_price'] = $walletprices->total_amount;
         $list['per_amount'] = $getcommission;

         $dataList[] = $list;
       }//p($dataList);
        $data['agentvieww'] = $dataList;

        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('timeslot/commisionpaymentsyclehistory', $data);
    }

 //Get Commission list per user

 public function get_commission_payment_list(){
   $parameter2 = array( 'act_mode'=>'get_commission_payment',
    'Param1'=>'',
    'Param2'=>'',
    'Param3'=>'',
    'Param4'=>'',
    'Param5'=>'',
    'Param6'=>'',
    'Param7'=>'',
    'Param8'=>'',
    'Param9'=>'');
    $commission_day = $this->supper_admin->call_procedureRow('proc_packages_s',$parameter2); 
    $chkupdation = $this->checkupdation();
    if($commission_day->commission_time == $chkupdation){ 
     $check_user_array  = $this->userArray();  
    foreach($check_user_array as $usercnt){
   // Genrate Randam code
   $randam = 'SNOW'.$this->keygen().$commission_day->commission_time; 

  //Set Commistion with price
  $getcommission =$this->user_commission($usercnt->cntuser);

   $parameter2 = array( 'act_mode'=>'get_commission_order',
    'Param1'=>'',
    'Param2'=>'',
    'Param3'=>'',
    'Param4'=>'',
    'Param5'=>'',
    'Param6'=>'',
    'Param7'=>'',
    'Param8'=>'',
    'Param9'=>'');
$commission_order = $this->supper_admin->call_procedure('proc_packages_s',$parameter2); 
//p($commission_order);exit;  
foreach($commission_order as $orderData){  
    //Set Commission price
    if($usercnt->uid == $orderData->uid){
     $getpromoprice = ($orderData->order_price * ($usercnt->cntuser / 100));
     $getpriceCommission = ($orderData->order_price - $getpromoprice);

   $parameter2 = array( 'act_mode'=>'update_order_payment_invoice',
    'Param1'=>$getpriceCommission,
    'Param2'=>$randam,
    'Param3'=>$usercnt->cntuser,
    'Param4'=>$getcommission,
    'Param5'=>$orderData->c_id,
    'Param6'=>'',
    'Param7'=>'',
    'Param8'=>'',
    'Param9'=>'');
     $commission_order = $this->supper_admin->call_procedure('proc_packages_s',$parameter2);
     }
    }
     
   }
    echo 'Successfully Updated';return;
 }
      echo 'Somthing went worng';
  
 }

public function keygen($length=10)
{
    $key = '';
    list($usec, $sec) = explode(' ', microtime());
    mt_srand((float) $sec + ((float) $usec * 100000));
    
    $inputs = array_merge(range('z','a'),range(0,9),range('A','Z'));

    for($i=0; $i<$length; $i++)
    {
        $key .= $inputs{mt_rand(0,61)};
    }
    return $key;
}

public function checkupdation(){
/*  $parameter2 = array( 'act_mode'=>'get_day_count_start',
    'Param1'=>'',
    'Param2'=>'',
    'Param3'=>'',
    'Param4'=>'',
    'Param5'=>'',
    'Param6'=>'',
    'Param7'=>'',
    'Param8'=>'',
    'Param9'=>'');
    $start_date = $this->supper_admin->call_procedureRow('proc_packages_s',$parameter2); */
   
     $parameter2 = array( 'act_mode'=>'get_day_count_end',
    'Param1'=>'',
    'Param2'=>'',
    'Param3'=>'',
    'Param4'=>'',
    'Param5'=>'',
    'Param6'=>'',
    'Param7'=>'',
    'Param8'=>'',
    'Param9'=>'');
     $end_date = $this->supper_admin->call_procedureRow('proc_packages_s',$parameter2); 
     $date = new DateTime(); 
     $start_date_get = $date->format('d');   
     $end_date_get = date('d', strtotime($end_date->created_on));
     $get_diff_date = $start_date_get-$end_date_get;
    return $get_diff_date;
}

    public function userArray(){
     $parameter2 = array( 'act_mode'=>'get_commission_order_user_array',
    'Param1'=>'',
    'Param2'=>'',
    'Param3'=>'',
    'Param4'=>'',
    'Param5'=>'',
    'Param6'=>'',
    'Param7'=>'',
    'Param8'=>'',
    'Param9'=>'');
 $commission_order = $this->supper_admin->call_procedure('proc_packages_s',$parameter2);
return $commission_order;
    }

  public function user_commission($ordercnt){

        $parameter2 = array( 'act_mode'=>'get_commission_list',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        $commission = $this->supper_admin->call_procedure('proc_packages_s',$parameter2);
         $comistiondata = ''; 
        if($commission[0]->start_ticket_id <= $ordercnt &&  $commission[0]->end_ticket_id >= $ordercnt){
               $comistiondata = $commission[0]->commission;  
        }
       
        if($commission[1]->start_ticket_id <= $ordercnt &&  $commission[1]->end_ticket_id >= $ordercnt){
                  $comistiondata = $commission[1]->commission;  
        }
         if($commission[2]->start_ticket_id <= $ordercnt &&  $commission[2]->end_ticket_id >= $ordercnt){
                  $comistiondata = $commission[2]->commission;  
        }
         if($commission[3]->start_ticket_id <= $ordercnt &&  $commission[3]->end_ticket_id >= $ordercnt){
                    $comistiondata = $commission[3]->commission;  
        }
         if($commission[4]->start_ticket_id <= $ordercnt &&  $commission[4]->end_ticket_id >= $ordercnt){
                   $comistiondata = $commission[4]->commission;  
        }
         if($commission[5]->start_ticket_id <= $ordercnt &&  $commission[5]->end_ticket_id >= $ordercnt){
                   $comistiondata = $commission[5]->commission;  
        }

        return $comistiondata;
  }


}// end class
?>