<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Galary extends MX_Controller
{
    public function __construct()
    {
        $this->load->model("supper_admin");
        $this->load->helper('my_helper');
 $this->load->library('session');
    }

    public function image()
    {
        $parameter2 = array( 'act_mode'=>'s_viewbranch',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        $response['s_viewbranch'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter2);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('galary/image', $response);
    }

}// end class
?>