<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Timeslots extends MX_Controller
{
    public function __construct()
    {
        $this->load->model("supper_admin");
        $this->load->helper('my_helper');


 $this->load->library('session');


    }

    public  function timeslotedit(){


        if($this->input->post('submit'))
        {

            $branchid = $this->input->post('branchids');
            $from = $this->input->post('field_name1');
            $from_minute = $this->input->post('minute_name1');
            $to = $this->input->post('field_name2');
            $to_minute = $this->input->post('minute_name2');
            $seats = $this->input->post('no_of_seats');
           // $packages_name = implode(",",$this->input->post('packages_name'));
           

              $addons=implode(',',$this->input->post('addon'));
             $packages=implode(',',$this->input->post('packages_name'));

            foreach($from as $a =>$b)
            {

             
                $parameter1 = array( 'act_mode'=>'s_edittimeslot',
                    'Param1'=>$branchid,
                    'Param2'=>$from[$a],
                    'Param3'=>$to[$a],
                    'Param4'=>$seats[$a],
                    'Param5'=>$from_minute[$a],
                    'Param6'=>$to_minute[$a],
                    'Param7'=>$packages,
                    'Param8'=>$addons,
                    'Param9'=>$this->input->post('updateid'));
                
                $response = $this->supper_admin->call_procedure('proc_timeslot_s',$parameter1);
            redirect("admin/timeslots/addTimeslot?empid=".$_GET['empid']."&uid=".str_replace(".html","",$_GET['uid'])."");

            }
        }


        $parameter2 = array( 'act_mode'=>'s_viewbranch',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        $response['s_viewbranch'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter2);

        $parameter3 = array( 'act_mode'=>'s_viewtimeslotval',
            'Param1'=>base64_decode($_GET['eid']),
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        $response['s_viewtimeslot'] = $this->supper_admin->call_procedurerow('proc_timeslot_s',$parameter3);
        //p($response['s_viewtimeslot']);exit();

        $parameter4 = array( 'act_mode'=>'s_viewpackage',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        //pend($parameter);
        $response['vieww_pack'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter4);



$parameter=array(
                        'act_mode'=>'AddonViewTimeslotPackage',
                        'Param1'=>'',
                        'Param2'=>'',
                        'Param3'=>'',
                        'Param4'=>'',
                        'Param5'=>'',
                        'Param6'=>'',
                        'Param7'=>'',
                        'Param8'=>'',
                        'Param9'=>'',
                        'Param10'=>'',
                        'Param11'=>'',
                        'Param12'=>'',
                        'Param13'=>'',
                        'Param14'=>'',
                        'Param15'=>'',
                    );
    $response['addon_view'] = $this->supper_admin->call_procedure('proc_timeslotspackages',$parameter);
       // pend($response['s_viewtimeslot']);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('timeslot/edittimeslot',$response);
    }




    public  function addTimeslot(){


        if($this->input->post('submit'))
        {
            $branchid = $this->input->post('branchids');
            $from = $this->input->post('field_name1');
             $from_minute = $this->input->post('minute_name1');
            $to = $this->input->post('field_name2');
              $to_minute = $this->input->post('minute_name2');
             $seats = $this->input->post('no_of_seats');


 
            foreach($from as $a =>$b)
            {
             
                $parameter11 = array( 'act_mode'=>'s_checktimeslot',
                    'Param1'=>$branchid,
                    'Param2'=>$from[$a],
                    'Param3'=>$to[$a],
                    'Param4'=>'',
                    'Param5'=>'',
                    'Param6'=>'',
                    'Param7'=>'',
                    'Param8'=>'',
                    'Param9'=>'');
                //p($parameter1);
                $response = $this->supper_admin->call_procedure('proc_timeslot_s',$parameter11);
                if($response[0]->cou > 0)
                { continue;}
                else{
             $addons=implode(',',$this->input->post('addon'.$a));
             $packages=implode(',',$this->input->post('package'.$a));
             //p($packages);exit();

                $parameter1 = array( 'act_mode'=>'s_addtimeslot',
                    'Param1'=>$branchid,
                    'Param2'=>$from[$a],
                    'Param3'=>$to[$a],
                    'Param4'=>$seats[$a],
                    'Param5'=>$from_minute[$a],
                    'Param6'=>$to_minute[$a],
                    'Param7'=>$packages,
                    'Param8'=>$addons,
                    'Param9'=>'');
                //p($parameter1);
                $response = $this->supper_admin->call_procedure('proc_timeslot_s',$parameter1);
                    }

            }
        }


$parameter=array(
                        'act_mode'=>'AddonViewTimeslotPackage',
                        'Param1'=>'',
                        'Param2'=>'',
                        'Param3'=>'',
                        'Param4'=>'',
                        'Param5'=>'',
                        'Param6'=>'',
                        'Param7'=>'',
                        'Param8'=>'',
                        'Param9'=>'',
                        'Param10'=>'',
                        'Param11'=>'',
                        'Param12'=>'',
                        'Param13'=>'',
                        'Param14'=>'',
                        'Param15'=>'',
                    );
    $response['addon_view'] = $this->supper_admin->call_procedure('proc_timeslotspackages',$parameter);

$parameter4 = array( 'act_mode'=>'s_viewpackage',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        //pend($parameter);
$response['vieww_pack'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter4);


        $parameter2 = array( 'act_mode'=>'s_viewbranch',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        $response['s_viewbranch'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter2);

        $parameter3 = array( 'act_mode'=>'s_viewtimeslot',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        $response['s_viewtimeslot'] = $this->supper_admin->call_procedure('proc_timeslot_s',$parameter3);
        //pend($response['s_viewtimeslot']);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('timeslot/timeslot',$response);
    }

    public function deletesingletime(){

        $parameter3 = array( 'act_mode'=>'deletesingletime',
            'Param1'=>$this->input->post('id'),
            'Param2'=>$this->input->post('from'),
            'Param3'=>$this->input->post('to'),
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        $response['s_viewtimeslot'] = $this->supper_admin->call_procedure('proc_timeslot_s',$parameter3);
        echo "hello";

    }
}// end class
?>