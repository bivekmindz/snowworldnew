<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Promocodes extends MX_Controller
{
    public function __construct()
    {
        $this->load->model("supper_admin");
        $this->load->helper('my_helper');
 $this->load->library('session');
 $this->load->helper('adminmenu_helper');
    }
   public function promo_edit()
    {

        $parameter9 = array( 'act_mode'=>'s_viewaddon_admin',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'',
        );
        $responseList['s_viewaddone'] = $this->supper_admin->call_procedure('proc_addon_s',$parameter9);

        $parameter2 = array( 'act_mode'=>'s_viewbranch',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'',

        );
        $responseList['s_viewbranch'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter2);

        $parameter = array('act_mode' => 'editselect_promo',
            'Param1' => $this->uri->segment('4'),
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' =>'',
            'Param11' =>'',
            'Param12' =>'');
        //pend($parameter);
        $responseList['vieww'] = $this->supper_admin->call_procedurerow('proc_promocode_s', $parameter);

           $parameter = array('act_mode' => 'editselect_promo_list_package',
            'Param1' => $this->uri->segment('4'),
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' =>'',
            'Param11' =>'',
            'Param12' =>'');
        //pend($parameter);
        $vieww_package = $this->supper_admin->call_procedure('proc_promocode_s', $parameter);
  
  

        $startDate = $responseList['vieww']->p_start_date;
        $endDate = $responseList['vieww']->p_expiry_date;
        $parameter1 = array('act_mode' => 'getpacagedata',
            'Param1' => '',
            'Param2' => $startDate,
            'Param3' => $endDate,
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' =>'',
            'Param11' =>'',
            'Param12' =>'');

        $response['vieww'] = $this->supper_admin->call_procedure('proc_promocode_s', $parameter1);
        $var = '';
    
        foreach($response['vieww'] as $getvalue){
         
        $parameter1 = array('act_mode' => 'getpacagedata_time',
        'Param1' => '',
        'Param2' => $getvalue->dailyinventory_date,
        'Param3' => '',
        'Param4' => '',
        'Param5' => '',
        'Param6' => '',
        'Param7' => '',
        'Param8' => '',
        'Param9' => '',
        'Param10' =>'',
        'Param11' =>'',
        'Param12' =>'');
        $gettime = $this->supper_admin->call_procedure('proc_promocode_s', $parameter1);

         $times ='';
         $listData =array();
       
        foreach($gettime as $time){    
             $parameterPa = array('act_mode' => 'getpacagedata_pakage',
            'Param1' => '',
            'Param2' => '',
            'Param3' => $time->dailyinventory_id,
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' =>'',
            'Param11' =>'',
            'Param12' =>'');

        $getpakage = $this->supper_admin->call_procedure('proc_promocode_s', $parameterPa);
        $pakageList= array();
        foreach($getpakage as $pakages){ 
            $lists['package_id'] =$pakages->dailyinventorypackages_packagesid;
            $lists['package_name'] =$pakages->package_name;
            $list['promopackage_timesallowed'] =$pakages->promopackage_timesallowed; 
            $list['promopackage_peruserallowed'] =$pakages->promopackage_peruserallowed; 
            $pakageList[] =$lists;
          }

           $list['dailyinventory_to'] =$time->dailyinventory_to;
           $list['dailyinventory_from'] =$time->dailyinventory_from;
           $list['dailyinventory_id'] =$time->dailyinventory_id; 
           $list['dailyinventory_minfrom'] =$time->dailyinventory_minfrom;
           $list['dailyinventory_minto'] =$time->dailyinventory_minto;  
           $list['package'] =$pakageList; 
           $listData[] =$list;
       }
  //  p($listData);

         $var .='<tr>
                     <td>'.$getvalue->dailyinventory_date.'</td>
                                <td colspan="3">
                                <div class="inner-table">
                                <div class="row-line">';

                                $i=0;
                             foreach($listData as $time){   
                                $promopackage_peruserallowed = '';
                                $promopackage_timesallowed='';
                                  if($time['promopackage_timesallowed']){
                                  $promopackage_timesallowed = $time['promopackage_timesallowed'];
                                 }else{
                                    $promopackage_timesallowed  = '';
                                 }
                                  if($time['promopackage_peruserallowed']){
                                    $promopackage_peruserallowed = $time['promopackage_peruserallowed'];
                                 }else{
                                     $promopackage_peruserallowed  = '';
                                 }
                            $var .='<div class="couting"><span class="date">'.$time['dailyinventory_from'].':'.$time['dailyinventory_minfrom'].'-'.$time['dailyinventory_to'].':'.$time['dailyinventory_minto'].'</span>';
                            $var .= '<span class="slect-p">
                            <input type="hidden" value='.$time['dailyinventory_id'].' name="dateid[]" id="frometime">
                           
                              
                               <span><input  type="text" maxlength="4" value ="'.$promopackage_timesallowed.'" name="timeallow[]" size="4" id="timeallow" onblur="" />
                                </span></span>
                                    <span class="slect-p">
                               <span><input  type="text" maxlength="4" value ="'.$promopackage_peruserallowed.'"  name="userallow[]" size="4" id="userallow" onblur="" />

                                </span></span>
                                </div>
                           </div>';
                                $i++;  }
                              $var .='</div>
                               
                         </div>
                  </tr>';
        }

        $responseList['listData'] =$var;
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('promocode/promo_edit', $responseList);
    }


    public function editpromo()
    {

        if($_POST['addones']!='') {
            $addonedata=implode(',', $_POST['addones']);
            $addoneqty=1;
            $promo=$this->input->post('promo');
            $promonumber=$this->input->post('promonumber');
        }
        else
        {
            $addonedata="";
            $addoneqty="";
            $promo=$this->input->post('promo');
            $promonumber=$this->input->post('promonumber');
        }


        $this->form_validation->set_rules('branchids', 'branchids', 'required');
        $this->form_validation->set_rules('promocode_name', 'promocode_name', 'required');

        if ($this->form_validation->run() == FALSE) {
            redirect('admin/promocodes/s_viewpromo');
        } else {
            foreach ($this->input->post('branchids') as $a) {
 
              $parameter = array(
                    'act_mode' => 'update_promo',
                    'Param1' => $a,
                    'Param2' => $this->input->post('promocode_name'),
                    'Param3' => $promo,
                    'Param4' => $promonumber,
                    'Param5' => $this->input->post('promocode_allowed_user'),
                    'Param6' => $this->input->post('promocode_allowed_times'),
                    'Param7' => $this->input->post('promocode_start'),
                    'Param8' => $this->input->post('promocode_end'),
                    'Param9' =>"".$this->input->post('user_type')."",
                    'Param10' =>$addonedata,
                    'Param11' =>$addoneqty,
                    'Param12' =>$this->input->post('uid'),
                    'Param13' =>$this->input->post('newuser'),
                    'Param14' =>'',
                    'Param15' =>'',
                    'Param16' =>'',
                    'Param17' =>'',
                    'Param18' =>'',
                    'Param19' =>'',
                    'Param20' =>''

                    );
                 $response['vieww'] = $this->supper_admin->call_procedure('proc_promocode_package', $parameter);
                 if($this->input->post('uid')){
                  $parametera = array(
                    'act_mode' => 'delete_promocode',
                    'Param1' => $this->input->post('uid'),
                    'Param2' => '',
                    'Param3' => '',
                    'Param4' => '',
                    'Param5' => '',
                    'Param6' => '',
                    'Param7' => '',
                    'Param8' => '',
                    'Param9' =>'',
                    'Param10' =>'',
                    'Param11' =>'',
                    'Param12' =>'',
                    'Param13' =>'',
                    'Param14' =>'',
                    'Param15' =>'',
                    'Param16' =>'',
                    'Param17' =>'',
                    'Param18' =>'',
                    'Param19' =>'',
                    'Param20' =>''

                    );
                  
                 $response['vieww'] = $this->supper_admin->call_procedure('proc_promocode_package', $parametera);
              $timeallow= $this->input->post('timeallow');
               $userallow= $this->input->post('userallow');
               $timeid= $this->input->post('dateid'); 
              $i=0;
              foreach($timeid as $timevalue){
               if(!empty($timeallow[$i]) && !empty($userallow[$i])){
              $parameter = array(
                'act_mode' => 'insert_package',
                'Param1' => $this->input->post('uid'),
                'Param2' => $timevalue,
                'Param3' => $timeallow[$i],
                'Param4' => $userallow[$i],
                'Param5' => '',
                'Param6' => '',
                'Param7' => '',
                'Param8' => '',
                'Param9' =>'',
                'Param10' =>'',
                'Param11' =>'',
                'Param12' =>'',
                'Param13' =>'',
                'Param14' =>'',
                'Param15' =>'',
                'Param16' =>'',
                'Param17' =>'',
                'Param18' =>'',
                'Param19' =>'',
                'Param20' =>'',
                );  
              $response['vieww'] = $this->supper_admin->call_procedure('proc_promocode_package', $parameter);
               } 
              $i++;
              }
             }
        }


          redirect("admin/promocodes/s_viewpromo?empid=".$_GET['empid']."&uid=".str_replace(".html","",$_GET['uid'])."");


        }

    }

    public function checkPHPVersion() { 
  


echo 'My username is ' .$_ENV["USER"] . '!';exit;


  }

    /*add promo code by zzz*/
       public function addpromo()
    {


        if($_POST['addones']!='') {
            $addonedata=implode(',', $_POST['addones']);
            $addoneqty=1;
            $promo=$this->input->post('promo');
            $promonumber=$this->input->post('promonumber');
        }
        else
        {
            $addonedata="";
            $addoneqty="";
            $promo=$this->input->post('promo');
            $promonumber=$this->input->post('promonumber');
        }


       
        $this->form_validation->set_rules('branchids', 'branchids', 'required');
        $this->form_validation->set_rules('promocode_name', 'promocode_name', 'required');

        if ($this->form_validation->run() == FALSE) {
          redirect('admin/promocodes/s_viewpromo');
        } else {
            
            foreach ($this->input->post('branchids') as $a) {
                 
            $parameter = array(
                'act_mode' => 'insert_promo',
                'Param1' => $a,
                'Param2' => $this->input->post('promocode_name'),
                'Param3' => $promo,
                'Param4' => $promonumber,
                'Param5' => $this->input->post('promocode_allowed_user'),
                'Param6' => $this->input->post('promocode_allowed_times'),
                'Param7' => $this->input->post('promocode_start'),
                'Param8' => $this->input->post('promocode_end'),
                'Param9' =>"".$this->input->post('user_type')."",
                'Param10' =>$addonedata,
                'Param11' =>$addoneqty,
                'Param12' =>$this->input->post('newuser'),
                'Param13' =>'',
                'Param14' =>'',
                'Param15' =>'',
                'Param16' =>'',
                'Param17' =>'',
                'Param18' =>'',
                'Param19' =>'',
                'Param20' =>''

                ); 
             $response['vieww'] = $this->supper_admin->call_procedure('proc_promocode_package', $parameter);
            // p($response['vieww']);exit;
            $promoid =$response['vieww'][0]->ids;
               $timeallow= $this->input->post('timeallow');
               $userallow= $this->input->post('userallow');
               $timeid= $this->input->post('dateid'); 
              $i=0;
              foreach($timeid as $timevalue){
                if(!empty($timeallow[$i]) && !empty($userallow[$i])){
              $parameter = array(
                'act_mode' => 'insert_package',
                'Param1' => $promoid,
                'Param2' => $timevalue,
                'Param3' => $timeallow[$i],
                'Param4' => $userallow[$i],
                'Param5' => '',
                'Param6' => '',
                'Param7' => '',
                'Param8' => '',
                'Param9' =>'',
                'Param10' =>'',
                'Param11' =>'',
                'Param12' =>'',
                'Param13' =>'',
                'Param14' =>'',
                'Param15' =>'',
                'Param16' =>'',
                'Param17' =>'',
                'Param18' =>'',
                'Param19' =>'',
                'Param20' =>'',
                ); 
              $response['vieww'] = $this->supper_admin->call_procedure('proc_promocode_package', $parameter);
             } 
              $i++;
          }
  
        }


            redirect("admin/promocodes/s_viewpromo?empid=".$_GET['empid']."&uid=".str_replace(".html","",$_GET['uid'])."");


        }

    }

    /*promocode form add*/
    public function s_promo()
    {

if(getMemberId()==1)
{

}
else
{
  redirect('admin/login/dashboard?empid='.$_GET['empid'].'&uid='.$_GET['uid'].'');
}
$parameter2 = array( 'act_mode'=>'s_viewbranch',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');

        $response['s_viewbranch'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter2);

        $parameter2 = array( 'act_mode'=>'s_viewaddon_admin',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'',
           );
        $response['s_viewaddone'] = $this->supper_admin->call_procedure('proc_addon_s',$parameter2);
//p($response['s_viewaddone']);

        $parameter = array(
            'act_mode' => 'viewcountry',
            'row_id' => '',
            'counname' => '',
            'coucode' => '',
            'commid' => '');

        $response['vieww'] = $this->supper_admin->call_procedure('proc_geographic', $parameter);

        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('promocode/s_promo', $response);


    }


    /*view promocode by zzz*/
    public function s_viewpromo()
    {
if(getMemberId()==1)

{

}
else
{
  redirect('admin/login/dashboard?empid='.$_GET['empid'].'&uid='.$_GET['uid'].'');
}
  

        $parameter = array('act_mode' => 'view_promo',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' =>'',
            'Param11' =>'',
            'Param12' =>'');
        //pend($parameter);
        $response['vieww'] = $this->supper_admin->call_procedure('proc_promocode_s', $parameter);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('promocode/s_viewpromo', $response);
    }

    /*delete the promocode*/
    public function promo_delete()
    {
        $parameter = array('act_mode' => 'delete_promo',
            'Param1' => $this->uri->segment('4'),
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' =>'',
            'Param11' =>'',
            'Param12' =>'');
        //pend($parameter);
        $response['vieww'] = $this->supper_admin->call_procedure('proc_promocode_s', $parameter);

        redirect("admin/promocodes/s_viewpromo?empid=".$_GET['empid']."&uid=".str_replace(".html","",$_GET['uid'])."");


    }


    public function promo_status($a,$b)
    {
        $status =  base64_decode($b)== 1 ? 0 : 1;
        $parameter1 = array('act_mode' => 'status_promo',
            'Param1' => base64_decode($a),
            'Param2' => $status,
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' =>'',
            'Param11' =>'',
            'Param12' =>'');
        //pend($parameter1);
        $response['vieww'] = $this->supper_admin->call_procedure('proc_promocode_s', $parameter1);

        redirect("admin/promocodes/s_viewpromo?empid=".$_GET['empid']."&uid=".str_replace(".html","",$_GET['uid'])."");

    }


    /*-----------------24-17-2018------------------------*/

    public function promopakage()
    {   
        $startDate = $this->input->post('startdateid');
        $endDate = $this->input->post('enddateid');
        $parameter1 = array('act_mode' => 'getpacagedata',
            'Param1' => '',
            'Param2' => $startDate,
            'Param3' => $endDate,
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' =>'',
            'Param11' =>'',
            'Param12' =>'');

        $response['vieww'] = $this->supper_admin->call_procedure('proc_promocode_s', $parameter1);
     //  p($parameter1);exit;
        
        $var = '';
        foreach($response['vieww'] as $getvalue){
         
        $parameter1 = array('act_mode' => 'getpacagedata_time',
        'Param1' => '',
        'Param2' => $getvalue->dailyinventory_date,
        'Param3' => '',
        'Param4' => '',
        'Param5' => '',
        'Param6' => '',
        'Param7' => '',
        'Param8' => '',
        'Param9' => '',
        'Param10' =>'',
        'Param11' =>'',
        'Param12' =>'');
        $gettime = $this->supper_admin->call_procedure('proc_promocode_s', $parameter1);
         $times ='';
         $listData =array();
       
        foreach($gettime as $time){
             $parameterPa = array('act_mode' => 'getpacagedata_pakage',
            'Param1' => '',
            'Param2' => '',
            'Param3' => $time->dailyinventory_id,
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' =>'',
            'Param11' =>'',
            'Param12' =>'');

        $getpakage = $this->supper_admin->call_procedure('proc_promocode_s', $parameterPa);
        $pakageList= array();
        foreach($getpakage as $pakages){ 
            $lists['package_id'] =$pakages->dailyinventorypackages_packagesid;
            $lists['package_name'] =$pakages->package_name;
            $pakageList[] =$lists;
          }
           $list['dailyinventory_to'] =$time->dailyinventory_to;
           $list['dailyinventory_from'] =$time->dailyinventory_from;
            $list['dailyinventory_minfrom'] =$time->dailyinventory_minfrom;
            $list['dailyinventory_minto'] =$time->dailyinventory_minto;
           $list['dailyinventory_id'] =$time->dailyinventory_id;  
           $list['package'] =$pakageList; 
           $listData[] =$list;
      }
         
         $var .='<tr>
                     <td>'.$getvalue->dailyinventory_date.'</td>
                                <td colspan="4">
                                <div class="inner-table">
                                <div class="row-line">';

                                $i=0;
                             foreach($listData as $time){
                                $checked='';
                              if($i==0){
                              $checked = '<input  type="checkbox" style="visibility: hidden" maxlength="4" name="checkedall" size="4" id="checkedall" onblur="" />';
                              }
                            $var .='<div class="couting"><span class="date">'.$time['dailyinventory_from'].':'.$time['dailyinventory_minfrom'].'-'.$time['dailyinventory_to'].':'.$time['dailyinventory_minto'].'</span>';
                            $var .= '<span class="slect-p">
                                  <input type="hidden" value='.$time['dailyinventory_id'].' name="dateid[]" id="frometime">
                                  <span>
                                 <input  type="text" maxlength="4" class="timeallow" name="timeallow[]" size="4" id="timeallow" onblur="" />
                                </span>
                               </span>
                               <span class="slect-p">
                               <span><input  type="text" maxlength="4"  class="userallow" name="userallow[]" size="4" id="userallow" onblur="" />
                              </span>
                             </span>
                            <span class="slect-p">
                               <span>'.$checked.'
                            </span>
                             </span>
                         </div>
                        </div>';
                                $i++;  }
                              $var .='</div>
                               
                         </div>
                  </tr>';
      }
     echo $var; 
    }


    public function checkdate(){
          p($_REQUEST);exit;
    }

}// end class
?>
