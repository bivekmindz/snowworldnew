<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Companys extends MX_Controller
{
    public function __construct()
    {
        $this->load->model("supper_admin");
        $this->load->helper('my_helper');
 $this->load->library('session');
 $this->load->helper('adminmenu_helper');
    }

    /*ADD,VIEW,UPDATE COMPANY BY zzz*/
    public function s_addcompany()
    {
if(getMemberId()==1)

{

}
else
{
  redirect('admin/login/dashboard?empid='.$_GET['empid'].'&uid='.$_GET['uid'].'');
}


        if ($this->input->post('submit')) {

            $this->form_validation->set_rules('com_name', 'name', 'required');
            if ($this->form_validation->run() != FALSE) {
                $parameter = array('act_mode' => 's_addcompany',
                    'Param1' => $this->input->post('com_name'),
                    'Param2' => '',
                    'Param3' => '',
                    'Param4' => '',
                    'Param5' => '',
                    'Param6' => '',
                    'Param7' => '',
                    'Param8' => '',
                    'Param9' => '');
                //pend($parameter);
                $response = $this->supper_admin->call_procedure('proc_country_s', $parameter);
                $this->session->set_flashdata('message', 'inserted sucessfully');
            }
        }

        if ($this->input->post('submit_update'))
        {

            $this->form_validation->set_rules('com_name_update', 'name', 'required');
            if ($this->form_validation->run() != FALSE)
            {


                $parameter = array('act_mode' => 's_updatecompany',
                    'Param1' => $this->input->post('com_name_update'),
                    'Param2' => $this->input->post('com_id'),
                    'Param3' => '',
                    'Param4' => '',
                    'Param5' => '',
                    'Param6' => '',
                    'Param7' => '',
                    'Param8' => '',
                    'Param9' => '');
                //p($parameter);
                $response = $this->supper_admin->call_procedure('proc_country_s', $parameter);
                $this->session->set_flashdata('message', 'Updated sucessfully');
            }
        }
        $parameter1 = array('act_mode' => 's_viewcompany',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //pend($parameter);
        $response['vieww'] = $this->supper_admin->call_procedure('proc_country_s', $parameter1);
        //pend($response['vieww']);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('helper/mastermanagehead');
        $this->load->view('company/addcompany', $response);

    }

    /*DELETE COMPANY BY zzz*/
    public function company_delete()
    {
        $parameter = array('act_mode' => 's_deletetcompany',
            'Param1' => $this->uri->segment('4'),
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //pend($parameter);
        $response['vieww'] = $this->supper_admin->call_procedure('proc_country_s', $parameter);
      redirect("admin/companys/s_addCompany?empid=".$_GET['empid']."&uid=".str_replace(".html","",$_GET['uid'])."");


        //redirect(base_url() . 'admin/companys/s_addcompany?empid='.$_GET["empid"]."&uid=".str_replace(".html","",$_GET["uid\'])."');

    }

    public function companystatus($a,$b)
    {
        $status= base64_decode($b)==1 ? 0 : 1;
        //pend(base64_decode($b));
        $parameter1 = array(
            'act_mode' => 'change_comp_status',
            'Param1' => base64_decode($a),
            'Param2' => $status,
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
      //pend($parameter1);
        $response['vieww'] = $this->supper_admin->call_procedure('proc_country_s', $parameter1);
        redirect("admin/companys/s_addCompany?empid=".$_GET['empid']."&uid=".str_replace(".html","",$_GET['uid'])."");

    }

}// end class
?>