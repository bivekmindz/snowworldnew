
<style>
    canvas{
        -moz-user-select: none;
        -webkit-user-select: none;
        -ms-user-select: none;
    }
    .box12{ width: 100%; float: left; margin: 20px 0 10px 0; }
    .box12 input[type="text"]{
           margin: 0 5px 0 0;padding: 10px 3px;
    width: 40%;
    }
    .box12 input[type="submit"]{
        border:none;
        background: #1B78C7;
        color: #fff;
        font-size: 14px;
        padding: 10px 15px;
    }
</style>
<script src="<?php echo base_url(); ?>js/Chart.bundle.js"></script>
<script src="<?php echo base_url(); ?>js/utils.js"></script>
<!-- <div class="wrapper"> -->
<div class="col-lg-10">
    <div class="row">
        <div class="page_contant">
            <div class="col-lg-12 col-xs-12">
                <div class="page_name">
                    <h2>Dashboard</h2>
                </div>
                <div class="all-invoeic">
                    <div class="col-md-12">
                        <div class="left_bar">
                            <div class="col-md-4">
                                <div class="box1">
                                    <p>Total Order</p>
                                    <h1><?php echo $countorder->totalorder; ?></h1>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="box2">
                                    <p>Total Agent :</p>
                                    <h1> <?php echo $countagent->totalagent;  ?></h1>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="box1 box3">
                                    <p> Total Transaction Amount :</p>
                                    <h1> <?php echo number_format($totaltrans->total,2) ?></h1>
                                </div>
                            </div>
                        </div>
                        <div class="left_bar">
                            <div class="col-md-4">
                                <div class="box1 box4">
                                    <p>Total Printed Ticket :</p>
                                    <h1> <?php echo  $totalpticket->totalprint; ?></h1>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="box2 box5">
                                    <p>  Total Users :</p>
                                    <h1><?php echo $totalusers->c; ?></h1>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="box1 box6">
                                    <p>Total Guests :</p>
                                    <h1><?php echo $totalguest->g; ?></h1>
                                </div>
                            </div>
                            </br>
                            <form id="" method="post" action="dashboard">
                             <div class="col-md-6">
                                <div class="box12">
                                   <input type="text" placeholder="From-date" required value ="<?php echo ($_POST['datepicker1']) ? $_POST['datepicker1'] : '' ?>" name="datepicker1" id="datepicker1">
                                   <input type="text" placeholder="To-date" required value ="<?php echo ($_POST['datepicker2']) ? $_POST['datepicker2'] : '' ?>" name="datepicker2" id="datepicker2">
                                   <input type="submit" value ="Search" name="search" id="search">
                                </div>
                            </div>
                            </form>

                        
                            <div style="width:75%; float: left;">
                                <canvas id="canvas"></canvas>
                            </div>
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    var MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var config = {
        type: 'line',
        data: {
            labels: [
                <?php foreach($data as $value){ ?>
                "<?php echo $value->monthname; ?>",
                <?php }
                ?>
            ],
            datasets: [{
                label: "Total Month-Wise Order",
                backgroundColor: window.chartColors.blue,
                borderColor: window.chartColors.blue,
                data: [
                    <?php foreach($data as $key=>$value)
                {
                    echo $value->coun.',';
                }
                    ?>
                ],
                fill: false,
            }]
        },
        options: {
            responsive: true,
            title:{
                display:true,
                // text:'Chart.js Line Chart'
            },
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Month'
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Value'
                    }
                }]
            }
        }
    };
    window.onload = function() {
        var ctx = document.getElementById("canvas").getContext("2d");
        window.myLine = new Chart(ctx, config);
    };
    document.getElementById('randomizeData').addEventListener('click', function() {
        config.data.datasets.forEach(function(dataset) {
            dataset.data = dataset.data.map(function() {
                return randomScalingFactor();
            });
        });
        window.myLine.update();
    });
    var colorNames = Object.keys(window.chartColors);
    document.getElementById('addDataset').addEventListener('click', function() {
        var colorName = colorNames[config.data.datasets.length % colorNames.length];
        var newColor = window.chartColors[colorName];
        var newDataset = {
            label: 'Dataset ' + config.data.datasets.length,
            backgroundColor: newColor,
            borderColor: newColor,
            data: [],
            fill: false
        };
        for (var index = 0; index < config.data.labels.length; ++index) {
            newDataset.data.push(randomScalingFactor());
        }
        config.data.datasets.push(newDataset);
        window.myLine.update();
    });
    document.getElementById('addData').addEventListener('click', function() {
        if (config.data.datasets.length > 0) {
            var month = MONTHS[config.data.labels.length % MONTHS.length];
            config.data.labels.push(month);
            config.data.datasets.forEach(function(dataset) {
                dataset.data.push(randomScalingFactor());
            });
            window.myLine.update();
        }
    });
    document.getElementById('removeDataset').addEventListener('click', function() {
        config.data.datasets.splice(0, 1);
        window.myLine.update();
    });
    document.getElementById('removeData').addEventListener('click', function() {
        config.data.labels.splice(-1, 1); // remove the label first
        config.data.datasets.forEach(function(dataset, datasetIndex) {
            dataset.data.pop();
        });
        window.myLine.update();
    });
</script>

<style>
    .count{
        width: 25%;
        float: left;
        padding: 0 15px;
        box-sizing: border-box;
    }
    .total-order{
        font-size: 18px;
        line-height: 24px;
        color: #393939;
    }
    .total-order{
        padding: 10px 0;
        border-bottom: 1px solid #c1c1c1;
    }
    .total-order span{
        color: #1B78C7;
        font-size: 22px;
        line-height: :24px;
        font-weight: 600;
    }
    .box1{
	width: 100%;
    float: left;
    text-align: right;
    background-color: #1272a5;
    color: white;
    padding:10px;
     box-sizing: border-box;
    }
    .left_bar {
    width: 100%;
    float: left;
    padding: 10px 0px;
}
.box2 {
    width: 100%;
    float: left;
    text-align: right;
    background-color: #278fc5;
    color: white;
    padding:10px;
    box-sizing: border-box;
}
.box3 {
	background-color: #3ca4da !important;
}
.box4 {
	background-color: #49aee2 !important;
}
.box5 {
	background-color: #56b5e6 !important;
}
.box6 {
	background-color: #71c5f1 !important;
}
</style>
 
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
 $(function() {
     $.noConflict();
     $("#datepicker1").datepicker({
         dateFormat: 'dd-mm-yy',
        // minDate: 0,
         maxDate: "-1M "
     });

     $("#datepicker2").datepicker({
         dateFormat: 'dd-mm-yy',
        // minDate: 0,
         //maxDate: "-1M "
     });
 });
</script>