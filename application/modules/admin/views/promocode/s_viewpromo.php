<!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->
<script src="<?php echo admin_url();?>js/moment.js"></script>

<link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"
      rel = "stylesheet">
<script src = "https://code.jquery.com/jquery-1.10.2.js"></script>
<script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script>
    $( function() {
    } );
</script>
<script type="text/javascript">

    function printDiv(divName) {
        
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }

</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"
        crossorigin="anonymous"></script>
<?php //p($vieww); ?>
<!-- jQuery Form Validation code -->
<style>
.btn-primary {
    text-decoration: none;
    margin: 12px;
}
    .input-group-addon {
        position: absolute;
        left: 0;
        width: 100%!important;
        padding: 12px 12px!important;
        font-size: 14px;
        font-weight: 400;
        line-height: 1;
        color: #555;
        text-align: center;
        background-color: transparent!important;
        border: none!important;
        border-radius: 4px;
        top: 0px;
    }
    .input-group-addon  .glyphicon-time{
        float:right;
    }
    input.error {
        border: 1px solid red;
    }

    label.error {
        border: 0px solid red;
        color: red;
        font-weight: normal;
        display: inline;
    }

    /*.tab_ble{ width:100%; float:left;}
    .tab_ble table{ width:100%; float:left;}
    .tab_ble table tr{ width:100%; float:left;}*/
    .tab_ble table tr th {
        /*padding: 5px 35px 15px 35px;*/
        text-align: center;
    }

    .tab_ble table tr td {
        /*border: 1px solid #CCC;*/
        text-align: center;

        /*min-width: 100px;*/
    }
    .lbl_box{
        float:left;
        margin-right:10px;
    }

    @media screen and (min-width: 320px) and (max-width: 680px){

        .lbl_box{ width: 100%;
            float:left;
            margin-right:10px;
        }

    }
    .checkbox_lbl{
        display: none;
        z-index: -9999;
    }
    .checkbox_lbl + label{
        position: relative;
        padding-left:25px;
        font-weight:normal;
        cursor: pointer;
    }
    .checkbox_lbl + label:before{
        position: absolute;
        content: "";
        width:19px;
        height:19px;
        border:solid 1px #abaaaa;
        left:0;
    }
    .checkbox_lbl + label:after{
        position: absolute;
        content: "";
        width: 10px;
        height: 6px;
        border-left: solid 2px #444;
        border-bottom: solid 2px #444;
        left: 5px;
        transform: rotate(-45deg);
        -o-transform: rotate(-45deg);
        -webkit-transform: rotate(-45deg);
        -moz-transform: rotate(-45deg);
        transition: 0.3s;
        -o-transition: 0.3s;
        -webkit-transition: 0.3s;
        -moz-transition: 0.3s;
        top: 5px;
        visibility: hidden;
        opacity: 0;
    }
    .checkbox_lbl:checked + label:after{
        visibility: visible;
        opacity: 1;
    }
   
    .page_box{
        box-shadow: none;
    }
    /* .tbl_overflow{
         float:left;
         width:100%;
         overflow-x: auto;
         padding-bottom: 10px;
     }*/
    #advance_search_d{
        display: none;
        margin-bottom:15px;
        float:left;
        width:100%;
    }

    .advance_btn{
        float:left;
        background: #0f71c3;
        color:#fff;
        padding:10px 15px;
        cursor: pointer;
    }
    .table > thead > tr > th {
        vertical-align: middle!important;

    }

    #container{width:100%;}
    #left{float:left}
    #right{float:right}

    #center{ margin:0 auto;width:100px;float: left;padding: 1px 17px; }
</style>





<!-- <div class="wrapper"> -->

    <div class="col-lg-10">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">

                    <div class="page_name">

                    <h2>VIEW PROMO CODE</h2>
                    </div>
                    <div class="page_box">
                        <div class="sep_box">
    
                            <div class="col-lg-12">
                                <div style="text-align:right;">
<!--                                    <a href="#"><button>CANCEL</button></a>-->
                                </div>
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?>
                                    <?php
                                    if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message');
                                    }
                                    ?>
                                </div>
                            </div>
                      


                        <div class="col-md-12">
                            <div class="tab_ble">
                                <div class="tbl_overflow">
                                    <table id="table1" style="width: 100%" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                        <th>S.NO</th>
                                        <th>PROMO CODE NAME</th>

                                        <th>TYPE</th>
                                        <th>DISCOUNT</th>
                                        <th>WEBURL</th>

                                        <th>PROMOCODE TYPE</th>
                                        <th>START DATE</th>
                                        <th>END DATE</th>
                                        <th>STATUS/DELETE</th>

                                    </tr>
                                        </thead>
                                        <tbody>
                                        <?php //pend($vieww);
                                    $i=1; foreach($vieww as $v) {?>
                                    <tr>
                                        <td><?= $i;?></td>
                                        <td><?php echo $v->p_codename; ?></td>

                                        <td><?php echo $v->p_type; ?></td>
                                        <td><?php echo $v->p_discount; ?>
                                        </td> <td><?php echo base_url()."paccoupon?couponid=".$v->p_codename ?>

                                        </td> <td><?php if($v->p_usertype == 0){echo "All";}
                                        elseif($v->p_usertype == 1){echo "User";}elseif($v->p_usertype == 2){echo "guest";} else {echo "Agent";} ?></td>
                                        </td> <td><?php echo $v->p_start_date; ?></td>
                                        </td> <td><?php echo $v->p_expiry_date; ?></td>


                                        <td style="width:100%;">




  <a href="<?= base_url('admin/promocodes/promo_edit/')."/".$v->promo_id ?>?empid=<?php echo $_GET['empid']; ?>&uid=<?php echo str_replace(".html","",$_GET['uid']); ?>"  class="btn_edit">
                                                <span class="fa fa-pencil"></span>
                                            </a>



                                            <a  href="<?= base_url('admin/promocodes/promo_status/')."/".base64_encode($v->promo_id)."/".base64_encode($v->p_status); ?>?empid=<?php echo $_GET['empid']; ?>&uid=<?php echo str_replace(".html","",$_GET['uid']); ?>" class="btn_status"><?php echo $v->p_status == 0 ? "Inactive": "Active"; ?></a>



                                            <a href="<?= base_url('admin/promocodes/promo_delete/')."/".$v->promo_id ?>?empid=<?php echo $_GET['empid']; ?>&uid=<?php echo str_replace(".html","",$_GET['uid']); ?>"  onclick="return confirm('Are you sure you want to delete this item?');" class="btn_delete">
                                                <span class="glyphicon glyphicon-trash"></span>
                                            </a>


                                        </td>

                                    </tr>

                                    <?php $i++;} ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                    </div>

                </div>
            </div>

        </div>
    </div>
</div>


    
    
<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<!-- Include Date Range Picker -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
<script>
    $(document).ready(function(){
        $(".advance_btn").click(function(){
            $("#advance_search_d").slideToggle();
        });

    })
</script>

<script src="<?php echo admin_url();?>js/bootstrap-datetimepicker.min.js"></script>
<link href="<?php echo admin_url();?>css/bootstrap-datetimepicker.css" rel="stylesheet">

<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<link rel="stylesheet"

      href="https://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css"></style>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
                                                                      <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

                                                                            <script>
                                                                            $(document).ready(function(){

 var table = $('#table1').DataTable({"scrollY": 400,"scrollX": true });
    $.noConflict();
    var date_input=$('input[name="filter_date_booking_from"],input[name="filter_date_booking_to"],input[name="filter_date_printed_from"],input[name="filter_date_printed_to"]'); //our date input has the name "date"
var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
date_input.datepicker({
    format: 'yyyy-mm-dd',
    container: container,
    todayHighlight: true,
    autoclose: true,
});


var date_input=$('input[name="filter_date_session_from"],input[name="filter_date_session_to"],input[name="filter_date_ticket"]'); //our date input has the name "date"
var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
date_input.datepicker({
    format: 'dd-mm-yyyy',
    container: container,
    todayHighlight: true,
    autoclose: true,
 });
});

</script>
<script type="text/javascript">
    function checkname(){
        var name=$('#cityname').val();

        $.ajax({
            url: '<?php echo base_url()?>admin/geographic/checkcityname',
            type: 'POST',
            dataType: 'json',
            data: {'name': name},
            success: function(data){

                if(data[0].count > 0){
                    $('#submitBtn').prop('disabled', 'disabled');
                    alert("Data Already Existed");
                }else{
                    $('#submitBtn').removeAttr('disabled');
                }
            }
        });

    }

    function selectstates(){

        var countryid=$('#countryid').val();
        $.ajax({
            url: '<?php echo base_url()?>admin/geographic/countrystate',
            type: 'POST',
            //dataType: 'json',
            data: {'countryid': countryid},
            success: function(data){
                var  option_brand = '<option value="">Select State</option>';
                $('#stateid').empty();
                $("#stateid").append(option_brand+data);

            }
        });

    }

    function selectcity(){

        var stateid=$('#stateid').val();
        $.ajax({
            url: '<?php echo base_url()?>admin/geographic/statecity',
            type: 'POST',
            //dataType: 'json',
            data: {'stateid': stateid},
            success: function(data){
                var  option_brand = '<option value="">Select City</option>';
                $('#cityid').empty();
                $("#cityid").append(option_brand+data);

            }
        });

    }

    function selectlocation(){
        var cityid = $('#cityid').val();
        $.ajax({
            url: '<?php echo base_url()?>admin/geographic/citylocation',
            type: "POST",
            data: {'cityid': cityid},
            success: function(data){
                var  option_brand = '<option value="">Select Location</option>';
                $('#locationid').empty();
                $("#locationid").append(option_brand+data);

            }

        })

    }
    $(function(){
        $("#flat").click(function () {
            $("#flatpercent").css('display','block');
        });
        $("#percent").click(function () {
            $("#flatpercent").css('display','block');
        })
    })

</script>