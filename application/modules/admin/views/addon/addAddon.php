<!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"
        crossorigin="anonymous"></script>
<?php //p($vieww); ?>
<!-- jQuery Form Validation code -->
<script>
    $(function() {
        // Initialize form validation on the registration form.
        // It has the name attribute "registration"
        $("#add_addon").validate({
            ignore:".ignore, .select2-input",
            // Specify validation rules
            rules: {
                // The key name on the left side is the name attribute
                // of an input field. Validation rules are defined
                // on the right side
                addon_name: "required",
                addon_desc: "required",
                branchids: "required",
                act_ids: "required",
                addon_price: {
                    "required": true,
                    "number": true,
                },
                addon_image : "required",

            },
            // Specify validation error messages
            messages: {
                addon_name: "Please enter name",
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
                var ext = $('#addon_image').val().split('.').pop().toLowerCase();
                if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
                    alert('Please Select Valid Image!');
                    return false;
                }
                else {
                    $("#branchids").attr('disabled',false);
                    form.submit();
                }
            }
        });
    });


</script>
<style>
    input.error{border:1px solid red;}
    label.error{border:0px solid red; color:red; font-weight: normal; display:inline; }
              td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
    border-top: 1px solid #dddddd !important;
    border-bottom: 1px solid #dddddd !important;
}
</style>





<!-- <div class="wrapper"> -->

    <div class="col-lg-10">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">

                        <h2>Add addon</h2>
                    </div>
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="sep_box">
                                <div class="col-lg-12">
                                    <div id="content">
                                       <?php //echo $this->uri->segment(1) .'br';
//echo $this->uri->segment(2) .'br';  class="active"
//echo $this->uri->segment(3) ;
if($this->uri->segment(3)=='addTimeslotPackage')
{  $statusactive1='class="active"';}
if($this->uri->segment(3)=='addTimeslot')
{  $statusactive2='class="active"';}
if($this->uri->segment(3)=='addPackage')
{  $statusactive3='class="active"';}
if($this->uri->segment(3)=='addAddon')
{  $statusactive4='class="active"';}
if($this->uri->segment(3)=='gateway')
{  $statusactive5='class="active"';}
if($this->uri->segment(3)=='smsgateway')
{  $statusactive6='class="active"';}
if($this->uri->segment(3)=='tearmcondtion')
{  $statusactive7='class="active"';}
if($this->uri->segment(3)=='addbranch')
{  $statusactive8='class="active"';}
if($this->uri->segment(3)=='s_addCompany')
{  $statusactive9='class="active"';}
?>

<ul id="tabs" class="nav nav-tabs" data-tabs="tabs">

    <li <?php echo $statusactive1; ?>><a href="<?php echo base_url('admin/timeslotspackages/addTimeslotPackage?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid']));?>" >Manual Session Time</a>
    </li>

    <li <?php echo $statusactive2; ?>><a href="<?php  echo base_url('admin/timeslots/addTimeslot?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid']));?>" >Auto Session Time</a></li>

    <li <?php echo $statusactive3; ?>><a href="<?php echo base_url('admin/packages/addPackage?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid']));?>" >Package</a></li>
    <li <?php echo $statusactive4; ?>><a href="<?php echo  base_url('admin/addon/addAddon?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid'])); ?>" >Addon</a></li>
    <li <?php echo $statusactive5; ?>><a href="<?php echo  base_url('admin/Payment/gateway?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid'])); ?>" >Payment Gateway</a></li>

    <li <?php echo $statusactive6; ?>><a href="<?php echo  base_url('admin/Payment/smsgateway?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid'])); ?>" >Sms Gateway</a></li>

    <li <?php echo $statusactive7; ?>><a href="<?php echo  base_url('admin/Payment/tearmcondtion?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid'])); ?>" >Terms & Conditions</a></li>
    <li <?php echo $statusactive8; ?>><a href="<?php  echo base_url('admin/branchs/addbranch?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid']));?>" >Branch</a></li>


    <li <?php echo $statusactive9; ?>><a href="<?php echo base_url('admin/companys/s_addCompany?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid']));?>" >Company</a></li>






</ul>

                                    </div>
                                    <div class='flashmsg'>
                                        <?php echo validation_errors(); ?>
                                        <?php
                                        if($this->session->flashdata('message')){
                                            echo $this->session->flashdata('message');
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <form  action="<?= base_url('admin/addon/addaddon?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid'])) ?>" id="update_com" method="post" enctype="multipart/form-data" >


                        <form action="<?= base_url('admin/addon/addaddon') ?>" name="add_addon" id="add_addon" method="post" enctype="multipart/form-data" >


                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">addon Name<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" id="addon_name" name="addon_name" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">addon Description<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <textarea  id="addon_desc" name="addon_desc" ></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Select Branch <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">


                                                <?php
                                                if($this->session->userdata('snowworld')->EmployeeId){
                                                    ?>
                                                    <script>
                                                        $(function(){
                                                            $('#branchids option[value="<?= $this->session->userdata('snowworld')->branchid; ?>"]').attr('selected',true);
                                                            $("#branchids").attr('disabled',true);
                                                            //$("#branchids").attr('name',<?= $this->session->userdata('snowworld')->branchid;?>);
                                                        });
                                                    </script>

                                                    <?php
                                                }
                                                ?>


                                                <select name="branchids[]" required multiple id="branchids">
                                                    <?php foreach ($s_viewbranch as $key => $value) { ?>
                                                        <option value="<?php echo $value->branch_id; ?>"><?php echo $value->branch_name; ?></option>
                                                    <?php } ?>
                                                </select></div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">addon Price<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" id="addon_price" name="addon_price" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Addon Image<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="file" id="addon_image" name="addon_image" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>




                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-8">
                                            <div class="submit_tbl">
                                                <input id="submitBtn" type="submit" name="submit" value="Submit" class="btn_button sub_btn" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </form>


                        <div class="col-md-12">
                        <div class="add_comp">
                            <h2>Table</h2>
                            <div class="table-responsive" style="width: 100%;">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>S.NO</th>
                                        <th>NAME</th>
                                        <th>DESCRIPTION</th>
                                        <th>PRICE</th>
                                        <th>BRANCH</th>
                                        <th>IMAGE</th>
                                        <th>DELETE</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php /*p($vieww);*/ $i=1; foreach($vieww_addon as $v) {



                                        if($this->session->userdata('snowworld')->EmployeeId) {
                                            if($this->session->userdata('snowworld')->branchid != $v->addon_branchid)
                                            {
                                                continue;
                                            }
                                        }




                                        ?>
                                        <tr>
                                            <td><?= $i;?></td>
                                            <td id="t_act_name"><?php echo $v->addon_name; ?></td>
                                            <td id="t_act_desc"><?php echo $v->addon_description; ?></td>
                                            <td id="t_act_price"><?php echo $v->addon_price; ?></td>
                                            <td><?php

                                                $parameter = array( 'act_mode'=>'getbranch',
                                                    'Param1'=>$v->addon_branchid,
                                                    'Param2'=>'',
                                                    'Param3'=>'',
                                                    'Param4'=>'',
                                                    'Param5'=>'',
                                                    'Param6'=>'',
                                                    'Param7'=>'',
                                                    'Param8'=>'',
                                                    'Param9'=>'');
                                                //p($parameter);
                                                $path = api_url()."main_snowworld_v/getlocation/format/json/";
                                                $br_name = curlpost($parameter,$path);
                                                //p($br_name);
                                                $arr = (array)$br_name;
                                                //p($arr);
                                                echo "<ul id='branch_map'>";
                                                if($arr[0]['branch_name'] != ''){
                                                    echo("<li value=".$arr[0]['branch_id'].">".$arr[0]['branch_name']."</li>");}
                                                echo "</ul>";
                                                ?></td>



                                            <td id="t_act_image">

                                                <img height="80px;" src="<?= base_url('assets/admin/images')."/".$v->addon_image;?> "
                                                <?php echo $v->addon_image; ?></td>
                                            <td>


                                                <a href="<?= base_url('admin/addon/addonstatus/'.base64_encode($v->addon_id) .'/'.base64_encode($v->addon_status).'?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid']).'') ?>"   class="btn_status"><?php echo $v->addon_status == 0 ? "Inactive": "Active"; ?></a>

                                                <a href="<?= base_url('admin/addon/addonviewupdate/'.base64_encode($v->addon_id) .'?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid']).'') ?>"   class="btn_edit">
                                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                                </a>

                                                <a href="<?= base_url('admin/addon/addondelete/'.$v->addon_id .'?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid']).'') ?>"  onclick="return confirm('Are you sure you want to delete this item?');" class="btn_delete">


                                                    <span class="glyphicon glyphicon-trash"></span>
                                                </a>







                                            </td>

                                        </tr>

                                        <?php $i++;} ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                          </div>




                    </div>

                </div>
            </div>
        </div>
    </div>
</div>




<div class="container">
    <!-- Modal -->
    <div class="modal fade" id="myModal1" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Update addon</h4>
                </div>
                <div class="modal-body">
                    <form action="<?= base_url('admin/addons/updateaddon') ?>" name="update_addon" id="update_addon" method="post" enctype="multipart/form-data" >


                        <div class="sep_box">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Update addon Name<span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                            <input type="text" id="update_addon_name" name="update_addon_name" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="sep_box">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Update addon Description<span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                            <textarea  id="update_addon_desc" name="update_addon_desc" ></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="sep_box">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Select Branch <span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                            <select name="update_branchids[]" multiple id="update_branchids">
                                                <?php foreach ($s_viewbranch as $key => $value) { ?>
                                                    <option value="<?php echo $value->branch_id; ?>"><?php echo $value->branch_name; ?></option>
                                                <?php } ?>
                                            </select></div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="sep_box">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Select Activity <span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                            <select name="update_act_ids[]" multiple id="update_act_ids">
                                                <?php foreach ($vieww_act as $key => $value) { ?>
                                                    <option value="<?php echo $value->activity_id; ?>"><?php echo $value->activity_name; ?></option>
                                                <?php } ?>
                                            </select></div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="sep_box">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Update addon Price<span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                            <input type="text" id="update_addon_price" name="update_addon_price" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="sep_box">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-8">
                                        <div class="submit_tbl">
                                            <input id="submitBtn" type="submit" name="submit" value="Submit" class="btn_button sub_btn" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                </div>
            </div>

        </div>
    </div>

</div>

<script>
    $(function () {
        $("#branchids,#act_ids").select2({
            placeholder: "Select the Data",
            allowClear: true
        }).select2('val', $('.select2 option:eq(0)').val());

        $("#updateaddonButton").on('click',function () {

            $("#update_act_ids,#update_branchids").select2({
                placeholder: "Select the Data",
                allowClear: true
            });



            /*var defaultData = [{id:1, text:'Item1'},{id:2,text:'Item2'},{id:3,text:'Item3'}];
             $('#update_act_ids').data().select2.updateSelection(defaultData);
             var a = $(this).parent('tr').find('ul [id=branch_map]').html('');
             console.log(a);*/

        });


        $("#act_ids").on('change',function () {

            var act =$(this).val().toString();
            var finalPrice = 0;
            var act_split = act.split(',');
            for(var count = 0 ; count < act_split.length ; count++)
            {
                $.ajax({
                    url: '<?php echo base_url()?>admin/addons/activityPrice',
                    type: 'POST',
                    dataType : "json",
                    data: {'act_id': act_split[count]},
                    success: function(data){
                        if(data.length > 0) {
                            finalPrice = finalPrice +  parseInt(data[0].activity_price);
                        }
                        $("#addon_price").val(finalPrice);
                    }
                });
            }

            //console.log(act_split.length);
            return false;

        })
    })







</script>