<!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"
crossorigin="anonymous"></script>
<?php //p($vieww); ?>
<!-- jQuery Form Validation code -->
<script>
$(function() {
// Initialize form validation on the registration form.
// It has the name attribute "registration"
$("#add_package").validate({
ignore:".ignore, .select2-input",
// Specify validation rules
rules: {
// The key name on the left side is the name attribute
// of an input field. Validation rules are defined
// on the right side
package_name: "required",
package_desc: "required",
branchids: "required",
act_ids: "required",
package_price: {
"required": true,
"number": true
},
},
// Specify validation error messages
messages: {
package_name: "Please enter name",
},
// Make sure the form is submitted to the destination defined
// in the "action" attribute of the form when valid
submitHandler: function(form) {
$("#branchids").attr('disabled',false);
form.submit();
}
});
});
</script>
<style>
input.error{border:1px solid red;}
label.error{border:0px solid red; color:red; font-weight: normal; display:inline; }
.btne_active{    text-decoration: none;
background-color: #c7921b;
padding: 6px 5px 9px 6px;
color: #fff;
border-radius: 5px;}
td, th {
border: 1px solid #dddddd;
text-align: left;
padding: 8px;
border-top: 1px solid #dddddd !important;
border-bottom: 1px solid #dddddd !important;
}
.btn-primary {
text-decoration: none;
margin: 2px;
}
</style>
<!-- <div class="wrapper"> -->
<div class="col-lg-10">
    <div class="row">
        <div class="page_contant">
            <div class="col-lg-12">
                <div class="page_name">
                    <h2>Add Package</h2>
                </div>
                <div class="page_box">
                    <div class="sep_box">
                        <div class="sep_box">
                            <div class="col-lg-12">
                                <div id="content">
                                    <?php //echo $this->uri->segment(1) .'br';
                                    //echo $this->uri->segment(2) .'br';  class="active"
                                    //echo $this->uri->segment(3) ;
                                    if($this->uri->segment(3)=='addTimeslotPackage')
                                    {  $statusactive1='class="active"';}
                                    if($this->uri->segment(3)=='addTimeslot')
                                    {  $statusactive2='class="active"';}
                                    if($this->uri->segment(3)=='addPackage')
                                    {  $statusactive3='class="active"';}
                                    if($this->uri->segment(3)=='addAddon')
                                    {  $statusactive4='class="active"';}
                                    if($this->uri->segment(3)=='gateway')
                                    {  $statusactive5='class="active"';}
                                    if($this->uri->segment(3)=='smsgateway')
                                    {  $statusactive6='class="active"';}
                                    if($this->uri->segment(3)=='tearmcondtion')
                                    {  $statusactive7='class="active"';}
                                    if($this->uri->segment(3)=='addbranch')
                                    {  $statusactive8='class="active"';}
                                    if($this->uri->segment(3)=='s_addCompany')
                                    {  $statusactive9='class="active"';}
                                    ?>
                                    <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
                                        <li <?php echo $statusactive1; ?>><a href="<?php echo base_url('admin/timeslotspackages/addTimeslotPackage?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid']));?>" >Manual Session Time</a>
                                    </li>
                                    <li <?php echo $statusactive2; ?>><a href="<?php  echo base_url('admin/timeslots/addTimeslot?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid']));?>" >Auto Session Time</a></li>
                                    <li <?php echo $statusactive3; ?>><a href="<?php echo base_url('admin/packages/addPackage?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid']));?>" >Package</a></li>
                                    <li <?php echo $statusactive4; ?>><a href="<?php echo  base_url('admin/addon/addAddon?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid'])); ?>" >Addon</a></li>
                                    <li <?php echo $statusactive5; ?>><a href="<?php echo  base_url('admin/Payment/gateway?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid'])); ?>" >Payment Gateway</a></li>
                                    <li <?php echo $statusactive6; ?>><a href="<?php echo  base_url('admin/Payment/smsgateway?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid'])); ?>" >Sms Gateway</a></li>
                                    <li <?php echo $statusactive7; ?>><a href="<?php echo  base_url('admin/Payment/tearmcondtion?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid'])); ?>" >Terms & Conditions</a></li>
                                    <li <?php echo $statusactive8; ?>><a href="<?php  echo base_url('admin/branchs/addbranch?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid']));?>" >Branch</a></li>
                                    <li <?php echo $statusactive9; ?>><a href="<?php echo base_url('admin/companys/s_addCompany?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid']));?>" >Company</a></li>
                                </ul>
                            </div>
                            <div class='flashmsg'>
                                <?php echo validation_errors(); ?>
                                <?php
                                if($this->session->flashdata('message')){
                                echo $this->session->flashdata('message');
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <form action="<?= base_url('admin/packages/addPackage?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid'])) ?>" name="add_package" id="add_package" method="post" enctype="multipart/form-data" >
                    <div class="sep_box">
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="tbl_text">Package Name<span style="color:red;font-weight: bold;">*</span></div>
                                </div>
                                <div class="col-lg-8">
                                    <div class="tbl_input">
                                        <input type="text" id="package_name" name="package_name" >
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="sep_box">
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="tbl_text">Package Description<span style="color:red;font-weight: bold;">*</span></div>
                                </div>
                                <div class="col-lg-8">
                                    <div class="tbl_input">
                                        <textarea  id="package_desc" name="package_desc" ></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="sep_box">
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="tbl_text">Select Branch <span style="color:red;font-weight: bold;">*</span></div>
                                </div>
                                <div class="col-lg-8">
                                    <div class="tbl_input">
                                        <?php
                                        if($this->session->userdata('snowworld')->EmployeeId){
                                        ?>
                                        <script>
                                        $(function(){
                                        $('#branchids option[value="<?= $this->session->userdata('snowworld')->branchid; ?>"]').attr('selected',true);
                                        $("#branchids").attr('disabled',true);
                                        //$("#branchids").attr('name',<?= $this->session->userdata('snowworld')->branchid;?>);
                                        });
                                        </script>
                                        <?php } ?>
                                        <select name="branchids[]" required multiple id="branchids">
                                            <?php foreach ($s_viewbranch as $key => $value) { ?>
                                            <option value="<?php echo $value->branch_id; ?>"><?php echo $value->branch_name; ?></option>
                                            <?php } ?>
                                        </select></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Select Activity <span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                            <select disabled="disabled" name="act_ids[]" required multiple id="act_ids">
                                                <option selected="selected" value="1">ALL ACTIVITIES</option>
                                                <?php foreach ($vieww_act as $key => $value) { ?>
                                                <option value="<?php echo $value->activity_id; ?>"><?php echo $value->activity_name; ?></option>
                                                <?php } ?>
                                            </select></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Package Price<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" id="package_price" name="package_price" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="sep_box" >
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Package For<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <select name="package_for" id="">
                                                    <option value="0">Users/Guests</option>
                                                    <option value="1">Partners</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Package Image<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="file" id="pack_image" name="pack_image" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-8">
                                            <div class="submit_tbl">
                                                <input id="submitBtn" type="submit" name="submit" value="Submit" class="btn_button sub_btn" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="col-md-12">
                            <div class="add_comp">
                                <h2>Table</h2>
                                <div class="table-responsive" style="width: 100%;">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>S.NO</th>
                                                <th>Package Name</th>
                                                <th width="200">Package Description</th>
                                                <th>Package Branch</th>
                                                <th>Package Price</th>
                                                <th>Package Images</th>
                                                <th>EDIT/DELETE</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i=1; foreach($vieww_pack as $v) {
                                            $exp_bp =  explode(',',$v->bp_id);
                                            $i_count_s=1;
                                            foreach($exp_bp as $bp_id) {
                                            if ($this->session->userdata('snowworld')->EmployeeId) {
                                            if ($this->session->userdata('snowworld')->branchid != $bp_id) {
                                            continue;
                                            }
                                            }
                                            }
                                            ?>
                                            <tr>
                                                <td><?= $i;?></td>
                                                <td id="u_pack_name"><?php echo $v->package_name; ?></td>
                                                <td id="u_pack_desc"><?php echo $v->package_description; ?></td>
                                                <td id="u_pack_price"><?php
                                                    //echo $v->bp_id;
                                                    $exp_bp =  explode(',',$v->bp_id);
                                                    $i_count_s=1;
                                                    foreach($exp_bp as $bp_id)
                                                    {
                                                    $parameter = array( 'act_mode'=>'getbranch',
                                                    'Param1'=>$bp_id,
                                                    'Param2'=>'',
                                                    'Param3'=>'',
                                                    'Param4'=>'',
                                                    'Param5'=>'',
                                                    'Param6'=>'',
                                                    'Param7'=>'',
                                                    'Param8'=>'',
                                                    'Param9'=>'');
                                                    //p($parameter);
                                                    $path = api_url()."main_snowworld_v/getlocation/format/json/";
                                                    $br_name = curlpost($parameter,$path);
                                                    //p($br_name);
                                                    $arr = (array)$br_name;
                                                    //p($arr);
                                                    echo "<ul id='branch_map' style='list-style-type:none;'>";
                                                        if($arr[0]['branch_name'] != ''){
                                                        echo("<li value=".$arr[0]['branch_id']."><strong>".$i_count_s.".  </strong>".$arr[0]['branch_name']."</li>");}
                                                    echo "</ul>";
                                                    $i_count_s++;
                                                    }
                                                ?></td>
                                                <!--   <td id="t_act_price"><?php
                                                    /*                                                $exp_ap =  explode(',',$v->ap_id);
                                                    foreach($exp_ap as $ap_id)
                                                    {
                                                    //echo $ap_id;
                                                    $parameter1 = array( 'act_mode'=>'getactivity',
                                                    'Param1'=>$ap_id,
                                                    'Param2'=>'',
                                                    'Param3'=>'',
                                                    'Param4'=>'',
                                                    'Param5'=>'',
                                                    'Param6'=>'',
                                                    'Param7'=>'',
                                                    'Param8'=>'',
                                                    'Param9'=>'');
                                                    //p($parameter);
                                                    $path1 = api_url()."main_snowworld_v/getlocation/format/json/";
                                                    $act_name = curlpost($parameter1,$path1);
                                                    //p($br_name);
                                                    $arr1 = (array)$act_name;
                                                    echo "<ul id='activity_map'>";
                                                        if($arr1[0]['activity_name'] != ''){
                                                        echo("<li value=".$arr1[0]['activity_id'].">".$arr1[0]['activity_name']."</li>");}
                                                    echo "</ul>";
                                                    }
                                                */?></td>-->
                                                <td id="t_act_price"><?php echo $v->package_price; ?></td>
                                                <td>  <img height="100px;" src="<?= base_url('assets/admin/images')."/".$v->package_image;?> "
                                                <?php echo $v->package_image; ?></td></td>
                                                <td>
                                                    <a href="<?= base_url('admin/packages/packagestatus/'.base64_encode($v->package_id) .'/'.base64_encode($v->package_status).'?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid']).'') ?>"  class="btn_status"><?php echo $v->package_status == 0 ? "Inactive": "Active"; ?>
                                                    </a>
                                                    <a href="<?= base_url('admin/packages/packageviewupdate/'.base64_encode($v->package_id) .'?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid']).'') ?>"   class="btn_edit">
                                                        <i class="fa fa-pencil" aria-hidden="true"></i>
                                                    </a>
                                                    <a href="<?= base_url('admin/packages/packageDelete1/'.$v->package_id .'?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid']).'') ?>"  onclick="return confirm('Are you sure you want to delete this item?');" class="btn_delete">
                                                        <span class="glyphicon glyphicon-trash"></span>
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php $i++;} ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <!-- Modal -->
    <div class="modal fade" id="myModal1" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Update Package</h4>
                </div>
                <div class="modal-body">
                    <form action="<?= base_url('admin/packages/updatePackage') ?>" name="update_package" id="update_package" method="post" enctype="multipart/form-data" >
                        <div class="sep_box">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Update Package Name<span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                            <input type="text" id="update_package_name" name="update_package_name" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="sep_box">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Update Package Description<span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                            <textarea  id="update_package_desc" name="update_package_desc" ></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="sep_box">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Select Branch <span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <?php
                                                                                //echo "<pre>";
                                                                                    //print_r($s_viewbranch);
                                            ?>
                                                                                    <div class="tbl_input">
                                                                                            <select name="update_branchids[]" multiple id="update_branchids">
                                                    <?php foreach ($s_viewbranch as $key => $value) { ?>
                                                    <option value="<?php echo $value->branch_id; ?>"><?php echo $value->branch_name; ?></option>
                                                    <?php } ?>
                                                                                            </select></div>
                                                                                </div>
                                                                        </div>
                                                                </div>
                                                        </div>
                                                        <div class="sep_box" >
                                                                <div class="col-lg-6">
                                                                        <div class="row">
                                                                                <div class="col-lg-4">
                                                                                        <div class="tbl_text">Package For<span style="color:red;font-weight: bold;">*</span></div>
                                                                                </div>
                                                                                <div class="col-lg-8">
                                                                                        <div class="tbl_input">
                                                                                                <select name="update_package_for" id="update_package_for">
                                                                                                        <option value="0">Users/Guests</option>
                                                                                                        <option value="1">Partners</option>
                                                                                                </select>
                                                                                        </div>
                                                                                </div>
                                                                        </div>
                                                                </div>
                                                        </div>
                                                        <div class="sep_box">
                                                                <div class="col-lg-12">
                                                                        <div class="row">
                                                                                <div class="col-lg-4">
                                                                                        <div class="tbl_text">Update Package Price<span style="color:red;font-weight: bold;">*</span></div>
                                                                                </div>
                                                                                <div class="col-lg-8">
                                                                                        <div class="tbl_input">
                                                                                                <input type="text" id="update_package_price" name="update_package_price" >
                                                                                                <input type="hidden" id="pack_update_hidden_id" name="pack_update_hidden_id">
                                                                                        </div>
                                                                                </div>
                                                                        </div>
                                                                </div>
                                                        </div>
                                                        <div class="sep_box">
                                                                <div class="col-lg-12">
                                                                        <div class="row">
                                                                                <div class="col-lg-4"></div>
                                                                                <div class="col-lg-8">
                                                                                        <div class="submit_tbl">
                                                                                                <input id="submitBtn" type="submit" name="submit" value="Submit" class="btn_button sub_btn" />
                                                                                        </div>
                                                                                </div>
                                                                        </div>
                                                                </div>
                                                        </div>
                                                </form>
                                        </div>
                                        <div class="modal-footer">
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
        <script>
            function func_update(dataid) {
                $('#update_branchids option').attr('selected',false);
                //alert(dataid);
                $.ajax({
        url: '<?php echo base_url()?>admin/packages/update_package_data',
                    type: 'POST',
                    dataType : "json",
                    data: {'pack_id':dataid},
                    success: function(data){
                        // alert(data['0'].package_name + data['0'].package_id);
                        $("#update_package_name").val(data['0'].package_name);
                        $("#update_package_desc").val(data['0'].package_description);
                        $("#update_package_price").val(data['0'].package_price);
                        $("#pack_update_hidden_id").val(data['0'].package_id);
                        $('#update_package_for option["value="'+ data['0'].package_for +"']').attr('selected',true);
                        $('.selDiv [value="SEL1"]')
                        var mystr = data['0'].bp_id;
                        var myarr = mystr.split(",");
                        for(var i = 0 ; i < myarr.length; i++)
                        {
                            //alert(myarr[i]);
                            $('#update_branchids option[value="'+myarr[i]+'"]').attr('selected',true);
                        }
                        $("#update_branchids").select2({
                            placeholder: "Select the Data"
                        });
                        //$("#update_package_name").val(data['0'].package_name);
                        return false;
                    }
                });
                //console.log(act_split.length);
                return false;
            }
            $(function () {
                $("#branchids,#act_ids").select2({
                    placeholder: "Select the Data"
                });
            });
        </script>