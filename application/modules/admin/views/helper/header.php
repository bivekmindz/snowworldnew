<?php
//p($this->session->userdata);
if($this->session->userdata('snowworldadmin')->LoginID == "")
{
// pend($this->session->userdata);
redirect(base_url('admin/login'));
}
//pend($this->session->userdata('snowworld')); ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel='shortcut icon' href="<?php echo base_url();?>assets/fevicon.png"/>
    <link href="<?php echo admin_url();?>bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo admin_url();?>fontawsome/css/font-awesome.css" rel="stylesheet" />
    <link href="<?php echo admin_url();?>css/StyleSheet.css" rel="stylesheet" />
    <script type="text/javascript">
    var site_url="<?php echo base_url(); ?>";
    </script>
   
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="<?php echo admin_url();?>js/JavaScript.js"></script>
    <!-- for select2 js -->
    <link href="<?php echo base_url(); ?>assets/js/select/dist/css/select2.min.css" rel="stylesheet" />
    <script src="<?php echo base_url(); ?>assets/js/select/dist/js/select2.min.js"></script>
    <!-- for select2 js -->
    <!-- datepicker -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <!--  <link rel="stylesheet" href="/resources/demos/style.css"> -->
    <!-- datepicker -->
    <script type="text/javascript">
    $(document).ready(function () {
    	
			$('.menu-sm').click(function(){
				$(".left_nav").toggleClass("active_mm");
			});
		
        });
    </script>
    <!-- Favicons-->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>img/favicon.png" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="<?php echo base_url(); ?>img/apple-touch-icon-57x57-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="<?php echo base_url(); ?>img/apple-touch-icon-72x72-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="<?php echo base_url(); ?>img/apple-touch-icon-114x114-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="<?php echo base_url(); ?>img/apple-touch-icon-144x144-precomposed.png">
  </head>
  <body>
    <div class="wrapper">
      <div class="col-lg-12">
        <div class="row">
          <div class="top_header">
            <div class="col-lg-4">
              	<span class="menu-sm hidden-lg"> <i class="fa fa-align-justify menu-sm-b"></i></span>
              		<!-- <a href="<?php echo  base_url();?>admin/login/dashboard" class="logo_admin"> skiindia 445 </a> -->
					<div class="logo"> <a href=""> <img src="http://192.168.1.65/snowworldnew/assets/agent/images/logo.png" width="57" alt=""> </a> </div>
              
            </div>
            <div class="col-lg-8">
              
              <!--  <a class="user" href="<?php echo base_url();?>admin/login/logout" style="color: #444;font-size: 13px;padding: 14px 0px;float: right;text-decoration:none;margin-left:30px;">
                <?php if(isset($this->session->userdata('admin')->UserName)){ echo 'Logout';}?>
              <i class="fa fa-user" style="margin-right:5px;"></i>Logout</a>
              <a href="<?php echo  base_url();?>admin/login/adminupdatepassword?empid=<?php echo $_GET['empid']; ?>&uid=<?php echo $_GET['uid']; ?>" style="color: #444;font-size: 13px;padding: 14px 0px;float: right;text-decoration:none;"><i class="fa fa-key"></i> Change Password</a> -->
              <div class="dropdown loginpanel open_drop">
				  <style>
					  .open_drop:hover .openpanel{
						  display:block!important;
					  }
					  .open_drop .dropdown-menu li a{
					  text-decoration:none;
					  }
				  </style>
                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><?php echo $this->session->userdata('snowworldadmin')->UserName;?>
                <span class="caret"></span></button>
                <ul class="dropdown-menu openpanel" style="margin:0px;">
                  <li><a href="<?php echo  base_url();?>admin/login/adminupdatepassword?empid=<?php echo $_GET['empid']; ?>&uid=<?php echo $_GET['uid']; ?>">Change Password</a></li> 
                  <li><a href="<?php echo base_url();?>admin/login/logout" id="logout_btn">Logout</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>