<!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->

<?php // pend($vieww_order); ?>
<script src="<?php echo admin_url();?>js/moment.js"></script>

<link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"
      rel = "stylesheet">
<script src = "https://code.jquery.com/jquery-1.10.2.js"></script>
<script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script>
    $( function() {
    } );
</script>

<script type="text/javascript">

    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }

</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"
        crossorigin="anonymous"></script>
<?php //p($vieww); ?>
<!-- jQuery Form Validation code -->
<style>
    .input-group-addon {
        position: absolute;
        left: 0;
        width: 100%!important;
        padding: 12px 12px!important;
        font-size: 14px;
        font-weight: 400;
        line-height: 1;
        color: #555;
        text-align: center;
        background-color: transparent!important;
        border: none!important;
        border-radius: 4px;
        top: 0px;
    }
    .input-group-addon  .glyphicon-time{
        float:right;
    }
    input.error {
        border: 1px solid red;
    }

    label.error {
        border: 0px solid red;
        color: red;
        font-weight: normal;
        display: inline;
    }

    /*.tab_ble{ width:100%; float:left;}
    .tab_ble table{ width:100%; float:left;}
    .tab_ble table tr{ width:100%; float:left;}*/
    .tab_ble table tr th {
        /*padding: 5px 35px 15px 35px;*/
        text-align: center;
    }

    .tab_ble table tr td {
        /*border: 1px solid #CCC;*/
        text-align: center;

        /*min-width: 100px;*/
    }
    .lbl_box{
        float:left;
        margin-right:10px;
    }

    @media screen and (min-width: 320px) and (max-width: 680px){

        .lbl_box{ width: 100%;
            float:left;
            margin-right:10px;
        }

    }
    .checkbox_lbl{
        display: none;
        z-index: -9999;
    }
    .checkbox_lbl + label{
        position: relative;
        padding-left:25px;
        font-weight:normal;
        cursor: pointer;
    }
    .checkbox_lbl + label:before{
        position: absolute;
        content: "";
        width:19px;
        height:19px;
        border:solid 1px #abaaaa;
        left:0;
    }
    .checkbox_lbl + label:after{
        position: absolute;
        content: "";
        width: 10px;
        height: 6px;
        border-left: solid 2px #444;
        border-bottom: solid 2px #444;
        left: 5px;
        transform: rotate(-45deg);
        -o-transform: rotate(-45deg);
        -webkit-transform: rotate(-45deg);
        -moz-transform: rotate(-45deg);
        transition: 0.3s;
        -o-transition: 0.3s;
        -webkit-transition: 0.3s;
        -moz-transition: 0.3s;
        top: 5px;
        visibility: hidden;
        opacity: 0;
    }
    .checkbox_lbl:checked + label:after{
        visibility: visible;
        opacity: 1;
    }
  
    
    /* .tbl_overflow{
         float:left;
         width:100%;
         overflow-x: auto;
         padding-bottom: 10px;
     }*/
    #advance_search_d{
        display: none;
        margin-bottom:15px;
        float:left;
        width:100%;
    }

    .advance_btn{
        float:left;
        background: #0f71c3;
        color:#fff;
        padding:10px 15px;
        cursor: pointer;
    }
    .table > thead > tr > th {
        vertical-align: middle!important;

    }

    #container{width:100%;}
    #left{float:left}
    #right{float:right}

    #center{margin:0 auto;width:100px;float: left;padding: 1px 17px;}
</style>





<!-- <div class="wrapper"> -->

    <div class="col-lg-10">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name"><h2>Agent Management</h2></div>
                    <div class="page_box" style="margin: 0 !important;padding: 0;">
                        <div class="sep_box" style="padding: 10px 0 !important;">
                            <div class="col-lg-12">
                                <div id="container">
                                    <div id="left"> <div class="advance_btn">Advance Search</div></div>
                                    <div id="center">
                                        <form method="post" action="<?= base_url('admin/agent/adminmanagement?empid='.$_GET['empid'].'&uid='.$_GET['uid']) ?>">
                                            <div class="submit_tbl">
                                                <input type="submit" value="Show All Records" class="btn_button sub_btn" name="Clear">
                                            </div>
                                        </form>
                                    </div>
                                    <div id="right">
                                        <button id="btnExport" class="btn btn-primary" onsubmit="<?php echo base_url(); ?>admin/agent/adminmanagement">Download EXCEL
                                        </button>
                                    </div>
                                </div>
                                <table id="table_Excel" style="display: none" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>S.NO</th>
                                        <th>Username</th>
                                        <th>User Email</th>
                                        <th>User Contact</th>
                                        <th>Ticket no.</th>
                                        <th>No. of Package.</th>
                                        <th>No. of Peoples </th>
                                        <th>Price After Discount(in Rs.)</th>
                                        <th>Date of Booking</th>
                                        <th>Visit Date</th>
                                        <th>Visit Time</th>
                                        <th>paymentstatus</th>
                                        <th>Payment mode</th>
                                        <th>CCAVENUE Tracking ID</th>
                                        <th>Voucher Quantity</th>
                                        <th>Voucher Price(Total in Rs.)</th>
                                        <th>Coupon Code</th>
                                        <th>Print</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i = 1;
                                    //p($vieww_order);
                                    foreach ($vieww_order as $v) {  //p($v);
                                        if($this->session->userdata('snowworld')->EmployeeId) {
                                            if($this->session->userdata('snowworld')->branchid != $v->branch_id)
                                            {
                                                continue;
                                            }
                                        }
                                        $expack = implode(',',(explode('#', $v->packproductname)));

                                        ?>
                                        <tr>
                                            <td style="width:500px;"><?= $i; ?></td>
                                            <td><?php echo $v->billing_name; ?></td>
                                            <td><?php echo $v->billing_email; ?></td>
                                            <td><?php echo $v->billing_tel; ?></td>
                                            <td><?php echo $v->ticketid; ?></td>
                                            <td><?php echo $expack; ?></td>
                                            <td><?php echo $v->packpkg; ?></td>
                                            <td><?php echo $v->total; ?></td>
                                            <td><?php echo $v->order_status_date_time; ?></td>
                                            <?php if($v->departuretime > 12){
                                                $dt = $v->departuretime - 12;}
                                            else{$dt = $v->departuretime; } ?>
                                            <?php
                                            $formdate = date("g:i", strtotime($departuretime.':'.$v->frommin));
                                            $todate = date("g:i", strtotime($v->tohrs.':'.$v->tomin));
                                            ?>
                                            <td><?php echo $v->departuredate."<br>".$formdate.' '.$v->txtfromd." TO ".$todate.' '.$v->txttod; ?></td>
                                            <td><?php echo $v->order_status; ?></td>
                                            <td><?php echo $v->paymenttype; ?></td>
                                            <td><?php echo $v->tracking_id; ?></td>
                                            <td><?php

                                                $ex = explode(',', $v->addon);
                                                $ex1 = explode(',', $v->addonquantity);
                                                $ex2 = explode(',', $v->orderaddon_print_status);
                                                $ex3 = explode(',', $v->orderaddon_print_quantity);
                                                $ex4 = explode(',', $v->addonprice);
                                                foreach ($ex as $key => $a) {
                                                    if ($a == "") {
                                                        echo "N/A";
                                                        continue;
                                                    } elseif ($ex1[$key] != $ex3[$key]) {
                                                        echo '<button disabled="disabled" style="float: left; text-align: center" type="button" class="btn btn-danger" > ' . $a . ' PENDING(' . $ex1[$key] . ')</button><br>';

                                                    } else {
                                                        echo '<button style="float: left; text-align: center" type="button" class="btn btn-success" disabled="disabled">ALL ' . $a . '(' . $ex1[$key] . ')</button><br>';


                                                    }
                                                }

                                                ?></td>
                                            <td><?php


                                                foreach ($ex4 as $a4) {
                                                    if ($a == "") {
                                                        echo "N/A";
                                                        continue;
                                                    }
                                                    echo "<li>" . $a4 . "</li>";
                                                }
                                                ?></td>
                                            <td><?php

                                                echo $v->p_codename;
                                                ?></td>
                                            <td>

                                                <?php if ($v->op_ticket_print_status == 0){echo "Pending";} ?>
                                                <?php if ($v->op_ticket_print_status == 1){echo "Processing";} ?>
                                                <?php if ($v->op_ticket_print_status == 2){echo "Printed";} ?>
                                                <?php if ($v->op_ticket_print_status == 3){echo "CANCELED";} ?>

                                            </td>

                                        </tr>




                                        <?php $i++;
                                    } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="tab_ble">
                                <div id="advance_search_d">
                                    <form method="post" action="<?= base_url('admin/agent/order_sess?empid='.$_GET['empid'].'&uid='.$_GET['uid']) ?>">
                                        <div class="sep_box">

                                            <div class="col-md-12">
                                                <div style="float:left;font-weight:600;margin-right:8px;">
                                                    Branch :
                                                </div>

                                                <?php
                                                if($this->session->userdata('snowworld')->EmployeeId){
                                                    ?>
                                                    <input type="checkbox" checked="checked" name="filter_branch" class="checkbox_lbl"  id="filter_branch" value="<?php echo $this->session->userdata('snowworld')->branchid; ?>">

                                                    <?php
                                                    $parameter = array('act_mode' => 'getbranch',
                                                        'Param1' => $this->session->userdata('snowworld')->branchid,
                                                        'Param2' => '',
                                                        'Param3' => '',
                                                        'Param4' => '',
                                                        'Param5' => '',
                                                        'Param6' => '',
                                                        'Param7' => '',
                                                        'Param8' => '',
                                                        'Param9' => '');
                                                    //p($parameter);
                                                    $path = api_url() . "main_snowworld_v/getlocation/format/json/";
                                                    //pend($path);
                                                    $br_name = (array)curlpost($parameter, $path);
                                                    echo($br_name[0]['branch_name']);
                                                    ?>


                                                    <?php
                                                }else{
                                                    ?>
                                                    <?php foreach ($s_viewbranch as $key => $value) { ?>
                                                        <div class="lbl_box">
                                                            <input type="checkbox" name="filter_branch[]" class="checkbox_lbl"  id="filter_branch<?php echo $key;?>" value="<?php echo $value->branch_id; ?>">
                                                            <label for="filter_branch<?php echo $key;?>"><?php echo $value->branch_name; ?></label>
                                                        </div>

                                                    <?php }} ?>
                                            </div>
                                        </div>


                                        <div class="sep_box">
                                            <div class="col-md-4">
                                                <div class="tbl_input">
                                                    <input type="text" placeholder="name" name="filter_name">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="tbl_input">
                                                    <input type="text" placeholder="email" name="filter_email">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="sep_box">
                                            <div class="col-md-4">
                                                <div class="tbl_input">
                                                    <input type="text" placeholder="ticket no." name="filter_ticket">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="tbl_input">
                                                    <input type="text" placeholder="phone number" name="filter_mobile" maxlength="10">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="sep_box">
                                            <div class="col-md-4">
                                                <div class="tbl_input">
                                                    <input type="text" placeholder="payment type" name="filter_payment">
                                                </div></div>
                                        </div>
                                        <div class="sep_box">
                                            <div class="col-md-4">
                                                <div class="tbl_input">
                                                    <input type="text" id="filter_date1"  placeholder="DATE OF BOOKING FROM" name="filter_date_booking_from">
                                                </div></div>
                                            <div class="col-md-4">
                                                <div class="tbl_input">
                                                    <input type="text" id="filter_date2"  placeholder="DATE OF BOOKING TO" name="filter_date_booking_to">
                                                </div></div></div>

                                        <div class="sep_box">
                                            <div class="col-md-4">
                                                <div class="tbl_input">
                                                    <input type="text" id="filter_date3"  placeholder="SESSION DATE FROM" name="filter_date_session_from">
                                                </div></div>
                                            <div class="col-md-4">
                                                <div class="tbl_input">
                                                    <input type="text" id="filter_date4"  placeholder="SESSION DATE TO" name="filter_date_session_to">
                                                </div></div>
                                        </div>
                                        <div class="sep_box">
                                            <div class="col-md-4">
                                                <div class="tbl_input">
                                                    <input type="text" id="filter_date5"  placeholder="PRINTED DATE FROM" name="filter_date_printed_from">
                                                </div></div>
                                            <div class="col-md-4">
                                                <div class="tbl_input"><input type="text" id="filter_date6"  placeholder="PRINTED DATE TO" name="filter_date_printed_to">
                                                </div></div></div>



                                        <div class="sep_box">
                                            <div class="col-md-4">
                                                <div class="tbl_input">
                                                    <input type="text" id="filter_date_ticket"  placeholder="Visit Date" name="filter_date_ticket">
                                                </div></div>
                                            <!-- <div class="col-md-4">
                                                 <div class="tbl_input"><div class='input-group date' id='datetimepicker31'>
                                                         <input  type='text' id="filter_date_session"  placeholder="Visit Time" name="filter_date_session"  class="date_pi" />
                                                         <span class="input-group-addon">
                                                     <span class="glyphicon glyphicon-time"></span>
                                                 </span>
                                                     </div>
                                                 </div></div>--></div>





                                        <div class="sep_box">
                                            <div class="col-md-12">
                                                <div style="float:left;font-weight:600;margin-right:8px;">
                                                    Status :
                                                </div>
                                                <div class="lbl_box">
                                                    <input name="filter_status[]" id="Pending" class="checkbox_lbl" type="checkbox" value="0"  <?php if ($v->op_ticket_print_status == 0){echo "selected";} ?>  >
                                                    <label for="Pending">Pending</label>
                                                </div>
                                                <div class="lbl_box">
                                                    <input name="filter_status[]" id="Processing" class="checkbox_lbl" type="checkbox" value="1" <?php if ($v->op_ticket_print_status == 1){echo "selected";} ?>>
                                                    <label for="Processing">Processing</label>
                                                </div>
                                                <div class="lbl_box">
                                                    <input name="filter_status[]" id="Printed" class="checkbox_lbl" type="checkbox" value="2" <?php if ($v->op_ticket_print_status == 2){echo "selected";} ?>>
                                                    <label for="Printed">Printed</label>
                                                </div>
                                                <div class="lbl_box">
                                                    <input name="filter_status[]" id="Canceled" class="checkbox_lbl" type="checkbox" value="3" <?php if ($v->op_ticket_print_status == 2){echo "selected";} ?>>
                                                    <label for="Canceled">Canceled</label>
                                                </div>
                                            </div>
                                        </div>





                                        <div class="sep_box">
                                            <div class="col-md-12">
                                                <div style="float:left;font-weight:600;margin-right:8px;">
                                                    TIMESLOT :
                                                </div>
                                                <div class="lbl_box">
                                                    <input type="checkbox" id="cb_1" class="checkbox_lbl">
                                                    <label for="cb_1">All</label>
                                                </div>
                                                <?php //p($vieww_time);
                                                foreach ($vieww_time as $key => $value) { ?>
                                                    <div class="lbl_box">

                                                        <input type="checkbox" name="filter_tim[]" class="checkbox_lbl"  id="filter_tim<?php echo $key;?>" value="<?php echo $value->timeslot_from; ?>">
                                                        <label for="filter_tim<?php echo $key;?>"><?php echo $value->timeslot_from >12? ($value->timeslot_from - 12).":00 PM" : $value->timeslot_from.":00 AM"; ?></label>
                                                    </div>

                                                <?php }
                                                ?>
                                            </div>
                                        </div>



                                        <div class="sep_box">
                                            <div class="col-md-12">
                                                <div style="float:left;font-weight:600;margin-right:8px;">
                                                    STATUS :
                                                </div>
                                                <div class="lbl_box">
                                                    <input type="checkbox" id="cb_11" class="checkbox_lbl">
                                                    <label for="cb_11">All</label>
                                                </div>
                                                <?php //p($vieww_status);
                                                foreach ($vieww_status as $key => $value) {
                                                    if($value->order_status == ""){continue;}?>
                                                    <div class="lbl_box">

                                                        <input type="checkbox" name="filter_sta[]" class="checkbox_lbl"  id="filter_sta<?php echo $key;?>" value="<?php echo $value->order_status; ?>">
                                                        <label for="filter_sta<?php echo $key;?>"><?php echo ($value->order_status==""? "Blank": $value->order_status); ?></label>
                                                    </div>

                                                <?php }
                                                ?>
                                            </div>
                                        </div>




                                        <div class="sep_box">
                                            <div class="col-md-12">
                                                <div class="submit_tbl">
                                                    <input type="submit" value="Search" class="btn_button sub_btn"></div>
                                                *For all data leave all fields blank and click Search button</div>
                                        </div>

                                    </form>
                                </div>


                                <div class="tbl_overflow">
                                    <table id="table1" style="width: 100%" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>S.NO</th>
                                            <th>Username</th>
                                            <th>User Email</th>
                                            <th>User Contact</th>
                                            <th>Ticket no.</th>
                                            <th>No. of Package.</th>
                                            <th>No. of Peoples </th>
                                            <th>Price After Discount(in Rs.)</th>
                                            <th>Date of Booking</th>
                                            <th>Visit Date - Time</th>
                                            <th>paymentstatus</th>
                                            <th>Payment mode</th>
                                            <th>CCAVENUE Tracking ID</th>
                                            <th>Voucher Quantity</th>
                                            <th>Voucher Price(Total in Rs.)</th>
                                            <th>Coupon Code</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $i = 1;
                                        //p($vieww_order[0]);
                                        foreach ($vieww_order as $v) {
                                            if($this->session->userdata('snowworld')->EmployeeId) {
                                                if($this->session->userdata('snowworld')->branchid != $v->branch_id)
                                                {
                                                    continue;
                                                }
                                            }
                                            $expack = implode(',',(explode('#', $v->packproductname)));
                                            ?>
                                            <tr>
                                                <td><?= $i; ?></td>
                                                <td><?php echo $v->billing_name; ?></td>
                                                <td><?php echo $v->billing_email; ?></td>
                                                <td><?php echo $v->billing_tel; ?></td>
                                                <td><?php echo $v->ticketid; ?></td>
                                                <td><?php echo $expack; ?></td>
                                                <td><?php echo $v->packpkg; ?></td>
                                                <td><?php echo $v->total; ?></td>
                                                <td><?php  if($v->order_status_date_time!='') {  echo $v->order_status_date_time;} else { echo $v->addedon; } ?></td>
                                                <?php
                                                // p($v);
                                                /* if($v->departuretime > 12){
                                                           $dt = $v->departuretime - 12;}
                                                     else{$dt = $v->departuretime; }
                                                     if($v->tohrs>0)
                                                     {
                                                     $v->tohrs=$v->tohrs;
                                                     $v->txttod=$v->txttod;
                                                     }
                                                     else
                                                     {
                                                     $v->tohrs=$v->tohrs+12;
                                                     $v->txttod="PM";
                                                     }*/

                                                $formdate = date("g:i", strtotime($v->departuretime.':'.$v->frommin));
                                                $todate = date("g:i", strtotime($v->tohrs.':'.$v->tomin));
                                                ?>
                                                <td><?php echo $v->departuredate."<br>".$formdate.' '.$v->txtfromd." TO ".$todate.' '.$v->txttod; ?></td>
                                                <td><?php echo $v->order_status; ?></td>
                                                <td><?php echo $v->paymenttype; ?></td>
                                                <td><?php echo $v->tracking_id; ?></td>

                                                <td><?php

                                                    $ex = explode(',', $v->addon);
                                                    $ex1 = explode(',', $v->addonquantity);
                                                    $ex2 = explode(',', $v->orderaddon_print_status);
                                                    $ex3 = explode(',', $v->orderaddon_print_quantity);
                                                    $ex4 = explode(',', $v->addonprice);
                                                    foreach ($ex as $key => $a) {
                                                        if ($a == "") {
                                                            echo "N/A";
                                                            continue;
                                                        } elseif ($ex1[$key] != $ex3[$key]) {
                                                            echo '<button disabled="disabled" style="float: left; text-align: center" type="button" class="btn btn-danger" > ' . $a . ' PENDING(' . $ex1[$key] . ')</button><br>';

                                                        } else {
                                                            echo '<button style="float: left; text-align: center" type="button" class="btn btn-success" disabled="disabled">ALL ' . $a . '(' . $ex1[$key] . ')</button><br>';


                                                        }
                                                    }

                                                    ?></td>
                                                <td><?php


                                                    foreach ($ex4 as $a4) {
                                                        if ($a == "") {
                                                            echo "N/A";
                                                            continue;
                                                        }
                                                        echo "<li>" . $a4 . "</li>";
                                                    }
                                                    ?></td>
                                                <td><?php
                                                    echo $v->p_codename;
                                                    ?></td>
                                            </tr>

                                            <?php $i++;
                                        } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- printable area start here   -->
<?php $j = 1;


foreach ($vieww_order as $v) {

?>

<div id="printableArea<?= $v->pacorderid ?>" style="display:none" >

    <?php

    $date_array1 = explode("-", $v->addedon); // split the array
    $var_day1 = $date_array1[2]; //day seqment
    $var_month1 = $date_array1[1]; //month segment
    $var_year1 = rtrim($date_array1[0]," "); //year segment
    $new_date_format1 = strtotime("$var_year1-$var_month1-$var_day1"); // join them together


    $d1 = date(' jS F Y', strtotime($v->addedon) );
    //$data[ 'd1' ] = $d1;

    $date = '19:24:15 ';
    $d2 = date('h:i:s a ');

    $date_array = explode("/", $v->departuredate); // split the array
    $var_day = $date_array[0]; //day seqment
    $var_month = $date_array[1]; //month segment
    $var_year = rtrim($date_array[2]," "); //year segment
    $new_date_format = strtotime( date('Y-m-d', strtotime($v->departuredate) ) ); // join them together

    $input = ("$var_year$var_month$var_day");
    //p( date('Y-m-d', strtotime($data['orderdisplaydataval']->departuredate) ) );
    $d3 = date( "D", strtotime( date('Y-m-d', strtotime($v->departuredate) ) ) ) . "\n";
    $data['d3'] = $d3 ;
    //p( $data['orderdisplaydataval'] );

    $prepare_time_slot_from_date = $v->departuretime .':'.$v->frommin .' '.$v->txtfromd .' - '.
        $$v->tohrs.':'.$v->tomin.' '.$v->txttod;



    $data['prepare_time_slot_from_date'] = $prepare_time_slot_from_date;

    //	completeTimeSlot( getTimeSlotInArray( $this->session->userdata('destinationType') ) )

    $d4 = date(' jS F Y', $new_date_format);
    //$data['d4'] = $d4;

    ?>

    <html>
    <head>
        <title>Booking</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>
    <body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
    <!-- Save for Web Slices (Untitled-1) -->
    <table width="953" height="967" cellpadding="0" cellspacing="0"  style="border:solid 1px #37ace1;font-family:Arial, Helvetica, sans-serif;color:#555;">
        <tr>
            <td colspan="3">
                <table width="953">
                    <tr>

                        <td  width="350" valign="top" style="padding:40px 0px 0px 15px !important;">
                            <img src="<?php echo base_url()?>assets/admin/images/<?php echo $banner->bannerimage_logo; ?>"  alt=""></td>
                        <td border="0" width="800" style="text-align:center;">
                            <table width="600" style="margin:20px 20px;line-height:25px;">
                                <tr>
                                    <td style="color: #37ACE1;font-weight:700;padding:0px 0px;font-size:29px;text-align:center;">
                                        <strong><?php echo $banner->bannerimage_top1; ?></strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding:8px 0px 5px;font-size:18px;font-weight:700;"><strong><?php echo $banner->bannerimage_top2; ?> </strong></td>
                                </tr>
                                <tr>
                                    <td style="padding:4px 0px;font-size:22px;font-weight:700;"><strong><?php echo $banner->bannerimage_top3; ?> </strong></td>
                                </tr>
                                <tr>
                                    <td style="color: #444;font-weight:700;padding:0px 0px 0px;font-size:22px;"><strong><?php echo $banner->bannerimage_top4; ?> </strong></td>
                                </tr>

                                <tr>
                                    <td style="color: #444;font-weight:700;padding:0px 0px 0px;font-size:22px;"><strong><?php echo $banner->bannerimage_gstno; ?> </strong></td>
                                </tr>

                                <tr>
                                    <td style="color: #37ACE1;font-weight:600;padding:15px 0px 0px;font-size:25px;"><strong>Booking Confirmation Details</strong></td>
                                </tr>
                            </table>
                        </td>

                    </tr>
                </table>
            </td>

        </tr>
        <tr><td  style="border-top:dashed 3px #37ace1;"><td></tr>
        <tr>
            <td colspan="3">
                <table width="1000" cellpadding="0" cellspacing="0" style="margin:15px 0;">
                    <tr>
                        <td width="475">
                            <table style="margin:35px 35px 35px 35px;">
                                <tr>
                                    <td style="line-height:25px;">
                                        <h3 style="font-size:23px;">Venue Details</h3>

                                    </td>
                                </tr>
                                <tr><td style="font-weight:normal;line-height:23px;font-size:18px;"><?php echo $branch->branch_add; ?></td></tr>
                                <tr><td style="font-size:18px;"><span style="color: rgb(79, 193, 242);">Email :</span><?php echo $banner->bannerimage_branch_email; ?></td></tr>
                                <tr><td style="font-size:18px;"><span style="color: rgb(79, 193, 242);">Phone :</span><?php echo $banner->bannerimage_branch_contact; ?></td></tr>


                            </table>
                        </td>
                        <td style="border-left:dashed 3px #37ace1;"></td>
                        <td width="475">
                            <table style="margin:35px 35px 35px 35px;">
                                <tr>
                                    <td style="line-height:25px;">
                                        <h3 style="font-size:23px;">Guest Details</h3>

                                    </td>
                                </tr>
                                <tr><td style="font-weight:normal;line-height:23px;font-size:18px;"><?php  echo $v->billing_name; ?> <br></EX><?php echo $v->billing_address; ?></td></tr>
                                <tr><td style="font-size:18px;"><span style="color: rgb(79, 193, 242);">Email :</span> <?php echo $v->billing_email; ?></td></tr>
                                <tr><td style="font-size:18px;"><span style="color: rgb(79, 193, 242);">Phone :</span> <?php echo $v->billing_tel; ?></td></tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <table width="953" cellpadding="0" cellspacing="0" style="margin:15px 0px;">
                                <tr>
                                    <td colspan="3" style="border-top:dashed 3px #37ace1;"></td>
                                </tr>
                                <tr>
                                    <td><table width="475" style="margin:15px 0;border-right:dashed 3px #37ace1;">

                                            <tr>
                                                <td style="margin-bottom:0;font-size:15px;margin-top:5px;text-align:center;font-weight:bold;">Booking No.</td>
                                                <td style="margin-bottom:0;font-size:15px;margin-top:5px;text-align:center;font-weight:bold;">Booking Date.</td></tr>
                                            <tr>
                                                <td style="text-align:center;margin-top:5px;font-size:13px;"><?php  echo $v->ticketid; ?> </td>
                                                <td style="text-align:center;margin-top:5px;font-size:13px;"><?php  echo $d1; ?></td></tr></table></td>
                                    <td width="3"></td>
                                    <td>
                                        <table width="475" style="margin:15px 0;">
                                            <tr>
                                                <td style="margin-bottom:0;font-size:15px;margin-top:5px;text-align:center;font-weight:bold;">Visit Date.</td>
                                                <td style="margin-bottom:0;font-size:15px;margin-top:5px;text-align:center;font-weight:bold;">Session Time</td>
                                            </tr>
                                            <tr>
                                                <td style="text-align:center;margin-top:5px;font-size:13px;"><?php  echo $d4; ?>.</td>
                                                <td style="text-align:center;margin-top:5px;font-size:13px;"><?php echo $prepare_time_slot_from_date;  ?></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="border-top:dashed 3px #37ace1;"></td>
                                </tr>
                            </table>
                        </td></tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <table width="860" cellpadding="0" cellspacing="0" style="border:solid 1px #37ace1;font-family:Arial, Helvetica, sans-serif;color:#555;margin:0px 50px 10px 50px; ">
                    <tr>
                        <td width="215" style="background:#444;text-align:center;font-size:18px;color:#fff;padding:15px 0px;font-weight:bold;">
                            Items
                        </td>
                        <td width="215" style="background:#444;text-align:center;font-size:18px;color:#fff;padding:15px 0px;font-weight:bold;">Qty
                        </td>
                        <td width="215" style="background:#444;text-align:center;font-size:18px;color:#fff;padding:15px 0px;font-weight:bold;">Cost</td>
                        <td width="215" style="background:#444;text-align:center;font-size:18px;color:#fff;padding:15px 0px;font-weight:bold;">Total</td>
                    </tr>
                    <?php 	//p($orderdisplaydataval);
                    $total_array = [];
                    $addon_total_price_with_quantity = $package_total_price = '';
                    $package_name_array = explode('#', $v->packproductname );
                    $package_img_array = explode('#', $v->packimg );
                    $package_qty_array = explode('#', $v->package_qty );
                    $package_price_array = explode('#', $v->package_price );

                    $package_total_price = $total_array[] = getSumAllArrayElement( calculatePriceByQty( explode('#', $v->package_price ), explode('#', $v->package_qty ) ) );

                    foreach( $package_name_array as $ky => $val){
                        ?>
                        <tr>
                            <td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px"><?php echo $val . ' Package'; ?> </td>
                            <td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px"><?php echo $package_qty_array[ $ky ];  ?></td>
                            <td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px"><?php echo $package_price_array[ $ky ]; ?></td>
                            <td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px"><?php echo '₹ ' . ( $package_qty_array[ $ky ] * $package_price_array[ $ky ] ) ?> </td>
                        </tr>
                    <?php	}	?>

                    <?php
                    //p($orderaddonedisplaydata);
                    // $addon_data = getAddonTotalPricePrint( $orderaddonedisplaydata ) ;
                    //p( $addon_data['addon_price_array'] );
                    //p($addon_data);

                    //p($total_array);


                    $total_array[] = $v->internethandlingcharges;

                    //			p($addon_total_price_with_quantity);
                    // $addon_price_array = $addon_data->addon_price_array;
                    // $addon_qty_array = $addon_data->addon_qty_array;



                    $addon_array = explode(',', $v->addon) ;
                    $addonquantity_array = explode(',', $v->addonquantity) ;
                    $addonprice_array = explode(',', $v->addonprice) ;

                    $addon_total_price_with_quantity = $total_array[] = getSumAllArrayElement( $addonprice_array );

                    $total = ($v->promocodeprice) ?   ( getSumAllArrayElement( $total_array ) - $v->promocodeprice )  :  getSumAllArrayElement( $total_array );

                    foreach ($addon_array as $a => $b) {
                        if($b!='Something Went Wrong' ){

                            ?>
                            <tr><td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px"><?php echo $b; ?></td>
                                <td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px"><?php echo $addonquantity_array[ $a ]; ?></td>
                                <td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px"><span><i class="icon-rupee" aria-hidden="true"></i></span><?php echo ( $addonprice_array[ $a ] / $addonquantity_array[ $a ] ); ?></td>
                                <td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px"><span><i class="icon-rupee" aria-hidden="true"></i></span><?php echo $addonprice_array[ $a ] ; ?></td>
                            </tr>
                        <?php  }} ?>


                    <tr>
                        <td colspan="4">
                            <table width="760" cellpadding="0" cellspacing="0" style="border-top:solid 1px #37ace1;font-family:Arial, Helvetica, sans-serif;color:#555;margin:10px 50px 10px 50px; ">
                                <tr>
                                    <td width="380" style="text-align:left;font-size:18px;color:#000;padding:10px 0px 10px 40px">Price:</td>
                                    <td width="380" style="text-align:right;font-size:18px;color:#000;padding:10px 40px 10px 0px"><span><i class="icon-rupee" aria-hidden="true"></i></span><?php echo ( $package_total_price + $addon_total_price_with_quantity ); ?></td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <table width="860" cellpadding="0" cellspacing="0" style="border:solid 1px #37ace1;font-family:Arial, Helvetica, sans-serif;color:#555;margin:0px 50px 10px 50px; ">
                    <tr><td width="430" style="text-align:left;font-size:18px;color:#000;padding:10px 0px 10px 40px">Discount Amount</td><td width="430" style="text-align:right;font-size:18px;color:#000;padding:10px 40px 10px 0px"><span><i class="icon-rupee" aria-hidden="true"></i></span><?php echo $v->promocodeprice; ?></td></tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <table width="860" cellpadding="0" cellspacing="0" style="border:solid 1px #37ace1;font-family:Arial, Helvetica, sans-serif;color:#555;margin:0px 50px 10px 50px; ">
                    <tr><td width="430" style="text-align:left;font-size:18px;color:#000;padding:10px 0px 10px 40px">Internet Handling Charges</td><td width="430" style="text-align:right;font-size:18px;color:#000;padding:10px 40px 10px 0px"><span><i class="icon-rupee" aria-hidden="true"></i></span><?php echo $v->internethandlingcharges	; ?></td></tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <table width="860" cellpadding="0" cellspacing="0" style="border:solid 1px #37ace1;font-family:Arial, Helvetica, sans-serif;color:#555;margin:0px 50px 30px 50px; ">
                    <tr><td width="430" style="text-align:left;font-size:18px;color:#000;padding:10px 0px 10px 40px">Total</td><td width="430" style="text-align:right;font-size:18px;color:#000;padding:10px 40px 10px 0px"><span><i class="icon-rupee" aria-hidden="true"></i></span><?php echo $total; ?></td></tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <table width="475" style="margin:15px 0px 15px 50px;">
                    <tr><td style="font-size:17px;font-weight:bold;color:#000;padding:4px 0px;"></td></tr>
                    <tr><td style="font-size:17px;font-weight:bold;color:#000;padding:4px 0px;">PAN No. : AAFFC9989B</td></tr>
                    <tr><td style="font-size:17px;font-weight:bold;color:#000;padding:4px 0px;">Encl. Terms and Conditions</td></tr>
                </table>
            </td>
        </tr>
    </table>

    <br>
    <br>  <br>  <br>  <br>  <br>


    <table width="953" cellpadding="0" cellspacing="0"  style="border:solid 1px #37ace1;font-family:Arial, Helvetica, sans-serif;color:#555;">
        <tr>
            <td>
                <table>
                    <tr>
                        <td width="150" valign="top" style="padding:15px 0px 0px 15px;"><img src="assets/admin/images/<?php echo $banner->bannerimage_logo; ?>"  alt="" width="220"></td>
                        <td style="text-align:center;">
                            <table width="653" style="margin:20px 20px;line-height:25px;">
                                <tr>
                                    <td width="653" style="color: #37ACE1;font-weight:700;padding:0px 0px;font-size:29px;text-align:center;">
                                        <strong><?php echo $banner->bannerimage_top1; ?></strong>

                                    </td>
                                </tr>
                                <tr><td style="padding:8px 0px 5px;font-size:18px;font-weight:700;"><strong><?php echo $banner->bannerimage_top2; ?></strong></td></tr>


                                <tr><td style="color: #37ACE1;font-weight:600;padding:15px 0px 0px;font-size:25px;"><strong>Terms and Conditions</strong></td></tr>
                            </table>
                        </td>
                        <td width="150" valign="top" style="padding:15px 15px 0px 0px;"><img src="assets/admin/images/<?php echo $banner->bannerimage_logo; ?>"  alt="" width="220"></td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <table width="940" style="margin-left:50px;margin-top:10px;margin-bottom:80px;color:#000;font-size:16px;line-height:25px;">


                                <?php  $j=1;foreach ($tearmsgatway as $key => $value11) {  ?>
                                    <tr>
                                        <td width="30" valign="top"><?php echo  $j; ?>.</td>
                                        <td width="900" valign="top"><?php echo $value11->term_name; ?></td>
                                    </tr>
                                    <?php  $j++;} ?>

                                <tr>
                                    <td width="30" valign="top">&nbsp;</td>
                                    <td width="900" valign="top">
                                        <table style="font-size:16px;line-height:25px;">

                                        </table>
                                    </td>
                                </tr>


                            </table>
                        </td>
                    </tr>
                </table>
            </td>

        </tr>
    </table>

    </td>
    </tr>



    </table>
    <?php $j++;} ?>

    <!-- printable area end here   -->


    <script>

        function funct_change_status(a,b) {
            if(b == 2) {
                $(".ticket" + a).attr('disabled', true);
            }
            $.ajax({
                url: '<?php echo base_url()?>admin/ordercontrol/op_ticket_print_status',
                type: 'POST',
                data: {'pacorderid': a,'op_ticket_print_status':b},
                dataType: "JSON",
                success: function(data){


//console.log(data.s[0].ticket_status);
                    if(data.s[0].ticket_status == 2)
                    {
                        a.attr('disabled',true);

                    }
//console.log(data1);
                }
            });
//var b = $(this).val();
// alert(b);return false;

        }

        /*    $(function () {
        $("#op_ticket_print_status").change(function () {
        alert("hello"); return false;
        var a = $(this);
        if($("#op_ticket_print_status").val() == 2)
        {
        a.attr('disabled',true);

        }
        //return false;
        $.ajax({
        url: ' echo base_url()?>admin/ordercontrol/op_ticket_print_status',
        type: 'POST',
        data: {'pacorderid': $(this).attr('data-id'),'op_ticket_print_status':$("#op_ticket_print_status").val()},
        dataType: "JSON",
        success: function(data){


        //console.log(data.s[0].ticket_status);
            if(data.s[0].ticket_status == 2)
            {
                a.attr('disabled',true);

            };
           //console.log(data1);
        }
        });
        });
        });*/

        function ticket_print(a) {
//alert(a);
            window.print();

        }

        function update_com(a, b) {
//alert(a +b);
            $("#com_name_update").val(a);
            $("#com_id").val(b);
            $("#update_com").css('display', 'block');
            $("#add_com").css('display', 'none');
            $("#com_name_update").focus();

        }
        function UpdateCancel() {
            $("#add_com").css('display', 'block');
            $("#update_com").css('display', 'none');
            return false;
        }


    </script>



    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>

    <!-- Include Date Range Picker -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

    <script>
        $(document).ready(function(){
            $(".advance_btn").click(function(){
                $("#advance_search_d").slideToggle();
            });

        })
    </script>

    <script src="<?php echo admin_url();?>js/bootstrap-datetimepicker.min.js"></script>
    <link href="<?php echo admin_url();?>css/bootstrap-datetimepicker.css" rel="stylesheet">

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css"></style>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
                                                                                                         <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
                                                                                                                                                                                                                    <script>
                                                                                                                                                                                                                    $(document).ready(function(){
    //$('#table1').DataTable();
    $('#table1').DataTable( {"scrollY": 400,"scrollX": true } );
    $.noConflict();
    var date_input=$('input[name="filter_date_booking_from"],input[name="filter_date_booking_to"],input[name="filter_date_printed_from"],input[name="filter_date_printed_to"]'); //our date input has the name "date"
    var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
    date_input.datepicker({
        format: 'yyyy-mm-dd',
        container: container,
        todayHighlight: true,
        autoclose: true,
    });


    var date_input=$('input[name="filter_date_session_from"],input[name="filter_date_session_to"],input[name="filter_date_ticket"]'); //our date input has the name "date"
    var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
    date_input.datepicker({
        format: 'dd/mm/yyyy',
        container: container,
        todayHighlight: true,
        autoclose: true,
    });
    /*$('#filter_date1').datepicker('setDate', 'today');
    $('#filter_date2').datepicker('setDate', 'today');
    $('#filter_date3').datepicker('setDate', 'today');
    $('#filter_date4').datepicker('setDate', 'today');*/
    });
    //$('#table1').dataTable();
    </script>





      <script type="text/javascript">
                                    $(function () {
    $('#datetimepicker31,#datetimepicker41').datetimepicker({
        format: 'LT'
    });

    $('.dpxx').datetimepicker({
        format: 'LT'
    });


    });

    $(document).ready(function() {
    $("#btnExport").click(function(e) {
        e.preventDefault();

    //getting data from our table
    var data_type = 'data:application/vnd.ms-excel';
        var table_div = document.getElementById('table_Excel');
        var table_html = table_div.outerHTML.replace(/ /g, '%20');

        var a = document.createElement('a');
        a.href = data_type + ', ' + table_html;
        a.download = 'exported_table_' + Math.floor((Math.random() * 9999999) + 1000000) + '.xls';
        a.click();
    });
    /*var x = $(window).height();
    var y = x-55;
    $(".page_contant").css({"height":y+"px","min-height":"auto"});
    var tp = $("#table1");*/
    });
    </script>