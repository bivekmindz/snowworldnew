			<!-- printable area start here -->
			<?php

			$date_array1 = explode("-", $v->addedon ); // split the array
			$var_day1 = $date_array1[2]; //day seqment
			$var_month1 = $date_array1[1]; //month segment
			$var_year1 = $date_array1[0]; //year segment
			$new_date_format1 = strtotime("$var_year1-$var_month1-$var_day1"); // join them together

			$d1= date(' jS F Y', $new_date_format1);


			$date = '19:24:15 ';
			$d2= date('h:i:s a');

			$date_array = explode("/", $v->departuredate); // split the array
			$var_day = $date_array[0]; //day seqment
			$var_month = $date_array[1]; //month segment
			$var_year = rtrim($date_array[2]," "); //year segment
			//$new_date_format = strtotime("$var_year-$var_month-$var_day"); // join them together
			$new_date_format = strtotime( date('Y-m-d', strtotime($data['orderdisplaydataval']->departuredate) ) );

			// do not delete any variable to avoid undefined index  ERROR				 

			$input = ("$var_year$var_month$var_day");

			$d3= date("D", strtotime($input)) . "\n";


			$visiting_date = date(' jS F Y', $new_date_format );

			if($timeslotses->timeslot_from > 12)
			{
				$from_session_time =  ($timeslotses->timeslot_from - 12).":".$timeslotses->timeslot_minfrom.'PM' ;

			}
			else{
				$from_session_time = $timeslotses->timeslot_from.":".$timeslotses->timeslot_minfrom."AM";
			}

			if($data['timeslotses']->timeslot_to > 12)
			{
				$to_session_time =  ($timeslotses->timeslot_to - 12).":".$timeslotses->timeslot_minto.$orderdisplaydataval->txttod;
			}
			else{
				$to_session_time = $timeslotses->timeslot_to.":".$timeslotses->timeslot_minto.$orderdisplaydataval->txttod;
			}


			?>
<html>
<head>
    <title>Booking</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
    <!-- Save for Web Slices (Untitled-1) -->
    <table width="953" height="967" cellpadding="0" cellspacing="0"  style="border:solid 1px #37ace1;font-family:Arial, Helvetica, sans-serif;color:#555;">
    <tr>
    	<td colspan="3">
    		<table width="953">
    			<tr>
   					<td  width="350" valign="top" style="padding:40px 0px 0px 15px !important;">
						<img src="assets/admin/images/<?php echo $banner->bannerimage_logo; ?>"  alt=""></td>
    				<td border="0" width="800" style="text-align:center;">
    						<table width="600" style="margin:20px 20px;line-height:25px;">
    			<tr>
    				<td style="color: #37ACE1;font-weight:700;padding:0px 0px;font-size:29px;text-align:center;">
   						<strong><?php echo $banner->bannerimage_top1; ?></strong>	
                	</td>
                </tr>
                <tr>
					<td style="padding:8px 0px 5px;font-size:18px;font-weight:700;"><strong><?php echo $banner->bannerimage_top2; ?> </strong></td>
				</tr>
                <tr>
					<td style="padding:4px 0px;font-size:22px;font-weight:700;"><strong><?php echo $banner->bannerimage_top3; ?> </strong></td>
				</tr>
                <tr>
					<td style="color: #444;font-weight:700;padding:0px 0px 0px;font-size:22px;"><strong><?php echo $banner->bannerimage_top4; ?> </strong></td>
				</tr>

 				<tr>
					<td style="color: #444;font-weight:700;padding:0px 0px 0px;font-size:22px;"><strong><?php echo $banner->bannerimage_gstno; ?> </strong></td>
				</tr>

                <tr>
					<td style="color: #37ACE1;font-weight:600;padding:15px 0px 0px;font-size:25px;"><strong>Booking Confirmation Details</strong></td>
				</tr>
            </table>
                </td>

                </tr>
                </table>
                </td>

    </tr>
    <tr><td  style="border-top:dashed 3px #37ace1;"><td></tr>
      <tr>
      <td colspan="3">
      <table width="1000" cellpadding="0" cellspacing="0" style="margin:15px 0;">
      <tr>
      <td width="475">
      <table style="margin:35px 35px 35px 35px;">
      <tr>
      <td style="line-height:25px;">
                    <h3 style="font-size:23px;">Venue Details</h3>

                    </td>
                    </tr>
                    <tr><td style="font-weight:normal;line-height:23px;font-size:18px;"><?php echo $branch->branch_add; ?></td></tr>
                    <tr><td style="font-size:18px;"><span style="color: rgb(79, 193, 242);">Email :</span><?php echo $banner->bannerimage_branch_email; ?></td></tr>
                    <tr><td style="font-size:18px;"><span style="color: rgb(79, 193, 242);">Phone :</span><?php echo $banner->bannerimage_branch_contact; ?></td></tr>

                </table>
                </td>
      <td style="border-left:dashed 3px #37ace1;"></td>
      <td width="475">
      <table style="margin:35px 35px 35px 35px;">
      <tr>
      <td style="line-height:25px;">
                    <h3 style="font-size:23px;">Guest Details</h3>

                    </td>
                    </tr>
                    <tr><td style="font-weight:normal;line-height:23px;font-size:18px;"><?php  echo $v->billing_name; ?> <br></EX><?php echo $v->billing_address; ?></td></tr>
                    <tr><td style="font-size:18px;"><span style="color: rgb(79, 193, 242);">Email :</span> <?php echo $v->billing_email; ?></td></tr>
                    <tr><td style="font-size:18px;"><span style="color: rgb(79, 193, 242);">Phone :</span> <?php echo $v->billing_tel; ?></td></tr>
                </table>
                </td>
      </tr>
      <tr>
      <td colspan="3">
       <table width="953" cellpadding="0" cellspacing="0" style="margin:15px 0px;">
       <tr>
       <td colspan="3" style="border-top:dashed 3px #37ace1;"></td>
       </tr>
       <tr>
       <td><table width="475" style="margin:15px 0;border-right:dashed 3px #37ace1;">

       			<tr>
					<td style="margin-bottom:0;font-size:15px;margin-top:5px;text-align:center;font-weight:bold;">Booking No.</td>
					<td style="margin-bottom:0;font-size:15px;margin-top:5px;text-align:center;font-weight:bold;">Booking Date.</td></tr>
                <tr>
					<td style="text-align:center;margin-top:5px;font-size:13px;"><?php  echo $v->ticketid; ?> </td>
					<td style="text-align:center;margin-top:5px;font-size:13px;"><?php  echo $d1; ?></td></tr></table></td>
               		 <td width="3"></td>
                	<td>
						<table width="475" style="margin:15px 0;">
							<tr>
								<td style="margin-bottom:0;font-size:15px;margin-top:5px;text-align:center;font-weight:bold;">Visit Date.</td>
								<td style="margin-bottom:0;font-size:15px;margin-top:5px;text-align:center;font-weight:bold;">Session Time</td>
							</tr>
							<tr>
								<td style="text-align:center;margin-top:5px;font-size:13px;"><?php  echo $d4; ?>.</td>
								<td style="text-align:center;margin-top:5px;font-size:13px;"><?php echo $prepare_time_slot_from_date;  ?></td>
							</tr>
						</table>
		   			</td>
       </tr>
       <tr>
       <td colspan="3" style="border-top:dashed 3px #37ace1;"></td>
       </tr>
       </table>
      </td></tr>
      </table>
      </td>
      </tr>
      <tr>
      <td colspan="3">
        <table width="860" cellpadding="0" cellspacing="0" style="border:solid 1px #37ace1;font-family:Arial, Helvetica, sans-serif;color:#555;margin:0px 50px 10px 50px; ">
        <tr>
        <td width="215" style="background:#444;text-align:center;font-size:18px;color:#fff;padding:15px 0px;font-weight:bold;">
        Items
</td>
        <td width="215" style="background:#444;text-align:center;font-size:18px;color:#fff;padding:15px 0px;font-weight:bold;">Qty
</td>
        <td width="215" style="background:#444;text-align:center;font-size:18px;color:#fff;padding:15px 0px;font-weight:bold;">Cost</td>
        <td width="215" style="background:#444;text-align:center;font-size:18px;color:#fff;padding:15px 0px;font-weight:bold;">Total</td>
        </tr>
<?php 	//p($orderdisplaydataval);
		$total_array = [];	
			$addon_total_price_with_quantity = $package_total_price = '';
		$package_name_array = explode('#', $v->packproductname );
		$package_img_array = explode('#', $v->packimg );
		$package_qty_array = explode('#', $v->package_qty );
		$package_price_array = explode('#', $v->package_price );
			
		$package_total_price = $total_array[] = getSumAllArrayElement( calculatePriceByQty( explode('#', $v->package_price ), explode('#', $v->package_qty ) ) );
		
		foreach( $package_name_array as $ky => $val){ 
?>
        <tr>
			<td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px"><?php echo $val . ' Package'; ?> </td>
        <td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px"><?php echo $package_qty_array[ $ky ];  ?></td>
        <td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px"><?php echo $package_price_array[ $ky ]; ?></td>
        <td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px"><?php echo '₹ ' . ( $package_qty_array[ $ky ] * $package_price_array[ $ky ] ) ?> </td>
        </tr>
<?php	}	?>

<?php 
			//p($orderaddonedisplaydata);
		 $addon_data = getAddonTotalPricePrint( $orderaddonedisplaydata ) ; 
		 //p( $addon_data['addon_price_array'] );
			//p($addon_data);
	     $addon_total_price_with_quantity = $total_array[] =  getSumAllArrayElement( $addon_data['addon_price_array'] );
		 //p($total_array);
							
			
		 $total_array[] = $orderdisplaydataval->internethandlingcharges;
//			p($addon_total_price_with_quantity);
		// $addon_price_array = $addon_data->addon_price_array;
		// $addon_qty_array = $addon_data->addon_qty_array;
			
			
		 
		$addon_array = explode(',', $v->addon) ;	
		$addonquantity_array = explode(',', $v->addonquantity) ;								
		$addonprice_array = explode(',', $v->addonprice) ;								
		$addon_total_price_with_quantity = $total_array[] = getSumAllArrayElement( $addonprice_array );
				
							
        foreach ($addon_array as $a => $b) {
            if($b!='Something Went Wrong' ){

                ?>
            <tr><td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px"><?php echo $b; ?></td>
				<td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px"><?php echo $addonquantity_array[ $a ]; ?></td>
                <td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px"><span><i class="icon-rupee" aria-hidden="true"></i></span><?php echo ( $addonprice_array[ $a ] / $addonquantity_array[ $a ] ); ?></td>
                <td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px"><span><i class="icon-rupee" aria-hidden="true"></i></span><?php echo $addonprice_array[ $a ] ; ?></td>
            </tr>
   <?php  }} ?>


            <tr>
                <td colspan="4">
                    <table width="760" cellpadding="0" cellspacing="0" style="border-top:solid 1px #37ace1;font-family:Arial, Helvetica, sans-serif;color:#555;margin:10px 50px 10px 50px; ">
                        <tr>
                            <td width="380" style="text-align:left;font-size:18px;color:#000;padding:10px 0px 10px 40px">Price:</td>
                            <td width="380" style="text-align:right;font-size:18px;color:#000;padding:10px 40px 10px 0px"><span><i class="icon-rupee" aria-hidden="true"></i></span><?php echo ( $package_total_price + $addon_total_price_with_quantity ); ?></td>
                        </tr>
                    </table>
                </td>
            </tr>

        </table>
      </td>
      </tr>
        <tr>
            <td colspan="3">
                <table width="860" cellpadding="0" cellspacing="0" style="border:solid 1px #37ace1;font-family:Arial, Helvetica, sans-serif;color:#555;margin:0px 50px 10px 50px; ">
                    <tr><td width="430" style="text-align:left;font-size:18px;color:#000;padding:10px 0px 10px 40px">Discount Amount</td><td width="430" style="text-align:right;font-size:18px;color:#000;padding:10px 40px 10px 0px"><span><i class="icon-rupee" aria-hidden="true"></i></span><?php echo $v->promocodeprice; ?></td></tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <table width="860" cellpadding="0" cellspacing="0" style="border:solid 1px #37ace1;font-family:Arial, Helvetica, sans-serif;color:#555;margin:0px 50px 10px 50px; ">
                    <tr><td width="430" style="text-align:left;font-size:18px;color:#000;padding:10px 0px 10px 40px">Internet Handling Charges</td><td width="430" style="text-align:right;font-size:18px;color:#000;padding:10px 40px 10px 0px"><span><i class="icon-rupee" aria-hidden="true"></i></span><?php echo $v->internethandlingcharges	; ?></td></tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <table width="860" cellpadding="0" cellspacing="0" style="border:solid 1px #37ace1;font-family:Arial, Helvetica, sans-serif;color:#555;margin:0px 50px 30px 50px; ">
                    <tr><td width="430" style="text-align:left;font-size:18px;color:#000;padding:10px 0px 10px 40px">Total</td><td width="430" style="text-align:right;font-size:18px;color:#000;padding:10px 40px 10px 0px"><span><i class="icon-rupee" aria-hidden="true"></i></span><?php echo getSumAllArrayElement( $total_array ); ?></td></tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <table width="475" style="margin:15px 0px 15px 50px;">
                    <tr><td style="font-size:17px;font-weight:bold;color:#000;padding:4px 0px;"></td></tr>
                    <tr><td style="font-size:17px;font-weight:bold;color:#000;padding:4px 0px;">PAN No. : AAFFC9989B</td></tr>
                    <tr><td style="font-size:17px;font-weight:bold;color:#000;padding:4px 0px;">Encl. Terms and Conditions</td></tr>
                </table>
            </td>
        </tr>
    </table>

    <br>
    <br>  <br>  <br>  <br>  <br>


    <table width="953" cellpadding="0" cellspacing="0"  style="border:solid 1px #37ace1;font-family:Arial, Helvetica, sans-serif;color:#555;">
        <tr>
            <td>
                <table>
                    <tr>
                        <td width="150" valign="top" style="padding:15px 0px 0px 15px;"><img src="assets/admin/images/<?php echo $banner->bannerimage_logo; ?>"  alt="" width="220"></td>
                        <td style="text-align:center;">
                            <table width="653" style="margin:20px 20px;line-height:25px;">
                                <tr>
                                    <td width="653" style="color: #37ACE1;font-weight:700;padding:0px 0px;font-size:29px;text-align:center;">
                                        <strong><?php echo $banner->bannerimage_top1; ?></strong>

                                    </td>
                                </tr>
                                <tr><td style="padding:8px 0px 5px;font-size:18px;font-weight:700;"><strong><?php echo $banner->bannerimage_top2; ?></strong></td></tr>


                                <tr><td style="color: #37ACE1;font-weight:600;padding:15px 0px 0px;font-size:25px;"><strong>Terms and Conditions</strong></td></tr>
                            </table>
                        </td>
                        <td width="150" valign="top" style="padding:15px 15px 0px 0px;"><img src="assets/admin/images/<?php echo $banner->bannerimage_logo; ?>"  alt="" width="220"></td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <table width="940" style="margin-left:50px;margin-top:10px;margin-bottom:80px;color:#000;font-size:16px;line-height:25px;">
                               

							<?php  $i=1;foreach ($tearmsgatway as $key => $value11) {  ?>

								<tr>
									<td width="30" valign="top"><?php echo  $i; ?>.</td>
									<td width="900" valign="top"><?php echo $value11['term_name']; ?></td>
								</tr>

							<?php  $i++;} ?>

                                <tr>
                                    <td width="30" valign="top">&nbsp;</td>
                                    <td width="900" valign="top">
                                        <table style="font-size:16px;line-height:25px;">
                                           
                                        </table>
                                    </td>
                                </tr>
                                

                            </table>
                        </td>
                    </tr>
                </table>
            </td>

        </tr>
    </table>

    </td>
    </tr>



    </table>
<!-- printable area end here -->
