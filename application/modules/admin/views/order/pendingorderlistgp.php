<!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->
<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery-1.9.1.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery.validate.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"
crossorigin="anonymous"></script>
<?php //p($vieww); ?>
<!-- jQuery Form Validation code -->
<style>
input.error {
border: 1px solid red;
}
label.error {
border: 0px solid red;
color: red;
font-weight: normal;
display: inline;
}
/*.tab_ble{ width:100%; float:left;}
.tab_ble table{ width:100%; float:left;}
.tab_ble table tr{ width:100%; float:left;}*/
.tab_ble table tr th {
padding: 5px 35px 15px 35px;
text-align: center;
}
.tab_ble table tr td {
border: 1px solid #CCC;
text-align: center;
min-width: 100px;
}
</style>
<meta name="viewport" content="width=device-width, initial-scale=1">
<div class="wrapper">
    <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>PENDING/PROCESSING ORDER LISTING</h2>
                    </div>
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-12">
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?>
                                    <?php
                                    if ($this->session->flashdata('message')) {
                                    echo $this->session->flashdata('message');
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="tab_ble">
                                <table id="table1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>S.NO</th>
                                            <th>Username</th>
                                            <th>User Email</th>
                                            <th>User Contact</th>
                                            <th>Ticket no.</th>
                                            <th>Branch</th>
                                            <th>Location</th>
                                            <th>Total Price(in Rs.)</th>
                                            <th>Discount Coupon</th>
                                            <th>Price After Discount(in Rs.)</th>
                                            <th>Date of Booking</th>
                                            <th>Ride Date</th>
                                            <th>Ride Time</th>
                                            <th>paymentstatus</th>
                                            <th>paymentmode</th>
                                            <th>Package Name</th>
                                            <!-- <th>packpkg</th>-->
                                            <th>No. Of Packages (i.e No. of Peoples) </th>
                                            <!--  <th>orderstatus</th>-->
                                            <th>Package Price (Total in Rs.)</th>
                                            <th>Voucher Quantity</th>
                                            <!--  <th>Addon Print status</th>-->
                                            <!-- <th>Printed Vouchers</th>-->
                                            <th>Voucher Price(Total in Rs.)</th>
                                            <th>Print</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i = 1;
                                        foreach ($vieww_order as $v) {
                                        if ($v->op_ticket_print_status == 2)
                                        {
                                        continue;
                                        }
                                        if($this->session->userdata('snowworld')->EmployeeId) {
                                        if($this->session->userdata('snowworld')->LocationId != $v->locationid)
                                        {
                                        continue;
                                        }
                                        }
                                        
                                        ?>
                                        <tr>
                                            <td><?= $i; ?></td>
                                            <td><?php echo $v->username; ?></td>
                                            <td><?php echo $v->email; ?></td>
                                            <td><?php echo $v->contact; ?></td>
                                            <td><?php echo $v->ticketid; ?></td>
                                            <td><?php
                                                $parameter = array('act_mode' => 'getbranch',
                                                'Param1' => $v->branch_id,
                                                'Param2' => '',
                                                'Param3' => '',
                                                'Param4' => '',
                                                'Param5' => '',
                                                'Param6' => '',
                                                'Param7' => '',
                                                'Param8' => '',
                                                'Param9' => '');
                                                //p($parameter);
                                                $path = api_url() . "main_snowworld_v/getlocation/format/json/";
                                                $br_name = (array)curlpost($parameter, $path);
                                                echo($br_name[0]['branch_name']);
                                            ?></td>
                                            <td><?php
                                                $parameter = array('act_mode' => 'getlocation',
                                                'Param1' => $v->locationid,
                                                'Param2' => '',
                                                'Param3' => '',
                                                'Param4' => '',
                                                'Param5' => '',
                                                'Param6' => '',
                                                'Param7' => '',
                                                'Param8' => '',
                                                'Param9' => '');
                                                //p($parameter);
                                                $path = api_url() . "main_snowworld_v/getlocation/format/json/";
                                                $loc_name = curlpost($parameter, $path);
                                                $arr = (array)$loc_name;
                                                echo($arr[0]['locationname']);
                                                $expack = implode(',',(explode('#', $v->packproductname)));
                                            ?></td>
                                            <td><?php echo $v->total; ?></td>
                                            <td><?php echo $v->promocodeprice; ?></td>
                                            <td><?php echo $v->subtotal; ?></td>
                                            <td><?php echo $v->addedon; ?></td>
                                            <td><?php echo $v->departuredate; ?></td>
                                            <td><?php echo $v->departuretime; ?></td>
                                            <td><?php echo $v->paymentstatus; ?></td>
                                            <td><?php echo $v->paymentmode; ?></td>
                                            <td><?php echo $expack; ?></td>
                                            <td><?php echo $v->packpkg; ?></td>
                                            <td><?php echo $v->packprice; ?></td>
                                            <!--<td><?php /*echo $v->orderstatus; */ ?></td>-->
                                            <td><?php
                                                $ex = explode(',', $v->addon);
                                                $ex1 = explode(',', $v->addonquantity);
                                                $ex2 = explode(',', $v->orderaddon_print_status);
                                                $ex3 = explode(',', $v->orderaddon_print_quantity);
                                                $ex4 = explode(',', $v->addonprice);
                                                foreach ($ex as $key => $a) {
                                                if ($a == "") {
                                                echo "N/A";
                                                continue;
                                                } elseif ($ex1[$key] != $ex3[$key]) {
                                                echo '<button disabled="disabled" style="float: left; text-align: center" type="button" class="btn btn-danger" >PRINT ' . $a . ' (' . $ex1[$key] . ')</button><br>';
                                                } else {
                                                echo '<button style="float: left; text-align: center" type="button" class="btn btn-success" disabled="disabled">ALL ' . $a . ' PRINTED(' . $ex1[$key] . ')</button><br>';
                                                }
                                                }
                                            ?></td>
                                            <!-- <td><?php
                                                /*
                                                foreach ($ex1 as $a1) {
                                                if ($a == "") {
                                                echo "N/A";
                                                continue;
                                                }
                                                echo '<li>' . $a1 . '</li>';
                                                }
                                            */?></td>-->
                                            <!--<td><?php
                                                /*
                                                foreach($ex2 as $a2)
                                                { if($a == ""){echo "N/A";continue;}
                                                echo '<li>'.$a2.'</li>';
                                                }
                                            */ ?></td>-->
                                            <!-- <td><?php
                                                /*
                                                foreach($ex3 as $a3)
                                                { if($a == ""){echo "N/A";continue;}
                                                echo '<li>'.$a3.'</li>';
                                                }
                                            */ ?></td>-->
                                            <td><?php
                                                foreach ($ex4 as $a4) {
                                                if ($a == "") {
                                                echo "N/A";
                                                continue;
                                                }
                                                echo "<li>" . $a4 . "</li>";
                                                }
                                            ?></td>
                                            <td>
                                                <select data-id="<?= $v->pacorderid ?>" id="op_ticket_print_status" name="op_ticket_print_status" <?php if ($v->op_ticket_print_status == 2){echo "disabled";} ?>>
                                                    <option value="0"  <?php if ($v->op_ticket_print_status == 0){echo "selected";} ?>  >Pending</option>
                                                    <option value="1" <?php if ($v->op_ticket_print_status == 1){echo "selected";} ?>>Processing</option>
                                                    <option value="2" <?php if ($v->op_ticket_print_status == 2){echo "selected";} ?>>Printed</option>
                                                </select>
                                                <!--  <?php /*if ($v->op_ticket_print_status == 0){ */?>
                                                <button type="button"
                                                onclick="return ticket_print('<?/*= $v->pacorderid */?>')"
                                                class="btn btn-danger" disabled="disabled">NOT PRINTED YET
                                                </button>
                                                <?php /*}else{ */?>
                                                <button type="button" class="btn btn-success" disabled="disabled">
                                                Already Printed
                                                </button>
                                                --><?php /*} */?>
                                            </td>
                                        </tr>
                                        <?php $i++;
                                        } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$(function () {
$("#op_ticket_print_status").change(function () {
var a = $(this);
if($("#op_ticket_print_status").val() == 2)
{
a.attr('disabled',true);
}
//return false;
$.ajax({
url: '<?php echo base_url()?>admin/ordercontrol/op_ticket_print_status',
type: 'POST',
data: {'pacorderid': $(this).attr('data-id'),'op_ticket_print_status':$("#op_ticket_print_status").val()},
dataType: "JSON",
success: function(data){
//console.log(data.s[0].ticket_status);
if(data.s[0].ticket_status == 2)
{
a.attr('disabled',true);
}
//console.log(data1);
}
});
});
});
function ticket_print(a) {
//alert(a);
window.print();
}
function update_com(a, b) {
//alert(a +b);
$("#com_name_update").val(a);
$("#com_id").val(b);
$("#update_com").css('display', 'block');
$("#add_com").css('display', 'none');
$("#com_name_update").focus();
}
function UpdateCancel() {
$("#add_com").css('display', 'block');
$("#update_com").css('display', 'none');
return false;
}
</script>
</script>
<link; href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css"; rel="stylesheet">
<script; src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<link rel="stylesheet"
    href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css"></style>
    <script type="text/javascript"
    src="http://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript"
    src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script>
    $(document).ready(function(){
    $('#table1').dataTable();
    });
    </script>