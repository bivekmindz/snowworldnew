<!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->
<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery-1.9.1.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery.validate.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"
        crossorigin="anonymous"></script>
<?php //p($vieww); ?>
<!-- jQuery Form Validation code -->
<style>
    input.error {
        border: 1px solid red;
    }

    label.error {
        border: 0px solid red;
        color: red;
        font-weight: normal;
        display: inline;
    }

    /*.tab_ble{ width:100%; float:left;}
    .tab_ble table{ width:100%; float:left;}
    .tab_ble table tr{ width:100%; float:left;}*/
    .tab_ble table tr th {
        padding: 5px 35px 15px 35px;
        text-align: center;
    }

    .tab_ble table tr td {
        border: 1px solid #CCC;
        text-align: center;

        min-width: 100px;
    }
</style>


<meta name="viewport" content="width=device-width, initial-scale=1">


<div class="wrapper">

    <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">

                        <h2>ALL ORDER LISTING</h2>
                    </div>
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-12">
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?>
                                    <?php
                                    if ($this->session->flashdata('message')) {
                                        echo $this->session->flashdata('message');
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-12">
                            <div class="tab_ble">
                                <table id="table1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>S.NO</th>
                                        <th>Booking Type</th>
                                        <th>Username</th>
                                        <th>User Email</th>
                                        <th>User Contact</th>
                                        <th>No. of Peoples</th>
                                        <th>Social Group</th>
                                        <th>Social Group Name</th>
                                        <th>Date</th>
                                        <th>Branch</th>
                                        <th>Location</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i = 1;
                                    foreach ($vieww_order as $v) {
                                        if($this->session->userdata('snowworld')->EmployeeId) {
                                            if($this->session->userdata('snowworld')->LocationId != $v->locationid)
                                            {
                                                continue;
                                            }
                                        }?>
                                        <tr>
                                            <td><?= $i; ?></td>
                                            <td><?php echo $v->bookingType; ?></td>
                                            <td><?php echo $v->full_name; ?></td>
                                            <td><?php echo $v->email_address; ?></td>
                                            <td><?php echo $v->phone_number; ?></td>
                                            <td><?php echo $v->number_people; ?></td>
                                            <td><?php echo $v->socialGroupRadio; ?></td>
                                            <td><?php echo ($v->socialGroupName == "" ? "......" :  $v->socialGroupName); ?></td>
                                            <td><?php echo $v->addeddate; ?></td>
                                            <td><?php

                                                $parameter = array('act_mode' => 'getbranch',
                                                    'Param1' => $v->branch_id,
                                                    'Param2' => '',
                                                    'Param3' => '',
                                                    'Param4' => '',
                                                    'Param5' => '',
                                                    'Param6' => '',
                                                    'Param7' => '',
                                                    'Param8' => '',
                                                    'Param9' => '');
                                                //p($parameter);
                                                $path = api_url() . "main_snowworld_v/getlocation/format/json/";
                                                $br_name = (array)curlpost($parameter, $path);
                                                echo($br_name[0]['branch_name']);
                                                ?></td>
                                            <td><?php

                                                $parameter = array('act_mode' => 'getlocation',
                                                    'Param1' => $v->locationid,
                                                    'Param2' => '',
                                                    'Param3' => '',
                                                    'Param4' => '',
                                                    'Param5' => '',
                                                    'Param6' => '',
                                                    'Param7' => '',
                                                    'Param8' => '',
                                                    'Param9' => '');
                                                //p($parameter);
                                                $path = api_url() . "main_snowworld_v/getlocation/format/json/";
                                                $loc_name = curlpost($parameter, $path);
                                                $arr = (array)$loc_name;
                                                echo($arr[0]['locationname']);
                                                ?></td>

                                        </tr>

                                        <?php $i++;
                                    } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>


                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script>

    function funct_change_status(a,b) {
        if(b == 2) {
            $(".ticket" + a).attr('disabled', true);
        }
        $.ajax({
            url: '<?php echo base_url()?>admin/ordercontrol/op_ticket_print_status',
            type: 'POST',
            data: {'pacorderid': a,'op_ticket_print_status':b},
            dataType: "JSON",
            success: function(data){


//console.log(data.s[0].ticket_status);
                if(data.s[0].ticket_status == 2)
                {
                    a.attr('disabled',true);

                }
                //console.log(data1);
            }
        });
        //var b = $(this).val();
       // alert(b);return false;

    }

/*    $(function () {
        $("#op_ticket_print_status").change(function () {
            alert("hello"); return false;
            var a = $(this);
            if($("#op_ticket_print_status").val() == 2)
            {
                a.attr('disabled',true);

            }
            //return false;
            $.ajax({
                url: ' echo base_url()?>admin/ordercontrol/op_ticket_print_status',
                type: 'POST',
                data: {'pacorderid': $(this).attr('data-id'),'op_ticket_print_status':$("#op_ticket_print_status").val()},
                dataType: "JSON",
                success: function(data){


//console.log(data.s[0].ticket_status);
                    if(data.s[0].ticket_status == 2)
                    {
                        a.attr('disabled',true);

                    };
                   //console.log(data1);
                }
            });
        });
    });*/

    function ticket_print(a) {
        //alert(a);
        window.print();

    }

    function update_com(a, b) {
        //alert(a +b);
        $("#com_name_update").val(a);
        $("#com_id").val(b);
        $("#update_com").css('display', 'block');
        $("#add_com").css('display', 'none');
        $("#com_name_update").focus();

    }
    function UpdateCancel() {
        $("#add_com").css('display', 'block');
        $("#update_com").css('display', 'none');
        return false;
    }


</script>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<link rel="stylesheet"

      href="https://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css"></style>
<script type="text/javascript"

src="https://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
                                                                     <script type="text/javascript"

src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

                                                                           <script>
                                                                           $(document).ready(function(){
    $('#table1').dataTable();
});
</script>



