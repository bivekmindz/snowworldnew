<?php //$this->load->view('helper/nav'); ?>


<!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->
<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"
        crossorigin="anonymous"></script>
<?php //p($vieww); ?>
<!-- jQuery Form Validation code -->
<script>


    $(function () {
        // Initialize form validation on the registration form.
        // It has the name attribute "registration"
        $("#add_branch").validate({
            // Specify validation rules
            rules: {
                // The key name on the left side is the name attribute
                // of an input field. Validation rules are defined
                // on the right side
                countryid: "required",
                stateid: "required",
                cityid: "required",
                locationid: "required",
                companyid: "required",
                branch_name: "required",
                branch_logo_image: "required",
                mail_image_background: "required",
                branch_internethandle: "required",
                branch_url: "required",
                branch_addr: "required",
                cronejob: "required",
                hrsrestriction: "required",

            },
            // Specify validation error messages
            messages: {
                countryid: "Please select country",
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function (form) {

                var ext = $('#mail_image_background').val().split('.').pop().toLowerCase();
                if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
                    alert('Please Select Valid Image!');
                    return false;
                }
                else {
                    form.submit();
                }

            }
        });
    });


</script>
<style>
    input.error {
        border: 1px solid red;
    }

    label.error {
        border: 0px solid red;
        color: red;
        font-weight: normal;
        display: inline;
    }
    td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
    border-top: 1px solid #dddddd !important;
    border-bottom: 1px solid #dddddd !important;
}
.btn-primary {
    text-decoration: none;
    margin: 2px;
}
</style>





<!-- <div class="wrapper"> -->

    <div class="col-lg-10">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">

                        <h2>ADD BRANCH</h2>
                    </div>
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="sep_box">
                                <div class="col-lg-12">
                                    <div id="content">
<?php //echo $this->uri->segment(1) .'br';
//echo $this->uri->segment(2) .'br';  class="active"
//echo $this->uri->segment(3) ;
if($this->uri->segment(3)=='addTimeslotPackage')
{  $statusactive1='class="active"';}
if($this->uri->segment(3)=='addTimeslot')
{  $statusactive2='class="active"';}
if($this->uri->segment(3)=='addPackage')
{  $statusactive3='class="active"';}
if($this->uri->segment(3)=='addAddon')
{  $statusactive4='class="active"';}
if($this->uri->segment(3)=='gateway')
{  $statusactive5='class="active"';}
if($this->uri->segment(3)=='smsgateway')
{  $statusactive6='class="active"';}
if($this->uri->segment(3)=='tearmcondtion')
{  $statusactive7='class="active"';}
if($this->uri->segment(3)=='addbranch')
{  $statusactive8='class="active"';}
if($this->uri->segment(3)=='s_addCompany')
{  $statusactive9='class="active"';}
?>

<ul id="tabs" class="nav nav-tabs" data-tabs="tabs">

    <li <?php echo $statusactive1; ?>><a href="<?php echo base_url('admin/timeslotspackages/addTimeslotPackage?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid']));?>" >Manual Session Time</a>
    </li>

    <li <?php echo $statusactive2; ?>><a href="<?php  echo base_url('admin/timeslots/addTimeslot?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid']));?>" >Auto Session Time</a></li>

    <li <?php echo $statusactive3; ?>><a href="<?php echo base_url('admin/packages/addPackage?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid']));?>" >Package</a></li>
    <li <?php echo $statusactive4; ?>><a href="<?php echo  base_url('admin/addon/addAddon?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid'])); ?>" >Addon</a></li>
    <li <?php echo $statusactive5; ?>><a href="<?php echo  base_url('admin/Payment/gateway?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid'])); ?>" >Payment Gateway</a></li>

    <li <?php echo $statusactive6; ?>><a href="<?php echo  base_url('admin/Payment/smsgateway?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid'])); ?>" >Sms Gateway</a></li>

    <li <?php echo $statusactive7; ?>><a href="<?php echo  base_url('admin/Payment/tearmcondtion?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid'])); ?>" >Terms & Conditions</a></li>
    <li <?php echo $statusactive8; ?>><a href="<?php  echo base_url('admin/branchs/addbranch?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid']));?>" >Branch</a></li>


    <li <?php echo $statusactive9; ?>><a href="<?php echo base_url('admin/companys/s_addCompany?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid']));?>" >Company</a></li>






</ul>

                                    </div>
                                    <div class='flashmsg'>
                                        <?php echo validation_errors(); ?>
                                        <?php
                                        if($this->session->flashdata('message')){
                                            echo $this->session->flashdata('message');
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <form action="<?= base_url('admin/branchs/addbranch?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid'])) ?>" name="add_branch" id="add_branch" method="post" enctype="multipart/form-data" >

                         <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Select Country <span
                                                        style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <select name="countryid" id="countryid" onchange="selectstates();">
                                                    <option value="">select Country</option>
                                                    <?php foreach ($vieww as $key => $value) { ?>
                                                        <option value="<?php echo $value->conid; ?>"><?php echo $value->name; ?></option>
                                                    <?php } ?>
                                                </select></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Select State <span
                                                        style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <select id="stateid" name="stateid" value="stateid"
                                                        onchange="selectcity();">
                                                    <option value="">Select State</option>
                                                </select>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Select City <span
                                                        style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <select id="cityid" onchange="selectlocation()" name="cityid"
                                                        value="cityid">
                                                    <option value="">Select city</option>
                                                </select>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Select Location <span
                                                        style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <select id="locationid" name="locationid" value="locationid">
                                                    <option value="">Select Location</option>
                                                </select>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                            <script>
                                function selectstates() {

                                    var countryid = $('#countryid').val();
                                    $.ajax({
                                        url: '<?php echo base_url()?>admin/geographic/countrystate',
                                        type: 'POST',
                                        //dataType: 'json',
                                        data: {'countryid': countryid},
                                        success: function (data) {
                                            var option_brand = '<option value="">Select State</option>';
                                            $('#stateid').empty();
                                            $('#cityid').empty();
                                            $('#locationid').empty();
                                            $("#stateid").append(option_brand + data);

                                        }
                                    });

                                }
                                function selectcity() {

                                    var stateid = $('#stateid').val();
                                    $.ajax({
                                        url: '<?php echo base_url()?>admin/geographic/statecity',
                                        type: 'POST',
                                        //dataType: 'json',
                                        data: {'stateid': stateid},
                                        success: function (data) {
                                            var option_brand = '<option value="">Select City</option>';
                                            $('#cityid').empty();
                                            $('#locationid').empty();
                                            $("#cityid").append(option_brand + data);

                                        }
                                    });

                                }
                                function selectlocation() {
                                    var cityid = $('#cityid').val();
                                    $.ajax({
                                        url: '<?php echo base_url()?>admin/geographic/citylocation',
                                        type: "POST",
                                        data: {'cityid': cityid},
                                        success: function (data) {
                                            //console.log(data);
                                            var option_brand = '<option value="">Select Location</option>';
                                            $('#locationid').empty();
                                            $("#locationid").append(option_brand + data);

                                        }

                                    })

                                }
                            </script>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Select Company <span
                                                        style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <select name="companyid" id="companyid">
                                                    <option value="">select Company</option>
                                                    <?php foreach ($vieww_company as $key => $value) { ?>
                                                        <option value="<?php echo $value->comp_id; ?>"><?php echo $value->comp_name; ?></option>
                                                    <?php } ?>
                                                </select></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Branch Name<span style="color:red;font-weight: bold;">*</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" id="branch_name" name="branch_name">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                             <div class="sep_box">
                                  <div class="col-lg-6">
                                      <div class="row">
                                          <div class="col-lg-4">
                                              <div class="tbl_text"> Select LOGO <span
                                                          style="color:red;font-weight: bold;">*</span></div>
                                          </div>
                                          <div class="col-lg-8">
                                              <div class="tbl_input">
                                                  <input type="file" id="branch_logo_image"
                                                         accept="image/gif, image/jpeg, image/png, image/jpg"
                                                         name="branch_logo_image">
                                              </div>
                                          </div>
                                      </div>
                                      `
                                  </div>
                              </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Select Background Image <span
                                                        style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="file" id="mail_image_background"
                                                       accept="image/gif, image/jpeg, image/png, image/jpg"
                                                       name="mail_image_background">
                                            </div>
                                        </div>
                                    </div>
                                    `
                                </div>
                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Internet Handling Charges<span
                                                        style="color:red;font-weight: bold;">*</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="number" id="branch_internethandle"
                                                       name="branch_internethandle">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Url <span
                                                        style="color:red;font-weight: bold;">*</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" id="branch_url" name="branch_url">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Branch Address <span
                                                        style="color:red;font-weight: bold;">*</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <textarea type="text" id="branch_addr" name="branch_addr"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Crone job <span
                                                        style="color:red;font-weight: bold;">*</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <select name="cronejob" id="cronejob">
                                                    <option value="1" >Inactive</option>
                                                    <option value="2" >Active</option>

                                                </select>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Hrs Restriction<span
                                                        style="color:red;font-weight: bold;">*</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" name="hrsrestriction" id="hrsrestriction" >

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Min Restriction<span
                                                        style="color:red;font-weight: bold;">*</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" name="minrestriction" id="minrestriction" >

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Cron Job Run Value<span
                                                        style="color:red;font-weight: bold;">*</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" name="cronjobrunvalue" id="cronjobrunvalue" >

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-8">
                                            <div class="submit_tbl">
                                                <input id="submitBtn" type="submit" name="submit" value="Submit"
                                                       class="btn_button sub_btn"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </form>


                        <div class="col-md-12">
                        <div class="add_comp">
                            <h2>Table</h2>
                            <div class="table-responsive" style="width: 100%;">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>S.NO</th>
                                        <th>Branch Name</th>
                                        <th> BACKGROUND</th>
                                        <th>INTERNET CHARGES</th>
                                        <th>URL</th>
                                        <th>Branch Address</th>
                                        <th>Branch location</th>
                                        <th>Branch Company</th>
                                        <th width="150">Status/Delete</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i = 1;
                                    //p($s_viewbranch);
                                    foreach ($s_viewbranch as $v) {
                                        //pend($v);?>
                                        <tr>
                                            <td><?= $i; ?></td>
                                            <td id="t_act_name"><?php echo $v->branch_name; ?></td>
                                            <td id="t_act_name"><img height="100px" width="200px"
                                                                     src="<?php echo base_url('/assets/admin/images/' . $v->branch_background); ?>"
                                            </td>
                                            <td id="t_act_name"><?php echo $v->branch_internet_handling_charge; ?></td>
                                            <td id="t_act_name"><?php echo $v->branch_url; ?></td>
                                            <td id="t_act_name"><?php echo $v->branch_add; ?></td>
                                            <td id="t_act_desc"><?php


                                                $parameter = array('act_mode' => 'getlocation',
                                                    'Param1' => $v->branch_location,
                                                    'Param2' => '',
                                                    'Param3' => '',
                                                    'Param4' => '',
                                                    'Param5' => '',
                                                    'Param6' => '',
                                                    'Param7' => '',
                                                    'Param8' => '',
                                                    'Param9' => '');
                                                //p($parameter);
                                                $path = api_url() . "main_snowworld_v/getlocation/format/json/";
                                                $loc_name = curlpost($parameter, $path);
                                                //pend($loc_name);
                                                $arr = (array)$loc_name;
                                                echo($arr[0]['locationname']);


                                                ?></td>
                                            <td id="t_act_price"><?php


                                                $parameter1 = array('act_mode' => 'getcompany',
                                                    'Param1' => $v->branch_company,
                                                    'Param2' => '',
                                                    'Param3' => '',
                                                    'Param4' => '',
                                                    'Param5' => '',
                                                    'Param6' => '',
                                                    'Param7' => '',
                                                    'Param8' => '',
                                                    'Param9' => '');
                                                //p($parameter1);
                                                $path1 = api_url() . "main_snowworld_v/getlocation/format/json/";
                                                $com_name = curlpost($parameter1, $path1);
                                                //pend($com_name);
                                                $arr1 = (array)$com_name;
                                                echo($arr1[0]['comp_name']);


                                                //echo $v->branch_company;


                                                ?></td>
                                            <td>
                                                <a href="<?= base_url('admin/branchs/branchstatus/'. base64_encode($v->branch_id) .'/'.base64_encode($v->branch_status).'?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid']).'') ?>"   class="btn_status"><?php echo $v->branch_status == 0 ? "Inactive" : "Active"; ?></a> 

                                                <a href="<?= base_url('admin/branchs/branchdelete1/'.  $v->branch_id.'?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid'])) ?>"   onclick="return confirm('Are you sure you want to delete this item?');"  class="btn_delete">
 <span class="glyphicon glyphicon-trash"></span></a>


                                                <a href="<?= base_url('admin/branchs/addbranchupdate/'. base64_encode($v->branch_id).'?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid'])) ?>"    class="btn_edit">
                                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                                </a>


                                            </td>

                                        </tr>

                                        <?php $i++;
                                    } ?>
                                    </tbody>
                                </table>
                            </div>
                            </div>
                        </div>


                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


<div class="container">
    <!-- Trigger the modal with a button -->

    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Update Branch</h4>
                </div>
                <div class="modal-body">

                    <form action="<?= base_url('admin/branchs/updatebranch') ?>" name="add_branch" id="add_branch"
                          method="post" enctype="multipart/form-data">

                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Update Location <span
                                                    style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                            <input type="hidden" id="update_branch_id" name="update_branch_id">
                                            <select id="locationid_update" name="locationid_update">
                                                <!-- <option value="">Select Location</option>-->


                                                <?php

                                                $parameter2 = array('act_mode' => 'getlocation_update',
                                                    'Param1' => '',
                                                    'Param2' => '',
                                                    'Param3' => '',
                                                    'Param4' => '',
                                                    'Param5' => '',
                                                    'Param6' => '',
                                                    'Param7' => '',
                                                    'Param8' => '',
                                                    'Param9' => '');
                                                //p($parameter2);
                                                echo $path2 = api_url() . "main_snowworld_v/getlocation/format/json/";
                                                $l_name_update = curlpost($parameter2, $path2);
                                                //p($l_name_update);
                                                foreach ($l_name_update as $c) {
                                                    echo "<option value=" . $c['locationid'] . ">" . $c['locationname'] . "</option>";
                                                }


                                                ?>
                                            </select>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>

                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Update Company <span
                                                    style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                            <select name="companyid_update" id="companyid_update">
                                                <?php foreach ($vieww_company as $key => $value) { ?>
                                                    <option value="<?php echo $value->comp_id; ?>"><?php echo $value->comp_name; ?></option>
                                                <?php } ?>
                                            </select></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Branch Name<span
                                                    style="color:red;font-weight: bold;">*</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                            <input type="text" id="branch_name_update" style="width: 100%"
                                                   name="branch_name_update">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-8">
                                        <div class="submit_tbl">
                                            <input id="submitBtn" type="submit" name="submit" value="Update"
                                                   class="btn_button sub_btn backbtn"/>
                                                    <button id="backBtn" type="" name="submit" 
                                                       class="btn_button sub_btn backbtn"/> Back</div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                </div>


                </form>
                <div class="modal-footer">

                </div>
            </div>

        </div>
    </div>

</div>
<script>


    function update_branch(a) {
        $("#companyid_update").find("option").removeAttr('selected');
        $("#locationid_update").find("option").removeAttr('selected');
        //alert("hello");
        //var a = $(this).domElement();
        //console.log(a);
        $.ajax({
            url: '<?php echo api_url() . "main_snowworld_v/getlocation/format/json/" ?>',
            type: "POST",
            dataType: "json",
            data: {'act_mode': 'getbranch_update', 'Param1': a},
            success: function (data) {
                $("#update_branch_id").val(a);
                $("#branch_name_update").val(data[0].branch_name);
                $("#companyid_update").find("option[value='" + data[0].branch_company + "']").attr('selected', true);
                $("#locationid_update").find("option[value='" + data[0].branch_location + "']").attr('selected', true);
                return false;
            }

        })


    }


</script>