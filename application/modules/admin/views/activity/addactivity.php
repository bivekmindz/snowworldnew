<script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery-1.9.1.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js" crossorigin="anonymous"></script>





<!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->

<?php //p($vieww); ?>
<!-- jQuery Form Validation code -->
<script>

    $(function() {


        $("#add_act").validate({
            rules: {
                countryid: "required",
                stateid: "required",
                cityid: "required",
                locationid: "required",
                act_name: "required",
                act_desc: "required",
                act_price: {required: true,
                    number: true},
                act_image : "required",
            },
            messages: {
                act_name: "Please enter name",
                act_desc: "Please enter description",
                act_price: {required: "Enter price",
                    number :"Number Only"}

            },
            submitHandler: function(form) {
                var ext = $('#act_image').val().split('.').pop().toLowerCase();
                if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
                    alert('Please Select Valid Image!');
                    return false;
                }
                else {
                    form.submit();
                }
            }
        });



        $("#update_act").validate({
            rules: {
                act_name_update: "required",
                act_desc_update: "required",
                act_price_update: {required: true,
                    number: true}
            },
            messages: {
                act_name_update: "Please enter name",
                act_desc_update: "Please enter description",
                act_price_update: {required: "Enter price",
                    number :"Number Only"}
            },
            submitHandler: function(form) {
                form.submit();
            }
        });


    });


</script>
<style>
    input.error{border:1px solid red;}
    label.error{border:0px solid red; color:red; font-weight: normal; display:inline; }
</style>


<meta name="viewport" content="width=device-width, initial-scale=1">


<div class="wrapper">

    <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">

                        <h2>ADD Activity</h2>
                    </div>
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-12">
                                <div style="text-align:right;">
                                    <a href=""><button>CANCEL</button></a>
                                </div>
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?>
                                    <?php
                                    if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message');
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <form style="display: none" action="<?= base_url('admin/activity/addactivity') ?>" id="update_act" method="post" enctype="multipart/form-data" >
                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Update Activity Name<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" id="act_name_update" required name="act_name_update" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Update Activity Description<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <textarea  id="act_desc_update" name="act_desc_update" required ></textarea>
                                                <input type="hidden" name="act_id" id="act_id">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Update Activity Price<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" id="act_price_update" required name="act_price_update" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-8">
                                            <div class="submit_tbl">
                                                <input id="submitBtn_update" type="submit" name="submit_update" value="Update" class="btn_button sub_btn" />

                                                <input id="submitBtn_update" type="button" onclick="return UpdateCancel()" name="submit_update" value="Cancel" class="btn_button sub_btn" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </form>


                        <form action="<?= base_url('admin/activity/addactivity') ?>" name="add_act" id="add_act" method="post" enctype="multipart/form-data" >


                            <!--location start-->
                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Select Country <span
                                                        style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <select required name="countryid" id="countryid" onchange="selectstates();">
                                                    <option value="">select Country</option>
                                                    <?php foreach ($vieww_country as $key => $value) { ?>
                                                        <option value="<?php echo $value->conid; ?>"><?php echo $value->name; ?></option>
                                                    <?php } ?>
                                                </select></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Select State <span
                                                        style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <select required id="stateid" name="stateid" value="stateid"
                                                        onchange="selectcity();">
                                                    <option value="">Select State</option>
                                                </select>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Select City <span
                                                        style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <select required id="cityid" onchange="selectlocation()" name="cityid"
                                                        value="cityid">
                                                    <option value="">Select city</option>
                                                </select>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Select Location <span
                                                        style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <select required id="locationid" name="locationid" value="locationid">
                                                    <option value="">Select Location</option>
                                                </select>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                            <?php
                            if($this->session->userdata('snowworld')->EmployeeId){
                                ?>
                                <script>
                                    $(function(){
                                        $('#countryid option[value="<?= $this->session->userdata('snowworld')->countryid; ?>"]').attr('selected',true);
                                        $("#countryid").trigger("change");
                                        $("#countryid").attr('disabled',true);
                                    });
                                </script>

                                <?php
                            }
                            ?>
                            <script>
                                function selectstates() {

                                    var countryid = $('#countryid').val();
                                    $.ajax({
                                        url: '<?php echo base_url()?>admin/geographic/countrystate',
                                        type: 'POST',
                                        //dataType: 'json',
                                        data: {'countryid': countryid},
                                        success: function (data) {
                                            var option_brand = '<option value="">Select State</option>';
                                            $('#stateid').empty();
                                            $('#cityid').empty();
                                            $('#locationid').empty();
                                            $("#stateid").append(option_brand + data);
                                            <?php
                                            if($this->session->userdata('snowworld')->EmployeeId){
                                            ?>
                                            $(function(){
                                                $('#stateid option[value="<?= $this->session->userdata('snowworld')->stateid; ?>"]').attr('selected',true);
                                                $("#stateid").trigger("change");
                                                $("#stateid").attr('disabled',true);
                                            });
                            <?php
                            }
                            ?>

                                        }
                                    });

                                }
                                function selectcity() {

                                    var stateid = $('#stateid').val();
                                    $.ajax({
                                        url: '<?php echo base_url()?>admin/geographic/statecity',
                                        type: 'POST',
                                        //dataType: 'json',
                                        data: {'stateid': stateid},
                                        success: function (data) {
                                            var option_brand = '<option value="">Select City</option>';
                                            $('#cityid').empty();
                                            $('#locationid').empty();
                                            $("#cityid").append(option_brand + data);
                                            <?php
                                            if($this->session->userdata('snowworld')->EmployeeId){
                                            ?>
                                            $(function(){
                                                $('#cityid option[value="<?= $this->session->userdata('snowworld')->cityid; ?>"]').attr('selected',true);
                                                $("#cityid").trigger("change");
                                                $("#cityid").attr('disabled',true);
                                            });
                                            <?php
                                            }
                                            ?>

                                        }
                                    });

                                }
                                function selectlocation() {
                                    var cityid = $('#cityid').val();
                                    $.ajax({
                                        url: '<?php echo base_url()?>admin/geographic/citylocation',
                                        type: "POST",
                                        data: {'cityid': cityid},
                                        success: function (data) {
                                            //console.log(data);
                                            var option_brand = '<option value="">Select Location</option>';
                                            $('#locationid').empty();
                                            $("#locationid").append(option_brand + data);

                                            <?php
                                            if($this->session->userdata('snowworld')->EmployeeId){
                                            ?>
                                            $(function(){
                                                $('#locationid option[value="<?= $this->session->userdata('snowworld')->LocationId; ?>"]').attr('selected',true);
                                                $("#locationid").trigger("change");
                                                $("#locationid").attr('disabled',true);
                                            });
                                            <?php
                                            }
                                            ?>

                                        }

                                    })

                                }
                            </script>
                            <!--location end-->



                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Activity Name<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" required maxlength="20" id="act_name" name="act_name" >
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Activity Description<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <textarea required id="act_desc" name="act_desc" ></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Activity Price<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" required maxlength="5" id="act_price" name="act_price" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Activity Image<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="file" required id="act_image" name="act_image" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-8">
                                            <div class="submit_tbl">
                                                <input id="submitBtn" type="submit" name="submit" value="Submit" class="btn_button sub_btn" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </form>


                        <div class="col-md-12">
                            <h2>Table</h2>
                            <div class="table-responsive" style="width: 100%;">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>S.NO</th>
                                        <th>Activity NAME</th>
                                        <th>DESCRIPTION</th>
                                        <th>PRICE</th>
                                        <th>Location</th>
                                        <th>IMAGE</th>
                                        <th>EDIT/DELETE</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php /*p($vieww);*/ $i=1; foreach($vieww as $v) {
                                    if($this->session->userdata('snowworld')->EmployeeId) {
                                        if($this->session->userdata('snowworld')->LocationId != $v->activity_location)
                                        {
                                            continue;
                                        }
                                    }
                                    ?>
                                        <tr>
                                            <td><?= $i;?></td>
                                            <td id="t_act_name"><?php echo $v->activity_name; ?></td>
                                            <td id="t_act_desc"><?php echo $v->activity_discription; ?></td>
                                            <td id="t_act_price"><?php echo $v->activity_price; ?></td>
                                            <td><?php

                                                $parameter = array( 'act_mode'=>'getlocation',
                                                    'Param1'=>$v->activity_location,
                                                    'Param2'=>'',
                                                    'Param3'=>'',
                                                    'Param4'=>'',
                                                    'Param5'=>'',
                                                    'Param6'=>'',
                                                    'Param7'=>'',
                                                    'Param8'=>'',
                                                    'Param9'=>'');
                                                $path = api_url()."main_snowworld_v/getlocation/format/json/";
                                                $loc_name = curlpost($parameter,$path);
                                                $arr = (array)$loc_name;
                                                echo($arr[0]['locationname']);
                                                ?></td>
                                            <td id="t_act_image">

                                                <img height="100px;" src="<?= base_url('assets/admin/images')."/".$v->activity_image;?> "
                                                <?php echo $v->activity_image; ?></td>
                                            <td>

                                                <a class="btn btn-primary" onclick="return update_act('<?php echo $v->activity_name; ?>','<?php echo $v->activity_discription; ?>','<?php echo $v->activity_price; ?>','<?php echo $v->activity_id; ?>')">
                                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                                </a>




                                                <a href="<?= base_url('admin/activity/activity_delete_new')."/".$v->activity_id ?>"  onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger btn">
                                                    <span class="glyphicon glyphicon-trash"></span>
                                                </a>







                                            </td>

                                        </tr>

                                        <?php $i++;} ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>




                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script>

    function update_act(a,b,c,d){
        //alert(a +b +c +d);return false;
        $("#act_name_update").val(a);
        $("#act_desc_update").val(b);
        $("#act_price_update").val(c);
        $("#act_id").val(d);
        $("#update_act").css('display','block');
        $("#add_act").css('display','none');
        $("#act_name_update").focus();

    }
    function UpdateCancel() {
        $("#add_act").css('display','block');
        $("#update_act").css('display','none');
        return false;
    }





</script>