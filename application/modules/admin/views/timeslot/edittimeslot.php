<?php //pend($s_viewtimeslot); ?><!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<style>
input.error {
border: 1px solid red;
}

label.error {
border: 0px solid red;
color: red;
font-weight: normal;
display: inline;
}

.to {
margin-right: 10px;
}

.to_input {
width: 67% !important;
}

.fonrr {
font-size: 26px;
margin-left: -14px;
color: rgba(0, 0, 0, 0.43);
margin-top: 7px;
}

.tbl_input {
margin-bottom: 15px;
}

.table_add {
width: 100%;
float: left;
}

.timebox{ width:100%; float:left; font-size:14px; color:#777676;}

.timebox button, input, select, textarea {padding: 3px 5px 3px 7px;
border: 1px solid rgba(0, 0, 0, 0.15);
margin: 3px 9px 13px 9px;
}

.branche_time{ width:100%; float:left;}

.branche_time li{padding: 3px 0px 12px 0px;}

.branche_time li button{    padding: 8px 20px 8px 20px; margin-left: 15px; font-size:20px;}
.backbtn{ margin: 3px 3px 0 0; display: inline-block; }

</style>


<meta name="viewport" content="width=device-width, initial-scale=1">


<div class="wrapper">

<div class="col-lg-10 col-lg-push-2">
<div class="row">
<div class="page_contant">
<div class="col-lg-12">
<div class="page_name">

<h2>ADD TIMESLOT</h2>
</div>
<div class="page_box">
<div class="sep_box">
<div class="col-lg-12">
<div id="content">

</div>
<div class='flashmsg'>
<?php echo validation_errors(); ?>
<?php
if($this->session->flashdata('message')){
echo $this->session->flashdata('message');
}
?>
</div>
</div>
</div>
<form action="<?= base_url('admin/timeslots/timeslotedit?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid'])) ?>" nname="add_com" id="add_com"method="post" enctype="multipart/form-data" >



<div class="sep_box">
<div class="col-lg-6">
<div class="row">
<div class="col-lg-4">
<div class="tbl_text">Select Branch <span
style="color:red;font-weight: bold;">*</span></div>
</div>
<div class="col-lg-8">
<div class="tbl_input">




<?php
if($this->session->userdata('snowworld')->EmployeeId){
?>
<script>
$(function(){
$('#branchids option[value="<?= $this->session->userdata('snowworld')->branchid; ?>"]').attr('selected',true);
$("#branchids").attr('disabled',true);
//$("#branchids").attr('name',<?= $this->session->userdata('snowworld')->branchid;?>);
});
</script>

<?php
}
?>


<select name="branchids" id="branchids">
<?php foreach ($s_viewbranch as $key => $value) { ?>
<option value="<?php echo $value->branch_id; ?>" <?php if($s_viewtimeslot->timeslot_branchid==$s_viewtimeslot->branch_id) { ?> selected<?php } ?>><?php echo $value->branch_name; ?></option>
<?php } ?>
</select></div>
</div>
</div>
</di v>
</div>
<div class="sep_box">
<div class="col-lg-12">
<div class="row">
<div class="col-lg-2">
<div class="tbl_text">Timeslot<span
style="color:red;font-weight: bold;">*</span></div>
</div>


<div class="col-md-9">

<div class="row field_wrapper">

<div class="timebox">FROM:


<select name="field_name1[]" id="field_name1" ><?php for($i=1;$i<=24;$i++){?><option value="<?php echo $i; ?>" <?php if($i==$s_viewtimeslot->timeslot_from) {?> selected<?php } ?>><?php echo $i; ?></option><?php } ?></select>
<select name="minute_name1[]" id="minute_name1" ><?php for($i=0;$i<=60;$i++){?><option value="<?php echo str_pad($i,2,"0",STR_PAD_LEFT); ?>" <?php if(str_pad($i,2,"0",STR_PAD_LEFT)==$s_viewtimeslot->timeslot_minfrom) {?> selected<?php } ?>><?php echo str_pad($i,2,"0",STR_PAD_LEFT);?> </option><?php } ?></select>
TO:
<select name="field_name2[]" id="field_name2" ><?php for($i=1;$i<=24;$i++){?><option value="<?php echo $i; ?>" <?php if($i==$s_viewtimeslot->timeslot_to) {?> selected<?php } ?>><?php echo $i; ?></option><?php } ?></select>
<select name="minute_name2[]" id="minute_name2" ><?php for($i=0;$i<=60;$i++){ ?><option value="<?php echo str_pad($i,2,"0",STR_PAD_LEFT); ?>" <?php if(str_pad($i,2,"0",STR_PAD_LEFT)==$s_viewtimeslot->timeslot_minto) {?> selected<?php } ?>><?php echo str_pad($i,2,"0",STR_PAD_LEFT);?> </option> <?php } ?></select>
<input id="no_of_seats" name="no_of_seats[]" placeholder="Seats available" type="number" min="0" max="1000" value="<?php echo $s_viewtimeslot->timeslot_seats; ?>">


<!-- <select name="packages_name[]" id="minute_name2"  multiple="multiple" >
<?php  foreach ($vieww_pack as $v) {
?>
<option value="<?php echo $v->package_id; ?>" <?php if($s_viewtimeslot->timeslot_packageid==$v->package_id) { ?> selected <?php } ?>><?php echo $v->package_name; ?></option>
<?php } ?>
</select> -->

<select id="packages" name="packages_name[]"  multiple="multiple" required>
<?php 
$edit_packages=array();
$add=explode(",",$s_viewtimeslot->timeslot_packageid);
foreach ($add as $key => $value_packages) {
$edit_packages[]=$value_packages;
}
$select_packages=array();
foreach ($vieww_pack as $key => $value) {
array_push($select_packages,$value->package_id);
}
$result = array_intersect($edit_packages, $select_packages); 
?>
<?php  foreach ($vieww_pack as $key => $value) {
?>
<option value="<?php echo $value->package_id;?>"
<?php 
if (in_array($value->package_id,$result)){ 
echo 'selected="selected"';} ?>><?php echo $value->package_name;?>
</option><?php  }?>
  </select>






<select id="addon" name="addon[]"  multiple=”multiple” required>
<?php 
$edit_addon=array();
$add=explode(",",$s_viewtimeslot->timeslot_addonsid);
foreach ($add as $key => $value_addon) {
$edit_addon[]=$value_addon;
}
$select_addon=array();
foreach ($addon_view as $key => $value) {
array_push($select_addon,$value->addon_id);
}
$result = array_intersect($edit_addon, $select_addon); 
?>
<?php  foreach ($addon_view as $key => $value) {
?>
<option value="<?php echo $value->addon_id;?>"
<?php 
if (in_array($value->addon_id,$result)){ 
echo 'selected="selected"';} ?>><?php echo $value->addon_name;?>
</option><?php  }?>
  </select>

<input type="hidden" name="updateid" value="<?php echo base64_decode($_GET['eid']); ?>">
</div>
</div>
</div>

</div>
</div>
</div>

<div class="sep_box">
<div class="col-lg-6">
<div class="row">
<div class="col-lg-4"></div>
<div class="col-lg-8">
<div class="submit_tbl">
<input id="submitBtn" type="submit" onclick='
$("#branchids").attr("disabled",false)' name="submit" value="Submit"
class="btn_button sub_btn"/>
<a href="<?= base_url('admin/timeslots/addTimeslot?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid'])) ?>" style="color:#fff;text-decoration:none;">
    <button id="backBtn" type="" name="submit" class="btn_button sub_btn backbtn"/>Back</a>

</div>
</div>
</div>
</div>
</div>
</div>
</form>




</div>

</div>
</div>
</div>
</div>
</div>
<link href="<?php echo base_url()?>assets/js/select2.min.css" rel="stylesheet" />
    <script src="<?php echo base_url()?>assets/js/select2.min.js"></script>   
    <script type="text/javascript"> 
     $('#addon').select2();
     $('#packages').select2();
     
     
    </script>