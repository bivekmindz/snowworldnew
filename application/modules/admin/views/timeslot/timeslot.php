<?php //pend($s_viewtimeslot); ?><!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<style>
input.error {
border: 1px solid red;
}
  table {
  border: 1px solid #ccc;
  border-collapse: collapse;
  table-layout: fixed;
  width: 100%;
}
     td, th {
   border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
    border-top: 1px solid #dddddd !important;
    border-bottom: 1px solid #dddddd !important;
          vertical-align: top !important;
          word-break: break-all;

}
ul.branche_time{ padding: 0px; list-style-type: none; }
.branche_time li span{ display: inline-block; }
.time-seat{ float: left; line-height: 35px; }
.btn-anchor{ float: right; }
label.error {
border: 0px solid red;
color: red;
font-weight: normal;
display: inline;
}

.to {
margin-right: 10px;
}

.to_input {
width: 67% !important;
}
button {
    background: #1870BB;
    border: none;
    color: #fff;
    padding: 6px 15px 11px 15px;
    font-size: 13px;
    border-radius: 4px;
}
.fonrr {
font-size: 26px;
margin-left: -14px;
color: rgba(0, 0, 0, 0.43);
margin-top: 7px;
}

.tbl_input {
margin-bottom: 15px;
}

.table_add {
width: 100%;
float: left;
}

.timebox{ width:100%; float:left; font-size:14px; color:#777676;}

.timebox button, input, select, textarea {padding: 3px 5px 3px 7px;
border: 1px solid rgba(0, 0, 0, 0.15);
margin: 3px 9px 13px 9px;
}

.branche_time{ width:100%; float:left;}

.branche_time li{padding: 3px 0px 12px 0px;  width: 100%; float: left;}

.branche_time li button{ margin-left: 15px; }

</style>




<!-- <div class="wrapper"> -->

<div class="col-lg-10">
<div class="row">
<div class="page_contant">
<div class="col-lg-12">
<div class="page_name">

<h2>ADD TIMESLOT</h2>
</div>
<div class="page_box">
<div class="sep_box">
<div class="col-lg-12">
<div id="content">
    <?php //echo $this->uri->segment(1) .'br';
//echo $this->uri->segment(2) .'br';  class="active"
//echo $this->uri->segment(3) ;
if($this->uri->segment(3)=='addTimeslotPackage')
{  $statusactive1='class="active"';}
if($this->uri->segment(3)=='addTimeslot')
{  $statusactive2='class="active"';}
if($this->uri->segment(3)=='addPackage')
{  $statusactive3='class="active"';}
if($this->uri->segment(3)=='addAddon')
{  $statusactive4='class="active"';}
if($this->uri->segment(3)=='gateway')
{  $statusactive5='class="active"';}
if($this->uri->segment(3)=='smsgateway')
{  $statusactive6='class="active"';}
if($this->uri->segment(3)=='tearmcondtion')
{  $statusactive7='class="active"';}
if($this->uri->segment(3)=='addbranch')
{  $statusactive8='class="active"';}
if($this->uri->segment(3)=='s_addCompany')
{  $statusactive9='class="active"';}
?>

<ul id="tabs" class="nav nav-tabs" data-tabs="tabs">

    <li <?php echo $statusactive1; ?>><a href="<?php echo base_url('admin/timeslotspackages/addTimeslotPackage?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid']));?>" >Manual Session Time</a>
    </li>

    <li <?php echo $statusactive2; ?>><a href="<?php  echo base_url('admin/timeslots/addTimeslot?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid']));?>" >Auto Session Time</a></li>

    <li <?php echo $statusactive3; ?>><a href="<?php echo base_url('admin/packages/addPackage?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid']));?>" >Package</a></li>
    <li <?php echo $statusactive4; ?>><a href="<?php echo  base_url('admin/addon/addAddon?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid'])); ?>" >Addon</a></li>
    <li <?php echo $statusactive5; ?>><a href="<?php echo  base_url('admin/Payment/gateway?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid'])); ?>" >Payment Gateway</a></li>

    <li <?php echo $statusactive6; ?>><a href="<?php echo  base_url('admin/Payment/smsgateway?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid'])); ?>" >Sms Gateway</a></li>

    <li <?php echo $statusactive7; ?>><a href="<?php echo  base_url('admin/Payment/tearmcondtion?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid'])); ?>" >Terms & Conditions</a></li>
    <li <?php echo $statusactive8; ?>><a href="<?php  echo base_url('admin/branchs/addbranch?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid']));?>" >Branch</a></li>


    <li <?php echo $statusactive9; ?>><a href="<?php echo base_url('admin/companys/s_addCompany?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid']));?>" >Company</a></li>






</ul>

</div>
<div class='flashmsg'>
<?php echo validation_errors(); ?>
<?php
if($this->session->flashdata('message')){
echo $this->session->flashdata('message');
}
?>
</div>
</div>
</div>

<form action="<?= base_url('admin/timeslots/addTimeslot?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid'])) ?>" name="add_com" id="add_com"method="post" enctype="multipart/form-data" >





<div class="sep_box">
<div class="col-lg-6">
<div class="row">
<div class="col-lg-4">
<div class="tbl_text">Select Branch <span
style="color:red;font-weight: bold;">*</span></div>
</div>
<div class="col-lg-8">
<div class="tbl_input">




<?php
if($this->session->userdata('snowworld')->EmployeeId){
?>
<script>
$(function(){
$('#branchids option[value="<?= $this->session->userdata('snowworld')->branchid; ?>"]').attr('selected',true);
$("#branchids").attr('disabled',true);
//$("#branchids").attr('name',<?= $this->session->userdata('snowworld')->branchid;?>);
});
</script>

<?php
}
?>



<select name="branchids" id="branchids">
<?php foreach ($s_viewbranch as $key => $value) { ?>
<option value="<?php echo $value->branch_id; ?>"><?php echo $value->branch_name; ?></option>
<?php } ?>
</select></div>
</div>
</div>
</di v>
</div>
<div class="sep_box">
<div class="col-lg-12">
<div class="row">
<div class="col-lg-2">
<div class="tbl_text">Timeslot<span
style="color:red;font-weight: bold;">*</span></div>
</div>


<div class="col-md-9">

<div class="row field_wrapper">

<div class="timebox">FROM:
<select name="field_name1[]" id="field_name1" ><?php for($i=1;$i<=24;$i++){echo "<option>".$i."</option>";} ?></select>
<select name="minute_name1[]" id="minute_name1" ><?php for($i=0;$i<=60;$i++){echo "<option>".str_pad($i,2,"0",STR_PAD_LEFT)."</option>";} ?></select>
TO:
<select name="field_name2[]" id="field_name2" ><?php for($i=1;$i<=24;$i++){echo "<option>".$i."</option>";} ?></select>
<select name="minute_name2[]" id="minute_name2" ><?php for($i=0;$i<=60;$i++){echo "<option>".str_pad($i,2,"0",STR_PAD_LEFT)."</option>";} ?></select>



<select id="package" name="package0[]"  multiple=”multiple” required>
	<?php  foreach ($vieww_pack as $key => $value) {?>
		 <option value="<?php echo $value->package_id;?>"><?php echo $value->package_name;?></option>
	<?php }?>
	</select>

<select id="addon" name="addon0[]"  multiple=”multiple” required>
  <?php  foreach ($addon_view as $key => $value) {?>
     <option value="<?php echo $value->addon_id;?>"><?php echo $value->addon_name;?></option>
  <?php }?>
  </select>
<input id="no_of_seats" name="no_of_seats[]" placeholder="Seats available" type="number" min="0" max="1000"><a href="javascript:void(0);" class="add_button" title="Add field">ADD</a>
</div>
</div>
</div>

</div>
</div>
</div>
<link href="<?php echo base_url()?>assets/js/select2.min.css" rel="stylesheet" />
    <script src="<?php echo base_url()?>assets/js/select2.min.js"></script>   
    <script type="text/javascript"> 
    $('#addon').select2();
     $('#package').select2();
    </script>
<script type="text/javascript">
$(document).ready(function(){
var maxField = 20; //Input fields increment limitation
var addButton = $('.add_button'); //Add button selector
var wrapper = $('.field_wrapper'); //Input field wrapper
 //New input field html
var x = 1; //Initial field counter is 1
$(addButton).click(function(){ //Once add button is clicked
if(x < maxField){ //Check maximum number of input fields

var jj=x;
x++;

 //Increment field counter




var fieldHTML = '<div class="timebox">FROM<select name="field_name1[]" id="field_name1" ><?php for($i=1;$i<=24;$i++){echo "<option>".$i."</option>";} ?></select><select name="minute_name1[]" id="minute_name1" ><?php for($i=0;$i<=60;$i++){echo "<option>".str_pad($i,2,"0",STR_PAD_LEFT)."</option>";} ?></select>TO:<select name="field_name2[]" id="field_name2" ><?php for($i=1;$i<=24;$i++){echo "<option>".$i."</option>";} ?></select> <select name="minute_name2[]" id="minute_name2" ><?php for($i=0;$i<=60;$i++){echo "<option>".str_pad($i,2,"0",STR_PAD_LEFT)."</option>";} ?></select><select id="package'+jj+'" name="package'+jj+'[]" multiple=”multiple” ><?php  foreach ($vieww_pack as $key => $value) {?><option value="<?php echo $value->package_id;?>"><?php echo $value->package_name;?></option><?php }?></select><select id="addon'+jj+'" name="addon'+jj+'[]" multiple=”multiple” required><?php foreach ($addon_view as $key => $value) {?><option value="<?php echo $value->addon_id;?>"><?php echo $value->addon_name;?></option><?php }?></select><input id="no_of_seats" name="no_of_seats[]" placeholder="Seats available" type="number" min="0" max="1000"><a href="javascript:void(0);" class="remove_button" title="Remove field">REMOVE</a></div>';
$(wrapper).append(fieldHTML); // Add field html
}
$('#addon'+jj).select2();
 $('#package'+jj).select2();
});
$(wrapper).on('click', '.remove_button', function(e){ //Once remove button is clicked
e.preventDefault();
$(this).parent('div').remove(); //Remove field html
x--; //Decrement field counter
});
});
</script>

<div class="sep_box">
<div class="col-lg-6">
<div class="row">
<div class="col-lg-4"></div>
<div class="col-lg-8">
<div class="submit_tbl">
<input id="submitBtn" type="submit" onclick='
$("#branchids").attr("disabled",false)' name="submit" value="Submit"
class="btn_button sub_btn"/>


</div>
</div>
</div>
</div>

</div>
</form>


<div class="col-md-12 refreshtime" >

<div class="table-responsive" style="width: 100%;">
<table class="table">
<thead>
<tr>
<th width="50">S.NO</th>
<th>Branch NAME</th>
<th>Branch TIME SLOT</th>


</tr>
</thead>
<tbody>
<?php $i = 1;
//p($s_viewtimeslot);
foreach ($s_viewtimeslot as $v) {

if($this->session->userdata('snowworld')->EmployeeId) {
if($this->session->userdata('snowworld')->branchid != $v->branch_id)
{
continue;
}
}

?>
<tr>
<td><?= $i; ?></td>
<td id="t_com_name"><?php echo $v->branch_name; ?></td>
<td>
<?php
if (empty($v->timeslotfrom)) {
echo "NO TIME IN LIST";
}

else{
$expid = explode(',', $v->timeslot_id);
$expfrom = explode(',', $v->timeslotfrom);
$expfrom_minute = explode(',', $v->timeslot_minfrom);
//p($expfrom_minute);
$expto = explode(',', $v->timeslotto);
$expto_minute = explode(',', $v->timeslot_minto);
$timeslotseats = explode(',', $v->timeslotseats);
echo "<ul class='branche_time'>";


foreach ($expfrom as $val => $a) {
echo '<li><span class="time-seat">' . $a . ":".$expfrom_minute[$val]."-to-" . $expto[$val] . ":".$expto_minute[$val]." 
 (Seats Available  = ".$timeslotseats[$val].')</span> <span class="btn-anchor">'." <button onclick='return deletesingletime(" . $v->branch_id . "," . $a . "," . $expto[$val] . ")'>-</button>&nbsp;&nbsp;<a class='btn btn-primary'  id='updatePackageButton' href='timeslotedit?eid=".base64_encode($expid[$val])."&empid=".$_GET['empid']."&uid=".str_replace(".html","",$_GET['uid'])."' ><i class='fa fa-pencil' aria-hidden='true'></i>
</a><span></li>";

}
echo "</ul>";
}

?>

</td>

</tr>

<?php $i++;
} ?>
</tbody>
</table>
</div>
</div>


</div>

</div>
</div>
</div>
</div>
</div>
<script>
function deletesingletime(id,from,to) {
var cou = confirm("ARE YOU WANT TO DELETE COMPLETELY?");
if(cou == true) {
$.ajax({
type: "POST",
url: "<?php echo base_url();?>admin/timeslots/deletesingletime",
data: {
'id': id,
'from': from,
'to': to
},
success: function (data) {
$(".refreshtime").load(location.href + " .refreshtime");
return false;
},
error: function (data) {
// alert(data);
}
});
return false;
}

}
</script>