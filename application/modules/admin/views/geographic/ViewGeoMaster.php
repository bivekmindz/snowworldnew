<?php


//pend($this->session->all_userdata());
//pend($viewcity);
//pend($data);
/*pend($viewcountry);*/ ?>
<script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery.validate.min.js"></script>

<!-- <div class="wrapper"> -->
<style>
.error {
color: red;

}

.tab-pane h4 b {
color: #1b78c7;
}

.modal-body {
width: 100%;
float: left;
position: relative;
}
</style>

<div class="col-lg-10 ">


<div class="row">

<div class="page_contant">


<div class="col-lg-12">
    <div class="page_name">

        <h2>Manage Geographic</h2>

    </div>

    <div class="page_box">

        <div class="col-lg-12">

            <div id="content">
                <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
                    <li class="active"><a href="#country" data-toggle="tab">Country</a></li>
                    <li><a href="#state" data-toggle="tab">State</a></li>
                    <li><a href="#city" data-toggle="tab">City</a></li>
                    <li><a href="#location" data-toggle="tab">Location</a></li>
                </ul>
                <div id="my-tab-content" class="tab-content">




<!----------------------------------------------------------------------------------------------------------------------------->




                    <div class="tab-pane active" id="country">
                        <h4 class="text-center"><b>COUNTRY PANEL</b></h4>
                        <div class="form-conte">
                            <!--  session flash message  -->
                            <div class='flashmsg'>
                                <?php echo validation_errors(); ?>
                                <?php
                                if ($this->session->flashdata('message')) {
                                    echo $this->session->flashdata('message');
                                }
                                ?>
                            </div>
                            <!--country form start-->
                            <form id="addCont" action="" method="post" enctype="multipart/form-data">

                                <div style="color: #1b6d57" class="text-center" id="msgdiv"></div>
                                <div class="sep_box">
                                    <div class="col-lg-6">

                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="tbl_text">Country Name <span
                                                            style="color:red;font-weight: bold;">*</span>
                                                </div>
                                            </div>
                                            <div class="col-lg-8">
                                                <div class="tbl_input"><input type="text" name="couname"
                                                                              id="couname"
                                                                              onblur="checkname();"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="sep_box">
                                    <div class="col-lg-6">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="tbl_text">Country Code <span
                                                            style="color:red;font-weight: bold;">*</span>
                                                </div>
                                            </div>
                                            <div class="col-lg-8">
                                                <div class="tbl_input"><input id="coucode" type="text"
                                                                              name="coucode"/></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="sep_box">
                                    <div class="col-lg-6">
                                        <div class="row">
                                            <div class="col-lg-4"></div>
                                            <div class="col-lg-8">
                                                <div class="submit_tbl">
                                                    <input id="submitBtn" type="submit" name="submit"
                                                           value="Submit" class="btn_button sub_btn"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </form>
                            <!--country form end-->

                            <!--country listing start-->
                            <div id="countrylist">
                                <table id="table1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>S:NO</th>
                                        <th>Country Name</th>
                                        <th>Country Code</th>
                                        <th>Action</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i = 0;
                                    foreach ($vieww as $key => $value) {
                                        $i++;
                                        ?>

                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo $value->name; ?></td>
                                            <td><?php echo $value->code; ?></td>
                                            <td>
                                                <a class="btn_edit" onclick="return edit_country('<?= $value->name ?>','<?= $value->code ?>','<?= $value->conid ?>')"
                                                   data-toggle="modal" data-target="#myModal"><i
                                                            class="fa fa-pencil"></a></i>


                                                <a class="btn_delete" onclick="return RemoveCompany('<?php echo $value->conid ?>')"><i
                                                            class="fa fa-trash"></i></a>


                                                <a class="btn_status" onclick="return StatusCompany('<?php echo $value->conid ?>','<?php echo $value->counstatus; ?>')"><?php if ($value->counstatus == '0') { ?>Inactive<?php } else { ?>Active<?php } ?></a>

                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <!--country listing close-->

                            <!--script of country start-->
                            <script type="text/javascript">

                                $(document).ready(function () {
                                    $("#addCont").validate({
                                        rules: {
                                            couname: "required",
                                            coucode: "required",
                                        },
                                        messages: {
                                            couname: "Country name required",
                                            coucode: "Country code required",
                                        },

                                        submitHandler: function (form) {
                                            var formData = new FormData($("#addCont").get(0));
                                            $.ajax({
                                                type: "POST",
                                                url: "<?php echo base_url();?>admin/geographic/add_country",
                                                data: formData,
                                                cache: false,
                                                contentType: false,
                                                processData: false,
                                                success: function (data) {
                                                    $("#countrylist").load(location.href + " #countrylist");
                                                    $("#msgdiv").html('inserted sucessfully');
                                                    $("#couname").val('');
                                                    $("#coucode").val('');
                                                },
                                                error: function (data) {
                                                    // alert(data);
                                                }
                                            });
                                            //form.submit();
                                        }
                                    });
                                });

                                function checkname() {
                                    var name = $('#couname').val();
                                    $.ajax({
                                        url: '<?php echo base_url()?>admin/geographic/checkname',
                                        type: 'POST',
                                        dataType: 'json',
                                        data: {'name': name},
                                        success: function (data) {
                                            if (data[0].count > 0) {
                                                $('#submitBtn').prop('disabled', 'disabled');
                                                alert("Data Already Existed");
                                            } else {
                                                $('#submitBtn').removeAttr('disabled');
                                            }
                                        }
                                    });

                                }


                                function RemoveCompany(a) {
                                    if (confirm("Are you sure to delete?") == true) {
                                        $.ajax({
                                            type: "POST",
                                            url: "<?php echo base_url()?>admin/geographic/countrydel_new/" + a,
                                            cache: false,
                                            contentType: false,
                                            processData: false,
                                            success: function (data) {
                                                // alert(data);
                                                $("#countrylist").load(location.href + " #countrylist");
                                                $("#msgdiv").html('deleted sucessfully');
                                            },
                                            error: function (data) {
                                                // alert(data);
                                            }
                                        });
                                    } else {
                                        return false;
                                    }
                                }


                                function edit_country(a, s, d) {
                                    //alert(a +s+d);
                                    $("#update_couname").val(a);
                                    $("#update_coucode").val(s);
                                    $("#update_id").val(d);
                                }
                                function editState(a, s, d, f) {
                                    $("#countryid_update").find("option").removeAttr('selected');
                                   var vaa = $("#countryid_update").find("option[value='"+a+"']").attr('selected',true);
                                    $("#statename_update").val(s);
                                    $("#statecode_update").val(d);
                                    $("#update_id_forstate").val(f);
                                }

                                function StatusCompany(a, b) {
                                    /*alert(a);
                                     alert(b);*/
                                    if (confirm("Are you sure to change status?") == true) {
                                        $.ajax({
                                            type: "POST",
                                            url: "<?php echo base_url()?>admin/geographic/countrystatus/" + a + "/" + b,
                                            cache: false,
                                            contentType: false,
                                            processData: false,
                                            success: function (data) {
                                                //alert(data);
                                                $("#countrylist").load(location.href + " #countrylist");
                                                $("#msgdiv").html('status change sucessfully');
                                            },
                                            error: function (data) {
                                                // alert(data);
                                            }
                                        });
                                    } else {
                                        return false;
                                    }
                                }


                                function update_submit_country() {
                                    var d = $("#update_id").val();
                                    //console.log({'name': $("#update_couname").val(),'code':$("#update_coucode").val(),'id' :d});
                                    $.ajax({
                                        type: "POST",
                                        url: "<?php echo base_url();?>admin/geographic/countryupdate",
                                        data: {
                                            'name': $("#update_couname").val(),
                                            'code': $("#update_coucode").val(),
                                            'id': d
                                        },
                                        success: function (data) {
                                            // alert(data);
                                            $("#countrylist").load(location.href + " #countrylist");
                                            $("#msgdiv").html('updated sucessfully');
                                            $("#myModal").modal('hide');
                                        },
                                        error: function (data) {
                                            // alert(data);
                                        }
                                    });

                                }

                                function update_submit_state() {
                                    var d = $("#update_id_forstate").val();
                                   /* console.log({
                                    'cid': $("#countryid_update").val(),
                                        'sid': $("#statename_update").val(),
                                        'scode': $("#statecode_update").val(),
                                        'id': d
                                    })*/
                                    $.ajax({
                                        type: "POST",
                                        url: "<?php echo base_url();?>admin/geographic/stateupdate",
                                        data: {
                                            'cid': $("#countryid_update").val(),
                                            'sid': $("#statename_update").val(),
                                            'scode': $("#statecode_update").val(),
                                            'id': d
                                        },
                                        success: function (data) {
                                            $("#state_table").load(location.href + " #state_table");
                                            $("#msgdiv_state").html('updated sucessfully');
                                            $("#myModal_forstate").modal('hide');
                                        },
                                        error: function (data) {
                                            // alert(data);
                                        }
                                    });

                                }

                                function update_submit_city() {
                                    var d = $("#id_for_city").val();
                                    /* console.log({
                                        'sid': $("#countryid_forcity_update").val(),
                                            'cname': $("#cityname_forcity_update").val(),
                                            'id': d
                                    });*/
                                    $.ajax({
                                        type: "POST",
                                        url: "<?php echo base_url();?>admin/geographic/cityupdate",
                                        data: {
                                            'sid': $("#countryid_forcity_update").val(),
                                            'cname': $("#cityname_forcity_update").val(),
                                            'id': d
                                        },
                                        success: function (data) {
                                            //alert(data);
                                            $("#city_refresh").load(location.href + " #city_refresh");
                                            $("#msgdiv_city").html('updated sucessfully');
                                            $("#myModal_forcity").modal('hide');
                                        },
                                        error: function (data) {
                                            // alert(data);
                                        }
                                    });
                                    return false;

                                }



                            </script>
                            <!--script of country ends-->


                        </div>
                    </div>


                  

                    <!--state start-->

                    <div class="tab-pane" id="state">
                        <h4 class="text-center"><b>STATE PANEL</b></h4>
                        <div style="color: #1b6d57" class="text-center" id="msgdiv_state"></div>
                        <div id="state_table">
                            <form action="" id="addState" method="post" enctype="multipart/form-data">

                                <div class="sep_box">
                                    <div class="col-lg-6">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="tbl_text">Select Country <span
                                                            style="color:red;font-weight: bold;">*</span>
                                                </div>
                                            </div>
                                            <div class="col-lg-8">
                                                <div class="tbl_input">
                                                    <select name="countryid" id="countryid">
                                                        <option value="">Select Country</option>
                                                        <?php foreach ($viewcountry as $value) { ?>
                                                            <option value="<?php echo $value->conid; ?>"><?php echo $value->name; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="sep_box">
                                    <div class="col-lg-6">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="tbl_text">State Name <span
                                                            style="color:red;font-weight: bold;">*</span>
                                                </div>
                                            </div>
                                            <div class="col-lg-8">
                                                <div class="tbl_input"><input type="text"
                                                                              name="statename"
                                                                              id="statename"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="sep_box">
                                    <div class="col-lg-6">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="tbl_text">State Code <span
                                                            style="color:red;font-weight: bold;">*</span>
                                                </div>
                                            </div>
                                            <div class="col-lg-8">
                                                <div class="tbl_input"><input type="text"
                                                                              name="statecode"
                                                                              id="statecode"
                                                                              maxlength="2"/></div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="sep_box">
                                        <div class="col-lg-6">
                                            <div class="row">
                                                <div class="col-lg-4"></div>
                                                <div class="col-lg-8">
                                                    <div class="submit_tbl">
                                                        <input id="Submit1" name="submit" type="submit"
                                                               value="Submit" class="btn_button sub_btn"
                                                               onclick="return validate16();"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>


                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>S.No.</th>
                                    <th>Country Name</th>
                                    <th>State Name</th>
                                    <th>State Code</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 0;
                                //pend($data);
                                foreach ($data as $key => $value) {
                                    $i++; ?>
                                    <tr>

                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $value->name; ?></td>
                                        <td><?php echo $value->statename; ?></td>
                                        <td><?php echo $value->statecode; ?></td>
                                        <td>
                                            <a class="btn_edit" onclick="return editState('<?= $value->countryid?>','<?= $value->statename?>','<?= $value->statecode?>','<?= $value->stateid?>')" data-toggle="modal" data-target="#myModal_forstate"><i
                                                        class="fa fa-pencil"></a></i>
                                            <a class="btn_delete" onclick="return RemoveState('<?php echo $value->stateid ?>')"><i
                                                        class="fa fa-trash fa-lg"></i></a>


                                            <a class="btn_status" onclick="return StatusState('<?php echo $value->stateid ?>','<?php echo $value->isdeleted; ?>')"><?php if ($value->isdeleted == '0') { ?>Inactive<?php } else { ?>Active<?php } ?></a>

                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <script>
                        $(function () {


                            $("#addState").validate({
                                rules: {
                                    statename: "required",
                                    statecode: "required",
                                    countryid: "required",
                                },
                                messages: {
                                    statename: "State name required",
                                    statecode: "State code required",
                                    countryid: "Country Id required"
                                },
                                submitHandler: function (form) {

                                    var formData = new FormData($("#addState").get(0));
                                    $.ajax({
                                        type: "POST",
                                        url: "<?php echo base_url();?>admin/geographic/addstate",
                                        data: formData,
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        success: function (data) {
                                            if (data == '0') {
                                                alert('already exists');
                                                return false;

                                            } else {
                                                $("#state_table").load(location.href + " #state_table");
                                                $("#msgdiv_state").html('inserted sucessfully');
                                            }
                                        },
                                        error: function (data) {
                                            // alert(data);
                                        }
                                    });
                                    return false;
                                }
                            });


                        });


                        function RemoveState(a) {
                            if (confirm("Are you sure to delete?") == true) {
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo base_url()?>admin/geographic/statedelete/" + a,
                                    cache: false,
                                    contentType: false,
                                    processData: false,
                                    success: function (data) {
                                        // alert(data);
                                        $("#state_table").load(location.href + " #state_table");
                                        $("#msgdiv_state").html('deleted sucessfully');
                                    },
                                    error: function (data) {
                                        // alert(data);
                                    }
                                });
                            } else {
                                return false;
                            }
                        }


                        function StatusState(a, b) {
                            if (confirm("Are you sure to change status?") == true) {
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo base_url()?>admin/geographic/statestatus/" + a + "/" + b,
                                    cache: false,
                                    contentType: false,
                                    processData: false,
                                    success: function (data) {
                                        //alert(data);
                                        $("#state_table").load(location.href + " #state_table");
                                        $("#msgdiv_state").html('status change sucessfully');
                                    },
                                    error: function (data) {
                                        // alert(data);
                                    }
                                });
                            } else {
                                return false;
                            }
                        }
                    </script>

                    <!--state end-->



                   
                    <!--city start-->

                    <div class="tab-pane" id="city">
                        <div style="color: #1b6d57" class="text-center" id="msgdiv_city"></div>

                            <h4 class="text-center"><b>CITY PANEL</b></h4>
                            <form action="" id="addCity" method="post" enctype="multipart/form-data">
                                <div class="sep_box">
                                    <div class="col-lg-6">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="tbl_text">Select Country <span
                                                            style="color:red;font-weight: bold;">*</span>
                                                </div>
                                            </div>
                                            <div class="col-lg-8">
                                                <div class="tbl_input">
                                                    <select name="countryid_forcity"
                                                            id="countryid_forcity"
                                                            onchange="selectstates_forcity();">
                                                        <option value="">select Country</option>
                                                        <?php foreach ($vieww as $key => $value) { ?>
                                                            <option value="<?php echo $value->conid; ?>"><?php echo $value->name; ?></option>
                                                        <?php } ?>
                                                    </select></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="sep_box">
                                    <div class="col-lg-6">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="tbl_text">Select State <span
                                                            style="color:red;font-weight: bold;">*</span>
                                                </div>
                                            </div>
                                            <div class="col-lg-8">
                                                <div class="tbl_input">
                                                    <select id="stateid_forcity" name="stateid_forcity"
                                                            value="stateid">
                                                        <option value="">Select State</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="sep_box">
                                    <div class="col-lg-6">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="tbl_text">City Name <span
                                                            style="color:red;font-weight: bold;">*</span>
                                                </div>
                                            </div>
                                            <div class="col-lg-8">
                                                <div class="tbl_input"><input type="text"
                                                                              name="cityname_forcity"
                                                                              id="cityname_forcity"
                                                                              onblur="checkname_forcity();"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>


                                <div class="sep_box">
                                    <div class="col-lg-6">
                                        <div class="row">
                                            <div class="col-lg-4"></div>
                                            <div class="col-lg-8">
                                                <div class="submit_tbl">
                                                    <input id="submitBtn" type="submit" name="submit"
                                                           value="Submit" class="btn_button sub_btn"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </form>
                        <div id="city_refresh">
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>S:NO</th>
                                    <th>State Name</th>
                                    <th>City Name</th>
                                    <th>Action</th>

                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 0;
                                if (!empty($vieww)) {
                                    if ($_GET['page']) {
                                        $page = $_GET['page'] - 1;
                                        $i = $page * 50;
                                    }
                                }
                                foreach ($viewcity as $key => $value) {
                                    $i++;
                                    ?>

                                    <tr>
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $value->statename; ?></td>
                                        <td><?php echo $value->cityname; ?></td>
                                        <td>




                                            <a class="btn_edit" onclick="return editCity('<?= $value->stateid ?>','<?= $value->cityname ?>','<?= $value->cityid ?>')"
                                               data-toggle="modal" data-target="#myModal_forcity"><i
                                                        class="fa fa-pencil"></a></i>


                                            <a class="btn_delete" onclick="return RemoveCity('<?php echo $value->cityid ?>')"><i
                                                        class="fa fa-trash"></i></a>


                                            <a class="btn_status" onclick="return StatusCity('<?php echo $value->cityid ?>','<?php echo $value->cstatus; ?>')"><?php if ($value->cstatus == '0') { ?>Inactive<?php } else { ?>Active<?php } ?></a>





                                           <!-- <a href="<?php /*echo base_url() */?>admin/geographic/cityupdate/<?php /*echo $value->cityid */?>"><i
                                                        class="fa fa-pencil"></a></i>
                                            <a href="<?php /*echo base_url() */?>admin/geographic/citydelete/<?php /*echo $value->cityid */?>"
                                               onclick="return confirm('Are you sure to delete city?');"><i
                                                        class="fa fa-trash fa-lg"></i></a>
                                            <a href="<?php /*echo base_url(); */?>admin/geographic/citystatus/<?php /*echo $value->cityid . '/' . $value->cstatus; */?>"><?php /*if ($value->cstatus == 'D') { */?>Inactive<?php /*} else { */?>Active<?php /*} */?></a>-->





                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>


                    <!--city end-->            


                    <!--location start-->
                    <div class="tab-pane" id="location">
                        <div style="color: #1b6d57" class="text-center" id="msgdiv_location"></div>

                            <h4 class="text-center"><b>LOCATION PANEL</b></h4>
                            <form action="" id="addLocation" method="post"
                                  enctype="multipart/form-data">
                                <div class="sep_box">
                                    <div class="col-lg-6">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="tbl_text">Select Country <span
                                                            style="color:red;font-weight: bold;">*</span>
                                                </div>
                                            </div>
                                            <div class="col-lg-8">
                                                <div class="tbl_input">
                                                    <select name="countryid_forlocation"
                                                            id="countryid_forlocation"
                                                            onchange="selectstates_forlocation();">
                                                        <option value="">select Country</option>
                                                        <?php foreach ($vieww as $key => $value) { ?>
                                                            <option value="<?php echo $value->conid; ?>"><?php echo $value->name; ?></option>
                                                        <?php } ?>
                                                    </select></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="sep_box">
                                    <div class="col-lg-6">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="tbl_text">Select State <span
                                                            style="color:red;font-weight: bold;">*</span>
                                                </div>
                                            </div>
                                            <div class="col-lg-8">
                                                <div class="tbl_input">
                                                    <select id="stateid_forlocation" required
                                                            name="stateid_forlocation" value="stateid"
                                                            onchange="selectcity_forlocation();">
                                                        <option value="">Select State</option>
                                                    </select>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>

                                <div class="sep_box">
                                    <div class="col-lg-6">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="tbl_text">Select City <span
                                                            style="color:red;font-weight: bold;">*</span>
                                                </div>
                                            </div>
                                            <div class="col-lg-8">
                                                <div class="tbl_input">
                                                    <select id="cityid_forlocation"
                                                            name="cityid_forlocation" value="cityid"
                                                            >
                                                        <option value="">Select city</option>
                                                    </select>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                                <div class="sep_box">
                                    <div class="col-lg-6">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="tbl_text">Location Name <span
                                                            style="color:red;font-weight: bold;">*</span>
                                                </div>
                                            </div>
                                            <div class="col-lg-8">
                                                <div class="tbl_input"><input type="text"
                                                                              name="name_forlocation"
                                                                              id="name_forlocation"
                                                                              required
                                                                              onblur="checkname_forlocation();"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="sep_box">
                                    <div class="col-lg-6">
                                        <div class="row">
                                            <div class="col-lg-4"></div>
                                            <div class="col-lg-8">
                                                <div class="submit_tbl">
                                                    <input id="submitBtn" type="submit" value="Submit"
                                                           class="btn_button sub_btn"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </form>
                        <div id="location_refresh">
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>S.No.</th>
                                    <th>Location Name</th>
                                    <th>Location City</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 0;
                                foreach ($vieww_location as $key => $value) {
                                    $i++; ?>
                                    <tr>

                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $value->locationname; ?></td>
                                        <td>
                                            <?php

                                            $parameter = array( 'act_mode'=>'getcity',
                                            'Param1'=>$value->cityid,
                                            'Param2'=>'',
                                            'Param3'=>'',
                                            'Param4'=>'',
                                            'Param5'=>'',
                                            'Param6'=>'',
                                            'Param7'=>'',
                                            'Param8'=>'',
                                            'Param9'=>'');
                                            $path = api_url()."main_snowworld_v/getlocation/format/json/";
                                            $loc_name = curlpost($parameter,$path);
                                           // p($loc_name);
                                            $arr = (array)$loc_name;
                                            echo($arr[0]['cityname']);
                                            ?>




                                        </td>
                                        <td>

                                            <a class="btn_edit" onclick="return editLocation('<?= $value->locationname ?>','<?= $value->cityid ?>','<?= $value->locationid ?>')"
                                               data-toggle="modal" data-target="#myModal_forlocation"><i
                                                        class="fa fa-pencil"></a></i>


                                            <a class="btn_delete" onclick="return RemoveLocation('<?php echo $value->locationid ?>')"><i
                                                        class="fa fa-trash"></i></a>


                                            <a class="btn_status" onclick="return StatusLocation('<?php echo $value->locationid ?>','<?php echo $value->lstatus; ?>')"><?php if ($value->lstatus == '0') { ?>Inactive<?php } else { ?>Active<?php } ?></a>


                                      <!--      <a href="<?php /*echo base_url() */?>admin/geographic/locationupdate/<?php /*echo $value->locationid */?>"><i
                                                        class="fa fa-pencil"></a></i>
                                            <a href="<?php /*echo base_url() */?>admin/geographic/locationdelete/<?php /*echo $value->locationid */?>"
                                               onclick="return confirm('Are you sure to delete?');"><i
                                                        class="fa fa-trash fa-lg"></i></a>
                                            <a href="<?php /*echo base_url(); */?>admin/geographic/locationstatus/<?php /*echo $value->locationid . '/' . $value->lstatus; */?>"><?php /*if ($value->lstatus == 'D') { */?>Inactive<?php /*} else { */?>Active<?php /*} */?></a>-->

                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!--location end-->

 <!------------------------------------------------------------------------------------------------------------------------->
                </div>


            </div>

        </div>
    </div>


</div>
</div>
</div>
</div>
</div>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
</button>
<h4 class="modal-title" id="myModalLabel">Update country</h4>
</div>
<div class="modal-body">
<form id="addCont1" action="" method="post" enctype="multipart/form-data">
    <div style="color: #1b6d57" class="text-center" id="msgdiv"></div>
    <div class="sep_box">
        <div class="col-lg-12">

            <div class="row">
                <div class="col-lg-4">
                    <div class="tbl_text">Country Name <span
                                style="color:red;font-weight: bold;">*</span></div>
                </div>
                <div class="col-lg-8">
                    <div class="tbl_input"><input type="text" name="update_couname" id="update_couname"
                                                  onblur="checkname();"/></div>
                </div>
            </div>
        </div>
    </div>
    <div class="sep_box">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-4">
                    <div class="tbl_text">Country Code<span
                                style="color:red;font-weight: bold;">*</span></div>
                </div>
                <div class="col-lg-8">
                    <div class="tbl_input"><input id="update_coucode" type="text"
                                                  name="update_coucode"/></div>
                    <input type="hidden" id="update_id">
                </div>
            </div>
        </div>
    </div>
    <div class="sep_box">
        <div class="col-lg-6">
            <div class="row">
                <div class="col-lg-4"></div>
                <div class="col-lg-8">
                    <div class="submit_tbl">
                        <input id="submitBtn" type="button" onclick="return update_submit_country()"
                               value="Submit" class="btn_button sub_btn"/>
                    </div>
                </div>
            </div>
        </div>

    </div>
</form>
</div>
<div class="modal-footer">
</div>
</div>
</div>
</div>


<script type="text/javascript">
function checkname() {
var name = $('#cityname').val();

$.ajax({
url: '<?php echo base_url()?>admin/geographic/checkcityname',
type: 'POST',
dataType: 'json',
data: {'name': name},
success: function (data) {

if (data[0].count > 0) {
    $('#submitBtn').prop('disabled', 'disabled');
    alert("Data Already Existed");
} else {
    $('#submitBtn').removeAttr('disabled');
}
}
});

}

function selectstates() {
var countryid = $('#countryid').val();
$.ajax({
url: '<?php echo base_url()?>admin/geographic/countrystate',
type: 'POST',
//dataType: 'json',
data: {'countryid': countryid},
success: function (data) {
var option_brand = '<option value="">Select State</option>';
$('#stateid').empty();
$("#stateid").append(option_brand + data);

}
});

}

</script>
<script type="text/javascript">

function selectcity() {

var stateid = $('#stateid').val();
$.ajax({
url: '<?php echo base_url()?>admin/geographic/statecity',
type: 'POST',
//dataType: 'json',
data: {'stateid': stateid},
success: function (data) {
var option_brand = '<option value="">Select City</option>';
$('#cityid').empty();
$("#cityid").append(option_brand + data);

}
});

}

function selectstates_forcity() {
var countryid = $('#countryid_forcity').val();
$.ajax({
url: '<?php echo base_url()?>admin/geographic/countrystate',
type: 'POST',
//dataType: 'json',
data: {'countryid': countryid},
success: function (data) {
var option_brand = '<option value="">Select State</option>';
$('#stateid_forcity').empty();
$("#stateid_forcity").append(option_brand + data);

}
});

}


$(function () {
$("#addCity").validate({
rules: {
countryid_forcity: "required",
stateid_forcity: "required",
cityname_forcity: "required",
},
messages: {
couname: "Country name required",
coucode: "Country code required",
cityname_forcity: "CIty name is required"
},

submitHandler: function (form) {

var formData = new FormData($("#addCity").get(0));
$.ajax({
    type: "POST",
    url: "<?php echo base_url();?>admin/geographic/add_city",
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    success: function (data) {
        $("#city_refresh").load(location.href + " #city_refresh");
        $("#msgdiv_city").html('inserted sucessfully');
        $( '#addCity' ).each(function(){
            this.reset();
        });
        return false;
    },
    error: function (data) {
        return false;
    }
});
return false;
}
});


$("#addLocation").validate({
rules: {
countryid_forlocation: "required",
stateid_forlocation: "required",
cityid_forlocation: "required",
name_forlocation: "required"
},
messages :{
    countryid_forlocation: " country  is required",
    stateid_forlocation: " state is required",
    cityid_forlocation: " city is required",
    name_forlocation: "name is required"

},
submitHandler: function (form) {
var formData = new FormData($("#addLocation").get(0));
    $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>admin/geographic/addlocation",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
            $("#location_refresh").load(location.href + " #location_refresh");
            $("#msgdiv_location").html('inserted sucessfully');
            $( '#addLocation' ).each(function(){
                this.reset();
            });
            return false;
        },
        error: function (data) {
            return false;
        }
    });
    return false;
}
});

});


function checkname_forcity(){
    var name=$('#cityname_forcity').val();

    $.ajax({
        url: '<?php echo base_url()?>admin/geographic/checkcityname',
        type: 'POST',
        dataType: 'json',
        data: {'name': name},
        success: function(data){

            if(data[0].count > 0){
                $('#submitBtn').prop('disabled', 'disabled');
                $( '#addCity' ).each(function(){
                    this.reset();
                });
                alert("Data Already Existed");
            }else{
                $('#submitBtn').removeAttr('disabled');
            }
        }
    });

}

function RemoveCity(a) {
    if (confirm("Are you sure to delete?") == true) {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url()?>admin/geographic/citydelete/" + a,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                // alert(data);
                $("#city_refresh").load(location.href + " #city_refresh");
                $("#msgdiv_city").html('deleted sucessfully');
            },
            error: function (data) {
                // alert(data);
            }
        });
    } else {
        return false;
    }
}


function editCity(a, s, d) {
    $("#countryid_forcity_update").find("option").removeAttr('selected');
    var vaa = $("#countryid_forcity_update").find("option[value='"+a+"']").attr('selected',true);
    $("#cityname_forcity_update").val(s);
    $("#id_for_city").val(d);

}

function StatusCity(a, b) {
    /*alert(a);
     alert(b);*/
    if (confirm("Are you sure to change status?") == true) {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url()?>admin/geographic/citystatus/" + a + "/" + b,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                //alert(data);
                $("#city_refresh").load(location.href + " #city_refresh");
                $("#msgdiv_city").html('status change sucessfully');
            },
            error: function (data) {
                // alert(data);
            }
        });
    } else {
        return false;
    }
}

function checkname_forlocation(){

/*    var name=$('#cityname_forlocation').val();
    alert(name);return false;

    $.ajax({
        url: 'echo base_url()?>admin/geographic/checkcityname',
        type: 'POST',
        dataType: 'json',
        data: {'name': name},
        success: function(data){
            if(data[0].count > 0){
                $('#submitBtn').prop('disabled', 'disabled');
                alert("Data Already Existed");
            }else{
                $('#submitBtn').removeAttr('disabled');
            }
        }
    });*/

}




function selectstates_forlocation(){

    var countryid=$('#countryid_forlocation').val();
    $.ajax({
        url: '<?php echo base_url()?>admin/geographic/countrystate',
        type: 'POST',
        //dataType: 'json',
        data: {'countryid': countryid},
        success: function(data){
            var  option_brand = '<option value="">Select State</option>';
            $('#stateid_forlocation').empty();
            $("#stateid_forlocation").append(option_brand+data);

        }
    });

}

function selectcity_forlocation(){

    var stateid=$('#stateid_forlocation').val();
    $.ajax({
        url: '<?php echo base_url()?>admin/geographic/statecity',
        type: 'POST',
        //dataType: 'json',
        data: {'stateid': stateid},
        success: function(data){
            var  option_brand = '<option value="">Select City</option>';
            $('#cityid_forlocation').empty();
            $("#cityid_forlocation").append(option_brand+data);

        }
    });

}




function RemoveLocation(a) {
    if (confirm("Are you sure to delete?") == true) {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url()?>admin/geographic/locationdelete/" + a,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                 //alert(data);
                $("#location_refresh").load(location.href + " #location_refresh");
                $("#msgdiv_location").html('deleted sucessfully');
            },
            error: function (data) {
                // alert(data);
            }
        });
    } else {
        return false;
    }
}


function editLocation(a, s, d) {
    $("#updatecity_forlocation").find("option").removeAttr('selected');
    $( '#update_addLocation' ).each(function(){
        this.reset();
    });
    var vaa = $("#updatecity_forlocation").find("option[value='"+s+"']").attr('selected',true);
    $("#updatename_forlocation").val(a);
    $("#updateid_forlocation").val(d);

}

function update_submit_location() {
    var d = $("#updateid_forlocation").val();
    /* console.log({
     'sid': $("#countryid_forcity_update").val(),
     'cname': $("#cityname_forcity_update").val(),
     'id': d
     });*/
    $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>admin/geographic/location_update_modal",
        data: {
            'cityid': $("#updatecity_forlocation").val(),
            'lname': $("#updatename_forlocation").val(),
            'id': d
        },
        success: function (data) {
            //alert(data);
            $("#location_refresh").load(location.href + " #location_refresh");
            $("#msgdiv_location").html('updated sucessfully');
            $("#myModal_forlocation").modal('hide');
        },
        error: function (data) {
            // alert(data);
        }
    });
    return false;

}

function StatusLocation(a, b) {
    /*alert(a);
     alert(b);*/
    if (confirm("Are you sure to change status?") == true) {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url()?>admin/geographic/locationstatus/" + a + "/" + b,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                //alert(data);
                $("#location_refresh").load(location.href + " #location_refresh");
                $("#msgdiv_location").html('status change sucessfully');
            },
            error: function (data) {
                // alert(data);
            }
        });
    } else {
        return false;
    }
}

</script>

<!-- Modal -->
<!--modal state-->
<div class="modal fade" id="myModal_forstate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Update State</h4>
            </div>
            <div class="modal-body">
                <form action="" id="addState" method="post" enctype="multipart/form-data">

                    <div class="sep_box">
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="tbl_text">Select Country <span
                                                style="color:red;font-weight: bold;">*</span>
                                    </div>
                                </div>
                                <div class="col-lg-8">
                                    <div class="tbl_input">
                                        <select name="countryid_update" id="countryid_update">
                                            <option value="">Select Country</option>
                                            <?php foreach ($viewcountry as $value) { ?>
                                                <option value="<?php echo $value->conid; ?>"><?php echo $value->name; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="sep_box">
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="tbl_text">State Name <span
                                                style="color:red;font-weight: bold;">*</span>
                                    </div>
                                </div>
                                <div class="col-lg-8">
                                    <div class="tbl_input"><input type="text"
                                                                  name="statename_update"
                                                                  id="statename_update"
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="sep_box">
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="tbl_text">State Code <span
                                                style="color:red;font-weight: bold;">*</span>
                                    </div>
                                </div>
                                <div class="col-lg-8">
                                    <div class="tbl_input"><input type="text"
                                                                  name="statecode_update"
                                                                  id="statecode_update"
                                                                  
                                                                  maxlength="2"/></div>
                                    <input type="hidden" id="update_id_forstate">
                                </div>
                            </div>
                        </div>


                        <div class="sep_box">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-8">
                                        <div class="submit_tbl">
                                            <input id="submitBtn" type="button" onclick="return update_submit_state()"
                                                   value="Submit" class="btn_button sub_btn"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
<!--modal state-->
<!--------------------------------------------------------------------------------------------------------------->

<!--modal city-->
<div class="modal fade" id="myModal_forcity" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Update City</h4>
            </div>
            <div class="modal-body">
                <form action="" id="addCity_update" method="post" enctype="multipart/form-data">
                    <div class="sep_box">
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="tbl_text">Select State <span
                                                style="color:red;font-weight: bold;">*</span>
                                    </div>
                                </div>
                                <div class="col-lg-8">
                                    <div class="tbl_input">
                                        <select name="countryid_forcity_update"
                                                id="countryid_forcity_update"
                                        >
                                            <?php foreach ($data as $key => $value) { ?>
                                                <option value="<?php echo $value->stateid; ?>"><?php echo $value->statename; ?></option>
                                            <?php } ?>
                                        </select></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="sep_box">
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="tbl_text">City Name <span
                                                style="color:red;font-weight: bold;">*</span>
                                    </div>
                                </div>
                                <div class="col-lg-8">
                                    <div class="tbl_input"><input type="text"
                                                                  name="cityname_forcity_update"
                                                                  id="cityname_forcity_update"
                                                                  onblur="checkname_forcity();"/>
                                        <input type="hidden" id="id_for_city">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>


                    <div class="sep_box">
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-4"></div>
                                <div class="col-lg-8">
                                    <div class="submit_tbl">
                                        <input id="submitBtn" type="button" onclick="return update_submit_city()"
                                               value="Submit" class="btn_button sub_btn"/>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
<!--modal city-->



<!--modal location-->
<div class="modal fade" id="myModal_forlocation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Update Location</h4>
            </div>
            <div class="modal-body">
                <form action="" id="update_addLocation" method="post"
                      enctype="multipart/form-data">
                    <div class="sep_box">
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="tbl_text">Select State<span
                                                style="color:red;font-weight: bold;">*</span>
                                    </div>
                                </div>
                                <div class="col-lg-8">
                                    <div class="tbl_input">
                                        <select name="updatecity_forlocation"
                                                id="updatecity_forlocation">
                                            <?php foreach ($viewcity as $key => $value) { ?>
                                                <option value="<?php echo $value->cityid; ?>"><?php echo $value->cityname; ?></option>
                                            <?php } ?>
                                        </select></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="sep_box">
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="tbl_text">Location Name <span
                                                style="color:red;font-weight: bold;">*</span>
                                    </div>
                                </div>
                                <div class="col-lg-8">
                                    <div class="tbl_input"><input type="text"
                                                                  name="updatename_forlocation"
                                                                  id="updatename_forlocation"/>
                                        <input type="hidden" id="updateid_forlocation">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="sep_box">
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-4"></div>
                                <div class="col-lg-8">
                                    <div class="submit_tbl">
                                        <input id="submitBtn" type="button" onclick="return update_submit_location()"
                                               value="Submit" class="btn_button sub_btn"/>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
<!--modal location-->