<!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->
<script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery-1.9.1.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
<?php //pend($vieww); ?>
<!-- jQuery Form Validation code -->
<style>
    input.error{border:1px solid red;}
    label.error{border:0px solid red; color:red; font-weight: normal; display:inline; }
</style>

<script language='javascript'>

    // When the browser is ready...
    $(document).ready(function() {

        // Setup form validation on the #register-form element
        $("#addCont").validate({
            // Specify the validation rules
            rules: {
                countryid : "required",
                stateid : "required",
                cityname : "required",
                cityid : "required",
                locationid : "required",
                promocode_name : "required",
                promo :"required",
                promonumber : {required: true,
                    number: true},
                /* email:{
                 required: true,
                 email: true
                 },
                 password: {
                 required: true,
                 minlength: 5
                 },
                 agree: "required" */
            },
            // Specify the validation error messages
            messages: {
                countryid : "Country is required",
                stateid : "State is required",
                cityname : "City name is required",
                locationid : "location name is required",
                promocode_name :"promo code is required",
                promo : "select any type",
                promonumber : {required: "SELECT DISCOUNT PRICING",
                    number :"Number Only"}
                /*password: {
                 required: "Please provide a password",
                 minlength: "Your password must be at least 5 characters long"
                 },
                 email: "Please enter a valid email address",
                 agree: "Please accept our policy"*/
            },
            submitHandler: function(form) {
                form.submit();
            }
        });

    });

</script>

<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<div class="wrapper">

    <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">

                        <h2>ADD PROMO CODE</h2>
                    </div>
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-12">
                                <div style="text-align:right;">
                                    <a href="<?php echo base_url();?>admin/geographic/viewcity"><button>CANCEL</button></a>
                                </div>
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?>
                                    <?php
                                    if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message');
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>





                        <div class="container">
                            <h2>Table</h2>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>S.NO</th>
                                        <th>PROMO CODE NAME</th>
                                        <th>LOCATION</th>
                                        <th>TYPE</th>
                                        <th>DISCOUNT</th>
                                        <th>EDIT/DELETE</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=1; foreach($vieww as $v) {?>
                                    <tr>
                                        <td><?= $i;?></td>
                                        <td><?php echo $v->p_codename; ?></td>
                                        <td><?php

                                            $parameter = array( 'act_mode'=>'getlocation',
                                                'Param1'=>$v->p_locationid,
                                                'Param2'=>'',
                                                'Param3'=>'',
                                                'Param4'=>'',
                                                'Param5'=>'',
                                                'Param6'=>'',
                                                'Param7'=>'',
                                                'Param8'=>'',
                                                'Param9'=>'');
                                            $path = api_url()."main_snowworld_v/getlocation/format/json/";
                                            $loc_name = curlpost($parameter,$path);
                                           $arr = (array)$loc_name;
                                          echo($arr[0]['locationname']);
                                            ?></td>
                                        <td><?php echo $v->p_type; ?></td>
                                        <td><?php echo $v->p_discount; ?></td>
                                        <td>

                                            <a class="btn btn-primary" href="#">
                                                <i class="fa fa-pencil" aria-hidden="true"></i>
                                            </a>




                                            <a href="<?= base_url('admin/geographic/promo_delete/')."/".$v->promo_id ?>"  onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger btn">
                                                <span class="glyphicon glyphicon-trash"></span>
                                            </a>







                                        </td>

                                    </tr>

                                    <?php $i++;} ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function checkname(){
        var name=$('#cityname').val();

        $.ajax({
            url: '<?php echo base_url()?>admin/geographic/checkcityname',
            type: 'POST',
            dataType: 'json',
            data: {'name': name},
            success: function(data){

                if(data[0].count > 0){
                    $('#submitBtn').prop('disabled', 'disabled');
                    alert("Data Already Existed");
                }else{
                    $('#submitBtn').removeAttr('disabled');
                }
            }
        });

    }

    function selectstates(){

        var countryid=$('#countryid').val();
        $.ajax({
            url: '<?php echo base_url()?>admin/geographic/countrystate',
            type: 'POST',
            //dataType: 'json',
            data: {'countryid': countryid},
            success: function(data){
                var  option_brand = '<option value="">Select State</option>';
                $('#stateid').empty();
                $("#stateid").append(option_brand+data);

            }
        });

    }

    function selectcity(){

        var stateid=$('#stateid').val();
        $.ajax({
            url: '<?php echo base_url()?>admin/geographic/statecity',
            type: 'POST',
            //dataType: 'json',
            data: {'stateid': stateid},
            success: function(data){
                var  option_brand = '<option value="">Select City</option>';
                $('#cityid').empty();
                $("#cityid").append(option_brand+data);

            }
        });

    }

    function selectlocation(){
        var cityid = $('#cityid').val();
        $.ajax({
            url: '<?php echo base_url()?>admin/geographic/citylocation',
            type: "POST",
            data: {'cityid': cityid},
            success: function(data){
                var  option_brand = '<option value="">Select Location</option>';
                $('#locationid').empty();
                $("#locationid").append(option_brand+data);

            }

        })

    }
    $(function(){
        $("#flat").click(function () {
            $("#flatpercent").css('display','block');
        });
        $("#percent").click(function () {
            $("#flatpercent").css('display','block');
        })
    })

</script>