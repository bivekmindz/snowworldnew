<!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->


<?php //p($vieww); ?>
<!-- jQuery Form Validation code -->
<script>
    $(function() {
        // Initialize form validation on the registration form.
        // It has the name attribute "registration"
        $("#add_addon").validate({
            // Specify validation rules
            rules: {
                addon_name : "required",
                addon_desc : "required",
                branchids : "required",
                act_ids : "required",
                addon_price : {required: true,
                    number: true}

            },
            message : {
                addon_name : "required"

            }
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
                form.submit();
            }
        });
    });


</script>
<style>
    input.error{border:1px solid red;}
    label.error{border:0px solid red; color:red; font-weight: normal; display:inline; }
</style>


<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery.validate.min.js"></script>

<div class="wrapper">

    <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">

                        <h2>ADD PROMO CODE</h2>
                    </div>
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-12">
                                <div style="text-align:right;">
                                    <a href=""><button>CANCEL</button></a>
                                </div>
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?>
                                    <?php
                                    if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message');
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>

                        <form action="<?= base_url('admin/geographic/addAddon') ?>" name="add_addon" id="add_addon" method="post" enctype="multipart/form-data" >


                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">addon Name<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" id="addon_name" name="addon_name" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">addon Description<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <textarea  id="addon_desc" name="addon_desc" ></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Select Branch <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <select name="branchids[]" multiple id="branchids">
                                                    <?php foreach ($s_viewbranch as $key => $value) { ?>
                                                        <option value="<?php echo $value->branch_id; ?>"><?php echo $value->branch_name; ?></option>
                                                    <?php } ?>
                                                </select></div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Select Activity <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <select name="act_ids[]" multiple id="act_ids" onchange="return price_pack(this)">
                                                    <?php foreach ($vieww_act as $key => $value) { ?>
                                                        <option value="<?php echo $value->activity_id; ?>"><?php echo $value->activity_name; ?></option>
                                                    <?php } ?>
                                                </select></div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">addon Price<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" id="addon_price" name="addon_price" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-8">
                                            <div class="submit_tbl">
                                                <input id="submitBtn" type="submit" name="submit" value="Submit" class="btn_button sub_btn" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </form>


                        <div class="col-md-12">
                            <h2>Table</h2>
                            <div class="table-responsive" style="width: 100%;">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>S.NO</th>
                                        <th>addon Name</th>
                                        <th>addon Description</th>
                                        <th>addon Branch</th>
                                        <th>addon Activity</th>
                                        <th>addon Price</th>
                                        <th>EDIT/DELETE</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=1; foreach($vieww_pack as $v) {?>
                                        <tr>
                                            <td><?= $i;?></td>
                                            <td id="t_act_name"><?php echo $v->addon_name; ?></td>
                                            <td id="t_act_desc"><?php echo $v->addon_description; ?></td>
                                            <td id="t_act_price"><?php




                                                echo $v->branch_company;





                                                ?></td>
                                            <td id="t_act_price"><?php echo $v->branch_company; ?></td>
                                            <td id="t_act_price"><?php echo $v->addon_price; ?></td>
                                            <td>

                                                <a class="btn btn-primary" onclick="return update_act('<?php echo $v->activity_name; ?>','<?php echo $v->activity_discription; ?>','<?php echo $v->activity_price; ?>','<?php echo $v->addon_id; ?>')">
                                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                                </a>




                                                <a href="<?= base_url('admin/geographic/activity_delete/')."/".$v->addon_id ?>"  onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger btn">
                                                    <span class="glyphicon glyphicon-trash"></span>
                                                </a>







                                            </td>

                                        </tr>

                                        <?php $i++;} ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>




                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function () {
        $("#branchids,#act_ids").select2({
            placeholder: "Select the Data",
            allowClear: true
        });
    });


    function selectstates(){

        var countryid=$('#countryid').val();
        $.ajax({
            url: '<?php echo base_url()?>admin/geographic/countrystate',
            type: 'POST',
            //dataType: 'json',
            data: {'countryid': countryid},
            success: function(data){
                var  option_brand = '<option value="">Select State</option>';
                $('#stateid').empty();
                $("#stateid").append(option_brand+data);

            }
        });

    }

    function selectcity(){

        var stateid=$('#stateid').val();
        $.ajax({
            url: '<?php echo base_url()?>admin/geographic/statecity',
            type: 'POST',
            //dataType: 'json',
            data: {'stateid': stateid},
            success: function(data){
                var  option_brand = '<option value="">Select City</option>';
                $('#cityid').empty();
                $("#cityid").append(option_brand+data);

            }
        });

    }

    function selectlocation(){
        var cityid = $('#cityid').val();
        $.ajax({
            url: '<?php echo base_url()?>admin/geographic/citylocation',
            type: "POST",
            data: {'cityid': cityid},
            success: function(data){
                var  option_brand = '<option value="">Select Location</option>';
                $('#locationid').empty();
                $("#locationid").append(option_brand+data);

            }

        })

    }

    function update_act(a,b,c,d){
        //alert(a +b +c +d);
        $("#act_name_update").val(a);
        $("#act_desc_update").val(b);
        $("#act_price_update").val(c);
        $("#act_id").val(d);
        $("#update_act").css('display','block');
        $("#add_act").css('display','none');
        $("#act_name_update").focus();

    }
    function UpdateCancel() {
        $("#add_act").css('display','block');
        $("#update_act").css('display','none');
        return false;
    }

    function price_pack(){
        console.log($("#act_ids").val());

    }





</script>