<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <title></title>


    <link rel='stylesheet prefetch' href='http://netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css'>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <link rel="stylesheet" href="<?= base_url('assets/login') ?>/css/style.css">

<!-- Favicons-->
   <link rel="shortcut icon" href="<?php echo base_url(); ?>img/favicon.png" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="<?php echo base_url(); ?>img/apple-touch-icon-57x57-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="<?php echo base_url(); ?>img/apple-touch-icon-72x72-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="<?php echo base_url(); ?>img/apple-touch-icon-114x114-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="<?php echo base_url(); ?>img/apple-touch-icon-144x144-precomposed.png">
</head>

<body>
<div class="wrapper">


       <!-- <div class="banner_pic"><img src="<?= base_url() ?>/assets/admin/images/background.jpg"></div> -->




    <form method="post" action="" class="form-signin">
        <div class="ad_login" style="margin-top:30%;">
        <h2 class="form-signin-heading"><strong>Admin</strong></h2>
        <div><?php echo validation_errors();  if(isset($result) && $result!=''){echo ($result);}?></div>
            <div class="form-group">
                <label>Email Address</label>
                <input type="text" class="form-control" name="email" placeholder="Email Address" required autofocus /></div>

            <div class="form-group">
                <label>Password</label>
                <input type="password" class="form-control" name="password" placeholder="Password" required/></div>

        <label style="margin-left:30px;" class="checkbox">
            <input type="checkbox" name="checkbox" checked="">Remember Me
        </label>
        <input class="btn btn-lg btn-primary btn-block" name="submit" value="Login" type="submit">
        </div>
    </form>
</div>


</body>
</html>
