

<!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->
<script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery-1.9.1.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js" crossorigin="anonymous"></script>
<?php //p($vieww); ?>
<!-- jQuery Form Validation code -->
<link HREF="<?php echo base_url()?>/css/date_time_picker.css" rel="stylesheet">

<script>


    $(function() {
        // Initialize form validation on the registration form.
        // It has the name attribute "registration"
        $("#add_com").validate({
            // Specify validation rules
            rules: {
                // The key name on the left side is the name attribute
                // of an input field. Validation rules are defined
                // on the right side
                com_name: "required",
            },
            // Specify validation error messages
            messages: {
                com_name: "Please enter name",
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
                form.submit();
            }
        });



        $("#update_com").validate({
            // Specify validation rules
            rules: {
                // The key name on the left side is the name attribute
                // of an input field. Validation rules are defined
                // on the right side
                com_name_update: "required",
            },
            // Specify validation error messages
            messages: {
                com_name_update: "Please enter name",
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
                form.submit();
            }
        });
    });


</script>
<style>
    input.error{border:1px solid red;}
    label.error{border:0px solid red; color:red; font-weight: normal; display:inline; }
</style>


<meta name="viewport" content="width=device-width, initial-scale=1">


<div class="wrapper">

    <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">

                        <h2>Date Wise Inventory</h2>
                    </div>
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-12">
                                <div style="text-align:right;">
                                    <a href=""><button>CANCEL</button></a>
                                </div>
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?>
                                    <?php
                                    if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message');
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>




                        <form action="" name="add_com" id="add_com" method="post" enctype="multipart/form-data" >
                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Select Branch <span
                                                        style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <select name="branchids" id="branchids">
                                                    <?php foreach ($s_viewbranch as $key => $value) { ?>
                                                        <option value="<?php echo $value->branch_id; ?>"><?php echo $value->branch_name; ?></option>
                                                    <?php } ?>
                                                </select></div>
                                        </div>
                                    </div>
                                    </di v>
                                </div>

                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Select Date <span
                                                        style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <link HREF="<?php echo base_url()?>css/date_time_picker.css" rel="stylesheet">

                                                <input required class="date-pick form-control formmm1" data-date-format="dd/mm/yyyy" type="text" id="datepicker1" name="departDate" placeholder="Pick Session Date"  title="Pick Session Time">

                                                <!-- Common scripts -->
                                                <script SRC="<?php echo base_url()?>js/jquery-1.11.2.min.js"></script>
                                                <script SRC="<?php echo base_url()?>js/common_scripts_min.js"></script>
                                                <script SRC="<?php echo base_url()?>js/functions.js"></script>

                                                <!-- Specific scripts -->
                                                <script SRC="<?php echo base_url()?>js/bootstrap-datepicker.js"></script>


                                            </div>
                                    </div>
                                    </di v>
                                </div>
                               </div>
                            <div class="sep_box">

                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-8">
                                            <div class="submit_tbl">
                                                <input id="submitBtn" type="submit" name="submit" value="Submit" class="btn_button sub_btn" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>




                        </form>


                        <div class="col-md-12">
                            <h2>Table</h2>
                            <div class="table-responsive" style="width: 100%;">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>S.NO</th>
                                        <th>From To</th>
                                        <th>Number Of Seat</th>
                                        <th>Booked Seat</th>
                                        <th>Remaining Seat</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=1; foreach($s_viewtimeslot as $v) {?>
                                        <tr>
                                            <td><?= $i;?></td>
                                            <td id="t_com_name"><?php echo $v->timeslot_from; ?>:<?php echo $v->timeslot_minfrom; ?> Hrs - <?php echo $v->timeslot_to; ?>:<?php echo $v->timeslot_minto; ?> Hrs </td>
                                            <td> <?php echo $v->timeslot_seats; ?> </td>
                                            <td> <?php if($v->coun!='') { echo $v->coun;} else { echo "0";} ?> </td>
                                            <td> <?php echo ( $v->timeslot_seats-$v->coun); ?> </td>
                                        </tr>

                                        <?php $i++;} ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>




                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script>

    function update_com(a,b){
        //alert(a +b);
        $("#com_name_update").val(a);
        $("#com_id").val(b);
        $("#update_com").css('display','block');
        $("#add_com").css('display','none');
        $("#com_name_update").focus();

    }
    function UpdateCancel() {
        $("#add_com").css('display','block');
        $("#update_com").css('display','none');
        return false;
    }





</script><script SRC="http://192.168.1.30/skiindia/js/bootstrap-datepicker.js"></script>