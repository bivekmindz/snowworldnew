 <script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>

<div class="wrapper">

 
<div class="col-lg-10 col-lg-push-2">



        <div class="row">

            <div class="page_contant">
            
            
                <div class="col-lg-12">
                    <div class="page_name">
                             
                        <h2>Manage Geographic</h2>
    
                    </div>
                    
                    <div class="page_box">
                    
                    <div class="col-lg-12">
                    
                    <div id="content">
    <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
        <li class="active"><a href="#country" data-toggle="tab">Country</a></li>
        <li><a href="#orange" data-toggle="tab">Orange</a></li>
        <li><a href="#yellow" data-toggle="tab">Yellow</a></li>
        <li><a href="#green" data-toggle="tab">Green</a></li>
        <li><a href="#blue" data-toggle="tab">Blue</a></li>
    </ul>
    <div id="my-tab-content" class="tab-content">




        <div class="tab-pane active" id="country">
        <div class="form-conte">
            <!--  session flash message  -->
            <div class='flashmsg'>
                <?php echo validation_errors(); ?>
                <?php
                if($this->session->flashdata('message')){
                    echo $this->session->flashdata('message');
                }
                ?>
            </div>
            <!--country form start-->
            <form id="addCont" action="<?=  base_url('admin/geographic/add_country') ?>" method="post" enctype="multipart/form-data" >
                <div class="sep_box">
                    <div class="col-lg-6">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="tbl_text">Country Name <span style="color:red;font-weight: bold;">*</span></div>
                            </div>
                            <div class="col-lg-8">
                                <div class="tbl_input"><input  type="text" name="couname"  id="couname" onblur="checkname();" /></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="sep_box">
                    <div class="col-lg-6">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="tbl_text">Country Code <span style="color:red;font-weight: bold;">*</span></div>
                            </div>
                            <div class="col-lg-8">
                                <div class="tbl_input"><input id="coucode" type="text" name="coucode" /></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="sep_box">
                    <div class="col-lg-6">
                        <div class="row">
                            <div class="col-lg-4"></div>
                            <div class="col-lg-8">
                                <div class="submit_tbl">
                                    <input id="submitBtn" type="submit" name="submit" value="Submit" class="btn_button sub_btn" />
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </form>
            <!--country form end-->

            <!--country listing start-->
            <table class="grid_tbl">
                <thead>
                <tr>
                    <th>S:NO</th>
                    <th>Country Name</th>
                    <th>Country Code</th>
                    <th>Action</th>

                </tr>
                </thead>
                <tbody>
                <?php $i=0; foreach ($vieww as $key => $value) {
                    $i++;
                    ?>

                    <tr>
                        <td><?php echo $i ;?></td>
                        <td><?php echo $value->name ;?></td>
                        <td><?php echo $value->code ;?></td>
                        <td><a href="<?php echo base_url()?>admin/geographic/countryupdate/<?php echo $value->conid?>" ><i class="fa fa-pencil"></a></i>
                            <a href="<?php echo base_url()?>admin/geographic/countrydelete/<?php echo $value->conid?>" onclick="return confirm('Are you sure to delete?');"><i class="fa fa-trash"></i></a>
                            <a href="<?php echo base_url(); ?>admin/geographic/countrystatus/<?php echo $value->conid.'/'.$value->counstatus; ?>"><?php if($value->counstatus=='D') { ?>Inactive<?php }else { ?>Active<?php } ?></a>

                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
            <!--country listing close-->

            <!--script of country start-->
            <script type="text/javascript">
                function checkname(){
                    var name=$('#couname').val();
                    $.ajax({
                        url: '<?php echo base_url()?>admin/geographic/checkname',
                        type: 'POST',
                        dataType: 'json',
                        data: {'name': name},
                        success: function(data){

                            if(data[0].count > 0){
                                $('#submitBtn').prop('disabled', 'disabled');
                                alert("Data Already Existed");
                            }else{
                                $('#submitBtn').removeAttr('disabled');
                            }
                        }
                    });

                }

            </script>
            <!--script of country ends-->


        
        </div>
        </div>






        <div class="tab-pane" id="orange">
            <h1>Orange</h1>
            <p>orange orange orange orange orange</p>
        </div>
        <div class="tab-pane" id="yellow">
            <h1>Yellow</h1>
            <p>yellow yellow yellow yellow yellow</p>
        </div>
        <div class="tab-pane" id="green">
            <h1>Green</h1>
            <p>green green green green green</p>
        </div>
        <div class="tab-pane" id="blue">
            <h1>Blue</h1>
            <p>blue blue blue blue blue</p>
        </div>
    </div>


</div>

</div>
                    </div>
                    <div class="page_box">
                    
                    
                    
                    
                    
                    	<div class="col-lg-12">
                    		<p> In this section , admin will be able to add the country names
and can view the country listsdfsdfsdfsdf</p>
                    	</div>
                    </div>
                    
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-12">

                                <div style="text-align:right;">
                                    <a href="<?php echo base_url();?>admin/geographic/add_country"><button>ADD COUNTRY</button></a>
                                </div>

                                <table class="grid_tbl">
                                    <thead>
                                        <tr>
                                            <th>S:NO</th>
                                            <th>Country Name</th>
                                            <th>Country Code</th>
                                            <th>Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=0; foreach ($vieww as $key => $value) {
                                          $i++;
                                        ?>
                                       
                                        <tr>
                                            <td><?php echo $i ;?></td>
                                            <td><?php echo $value->name ;?></td>
                                            <td><?php echo $value->code ;?></td>
                                            <td><a href="<?php echo base_url()?>admin/geographic/countryupdate/<?php echo $value->conid?>" ><i class="fa fa-pencil"></a></i>
                                            <a href="<?php echo base_url()?>admin/geographic/countrydelete/<?php echo $value->conid?>" onclick="return confirm('Are you sure to delete?');"><i class="fa fa-trash"></i></a>
                                            <a href="<?php echo base_url(); ?>admin/geographic/countrystatus/<?php echo $value->conid.'/'.$value->counstatus; ?>"><?php if($value->counstatus=='D') { ?>Inactive<?php }else { ?>Active<?php } ?></a>

                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>