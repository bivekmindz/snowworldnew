<div class="wrapper">

  <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Manage Location</h2>
                    </div>
                      <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this section , admin will be able to add the State names
and can view the Location list.</p>
                        </div>
                    </div>
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-12">

                                <div style="text-align:right;">
                                    <a href="<?php echo base_url();?>admin/geographic/addlocation"><button>ADD Location</button></a>
                                </div>
                                <!--  session flash message  -->
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?> 
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </div>
                                <table class="grid_tbl">
                                    <thead>
                                        <tr>
                                            <th>S.No.</th>
                                            <th>Location  Name</th>
                                            <th>Location Seat</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i=0; foreach ($data as $key => $value) { $i++; ?>
                                       <tr>

                                            <td><?php echo $i;?></td>
                                            <td><?php echo $value->locationname;?></td>
                                            <td><?php echo $value->totalseatperslot;?></td>
                                            <td><a href="<?php echo base_url()?>admin/geographic/locationupdate/<?php echo $value->locationid?>" ><i class="fa fa-pencil"></a></i>
                                            <a href="<?php echo base_url()?>admin/geographic/locationdelete/<?php echo $value->locationid?>" onclick="return confirm('Are you sure to delete?');"><i class="fa fa-trash fa-lg"></i></a>
                                            <a href="<?php echo base_url(); ?>admin/geographic/locationstatus/<?php echo $value->locationid.'/'.$value->lstatus; ?>"><?php if($value->lstatus=='D') { ?>Inactive<?php }else { ?>Active<?php } ?></a>

                                            </td>
                                        </tr>
                                        <?php } ?>
                                   </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>