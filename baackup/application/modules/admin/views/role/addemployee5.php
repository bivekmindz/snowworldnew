<!DOCTYPE html>
<html><script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"
        crossorigin="anonymous"></script>
<style>
    .error{
        color: red;
    }
</style>
</head>
<body ng-app="myApp" ng-controller="validateCtrl">
<div class="wrapper">

    <div class="col-lg-10 col-lg-push-2">
        <div class="row">

            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Add Employee</h2>
                    </div>
                    <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this section, you can add Employee!
                                <!-- <a href="admin/retailer/viewmanagers"><button type="button" style="float:right;">CANCEL</button></a> --></p>
                            <p><div class='flashmsg'>
                                <?php echo validation_errors(); ?>
                                <?php
                                if($this->session->flashdata('message')){
                                    echo $this->session->flashdata('message');
                                }
                                ?>
                            </div></p>
                        </div>
                    </div>
                    <form action="<?php echo base_url('admin/managerole/addemp');?>" id="mgrForm" name="mgrForm" method="post" enctype="multipart/form-data" >
                        <div class="page_box">
                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Name <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" name="s_username" id="s_username" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Employee ID <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input id="empid" title="PLESE FILL THIS FIELD" type="text" name="empid" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Email ID <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input  id="s_loginemail" type="email" name="s_loginemail" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Password <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input" style="position: relative;">
                                                <p id="msg"></p>
                                                <input type="text" title="Password Must contain 1 special character,1 uppercase, 1 lowercase and 1 numeric." name="s_loginpassword" id="s_loginpassword" autocomplete="off" />
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Contact No <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">

                                                <input type="text" title="PLESE FILL THIS FIELD" name="contact_no" id="contact_no" onkeyup="checknumber();"/>


                                                <!-- <input type="text" name="contact_no" id="contact_no" onkeyup="checknumber();" /> -->
                                            </div>
                                            <div class="tbl_input" id="errorsss"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Select Country <span
                                                        style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <select name="countryid" id="countryid" onchange="selectstates();">
                                                    <option value="">select Country</option>
                                                    <?php foreach ($vieww as $key => $value) { ?>
                                                        <option value="<?php echo $value->conid; ?>"><?php echo $value->name; ?></option>
                                                    <?php } ?>
                                                </select></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Select State <span
                                                        style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <select id="stateid" name="stateid" value="stateid"
                                                        onchange="selectcity();">
                                                    <option value="">Select State</option>
                                                </select>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Select City <span
                                                        style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <select id="cityid" onchange="selectlocation()" name="cityid"
                                                        value="cityid">
                                                    <option value="">Select city</option>
                                                </select>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>


                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Select Location <span
                                                        style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <select id="locationid" name="locationid" value="locationid">
                                                    <option value="">Select Location</option>
                                                </select>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>




                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input class="submitbtn"  type="submit" name="submit" value="Submit" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>




                    <div class="col-md-12">
                        <h2>Table</h2>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>S.NO</th>
                                    <th>NAME</th>
                                    <th>Id</th>
                                    <th>Email</th>
                                    <th>Password</th>
                                    <th>Contact</th>
                                    <th>location</th>
                                    <th>EDIT/DELETE</th>

                                </tr>
                                </thead>
                                <tbody>
                                <?php $i=1; foreach($vieww_employee as $v) {?>
                                    <tr>
                                        <td><?= $i;?></td>
                                        <td><?php echo $v->emp_name; ?></td>
                                        <td><?php echo $v->emp_main_id; ?></td>
                                        <td><?php echo $v->emp_email; ?></td>
                                        <td><?php echo $v->emp_password; ?></td>
                                        <td><?php echo $v->emp_contact; ?></td>
                                        <td><?php

                                            $parameter = array( 'act_mode'=>'getlocation',
                                                'Param1'=>$v->emp_location,
                                                'Param2'=>'',
                                                'Param3'=>'',
                                                'Param4'=>'',
                                                'Param5'=>'',
                                                'Param6'=>'',
                                                'Param7'=>'',
                                                'Param8'=>'',
                                                'Param9'=>'');
                                            //p($parameter);
                                            $path = api_url()."main_snowworld_v/getlocation/format/json/";
                                            $loc_name = curlpost($parameter,$path);
                                            $arr = (array)$loc_name;
                                            echo($arr[0]['locationname']);
                                            ?></td>
                                        <td>
                                            <a href="<?= base_url('admin/managerole/delemp/')."/".$v->emp_tbl_id ?>"  onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger btn">
                                                <span class="glyphicon glyphicon-trash"></span>
                                            </a>

                                        </td>

                                    </tr>

                                    <?php $i++;} ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
</body>

</html>

<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery-1.9.1.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>

<script>


    function selectstates() {

        var countryid = $('#countryid').val();
        $.ajax({
            url: '<?php echo base_url()?>admin/geographic/countrystate',
            type: 'POST',
            //dataType: 'json',
            data: {'countryid': countryid},
            success: function (data) {
                var option_brand = '<option value="">Select State</option>';
                $('#stateid').empty();
                $("#stateid").append(option_brand + data);

            }
        });

    }

    function selectcity() {

        var stateid = $('#stateid').val();
        $.ajax({
            url: '<?php echo base_url()?>admin/geographic/statecity',
            type: 'POST',
            //dataType: 'json',
            data: {'stateid': stateid},
            success: function (data) {
                var option_brand = '<option value="">Select City</option>';
                $('#cityid').empty();
                $("#cityid").append(option_brand + data);

            }
        });

    }

    function selectlocation() {
        var cityid = $('#cityid').val();
        $.ajax({
            url: '<?php echo base_url()?>admin/geographic/citylocation',
            type: "POST",
            data: {'cityid': cityid},
            success: function (data) {
                //console.log(data);
                var option_brand = '<option value="">Select Location</option>';
                $('#locationid').empty();
                $("#locationid").append(option_brand + data);

            }

        })

    }

    function update_branch(a) {
        $("#companyid_update").find("option").removeAttr('selected');
        $("#locationid_update").find("option").removeAttr('selected');
        //alert("hello");
        //var a = $(this).domElement();
        //console.log(a);
        $.ajax({
            url: '<?php echo  api_url()."main_snowworld_v/getlocation/format/json/" ?>',
            type: "POST",
            dataType :"json",
            data: {'act_mode':'getbranch_update','Param1': a},
            success: function (data) {
                $("#update_branch_id").val(a);
                $("#branch_name_update").val(data[0].branch_name);
                $("#companyid_update").find("option[value='"+data[0].branch_company+"']").attr('selected',true);
                $("#locationid_update").find("option[value='"+data[0].branch_location+"']").attr('selected',true);
                return false;
            }

        })


    }

    $(function() {
        // Initialize form validation on the registration form.
        // It has the name attribute "registration"
        $("#mgrForm").validate({
            // Specify validation rules
            rules: {
                // The key name on the left side is the name attribute
                // of an input field. Validation rules are defined
                // on the right side
                s_username: "required",
                empid: "required",
                s_loginemail: "required",
                s_loginpassword: "required",
                contact_no: {
                    "required": true,
                    "number": true
                },
                countryid: "required",
                stateid: "required",
                cityid: "required",
                locationid: "required",

            },
            // Specify validation error messages
            messages: {
                s_username: "Please enter name",
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
                form.submit();
            }
        });
    });


</script>