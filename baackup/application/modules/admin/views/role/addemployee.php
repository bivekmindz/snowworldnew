<!DOCTYPE html>
<html><script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"
        crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
<style>
    .error{
        color: red;
    }
    .tbl_input input[type="url"], .tbl_input input[type="number"], .tbl_input input[type="text"], .tbl_input input[type="email"], .tbl_input input[type="password"], .tbl_input select, .tbl_input textarea {
        padding: 9px 6px;
        border: solid 1px #E6E6E6;
        width: 100%;
    }
</style>
</head>
<body ng-app="myApp" ng-controller="myCtrl" data-ng-init="init()">
<div class="wrapper">

    <div class="col-lg-10 col-lg-push-2">
        <div class="row">

            <div class="page_contant pad_ding">
                <div class="col-lg-12">
                    <div class="page_name">
                        <h2>Add Employee</h2>
                    </div>
                    <div class="page_box">
                        <div class="col-lg-12">
                            <p> In this section, you can add Employee!
                                <!-- <a href="admin/retailer/viewmanagers"><button type="button" style="float:right;">CANCEL</button></a> --></p>
                            <p><div class='flashmsg'>
                                <?php echo validation_errors(); ?>
                                <?php
                                if($this->session->flashdata('message')){
                                    echo $this->session->flashdata('message');
                                }
                                ?>
                            </div></p>
                        </div>
                    </div>
                    <form action='<?php echo base_url('admin/managerole/addemp?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid']));?>' id="mgrForm" name="mgrForm" method="post" enctype="multipart/form-data" >
                        <div class="page_box">
                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Name <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" title="Please fill valid details." name="s_username" id="s_username" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Email ID <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input title="Please fill valid details."  id="s_loginemail" type="email" name="s_loginemail" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Password <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input" style="position: relative;">
                                                <p id="msg"></p>
                                                <input type="password" title="Please fill valid details." name="s_loginpassword" id="s_loginpassword" autocomplete="off" />
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Contact No <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">

                                                <input type="number" title="Please fill valid details."  name="contact_no" id="contact_no" onkeyup="checknumber();"/>


                                                <!-- <input type="text" name="contact_no" id="contact_no" onkeyup="checknumber();" /> -->
                                            </div>
                                            <div class="tbl_input" id="errorsss"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Select Branch <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">


                                                <?php
                                                if($this->session->userdata('snowworld')->EmployeeId){
                                                    ?>
                                                    <script>
                                                        $(function(){
                                                            $('#branchids option[value="<?= $this->session->userdata('snowworld')->branchid; ?>"]').attr('selected',true);
                                                            $("#branchids").attr('disabled',true);
                                                            //$("#branchids").attr('name',<?= $this->session->userdata('snowworld')->branchid;?>);
                                                        });
                                                    </script>

                                                    <?php
                                                }
                                                ?>



                                                <select title="Please fill valid details." name="branchids" required  id="branchids">
                                                    <?php foreach ($s_viewbranch as $key => $value) { ?>
                                                        <option value="<?php echo $value->branch_id; ?>"><?php echo $value->branch_name; ?></option>
                                                    <?php }?>
                                                </select></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sep_box">
                                <div class="col-lg-10">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <div class="tbl_text">Modules</div>
                                        </div>
                                        <div class="col-lg-9">
                                            <div class="tbl_input firstmainmenu">
                                                <select title="Please fill valid details."  name="mainmenu[]"
                                                         id="mainmenu" multiple="multiple"
                                                         ng-model="mainmenu" ng-change="submenu()" required>
                                                    <?php
                                                    foreach ($vieww_menu as $mainmenu) {
                                                        ?>
                                                        <option value="<?php echo $mainmenu->id; ?>"><?php echo $mainmenu->menuname; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>


                                            <div class="tbl_input secondmainmenu">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sep_box">
                                <div class="col-lg-10">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <div class="tbl_text"> SUB Modules</div>
                                        </div>
                                        <div class="col-lg-9">
                                            <div class="tbl_input firstmainmenu">
                                                <select title="Please fill valid details." name="mainmenu_sub[]" id="mainmenu_sub"
                                                        ng-model="mainmenu_sub" multiple="multiple"

                                                        required>
                                                    <option ng-repeat="(x,y) in data"
                                                            value="{{y.id}}">{{y.menuname}}
                                                    </option>
                                                </select>
                                            </div>


                                            <div class="tbl_input secondmainmenu">

                                                `
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input class="submitbtn"  type="submit" name="submit" value="Submit" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>




                    <div class="col-md-12">

                    <div class="add_comp">
                        <h2>Table</h2>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>S.NO</th>
                                    <th>NAME</th>
                                    <th>Email</th>
                                    <th>Password</th>
                                    <th>Contact</th>
                                    <th>Branch</th>
                                    <th>Roles</th>
                                    <th>EDIT/DELETE</th>

                                </tr>
                                </thead>
                                <tbody>
                                <?php $i=1; foreach($vieww_employee as $v) {?>
                                    <tr>
                                        <td><?= $i;?></td>
                                        <td><?php echo $v->emp_name; ?></td>
                                        <td><?php echo $v->emp_email; ?></td>
                                        <td><?php echo "*********"; //base64_decode($v->emp_password); ?></td>
                                        <td><?php echo $v->emp_contact; ?></td>
                                        <td><?php

                                            $parameter = array('act_mode' => 'getbranch',
                                                'Param1' => $v->emp_branch_id,
                                                'Param2' => '',
                                                'Param3' => '',
                                                'Param4' => '',
                                                'Param5' => '',
                                                'Param6' => '',
                                                'Param7' => '',
                                                'Param8' => '',
                                                'Param9' => '');
                                            //p($parameter);
                                            $path = api_url() . "main_snowworld_v/getlocation/format/json/";
                                            $br_name = (array)curlpost($parameter, $path);
                                            echo($br_name[0]['branch_name']);
                                            ?></td>
                                        <td><?php
                                        $sub_menu = explode(',',$v->submenuu);
                                        echo "<ol>";
                                        foreach ($sub_menu as $value)
                                        {
                                            echo "<li>".$value."</li>";
                                        }
echo "</ol>";


                                        ?></td>
                                        <td>


                                            <a href="<?= base_url('admin/managerole/delemp/'.$v->emp_tbl_id .'?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid']));?>"  onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger btn">
                                                <span class="glyphicon glyphicon-trash"></span></a>
                                    <a href='<?php echo base_url('admin/managerole/editempp/'.base64_encode($v->emp_tbl_id).'?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid']));?>'  class="btn btn-primary btn"><span class="glyphicon glyphicon-pencil"></span>











                                            </a>

                                        </td>

                                    </tr>

                                    <?php $i++;} ?>
                                </tbody>
                            </table>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
</body>

</html>

<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery-1.9.1.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>

<script>



    $(function() {
        // Initialize form validation on the registration form.
        // It has the name attribute "registration"
        $("#mgrForm").validate({
            // Specify validation rules
            rules: {
                // The key name on the left side is the name attribute
                // of an input field. Validation rules are defined
                // on the right side
                s_username: "required",
                empid: "required",
                s_loginemail: "required",
                s_loginpassword: "required",
                contact_no: {
                    "required": true,
                    "number": true
                },
                countryid: "required",
                stateid: "required",
                cityid: "required",
                locationid: "required",

            },
            // Specify validation error messages
            messages: {
                s_username: "Please enter name",
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
                form.submit();
            }
        });
    });


</script>

<script>
    $(function () {
        jQuery.noConflict();
        $('#mainmenu,#mainmenu_sub').select2({
            allowClear: true
        });
    });



    var app = angular.module('myApp', []);
    app.controller('myCtrl', function ($scope, $http) {
        $scope.isProcessing = true;


        $scope.submenu = function () {
            var data = $.param({
                mainmenuid: $scope.mainmenu
            });
            var config = {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                }
            };
            $http.post('<?php echo base_url('admin/managerole/getsubmenu'); ?>', data, config)
                .then(function (data, status, headers, config) {
                    //console.log(data.data.vieww_submenu);
                    $scope.data = data.data.vieww_submenu;
                });
        };





        $scope.myFunc = function (e) {

            $(function () {
                $('#mainmenu option').attr('selected', false);
//alert($("#rolename").val());
            })


            var data = $.param({
                roleid: $scope.role_id,
                empid: $scope.employee_id,
            });
            var config = {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                }
            };
            $http.post('<?php echo base_url('admin/managerole/get_menu_submenu'); ?>', data, config)
                .success(function (data, status, headers, config) {
//console.log(data);
                    if (data != ""){
                        if (data['0'].mainmenu) {

                            var mmenuu = (data['0'].mainmenu).split(',');
                            var subbmenuu = (data['0'].submenu).split(',');
//console.log(data);
                            for (var i = 0; i < mmenuu.length; i++) {
                                $('#mainmenu option[value="' + mmenuu[i] + '"]').attr('selected', true);
                            }

//alert();
                            var data = $.param({
                                mainmenuid: $("#mainmenu").val()
                            });
                            var config = {
                                headers: {
                                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                                }
                            };
                            $http.post('<?php echo base_url('admin/managerole/getsubmenu'); ?>', data, config)
                                .success(function (data, status, headers, config) {
                                    //console.log(data);
                                    $scope.data = data.vieww_submenu;
                                    //console.log(data.vieww_submenu['0'].id);
                                    for (var j = 0; j < subbmenuu.length; j++) {
                                        //console.log(subbmenuu[j]);
                                        //console.log($('#mainmenu_sub option'));
                                        //$('#mainmenu_sub option[value="' + subbmenuu[j] + '"]').attr('selected', true);
                                    }
                                });

                            $timeout(function () {
                                for (var j = 0; j < subbmenuu.length; j++) {
                                    //console.log(subbmenuu[j]);
                                    $('#mainmenu_sub option[value="' + subbmenuu[j] + '"]').attr('selected', true);
                                }
                                jQuery.noConflict();
                                $('#mainmenu,#mainmenu_sub').select2();
                            }, 1000);


                        }
                    }

                });


        };

    });

</script>