<!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->
<script src="<?php echo admin_url();?>js/moment.js"></script>

<link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"
rel = "stylesheet">
<script src = "https://code.jquery.com/jquery-1.10.2.js"></script>
<script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script>
$( function() {
} );
</script>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"
crossorigin="anonymous"></script>
<?php //p($vieww); ?>
<!-- jQuery Form Validation code -->
<style>
    .input-group-addon {
        position: absolute;
        left: 0;
        width: 100%!important;
        padding: 12px 12px!important;
        font-size: 14px;
        font-weight: 400;
        line-height: 1;
        color: #555;
        text-align: center;
        background-color: transparent!important;
        border: none!important;
        border-radius: 4px;
        top: 0px;
    }
    .input-group-addon  .glyphicon-time{
        float:right;
    }
input.error {
border: 1px solid red;
}

label.error {
border: 0px solid red;
color: red;
font-weight: normal;
display: inline;
}

/*.tab_ble{ width:100%; float:left;}
.tab_ble table{ width:100%; float:left;}
.tab_ble table tr{ width:100%; float:left;}*/
.tab_ble table tr th {
/*padding: 5px 35px 15px 35px;*/
text-align: center;
}

.tab_ble table tr td {
/*border: 1px solid #CCC;*/
text-align: center;

/*min-width: 100px;*/
}
 .lbl_box{
                    float:left;
                    margin-right:10px;
                }

                @media screen and (min-width: 320px) and (max-width: 680px){

.lbl_box{ width: 100%;
                    float:left;
                    margin-right:10px;
                }

                }
                .checkbox_lbl{
                    display: none;
                    z-index: -9999;
                }
                .checkbox_lbl + label{
                    position: relative;
                    padding-left:25px;
                    font-weight:normal;
                    cursor: pointer;
                }
                .checkbox_lbl + label:before{
                    position: absolute;
                    content: "";
                    width:19px;
                    height:19px;
                    border:solid 1px #abaaaa;
                    left:0;
                }
                .checkbox_lbl + label:after{
                       position: absolute;
                       content: "";
                       width: 10px;
                       height: 6px;
                       border-left: solid 2px #444;
                       border-bottom: solid 2px #444;
                       left: 5px;
                      transform: rotate(-45deg);
                      -o-transform: rotate(-45deg);
                      -webkit-transform: rotate(-45deg);
                      -moz-transform: rotate(-45deg);
                      transition: 0.3s;
                      -o-transition: 0.3s;
                      -webkit-transition: 0.3s;
                      -moz-transition: 0.3s;
                      top: 5px;
                      visibility: hidden;
                      opacity: 0;
                }
                .checkbox_lbl:checked + label:after{
                    visibility: visible;
                    opacity: 1;
                }
                .page_contant{
                  margin-top:0px;
                }
                .page_box{
                  box-shadow: none;
                }
               /* .tbl_overflow{
                    float:left;
                    width:100%;
                    overflow-x: auto;
                    padding-bottom: 10px;
                }*/
                #advance_search_d{
                  display: none;
                  margin-bottom:15px;
                  float:left;
                  width:100%;
                }

                .advance_btn{
                    float:left;
                    background: #0f71c3;
                    color:#fff;
                    padding:10px 15px;
                    cursor: pointer;
                }
                .table > thead > tr > th {
                 vertical-align: middle!important;

                  }

#container{width:100%;}
#left{float:left}
    #right{float:right}

#center{margin:0 auto;width:100px;float: left;padding: 1px 17px;}
</style>


<meta name="viewport" content="width=device-width, initial-scale=1">


<div class="wrapper">

<div class="col-lg-10 col-lg-push-2">
<div class="row">
<div class="page_contant">
<div class="col-lg-12">





    <div class="page_name">

        <h2>ALL ORDER LISTING</h2>
    </div>
    <div class="page_box">
    <div class="sep_box">
    <div class="col-lg-12">





        <div id="container">
            <div id="left"> <div class="advance_btn">Advance Search</div></div>

            <div id="center">
                <form method="post" action="<?= base_url('admin/ordercontrol/order_sess?empid='.$_GET['empid'].'&uid='.$_GET['uid']) ?>">


                    <div class="submit_tbl">
                        <input type="submit" value="Show All Records" class="btn_button sub_btn" name="Clear"></div></form>

        </div>

            <div id="right"><button id="btnExport" class="btn btn-primary">Download EXCEL</button></div></div>
        <table id="table_Excel" style="display: none" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>S.NO</th>
                <th>Username</th>
                <th>User Email</th>
                <th>User Contact</th>
                <th>Ticket no.</th>
                <th>No. of Peoples </th>
                <th>Price After Discount(in Rs.)</th>
                <th>Date of Booking</th>
                <th>Visit Date</th>
                <th>Visit Time</th>
                <th>paymentstatus</th>
                <th>Payment mode</th>
                <th>CCAVENUE Tracking ID</th>
                <th>Voucher Quantity</th>
                <th>Voucher Price(Total in Rs.)</th>
                <th>Coupon Code</th>
                <th>Print</th>
            </tr>
            </thead>
            <tbody>
            <?php $i = 1;
            //p($vieww_order[0]);
            foreach ($vieww_order as $v) {
                if($this->session->userdata('snowworld')->EmployeeId) {
                   if($this->session->userdata('snowworld')->branchid != $v->branch_id)
                    {
                        continue;
                    }
                }?>
                <tr>
                    <td style="width:500px;"><?= $i; ?></td>
                    <td><?php echo $v->billing_name; ?></td>
                    <td><?php echo $v->billing_email; ?></td>
                    <td><?php echo $v->billing_tel; ?></td>
                    <td><?php echo $v->ticketid; ?></td>
                    <td><?php echo $v->packpkg; ?></td>
                    <td><?php echo $v->total; ?></td>
                    <td><?php echo $v->order_status_date_time; ?></td>
                    <?php if($v->departuretime > 12){
                        $dt = $v->departuretime - 12;}



else{$dt = $v->departuretime; } ?>

                    <td><?php echo $v->departuredate."</td><td>".$dt.":".$v->frommin.$v->txtfromd." TO ".$v->tohrs.":".$v->tomin.$v->txttod; ?></td>
                    <td><?php echo $v->order_status; ?></td>
                    <td><?php echo $v->paymenttype; ?></td>
                    <td><?php echo $v->tracking_id; ?></td>
                    <td><?php

                        $ex = explode(',', $v->addon);
                        $ex1 = explode(',', $v->addonquantity);
                        $ex2 = explode(',', $v->orderaddon_print_status);
                        $ex3 = explode(',', $v->orderaddon_print_quantity);
                        $ex4 = explode(',', $v->addonprice);
                        foreach ($ex as $key => $a) {
                            if ($a == "") {
                                echo "N/A";
                                continue;
                            } elseif ($ex1[$key] != $ex3[$key]) {
                                echo '<button disabled="disabled" style="float: left; text-align: center" type="button" class="btn btn-danger" > ' . $a . ' PENDING(' . $ex1[$key] . ')</button><br>';

                            } else {
                                echo '<button style="float: left; text-align: center" type="button" class="btn btn-success" disabled="disabled">ALL ' . $a . '(' . $ex1[$key] . ')</button><br>';


                            }
                        }

                        ?></td>
                    <td><?php


                        foreach ($ex4 as $a4) {
                            if ($a == "") {
                                echo "N/A";
                                continue;
                            }
                            echo "<li>" . $a4 . "</li>";
                        }
                        ?></td>
                    <td><?php

                      echo $v->billing_promocode;
                        ?></td>
                    <td>

                            <?php if ($v->op_ticket_print_status == 0){echo "Pending";} ?>
                             <?php if ($v->op_ticket_print_status == 1){echo "Processing";} ?>
                            <?php if ($v->op_ticket_print_status == 2){echo "Printed";} ?>
                      <?php if ($v->op_ticket_print_status == 3){echo "CANCELED";} ?>

                    </td>

                </tr>

                <?php $i++;
            } ?>
            </tbody>
        </table>



    </div>
    </div>
        <div class="sep_box">
            <div class="col-lg-12">
                <!-- <div class='flashmsg'>
                    <?php echo validation_errors(); ?>
                    <?php
                    if ($this->session->flashdata('message')) {
                        echo $this->session->flashdata('message');
                    }
                    ?>
                </div> -->
            </div>
        </div>


        <div class="col-md-12">
            <div class="tab_ble">


            <div id="advance_search_d">
                <form method="post" action="<?= base_url('admin/ordercontrol/order_sess?empid='.$_GET['empid'].'&uid='.$_GET['uid']) ?>">
                <div class="sep_box">

                <div class="col-md-12">
                <div style="float:left;font-weight:600;margin-right:8px;">
                  Branch :
                  </div>

                    <?php
                    if($this->session->userdata('snowworld')->EmployeeId){
                        ?>
                        <input type="checkbox" checked="checked" name="filter_branch" class="checkbox_lbl"  id="filter_branch" value="<?php echo $this->session->userdata('snowworld')->branchid; ?>">

<?php
                        $parameter = array('act_mode' => 'getbranch',
                        'Param1' => $this->session->userdata('snowworld')->branchid,
                        'Param2' => '',
                        'Param3' => '',
                        'Param4' => '',
                        'Param5' => '',
                        'Param6' => '',
                        'Param7' => '',
                        'Param8' => '',
                        'Param9' => '');
                        //p($parameter);
                        $path = api_url() . "main_snowworld_v/getlocation/format/json/";
                        $br_name = (array)curlpost($parameter, $path);
                        echo($br_name[0]['branch_name']);
                        ?>


                        <?php
                    }else{
                    ?>
                     <?php foreach ($s_viewbranch as $key => $value) { ?>
                     <div class="lbl_box">
                            <input type="checkbox" name="filter_branch[]" class="checkbox_lbl"  id="filter_branch<?php echo $key;?>" value="<?php echo $value->branch_id; ?>">
                            <label for="filter_branch<?php echo $key;?>"><?php echo $value->branch_name; ?></label>
                            </div>

                        <?php }} ?>
                </div>
                </div>


                    <div class="sep_box">
                    <div class="col-md-4">
                    <div class="tbl_input">
                    <input type="text" placeholder="name" name="filter_name">
                    </div>
                    </div>
                     <div class="col-md-4">
                    <div class="tbl_input">
                     <input type="text" placeholder="email" name="filter_email">
                     </div>
                     </div>
                    </div>

                     <div class="sep_box">
                     <div class="col-md-4">
                    <div class="tbl_input">
                     <input type="text" placeholder="ticket no." name="filter_ticket">
                     </div>
                     </div>
                      <div class="col-md-4">
                    <div class="tbl_input">
                     <input type="text" placeholder="phone number" name="filter_mobile" maxlength="10">
                     </div>
                     </div>
                     </div>

                     <div class="sep_box">
                     <div class="col-md-4">
                    <div class="tbl_input">
                     <input type="text" placeholder="payment type" name="filter_payment">
                     </div></div>
                     </div>
                     <div class="sep_box">
                     <div class="col-md-4">
                    <div class="tbl_input">
                      <input type="text" id="filter_date1"  placeholder="DATE OF BOOKING FROM" name="filter_date_booking_from">
                    </div></div>
                    <div class="col-md-4">
                    <div class="tbl_input">
                      <input type="text" id="filter_date2"  placeholder="DATE OF BOOKING TO" name="filter_date_booking_to">
                      </div></div></div>

                  <div class="sep_box">
                  <div class="col-md-4">
                    <div class="tbl_input">
                  <input type="text" id="filter_date3"  placeholder="SESSION DATE FROM" name="filter_date_session_from">
                  </div></div>
                  <div class="col-md-4">
                    <div class="tbl_input">
                  <input type="text" id="filter_date4"  placeholder="SESSION DATE TO" name="filter_date_session_to">
                  </div></div>
                  </div>
                  <div class="sep_box">
                  <div class="col-md-4">
                    <div class="tbl_input">
                  <input type="text" id="filter_date5"  placeholder="PRINTED DATE FROM" name="filter_date_printed_from">
                  </div></div>
                  <div class="col-md-4">
                    <div class="tbl_input"><input type="text" id="filter_date6"  placeholder="PRINTED DATE TO" name="filter_date_printed_to">
                    </div></div></div>



                    <div class="sep_box">
                        <div class="col-md-4">
                            <div class="tbl_input">
                                <input type="text" id="filter_date_ticket"  placeholder="Visit Date" name="filter_date_ticket">
                            </div></div>
                       <!-- <div class="col-md-4">
                            <div class="tbl_input"><div class='input-group date' id='datetimepicker31'>
                                    <input  type='text' id="filter_date_session"  placeholder="Visit Time" name="filter_date_session"  class="date_pi" />
                                    <span class="input-group-addon">
                                <span class="glyphicon glyphicon-time"></span>
                            </span>
                                </div>
                            </div></div>--></div>





                  <div class="sep_box">
                  <div class="col-md-12">
                  <div style="float:left;font-weight:600;margin-right:8px;">
                  Status :
                  </div>
                  <div class="lbl_box">
                       <input name="filter_status[]" id="Pending" class="checkbox_lbl" type="checkbox" value="0"  <?php if ($v->op_ticket_print_status == 0){echo "selected";} ?>  >
                       <label for="Pending">Pending</label>
                       </div>
                       <div class="lbl_box">
                        <input name="filter_status[]" id="Processing" class="checkbox_lbl" type="checkbox" value="1" <?php if ($v->op_ticket_print_status == 1){echo "selected";} ?>>
                        <label for="Processing">Processing</label>
                        </div>
                        <div class="lbl_box">
                        <input name="filter_status[]" id="Printed" class="checkbox_lbl" type="checkbox" value="2" <?php if ($v->op_ticket_print_status == 2){echo "selected";} ?>>
                        <label for="Printed">Printed</label>
                        </div>
                        <div class="lbl_box">
                        <input name="filter_status[]" id="Canceled" class="checkbox_lbl" type="checkbox" value="3" <?php if ($v->op_ticket_print_status == 2){echo "selected";} ?>>
                        <label for="Canceled">Canceled</label>
                        </div>
                        </div>
                  </div>





                    <div class="sep_box">
                        <div class="col-md-12">
                            <div style="float:left;font-weight:600;margin-right:8px;">
                                TIMESLOT :
                            </div>
                            <div class="lbl_box">
                            <input type="checkbox" id="cb_1" class="checkbox_lbl">
                                <label for="cb_1">All</label>
                            </div>
                    <?php //p($vieww_time);
                    foreach ($vieww_time as $key => $value) { ?>
                        <div class="lbl_box">

                            <input type="checkbox" name="filter_tim[]" class="checkbox_lbl"  id="filter_tim<?php echo $key;?>" value="<?php echo $value->timeslot_from; ?>">
                            <label for="filter_tim<?php echo $key;?>"><?php echo $value->timeslot_from >12? ($value->timeslot_from - 12).":00 PM" : $value->timeslot_from.":00 AM"; ?></label>
                        </div>

                    <?php }
                    ?>
                    </div>
                    </div>



                    <div class="sep_box">
                        <div class="col-md-12">
                            <div style="float:left;font-weight:600;margin-right:8px;">
                                STATUS :
                            </div>
                            <div class="lbl_box">
                                <input type="checkbox" id="cb_11" class="checkbox_lbl">
                                <label for="cb_11">All</label>
                            </div>
                            <?php //p($vieww_status);
                            foreach ($vieww_status as $key => $value) {
                                if($value->order_status == ""){continue;}?>
                                <div class="lbl_box">

                                    <input type="checkbox" name="filter_sta[]" class="checkbox_lbl"  id="filter_sta<?php echo $key;?>" value="<?php echo $value->order_status; ?>">
                                    <label for="filter_sta<?php echo $key;?>"><?php echo ($value->order_status==""? "Blank": $value->order_status); ?></label>
                                </div>

                            <?php }
                            ?>
                        </div>
                    </div>




                  <div class="sep_box">
                  <div class="col-md-12">
                  <div class="submit_tbl">
                  <input type="submit" value="Search" class="btn_button sub_btn"></div>
                  *For all data leave all fields blank and click Search button</div>
                  </div>

                </form>
                </div>
                <div class="tbl_overflow">
                    <table id="table1" style="width: 100%" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>S.NO</th>
                            <th>Username</th>
                            <th>User Email</th>
                            <th>User Contact</th>
                            <th>Ticket no.</th>
                            <th>No. of Peoples </th>
                            <th>Price After Discount(in Rs.)</th>
                            <th>Date of Booking</th>
                            <th>Visit Date - Time</th>
                            <th>paymentstatus</th>
                            <th>Payment mode</th>
                            <th>CCAVENUE Tracking ID</th>
                            <th>Voucher Quantity</th>
                            <th>Voucher Price(Total in Rs.)</th>
                            <th>Branch</th>
                            <th>Print</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i = 1;
                        //p($vieww_order[0]);
                        foreach ($vieww_order as $v) {
                            if($this->session->userdata('snowworld')->EmployeeId) {
                               if($this->session->userdata('snowworld')->branchid != $v->branch_id)
                                {
                                    continue;
                                }
                            }?>
                            <tr>
                                <td><?= $i; ?></td>
                                <td><?php echo $v->billing_name; ?></td>
                                <td><?php echo $v->billing_email; ?></td>
                                <td><?php echo $v->billing_tel; ?></td>
                                <td><?php echo $v->ticketid; ?></td>
                                <td><?php echo $v->packpkg; ?></td>
                                <td><?php echo $v->total; ?></td>
                                <td><?php  if($v->order_status_date_time!='') {  echo $v->order_status_date_time;} else { echo $v->addedon; } ?></td>
                                <?php if($v->departuretime > 12){
                                    $dt = $v->departuretime - 12;}


else{$dt = $v->departuretime; } 
if($v->tohrs>0)
{
$v->tohrs=$v->tohrs;
$v->txttod=$v->txttod;
}
else
{
$v->tohrs=$v->tohrs+12;
$v->txttod="PM";
}

?>

                                <td><?php echo $v->departuredate."<br>".$dt.":".$v->frommin.$v->txtfromd." TO ".$v->tohrs.":".$v->tomin.$v->txttod; ?></td>
                                <td><?php echo $v->order_status; ?></td>
                                <td><?php echo $v->paymenttype; ?></td>
                                <td><?php echo $v->tracking_id; ?></td>
                                <td><?php

                                    $ex = explode(',', $v->addon);
                                    $ex1 = explode(',', $v->addonquantity);
                                    $ex2 = explode(',', $v->orderaddon_print_status);
                                    $ex3 = explode(',', $v->orderaddon_print_quantity);
                                    $ex4 = explode(',', $v->addonprice);
                                    foreach ($ex as $key => $a) {
                                        if ($a == "") {
                                            echo "N/A";
                                            continue;
                                        } elseif ($ex1[$key] != $ex3[$key]) {
                                            echo '<button disabled="disabled" style="float: left; text-align: center" type="button" class="btn btn-danger" > ' . $a . ' PENDING(' . $ex1[$key] . ')</button><br>';

                                        } else {
                                            echo '<button style="float: left; text-align: center" type="button" class="btn btn-success" disabled="disabled">ALL ' . $a . '(' . $ex1[$key] . ')</button><br>';


                                        }
                                    }

                                    ?></td>
                                <td><?php


                                    foreach ($ex4 as $a4) {
                                        if ($a == "") {
                                            echo "N/A";
                                            continue;
                                        }
                                        echo "<li>" . $a4 . "</li>";
                                    }
                                    ?></td>
                                <td><?php
echo $v->branch_name;
                                    ?></td>
                                <td>
                                    <select data-id="<?= $v->pacorderid ?>" class="ticket<?= $v->pacorderid ?>" id="op_ticket_print_status" onchange="return funct_change_status('<?= $v->pacorderid ?>',this.value)" name="op_ticket_print_status" <?php if ($v->op_ticket_print_status == 2){echo "disabled";} ?>>
                                        <option value="0"  <?php if ($v->op_ticket_print_status == 0){echo "selected";} ?>  >Pending</option>
                                        <option value="1" <?php if ($v->op_ticket_print_status == 1){echo "selected";} ?>>Processing</option>
                                        <option value="2" <?php if ($v->op_ticket_print_status == 2){echo "selected";} ?>>Printed</option>
                                        <option value="3" <?php if ($v->op_ticket_print_status == 3){echo "selected";} ?>>CANCELED</option>
                                    </select>
                                </td>

                            </tr>

                            <?php $i++;
                        } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


    </div>

</div>
</div>
</div>
</div>
</div>

<script>

function funct_change_status(a,b) {
if(b == 2) {
$(".ticket" + a).attr('disabled', true);
}
$.ajax({
url: '<?php echo base_url()?>admin/ordercontrol/op_ticket_print_status',
type: 'POST',
data: {'pacorderid': a,'op_ticket_print_status':b},
dataType: "JSON",
success: function(data){


//console.log(data.s[0].ticket_status);
if(data.s[0].ticket_status == 2)
{
    a.attr('disabled',true);

}
//console.log(data1);
}
});
//var b = $(this).val();
// alert(b);return false;

}

/*    $(function () {
$("#op_ticket_print_status").change(function () {
alert("hello"); return false;
var a = $(this);
if($("#op_ticket_print_status").val() == 2)
{
a.attr('disabled',true);

}
//return false;
$.ajax({
url: ' echo base_url()?>admin/ordercontrol/op_ticket_print_status',
type: 'POST',
data: {'pacorderid': $(this).attr('data-id'),'op_ticket_print_status':$("#op_ticket_print_status").val()},
dataType: "JSON",
success: function(data){


//console.log(data.s[0].ticket_status);
    if(data.s[0].ticket_status == 2)
    {
        a.attr('disabled',true);

    };
   //console.log(data1);
}
});
});
});*/

function ticket_print(a) {
//alert(a);
window.print();

}

function update_com(a, b) {
//alert(a +b);
$("#com_name_update").val(a);
$("#com_id").val(b);
$("#update_com").css('display', 'block');
$("#add_com").css('display', 'none');
$("#com_name_update").focus();

}
function UpdateCancel() {
$("#add_com").css('display', 'block');
$("#update_com").css('display', 'none');
return false;
}


</script>



<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>

<!-- Include Date Range Picker -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
                                                                                                                        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

<script>
$(document).ready(function(){
$(".advance_btn").click(function(){
    $("#advance_search_d").slideToggle();
});

})
</script>

<script src="<?php echo admin_url();?>js/bootstrap-datetimepicker.min.js"></script>
<link href="<?php echo admin_url();?>css/bootstrap-datetimepicker.css" rel="stylesheet">

<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<link rel="stylesheet"

href="https://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css"></style>
<script type="text/javascript"

src="https://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
                                                     <script type="text/javascript"

src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

                                                           <script>
                                                           $(document).ready(function(){


var table = $('#table1').DataTable();
$.noConflict();
var date_input=$('input[name="filter_date_booking_from"],input[name="filter_date_booking_to"],input[name="filter_date_printed_from"],input[name="filter_date_printed_to"]'); //our date input has the name "date"
var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
date_input.datepicker({
format: 'yyyy-mm-dd',
container: container,
todayHighlight: true,
autoclose: true,
});


var date_input=$('input[name="filter_date_session_from"],input[name="filter_date_session_to"],input[name="filter_date_ticket"]'); //our date input has the name "date"
var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
date_input.datepicker({
    format: 'dd/mm/yyyy',
    container: container,
    todayHighlight: true,
    autoclose: true,
});
/*$('#filter_date1').datepicker('setDate', 'today');
$('#filter_date2').datepicker('setDate', 'today');
$('#filter_date3').datepicker('setDate', 'today');
$('#filter_date4').datepicker('setDate', 'today');*/
});
//$('#table1').dataTable();
</script>





  <script type="text/javascript">
$(function () {
$('#datetimepicker31,#datetimepicker41').datetimepicker({
    format: 'LT'
});

$('.dpxx').datetimepicker({
    format: 'LT'
});


});

$(document).ready(function() {
$("#btnExport").click(function(e) {
    e.preventDefault();

//getting data from our table
var data_type = 'data:application/vnd.ms-excel';
    var table_div = document.getElementById('table_Excel');
    var table_html = table_div.outerHTML.replace(/ /g, '%20');

    var a = document.createElement('a');
    a.href = data_type + ', ' + table_html;
    a.download = 'exported_table_' + Math.floor((Math.random() * 9999999) + 1000000) + '.xls';
    a.click();
});
var x = $(window).height();
var y = x-55;
$(".page_contant").css({"height":y+"px","min-height":"auto"});
var tp = $("#table1");
});
</script>