<?php //$this->load->view('helper/nav'); ?>


<!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->
<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"
        crossorigin="anonymous"></script>
<?php //p($s_viewbranch); ?>
<!-- jQuery Form Validation code -->
<script>


    $(function () {

        $("#countryid").trigger('change');
        // Initialize form validation on the registration form.
        // It has the name attribute "registration"
        $("#add_branch").validate({
            // Specify validation rules
            rules: {
                // The key name on the left side is the name attribute
                // of an input field. Validation rules are defined
                // on the right side
                locationid: "required",
                companyid: "required",
                branch_name: "required",
                branch_internethandle: "required",
                branch_url: "required",
                branch_addr: "required",

            },
            // Specify validation error messages
            messages: {
                countryid: "Please select country",
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function (form) {
                        form.submit();

            }
        });
    });


</script>
<style>
    input.error {
        border: 1px solid red;
    }

    label.error {
        border: 0px solid red;
        color: red;
        font-weight: normal;
        display: inline;
    }
</style>


<meta name="viewport" content="width=device-width, initial-scale=1">


<div class="wrapper">

    <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">

                        <h2>Update BRANCH</h2>
                    </div>
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-12">
                                <!--<div style="text-align:right;">
                                    <a href=""><button>CANCEL</button></a>
                                </div>-->
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?>
                                    <?php
                                    if ($this->session->flashdata('message')) {
                                        echo $this->session->flashdata('message');
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>


                        <form  name="add_branch" id="add_branch"
                              method="post" enctype="multipart/form-data">


                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Location <span
                                                        style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <select name="locationid" id="locationid" onchange="selectstates();">
                                                    <?php  foreach ($vieww_location as $key => $value) { ?>
                                                        <option <?php if($s_viewbranch[0]->branch_location == $value->locationid){echo "selected";} ?> value="<?php echo $value->locationid; ?>"><?php echo $value->locationname; ?></option>
                                                    <?php } ?>
                                                </select></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <script>
                                function selectstates() {

                                    var countryid = $('#countryid').val();
                                    $.ajax({
                                        url: '<?php echo base_url()?>admin/geographic/countrystate',
                                        type: 'POST',
                                        //dataType: 'json',
                                        data: {'countryid': countryid},
                                        success: function (data) {
                                            var option_brand = '<option value="">Select State</option>';
                                            $('#stateid').empty();
                                            $('#cityid').empty();
                                            $('#locationid').empty();
                                            $("#stateid").append(option_brand + data);

                                        }
                                    });

                                }
                                function selectcity() {

                                    var stateid = $('#stateid').val();
                                    $.ajax({
                                        url: '<?php echo base_url()?>admin/geographic/statecity',
                                        type: 'POST',
                                        //dataType: 'json',
                                        data: {'stateid': stateid},
                                        success: function (data) {
                                            var option_brand = '<option value="">Select City</option>';
                                            $('#cityid').empty();
                                            $('#locationid').empty();
                                            $("#cityid").append(option_brand + data);

                                        }
                                    });

                                }
                                function selectlocation() {
                                    var cityid = $('#cityid').val();
                                    $.ajax({
                                        url: '<?php echo base_url()?>admin/geographic/citylocation',
                                        type: "POST",
                                        data: {'cityid': cityid},
                                        success: function (data) {
                                            //console.log(data);
                                            var option_brand = '<option value="">Select Location</option>';
                                            $('#locationid').empty();
                                            $("#locationid").append(option_brand + data);

                                        }

                                    })

                                }
                            </script>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Select Company <span
                                                        style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <select name="companyid" id="companyid">
                                                    <?php foreach ($vieww_company as $key => $value) { ?>
                                                        <option  <?php if($s_viewbranch[0]->branch_company == $value->comp_id){echo "selected";} ?> value="<?php echo $value->comp_id; ?>"><?php echo $value->comp_name; ?></option>
                                                    <?php } ?>
                                                </select></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Branch Name<span style="color:red;font-weight: bold;">*</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" value="<?= $s_viewbranch[0]->branch_name; ?>" id="branch_name" name="branch_name">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                          <!--  <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Select LOGO <span
                                                        style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="file" id="branch_logo_image"
                                                       accept="image/gif, image/jpeg, image/png, image/jpg"
                                                       name="branch_logo_image">
                                            </div>
                                        </div>
                                    </div>
                                    `
                                </div>
                            </div>-->

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Select Background Image <span
                                                        style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="file" id="mail_image_background"
                                                       accept="image/gif, image/jpeg, image/png, image/jpg"
                                                       name="mail_image_background">
                                                Previous image :
                                                <img height="100px;" src="<?= base_url('/assets/admin/images/'.$s_viewbranch[0]->branch_background) ?>">
                                                <input type="hidden" name="mail_image_background_hidden" value="<?=$s_viewbranch[0]->branch_background;?>">
                                                     `
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Internet Handling Charges<span
                                                        style="color:red;font-weight: bold;">*</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="number" value="<?= $s_viewbranch[0]->branch_internet_handling_charge?>" id="branch_internethandle"
                                                       name="branch_internethandle">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Url <span
                                                        style="color:red;font-weight: bold;">*</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="url"  value="<?= $s_viewbranch[0]->branch_url?>"  id="branch_url" name="branch_url">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Branch Address <span
                                                        style="color:red;font-weight: bold;">*</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <textarea type="text"  value=""  id="branch_addr" name="branch_addr"><?= $s_viewbranch[0]->branch_add; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-8">
                                            <div class="submit_tbl">
                                                <input id="submitBtn" type="submit" name="submit" value="Submit"
                                                       class="btn_button sub_btn"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </form>




                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


<script>


    function update_branch(a) {
        $("#companyid_update").find("option").removeAttr('selected');
        $("#locationid_update").find("option").removeAttr('selected');
        //alert("hello");
        //var a = $(this).domElement();
        //console.log(a);
        $.ajax({
            url: '<?php echo api_url() . "main_snowworld_v/getlocation/format/json/" ?>',
            type: "POST",
            dataType: "json",
            data: {'act_mode': 'getbranch_update', 'Param1': a},
            success: function (data) {
                $("#update_branch_id").val(a);
                $("#branch_name_update").val(data[0].branch_name);
                $("#companyid_update").find("option[value='" + data[0].branch_company + "']").attr('selected', true);
                $("#locationid_update").find("option[value='" + data[0].branch_location + "']").attr('selected', true);
                return false;
            }

        })


    }


</script>