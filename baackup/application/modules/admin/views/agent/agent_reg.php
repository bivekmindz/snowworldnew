<?php


//pend($this->session->all_userdata());
//pend($viewcity);
//pend($data);
/*pend($viewcountry);*/ ?>
<script src="<?php echo base_url() ?>assets/js/jquery.validate.min.js"></script>

<div class="wrapper">
<style>
.error {
color: red;

}

.tab-pane h4 b {
color: #1b78c7;
}

.modal-body {
width: 100%;
float: left;
position: relative;
}
</style>

<div class="col-lg-10 col-lg-push-2">


<div class="row">

<div class="page_contant">


    <div class="col-lg-12">
        <div class="page_name">

            <h2>Manage Agent</h2>

        </div>

        <div class="page_box">

            <div class="col-lg-12">
            
            

                <div id="content">
                    <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
                        <li class="active"><a href="#Personal" data-toggle="tab">Personal</a></li>
                        <li><a href="#Official" data-toggle="tab">Official</a></li>
                        <li><a href="#StatutoryRequirements" data-toggle="tab"> Statutory Requirements </a></li>
                         <li><a href="#Bankdetails" data-toggle="tab">Bank Details</a></li>
                        <li><a href="#location" data-toggle="tab">Document/Amount</a></li>
                    </ul>

                    <form id="addCont" action="" method="post" enctype="multipart/form-data">
                        <div id="my-tab-content" class="tab-content">

                            <!----------------------------------------------------------------------------------------------------------------------------->


                            <div class="tab-pane active" id="Personal">
                                <h4 class="text-center"><b>Personal</b></h4>
                                <div class="form-conte">
                                    <!--  session flash message  -->
                                    <div class='flashmsg'>
                                        <?php echo validation_errors(); ?>
                                        <?php
                                        if ($this->session->flashdata('message')) {
                                            echo $this->session->flashdata('message');
                                        }
                                        ?>
                                    </div>
                                    <!--Personal form start-->


                                    <div style="color: #1b6d57" class="text-center" id="msgdiv"></div>
                                    <div class="sep_box">
                                        <div class="col-lg-4">

                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="tbl_text">Login Type <span
                                                                style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="tbl_input">


                                                        <select name="logintype" class="form-control-1"
                                                        >
                                                            <option value="1">Corporate Login</option>
                                                            <option value="2">Officer Login</option>

                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="tbl_text">Country <span
                                                                style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="tbl_input"><input type="text"
                                                                                  name="emailcountry"
                                                                                  class="form-control-1"
                                                                                  placeholder="Enter Your Country"


                                                                                  title="Enter Your Country"
                                                                                  maxlength="50"></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="tbl_text">Agency Name <span
                                                                style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="tbl_input"><input type="text"
                                                                                  name="agencyname"
                                                                                  class="form-control-1"
                                                                                  placeholder="Enter Agency Name"


                                                                                  title="Agency Name should only contain  letters. e.g. John"
                                                                                  maxlength="60">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="sep_box">
                                        <div class="col-lg-4">

                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="tbl_text">Registered / Head Office
                                                        Address </label>
                                                        <span
                                                                style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="tbl_input"><input type="text"
                                                                                  name="office_address"
                                                                                  class="form-control-1"
                                                                                  placeholder="Enter Office Address"

                                                                                  title="Enter Registered / Head Office Address "
                                                                                  maxlength="60">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="tbl_text">City <span
                                                                style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="tbl_input"><input type="text" name="city"
                                                                                  class="form-control-1"
                                                                                  placeholder="Enter City"


                                                                                  title="City Name should only contain  letters. e.g. Delhi"
                                                                                  maxlength="60"></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="tbl_text">State <span
                                                                style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="tbl_input"><input type="text" name="state"
                                                                                  class="form-control-1"
                                                                                  placeholder="Enter State"


                                                                                  title="State Name should only contain  letters. e.g. Delhi"
                                                                                  maxlength="60">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="sep_box">
                                        <div class="col-lg-4">

                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="tbl_text">User Email id <span
                                                                style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="tbl_input"><input type="email"
                                                                                  name="username"
                                                                                  class="form-control-1"
                                                                                  placeholder="Enter Your Email"


                                                                                  title="The input is not a valid email address"
                                                                                  maxlength="50">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="tbl_text">Password <span
                                                                style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="tbl_input"><input type="text"
                                                                                  name="password"
                                                                                  class="form-control-1"
                                                                                  placeholder="Enter Password"
                                                                                  maxlength="35">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="tbl_text">Pincode <span
                                                                style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="tbl_input"><input type="text" name="pincode"
                                                                                  class="form-control-1"
                                                                                  placeholder="Enter Your Pincode"

                                                                                  title="Your Pincode Should Only Contain  Number. e.g. 110011"
                                                                                  maxlength="10">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="sep_box">
                                        <div class="col-lg-4">

                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="tbl_text">Telephone <span
                                                                style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="tbl_input"><input type="tel"
                                                                                  name="telephone"
                                                                                  class="form-control-1"
                                                                                  placeholder="Enter Your Telephone Number"

                                                                                  title="Your Phone Number Should Only Contain  Number. e.g. 9999999999"
                                                                                  maxlength="20">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="tbl_text">Mobile <span
                                                                style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="tbl_input"><input type="tel" name="mobile"
                                                                                  class="form-control-1"
                                                                                  placeholder="Enter Your Phone Number"
                                                                                  title="Your Phone Number Should Only Contain  Number. e.g. 9999999999"
                                                                                  maxlength="10"></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="tbl_text">Fax <span
                                                                style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="tbl_input"><input type="text" name="fax"
                                                                                  class="form-control-1"
                                                                                  placeholder="Enter Your Fax Number"
                                                                                  title="Your Fax Number Should Only Contain  Number. e.g. 9999999999"
                                                                                  maxlength="20">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="sep_box">
                                        <div class="col-lg-4">

                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="tbl_text">Website
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="tbl_input"><input type="text" name="website"
                                                                                  class="form-control-1"
                                                                                  placeholder="Enter Your Website"
                                                                                  maxlength="60">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="tbl_text">Email <span
                                                                style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="tbl_input"><input type="email" name="email"
                                                                                  class="form-control-1"
                                                                                  placeholder="Enter Your Email"


                                                                                  title="The input is not a valid email address"
                                                                                  maxlength="50"></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="tbl_text">Status <span
                                                                style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="tbl_input"><select name="emailstatus"
                                                                                   class="form-control-1">
                                                            <option value="International">International
                                                            </option>
                                                            <option value="Domestic">Domestic</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="sep_box">
                                        <div class="col-lg-4">

                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="tbl_text">Currency in which payments will be
                                                        made: <span
                                                                style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="tbl_input"><input type="text"
                                                                                  name="emailcurrency"
                                                                                  class="form-control-1"
                                                                                  placeholder="Enter Your Currency"

                                                                                  title="Enter Your Currency"
                                                                                  maxlength="50">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>


                                    <!--country form end-->

                                    <!--country listing start--------------------------------------------------------------------------------------------->


                                </div>
                            </div>


                            <!------------------------------------------------------------------------------------------------------------------------->

                            <!--#Official start-->

                            <div class="tab-pane" id="Official">
                                <h4 class="text-center"><b>Official to Lialise with Chiliad Procons</b></h4>
                                <div style="color: #1b6d57" class="text-center" id="msgdiv_state"></div>
                                <div id="state_table">


                                    <div class="sep_box">
                                        <div class="col-lg-4">

                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="tbl_text">Primary Contact Person <span
                                                                style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="tbl_input"><input type="text"
                                                                                  name="primarycontact_name"
                                                                                  class="form-control-1"
                                                                                  placeholder="Enter Primary Contact Person"

                                                                                  title="Primary Contact Person Name should only contain  letters. e.g. John"
                                                                                  maxlength="60">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="tbl_text">Email <span
                                                                style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="tbl_input"><input type="email"
                                                                                  name="primarycontact_email_mobile"
                                                                                  class="form-control-1"
                                                                                  placeholder="Enter  Email"


                                                                                  title="The input is not a valid email address"
                                                                                  maxlength="50"></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="tbl_text">Mobile <span
                                                                style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="tbl_input"><input type="tel"
                                                                                  name="primarycontact_telephone"
                                                                                  class="form-control-1"
                                                                                  placeholder="Enter Mobile Number"

                                                                                  title="Mobile Number Should Only Contain  Letters. e.g. 9999999999"
                                                                                  maxlength="10">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="sep_box">
                                        <div class="col-lg-4">

                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="tbl_text">Secondary Contact Person <span
                                                                style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="tbl_input"><input type="text"
                                                                                  name="secondrycontact_name"
                                                                                  class="form-control-1"
                                                                                  placeholder="Enter Secondary Contact Person"

                                                                                  title="Secondary Contact Person Name should only contain  letters. e.g. John"
                                                                                  maxlength="60">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="tbl_text">Email <span
                                                                style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="tbl_input"><input type="email"
                                                                                  name="secondrycontact_email_phone"
                                                                                  class="form-control-1"
                                                                                  placeholder="Enter  Email"


                                                                                  title="The input is not a valid email address"
                                                                                  maxlength="50"></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="tbl_text">Residence Telephone <span
                                                                style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="tbl_input">< <input type="tel"
                                                                                    name="secondrycontact_telephone"
                                                                                    class="form-control-1"
                                                                                    placeholder="Enter  Phone Number"

                                                                                    title=" Phone Number Should Only Contain  Letters. e.g. 9999999999"
                                                                                    maxlength="10">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="sep_box">
                                        <div class="col-lg-4">

                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="tbl_text">Management Executives <span
                                                                style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="tbl_input"><input type="text"
                                                                                  name="management_executives_name"
                                                                                  class="form-control-1"
                                                                                  placeholder="Management Executives  Name"

                                                                                  title=" Management Executives should only contain  letters. e.g. Singh"
                                                                                  maxlength="60">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="tbl_text">Email <span
                                                                style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="tbl_input"><input type="email"
                                                                                  name="management_executives_email_phone"
                                                                                  class="form-control-1"
                                                                                  placeholder="Management Executives Email"


                                                                                  title="The input is not a valid email address"
                                                                                  maxlength="50"></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="tbl_text">Branches( if any) <span
                                                                style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="tbl_input"><input type="text"
                                                                                  name="branches"
                                                                                  class="form-control-1"
                                                                                  placeholder="Enter Branches">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="sep_box">
                                        <div class="col-lg-4">

                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="tbl_text">Year of establishment <span
                                                                style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="tbl_input"><input type="text"
                                                                                  name="yearofestablism"
                                                                                  class="form-control-1"

                                                                                  placeholder="Enter year of establism">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="tbl_text">Type of Organization <span
                                                                style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="tbl_input"><select class="form-control-1"
                                                                                   name="organisationtype">
                                                            <option value="Proprietorship">Proprietorship
                                                            </option>
                                                            <option value="Limited">Limited</option>

                                                            <option value="Limited Liability">Limited
                                                                Liability
                                                            </option>

                                                            <option value="Any other">Any other</option>

                                                        </select></div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>


                                </div>
                            </div>


                            <!--Official end-->


                            <!--Statutory Requirements Start-->

                            <div class="tab-pane" id="StatutoryRequirements">
                                <h4 class="text-center"><b>Statutory Requirements : Mention NA where not
                                        applicable</b></h4>
                                <div style="color: #1b6d57" class="text-center" id="msgdiv_state"></div>
                                <div id="state_table">


                                    <div class="sep_box">
                                        <div class="col-lg-4">

                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="tbl_text">LST No <span
                                                                style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="tbl_input"><input type="text" name="lstno"
                                                                                  class="form-control-1"
                                                                                  placeholder="Enter LST No"
                                                                                  title="Enter LST No"
                                                                                  maxlength="60">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="tbl_text">CST No <span
                                                                style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="tbl_input"><input type="text" name="cstno"
                                                                                  class="form-control-1"
                                                                                  placeholder="Enter CST No"
                                                                                  title="Enter CST No"
                                                                                  maxlength="60"></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="tbl_text">Excise Registration No <span
                                                                style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="tbl_input"><input type="text"
                                                                                  name="registrationno"
                                                                                  class="form-control-1"
                                                                                  placeholder="Enter Excise Registration No"

                                                                                  title="Enter Excise Registration No"
                                                                                  maxlength="60">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="sep_box">
                                        <div class="col-lg-4">

                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="tbl_text">VAT No. / TIN No <span
                                                                style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="tbl_input"><input type="text" name="vatno"
                                                                                  class="form-control-1"
                                                                                  placeholder="Enter VAT No. / TIN No"

                                                                                  title="Enter VAT No. / TIN No"
                                                                                  maxlength="60">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="tbl_text">PAN No <span
                                                                style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="tbl_input"><input type="text" name="panno"
                                                                                  class="form-control-1"
                                                                                  placeholder="Enter PAN No"
                                                                                  title="Enter PAN No"
                                                                                  maxlength="60"></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="tbl_text">Service Tax Registration No <span
                                                                style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="tbl_input"><input type="text"
                                                                                  name="servicetaxno"
                                                                                  class="form-control-1"
                                                                                  placeholder="Enter Service Tax Registration No"

                                                                                  title="Enter Service Tax Registration No"
                                                                                  maxlength="60">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="sep_box">
                                        <div class="col-lg-4">

                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="tbl_text">TAN No
                                                        <span
                                                                style="color:red;font-weight: bold;"><?php echo "*"; ?></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="tbl_input"><input type="text" name="tanno"
                                                                                  class="form-control-1"
                                                                                  placeholder="Enter TAN No"
                                                                                  title="Enter TAN No"
                                                                                  maxlength="60">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="tbl_text">PF No <span
                                                                style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="tbl_input"><input type="text" name="pfno"
                                                                                  class="form-control-1"
                                                                                  placeholder="Enter PF No"
                                                                                  title="Enter PF No"
                                                                                  maxlength="60"></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="tbl_text">ESIC No <span
                                                                style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="tbl_input"><input type="text" name="esisno"
                                                                                  class="form-control-1"
                                                                                  placeholder="Enter ESIC No"
                                                                                  title="Enter ESIC No"
                                                                                  maxlength="60">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="sep_box">
                                        <div class="col-lg-4">

                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="tbl_text">Office / Establishment / Factory
                                                        Registration
                                                        No./ Labour Licence no <span
                                                                style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="tbl_input"><input type="text"
                                                                                  name="officeregistrationno"
                                                                                  class="form-control-1"
                                                                                  placeholder="Enter Office / Establishment / Factory Registration
                No./ Labour Licence no" title="Enter Office / Establishment / Factory Registration
                No./ Labour Licence no" maxlength="60"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="tbl_text">Any exemption from tax <span
                                                                style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="tbl_input"><input type="text"
                                                                                  name="exceptiontax"
                                                                                  class="form-control-1"
                                                                                  placeholder="Enter Any exemption from tax"

                                                                                  title="Enter Any exemption from tax"
                                                                                  maxlength="60"></div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>


                                </div>
                            </div>


                            <!--Statutory Requirements end-->


                            <!--#Official start-->

                            <div class="tab-pane" id="Bankdetails">
                                <h4 class="text-center"><b>Bank Details</b></h4>

                                <div style="color: #1b6d57" class="text-center" id="msgdiv_state"></div>
                                <div id="state_table">


                                    <div class="sep_box">
                                        <div class="col-lg-4">

                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="tbl_text">Beneficiary Name <span
                                                                style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="tbl_input"><input type="text"
                                                                                  name="bank_benificialname"
                                                                                  class="form-control-1"
                                                                                  placeholder="Enter Beneficiary Name"

                                                                                  title="Enter Beneficiary Name"
                                                                                  maxlength="60">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="tbl_text">Beneficiary Account Number <span
                                                                style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="tbl_input"><input type="text"
                                                                                  name="bank_benificialaccno"
                                                                                  class="form-control-1"
                                                                                  placeholder="Enter Beneficiary Account Number"

                                                                                  title="Enter Beneficiary Account Number"
                                                                                  maxlength="60"></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="tbl_text">Beneficiary Bank Name <span
                                                                style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="tbl_input"><input type="text"
                                                                                  name="bank_benificialbankname"
                                                                                  class="form-control-1"
                                                                                  placeholder="Enter Beneficiary Bank Name"

                                                                                  title="Enter Beneficiary Bank Name"
                                                                                  maxlength="60">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="sep_box">
                                        <div class="col-lg-4">

                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="tbl_text">Beneficiary Bank Branch Name <span
                                                        < span style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="tbl_input"><input type="text"
                                                                                  name="bank_benificialbranchname"
                                                                                  class="form-control-1"
                                                                                  placeholder="Enter Beneficiary Bank Branch Name"

                                                                                  title="Enter Beneficiary Bank Branch Name"
                                                                                  maxlength="60">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="tbl_text">Bank Address
                                                        <span style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="tbl_input"><input type="text"
                                                                                  name="bank_benificialaddress"
                                                                                  class="form-control-1"
                                                                                  placeholder="Enter Bank Address"

                                                                                  title="Enter Bank Address"
                                                                                  maxlength="60"></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="tbl_text">IFSC Code/SORT/ABA
                                                        <span style="color:red;font-weight:bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="tbl_input"> <input type="text"
                                                                                    name="bank_benificialifsc"
                                                                                    class="form-control-1"
                                                                                    placeholder="Enter IFSC Code/SORT/ABA "

                                                                                    title="Enter IFSC Code/SORT/ABA "
                                                                                    maxlength="60">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="sep_box">
                                        <div class="col-lg-4">

                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="tbl_text">Swift Code <span
                                                                style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="tbl_input"><input type="text"
                                                                                  name="bank_benificialswiftcode"
                                                                                  class="form-control-1"
                                                                                  placeholder="Enter Swift Code"

                                                                                  title="Enter Swift Code"
                                                                                  maxlength="60">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="tbl_text">IBAN No <span
                                                                style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="tbl_input"><input type="text"
                                                                                  name="bank_benificialibanno"
                                                                                  class="form-control-1"
                                                                                  placeholder="Enter IBAN No"
                                                                                  title="Enter IBAN No"
                                                                                  maxlength="60"></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="tbl_text">Intermediatery bank <span
                                                                style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="tbl_input"><input type="text"
                                                                                  name="intermidiatebankname"
                                                                                  class="form-control-1"
                                                                                  placeholder="Enter Bank name"

                                                                                  title="Enter Bank name"
                                                                                  maxlength="60">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="sep_box">
                                        <div class="col-lg-4">

                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="tbl_text">Bank Address <span
                                                                style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="tbl_input"><input type="text"
                                                                                  name="intermidiatebankaddress"
                                                                                  class="form-control-1"
                                                                                  placeholder="Enter Bank Address"

                                                                                  title="Enter Bank Address"
                                                                                  maxlength="60">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="tbl_text">SWIFT Code <span
                                                                style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="tbl_input"><input type="text"
                                                                                  name="intermidiatebankswiftcode"
                                                                                  class="form-control-1"
                                                                                  placeholder="Enter SWIFT Code"

                                                                                  title="Enter SWIFT Code"
                                                                                  maxlength="60"></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="tbl_text">ECS form duly signed by authorised
                                                        person attached? <span
                                                                style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="tbl_input"><select name="bank_ecs"
                                                                                   class="form-control-1">
                                                            <option value="Yes">Yes</option>
                                                            <option value="No">No</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>


                            <div class="tab-pane" id="location">
                                <h4 class="text-center"><b>Document/Amount</b></h4>

                                <div style="color: #1b6d57" class="text-center" id="msgdiv_state"></div>
                                <div id="state_table">


                                    <div class="sep_box">
                                        <div class="col-lg-4">

                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="tbl_text">Copy of PAN CARD <span
                                                                style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="tbl_input">
                                                        <input type="file" name="copypancart"
                                                               class="form-control-1">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="tbl_text">Copy of Service Tax/ Sales Tax/
                                                        VAT <span
                                                                style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="tbl_input"><input type="file"
                                                                                  name="copyservicetax"
                                                                                  class="form-control-1"
                                                        ></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="tbl_text">Copy of TAN <span
                                                                style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="tbl_input"><input type="file" name="copytan"
                                                                                  class="form-control-1"
                                                        >
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="sep_box">
                                        <div class="col-lg-4">

                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="tbl_text">Address Proof <span
                                                        < span style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="tbl_input"><input type="file"
                                                                                  name="copyaddressproof"
                                                                                  class="form-control-1"
                                                        >
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="display: none" class="col-lg-4">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="tbl_text">Credit Amount
                                                        <span style="color:red;font-weight: bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="tbl_input"><input type="text"
                                                                                  name="bank_creditamount" value="0"
                                                                                  class="form-control-1"
                                                                                  placeholder="Enter Bank Credit Amount"

                                                                                  title="Enter Bank Credit Amount"
                                                                                  maxlength="60"></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div style="display: none;" class="col-lg-4">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="tbl_text">Agent Commision
                                                        <span style="color:red;font-weight:bold;">*</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="tbl_input"><input type="text"
                                                                                  name="bank_agentcommision"
                                                                                  class="form-control-1"
                                                                                  placeholder="Enter Agent Commision "

                                                                                  title="Enter Agent Commision"
                                                                                  maxlength="60" value="0">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="sep_box">
                                        <div class="col-lg-4">

                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="tbl_text">Branch

                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="tbl_input">
                                                        <select name="branch">
                                                            <?php foreach ($branch as $branchkey => $branchval) { ?>
                                                                <option
                                                                        value="<?php echo $branchval->branch_id; ?>"><?php echo $branchval->branch_name; ?></option>
                                                            <?php } ?>

                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <input type="submit" value="submit" name="submit">


                                    </div>
                                </div>


                            </div>


                        </div>

                </div>
            </div>
            </form>
            <div id="agentdiv">
                <table class="grid_tbl">
                    <thead>
                    <tr>
                        <th>S:NO</th>
                        <th>Agency Name</th>
                        <th>User Name</th>
                        <th>Contact</th>
                        <th>Mail id</th>
                        <th>Comission</th>
                        <th>Wallet Amt</th>
                        <th>Action</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php $i = 0;
                    //p($agentvieww);
                    foreach ($agentvieww as $key => $value) {
                        $i++;
                        ?>

                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $value->agent_agencyname; ?></td>
                            <td><?php echo $value->agent_username; ?></td>
                            <td><?php echo $value->agent_mobile; ?></td>
                            <td><?php echo $value->agent_email; ?></td>
                            <td><a  title="Manage Comission Amount" href="agentwallet_v?edid=<?php echo base64_encode($value->agent_id); ?>&empid=<?php echo $_GET['empid']; ?>&uid=<?php echo str_replace(".html","",$_GET['uid']); ?>"><i
                                            class="btn fa fa"><?php echo " " . $value->bank_agentcommision . " %"; ?>
                                </a></i></td>
                            <td><a title="Manage Wallet Amount" href="agentamount?edid=<?php echo base64_encode($value->agent_id); ?>?empid=<?php echo $_GET['empid']; ?>&uid=<?php echo str_replace(".html","",$_GET['uid']); ?>"><i
                                            class="btn fa fa-inr"><?php echo " " . $value->bank_creditamount; ?>
                                </a></i></td>

                            <td>
                                <a href="agentedit?empid=<?php echo $_GET['empid']; ?>&uid=<?php echo str_replace(".html","",$_GET['uid']); ?>&edid=<?php echo base64_encode($value->agent_id); ?>"><i
                                            class="fa fa-pencil"></a></i>


                                <a onclick="return RemoveCompany('<?php echo $value->agent_id ?>?empid=<?php echo $_GET['empid']; ?>&uid=<?php echo str_replace(".html","",$_GET['uid']); ?>')"><i
                                            class="fa fa-trash"></i></a>


                                <a onclick="return StatusCompany('<?php echo $value->agent_id ?>','<?php echo $value->agent_status; ?>')"><?php if ($value->agent_status == '0') { ?>Pending<?php } else { ?>Active<?php } ?></a>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>
</div>
</div>
<script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>

<script>

function StatusCompany(a, b) {
/*alert(a);
 alert(b);*/
if (confirm("Are you sure to change status?") == true) {
    $.ajax({
        type: "POST",
        url: "<?php echo base_url()?>admin/b2b/agentstatus/" + a + "/" + b,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
            //alert(data);
            $("#agentdiv").load(location.href + " #agentdiv");
            $("#msgdiv").html('status change sucessfully');
        },
        error: function (data) {
            // alert(data);
        }
    });
} else {
    return false;
}
}


function RemoveCompany(a) {
if (confirm("Are you sure to delete?") == true) {
    $.ajax({
        type: "POST",
        url: "<?php echo base_url()?>admin/b2b/agentdel/" + a,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
            // alert(data);
            $("#agentdiv").load(location.href + " #agentdiv");
            $("#msgdiv").html('deleted sucessfully');
        },
        error: function (data) {
            // alert(data);
        }
    });
} else {
    return false;
}
}


</script>