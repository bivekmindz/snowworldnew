<?php


//pend($this->session->all_userdata());
//pend($viewcity);
//pend($data);
/*pend($viewcountry);*/ ?>
<script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/jquery.validate.min.js"></script>

<div class="wrapper">
    <style>
        .error {
            color: red;

        }

        .tab-pane h4 b {
            color: #1b78c7;
        }

        .modal-body {
            width: 100%;
            float: left;
            position: relative;
        }
    </style>

    <div class="col-lg-10 col-lg-push-2">


        <div class="row">

            <div class="page_contant">


                <div class="col-lg-12">
                    <div class="page_name">

                        <h2>Manage Agent</h2>

                    </div>

                    <div class="page_box">

                        <div class="col-lg-12">

                            <div id="content">
                              
                                <div >



                                    <form id="addCont" action="" method="post" enctype="multipart/form-data">




                                    <div class="tab-pane active" id="">
                                        <h4 class="text-center"><b>Personal</b></h4>
                                        <div class="form-conte">
                                            <!--  session flash message  -->
                                            <div class='flashmsg'>
                                                <?php echo validation_errors(); ?>
                                                <?php
                                                if ($this->session->flashdata('message')) {
                                                    echo $this->session->flashdata('message');
                                                }
                                                ?>
                                            </div>
                                            <!--Personal form start-->

                                                <div style="color: #1b6d57" class="text-center" id="msgdiv"></div>
                                                <div class="sep_box">
                                                    <div class="col-lg-4">

                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <div class="tbl_text">Login Type <span
                                                                            style="color:red;font-weight: bold;">*</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="tbl_input"><select name="logintype"  class="form-control-1" >
                                                                        <option value="1" <?php if($agentviewwdata->logintype==1) {?> selected="selected"<?php } ?> >Corporate Login  </option>
                                                                        <option value="2"<?php if($agentviewwdata->logintype==2) {?> selected="selected"<?php } ?>>Officer Login </option>

                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="row">
                                                            <div class="col-lg-4">
                                                                <div class="tbl_text">Country <span
                                                                            style="color:red;font-weight: bold;">*</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-8">
                                                                <div class="tbl_input"> <input type="text" name="emailcountry" class="form-control-1" placeholder="Enter Your Country" pattern="[A-Za-z\s]+"  title="Enter Your Country" maxlength="50" value="<?php echo $agentviewwdata->agent_emailcountry; ?>"></div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-4">
                                                        <div class="row">
                                                            <div class="col-lg-4">
                                                                <div class="tbl_text">Agency Name <span
                                                                            style="color:red;font-weight: bold;">*</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-8">
                                                                <div class="tbl_input"><input type="text" name="agencyname" class="form-control-1" placeholder="Enter Agency Name" pattern="[A-Za-z\s]+"  title="Agency Name should only contain  letters. e.g. John" maxlength="60" value="<?php echo $agentviewwdata->agent_agencyname; ?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="sep_box">
                                                    <div class="col-lg-4">

                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <div class="tbl_text">Registered / Head Office Address </label>
                                                                    <span
                                                                            style="color:red;font-weight: bold;">*</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="tbl_input"> <input type="text" name="office_address" class="form-control-1" placeholder="Enter Office Address"   title="Enter Registered / Head Office Address " maxlength="60" value="<?php echo $agentviewwdata->agent_office_address; ?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="row">
                                                            <div class="col-lg-4">
                                                                <div class="tbl_text">City <span
                                                                            style="color:red;font-weight: bold;">*</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-8">
                                                                <div class="tbl_input"> <input type="text" name="city" class="form-control-1" placeholder="Enter City" pattern="[A-Za-z\s]+"  title="City Name should only contain  letters. e.g. Delhi" maxlength="60" value="<?php echo $agentviewwdata->agent_city; ?>"></div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-4">
                                                        <div class="row">
                                                            <div class="col-lg-4">
                                                                <div class="tbl_text">State <span
                                                                            style="color:red;font-weight: bold;">*</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-8">
                                                                <div class="tbl_input"><input type="text" name="state" class="form-control-1" placeholder="Enter State" pattern="[A-Za-z\s]+"  title="State Name should only contain  letters. e.g. Delhi" maxlength="60" value="<?php echo $agentviewwdata->agent_state; ?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="sep_box">
                                                    <div class="col-lg-4">

                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <div class="tbl_text">User Email id <span
                                                                            style="color:red;font-weight: bold;">*</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="tbl_input"><input type="email" name="username" class="form-control-1" placeholder="Enter Your Email"  pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="The input is not a valid email address" maxlength="50" value="<?php echo $agentviewwdata->agent_username; ?>" readonly>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="row">
                                                            <div class="col-lg-4">
                                                                <div class="tbl_text">Currency in which payments will be made: <span
                                                                            style="color:red;font-weight: bold;">*</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-8">
                                                                <div class="tbl_input"><input type="text" name="emailcurrency" class="form-control-1" placeholder="Enter Your Currency"  title="Enter Your Currency" maxlength="50" value="<?php echo $agentviewwdata->agent_emailcurrency; ?>">  </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-4">
                                                        <div class="row">
                                                            <div class="col-lg-4">
                                                                <div class="tbl_text">Pincode <span
                                                                            style="color:red;font-weight: bold;">*</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-8">
                                                                <div class="tbl_input"><input type="text" name="pincode" class="form-control-1" placeholder="Enter Your Pincode" pattern="^\d{6}$"  title="Your Pincode Should Only Contain  Number. e.g. 110011" maxlength="10" value="<?php echo $agentviewwdata->agent_pincode; ?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="sep_box">
                                                    <div class="col-lg-4">

                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <div class="tbl_text">Telephone <span
                                                                            style="color:red;font-weight: bold;">*</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="tbl_input"><input type="tel" name="telephone" class="form-control-1" placeholder="Enter Your Telephone Number"   title="Your Phone Number Should Only Contain  Number. e.g. 9999999999" maxlength="20" value="<?php echo $agentviewwdata->agent_telephone; ?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="row">
                                                            <div class="col-lg-4">
                                                                <div class="tbl_text">Mobile <span
                                                                            style="color:red;font-weight: bold;">*</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-8">
                                                                <div class="tbl_input">  <input type="tel" name="mobile" class="form-control-1" placeholder="Enter Your Phone Number" pattern="^\d{10}$"  title="Your Phone Number Should Only Contain  Number. e.g. 9999999999" maxlength="10" value="<?php echo $agentviewwdata->agent_mobile; ?>"></div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-4">
                                                        <div class="row">
                                                            <div class="col-lg-4">
                                                                <div class="tbl_text">Fax <span
                                                                            style="color:red;font-weight: bold;">*</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-8">
                                                                <div class="tbl_input"> <input type="text" name="fax" class="form-control-1" placeholder="Enter Your Fax Number"   title="Your Fax Number Should Only Contain  Number. e.g. 9999999999" maxlength="20" value="<?php echo $agentviewwdata->agent_fax; ?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="sep_box">
                                                    <div class="col-lg-4">

                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <div class="tbl_text">Website <span
                                                                            style="color:red;font-weight: bold;">*</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="tbl_input"><input type="text" name="website" class="form-control-1" placeholder="Enter Your Website"   maxlength="60"  value="<?php echo $agentviewwdata->agent_website; ?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="row">
                                                            <div class="col-lg-4">
                                                                <div class="tbl_text">Email <span
                                                                            style="color:red;font-weight: bold;">*</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-8">
                                                                <div class="tbl_input"> <input type="email" name="email" class="form-control-1" placeholder="Enter Your Email"  pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="The input is not a valid email address" maxlength="50" value="<?php echo $agentviewwdata->agent_email; ?>"></div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-4">
                                                        <div class="row">
                                                            <div class="col-lg-4">
                                                                <div class="tbl_text">Status <span
                                                                            style="color:red;font-weight: bold;">*</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-8">
                                                                <div class="tbl_input"> <select name="emailstatus" class="form-control-1">
                                                                        <option value="International" <?php if($agentviewwdata->agent_emailstatus=='International') {?> selected="selected" <?php } ?>>International</option>
                                                                        <option value="Domestic" <?php if($agentviewwdata->agent_emailstatus=='Domestic') {?> selected="selected" <?php } ?>>Domestic</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                              


                                    <div >
                                        <h4 class="text-center"><b>Official to Lialise with Chiliad Procons</b></h4>
                                        <div style="color: #1b6d57" class="text-center" id="msgdiv_state"></div>
                                        <div id="state_table">


                                            <div class="sep_box">
                                                <div class="col-lg-4">

                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <div class="tbl_text">Primary Contact Person <span
                                                                        style="color:red;font-weight: bold;">*</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="tbl_input"><input type="text" name="primarycontact_name" class="form-control-1" placeholder="Enter Primary Contact Person" pattern="[A-Za-z\s]+"  title="Primary Contact Person Name should only contain  letters. e.g. John" maxlength="60" value="<?php echo $agentviewwdata->agent_primarycontact_name; ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="row">
                                                        <div class="col-lg-4">
                                                            <div class="tbl_text">Email <span
                                                                        style="color:red;font-weight: bold;">*</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-8">
                                                            <div class="tbl_input">  <input type="email" name="primarycontact_email_mobile" class="form-control-1" placeholder="Enter  Email"  pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="The input is not a valid email address" maxlength="50" value="<?php echo $agentviewwdata->agent_primarycontact_email_mobile; ?>"></div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-4">
                                                    <div class="row">
                                                        <div class="col-lg-4">
                                                            <div class="tbl_text">Mobile <span
                                                                        style="color:red;font-weight: bold;">*</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-8">
                                                            <div class="tbl_input"><input type="tel" name="primarycontact_telephone" class="form-control-1" placeholder="Enter Mobile Number" pattern="^\d{10}$"  title="Mobile Number Should Only Contain  Letters. e.g. 9999999999" maxlength="10" value="<?php echo $agentviewwdata->agent_primarycontact_telephone; ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="sep_box">
                                                <div class="col-lg-4">

                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <div class="tbl_text">Secondary Contact Person <span
                                                                        style="color:red;font-weight: bold;">*</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="tbl_input"><input type="text" name="secondrycontact_name" class="form-control-1" placeholder="Enter Secondary Contact Person" pattern="[A-Za-z\s]+"  title="Secondary Contact Person Name should only contain  letters. e.g. John" maxlength="60" value="<?php echo $agentviewwdata->agent_secondrycontact_name; ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="row">
                                                        <div class="col-lg-4">
                                                            <div class="tbl_text">Email <span
                                                                        style="color:red;font-weight: bold;">*</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-8">
                                                            <div class="tbl_input"> <input type="email" name="secondrycontact_email_phone" class="form-control-1" placeholder="Enter  Email"  pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="The input is not a valid email address" maxlength="50" value="<?php echo $agentviewwdata->agent_secondrycontact_email_phone; ?>"></div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-4">
                                                    <div class="row">
                                                        <div class="col-lg-4">
                                                            <div class="tbl_text">Residence Telephone <span
                                                                        style="color:red;font-weight: bold;">*</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-8">
                                                            <div class="tbl_input">< <input type="tel" name="secondrycontact_telephone" class="form-control-1" placeholder="Enter  Phone Number" pattern="^\d{10}$"  title=" Phone Number Should Only Contain  Letters. e.g. 9999999999" maxlength="10" value="<?php echo $agentviewwdata->agent_secondrycontact_telephone; ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="sep_box">
                                                <div class="col-lg-4">

                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <div class="tbl_text">Management Executives <span
                                                                        style="color:red;font-weight: bold;">*</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="tbl_input"><input type="text" name="management_executives_name" class="form-control-1" placeholder="Management Executives  Name" pattern="[A-Za-z\s]+"  title=" Management Executives should only contain  letters. e.g. Singh" maxlength="60" value="<?php echo $agentviewwdata->agent_management_executives_name; ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="row">
                                                        <div class="col-lg-4">
                                                            <div class="tbl_text">Email <span
                                                                        style="color:red;font-weight: bold;">*</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-8">
                                                            <div class="tbl_input">  <input type="email" name="management_executives_email_phone" class="form-control-1" placeholder="Management Executives Email"  pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="The input is not a valid email address" maxlength="50" value="<?php echo $agentviewwdata->agent_management_executives_email_phone; ?>"></div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-4">
                                                    <div class="row">
                                                        <div class="col-lg-4">
                                                            <div class="tbl_text">Branches( if any) <span
                                                                        style="color:red;font-weight: bold;">*</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-8">
                                                            <div class="tbl_input"><input type="text" name="branches" class="form-control-1"  placeholder="Enter Branches" value="<?php echo $agentviewwdata->agent_branches; ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="sep_box">
                                                <div class="col-lg-4">

                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <div class="tbl_text">Year of establishment <span
                                                                        style="color:red;font-weight: bold;">*</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="tbl_input"> <input type="text" name="yearofestablism" class="form-control-1"   placeholder="Enter year of establism" value="<?php echo $agentviewwdata->agent_yearofestablism; ?>">                                </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="row">
                                                        <div class="col-lg-4">
                                                            <div class="tbl_text">Type of Organization <span
                                                                        style="color:red;font-weight: bold;">*</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-8">
                                                            <div class="tbl_input"> <select class="form-control-1" name="organisationtype" >
                                                                    <option value="Proprietorship" <?php if($agentviewwdata->agent_organisationtype=='Proprietorship') {?> selected="selected" <?php } ?>>Proprietorship</option>
                                                                    <option value="Limited" <?php if($agentviewwdata->agent_organisationtype=='Limited') {?> selected="selected" <?php } ?>>Limited</option>

                                                                    <option value="Limited Liability" <?php if($agentviewwdata->agent_organisationtype=='Limited Liability') {?> selected="selected" <?php } ?>>Limited Liability</option>

                                                                    <option value="Any other" <?php if($agentviewwdata->agent_organisationtype=='Any other') {?> selected="selected" <?php } ?>>Any other</option>

                                                                </select></div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>



                                        </div>
                                    </div>



                                    <!--Official end-->


                                    <!--Statutory Requirements Start-->

                                    <div >
                                        <h4 class="text-center"><b>Statutory Requirements : Mention NA where not applicable</b></h4>
                                        <div style="color: #1b6d57" class="text-center" id="msgdiv_state"></div>
                                        <div id="state_table">


                                            <div class="sep_box">
                                                <div class="col-lg-4">

                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <div class="tbl_text">LST No <span
                                                                        style="color:red;font-weight: bold;">*</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="tbl_input"><input type="text" name="lstno" class="form-control-1" placeholder="Enter LST No"   title="Enter LST No" maxlength="60" value="<?php echo $agentviewwdata->agent_lstno; ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="row">
                                                        <div class="col-lg-4">
                                                            <div class="tbl_text">CST No <span
                                                                        style="color:red;font-weight: bold;">*</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-8">
                                                            <div class="tbl_input"> <input type="text" name="cstno" class="form-control-1" placeholder="Enter CST No"   title="Enter CST No" maxlength="60" value="<?php echo $agentviewwdata->agent_cstno; ?>"></div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-4">
                                                    <div class="row">
                                                        <div class="col-lg-4">
                                                            <div class="tbl_text">Excise Registration No <span
                                                                        style="color:red;font-weight: bold;">*</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-8">
                                                            <div class="tbl_input"> <input type="text" name="registrationno" class="form-control-1" placeholder="Enter Excise Registration No"   title="Enter Excise Registration No" maxlength="60" value="<?php echo $agentviewwdata->agent_registrationno; ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="sep_box">
                                                <div class="col-lg-4">

                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <div class="tbl_text">VAT No. / TIN No <span
                                                                        style="color:red;font-weight: bold;">*</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="tbl_input"><input type="text" name="vatno" class="form-control-1" placeholder="Enter VAT No. / TIN No"   title="Enter VAT No. / TIN No" maxlength="60" value="<?php echo $agentviewwdata->agent_vatno; ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="row">
                                                        <div class="col-lg-4">
                                                            <div class="tbl_text">PAN No <span
                                                                        style="color:red;font-weight: bold;">*</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-8">
                                                            <div class="tbl_input"> <input type="text" name="panno" class="form-control-1" placeholder="Enter PAN No"   title="Enter PAN No" maxlength="60" value="<?php echo $agentviewwdata->agent_panno; ?>"></div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-4">
                                                    <div class="row">
                                                        <div class="col-lg-4">
                                                            <div class="tbl_text">Service Tax Registration No <span
                                                                        style="color:red;font-weight: bold;">*</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-8">
                                                            <div class="tbl_input"><input type="text" name="servicetaxno" class="form-control-1" placeholder="Enter Service Tax Registration No"   title="Enter Service Tax Registration No" maxlength="60" value="<?php echo $agentviewwdata->agent_panno; ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="sep_box">
                                                <div class="col-lg-4">

                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <div class="tbl_text">TAN No <span
                                                                <style="color:red;font-weight: bold;">*</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="tbl_input">  <input type="text" name="tanno" class="form-control-1" placeholder="Enter TAN No"   title="Enter TAN No" maxlength="60" value="<?php echo $agentviewwdata->agent_tanno; ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="row">
                                                        <div class="col-lg-4">
                                                            <div class="tbl_text">PF No <span
                                                                        style="color:red;font-weight: bold;">*</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-8">
                                                            <div class="tbl_input">  <input type="text" name="pfno" class="form-control-1" placeholder="Enter PF No"   title="Enter PF No" maxlength="60" value="<?php echo $agentviewwdata->agent_pfno; ?>"></div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-4">
                                                    <div class="row">
                                                        <div class="col-lg-4">
                                                            <div class="tbl_text">ESIC No <span
                                                                        style="color:red;font-weight: bold;">*</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-8">
                                                            <div class="tbl_input"> <input type="text" name="esisno" class="form-control-1" placeholder="Enter ESIC No"   title="Enter ESIC No" maxlength="60" value="<?php echo $agentviewwdata->agent_esisno; ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="sep_box">
                                                <div class="col-lg-4">

                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <div class="tbl_text">Office / Establishment / Factory Registration
                                                                No./ Labour Licence no <span
                                                                        style="color:red;font-weight: bold;">*</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="tbl_input"> <input type="text" name="officeregistrationno" class="form-control-1" placeholder="Enter Office / Establishment / Factory Registration
                                    No./ Labour Licence no"   title="Enter Office / Establishment / Factory Registration
                                    No./ Labour Licence no" maxlength="60" value="<?php echo $agentviewwdata->agent_officeregistrationno; ?>">                               </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="row">
                                                        <div class="col-lg-4">
                                                            <div class="tbl_text">Any exemption from tax <span
                                                                        style="color:red;font-weight: bold;">*</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-8">
                                                            <div class="tbl_input"> <input type="text" name="exceptiontax" class="form-control-1" placeholder="Enter Any exemption from tax"   title="Enter Any exemption from tax" maxlength="60" value="<?php echo $agentviewwdata->agent_exceptiontax; ?>"></div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>



                                        </div>
                                    </div>



                                    <!--Statutory Requirements end-->


                                    <!--#Official start-->

                                    <div>
                                        <h4 class="text-center"><b>Bank Details</b></h4>

                                        <div style="color: #1b6d57" class="text-center" id="msgdiv_state"></div>
                                        <div id="state_table">


                                            <div class="sep_box">
                                                <div class="col-lg-4">

                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <div class="tbl_text">Beneficiary Name <span
                                                                        style="color:red;font-weight: bold;">*</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="tbl_input"> <input type="text" name="bank_benificialname" class="form-control-1" placeholder="Enter Beneficiary Name"   title="Enter Beneficiary Name" maxlength="60" value="<?php echo $agentviewwdata->agent_bank_benificialname; ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="row">
                                                        <div class="col-lg-4">
                                                            <div class="tbl_text">Beneficiary Account Number <span
                                                                        style="color:red;font-weight: bold;">*</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-8">
                                                            <div class="tbl_input">   <input type="text" name="bank_benificialaccno" class="form-control-1" placeholder="Enter Beneficiary Account Number"   title="Enter Beneficiary Account Number" maxlength="60" value="<?php echo $agentviewwdata->agent_bank_benificialaccno; ?>"></div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-4">
                                                    <div class="row">
                                                        <div class="col-lg-4">
                                                            <div class="tbl_text">Beneficiary Bank Name <span
                                                                        style="color:red;font-weight: bold;">*</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-8">
                                                            <div class="tbl_input"> <input type="text" name="bank_benificialbankname" class="form-control-1" placeholder="Enter Beneficiary Bank Name"   title="Enter Beneficiary Bank Name" maxlength="60" value="<?php echo $agentviewwdata->agent_bank_benificialbankname; ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="sep_box">
                                                <div class="col-lg-4">

                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <div class="tbl_text">Beneficiary Bank Branch Name <span
                                                                        < span style="color:red;font-weight: bold;">*</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="tbl_input"> <input type="text" name="bank_benificialbranchname" class="form-control-1" placeholder="Enter Beneficiary Bank Branch Name"   title="Enter Beneficiary Bank Branch Name" maxlength="60" value="<?php echo $agentviewwdata->agent_bank_benificialbranchname; ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="row">
                                                        <div class="col-lg-4">
                                                            <div class="tbl_text">Bank Address
                                                                <span  style="color:red;font-weight: bold;">*</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-8">
                                                            <div class="tbl_input"> <input type="text" name="bank_benificialaddress" class="form-control-1" placeholder="Enter Bank Address"   title="Enter Bank Address" maxlength="60" value="<?php echo $agentviewwdata->bank_benificialaddress; ?>"></div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-4">
                                                    <div class="row">
                                                        <div class="col-lg-4">
                                                            <div class="tbl_text">IFSC Code/SORT/ABA
                                                                <span style="color:red;font-weight:bold;">*</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-8">
                                                            <div class="tbl_input">< <input type="text" name="bank_benificialifsc" class="form-control-1" placeholder="Enter IFSC Code/SORT/ABA "   title="Enter IFSC Code/SORT/ABA " maxlength="60" value="<?php echo $agentviewwdata->bank_benificialifsc; ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="sep_box">
                                                <div class="col-lg-4">

                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <div class="tbl_text">Swift Code <span
                                                                        style="color:red;font-weight: bold;">*</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="tbl_input"> <input type="text" name="bank_benificialswiftcode" class="form-control-1" placeholder="Enter Swift Code"   title="Enter Swift Code" maxlength="60" value="<?php echo $agentviewwdata->bank_benificialswiftcode; ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="row">
                                                        <div class="col-lg-4">
                                                            <div class="tbl_text">IBAN No <span
                                                                        style="color:red;font-weight: bold;">*</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-8">
                                                            <div class="tbl_input">   <input type="text" name="bank_benificialibanno" class="form-control-1" placeholder="Enter IBAN No"   title="Enter IBAN No" maxlength="60" value="<?php echo $agentviewwdata->bank_benificialibanno; ?>"></div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-4">
                                                    <div class="row">
                                                        <div class="col-lg-4">
                                                            <div class="tbl_text">Intermediatery bank <span
                                                                        style="color:red;font-weight: bold;">*</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-8">
                                                            <div class="tbl_input"> <input type="text" name="intermidiatebankname" class="form-control-1" placeholder="Enter Bank name"   title="Enter Bank name" maxlength="60" value="<?php echo $agentviewwdata->agent_intermidiatebankname; ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="sep_box">
                                                <div class="col-lg-4">

                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <div class="tbl_text">Bank Address <span
                                                                        style="color:red;font-weight: bold;">*</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="tbl_input">  <input type="text" name="intermidiatebankaddress" class="form-control-1" placeholder="Enter Bank Address"   title="Enter Bank Address"  value="<?php echo $agentviewwdata->agent_intermidiatebankaddress; ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="row">
                                                        <div class="col-lg-4">
                                                            <div class="tbl_text">SWIFT Code <span
                                                                        style="color:red;font-weight: bold;">*</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-8">
                                                            <div class="tbl_input">   <input type="text" name="intermidiatebankswiftcode" class="form-control-1" placeholder="Enter SWIFT Code"   title="Enter SWIFT Code" maxlength="60"  value="<?php echo $agentviewwdata->intermidiatebankswiftcode; ?>"></div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-4">
                                                    <div class="row">
                                                        <div class="col-lg-4">
                                                            <div class="tbl_text">ECS form duly signed by authorised person attached? <span
                                                                        style="color:red;font-weight: bold;">*</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-8">
                                                            <div class="tbl_input"><select  name="bank_ecs" class="form-control-1">
                                                                    <option value="Yes"<?php if($agentviewwdata->agent_bank_ecs=='Yes') { ?> selected="selected" <?php } ?>>Yes</option>
                                                                    <option value="No" <?php if($agentviewwdata->agent_bank_ecs=='No') { ?> selected="selected" <?php } ?>>No</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    </div>




















                                    <div >
                                        <h4 class="text-center"><b>Document/Amount</b></h4>

                                        <div style="color: #1b6d57" class="text-center" id="msgdiv_state"></div>
                                        <div id="state_table">


                                            <div class="sep_box">
                                                <div class="col-lg-4">

                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <div class="tbl_text">Copy of PAN CARD <span
                                                                        style="color:red;font-weight: bold;">*</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="tbl_input">
                                                                <input  type="file" name="copypancart" class="form-control-1"  > <img src="../../assets/admin/images/partner/<?php echo $agentviewwdata->agent_copypancart; ?>" width="50px" height="60px">
                                                                <input type="hidden" name="copypancartdata" value="<?php echo $agentviewwdata->agent_copypancart; ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="row">
                                                        <div class="col-lg-4">
                                                            <div class="tbl_text">Copy of Service Tax/ Sales Tax/ VAT <span
                                                                        style="color:red;font-weight: bold;">*</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-8">
                                                            <div class="tbl_input">    <input  type="file" name="copyservicetax" class="form-control-1" > <img src="../../assets/admin/images/partner/<?php echo $agentviewwdata->agent_copyservicetax; ?>" width="50px" height="60px">
 <input type="hidden" name="copyservicetaxdata" value="<?php echo $agentviewwdata->agent_copyservicetax; ?>">

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-4">
                                                    <div class="row">
                                                        <div class="col-lg-4">
                                                            <div class="tbl_text">Copy of TAN <span
                                                                        style="color:red;font-weight: bold;">*</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-8">
                                                            <div class="tbl_input"> <input  type="file" name="copytan" class="form-control-1" ><img src="../../assets/admin/images/partner/<?php echo $agentviewwdata->agent_copytan; ?>" width="50px" height="60px">
                                                             <input type="hidden" name="copytandata" value="<?php echo $agentviewwdata->agent_copytan; ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="sep_box">
                                                <div class="col-lg-4">

                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <div class="tbl_text">Address Proof <span
                                                                < span style="color:red;font-weight: bold;">*</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="tbl_input"> <input  type="file" name="copyaddressproof" class="form-control-1" >

                                                                <img src="../../assets/admin/images/partner/<?php echo $agentviewwdata->agent_copyaddressproof; ?>" width="50px" height="60px">
                                                                <input type="hidden" name="copyaddressproofdata" value="<?php echo $agentviewwdata->agent_copyaddressproof; ?>">


                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="row">




                                            </div>


                                            <div class="sep_box">
                                                <div class="col-lg-4">

                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <div class="tbl_text">

                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="tbl_input">
                                                                                                                         <s
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>




     <input type="submit" value="submit" name="submit">


                                        </div>
                                    </div>


        </div>


                            </div>

                        </div>
                    </div>
                    
</form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    function StatusCompany(a, b) {
        /*alert(a);
         alert(b);*/
        if (confirm("Are you sure to change status?") == true) {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url()?>admin/b2b/agentstatus/" + a + "/" + b,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                   //alert(data);
                    $("#agentdiv").load(location.href + " #agentdiv");
                    $("#msgdiv").html('status change sucessfully');
                },
                error: function (data) {
                    // alert(data);
                }
            });
        } else {
            return false;
        }
    }




    function RemoveCompany(a) {
        if (confirm("Are you sure to delete?") == true) {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url()?>admin/b2b/agentdel/" + a,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    // alert(data);
                    $("#agentdiv").load(location.href + " #agentdiv");
                    $("#msgdiv").html('deleted sucessfully');
                },
                error: function (data) {
                    // alert(data);
                }
            });
        } else {
            return false;
        }
    }



</script>