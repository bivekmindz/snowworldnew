<!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->
<script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery-1.9.1.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"
        crossorigin="anonymous"></script>
<!-- jQuery Form Validation code -->
<style>
    input.error{border:1px solid red;}
    label.error{border:0px solid red; color:red; font-weight: normal; display:inline; }
</style>

<script language='javascript'>

    // When the browser is ready...
 $(document).ready(function() {
     $.noConflict();
        // Setup form validation on the #register-form element
        $("#addCont").validate({
            // Specify the validation rules
            rules: {
                branchids : "required",
                promocode_name : "required",
                  promocode_start : "required",
                promocode_end : "required",
                user_type : "required"

                /* email:{
                 required: true,
                 email: true
                 },
                 password: {
                 required: true,
                 minlength: 5
                 },
                 agree: "required" */
            },
            // Specify the validation error messages
            messages: {
                branchids : "Branch is required",
                promocode_name :"promo code is required",
                  promocode_start : "select start date",
                promocode_end : "select end  date"
                /*password: {
                 required: "Please provide a password",
                 minlength: "Your password must be at least 5 characters long"
                 },
                 email: "Please enter a valid email address",
                 agree: "Please accept our policy"*/
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
     $.noConflict();
    });

</script>

<div class="wrapper">

    <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">

                        <h2>ADD PROMO CODE</h2>
                    </div>
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-12">
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?>
                                    <?php
                                    if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message');
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>

                        <form action='<?php echo base_url('admin/promocodes/addpromo?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid']));?>' id="mgrForm" name="mgrForm" method="post" enctype="multipart/form-data" >



                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Select Branch <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">


                                                <?php
                                                if($this->session->userdata('snowworld')->EmployeeId){
                                                    ?>
                                                    <script>
                                                        $(function(){
                                                            $('#branchids option[value="<?= $this->session->userdata('snowworld')->branchid; ?>"]').attr('selected',true);
                                                            $("#branchids").attr('disabled',true);
                                                            //$("#branchids").attr('name',<?= $this->session->userdata('snowworld')->branchid;?>);
                                                        });
                                                    </script>

                                                    <?php
                                                }
                                                ?>



                                                <select multiple name="branchids[]" required  id="branchids">
                                                    <?php foreach ($s_viewbranch as $key => $value) { ?>
                                                        <option value="<?php echo $value->branch_id; ?>"><?php echo $value->branch_name; ?></option>
                                                    <?php }?>
                                                </select></div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Promo Code Name:<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input"><input  type="text" name="promocode_name"  id="promocode_name" onblur="" /></div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> START DATE :<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input"><input  type="text" name="promocode_start"  id="promocode_start" onblur="" /></div>
                                        </div>
                                    </div>
                                </div>

                            </div>


                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">END DATE:<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input"><input  type="text" name="promocode_end"  id="promocode_end" onblur="" /></div>
                                        </div>
                                    </div>
                                </div>

                            </div>



                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Times allowed:</div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input"><input  type="text" name="promocode_allowed_times"  id="promocode_allowed_times" onblur="" /></div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Per user allowed:</div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input"><input  type="text" name="promocode_allowed_user"  id="promocode_allowed_user" onblur="" /></div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Addons:</div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <select name="addones[]"  multiple id="branchidss">
                                                    <?php foreach ($s_viewaddone as $key => $value) { ?>
                                                        <option value="<?php echo $value->addon_id; ?>"><?php echo $value->addon_name; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>




                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">User Type:<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <select name="user_type" id="user_type">

                                                    <option value="0">ALL</option>
                                                    <option value="1">User</option>
                                                    <option value="2">Guest</option>

                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>


                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Promo Code Type:</div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                               FLAT <input  type="radio" value="flat" id="flat" name="promo"/>


                                               PERCENT  <input max="99"  type="radio" value="percent" id="percent" name="promo"/>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="sep_box" id="flatpercent" style="display: none;">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Discount:</div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input  type="text" name="promonumber"  id="promonumber_flat" onblur="" />

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-8">
                                            <div class="submit_tbl">
                                                <input id="submitBtn" type="submit" name="submit" value="Submit" class="btn_button sub_btn" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function checkname(){
        var name=$('#cityname').val();

        $.ajax({
            url: '<?php echo base_url()?>admin/geographic/checkcityname',
            type: 'POST',
            dataType: 'json',
            data: {'name': name},
            success: function(data){

                if(data[0].count > 0){
                    $('#submitBtn').prop('disabled', 'disabled');
                    alert("Data Already Existed");
                }else{
                    $('#submitBtn').removeAttr('disabled');
                }
            }
        });

    }

    function selectstates(){

        var countryid=$('#countryid').val();
        $.ajax({
            url: '<?php echo base_url()?>admin/geographic/countrystate',
            type: 'POST',
            //dataType: 'json',
            data: {'countryid': countryid},
            success: function(data){
                var  option_brand = '<option value="">Select State</option>';
                $('#stateid').empty();
                $("#stateid").append(option_brand+data);

            }
        });

    }

    function selectcity(){

        var stateid=$('#stateid').val();
        $.ajax({
            url: '<?php echo base_url()?>admin/geographic/statecity',
            type: 'POST',
            //dataType: 'json',
            data: {'stateid': stateid},
            success: function(data){
                var  option_brand = '<option value="">Select City</option>';
                $('#cityid').empty();
                $("#cityid").append(option_brand+data);

            }
        });

    }

    function selectlocation(){
        var cityid = $('#cityid').val();
        $.ajax({
            url: '<?php echo base_url()?>admin/geographic/citylocation',
            type: "POST",
            data: {'cityid': cityid},
            success: function(data){
                var  option_brand = '<option value="">Select Location</option>';
                $('#locationid').empty();
                $("#locationid").append(option_brand+data);

            }

        })

    }



   $(function(){
       $("#flat").click(function () {
        $("#flatpercent").css('display','block');
       });
       $("#percent").click(function () {
           $("#flatpercent").css('display','block');
       });

       var date_input=$('input[name="promocode_start"],input[name="promocode_end"]'); //our date input has the name "date"
       var container=$('.bootstrap-iso form').length >0 ? $('.bootstrap-iso form').parent() : "body";
       date_input.datepicker({
           format: 'dd/mm/yyyy',
           container: container,
           todayHighlight: true,
           autoclose: true,
       });

       //$('#branchids').select2();


   })

</script>
<script>
    $(function () {
        $("#branchids,#act_ids").select2({
            placeholder: "Select the Data",
            allowClear: true
        }).select2('val', $('.select2 option:eq(0)').val());

        $("#updateaddonButton").on('click',function () {

            $("#update_act_ids,#update_branchids").select2({
                placeholder: "Select the Data",
                allowClear: true
            });



            /*var defaultData = [{id:1, text:'Item1'},{id:2,text:'Item2'},{id:3,text:'Item3'}];
             $('#update_act_ids').data().select2.updateSelection(defaultData);
             var a = $(this).parent('tr').find('ul [id=branch_map]').html('');
             console.log(a);*/

        });


        $("#act_ids").on('change',function () {

            var act =$(this).val().toString();
            var finalPrice = 0;
            var act_split = act.split(',');
            for(var count = 0 ; count < act_split.length ; count++)
            {
                $.ajax({
                    url: '<?php echo base_url()?>admin/addons/activityPrice',
                    type: 'POST',
                    dataType : "json",
                    data: {'act_id': act_split[count]},
                    success: function(data){
                        if(data.length > 0) {
                            finalPrice = finalPrice +  parseInt(data[0].activity_price);
                        }
                        $("#addon_price").val(finalPrice);
                    }
                });
            }

            //console.log(act_split.length);
            return false;

        })
    })







</script>

<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>

<!-- Include Date Range Picker -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>