<!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->
<script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery-1.9.1.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
<?php //pend($vieww); ?>
<!-- jQuery Form Validation code -->
<style>
    input.error{border:1px solid red;}
    label.error{border:0px solid red; color:red; font-weight: normal; display:inline; }
</style>

<script language='javascript'>

    // When the browser is ready...
    $(document).ready(function() {

        // Setup form validation on the #register-form element
        $("#addCont").validate({
            // Specify the validation rules
            rules: {
                countryid : "required",
                stateid : "required",
                cityname : "required",
                cityid : "required",
                locationid : "required",
                promocode_name : "required",
                promo :"required",
                promonumber : {required: true,
                    number: true},
                /* email:{
                 required: true,
                 email: true
                 },
                 password: {
                 required: true,
                 minlength: 5
                 },
                 agree: "required" */
            },
            // Specify the validation error messages
            messages: {
                countryid : "Country is required",
                stateid : "State is required",
                cityname : "City name is required",
                locationid : "location name is required",
                promocode_name :"promo code is required",
                promo : "select any type",
                promonumber : {required: "SELECT DISCOUNT PRICING",
                    number :"Number Only"}
                /*password: {
                 required: "Please provide a password",
                 minlength: "Your password must be at least 5 characters long"
                 },
                 email: "Please enter a valid email address",
                 agree: "Please accept our policy"*/
            },
            submitHandler: function(form) {
                form.submit();
            }
        });

    });

</script>

<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<div class="wrapper">

    <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">

                        <h2>ADD PROMO CODE</h2>
                    </div>
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-12">
                                <div style="text-align:right;">
                                    <a href="#"><button>CANCEL</button></a>
                                </div>
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?>
                                    <?php
                                    if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message');
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>





                        <div class="col-md-12">
                            <h2>Table</h2>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>S.NO</th>
                                        <th>PROMO CODE NAME</th>

                                        <th>TYPE</th>
                                        <th>DISCOUNT</th>
                                        <th>WEBURL</th>

                                        <th>PROMOCODE TYPE</th>
                                        <th>START DATE</th>
                                        <th>END DATE</th>
                                        <th>STATUS/DELETE</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php //pend($vieww);
                                    $i=1; foreach($vieww as $v) {?>
                                    <tr>
                                        <td><?= $i;?></td>
                                        <td><?php echo $v->p_codename; ?></td>

                                        <td><?php echo $v->p_type; ?></td>
                                        <td><?php echo $v->p_discount; ?>
                                        </td> <td><?php echo base_url()."paccoupon?couponid=".$v->p_codename ?>

                                        </td> <td><?php if($v->p_logintype == 0){echo "All";}
                                        elseif($v->p_logintype == 1){echo "User";} else {echo "guest";} ?></td>
                                        </td> <td><?php echo $v->p_start_date; ?></td>
                                        </td> <td><?php echo $v->p_expiry_date; ?></td>


                                        <td style="width:100%;">







                                            <a  href="<?= base_url('admin/promocodes/promo_status/')."/".base64_encode($v->promo_id)."/".base64_encode($v->p_status); ?>?empid=<?php echo $_GET['empid']; ?>&uid=<?php echo str_replace(".html","",$_GET['uid']); ?>" class="btn btn-primary"><?php echo $v->p_status == 0 ? "Inactive": "Active"; ?></a>



                                            <a href="<?= base_url('admin/promocodes/promo_delete/')."/".$v->promo_id ?>?empid=<?php echo $_GET['empid']; ?>&uid=<?php echo str_replace(".html","",$_GET['uid']); ?>"  onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger btn">
                                                <span class="glyphicon glyphicon-trash"></span>
                                            </a>


                                        </td>

                                    </tr>

                                    <?php $i++;} ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function checkname(){
        var name=$('#cityname').val();

        $.ajax({
            url: '<?php echo base_url()?>admin/geographic/checkcityname',
            type: 'POST',
            dataType: 'json',
            data: {'name': name},
            success: function(data){

                if(data[0].count > 0){
                    $('#submitBtn').prop('disabled', 'disabled');
                    alert("Data Already Existed");
                }else{
                    $('#submitBtn').removeAttr('disabled');
                }
            }
        });

    }

    function selectstates(){

        var countryid=$('#countryid').val();
        $.ajax({
            url: '<?php echo base_url()?>admin/geographic/countrystate',
            type: 'POST',
            //dataType: 'json',
            data: {'countryid': countryid},
            success: function(data){
                var  option_brand = '<option value="">Select State</option>';
                $('#stateid').empty();
                $("#stateid").append(option_brand+data);

            }
        });

    }

    function selectcity(){

        var stateid=$('#stateid').val();
        $.ajax({
            url: '<?php echo base_url()?>admin/geographic/statecity',
            type: 'POST',
            //dataType: 'json',
            data: {'stateid': stateid},
            success: function(data){
                var  option_brand = '<option value="">Select City</option>';
                $('#cityid').empty();
                $("#cityid").append(option_brand+data);

            }
        });

    }

    function selectlocation(){
        var cityid = $('#cityid').val();
        $.ajax({
            url: '<?php echo base_url()?>admin/geographic/citylocation',
            type: "POST",
            data: {'cityid': cityid},
            success: function(data){
                var  option_brand = '<option value="">Select Location</option>';
                $('#locationid').empty();
                $("#locationid").append(option_brand+data);

            }

        })

    }
    $(function(){
        $("#flat").click(function () {
            $("#flatpercent").css('display','block');
        });
        $("#percent").click(function () {
            $("#flatpercent").css('display','block');
        })
    })

</script>