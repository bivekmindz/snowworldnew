<!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->
<script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery-1.9.1.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js" crossorigin="anonymous"></script>
<?php //p($vieww); ?>
<!-- jQuery Form Validation code -->
<script>


    $(function() {
        // Initialize form validation on the registration form.
        // It has the name attribute "registration"
        $("#bookingtype").validate({
            // Specify validation rules
            rules: {
                // The key name on the left side is the name attribute
                // of an input field. Validation rules are defined
                // on the right side
                branchids : "required",
                book_name : "required",
            },
            // Specify validation error messages
            messages: {
                book_name: "Please enter name",
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
                $("#branchids").attr('disabled',false);
                form.submit();
            }
        });

    });


</script>
<style>
    input.error{border:1px solid red;}
    label.error{border:0px solid red; color:red; font-weight: normal; display:inline; }
</style>


<meta name="viewport" content="width=device-width, initial-scale=1">


<div class="wrapper">

    <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant pad_ding">
                <div class="col-lg-12">
                    <div class="page_name">

                        <h2>ADD Booking type</h2>
                    </div>
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-12">
                                <div style="text-align:right;">
                                    <a href=""><button>CANCEL</button></a>
                                </div>
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?>
                                    <?php
                                    if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message');
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>




                        <form action="<?= base_url('admin/groupbooking/bookingtype') ?>" name="bookingtype" id="bookingtype" method="post" enctype="multipart/form-data" >


                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Select Branch <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">



                                                <?php
                                                if($this->session->userdata('snowworld')->EmployeeId){
                                                    ?>
                                                    <script>
                                                        $(function(){
                                                            $('#branchids option[value="<?= $this->session->userdata('snowworld')->branchid; ?>"]').attr('selected',true);
                                                            $("#branchids").attr('disabled',true);
                                                            //$("#branchids").attr('name',<?= $this->session->userdata('snowworld')->branchid;?>);
                                                        });
                                                    </script>

                                                    <?php
                                                }
                                                ?>





                                                <select name="branchids"  id="branchids">
                                                    <option value="">SELECT BRANCH</option>
                                                    <?php foreach ($s_viewbranch as $key => $value) { ?>
                                                        <option value="<?php echo $value->branch_id; ?>"><?php echo $value->branch_name; ?></option>
                                                    <?php } ?>
                                                </select></div>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Booking type Name<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" id="book_name" name="book_name" >
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-8">
                                            <div class="submit_tbl">
                                                <input id="submitBtn" type="submit" name="submit" value="Submit" class="btn_button sub_btn" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </form>


                        <div class="col-md-12">
                        <div class="add_comp">
                            <h2>Table</h2>
                            <div class="table-responsive" style="width: 100%;">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>S.NO</th>
                                        <th>BOOKING TYPE NAME</th>
                                        <th>Branch</th>
                                        <th>DELETE</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=1; foreach($vieww_bookingtype as $v) {


                                        if($this->session->userdata('snowworld')->EmployeeId) {
                                            if($this->session->userdata('snowworld')->branchid != $v->booking_branchid)
                                            {
                                                continue;
                                            }
                                        }


                                        ?>
                                        <tr>
                                            <td><?= $i;?></td>
                                            <td id="t_com_name"><?php echo $v->bookingname; ?></td>
                                            <td id="t_com_name"><?php //echo $v->booking_branchid;


                                                $parameter = array( 'act_mode'=>'getbranch',
                                                    'Param1'=>$v->booking_branchid,
                                                    'Param2'=>'',
                                                    'Param3'=>'',
                                                    'Param4'=>'',
                                                    'Param5'=>'',
                                                    'Param6'=>'',
                                                    'Param7'=>'',
                                                    'Param8'=>'',
                                                    'Param9'=>'');
                                                //p($parameter);
                                                $path = api_url()."main_snowworld_v/getlocation/format/json/";
                                                $br_name = curlpost($parameter,$path);
                                                $br_name = (array)$br_name;
                                                echo ($br_name[0]['branch_name']);



                                            ?></td>
                                            <td>

                                              <!--  <a class="btn btn-primary" onclick="return update_com('<?php /*echo $v->comp_name; */?>','<?php /*echo $v->comp_id; */?>')">
                                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                                </a>-->




                                                <a href="<?= base_url('admin/groupbooking/bookingtype_delete')."/".$v->bookingtypeid ?>"  onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger btn">
                                                    <span class="glyphicon glyphicon-trash"></span>
                                                </a>







                                            </td>

                                        </tr>

                                        <?php $i++;} ?>
                                    </tbody>
                                </table>
                            </div>
                            </div>
                        </div>




                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script>

    function update_com(a,b){
        //alert(a +b);
        $("#com_name_update").val(a);
        $("#com_id").val(b);
        $("#update_com").css('display','block');
        $("#add_com").css('display','none');
        $("#com_name_update").focus();

    }
    function UpdateCancel() {
        $("#add_com").css('display','block');
        $("#update_com").css('display','none');
        return false;
    }





</script>