

<?php $paymentup1 = $paymentup['0'];?><!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<style>
    input.error {
        border: 1px solid red;
    }

    label.error {
        border: 0px solid red;
        color: red;
        font-weight: normal;
        display: inline;
    }

    .to {
        margin-right: 10px;
    }

    .to_input {
        width: 67% !important;
    }

    .fonrr {
        font-size: 26px;
        margin-left: -14px;
        color: rgba(0, 0, 0, 0.43);
        margin-top: 7px;
    }

    .tbl_input {
        margin-bottom: 15px;
    }

    .table_add {
        width: 100%;
        float: left;
    }

    .timebox{ width:100%; float:left; font-size:14px; color:#777676;}

    .timebox button, input, select, textarea {padding: 3px 5px 3px 7px;
        border: 1px solid rgba(0, 0, 0, 0.15);
        margin: 3px 9px 13px 9px;
    }

    .branche_time{ width:100%; float:left;}

    .branche_time li{padding: 3px 0px 12px 0px;}

    .branche_time li button{    padding: 8px 20px 8px 20px; margin-left: 15px; font-size:20px;}

</style>


<meta name="viewport" content="width=device-width, initial-scale=1">


<div class="wrapper">

    <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">

                        <h2>Payment</h2>
                    </div>
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-12">
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?>
                                    <?php
                                    if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message');
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>


                        <form action="" name="add_com" id="add_com"
                              method="post" enctype="multipart/form-data">


                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Select Branch <span
                                                        style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">




                                                <?php
                                                if($this->session->userdata('snowworld')->EmployeeId){
                                                    ?>
                                                    <script>
                                                        $(function(){
                                                            $('#branchids option[value="<?= $this->session->userdata('snowworld')->branchid; ?>"]').attr('selected',true);
                                                            $("#branchids").attr('disabled',true);
                                                            //$("#branchids").attr('name',<?= $this->session->userdata('snowworld')->branchid;?>);
                                                        });
                                                    </script>

                                                    <?php
                                                }
                                                ?>



                                                <select name="branchids" id="branchids">
                                                    <?php foreach ($s_viewbranch as $key => $value) { ?>
                                                        <option <?php if($paymentup1->pg_branchid == $value->branch_id) {?> selected="selected"<?php } ?>  value="<?php echo $value->branch_id; ?>"><?php echo $value->branch_name; ?></option>
                                                    <?php } ?>
                                                </select></div>
                                        </div>
                                    </div>
                                    </div>
                                </div>


                                <div class="sep_box">
                                    <div class="col-lg-6">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="tbl_text">Merchant id<span style="color:red;font-weight: bold;">*</span></div>
                                            </div>
                                            <div class="col-lg-8">
                                                <div class="tbl_input">
                                                    <input type="text" value="<?php echo $paymentup1->pg_merchant_id; ?>" name="merchant_id" id="merchant_id">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Access key<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" value="<?php echo $paymentup1->pg_access_key; ?>" name="access_key" id="access_key">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Working key<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" value="<?php echo $paymentup1->pg_working_key; ?>" name="working_key" id="working_key">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Sucess url<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" value="<?php echo $paymentup1->pg_sucess_link; ?>" name="sucess_url" id="sucess_url">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Fail url<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" value="<?php echo $paymentup1->pg_fail_link; ?>" name="fail_url" id="fail_url">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Currency<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" value="<?php echo $paymentup1->pg_currency; ?>" name="currency_nam" id="currency_nam">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Language<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" value="<?php echo $paymentup1->pg_language; ?>" name="language_nam" id="language_nam">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Prefix<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" value="<?php echo $paymentup1->pg_prefix; ?>" name="prefix_nam" id="prefix_nam">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                                <div class="sep_box">
                                    <div class="col-lg-6">
                                        <div class="row">
                                            <div class="col-lg-4"></div>
                                            <div class="col-lg-8">
                                                <div class="submit_tbl">
                                                    <input id="submitBtn" type="submit" name="submit" value="Update"
                                                           class="btn_button sub_btn"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                        </form>


                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js" crossorigin="anonymous"></script>
<script>


    $(function() {
        //alert('asdsdf');

        // Initialize form validation on the registration form.
        // It has the name attribute "registration"
        $("#add_com").validate({
            // Specify validation rules
            rules: {
                // The key name on the left side is the name attribute
                // of an input field. Validation rules are defined
                // on the right side
                merchant_id: "required",
                access_key: "required",
                working_key: "required",
                sucess_url: "required",
                fail_url: "required",
                currency_nam: "required",
                language_nam: "required",
                prefix_nam: "required"
            },

            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function (form) {
                form.submit();
            }
        });
    });
</script>