<!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"
        crossorigin="anonymous"></script>
<?php //p($vieww); ?>
<!-- jQuery Form Validation code -->
<script>
    $(function() {




       // $('#update_branchids option[value="'+myarr[i]+'"]').attr('selected','selected');
        // Initialize form validation on the registration form.
        // It has the name attribute "registration"
        $("#add_addon").validate({
            ignore:".ignore, .select2-input",
            // Specify validation rules
            rules: {
                // The key name on the left side is the name attribute
                // of an input field. Validation rules are defined
                // on the right side
                addon_name: "required",
                addon_desc: "required",
                branchids: "required",
                act_ids: "required",
                addon_price: {
                    "required": true,
                    "number": true,
                }

            },
            // Specify validation error messages
            messages: {
                addon_name: "Please enter name",
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
                    form.submit();

            }
        });
    });


</script>
<style>
    input.error{border:1px solid red;}
    label.error{border:0px solid red; color:red; font-weight: normal; display:inline; }
</style>


<meta name="viewport" content="width=device-width, initial-scale=1">


<div class="wrapper">

    <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">

                        <h2>Add addon</h2>
                    </div>
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-12">
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?>
                                    <?php
                                    if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message');
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>

                        <form action="" name="add_addon" id="add_addon" method="post" enctype="multipart/form-data" >


                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">addon Name<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" value="<?php echo $vieww['0']->addon_name; ?>" id="addon_name" name="addon_name" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">addon Description<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <textarea  id="addon_desc" value=""  name="addon_desc" ><?php echo $vieww['0']->addon_description; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Select Branch <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">


                                                <?php
                                                if($this->session->userdata('snowworld')->EmployeeId){
                                                    ?>
                                                    <script>
                                                        $(function(){
                                                            $('#branchids option[value="<?= $this->session->userdata('snowworld')->branchid; ?>"]').attr('selected',true);
                                                            $("#branchids").attr('disabled',true);
                                                            //$("#branchids").attr('name',<?= $this->session->userdata('snowworld')->branchid;?>);
                                                        });
                                                    </script>

                                                    <?php
                                                }
                                                ?>


                                                <select name="branchids" required  id="branchids">
                                                    <?php foreach ($s_viewbranch as $key => $value) { ?>
                                                        <option value="<?php echo $value->branch_id; ?>" <?php if($vieww['0']->addon_branchid == $value->branch_id){echo "selected";} ?>><?php echo $value->branch_name; ?></option>
                                                    <?php } ?>
                                                </select></div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">addon Price<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" value="<?php echo $vieww['0']->addon_price; ?>"  id="addon_price" name="addon_price" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Addon Image<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="file" id="addon_image" value="<?php echo $vieww['0']->addon_image; ?>" name="addon_image" >
                                               Prevoius Image : <img src="<?php echo base_url('assets/admin/images/'.$vieww['0']->addon_image); ?>">
                                                <input type="hidden" name="update_addon_id" value="<?php echo  base64_decode($this->uri->segment(4));?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>




                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-8">
                                            <div class="submit_tbl">
                                                <input id="submitBtn" type="submit" name="submit" value="Update" class="btn_button sub_btn" /> &nbsp;
                                                <a  href="<?= base_url('admin/addon/addAddon'); ?>" class="btn btn-primary">CANCEL</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </form>

                        




                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function () {
        $("#branchids,#act_ids").select2({
            placeholder: "Select the Data",
            allowClear: true
        }).select2('val', $('.select2 option:eq(0)').val());

        $("#updateaddonButton").on('click',function () {

            $("#update_act_ids,#update_branchids").select2({
                placeholder: "Select the Data",
                allowClear: true
            });



            /*var defaultData = [{id:1, text:'Item1'},{id:2,text:'Item2'},{id:3,text:'Item3'}];
             $('#update_act_ids').data().select2.updateSelection(defaultData);
             var a = $(this).parent('tr').find('ul [id=branch_map]').html('');
             console.log(a);*/

        });


        $("#act_ids").on('change',function () {

            var act =$(this).val().toString();
            var finalPrice = 0;
            var act_split = act.split(',');
            for(var count = 0 ; count < act_split.length ; count++)
            {
                $.ajax({
                    url: '<?php echo base_url()?>admin/addons/activityPrice',
                    type: 'POST',
                    dataType : "json",
                    data: {'act_id': act_split[count]},
                    success: function(data){
                        if(data.length > 0) {
                            finalPrice = finalPrice +  parseInt(data[0].activity_price);
                        }
                        $("#addon_price").val(finalPrice);
                    }
                });
            }

            //console.log(act_split.length);
            return false;

        })
    })







</script>