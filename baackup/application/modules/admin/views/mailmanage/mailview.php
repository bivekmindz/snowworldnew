<!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet"> -->
<script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery-1.9.1.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js" crossorigin="anonymous"></script>

<?php //p($vieww); ?>
<!-- jQuery Form Validation code -->
<script>


    $(function() {
        $("#add_mail").validate({
            rules: {
                branchids: "required",
                mail_image: "required",
                mail_text1: "required",
                mail_text2: "required",
                mail_text3: "required",
                mail_text4: "required",
                mail_subject: "required",
                mail_from: "required",
                mail_branch_phone: "required",
                mail_branch_email: "required",
                mail_branch_apicode: "required",
            },
            messages: {
                com_name: "Please enter name",
            },
            submitHandler: function(form) {
                form.submit();
            }
        });



        $("#update_com").validate({
            // Specify validation rules
            rules: {
                // The key name on the left side is the name attribute
                // of an input field. Validation rules are defined
                // on the right side
                com_name_update: "required",
            },
            // Specify validation error messages
            messages: {
                com_name_update: "Please enter name",
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
                $("#branchids").attr('disabled',false);
                form.submit();
            }
        });
    });


</script>
<style>
    input.error{border:1px solid red;}
    label.error{border:0px solid red; color:red; font-weight: normal; display:inline; }
    .tbl_input textarea{        display: -webkit-box !important;}

</style>


<meta name="viewport" content="width=device-width, initial-scale=1">


<div class="wrapper">

    <div class="col-lg-10 col-lg-push-2">
        <div class="row">
            <div class="page_contant">
                <div class="col-lg-12">
                    <div class="page_name">

                        <h2>MAIL MANAGEMENT</h2>
                    </div>
                    <div class="page_box">
                        <div class="sep_box">
                            <div class="col-lg-12">
                                <div class='flashmsg'>
                                    <?php echo validation_errors(); ?>
                                    <?php
                                    if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message');
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>


                        <form action="" name="add_mail" id="add_mail" method="post" enctype="multipart/form-data" >


                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Select Branch <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">


                                                <?php
                                                if($this->session->userdata('snowworld')->EmployeeId){
                                                    ?>
                                                    <script>
                                                        $(function(){
                                                            $('#branchids option[value="<?= $this->session->userdata('snowworld')->branchid; ?>"]').attr('selected',true);
                                                            $("#branchids").attr('disabled',true);
                                                            //$("#branchids").attr('name',<?= $this->session->userdata('snowworld')->branchid;?>);
                                                        });
                                                    </script>

                                                    <?php
                                                }
                                                ?>



                                                <select name="branchids" required  id="branchids">
                                                    <?php foreach ($s_viewbranch as $key => $value) { ?>
                                                        <option value="<?php echo $value->branch_id; ?>"><?php echo $value->branch_name; ?></option>
                                                    <?php }?>
                                                </select></div>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Select LOGO <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="file" id="mail_image" name="mail_image" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>





                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Heading 1 <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <textarea  id="mail_text1" style="display: block" name="mail_text1" ></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Heading 2 <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <textarea  id="mail_text2" style="display: block" name="mail_text2" ></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Heading 3 <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <textarea  id="mail_text3" style="display: block" name="mail_text3" ></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Heading 4 <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <textarea  id="mail_text4" style="display: block" name="mail_text4" ></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Subject <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" id="mail_subject" name="mail_subject" >
                                            </div>
                                        </div>
                                    </div>`
                                </div>
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> From <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" id="mail_from" name="mail_from" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Branch phone <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text"  minlength="10"  id="mail_branch_phone" name="mail_branch_phone" >
                                            </div>
                                        </div>
                                    </div>`
                                </div>
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Branch Email <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="email" id="mail_branch_email" name="mail_branch_email" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="sep_box" >

                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Branch Api Code <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" id="mail_branch_apicode" name="mail_branch_apicode" >
                                            </div>
                                        </div>
                                    </div>
                                </div>




                                <div class="col-lg-6" style="display:none;">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text"> Internet Charge <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                                <input type="text" id="mail_branch_internethandlingcharge" name="mail_branch_internethandlingcharge" >
                                            </div>
                                        </div>
                                    </div>`
                                </div>
                                





                            <div class="sep_box">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-8">
                                            <div class="submit_tbl">
                                                <input id="submitBtn" type="submit" name="submit" value="Submit" class="btn_button sub_btn" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </form>


                        <div class="col-md-12">
                            <h2>Table</h2>
                            <div class="table-responsive" style="width: 100%;">
                                <table class="table" >
                                    <thead>
                                    <tr>
                                        <th>S.NO</th>
                                        <th>Mail logo</th>
                                        <td>Mail branch</td>
                                        <strong><td>Heading 1</td></strong>
                                        <th>Heading 2</th>
                                        <th>Heading 3</th>
                                        <th>Heading 4</th>
                                        <th>From</th>
                                        <th>Branch contact number</th>
                                        <th>Branch email</th>
                                        <th>Branch Api</th>
                                        <th>Status</th>
                                        <th>EDIT/DELETE</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=1;//pend($vieww);
                                    foreach($vieww as $v) {
                                        if($this->session->userdata('snowworld')->EmployeeId) {
                                            if($this->session->userdata('snowworld')->branchid != $v->bannerimage_branch)
                                            {
                                                continue;
                                            }
                                        }
                                        ?>

                                            <td><?= $i;?></td>
                                            <td>
                                            <img style="height: 50px;" src="<?php echo base_url('assets/admin/images/'.$v->bannerimage_logo); ?>" >

                                            </td>

                                            <td><?php

                                                $parameter = array('act_mode' => 'getbranch',
                                                    'Param1' => $v->bannerimage_branch,
                                                    'Param2' => '',
                                                    'Param3' => '',
                                                    'Param4' => '',
                                                    'Param5' => '',
                                                    'Param6' => '',
                                                    'Param7' => '',
                                                    'Param8' => '',
                                                    'Param9' => '');
                                                //p($parameter);
                                                $path = api_url() . "main_snowworld_v/getlocation/format/json/";
                                                $br_name = (array)curlpost($parameter, $path);
                                                echo($br_name[0]['branch_name']);
                                                ?></td>
                                            <td><?php echo $v->bannerimage_top1; ?></td>
                                            <td><?php echo $v->bannerimage_top2; ?></td>
                                            <td><?php echo $v->bannerimage_top3; ?></td>
                                            <td><?php echo $v->bannerimage_top4; ?></td>
                                            <td><?php echo $v->bannerimage_from; ?></td>
                                            <td><?php echo $v->bannerimage_branch_contact; ?></td>
                                            <td><?php echo $v->bannerimage_branch_email; ?></td>

                                        <td><?php echo $v->bannerimage_apikey; ?></td>
                                            <td>

                                                <a  href="<?= base_url('admin/managemail/mailviewstatus/')."/".base64_encode($v->bannerimage_id)."/".base64_encode($v->bannerimage_status); ?>?empid=<?php echo $_GET['empid']; ?>&uid=<?php echo str_replace(".html","",$_GET['uid']); ?>" class="btn btn-primary"><?php echo $v->bannerimage_status == 0 ? "Inactive": "Active"; ?></a>
                                         </td>
                                            <td>

                                                <a  href="<?= base_url('admin/managemail/mailviewupdate/')."/".base64_encode($v->bannerimage_id); ?>?empid=<?php echo $_GET['empid']; ?>&uid=<?php echo str_replace(".html","",$_GET['uid']); ?>" class="btn btn-primary">


                                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                                </a>



                                                <a  href="<?= base_url('admin/managemail/mailviewdelete/')."/".base64_encode($v->bannerimage_id); ?>?empid=<?php echo $_GET['empid']; ?>&uid=<?php echo str_replace(".html","",$_GET['uid']); ?>" class="btn btn-danger btn" onclick="return confirm('Are you sure you want to delete this item?');" >
                                                   <span class="glyphicon glyphicon-trash"></span>
                                                </a>







                                            </td>

                                        </tr>

                                        <?php $i++;} ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>




                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script>

    function update_com(a,b){
        //alert(a +b);
        $("#com_name_update").val(a);
        $("#com_id").val(b);
        $("#update_com").css('display','block');
        $("#add_com").css('display','none');
        $("#com_name_update").focus();

    }
    function UpdateCancel() {
        $("#add_com").css('display','block');
        $("#update_com").css('display','none');
        return false;
    }





</script>
