<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Addon extends MX_Controller
{
    public function __construct()
    {
        $this->load->model("supper_admin");
        $this->load->helper('my_helper');
 $this->load->library('session');
    }

    /*add addon*/
    public function addAddon()
    {

        if ($this->input->post('submit')) {

           /* echo $_FILES['addon_image']['name'];
            die();*/
            $this->form_validation->set_rules('addon_name', 'name', 'required');
            $this->form_validation->set_rules('addon_desc', 'description', 'required');
            $this->form_validation->set_rules('branchids', 'branch', 'required');
            $this->form_validation->set_rules('addon_price', 'price', 'required');

            if ($this->form_validation->run() != FALSE) {
                $configUpload['upload_path']    = './assets/admin/images';              #the folder placed in the root of project
                $configUpload['allowed_types']  = 'gif|jpg|png|bmp|jpeg';       #allowed types description
                $configUpload['max_size']       = '0';                          #max size
                $configUpload['max_width']      = '0';                          #max width
                $configUpload['max_height']     = '0';                          #max height
                $configUpload['encrypt_name']   = true;                         #encrypt name of the uploaded file
                $this->load->library('upload', $configUpload);                  #init the upload class
                if(!$this->upload->do_upload('addon_image')){
                    $uploadedDetails    = $this->upload->display_errors();
                    $this->session->set_flashdata('message', $uploadedDetails);
                }else {
                    $uploadedDetails    = $this->upload->data();
                    $this->session->set_flashdata('message', 'inserted sucessfully');
                    foreach ($this->input->post('branchids') as $branch) {
                        $parameter_branch = array('act_mode' => 'addbranchAddon',
                            'Param1' => $this->input->post('addon_name'),
                            'Param2' => $branch,
                            'Param3' => $this->input->post('addon_desc'),
                            'Param4' => $this->input->post('addon_price'),
                            'Param5' => $uploadedDetails['file_name'],
                            'Param6' => '',
                            'Param7' => '',
                            'Param8' => '',
                            'Param9' => '');
                        //pend($parameter_branch);
                        $response_branch = $this->supper_admin->call_procedure('proc_addon_s', $parameter_branch);
                        $this->session->set_flashdata('message', 'inserted sucessfully');
                    }
                }
                //pend($response_act);


            }
        }

        $parameter2 = array('act_mode' => 's_viewbranch',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        $response['s_viewbranch'] = $this->supper_admin->call_procedure('proc_location_v', $parameter2);

        $parameter4 = array('act_mode' => 's_viewaddon_admin',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //pend($parameter);
        $response['vieww_addon'] = $this->supper_admin->call_procedure('proc_addon_s', $parameter4);
        $parameter = array('act_mode' => 'viewcountry', 'row_id' => '', 'counname' => '', 'coucode' => '', 'commid' => '');
        $response['vieww'] = $this->supper_admin->call_procedure('proc_geographic', $parameter);
        //pend( $response['vieww_addon']);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('addon/addAddon', $response);

    }
    public function addondelete($a)
    {
        $parameter4 = array('act_mode' => 's_deleteaddon_admin',
            'Param1' => $a,
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //pend($parameter);
        $response = $this->supper_admin->call_procedure('proc_addon_s', $parameter4);
        redirect("admin/addon/addAddon?empid=".$_GET['empid']."&uid=".str_replace(".html","",$_GET['uid'])."");

    }

    public function addonstatus($a,$b)
    {
        $status =  base64_decode($b)== 1 ? 0 : 1;
        $parameter4 = array('act_mode' => 's_statusaddon_admin',
            'Param1' => base64_decode($a),
            'Param2' => $status,
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //pend($parameter4);
        $response = $this->supper_admin->call_procedure('proc_addon_s', $parameter4);
        redirect("admin/addon/addAddon?empid=".$_GET['empid']."&uid=".str_replace(".html","",$_GET['uid'])."");
    }



    public  function addonviewupdate($a)
    {


        if ($this->input->post('submit')) {
            //p($_FILES['addon_image']['name']);
           //pend($_POST);
            if ($_FILES['addon_image']['name'] == "") {
                $this->form_validation->set_rules('addon_name', 'name', 'required');
                $this->form_validation->set_rules('addon_desc', 'description', 'required');
                $this->form_validation->set_rules('branchids', 'branch', 'required');
                $this->form_validation->set_rules('addon_price', 'price', 'required');
                $parameter_branch = array('act_mode' => 'addbranchAddon_update',
                    'Param1' => $this->input->post('addon_name'),
                    'Param2' => $this->input->post('update_addon_id'),
                    'Param3' => $this->input->post('addon_desc'),
                    'Param4' => $this->input->post('addon_price'),
                    'Param5' => $this->input->post('branchids'),
                    'Param6' => '',
                    'Param7' => '',
                    'Param8' => '',
                    'Param9' => '');
                //pend($parameter_branch);
                $response_branch = $this->supper_admin->call_procedure('proc_addon_s', $parameter_branch);
                redirect("admin/addon/addAddon?empid=".$_GET['empid']."&uid=".str_replace(".html","",$_GET['uid'])."");

            } else{

                $this->form_validation->set_rules('addon_name', 'name', 'required');
            $this->form_validation->set_rules('addon_desc', 'description', 'required');
            $this->form_validation->set_rules('branchids', 'branch', 'required');
            $this->form_validation->set_rules('addon_price', 'price', 'required');

            if ($this->form_validation->run() != FALSE) {
                $configUpload['upload_path'] = './assets/admin/images';              #the folder placed in the root of project
                $configUpload['allowed_types'] = 'gif|jpg|png|bmp|jpeg';       #allowed types description
                $configUpload['max_size'] = '0';                          #max size
                $configUpload['max_width'] = '0';                          #max width
                $configUpload['max_height'] = '0';                          #max height
                $configUpload['encrypt_name'] = true;                         #encrypt name of the uploaded file
                $this->load->library('upload', $configUpload);                  #init the upload class
                if (!$this->upload->do_upload('addon_image')) {
                    $uploadedDetails = $this->upload->display_errors();
                    $this->session->set_flashdata('message', $uploadedDetails);
                } else {
                    $uploadedDetails = $this->upload->data();
                    $this->session->set_flashdata('message', 'updated sucessfully');
                        $parameter_branch = array('act_mode' => 'addbranchAddon_update_pic',
                            'Param1' => $this->input->post('addon_name'),
                            'Param2' => $this->input->post('update_addon_id'),
                            'Param3' => $this->input->post('addon_desc'),
                            'Param4' => $this->input->post('addon_price'),
                            'Param5' => $uploadedDetails['file_name'],
                            'Param6' => $this->input->post('branchids'),
                            'Param7' => '',
                            'Param8' => '',
                            'Param9' => '');
                        //pend($parameter_branch);
                        $response_branch = $this->supper_admin->call_procedure('proc_addon_s', $parameter_branch);
                        $this->session->set_flashdata('message', 'updated sucessfully');
                    redirect("admin/addon/addAddon?empid=".$_GET['empid']."&uid=".str_replace(".html","",$_GET['uid'])."");

                }
                //pend($response_act);


            }
        }
        }



        $parameter2 = array('act_mode' => 's_viewbranch',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        $response['s_viewbranch'] = $this->supper_admin->call_procedure('proc_location_v', $parameter2);


        $parameter4 = array('act_mode' => 'addonviewupdate',
            'Param1' => base64_decode($a),
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //pend($parameter);
        $response['vieww'] = (array)$this->supper_admin->call_procedure('proc_addon_s', $parameter4);
//p($response['vieww']);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('addon/updateAddon', $response);



    }

    }// end class
?>