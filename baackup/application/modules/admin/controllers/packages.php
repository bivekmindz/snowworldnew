<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Packages extends MX_Controller
{
    public function __construct()
    {
        $this->load->model("supper_admin");
        $this->load->helper('my_helper');
 $this->load->library('session');
    }

    /*Add package by zzz*/
    public function addPackage()
    {
        if ($this->input->post('submit')) {
            //pend($_POST);
            $this->form_validation->set_rules('package_name', 'package_name', 'required');
            $this->form_validation->set_rules('package_desc', 'package_desc', 'required');
            $this->form_validation->set_rules('branchids', 'branchids', 'required');
            //$this->form_validation->set_rules('act_ids', 'act_ids', 'required');
            $this->form_validation->set_rules('package_price', 'package_price', 'required');
            if ($this->form_validation->run() != FALSE) {
                $parameter_pack_master = array('act_mode' => 's_addpackage_master',
                    'Param1' => $this->input->post('package_name'),
                    'Param2' => $this->input->post('package_desc'),
                    'Param3' => $this->input->post('package_price'),
                    'Param4' => '',
                    'Param5' => '',
                    'Param6' => '',
                    'Param7' => '',
                    'Param8' => '',
                    'Param9' => '');
                //pend($parameter_pack_master);
                $response = $this->supper_admin->call_procedure('proc_packages_s', $parameter_pack_master);
                // pend($response);
                if ($response[0]->pack_id != '') {
                    foreach ($this->input->post('branchids') as $branch) {
                        $parameter_branch = array('act_mode' => 's_addpackage_branch',
                            'Param1' => $response[0]->pack_id,
                            'Param2' => $branch,
                            'Param3' => '',
                            'Param4' => '',
                            'Param5' => '',
                            'Param6' => '',
                            'Param7' => '',
                            'Param8' => '',
                            'Param9' => '');
                        // pend($parameter_branch);
                        $response_branch = $this->supper_admin->call_procedure('proc_packages_s', $parameter_branch);
                    }
                    /*foreach ($this->input->post('act_ids') as $act) {
                        $parameter_act = array('act_mode' => 's_addpackage_activity',
                            'Param1' => $response[0]->pack_id,
                            'Param2' => $act,
                            'Param3' => '',
                            'Param4' => '',
                            'Param5' => '',
                            'Param6' => '',
                            'Param7' => '',
                            'Param8' => '',
                            'Param9' => '');
                        //pend($parameter_branch);
                        $response_act = $this->supper_admin->call_procedure('proc_packages_s', $parameter_act);
                    }*/
                    //pend($response_act);
                    $this->session->set_flashdata('message', 'inserted sucessfully');


                }


            }
        }


        $parameter2 = array( 'act_mode'=>'s_viewbranch',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        $response['s_viewbranch'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter2);
        //pend($response['s_viewbranch']);
        $parameter3 = array( 'act_mode'=>'s_viewactivity',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        //pend($parameter);
        $response['vieww_act'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter3);
        $parameter4 = array( 'act_mode'=>'s_viewpackage',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        //pend($parameter);
        $response['vieww_pack'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter4);
        //pend( $response['vieww_pack']);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('package/addPackage',$response);

    }

    /*get price of total activities by zzz*/
    public function activityPrice()
    {
        $parameter4 = array( 'act_mode'=>'activityPrice',
            'Param1'=> $this->input->post('act_id'),
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        //pend($parameter);
        $response = $this->supper_admin->call_procedure('proc_packages_s',$parameter4);
        print_r(json_encode($response));
    }


    public function update_package_data()
    {
        $a =  $_POST['pack_id'];
        $parameter4 = array( 'act_mode'=>'update_package_data',
            'Param1'=> $a,
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        //pend($parameter4);
        $response = $this->supper_admin->call_procedure('proc_packages_s',$parameter4);
        print_r(json_encode($response));
    }

    /*Delete package By zzz*/
    public function packageDelete1()
    {
        $parameter = array('act_mode' => 'delete_package',
            'Param1' => $this->uri->segment('4'),
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');

        $response['vieww'] = $this->supper_admin->call_procedure('proc_packages_s', $parameter);

        redirect("admin/packages/addPackage?empid=".$_GET['empid']."&uid=".str_replace(".html","",$_GET['uid'])."");


    }

    public function updatePackage()
    {
        //pend($_POST);
        $this->form_validation->set_rules('update_package_name', 'package_name', 'required');
        $this->form_validation->set_rules('update_package_desc', 'package_desc', 'required');
        $this->form_validation->set_rules('update_branchids', 'branchids', 'required');
        //$this->form_validation->set_rules('act_ids', 'act_ids', 'required');
        $this->form_validation->set_rules('update_package_price', 'package_price', 'required');
        if ($this->form_validation->run() != FALSE) {
            $parameter_pack_master = array('act_mode' => 's_addpackage_master_update',
                'Param1' => $this->input->post('update_package_name'),
                'Param2' => $this->input->post('update_package_desc'),
                'Param3' => $this->input->post('update_package_price'),
                'Param4' => $this->input->post('pack_update_hidden_id'),
                'Param5' => '',
                'Param6' => '',
                'Param7' => '',
                'Param8' => '',
                'Param9' => '');
            //pend($parameter_pack_master);
            $response = $this->supper_admin->call_procedure('proc_packages_s', $parameter_pack_master);
            // pend($response);

                foreach ($this->input->post('update_branchids') as $branch) {
                    $parameter_branch = array('act_mode' => 's_addpackage_branch_update',
                        'Param1' => $this->input->post('pack_update_hidden_id'),
                        'Param2' => $branch,
                        'Param3' => '',
                        'Param4' => '',
                        'Param5' => '',
                        'Param6' => '',
                        'Param7' => '',
                        'Param8' => '',
                        'Param9' => '');
                    // pend($parameter_branch);
                    $response_branch = $this->supper_admin->call_procedure('proc_packages_s', $parameter_branch);
                }
                /*foreach ($this->input->post('act_ids') as $act) {
                    $parameter_act = array('act_mode' => 's_addpackage_activity',
                        'Param1' => $response[0]->pack_id,
                        'Param2' => $act,
                        'Param3' => '',
                        'Param4' => '',
                        'Param5' => '',
                        'Param6' => '',
                        'Param7' => '',
                        'Param8' => '',
                        'Param9' => '');
                    //pend($parameter_branch);
                    $response_act = $this->supper_admin->call_procedure('proc_packages_s', $parameter_act);
                }*/
                //pend($response_act);
                $this->session->set_flashdata('message', 'updated sucessfully');
            redirect("admin/packages/addPackage?empid=".$_GET['empid']."&uid=".str_replace(".html","",$_GET['uid'])."");

        }
        else{
            $this->session->set_flashdata('message', 'not updated');
            redirect("admin/packages/addPackage?empid=".$_GET['empid']."&uid=".str_replace(".html","",$_GET['uid'])."");
        }



    }



}// end class
?>