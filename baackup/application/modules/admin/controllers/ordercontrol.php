<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Ordercontrol extends MX_Controller
{
    public function __construct()
    {
        $this->load->model("supper_admin");
        $this->load->helper('my_helper');
        //$this->load->library('session');
 $this->load->helper('adminmenu_helper');
    }

    /*listing filter */
    public function order_sess(){

        if($_POST['Clear']=='Show All Records')
        {
            $this->session->unset_userdata('order_filter');
        }
  

$a = explode(":",$this->input->post('filter_date_session'));
        $b = explode(" ",$this->input->post('filter_date_session'));
//p($a);
//p($b);

if($b['1'] == 'PM'){
   $session_time_param = $a['0'] + 12;
}
else{
   $session_time_param = $a['0'];
}
        //p($_POST);
        //$this->session->unset_userdata('order_filter');
        $a = $this->input->post('filter_branch');
        $b = $this->input->post('filter_status');
        $c = $this->input->post('filter_tim');
        $d = $this->input->post('filter_sta');

         $branchids = implode(',',$a);
         $filterstatus = implode(',',$b);
         $filtertime = implode(',',$c);
         $filtersta = implode(',',$d);
        $array = array('branchids' =>$branchids,
            'filter_name' => $this->input->post('filter_name'),
            'filter_email' => $this->input->post('filter_email'),
            'filter_ticket' => $this->input->post('filter_ticket'),
            'filter_mobile' => $this->input->post('filter_mobile'),
            'filter_payment' => $this->input->post('filter_payment'),
            'filter_date_booking_from' => $this->input->post('filter_date_booking_from'),
            'filter_date_booking_to' => $this->input->post('filter_date_booking_to'),
            'filter_date_session_from' => $this->input->post('filter_date_session_from'),
            'filter_date_session_to' => $this->input->post('filter_date_session_to'),
            'filter_status' => $filterstatus,
            'filter_date_printed_from' => $this->input->post('filter_date_printed_from'),
            'filter_date_printed_to' => $this->input->post('filter_date_printed_to'),
            'filter_date_ticket' => $this->input->post('filter_date_ticket'),
            'filter_date_session' =>    $session_time_param,
            'filter_tim' => $filtertime,
            'filter_sta' => $filtersta
            );
        $this->session->set_userdata('order_filter',$array);
        redirect('admin/ordercontrol/order?empid='.$_GET['empid'].'&uid='.str_replace(".html","",$_GET['uid']));


    }


        /*view all orders*/
    public function order()
    {

if(getMemberId()==1)

{

}
else
{
  redirect('admin/login/dashboard?empid='.$_GET['empid'].'&uid='.$_GET['uid'].'');
}
     //p($_POST);
    
       //$this->session->unset_userdata('order_filter');

        if($this->session->userdata('order_filter'))
        {
           //p($this->session->userdata('order_filter'));
                 $parameter1 = array('act_mode' => 'S_vieworder',
                'Param1' => $this->session->userdata('order_filter')['branchids'],
                'Param2' => $this->session->userdata('order_filter')['filter_name'],
                'Param3' => $this->session->userdata('order_filter')['filter_email'],
                'Param4' => $this->session->userdata('order_filter')['filter_ticket'],
                'Param5' => $this->session->userdata('order_filter')['filter_mobile'],
                'Param6' => $this->session->userdata('order_filter')['filter_payment'],
                'Param7' => $this->session->userdata('order_filter')['filter_date_booking_from'],
                'Param8' => $this->session->userdata('order_filter')['filter_date_booking_to'],
                'Param9' => $this->session->userdata('order_filter')['filter_date_session_from'],
                'Param10' => $this->session->userdata('order_filter')['filter_date_session_to'],
                 'Param11' => $this->session->userdata('order_filter')['filter_status'],
                'Param12' => $this->session->userdata('order_filter')['filter_date_printed_from'],
                'Param13' => $this->session->userdata('order_filter')['filter_date_printed_to'],
                'Param14' => $this->session->userdata('order_filter')['filter_date_ticket'],
                'Param15' => $this->session->userdata('order_filter')['filter_date_session'],
                'Param16' => $this->session->userdata('order_filter')['filter_tim'],
                'Param17' => $this->session->userdata('order_filter')['filter_sta'],
                'Param18' => '',
                'Param19' => '');
            foreach($parameter1 as $key=>$val){
                if($parameter1[$key] == '')
                {
                    $parameter1[$key] =-1;
                }
                //echo $parameter1[$key];
            }
           // p($parameter1);
            $response['vieww_order'] = $this->supper_admin->call_procedure('proc_order_filter_s', $parameter1);
            //p($response['vieww_order']);
            $this->session->unset_userdata('order_filter');


        }
        else {
            $parameter1 = array('act_mode' => 'S_vieworder',
                'Param1' => '',
                'Param2' => '',
                'Param3' => '',
                'Param4' => '',
                'Param5' => '',
                'Param6' => '',
                'Param7' => '',
                'Param8' => '',
                'Param9' => '');
           
            $response['vieww_order'] = $this->supper_admin->call_procedure('proc_order_s', $parameter1);
//pend($response['vieww_order']);

        }

        $parameter_time = array('act_mode' => 'branch_list_for_filter',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' =>'',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' =>'',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',
            'Param16' => '',
            'Param17' => '',
            'Param18' => '',
            'Param19' => '');
        $response['vieww_time'] = $this->supper_admin->call_procedure('proc_order_filter_s', $parameter_time);

        $parameter_status = array('act_mode' => 'status_list_for_filter',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' =>'',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' =>'',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',
            'Param16' => '',
            'Param17' => '',
            'Param18' => '',
            'Param19' => '');
        $response['vieww_status'] = $this->supper_admin->call_procedure('proc_order_filter_s',$parameter_status);
//pend($response['vieww_status']);
        $parameter2 = array( 'act_mode'=>'s_viewbranch',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        $response['s_viewbranch'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter2);

       //pend($response);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('order/orderlist',$response);
    }

    /*for change status*/
    public function op_ticket_print_status()
    {
        $parameter1 = array('act_mode' => 'op_ticket_print_status',
            'Param1' => $this->input->post('pacorderid'),
            'Param2' => $this->input->post('op_ticket_print_status'),
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //pend($parameter1);
        $response['s'] = $this->supper_admin->call_procedure('proc_order_s', $parameter1);
        print_r(json_encode((array)$response));
    }

    /*for pending order*/
    public function pendingorder()
    {
if(getMemberId()==1)

{

}
else
{
  redirect('admin/login/dashboard?empid='.$_GET['empid'].'&uid='.$_GET['uid'].'');
}
        $parameter1 = array('act_mode' => 'S_vieworder',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //pend($parameter);
        $response['vieww_order'] = $this->supper_admin->call_procedure('proc_order_s', $parameter1);
        //pend($response);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('order/pendingorderlist',$response);
    }

    public function sucessgorder()
    {
if(getMemberId()==1)

{

}
else
{
  redirect('admin/login/dashboard?empid='.$_GET['empid'].'&uid='.$_GET['uid'].'');
}
        $parameter1 = array('act_mode' => 'S_vieworder',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //pend($parameter);
        $response['vieww_order'] = $this->supper_admin->call_procedure('proc_order_s', $parameter1);
        //pend($response);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('order/sucessgorder',$response);
    }
    public function sucesspendingprinting()
    {
if(getMemberId()==1)

{

}
else
{
  redirect('admin/login/dashboard?empid='.$_GET['empid'].'&uid='.$_GET['uid'].'');
}
        $parameter1 = array('act_mode' => 'S_vieworder',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //pend($parameter);
        $response['vieww_order'] = $this->supper_admin->call_procedure('proc_order_s', $parameter1);
        //pend($response);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('order/sucesspendingprinting',$response);
    }
    public function sucessgordercheck()
    {
        //pend($_POST);
        $parameter1 = array('act_mode' => 'update_tracking',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //pend($parameter);
        $response['vieww_order'] = $this->supper_admin->call_procedure('proc_order_s', $parameter1);
        //pend($response);
    }
    
    public function s_print()
    {
    echo "<pre>";
	print_r($this->session);
//    $this->session->set_userdata('dsadasd','asdasdasd');
//    p($this->session->all_userdata());
        
    }
    

}// end class
?>