<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Managemail extends MX_Controller
{
    public function __construct()
    {
        $this->load->model("supper_admin");
        $this->load->helper('my_helper');
        $this->load->library('session');
 $this->load->helper('adminmenu_helper');
    }


    public function viewmail()
    {
if(getMemberId()==1)

{

}
else
{
  redirect('admin/login/dashboard?empid='.$_GET['empid'].'&uid='.$_GET['uid'].'');
}

        if($this->input->post('submit')) {
            $this->load->helper(array('form', 'url'));
            $this->load->library('form_validation');
            $this->form_validation->set_rules('branchids', 'Branch', 'required');
            $this->form_validation->set_rules('mail_text1', 'mail_text1', 'required');
            $this->form_validation->set_rules('mail_text2', 'mail_text2', 'required');
            $this->form_validation->set_rules('mail_text3', 'mail_text3', 'required');
            $this->form_validation->set_rules('mail_text4', 'mail_text4', 'required');
            $this->form_validation->set_rules('mail_subject', 'mail_subject', 'required');
            $this->form_validation->set_rules('mail_from', 'mail_from', 'required');
            $this->form_validation->set_rules('mail_branch_phone', 'mail_branch_phone', 'required');
            $this->form_validation->set_rules('mail_branch_email', 'mail_branch_email', 'required');
               $this->form_validation->set_rules('mail_branch_apicode', 'mail_branch_apicode', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('message', validation_errors());
            } else {
                $configUpload['upload_path'] = './assets/admin/images';              #the folder placed in the root of project
                $configUpload['allowed_types'] = 'gif|jpg|png|bmp|jpeg';       #allowed types description
                $configUpload['max_size'] = '0';                          #max size
                $configUpload['max_width'] = '0';                          #max width
                $configUpload['max_height'] = '0';                          #max height
                $configUpload['encrypt_name'] = true;                         #encrypt name of the uploaded file
                $this->load->library('upload', $configUpload);                  #init the upload class
                if (!$this->upload->do_upload('mail_image')) {
                    $uploadedDetails = $this->upload->display_errors();
                    $this->session->set_flashdata('message', $uploadedDetails);
                } else {
                    $uploadedDetails = $this->upload->data();
                    $this->session->set_flashdata('message', 'inserted sucessfully');
                    $parameter1 = array('act_mode' => 's_addemail_details',
                        'Param1' => $this->input->post('branchids'),
                        'Param2' => $uploadedDetails['file_name'],
                        'Param3' => $this->input->post('mail_text1'),
                        'Param4' => $this->input->post('mail_text2'),
                        'Param5' => $this->input->post('mail_text3'),
                        'Param6' => $this->input->post('mail_text4'),
                        'Param7' => $this->input->post('mail_subject'),
                        'Param8' => $this->input->post('mail_from'),
                        'Param9' => $this->input->post('mail_branch_phone'),
                        'Param10' => $this->input->post('mail_branch_email'),
                        'Param11' =>$this->input->post('mail_branch_apicode'),
                        'Param12' =>$this->input->post('mail_branch_internethandlingcharge'),
                    );
                    //pend($parameter);
                    $response['vieww'] = $this->supper_admin->call_procedure('proc_mail_s', $parameter1);
                }
            }
        }

        $parameter1 = array('act_mode' => 's_viewmail',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' =>'',
             'Param12' =>'');
        //pend($parameter);
        $response['vieww'] = $this->supper_admin->call_procedure('proc_mail_s', $parameter1);
        $parameter2 = array( 'act_mode'=>'s_viewbranch',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        $response['s_viewbranch'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter2);
        //pend($response['vieww']);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('mailmanage/mailview', $response);
    }

    public  function mailviewupdate($a)
    {

        if($this->input->post('submit')) {

            //pend($_POST);
            $this->load->helper(array('form', 'url'));
            $this->load->library('form_validation');
            $this->form_validation->set_rules('branchids', 'Branch', 'required');
            $this->form_validation->set_rules('mail_text1', 'mail_text1', 'required');
            $this->form_validation->set_rules('mail_text2', 'mail_text2', 'required');
            $this->form_validation->set_rules('mail_text3', 'mail_text3', 'required');
            $this->form_validation->set_rules('mail_text4', 'mail_text4', 'required');
            $this->form_validation->set_rules('mail_subject', 'mail_subject', 'required');
            $this->form_validation->set_rules('mail_from', 'mail_from', 'required');
            $this->form_validation->set_rules('mail_branch_phone', 'mail_branch_phone', 'required');
            $this->form_validation->set_rules('mail_branch_email', 'mail_branch_email', 'required');
             $this->form_validation->set_rules('mail_branch_apicode', 'mail_branch_apicode', 'required');
            if ($this->form_validation->run() == FALSE) {

                $this->session->set_flashdata('message', validation_errors());
            } else {


                if($_FILES['mail_image']['name'] == ''){
                    $parameter1 = array('act_mode' => 's_addemail_details_update',
                        'Param1' => $this->input->post('branchids'),
                        'Param2' => $this->input->post('mail_image_before_update'),
                        'Param3' => $this->input->post('mail_text1'),
                        'Param4' => $this->input->post('mail_text2'),
                        'Param5' => $this->input->post('mail_text3'),
                        'Param6' => $this->input->post('mail_text4'),
                        'Param7' => $this->input->post('mail_subject'),
                        'Param8' => $this->input->post('mail_from'),
                        'Param9' => $this->input->post('mail_branch_phone'),
                        'Param10' => $this->input->post('mail_branch_email'),
                        'Param11' => base64_decode($a),
                        'Param12' => $this->input->post('mail_branch_apicode'),
                    );
                    //pend($parameter);
                    $response = $this->supper_admin->call_procedure('proc_mail_s', $parameter1);
                    $this->session->set_flashdata('message', 'Updated Sucessfully');
                    redirect("admin/managemail/viewmail?empid=".$_GET['empid']."&uid=".str_replace(".html","",$_GET['uid'])."");


                }
                else {

                    $configUpload['upload_path'] = './assets/admin/images';              #the folder placed in the root of project
                    $configUpload['allowed_types'] = 'gif|jpg|png|bmp|jpeg';       #allowed types description
                    $configUpload['max_size'] = '0';                          #max size
                    $configUpload['max_width'] = '0';                          #max width
                    $configUpload['max_height'] = '0';                          #max height
                    $configUpload['encrypt_name'] = true;                         #encrypt name of the uploaded file
                    $this->load->library('upload', $configUpload);                  #init the upload class
                    if (!$this->upload->do_upload('mail_image')) {

                        $uploadedDetails = $this->upload->display_errors();
                        $this->session->set_flashdata('message', $uploadedDetails);

                    } else {

                        $uploadedDetails = $this->upload->data();
                        $this->session->set_flashdata('message', 'inserted sucessfully');
                        $parameter1 = array('act_mode' => 's_addemail_details_update',
                            'Param1' => $this->input->post('branchids'),
                            'Param2' => $uploadedDetails['file_name'],
                            'Param3' => $this->input->post('mail_text1'),
                            'Param4' => $this->input->post('mail_text2'),
                            'Param5' => $this->input->post('mail_text3'),
                            'Param6' => $this->input->post('mail_text4'),
                            'Param7' => $this->input->post('mail_subject'),
                            'Param8' => $this->input->post('mail_from'),
                            'Param9' => $this->input->post('mail_branch_phone'),
                            'Param10' => $this->input->post('mail_branch_email'),
                            'Param11' => base64_decode($a),
                            'Param12' => $this->input->post('mail_branch_internethandlingcharge'),
                        );
                        //pend($parameter);
                        $response = $this->supper_admin->call_procedure('proc_mail_s', $parameter1);
                        $this->session->set_flashdata('message', 'Updated Sucessfully');
                        redirect("admin/managemail/viewmail?empid=".$_GET['empid']."&uid=".str_replace(".html","",$_GET['uid'])."");

                    }
                }
            }
        }

        $parameter2 = array( 'act_mode'=>'s_viewbranch',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        $response['s_viewbranch'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter2);

        $parameter1 = array('act_mode' => 's_viewmail_update',
            'Param1' => base64_decode($a),
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11'=>'',
             'Param12'=>'');
        //pend($parameter1);
        $response['vieww'] = $this->supper_admin->call_procedure('proc_mail_s', $parameter1);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('mailmanage/mailview_update', $response);

    }

    public  function mailviewstatus($a,$b)
    {
        $status =  base64_decode($b)== 1 ? 0 : 1;
        $parameter1 = array('act_mode' => 's_viewmail_status',
            'Param1' => base64_decode($a),
            'Param2' => $status,
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11'=>'',
             'Param12'=>'');
        //pend($parameter1);
        $response['vieww'] = $this->supper_admin->call_procedure('proc_mail_s', $parameter1);
        redirect("admin/managemail/viewmail?empid=".$_GET['empid']."&uid=".str_replace(".html","",$_GET['uid'])."");

    }

    public  function mailviewdelete($a)
    {
        $parameter1 = array('act_mode' => 's_viewmail_delete',
            'Param1' => base64_decode($a),
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11'=>'',
            'Param12'=>'');
        //pend($parameter1);
        $response['vieww'] = $this->supper_admin->call_procedure('proc_mail_s', $parameter1);
        redirect("admin/managemail/viewmail?empid=".$_GET['empid']."&uid=".str_replace(".html","",$_GET['uid'])."");
    }



}// end class
?>