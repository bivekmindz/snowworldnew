<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Managerole extends MX_Controller
{
    public function __construct()
    {
        $this->load->model("supper_admin");
        $this->load->helper('my_helper');
 $this->load->helper('adminmenu_helper');

    }

    public function addrole()
    {

  // pend($this->session->all_userdata());
        $parameter = array('act_mode' => 'viewrole',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        $response['vieww_role'] = $this->supper_admin->call_procedure('proc_rolemanage_s', $parameter);
      //pend($response['vieww_role']);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('role/addrole', $response);
    }
    public function addrole_formsubmit()
    {
        $parameter = array('act_mode' => 'addrole',
            'Param1' => $this->input->post('role_name'),
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        $response['vieww'] = $this->supper_admin->call_procedure('proc_rolemanage_s', $parameter);
        $this->session->set_flashdata('message', 'inserted sucessfully');
        redirect("admin/managerole/addrole?empid=".$_GET['empid']."&uid=".str_replace(".html","",$_GET['uid'])."");

    }

    public function deleterole($a)
    {
        $parameter = array('act_mode' => 'deleterole',
            'Param1' => $a,
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        $response['vieww'] = $this->supper_admin->call_procedure('proc_rolemanage_s', $parameter);
        $this->session->set_flashdata('message', 'deleted sucessfully');

        redirect("admin/managerole/addrole?empid=".$_GET['empid']."&uid=".str_replace(".html","",$_GET['uid'])."");
    }



    public function addemployee()
    {
if(getMemberId()==1)

{

}
else
{
  redirect('admin/login/dashboard?empid='.$_GET['empid'].'&uid='.$_GET['uid'].'');
}


    //p($this->session->all_userdata());
        $parameter = array('act_mode' => 'viewmenu',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        $response['vieww_menu'] = $this->supper_admin->call_procedure('proc_rolemanage_s', $parameter);

        $parameter2 = array( 'act_mode'=>'s_viewbranch',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        $response['s_viewbranch'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter2);

        $parameter = array('act_mode' => 'viewemp',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        $response['vieww_employee'] = $this->supper_admin->call_procedure('proc_rolemanage_s', $parameter);
        //pend( $response['vieww_employee']);
        $parameter = array('act_mode' => 'viewcountry', 'row_id' => '', 'counname' => '', 'coucode' => '', 'commid' => '');
        $response['vieww'] = $this->supper_admin->call_procedure('proc_geographic', $parameter);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('role/addemployee',$response);
    }

    public function addemp()
    {

        $this->form_validation->set_rules('s_username', 's_username', 'required');
        $this->form_validation->set_rules('s_loginpassword', 'password', 'required');
        $this->form_validation->set_rules('contact_no', 'contact', 'required');
        $this->form_validation->set_rules('branchids', 'branch', 'required');
        $this->form_validation->set_rules('s_loginemail','Email already exists','required|valid_email');
        $this->db->select('emp_email');
        $this->db->from('tbl_employee');
        $this->db->where('emp_email', $this->input->post('s_loginemail'));
        $this->db->where('emp_status', '1');
        $query = $this->db->get();
        $num = $query->num_rows();

        if (($this->form_validation->run() != FALSE) && ($num == 0)) {
            //p($_POST);
            $parameter = array('act_mode' => 'addemp',
                'Param1' => $this->input->post('s_username'),
                'Param2' => '',
                'Param3' => $this->input->post('s_loginemail'),
                'Param4' => md5($this->input->post('s_loginpassword')),
                'Param5' => $this->input->post('contact_no'),
                'Param6' => $this->input->post('branchids'),
                'Param7' => '',
                'Param8' => '',
                'Param9' => ''
            );
            $response['vieww'] = (array)$this->supper_admin->call_procedure('proc_rolemanage_s', $parameter);
//p($response['vieww']['0']->dd);
            foreach ($this->input->post('mainmenu_sub') as $a) {

                $parameter = array('act_mode' => 'addemp_role',
                    'Param1' => $a,
                    'Param2' => $response['vieww']['0']->dd,
                    'Param3' => '',
                    'Param4' => '',
                    'Param5' => '',
                    'Param6' => '',
                    'Param7' => '',
                    'Param8' => '',
                    'Param9' => '');
                $response['roleee'] = (array)$this->supper_admin->call_procedure('proc_rolemanage_s', $parameter);
                //p($parameter);
//p($response['roleee']);
            }
            //die();
 $from_email = "customerservice@skiindiadelhi.com";
        $mess = "Password :".$this->input->post('s_loginpassword')."<br>Use this to login in <a href='https://www.bookingprocessing.com/admin'>https://www.bookingprocessing.com/admin<a>";
        $to_email = $this->input->post('s_loginemail');
        //Load email library
        $this->load->library('email');
        $this->email->from($from_email);
        $this->email->reply_to($from_email);
        $this->email->to($to_email);
        $this->email->subject('Login credentials');
        $this->email->message($mess);
        //Send mail
        $this->email->send();
        //$this->email->print_debugger();
        //pend($this->email->print_debugger());
        }
       // $this->session->set_flashdata('message',validation_errors());
        redirect("admin/managerole/addemployee?empid=".$_GET['empid']."&uid=".str_replace(".html","",$_GET['uid'])."");



    }

    public function upd_emp()
    {
       //pend($_POST);

        $this->form_validation->set_rules('s_username', 's_username', 'required');
        $this->form_validation->set_rules('s_loginemail', 'Email', 'required');
         $this->form_validation->set_rules('contact_no', 'contact', 'required');
        $this->form_validation->set_rules('branchids', 'branch', 'required');
if($_POST['s_loginpassword']!='')
{
    $pass= md5($_POST['s_loginpassword']);
}
else {
    $pass= $_POST['s_loginpasswordnew'];

}


        if ($this->form_validation->run() != FALSE) {
            $parameter = array('act_mode' => 'upd_emp',
                'Param1' => $this->input->post('s_username'),
                'Param2' => '',
                'Param3' => $this->input->post('s_loginemail'),
                'Param4' => ($pass),
                'Param5' => $this->input->post('contact_no'),
                'Param6' => $this->input->post('branchids'),
                'Param7' => $this->input->post('empoid'),
                'Param8' => '',
                'Param9' => ''
            );
            $response['vieww'] = (array)$this->supper_admin->call_procedure('proc_rolemanage_s', $parameter);
//p($response['vieww']['0']->dd);


            $parameter = array('act_mode' => 'addasignrole_DELETE',
                'Param1' => 100,
                'Param2' => $this->input->post('empoid'),
                'Param3' => '',
                'Param4' => '',
                'Param5' => '',
                'Param6' => '',
                'Param7' => '',
                'Param8' => '',
                'Param9' => '');
            $response = $this->supper_admin->call_procedure('proc_rolemanage_s', $parameter);


            foreach ($this->input->post('mainmenu_sub') as $a) {

                $parameter = array('act_mode' => 'addemp_role',
                    'Param1' => $a,
                    'Param2' => $this->input->post('empoid'),
                    'Param3' => '',
                    'Param4' => '',
                    'Param5' => '',
                    'Param6' => '',
                    'Param7' => '',
                    'Param8' => '',
                    'Param9' => '');
                $response['roleee'] = (array)$this->supper_admin->call_procedure('proc_rolemanage_s', $parameter);
                //p($parameter);
//p($response['roleee']);
            }
 $from_email = "customerservice@skiindiadelhi.com";
        $mess = "Your profile is changed,Your new Password :".$this->input->post('s_loginpassword')."<br>Use this to login in <a href='https://www.bookingprocessing.com/admin'>https://www.bookingprocessing.com/admin<a>";
        $to_email = $this->input->post('s_loginemail');
        //Load email library
        $this->load->library('email');
        $this->email->from($from_email);
        $this->email->reply_to($from_email);
        $this->email->to($to_email);
        $this->email->subject('Login credentials');
        $this->email->message($mess);
        //Send mail
     //   $this->email->send();
        //$this->email->print_debugger();
        //pend($this->email->print_debugger());
        }


        //die();
        redirect("admin/managerole/addemployee?empid=".$_GET['empid']."&uid=".str_replace(".html","",$_GET['uid'])."");



    }

    public function delemp()
    {

        $parameter = array('act_mode' => 'delemp',
            'Param1' => $this->uri->segment('4'),
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //pend($parameter);
        $response['vieww_employee'] = $this->supper_admin->call_procedure('proc_rolemanage_s', $parameter);
        $this->session->set_flashdata('message','deleted sucessfully');
        redirect("admin/managerole/addemployee?empid=".$_GET['empid']."&uid=".str_replace(".html","",$_GET['uid'])."");

    }

    public function assignroles()
    {
        $parameter = array('act_mode' => 'viewrole',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        $response['vieww_role'] = $this->supper_admin->call_procedure('proc_rolemanage_s', $parameter);

        $parameter = array('act_mode' => 'viewemp',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        $response['vieww_employee'] = $this->supper_admin->call_procedure('proc_rolemanage_s', $parameter);

        $parameter = array('act_mode' => 'viewmenu',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        $response['vieww_menu'] = $this->supper_admin->call_procedure('proc_rolemanage_s', $parameter);
        //pend($response);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('role/role',$response);
    }


    public function getsubmenu()
    {
        $a = implode(',',$this->input->post('mainmenuid'));
        $parameter = array('act_mode' => 'getsubmenu',
            'Param1' => $a,
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');

        $response['vieww_submenu'] = $this->supper_admin->call_procedure('proc_rolemanage_s', $parameter);
        print_r(json_encode($response));

    }

    public function addasignrole()
    {
        foreach ($this->input->post('submenu') as $a) {
                $parameter1 = array('act_mode' => 'getmainmenu',
                    'Param1' => $a,
                    'Param2' => '',
                    'Param3' => '',
                    'Param4' => '',
                    'Param5' => '',
                    'Param6' => '',
                    'Param7' => '',
                    'Param8' => '',
                    'Param9' => '');
                $response['vieww_mainmenu'] = $this->supper_admin->call_procedure('proc_rolemanage_s', $parameter1);
                $parameter = array('act_mode' => 'insertadminrole',
                    'Param1' => $this->input->post('roleid'),
                    'Param2' => $this->input->post('empid'),
                    'Param3' => $response['vieww_mainmenu'][0]->pid,
                    'Param4' => $a,
                    'Param5' => '',
                    'Param6' => '',
                    'Param7' => '',
                    'Param8' => '',
                    'Param9' => '');
            $response = $this->supper_admin->call_procedure('proc_rolemanage_s', $parameter);

    }

    }


    public function get_menu_submenu()
    {
        $parameter = array('act_mode' => 'get_menu_submenu',
            'Param1' => $this->input->post('roleid'),
            'Param2' => $this->input->post('empid'),
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        //p($parameter);
        $response = $this->supper_admin->call_procedure('proc_rolemanage_s', $parameter);
        print_r(json_encode($response));
    }


    public function addasignrole_DELETE()
    {
            $parameter = array('act_mode' => 'addasignrole_DELETE',
                'Param1' => 100,
                'Param2' => $this->input->post('empid'),
                'Param3' => '',
                'Param4' => '',
                'Param5' => '',
                'Param6' => '',
                'Param7' => '',
                'Param8' => '',
                'Param9' => '');
            $response = $this->supper_admin->call_procedure('proc_rolemanage_s', $parameter);
    }

    public function editempp()
    {
        $parameter = array('act_mode' => 'viewmenu',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        $response['vieww_menu'] = $this->supper_admin->call_procedure('proc_rolemanage_s', $parameter);

        $parameter2 = array( 'act_mode'=>'s_viewbranch',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        $response['s_viewbranch'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter2);

        $parameter = array('act_mode' => 'viewemp_update',
            'Param1' => base64_decode($this->uri->segment(4)),
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '');
        $response['vieww_employee'] = (array)$this->supper_admin->call_procedure('proc_rolemanage_s', $parameter)['0'];
        //pend( $response['vieww_employee']);
        $parameter = array('act_mode' => 'viewcountry', 'row_id' => '', 'counname' => '', 'coucode' => '', 'commid' => '');
        $response['vieww'] = $this->supper_admin->call_procedure('proc_geographic', $parameter);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('role/updemployee',$response);
    }
}// end class
?>