<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Groupbooking extends MX_Controller
{
    public function __construct()
    {
        $this->load->model("supper_admin");
        $this->load->helper('my_helper');
 $this->load->library('session');
 $this->load->helper('adminmenu_helper');
    }

    public function bookingtype()
    {
if(getMemberId()==1)

{

}
else
{
  redirect('admin/login/dashboard?empid='.$_GET['empid'].'&uid='.$_GET['uid'].'');
}
        if ($this->input->post('submit')) {
            $this->form_validation->set_rules('branchids', 'branchids', 'required');
            $this->form_validation->set_rules('book_name', 'book_name', 'required');
            if ($this->form_validation->run() != FALSE) {
                $parameter11 = array('act_mode' => 'addbookingtype_admin',
                    'countryid' => $this->input->post('branchids'),
                    'stateid' => $this->input->post('book_name'),
                    'cityid' => '',
                    'branch_id' => '',
                    'locationid' => '');
                $response = $this->supper_admin->call_procedure('proc_bookingtype_v', $parameter11);
                $this->session->set_flashdata('message', 'inserted sucessfully');
            }
            else{
                $this->session->set_flashdata('message', 'not inserted sucessfully');

            }
        }

        $parameter2 = array( 'act_mode'=>'s_viewbranch',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'');
        $response['s_viewbranch'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter2);

        $parameter1 = array('act_mode' => 'viewbookingtype_admin',
            'countryid' => '',
            'stateid' => '',
            'cityid' => '',
            'branch_id' => '',
            'locationid' => '');
        //pend($parameter1);
        $response['vieww_bookingtype'] = $this->supper_admin->call_procedure('proc_bookingtype_v', $parameter1);
      //pend($response);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('groupbooking/bookingtype', $response);
    }

    public function bookingtype_delete($a)
    {
        $parameter1 = array('act_mode' => 'deletebookingtype_admin',
            'countryid' => $a,
            'stateid' => '',
            'cityid' => '',
            'branch_id' => '',
            'locationid' => '');
        //pend($parameter1);
        $response = $this->supper_admin->call_procedure('proc_bookingtype_v', $parameter1);
        redirect("admin/groupbooking/bookingtype");

    }

}// end class
?>