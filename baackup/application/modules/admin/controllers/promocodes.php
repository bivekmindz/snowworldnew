<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Promocodes extends MX_Controller
{
    public function __construct()
    {
        $this->load->model("supper_admin");
        $this->load->helper('my_helper');
 $this->load->library('session');
 $this->load->helper('adminmenu_helper');
    }


    /*add promo code by zzz*/
       public function addpromo()
    {

        if($_POST['addones']!='') {
            $addonedata=implode(',', $_POST['addones']);
            $addoneqty=1;
            $promo=$this->input->post('promo');
            $promonumber=$this->input->post('promonumber');
        }
        else
        {
            $addonedata="";
            $addoneqty="";
            $promo=$this->input->post('promo');
            $promonumber=$this->input->post('promonumber');
        }

       
        $this->form_validation->set_rules('branchids', 'branchids', 'required');
        $this->form_validation->set_rules('promocode_name', 'promocode_name', 'required');

        if ($this->form_validation->run() == FALSE) {
          redirect('admin/promocodes/s_viewpromo');
        } else {
            foreach ($this->input->post('branchids') as $a) {

            $parameter = array('act_mode' => 'insert_promo',
                'Param1' => $a,
                'Param2' => $this->input->post('promocode_name'),
                'Param3' => $promo,
                'Param4' => $promonumber,
                'Param5' => $this->input->post('promocode_allowed_user'),
                'Param6' => $this->input->post('promocode_allowed_times'),
                'Param7' => $this->input->post('promocode_start'),
                'Param8' => $this->input->post('promocode_end'),
                'Param9' =>$this->input->post('user_type'),
                'Param10' =>$addonedata,
 'Param11' =>$addoneqty,
 'Param12' =>'');
             $response['vieww'] = $this->supper_admin->call_procedure('proc_promocode_s', $parameter);
        }


            redirect("admin/promocodes/s_viewpromo?empid=".$_GET['empid']."&uid=".str_replace(".html","",$_GET['uid'])."");


        }

    }

    /*promocode form add*/
    public function s_promo()
    {

if(getMemberId()==1)

{

}
else
{
  redirect('admin/login/dashboard?empid='.$_GET['empid'].'&uid='.$_GET['uid'].'');
}
        $parameter2 = array( 'act_mode'=>'s_viewbranch',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'',

        );
        $response['s_viewbranch'] = $this->supper_admin->call_procedure('proc_packages_s',$parameter2);



        $parameter2 = array( 'act_mode'=>'s_viewaddon_admin',
            'Param1'=>'',
            'Param2'=>'',
            'Param3'=>'',
            'Param4'=>'',
            'Param5'=>'',
            'Param6'=>'',
            'Param7'=>'',
            'Param8'=>'',
            'Param9'=>'',
           );
        $response['s_viewaddone'] = $this->supper_admin->call_procedure('proc_addon_s',$parameter2);
//p($response['s_viewaddone']);



        $parameter = array('act_mode' => 'viewcountry', 'row_id' => '', 'counname' => '', 'coucode' => '', 'commid' => '');
        $response['vieww'] = $this->supper_admin->call_procedure('proc_geographic', $parameter);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('promocode/s_promo', $response);
    }


    /*view promocode by zzz*/
    public function s_viewpromo()
    {
if(getMemberId()==1)

{

}
else
{
  redirect('admin/login/dashboard?empid='.$_GET['empid'].'&uid='.$_GET['uid'].'');
}
  

        $parameter = array('act_mode' => 'view_promo',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' =>'',
            'Param11' =>'',
            'Param12' =>'');
        //pend($parameter);
        $response['vieww'] = $this->supper_admin->call_procedure('proc_promocode_s', $parameter);
        $this->load->view('helper/header');
        $this->load->view('helper/nav');
        $this->load->view('promocode/s_viewpromo', $response);
    }

    /*delete the promocode*/
    public function promo_delete()
    {
        $parameter = array('act_mode' => 'delete_promo',
            'Param1' => $this->uri->segment('4'),
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' =>'',
            'Param11' =>'',
            'Param12' =>'');
        //pend($parameter);
        $response['vieww'] = $this->supper_admin->call_procedure('proc_promocode_s', $parameter);

        redirect("admin/promocodes/s_viewpromo?empid=".$_GET['empid']."&uid=".str_replace(".html","",$_GET['uid'])."");


    }


    public function promo_status($a,$b)
    {
        $status =  base64_decode($b)== 1 ? 0 : 1;
        $parameter1 = array('act_mode' => 'status_promo',
            'Param1' => base64_decode($a),
            'Param2' => $status,
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' =>'',
            'Param11' =>'',
            'Param12' =>'');
        //pend($parameter1);
        $response['vieww'] = $this->supper_admin->call_procedure('proc_promocode_s', $parameter1);

        redirect("admin/promocodes/s_viewpromo?empid=".$_GET['empid']."&uid=".str_replace(".html","",$_GET['uid'])."");

    }
}// end class
?>