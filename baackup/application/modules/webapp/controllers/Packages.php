<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Packages extends MX_Controller {

    public function __construct() {
        $this->load->model("supper_admin");
        $this->load->library('session');

    }


//Select Package


    public function packages(){

        $this->load->library('session');
        if($this->session->userdata('skiindiamember')->id){
            redirect(base_url());
        }
        else
        { 

             $this->session->unset_userdata('ppromo_id');
            $this->session->unset_userdata('ppcodename');
            $this->session->unset_userdata('pp_type');
            $this->session->unset_userdata('pp_addonedata');
            $this->session->unset_userdata('pp_locationid');
            $this->session->unset_userdata('pp_branchid');
            $this->session->unset_userdata('pp_discount');
            $this->session->unset_userdata('pp_status');
            $this->session->unset_userdata('pp_expiry_date');
            $this->session->unset_userdata('pp_allowed_per_user');



            $this->session->unset_userdata('pp_start_date');
            $this->session->unset_userdata('pp_allowed_times');
            $this->session->unset_userdata('pp_createdon');
            $this->session->unset_userdata('pp_modifiedon');
            $this->session->unset_userdata('pp_logintype');
            $this->session->unset_userdata('pp_addoneqty');


            $this->session->unset_userdata('packageval');
            $this->session->unset_userdata('packageimageval');
            $this->session->unset_userdata('packagenameval');
            $this->session->unset_userdata('packageqtyval');
            $this->session->unset_userdata('packagepriceval');
            $this->session->unset_userdata('packagedescription');
            $this->session->unset_userdata('cartpackageimage');
            $this->session->unset_userdata('cartpackagename');
            $this->session->unset_userdata('cartpackageprice');
            $this->session->unset_userdata('cartsubtotal');

            $this->session->unset_userdata('cartaddoneid');
            $this->session->unset_userdata('cartaddoneimage');
            $this->session->unset_userdata('cartaddonname');
            $this->session->unset_userdata('cartaddonqty');
            $this->session->unset_userdata('cartaddonprice');
            $this->session->unset_userdata('orderlastinsertid');

            $this->session->unset_userdata('Promocodeprice');


            $this->session->unset_userdata('billing_name');
            $this->session->unset_userdata('billing_email');
            $this->session->unset_userdata('billing_tel');
            $this->session->unset_userdata('billing_address');
            $this->session->unset_userdata('billing_city');
            $this->session->unset_userdata('billing_state');

            $this->session->unset_userdata('billing_zip');
            $this->session->unset_userdata('billing_country');

            $siteurl= base_url();
            $parameterbranch=array(
                'act_mode' =>'selectbranch',
                'weburl' =>$siteurl,
                'type'=>'web',

            );

            $path=api_url().'selectsiteurl/branch/format/json/';
            $data['branch']=curlpost($parameterbranch,$path);

            $data = array(


                'countryid' => $data['branch']->countryid,
                'stateid' => $data['branch']->stateid,
                'cityid' => $data['branch']->cityid,
                'branch_id' => $data['branch']->branch_id,
                'locationid' => $data['branch']->branch_location
            );
            $this->session->set_userdata($data);
$siteurl= base_url();
            $parameterbranch=array(
                'act_mode' =>'selectbranch',
                'weburl' =>$siteurl,
                'type'=>'web',

            );

            $path=api_url().'selectsiteurl/branch/format/json/';
            $data['branch']=curlpost($parameterbranch,$path);


//select banner images
            $parameterbanner=array(
                'act_mode' =>'selectbannerimages',
                'branchid' =>$data['branch']->branch_id,
                'type'=>'web',

            );

            $path=api_url().'selectsiteurl/banner/format/json/';
            $data['banner']=curlpost($parameterbanner,$path);


//Select Time
            $parametertimeslot=array(
                'act_mode' =>'selecttimeslot',
                'branchid' =>$data['branch']->branch_id,

                'destinationType' =>$this->session->userdata('destinationType'),

                'type'=>'web',

            );

            $path=api_url().'selecttimesloturl/timeslot/format/json/';
            $data['timeslot']=curlpost($parametertimeslot,$path);
//Select Time slot
            $parametertimeslot=array(
                'act_mode' =>'selecttimeslot',
                'branchid' =>$data['branch']->branch_id,
                'destinationType' =>$this->session->userdata('destinationType'),
                'type'=>'web',

            );

            $path12=api_url().'selecttimesloturl/timeslotses/format/json/';
            $data['timeslotses']=curlpost($parametertimeslot,$path12);
          

//Search PACKAGES
            if($_POST['ddAdult']!='')
            {

                $date=explode("/",$this->input->post('txtDepartDate'));

                $dateval =preg_replace('/\s+/', '', $date[1]."/".$date[0]."/".$date[2]);
                $parameter2 = array( 'act_mode'=>'s_viewtimeslotf',
                    'nextDate'=>$dateval,
                    'starttime'=>$this->input->post('destinationType'),
                    'branchid'=>$data['branch']->branch_id,
                    'dailyinventory_to' =>'',
                    'dailyinventory_seats' =>'',
                    'dailyinventory_status' =>'',
                    'dailyinventory_createdon' =>'',
                    'dailyinventory_modifiedon' =>'',
                    'dailyinventory_minfrom' =>'',
                    'dailyinventory_minto' =>'',
                );

                $data['s_viewtimeslot'] = $this->supper_admin->call_procedurerow('proc_crone_v',$parameter2);




//echo ($data['s_viewtimeslot']->coun+$this->input->post('ddAdult'))."-----------".$data['s_viewtimeslot']->timeslot_seats;


                if(($data['s_viewtimeslot']->coun+$this->input->post('ddAdult'))<=$data['s_viewtimeslot']->dailyinventory_seats)
                {


                    $dateval=explode("/",$_POST['txtDepartDate']);

                    $_POST['txtDepartDate']=$dateval[1]."/".$dateval[0]."/".$dateval[2]."";

                    $data = array(
                        'uniqid' => uniqid(),
                        'txtDepartDate' => $_POST['txtDepartDate'],
                        'destinationType' => $_POST['destinationType'],
                        'ddAdult' => $_POST['ddAdult']
                    );
                    $this->session->set_userdata($data);



                    redirect(base_url()."packagesstep");
                }
                else{
                    if(($data['s_viewtimeslot']->dailyinventory_seats-$data['s_viewtimeslot']->coun)<=0)
                    {
                        $data['emsg']=" No Seats Avalilable";

                    }
                    else
                    {
                        $data['emsg']=" No Seats Avalilable";
                        //$data['emsg']=($data['s_viewtimeslot']->dailyinventory_seats-$data['s_viewtimeslot']->coun)." Seats Avalilable";
                    }     }




//header("location:".base_url()."packages?searchid=MQ==");
            }


            //$this->session->userdata('skiindia')
            $this->load->view("helper/header");
            $this->load->view("helper/topbar",$data);
            $this->load->view("packages",$data);
            $this->load->view("helper/footerpac");


        }
    }


}//end of class
?>