<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Webapp extends MX_Controller {

   public function __construct() {
      $this->load->model("supper_admin");
   $this->load->library('session'); 
  }

   public function errorfnf()
    {
        $this->load->view("helper/header");
        $this->load->view("errorfnf");
        $this->load->view("helper/footer");
    }
    public function installa()
    {
       
header("location:install/index.php");
    }
  public function paccoupon()
    {

        $siteurl= base_url();
        $parameterbranch=array(
            'act_mode' =>'selectbranch',
            'weburl' =>$siteurl,
            'type'=>'web',

        );

        $path=api_url().'selectsiteurl/branch/format/json/';
        $data['branch']=curlpost($parameterbranch,$path);


        $parameter         = array('act_mode'=>'promoselect_package',
            'promovalue'=>$_GET['couponid'],
            'locationid'=>'',
            'branch_id'=>$data['branch']->branch_id,
            'userid'=>'',
        );
        $path = api_url()."Cart/getpromoselect_package/format/json/";
        $data['promocode']= curlpost($parameter,$path);



        $data = array(
            'sespromo_id' => $data['promocode']->promo_id,
            'sescodename' => $data['promocode']->p_codename,
          'sesbranchid' => $data['promocode']->p_branchid,


        );
        $this->session->set_userdata($data);
        //pend($data );
redirect(base_url());
    }

  //Index page
public function landing(){

if($this->session->userdata('skiindiamember')->id){
    redirect(base_url()."home");
  }
  else{
     
 $this->load->model("supper_admin");



$this->session->unset_userdata('txtDepartDate');
$this->session->unset_userdata('destinationType');
$this->session->unset_userdata('ddAdult');
    $parameterseo=array('act_mode'=>'viewseotags','rowid'=>1);
     $data['seotags']=$this->supper_admin->call_procedureRow('proc_siteconfig',$parameterseo);
    
     $siteurl= base_url();
$parameterbranch=array(
          'act_mode' =>'selectbranch',
          'weburl' =>$siteurl,
        'type'=>'web',
         
          );

       $path=api_url().'selectsiteurl/branch/format/json/'; 
   $data['branch']=curlpost($parameterbranch,$path);    

 $data = array(
     
  
    'countryid' => $data['branch']->countryid,
    'stateid' => $data['branch']->stateid,
    'cityid' => $data['branch']->cityid,
     'branch_id' => $data['branch']->branch_id,
    'locationid' => $data['branch']->branch_location
);
$this->session->set_userdata($data);  
header("location:packages");
    $this->load->view("helper/header");
 $this->load->view("landingpage",$data);
     $this->load->view("helper/footer");
  }
}
   public function paclanding()
    {
       
        $parameterbookingid=array(
            'act_mode' =>'selectbooking',
            'subtotal' =>'',
            'discountamount' =>'',
            'total' =>'',
            'countryid' =>'',
            'stateid' =>'',
            'cityid' =>'',
            'branch_id' =>'',
            'locationid' =>'',
            'userid' =>'',
            'txtDepartDate1' =>'',
            'txtDepartdata' =>'',
            'paymentmode' =>'',
           'ticketid' =>$_GET['bookingid'],
            'packproductname' =>'',
            'packimg' =>'',
            'packpkg' =>'',
            'packprice' =>'',
            'internethandlingcharges' =>'',
             'txtfrommin' =>'',
                'txttohrs' =>'',
                'txttomin' =>'',
                'txtfromd' =>'',
                'txttod' =>'',
                'p_codename' =>'',
                'p_usertype' =>'',
            'type'=>'web',

        );

        $path=api_url().'selectsiteurl/bookingid/format/json/';
        $data['bookingid']=curlpost($parameterbookingid,$path);
      

     
        if(($data['bookingid']->ordersucesmail)==1) {
            header("location:orderaborted");
            exit;
        } else {
    $parameteraddones = array(
        'act_mode' => 'selectaddones',
        'addonnameses' => '',
        'addonquantityses' => '',
        'addonidses' => '',
        'lastidses' => $data['bookingid']->pacorderid,
        'addonpriceses' => '',

        'type' => 'web',

    );

    $path = api_url() . 'selectsiteurl/addones/format/json/';
    $data['addones'] = curlpost($parameteraddones, $path);
// Order mail
    $orderpaymentupdate = array('act_mode'=>'orderpaymentupdate',
            'orderid'=>$this->session->userdata['orderlastinsertid'],
            'tracking_id'=>$tracking_id,
            'order_status'=>$order_status,
            'status_message'=>$status_message,
            'paymentmode'=>"$paymentmode",
              'paymentstatus'=>1,
              'ordersucesmail'=>0,
               'ordermailstatus'=>0,
              
            'type'=>'web',

        );

        $path = api_url()."Ordersucess/selectorderdata/format/json/";
        $data['orderdisplaydata']= curlpost($orderpaymentupdate,$path);



    foreach ($data['addones'] as $key => $value) {

        $orderaddoneid .= $value['orderaddoneid'] . ',';

        $addon_image .= $value['addon_image'] . ',';
        $addonename .= $value['addonename'] . ',';
        $addonevisiter .= $value['addonevisiter'] . ',';
        $addoneprice .= $value['addoneprice'] . ',';
       $ccartpackageimage=implode(',',$data['ccartpackageimage']);
    }
    $cartaddoneid = rtrim($orderaddoneid, ",");
    $cartaddoneimage = rtrim($addon_image, ",");
    $cartaddonname = rtrim($addonename, ",");
    $cartaddonqty = rtrim($addonevisiter, ",");
    $cartaddonprice = rtrim($addoneprice, ",");
            $ccartpackageimage = rtrim($ccartpackageimage, ",");

    $data = array(


        'countryid' => $data['bookingid']->countryid,
        'stateid' => $data['bookingid']->stateid,
        'cityid' => $data['bookingid']->cityid,
        'branch_id' => $data['bookingid']->branch_id,
        'locationid' => $data['bookingid']->locationid,


        'uniqid' => $data['bookingid']->ticketid,
        'txtDepartDate' => $data['bookingid']->departuredate,
        'destinationType' => $data['bookingid']->departuretime,
        'ddAdult' => $data['bookingid']->packpkg,
        'packageval' => $data['bookingid']->packprice,
        'packageimageval' => $data['bookingid']->packimg,
        'packagenameval' => $data['bookingid']->packproductname,
        'packageqtyval' => $data['bookingid']->packpkg,
        'packagepriceval' => $data['bookingid']->packprice,
        'cartpackageimage' => $data['bookingid']->packimg,
        'cartpackagename' => $data['bookingid']->packproductname,
        'cartpackageprice' => $data['bookingid']->packprice,
        'cartsubtotal' => $data['bookingid']->subtotal,
        'orderlastinsertid' => $data['bookingid']->pacorderid,
         'title' => $data['bookingid']->title,
        'billing_name' => $data['bookingid']->billing_name,
        'billing_email' => $data['bookingid']->billing_email,
        'billing_tel' => $data['bookingid']->billing_tel,
        'billing_address' => $data['bookingid']->billing_address,
        'billing_city' => $data['bookingid']->billing_city,
        'billing_state' => $data['bookingid']->billing_state,
        'billing_zip' => $data['bookingid']->billing_zip,
        'billing_country' => $data['bookingid']->billing_country,
        'cartaddoneid' => $cartaddoneid,
        'cartaddoneimage' =>  $cartaddoneimage ,
        'cartaddonname' =>  $cartaddonname,
        'cartaddonqty' =>  $cartaddonqty,
        'cartaddonprice' => $cartaddonprice,
    );
    $this->session->set_userdata($data);



 header("location:payment");

}



    }
    public function orderaborted()
    {




// Ticket Type
        $orderdisplay = array('act_mode'=>'select_order',
            'orderid'=>$this->session->userdata['orderlastinsertid'],

            'type'=>'web',

        );
        $path = api_url()."Ordersucess/selectorder/format/json/";
        $data['orderdisplaydata']= curlpost($orderdisplay,$path);



// Package Type
        $orderPackage = array('act_mode'=>'select_package',
            'orderid'=>$this->session->userdata['orderlastinsertid'],
            'type'=>'web',

        );
        $path1 = api_url()."Ordersucess/selectpackage/format/json/";
        $data['orderpackagedisplaydata']= curlpost($orderPackage,$path1);


        $arr = (array)$data['orderdisplaydata'];

// Addone Type
        $orderaddone = array('act_mode'=>'select_addone',
            'orderid'=>$this->session->userdata['orderlastinsertid'],
            'type'=>'web',

        );
        $path2 = api_url()."Ordersucess/selectaddone/format/json/";
        $data['orderaddonedisplaydata']= curlpost($orderaddone,$path2);

// User Display
        $userdisplay = array('act_mode'=>'select_customer',
            'userid'=>$arr['userid'],
            'type'=>'web',

        );
        $path3 = api_url()."Ordersucess/selectuser/format/json/";
        $data['userdisplaydisplaydata']= curlpost($userdisplay,$path3);



//Select branch
        $siteurl= base_url();
        $parameterbranch=array(
            'act_mode' =>'selectbranch',
            'weburl' =>$siteurl,
            'type'=>'web',

        );

        $path=api_url().'selectsiteurl/branch/format/json/';
        $data['branch']=curlpost($parameterbranch,$path);



//select banner images
        $parameterbanner=array(
            'act_mode' =>'selectbannerimages',
            'branchid' =>$data['branch']->branch_id,
            'type'=>'web',

        );

        $path=api_url().'selectsiteurl/banner/format/json/';
        $data['banner']=curlpost($parameterbanner,$path);


        $orderdisplay = array('act_mode'=>'select_order',
            'orderid'=>$this->session->userdata('orderlastinsertid'),

            'type'=>'web',

        );
        $path = api_url()."Ordersucess/selectorder/format/json/";
        $data['orderdisplaydataval']= curlpost($orderdisplay,$path);

        $arr = (array)$data['orderdisplaydataval'];
// User Display
        $userdisplay = array('act_mode'=>'select_customer',
            'userid'=>$arr['userid'],
            'type'=>'web',

        );
        $path3 = api_url()."Ordersucess/selectuser/format/json/";
        $data['userdisplaydisplaydata']= curlpost($userdisplay,$path3);




        $this->load->view("helper/header");
        $this->load->view("orderaborted",$data);
        $this->load->view("helper/footer");
    }



}//end of class
?>