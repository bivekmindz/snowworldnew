<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Partnerbooking extends MX_Controller {

    public function __construct() {
        $this->load->model("supper_admin");
        $this->load->library('session');
    }
    //addones page
    public function partnerbookingsearch(){

        $this->session->unset_userdata('packageval');
        $this->session->unset_userdata('packageimageval');
        $this->session->unset_userdata('packagenameval');
        $this->session->unset_userdata('packageqtyval');
        $this->session->unset_userdata('packagepriceval');
        $this->session->unset_userdata('packagedescription');
        $this->session->unset_userdata('cartpackageimage');
        $this->session->unset_userdata('cartpackagename');
        $this->session->unset_userdata('cartpackageprice');
        $this->session->unset_userdata('cartsubtotal');

        $this->session->unset_userdata('cartaddoneid');
        $this->session->unset_userdata('cartaddoneimage');
        $this->session->unset_userdata('cartaddonname');
        $this->session->unset_userdata('cartaddonqty');
        $this->session->unset_userdata('cartaddonprice');
        $this->session->unset_userdata('orderlastinsertid');

        $this->session->unset_userdata('Promocodeprice');


        $this->session->unset_userdata('billing_name');
        $this->session->unset_userdata('billing_email');
        $this->session->unset_userdata('billing_tel');
        $this->session->unset_userdata('billing_address');
        $this->session->unset_userdata('billing_city');
        $this->session->unset_userdata('billing_state');

        $this->session->unset_userdata('billing_zip');
        $this->session->unset_userdata('billing_country');

        $siteurl= base_url();
        $parameterbranch=array(
            'act_mode' =>'selectbranch',
            'weburl' =>$siteurl,
            'type'=>'web',

        );

        $path=api_url().'selectsiteurl/branch/format/json/';
        $data['branch']=curlpost($parameterbranch,$path);

        $data = array(


            'countryid' => $data['branch']->countryid,
            'stateid' => $data['branch']->stateid,
            'cityid' => $data['branch']->cityid,
            'branch_id' => $data['branch']->branch_id,
            'locationid' => $data['branch']->branch_location
        );
        $this->session->set_userdata($data);
        $siteurl= base_url();
        $parameterbranch=array(
            'act_mode' =>'selectbranch',
            'weburl' =>$siteurl,
            'type'=>'web',

        );

        $path=api_url().'selectsiteurl/branch/format/json/';
        $data['branch']=curlpost($parameterbranch,$path);


//select banner images
        $parameterbanner=array(
            'act_mode' =>'selectbannerimages',
            'branchid' =>$data['branch']->branch_id,
            'type'=>'web',

        );

        $path=api_url().'selectsiteurl/banner/format/json/';
        $data['banner']=curlpost($parameterbanner,$path);


//Select Time
        $parametertimeslot=array(
            'act_mode' =>'selecttimeslot',
            'branchid' =>$data['branch']->branch_id,

            'destinationType' =>$this->session->userdata('destinationType'),

            'type'=>'web',

        );

        $path=api_url().'selecttimesloturl/timeslot/format/json/';
        $data['timeslot']=curlpost($parametertimeslot,$path);
//Select Time slot
        $parametertimeslot=array(
            'act_mode' =>'selecttimeslot',
            'branchid' =>$data['branch']->branch_id,
            'destinationType' =>$this->session->userdata('destinationType'),
            'type'=>'web',

        );

        $path12=api_url().'selecttimesloturl/timeslotses/format/json/';
        $data['timeslotses']=curlpost($parametertimeslot,$path12);


        //Search PACKAGES
        if($_POST['ddAdult']!='')
        {

            $date=explode("/",$this->input->post('txtDepartDate'));

            $dateval =preg_replace('/\s+/', '', $date[1]."/".$date[0]."/".$date[2]);
            $parameter2 = array( 'act_mode'=>'s_viewtimeslotf',
                'nextDate'=>$dateval,
                'starttime'=>$this->input->post('destinationType'),
                'branchid'=>$data['branch']->branch_id,
                'dailyinventory_to' =>'',
                'dailyinventory_seats' =>'',
                'dailyinventory_status' =>'',
                'dailyinventory_createdon' =>'',
                'dailyinventory_modifiedon' =>'',
                'dailyinventory_minfrom' =>'',
                'dailyinventory_minto' =>'',
            );

            $data['s_viewtimeslot'] = $this->supper_admin->call_procedurerow('proc_crone_v',$parameter2);




//echo ($data['s_viewtimeslot']->coun+$this->input->post('ddAdult'))."-----------".$data['s_viewtimeslot']->timeslot_seats;


            if(($data['s_viewtimeslot']->coun+$this->input->post('ddAdult'))<=$data['s_viewtimeslot']->dailyinventory_seats)
            {


                $dateval=explode("/",$_POST['txtDepartDate']);

                $_POST['txtDepartDate']=$dateval[1]."/".$dateval[0]."/".$dateval[2]."";

                $data = array(
                    'uniqid' => uniqid(),
                    'txtDepartDate' => $_POST['txtDepartDate'],
                    'destinationType' => $_POST['destinationType'],
                    'ddAdult' => $_POST['ddAdult']
                );
                $this->session->set_userdata($data);



                redirect(base_url()."agentpackagesstep");
            }
            else{
                if(($data['s_viewtimeslot']->dailyinventory_seats-$data['s_viewtimeslot']->coun)<=0)
                {
                    $data['emsg']=" No Seats Avalilable";

                }
                else
                {
                    $data['emsg']=" No Seats Avalilable";
                    //$data['emsg']=($data['s_viewtimeslot']->dailyinventory_seats-$data['s_viewtimeslot']->coun)." Seats Avalilable";
                }     }




//header("location:".base_url()."packages?searchid=MQ==");
        }




        $this->load->view("agent/helper/header",$data);
        $this->load->view("agent/helper/leftbar",$data);
        $this->load->view("agent/bookingsearch",$data);
        $this->load->view("agent/helper/footer");

    }
    public function agentpackagesstep(){

//Select branch
        $siteurl= base_url();
        $parameterbranch=array(
            'act_mode' =>'selectbranch',
            'weburl' =>$siteurl,
            'type'=>'web',

        );

        $path=api_url().'selectsiteurl/branch/format/json/';
        $data['branch']=curlpost($parameterbranch,$path);



//select banner images
        $parameterbanner=array(
            'act_mode' =>'selectbannerimages',
            'branchid' =>$data['branch']->branch_id,
            'type'=>'web',

        );

        $path=api_url().'selectsiteurl/banner/format/json/';
        $data['banner']=curlpost($parameterbanner,$path);

        //Select Time slot
        $parametertimeslot=array(
            'act_mode' =>'selectsestimeslot',
            'branchid' =>$data['branch']->branch_id,
            'destinationType' =>$this->session->userdata('destinationType'),
            'type'=>'web',

        );

        $path=api_url().'selecttimesloturl/timeslotses/format/json/';
        $data['timeslotses']=curlpost($parametertimeslot,$path);
        if(($this->session->userdata['ppid'])>0)

        {

            $parameterpac = array('act_mode' => 'select_packagepartner',
                'branchid' => $this->session->userdata['branch_id'],
                'type' => 'web',
                'packageqty' => '',
                'packageid' => ''
            );


        }
        else
        {

            $parameterpac = array('act_mode' => 'select_packageuserguest',
                'branchid' => $this->session->userdata['branch_id'],
                'type' => 'web',
                'packageqty' => '',
                'packageid' => ''
            );
        }
        $path = api_url()."Packages/packageselect/format/json/";

        $data['vieww']= curlpost($parameterpac,$path);


//continue to 2nd stage
        if($this->input->post('submit')=='CONTINUE package')
        {
            $this->session->unset_userdata('addonsval');
            if($_POST['packageid']!='')
            {

                $data = array(
                    'uniqid' => uniqid(),
                    'packageval' => $_POST['packageid'],
                    'packageimageval' => $_POST['packageimage'],
                    'packagenameval' => $_POST['packagename'],
                    'packageqtyval' => $_POST['packageqty'],
                    'packagepriceval' => $_POST['packageprice'],
                    'packagedescription' => $_POST['packagedescription'],
                );


                $this->session->set_userdata($data);
                header("location:".base_url()."agentaddones");
            }
            else
            {header("location:".base_url()."agentpackagesstep");}

        }



        $this->load->view("agent/helper/header",$data);
        $this->load->view("agent/helper/leftbar",$data);
        $this->load->view("agent/agentpackagesstep",$data);
        $this->load->view("agent/helper/footer");
    }
    public function agentaddones(){


//$this->session->sess_destroy();
        //Select Time slot
//Select branch
        $siteurl= base_url();
        $parameterbranch=array(
            'act_mode' =>'selectbranch',
            'weburl' =>$siteurl,
            'type'=>'web',

        );

        $path=api_url().'selectsiteurl/branch/format/json/';
        $data['branch']=curlpost($parameterbranch,$path);



//select banner images
        $parameterbanner=array(
            'act_mode' =>'selectbannerimages',
            'branchid' =>$data['branch']->branch_id,
            'type'=>'web',

        );

        $path=api_url().'selectsiteurl/banner/format/json/';
        $data['banner']=curlpost($parameterbanner,$path);


        $parametertimeslot=array(
            'act_mode' =>'selectsestimeslot',
            'branchid' =>$data['branch']->branch_id,
            'destinationType' =>$this->session->userdata('destinationType'),
            'type'=>'web',

        );

        $path=api_url().'selecttimesloturl/timeslotses/format/json/';
        $data['timeslotses']=curlpost($parametertimeslot,$path);

        if($this->input->post('delete')!='')

        {


            $datadelete = array(
                'act_mode' =>'deleteaddone',
                'categories1' => '',
                'addonid' => $this->input->post('delete'),
                'addonprice' => '',
                'addonqty' =>'',
                'addonimage' => '',
                'addonname' => '',
                'uniqueid' => '',
                'type' => 'web'
            );
            $pathdelete= api_url()."Cart/deleteaddonscartses_select/format/json/";
            $data['addonscartdelete']= curlpost($datadelete,$pathdelete);


        }

        if($_POST['categories1']){




            for($i=0;$i<=count($_POST['categories1']);$i++)

            {


                if($_POST['categories1'][$i]!=0)
                {


                    //echo $_POST['addonid'][$i];
                    $data13[$i] = array(
                        'act_mode' =>'selectaddone',
                        'categories1' => '',
                        'addonid' => $_POST['addonid'][$i],
                        'addonprice' =>'',
                        'addonqty' => '',
                        'addonimage' => '',
                        'addonname' => '',
                        'uniqueid' => $this->session->userdata['uniqid'],
                        'type' =>'web');
                    $pathaddone13 = api_url()."Cart/insertaddonscartses_select/format/json/";
                    $data['addonscartselectd']= curlpost($data13[$i],$pathaddone13);


                    $arr = (array)$data['addonscartselectd'];




                    if ($arr[0]['addonid'] == $_POST['addonid'][$i])
                    {

                        $data[$i] = array(
                            'act_mode' =>'updateaddone111',
                            'categories1' => $_POST['categories1'][$i],
                            'addonid' => $_POST['addonid'][$i],
                            'addonprice' => $_POST['addonprice'][$i],
                            'addonqty' => $_POST['addonqty'][$i],
                            'addonimage' => $_POST['addonimage'][$i],
                            'addonname' => $_POST['addonname'][$i],
                            'uniqueid' => $this->session->userdata['uniqid'],
                            'type' =>'web'
                        );

                        $pathaddone= api_url()."Cart/insertaddonscartses_select/format/json/";
                        $data['addonscart']= curlpost($data[$i],$pathaddone);

                        // p($data[$i]);



                    }
                    else{

                        $data[$i] = array(
                            'act_mode' =>'insertaddone',
                            'categories1' => $_POST['categories1'][$i],
                            'addonid' => $_POST['addonid'][$i],
                            'addonprice' => $_POST['addonprice'][$i],
                            'addonqty' => $_POST['addonqty'][$i],
                            'addonimage' => $_POST['addonimage'][$i],
                            'addonname' => $_POST['addonname'][$i],
                            'uniqueid' => $this->session->userdata['uniqid'],
                            'type' =>'web'
                        );
                        $pathaddone= api_url()."Cart/insertaddonscartses_select/format/json/";
                        $data['addonscart']= curlpost($data[$i],$pathaddone);
                        // p($data['addonscart']);
                        // p($data[$i]);

                    }



                }


            }




        }
        //p($obj);
        $dataaddonedisplay = array(
            'act_mode' =>'aaddonedisplay',
            'categories1' => '',
            'addonid' => '',
            'addonprice' => '',
            'addonqty' =>'',
            'addonimage' => '',
            'addonname' =>'',
            'uniqueid' => $this->session->userdata['uniqid'],
            'type' => 'web'
        );
        $pathdisplay= api_url()."Cart/displayaddonscartses_select/format/json/";
        $data['addonsdisplay']= curlpost($dataaddonedisplay,$pathdisplay);
//p($data['addonsdisplay']);
        //  p($this->session->userdata);




        $parameterseo=array('act_mode'=>'viewseotags','rowid'=>1);
        $data['seotags']=$this->supper_admin->call_procedureRow('proc_siteconfig',$parameterseo);

//display addons parameters
        $parameterbranch        = array('act_mode'=>'addonscartses_package',
            'branch_id'=>$this->session->userdata['branch_id'],
            'type'=>'web',
        );
        $pathbranch= api_url()."Cart/getaddonscartses_select/format/json/";
        $data['addonscart']= curlpost($parameterbranch,$pathbranch);


        if($this->input->post('submit')=='cartses')
        {

            $data = array(

                'cartpackageimage' => $_POST['cartpackageimage'],
                'cartpackagename' => $_POST['cartpackagename'],
                'cartpackageprice' => $_POST['cartpackageprice'],
                'cartsubtotal' => $_POST['cartsubtotal'],

                'cartaddoneid' =>  implode(',',$_POST['cartaddoneid']),
                'cartaddoneimage' =>  implode(',',$_POST['cartaddoneimage']),
                'cartaddonname' =>  implode(',',$_POST['cartaddonname']),
                'cartaddonqty' =>  implode(',',$_POST['cartaddonqty']),
                'cartaddonprice' =>  implode(',',$_POST['cartaddonprice']),

            );
            $this->session->set_userdata($data);



            redirect(base_url()."partnersummary");

        }








        $this->load->view("agent/helper/header",$data);
        $this->load->view("agent/helper/leftbar",$data);
        $this->load->view("agent/agentaddones",$data);
        $this->load->view("agent/helper/footer");
    }

public function partnersummary()
{


//Select branch
    $siteurl= base_url();
    $parameterbranch=array(
        'act_mode' =>'selectbranch',
        'weburl' =>$siteurl,
        'type'=>'web',

    );

    $path=api_url().'selectsiteurl/branch/format/json/';
    $data['branch']=curlpost($parameterbranch,$path);



//select banner images
    $parameterbanner=array(
        'act_mode' =>'selectbannerimages',
        'branchid' =>$data['branch']->branch_id,
        'type'=>'web',

    );

    $path=api_url().'selectsiteurl/banner/format/json/';
    $data['banner']=curlpost($parameterbanner,$path);

//Select Time slot
    $parametertimeslot=array(
        'act_mode' =>'selectsestimeslot',
        'branchid' =>$data['branch']->branch_id,
        'destinationType' =>$this->session->userdata('destinationType'),
        'type'=>'web',

    );

    $path=api_url().'selecttimesloturl/timeslotses/format/json/';
    $data['timeslotses']=curlpost($parametertimeslot,$path);


// Promocode
    if($_POST['promoFormdata']!='')
    {
        $parameter         = array('act_mode'=>'promoselect_package','promovalue'=>$_POST['promoFormdata'],'locationid'=>$this->session->userdata['locationid'],
            'branch_id'=>$this->session->userdata['branch_id'],
            'userid'=>$this->session->userdata['userid'],
        );
        $path = api_url()."Cart/getpromoselect_package/format/json/";
        $data['promocode']= curlpost($parameter,$path);


        if($data['promocode']->scalar=='Something Went Wrong'){ $data['msg']="Invalid Promocode"; }
        else
        {
            $originalDate = $this->session->userdata('txtDepartDate');
            $Date = explode("/",$this->session->userdata('txtDepartDate'));
            $newDate=$Date[1]."/".$Date[0]."/".$Date[2];
            if($data['promocode']->p_logintype=='0' or $data['promocode']->p_logintype=='1') {

//echo $newDate."---".$data['promocode']->p_start_date."---".$data['promocode']->p_expiry_date;

                if ($newDate >= $data['promocode']->p_start_date and $newDate <= $data['promocode']->p_expiry_date) {

                    //Complete Promo Select
                    $parametercompuser = array('act_mode' => 'promoselectcompuser_package', 'promovalue' => $_POST['promoFormdata'], 'locationid' => '',
                        'branch_id' => '',
                        'userid' => '',
                    );
                    $pathcompuser = api_url() . "Cart/getpromoselect_packageuser/format/json/";
                    $data['promocompuser'] = curlpost($parametercompuser, $pathcompuser);


                    if ($data['promocompuser']->num < $data['promocode']->p_allowed_times) {
//Userwise Promo select

                        $parameteruser = array('act_mode' => 'promoselectuser_package', 'promovalue' => $_POST['promoFormdata'], 'locationid' => '',
                            'branch_id' => '',
                            'userid' => $this->session->userdata('skiindia'),
                        );
                        $pathuser = api_url() . "Cart/getpromoselect_packageuser/format/json/";
                        $data['promocodeuser'] = curlpost($parameteruser, $pathuser);


                        if ($data['promocode']->p_allowed_per_user > 0) {

                            if ($data['promocodeuser']->num < $data['promocode']->p_allowed_per_user) {


                            } else {

                                $data['msg'] = "Max Allowed Exceed";
                            }
                        } else {

                            $data['msg'] = "Max Allowed Exceed";

                        }


                    } else {

                        $data['msg'] = "Invalid Promocode";
                    }

                    // echo($this->session->userdata('txtDepartDate'));
                }
                else
                {

                    $data['msg']="Invalid Promocode";
                }}
            else
            {

                $data['msg']="Invalid Promocode";
            }
        }



    }
    else
    {

        $data['msg']="Promo Not Add";
    }


    if($_POST['submit']=='final add')
    {

        $parameterunique         = array('act_mode'=>'selectbooking',
            'subtotal'=>'',
            'discountamount'=>'',
            'total'=>'',
            'countryid'=>'',
            'stateid'=>'',
            'cityid'=>'',
            'branch_id'=>'',
            'locationid'=>'',
            'userid'=>'',
            'txtDepartDate1'=>'',
            'txtDepartdata'=>'',
            'paymentmode'=>'',
            'ticketid'=>$this->session->userdata['uniqid'],
            'packproductname'=>'',
            'packimg'=>'',
            'packpkg'=>'',
            'packprice'=>'',
            'promocodeprice'=>'',
            'internethandlingcharges'=>'',
            'txtfrommin'=>'',
            'txttohrs'=>'',
            'txttomin'=>'',
            'txtfromd'=>'',
            'txttod'=>'',
            'p_codename'=>'',
            'p_usertype'=>'',
            'type'=>'web',

        );

        $path = api_url()."Cart/getorderadd_package/format/json/";
        $data['packageunique']= curlpost($parameterunique,$path);


        if($this->session->userdata('pid')>0)
        {
            $_POST['p_usertype']="Agent";

        }
        elseif($this->session->userdata('skiindia')>0)
        {
            $_POST['p_usertype']="User";

        }
        else{
            $_POST['p_usertype']="Guest";

        }


        if($data['packageunique']->scalar=='Something Went Wrong')
        {

            $parameter         = array('act_mode'=>'orderpackage_insert',
                'subtotal'=>$_POST['subtotal'],
                'discountamount'=>($_POST['discountamount']),
                'total'=>$_POST['total'],
                'countryid'=>$this->session->userdata['countryid'],
                'stateid'=>$this->session->userdata['stateid'],
                'cityid'=>$this->session->userdata['cityid'],
                'branch_id'=>$this->session->userdata['branch_id'],
                'locationid'=>$this->session->userdata['locationid'],
                'userid'=>$this->session->userdata('skiindia'),
                'txtDepartDate1'=>$_POST['txtDepartDate1'],
                'txtDepartdata'=>$_POST['txtDepartdata'],
                'paymentmode'=>'NULL',
                'ticketid'=>$this->session->userdata['uniqid'],
                'packproductname'=>$this->session->userdata['cartpackagename'],
                'packimg'=>$this->session->userdata['packageimageval'],
                'packpkg'=>$this->session->userdata['packageqtyval'],
                'packprice'=>$this->session->userdata['packagepriceval'],
                'promocodeprice'=>$_POST['discountamount'],
                'internethandlingcharges'=>$_POST['internethandlingcharges'],
                'txtfrommin'=>$_POST['txtfrommin'],
                'txttohrs'=>$_POST['txttohrs'],
                'txttomin'=>$_POST['txttomin'],
                'txtfromd'=>$_POST['txtfromd'],
                'txttod'=>$_POST['txttod'],
                'p_codename'=>$_POST['p_codename'],
                'p_usertype'=>$_POST['p_usertype'],

                'type'=>'web',

            );

            $path = api_url()."Cart/getorderadd_package/format/json/";
            $data['packageadd']= curlpost($parameter,$path);
        }
        else
        {

            $parameter         = array('act_mode'=>'orderpackage_update',
                'subtotal'=>$_POST['subtotal'],
                'discountamount'=>($_POST['discountamount']),
                'total'=>$_POST['total'],
                'countryid'=>$this->session->userdata['countryid'],
                'stateid'=>$this->session->userdata['stateid'],
                'cityid'=>$this->session->userdata['cityid'],
                'branch_id'=>$this->session->userdata['branch_id'],
                'locationid'=>$this->session->userdata['locationid'],
                'userid'=>$this->session->userdata('skiindia'),
                'txtDepartDate1'=>$_POST['txtDepartDate1'],
                'txtDepartdata'=>$_POST['txtDepartdata'],
                'paymentmode'=>'NULL',
                'ticketid'=>$this->session->userdata['uniqid'],
                'packproductname'=>$this->session->userdata['cartpackagename'],
                'packimg'=>$this->session->userdata['packageimageval'],
                'packpkg'=>$this->session->userdata['packageqtyval'],
                'packprice'=>$this->session->userdata['packagepriceval'],
                'promocodeprice'=>$_POST['discountamount'],
                'internethandlingcharges'=>$_POST['internethandlingcharges'],
                'txtfrommin'=>$_POST['txtfrommin'],
                'txttohrs'=>$_POST['txttohrs'],
                'txttomin'=>$_POST['txttomin'],
                'txtfromd'=>$_POST['txtfromd'],
                'txttod'=>$_POST['txttod'],
                'p_codename'=>$_POST['p_codename'],
                'type'=>'web',

            );

            $path = api_url()."Cart/getorderadd_package/format/json/";
            $data['packageadd']= curlpost($parameter,$path);

        }




        $arr = (array)$data['packageadd'];

        for( $i= 0 ; $i < (count($_POST['ccartaddonqty'])) ; $i++ ) {

            //echo "hello";
            $parameter1 = array('act_mode'=>'orderaddone_insert',

                'addonnameses'=>$_POST['ccartaddonname'][$i],
                'addonquantityses'=>$_POST['ccartaddonqty'][$i],
                'addonidses'=>$_POST['addonid'][$i],
                'lastidses'=>$arr[0]['last_insert_id()'],
                'addonpriceses'=>$_POST['ccartaddonprice'][$i],
                'addonidses'=>$_POST['ccaddonid'][$i],
                'ccartpackageimage'=>$_POST['ccartpackageimage'][$i],

            );
            $path1 = api_url()."Cart/getaddonadd_package/format/json/";
            $data['promoaddoneadd']= curlpost($parameter1,$path1);


        }
        $data = array(
            'orderlastinsertid' => $arr[0]['last_insert_id()'],
        );

        $this->session->set_userdata($data);

        $data['skiindases'] =($data['login']->user_id);

       if($this->session->userdata('ppid')>0)
        {
            redirect(base_url()."partnerpaymentagent");
        }
        else
        {
            redirect(base_url()."partnersummary");
        }



        // header("location:".base_url()."payment");




    }
    $data['internethandlingcharge']=($data['branch']->branch_internet_handling_charge);
    $this->load->view("agent/helper/header",$data);
    $this->load->view("agent/helper/leftbar",$data);
    $this->load->view("agent/partnersummary",$data);
    $this->load->view("agent/helper/footer");
}
public function partnerpaymentagent()
{
//Select branch
    $siteurl= base_url();
    $parameterbranch=array(
        'act_mode' =>'selectbranch',
        'weburl' =>$siteurl,
        'type'=>'web',

    );

    $path=api_url().'selectsiteurl/branch/format/json/';
    $data['branch']=curlpost($parameterbranch,$path);



    $parameterccgatway=array(
        'act_mode' =>'selectccavenue',
        'branchid' =>$data['branch']->branch_id,
        'type'=>'web',

    );

    $path=api_url().'ccavenue/ccavRequestHandler/format/json/';
    $data['ccavRequestHandler']=curlpost($parameterccgatway,$path);

//select banner images
    $parameterbanner=array(
        'act_mode' =>'selectbannerimages',
        'branchid' =>$data['branch']->branch_id,
        'type'=>'web',

    );

    $path=api_url().'selectsiteurl/banner/format/json/';
    $data['banner']=curlpost($parameterbanner,$path);




    $parameter=array(
        'act_mode' =>'memusersesiid',
        'userid' =>$this->session->userdata['skiindia'],
        'type'=>'web',
    );

    $path=api_url().'userapi/usersesion/format/json/';
    $data['memuser']=curlpost($parameter,$path);




    $parametertimeslot=array(
        'act_mode' =>'selectsestimeslot',
        'branchid' =>$data['branch']->branch_id,
        'destinationType' =>$this->session->userdata('destinationType'),
        'type'=>'web',

    );

    $path=api_url().'selecttimesloturl/timeslotses/format/json/';
    $data['timeslotses']=curlpost($parametertimeslot,$path);


    $parameter = array('act_mode'=>'select_order',

        'orderid'=>$this->session->userdata('orderlastinsertid'),
        'type'=>'web',

    );
    $path1 = api_url()."Cart/getpayment_package/format/json/";
    $data['paymentpac']= curlpost($parameter,$path1);

    $this->load->view("agent/helper/header",$data);
    $this->load->view("agent/helper/leftbar",$data);
    $this->load->view("agent/partnerpaymentagent",$data);
    $this->load->view("agent/helper/footer");
}
}//end of class
?>