<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Partner extends MX_Controller {

   public function __construct() {
      $this->load->model("supper_admin");
   $this->load->library('session');
  }
    public function dashboard()
    {
       
        $this->load->view("agent/helper/header",$data);
        $this->load->view("agent/helper/leftbar",$data);
        $this->load->view("agent/partnerdashboard",$data);
        $this->load->view("agent/helper/footer");

    }
//partnerregister update   page
public function agentform1()
{

   
    if($this->input->post('submit')=='Send Pin')

    {
    $smsphnon = $this->input->post('umob');

    $num = rand(1111, 9999);
    $parameter =array('act_mode'=>'chmobileotp',
        'row_id'=>'',
        'p_userid'=>'',
        'p_email'=>$this->session->userdata['pemailid'],
        'p_mobilenum'=>$smsphnon,
        'p_mas'=>'');
    $path1=api_url().'partnerapi/chmobileotp/format/json/';
    $record=curlpost($parameter,$path1);
   // p($record);exit();
        $smsmsgg = urlencode('Hi User, your OTP Password is ' . $num . ' for Mobile Number Verification.');

        $sms_url = "http://bulkpush.mytoday.com/BulkSms/SingleMsgApi?feedid=356637&username=9867622549&password=mtpjp&To=" . $this->input->post('umob') . "&Text=" . $smsmsgg;
        $sms = file_get_contents($sms_url);

        $data = array(
            'agentmobile' => $this->input->post('umob'),
            'p_email' => $this->session->userdata['pemailid'],
            'agentotp' => $num,
        );

        $this->session->set_userdata($data);

        header("Location:agentformverify");

    }
 $this->load->view("helper/header",$data);
    $this->load->view("helper/topbar",$data);
    $this->load->view("agent/form1",$data);
    $this->load->view("helper/footer");

}
    public function agentformverify()
    {

        if($this->input->post('submit')=='Send Pin')

        {

            if($this->input->post('umob')==$this->session->userdata('agentotp'))

            {

                $smsphnon = $this->session->userdata('agentmobile');
                $parameter =array('act_mode'=>'chmobileotpinsert',
                    'row_id'=>'',
                    'p_userid'=>'',
                    'p_email'=>$this->session->userdata['pemailid'],
                    'p_mobilenum'=>$smsphnon,
                    'p_mas'=>'');
                $path1=api_url().'partnerapi/chmobileotp/format/json/';
                $record=curlpost($parameter,$path1);

               header("location:agentform2");
            }
            else{

                $data['message']="Invalid Promocode";
            }

        }

        $this->load->view("helper/header",$data);
        $this->load->view("helper/topbar",$data);
        $this->load->view("agent/agentformverify",$data);
        $this->load->view("helper/footer");

    }



    public function agentform2()
    {


        if($this->input->post('Submit')=='Submit') {
            $parameterreg = array(
                'act_mode' => 'insertpartnerstep1',
                'title' => '',
                'firstname' => '',
                'lastname' => '',
                'row_id' => $this->session->userdata['pid'],
                'logintype' => $this->input->post('logintype'),
                'emailstatus' => $this->input->post('emailstatus'),
                'emailcurrency' => $this->input->post('emailcurrency'),
                'emailcountry' => $this->input->post('emailcountry'),

                'agencyname' => $this->input->post('agencyname'),
                'office_address' => $this->input->post('office_address'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'pincode' => $this->input->post('pincode'),
                'telephone' => $this->input->post('telephone'),
                'mobile' => '',
                'website' => $this->input->post('website'),
                'fax' => $this->input->post('fax'),
                'email' => $this->session->userdata['pemailid'],
                'primarycontact_name' => $this->input->post('primarycontact_name'),
                'primarycontact_email_mobile' => $this->input->post('primarycontact_email_mobile'),
                'primarycontact_telephone' => $this->input->post('primarycontact_telephone'),
                'secondrycontact_name' => $this->input->post('secondrycontact_name'),
                'secondrycontact_email_phone' => $this->input->post('secondrycontact_email_phone'),
                'secondrycontact_telephone' => $this->input->post('secondrycontact_telephone'),
                'management_executives_name' => $this->input->post('management_executives_name'),
                'management_executives_email_phone' => $this->input->post('management_executives_email_phone'),
                'management_executives_telephone' => $this->input->post('management_executives_telephone	'),
                'branches' => $this->input->post('branches'),

                'yearofestablism' => $this->input->post('yearofestablism'),
                'organisationtype' => $this->input->post('organisationtype'),
                'lstno' => $this->input->post('lstno'),
                'cstno' => $this->input->post('cstno'),
                'registrationno' => $this->input->post('registrationno'),
                'vatno' => $this->input->post('vatno'),
                'panno' => $this->input->post('panno'),
                'servicetaxno' => $this->input->post('servicetaxno'),
                'tanno' => $this->input->post('tanno'),
                'pfno' => $this->input->post('pfno'),
                'esisno' => $this->input->post('esisno'),
                'officeregistrationno' => $this->input->post('officeregistrationno'),
                'exceptiontax' => $this->input->post('exceptiontax'),
                'otherexceptiontax' => $this->input->post('otherexceptiontax'),
                'bank_benificialname' => $this->input->post('bank_benificialname'),
                'bank_benificialaccno' => $this->input->post('bank_benificialaccno'),
                'bank_benificialbankname' => $this->input->post('bank_benificialbankname'),
                'bank_benificialbranchname' => $this->input->post('bank_benificialbranchname'),
                'bank_benificialaddress' => $this->input->post('bank_benificialaddress'),
                'bank_benificialifsc' => $this->input->post('bank_benificialifsc'),
                'bank_benificialswiftcode' => $this->input->post('bank_benificialswiftcode'),
                'bank_benificialibanno' => $this->input->post('bank_benificialibanno'),
                'intermidiatebankname' => $this->input->post('intermidiatebankname'),
                'intermidiatebankaddress' => $this->input->post('intermidiatebankaddress'),

                'intermidiatebankswiftcode' => $this->input->post('intermidiatebankswiftcode'),
                'bank_ecs' => $this->input->post('bank_ecs'),
                'copypancart' => $pancart,
                'copyservicetax' => $servicetax,
                'copytan' => $tan,
                'copyaddressproof' => $addressproof,

                'username' => $this->session->userdata['pemailid'],
                'password' => base64_encode($this->input->post('password')),

                'countryid' => $data['branch']->countryid,
                'stateid' => $data['branch']->stateid,
                'cityid' => $data['branch']->cityid,
                'branch_id' => $data['branch']->branch_id,
                'locationid' => $data['branch']->branch_location,
                'bank_creditamount' => '',
                'bank_agentcommision' => '',


                'type' => 'web',
            );

            $path = api_url() . 'partnerapi/partnerregister/format/json/';
            $data['reg'] = curlpost($parameterreg, $path);
header("location:agentform3");

        }


        $this->load->view("helper/header",$data);
        $this->load->view("helper/topbar",$data);
        $this->load->view("agent/form2",$data);
        $this->load->view("helper/footer");

    }
    public function agentform3()
    {


        if($this->input->post('Submit')=='Submit')
        {
            $parameterreg = array(
                'act_mode' => 'insertpartnerstep2',
                'title' => '',
                'firstname' => '',
                'lastname' => '',
                'row_id' => $this->session->userdata['pid'],
                'logintype' => $this->input->post('logintype'),
                'emailstatus' => $this->input->post('emailstatus'),
                'emailcurrency' => $this->input->post('emailcurrency'),
                'emailcountry' => $this->input->post('emailcountry'),

                'agencyname' => $this->input->post('agencyname'),
                'office_address' => $this->input->post('office_address'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'pincode' => $this->input->post('pincode'),
                'telephone' => $this->input->post('telephone'),
                'mobile' => '',
                'website' => $this->input->post('website'),
                'fax' => $this->input->post('fax'),
                'email' => $this->session->userdata['pemailid'],
                'primarycontact_name' => $this->input->post('primarycontact_name'),
                'primarycontact_email_mobile' => $this->input->post('primarycontact_email_mobile'),
                'primarycontact_telephone' => $this->input->post('primarycontact_telephone'),
                'secondrycontact_name' => $this->input->post('secondrycontact_name'),
                'secondrycontact_email_phone' => $this->input->post('secondrycontact_email_phone'),
                'secondrycontact_telephone' => $this->input->post('secondrycontact_telephone'),
                'management_executives_name' => $this->input->post('management_executives_name'),
                'management_executives_email_phone' => $this->input->post('management_executives_email_phone'),
                'management_executives_telephone' => $this->input->post('management_executives_telephone	'),
                'branches' => $this->input->post('branches'),

                'yearofestablism' => $this->input->post('yearofestablism'),
                'organisationtype' => $this->input->post('organisationtype'),
                'lstno' => $this->input->post('lstno'),
                'cstno' => $this->input->post('cstno'),
                'registrationno' => $this->input->post('registrationno'),
                'vatno' => $this->input->post('vatno'),
                'panno' => $this->input->post('panno'),
                'servicetaxno' => $this->input->post('servicetaxno'),
                'tanno' => $this->input->post('tanno'),
                'pfno' => $this->input->post('pfno'),
                'esisno' => $this->input->post('esisno'),
                'officeregistrationno' => $this->input->post('officeregistrationno'),
                'exceptiontax' => $this->input->post('exceptiontax'),
                'otherexceptiontax' => $this->input->post('otherexceptiontax'),
                'bank_benificialname' => $this->input->post('bank_benificialname'),
                'bank_benificialaccno' => $this->input->post('bank_benificialaccno'),
                'bank_benificialbankname' => $this->input->post('bank_benificialbankname'),
                'bank_benificialbranchname' => $this->input->post('bank_benificialbranchname'),
                'bank_benificialaddress' => $this->input->post('bank_benificialaddress'),
                'bank_benificialifsc' => $this->input->post('bank_benificialifsc'),
                'bank_benificialswiftcode' => $this->input->post('bank_benificialswiftcode'),
                'bank_benificialibanno' => $this->input->post('bank_benificialibanno'),
                'intermidiatebankname' => $this->input->post('intermidiatebankname'),
                'intermidiatebankaddress' => $this->input->post('intermidiatebankaddress'),

                'intermidiatebankswiftcode' => $this->input->post('intermidiatebankswiftcode'),
                'bank_ecs' => $this->input->post('bank_ecs'),
                'copypancart' => $pancart,
                'copyservicetax' => $servicetax,
                'copytan' => $tan,
                'copyaddressproof' => $addressproof,

                'username' => $this->session->userdata['pemailid'],
                'password' => base64_encode($this->input->post('password')),

                'countryid' => $data['branch']->countryid,
                'stateid' => $data['branch']->stateid,
                'cityid' => $data['branch']->cityid,
                'branch_id' => $data['branch']->branch_id,
                'locationid' => $data['branch']->branch_location,
                'bank_creditamount' => '',
                'bank_agentcommision' => '',


                'type' => 'web',
            );

            $path = api_url() . 'partnerapi/partnerregister/format/json/';
            $data['reg'] = curlpost($parameterreg, $path);
            header("location:agentform4");
        }
        $this->load->view("helper/header",$data);
        $this->load->view("helper/topbar",$data);
        $this->load->view("agent/form3",$data);
        $this->load->view("helper/footer");

    }

    public function agentform4()
    {

        if($this->input->post('Submit')=='Submit')
        {
            $parameterreg = array(
                'act_mode' => 'insertpartnerstep4',
                'title' => '',
                'firstname' => '',
                'lastname' => '',
                'row_id' => $this->session->userdata['pid'],
                'logintype' => $this->input->post('logintype'),
                'emailstatus' => $this->input->post('emailstatus'),
                'emailcurrency' => $this->input->post('emailcurrency'),
                'emailcountry' => $this->input->post('emailcountry'),

                'agencyname' => $this->input->post('agencyname'),
                'office_address' => $this->input->post('office_address'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'pincode' => $this->input->post('pincode'),
                'telephone' => $this->input->post('telephone'),
                'mobile' => '',
                'website' => $this->input->post('website'),
                'fax' => $this->input->post('fax'),
                'email' => $this->session->userdata['pemailid'],
                'primarycontact_name' => $this->input->post('primarycontact_name'),
                'primarycontact_email_mobile' => $this->input->post('primarycontact_email_mobile'),
                'primarycontact_telephone' => $this->input->post('primarycontact_telephone'),
                'secondrycontact_name' => $this->input->post('secondrycontact_name'),
                'secondrycontact_email_phone' => $this->input->post('secondrycontact_email_phone'),
                'secondrycontact_telephone' => $this->input->post('secondrycontact_telephone'),
                'management_executives_name' => $this->input->post('management_executives_name'),
                'management_executives_email_phone' => $this->input->post('management_executives_email_phone'),
                'management_executives_telephone' => $this->input->post('management_executives_telephone	'),
                'branches' => $this->input->post('branches'),

                'yearofestablism' => $this->input->post('yearofestablism'),
                'organisationtype' => $this->input->post('organisationtype'),
                'lstno' => $this->input->post('lstno'),
                'cstno' => $this->input->post('cstno'),
                'registrationno' => $this->input->post('registrationno'),
                'vatno' => $this->input->post('vatno'),
                'panno' => $this->input->post('panno'),
                'servicetaxno' => $this->input->post('servicetaxno'),
                'tanno' => $this->input->post('tanno'),
                'pfno' => $this->input->post('pfno'),
                'esisno' => $this->input->post('esisno'),
                'officeregistrationno' => $this->input->post('officeregistrationno'),
                'exceptiontax' => $this->input->post('exceptiontax'),
                'otherexceptiontax' => $this->input->post('otherexceptiontax'),
                'bank_benificialname' => $this->input->post('bank_benificialname'),
                'bank_benificialaccno' => $this->input->post('bank_benificialaccno'),
                'bank_benificialbankname' => $this->input->post('bank_benificialbankname'),
                'bank_benificialbranchname' => $this->input->post('bank_benificialbranchname'),
                'bank_benificialaddress' => $this->input->post('bank_benificialaddress'),
                'bank_benificialifsc' => $this->input->post('bank_benificialifsc'),
                'bank_benificialswiftcode' => $this->input->post('bank_benificialswiftcode'),
                'bank_benificialibanno' => $this->input->post('bank_benificialibanno'),
                'intermidiatebankname' => $this->input->post('intermidiatebankname'),
                'intermidiatebankaddress' => $this->input->post('intermidiatebankaddress'),

                'intermidiatebankswiftcode' => $this->input->post('intermidiatebankswiftcode'),
                'bank_ecs' => $this->input->post('bank_ecs'),
                'copypancart' => $pancart,
                'copyservicetax' => $servicetax,
                'copytan' => $tan,
                'copyaddressproof' => $addressproof,

                'username' => $this->session->userdata['pemailid'],
                'password' => base64_encode($this->input->post('password')),

                'countryid' => $data['branch']->countryid,
                'stateid' => $data['branch']->stateid,
                'cityid' => $data['branch']->cityid,
                'branch_id' => $data['branch']->branch_id,
                'locationid' => $data['branch']->branch_location,
                'bank_creditamount' => '',
                'bank_agentcommision' => '',


                'type' => 'web',
            );

            $path = api_url() . 'partnerapi/partnerregister/format/json/';
            $data['reg'] = curlpost($parameterreg, $path);
           header("location:agentform5");
        }

        $this->load->view("helper/header",$data);
        $this->load->view("helper/topbar",$data);
        $this->load->view("agent/form4",$data);
        $this->load->view("helper/footer");

    }
    public function agentform5()
    {

        if($this->input->post('Submit')=='Submit')
        {
            $parameterreg = array(
                'act_mode' => 'insertpartnerstep5',
                'title' => '',
                'firstname' => '',
                'lastname' => '',
                'row_id' => $this->session->userdata['pid'],
                'logintype' => $this->input->post('logintype'),
                'emailstatus' => $this->input->post('emailstatus'),
                'emailcurrency' => $this->input->post('emailcurrency'),
                'emailcountry' => $this->input->post('emailcountry'),

                'agencyname' => $this->input->post('agencyname'),
                'office_address' => $this->input->post('office_address'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'pincode' => $this->input->post('pincode'),
                'telephone' => $this->input->post('telephone'),
                'mobile' => '',
                'website' => $this->input->post('website'),
                'fax' => $this->input->post('fax'),
                'email' => $this->session->userdata['pemailid'],
                'primarycontact_name' => $this->input->post('primarycontact_name'),
                'primarycontact_email_mobile' => $this->input->post('primarycontact_email_mobile'),
                'primarycontact_telephone' => $this->input->post('primarycontact_telephone'),
                'secondrycontact_name' => $this->input->post('secondrycontact_name'),
                'secondrycontact_email_phone' => $this->input->post('secondrycontact_email_phone'),
                'secondrycontact_telephone' => $this->input->post('secondrycontact_telephone'),
                'management_executives_name' => $this->input->post('management_executives_name'),
                'management_executives_email_phone' => $this->input->post('management_executives_email_phone'),
                'management_executives_telephone' => $this->input->post('management_executives_telephone	'),
                'branches' => $this->input->post('branches'),

                'yearofestablism' => $this->input->post('yearofestablism'),
                'organisationtype' => $this->input->post('organisationtype'),
                'lstno' => $this->input->post('lstno'),
                'cstno' => $this->input->post('cstno'),
                'registrationno' => $this->input->post('registrationno'),
                'vatno' => $this->input->post('vatno'),
                'panno' => $this->input->post('panno'),
                'servicetaxno' => $this->input->post('servicetaxno'),
                'tanno' => $this->input->post('tanno'),
                'pfno' => $this->input->post('pfno'),
                'esisno' => $this->input->post('esisno'),
                'officeregistrationno' => $this->input->post('officeregistrationno'),
                'exceptiontax' => $this->input->post('exceptiontax'),
                'otherexceptiontax' => $this->input->post('otherexceptiontax'),
                'bank_benificialname' => $this->input->post('bank_benificialname'),
                'bank_benificialaccno' => $this->input->post('bank_benificialaccno'),
                'bank_benificialbankname' => $this->input->post('bank_benificialbankname'),
                'bank_benificialbranchname' => $this->input->post('bank_benificialbranchname'),
                'bank_benificialaddress' => $this->input->post('bank_benificialaddress'),
                'bank_benificialifsc' => $this->input->post('bank_benificialifsc'),
                'bank_benificialswiftcode' => $this->input->post('bank_benificialswiftcode'),
                'bank_benificialibanno' => $this->input->post('bank_benificialibanno'),
                'intermidiatebankname' => $this->input->post('intermidiatebankname'),
                'intermidiatebankaddress' => $this->input->post('intermidiatebankaddress'),

                'intermidiatebankswiftcode' => $this->input->post('intermidiatebankswiftcode'),
                'bank_ecs' => $this->input->post('bank_ecs'),
                'copypancart' => $pancart,
                'copyservicetax' => $servicetax,
                'copytan' => $tan,
                'copyaddressproof' => $addressproof,

                'username' => $this->session->userdata['pemailid'],
                'password' => base64_encode($this->input->post('password')),

                'countryid' => $data['branch']->countryid,
                'stateid' => $data['branch']->stateid,
                'cityid' => $data['branch']->cityid,
                'branch_id' => $data['branch']->branch_id,
                'locationid' => $data['branch']->branch_location,
                'bank_creditamount' => '',
                'bank_agentcommision' => '',


                'type' => 'web',
            );

            $path = api_url() . 'partnerapi/partnerregister/format/json/';
            $data['reg'] = curlpost($parameterreg, $path);

        header("location:agentform6");
        }

        $this->load->view("helper/header",$data);
        $this->load->view("helper/topbar",$data);
        $this->load->view("agent/form5",$data);
        $this->load->view("helper/footer");

    }

    public function agentform6()
    {

        if($this->input->post('Submit')=='Submit')
        {




            $configUpload['upload_path']    = 'assets/admin/images/partner';              #the folder placed in the root of project
            $configUpload['allowed_types']  = 'gif|jpg|png|bmp|jpeg';       #allowed types description
            $configUpload['max_size']       = '0';                          #max size
            $configUpload['max_width']      = '0';                          #max width
            $configUpload['max_height']     = '0';                          #max height
            $configUpload['encrypt_name']   = true;                         #encrypt name of the uploaded file
            $this->load->library('upload', $configUpload);

            $this->upload->do_upload('copypancart');
           // echo $this->upload->display_errors();

            $pancart=($this->upload->data('copypancart')['file_name']);

            
            $this->upload->do_upload('copyservicetax');
            $servicetax=($this->upload->data('copyservicetax')['file_name']);
            $this->upload->do_upload('copytan');
            $tan=($this->upload->data('copytan')['file_name']);
            $this->upload->do_upload('copyaddressproof');
           $addressproof=($this->upload->data('copyaddressproof')['file_name']);




            $parameterreg = array(
                'act_mode' => 'insertpartnerstep6',
                'title' => '',
                'firstname' => '',
                'lastname' => '',
                'row_id' => $this->session->userdata['pid'],
                'logintype' => $this->input->post('logintype'),
                'emailstatus' => $this->input->post('emailstatus'),
                'emailcurrency' => $this->input->post('emailcurrency'),
                'emailcountry' => $this->input->post('emailcountry'),

                'agencyname' => $this->input->post('agencyname'),
                'office_address' => $this->input->post('office_address'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'pincode' => $this->input->post('pincode'),
                'telephone' => $this->input->post('telephone'),
                'mobile' => '',
                'website' => $this->input->post('website'),
                'fax' => $this->input->post('fax'),
                'email' => $this->session->userdata['pemailid'],
                'primarycontact_name' => $this->input->post('primarycontact_name'),
                'primarycontact_email_mobile' => $this->input->post('primarycontact_email_mobile'),
                'primarycontact_telephone' => $this->input->post('primarycontact_telephone'),
                'secondrycontact_name' => $this->input->post('secondrycontact_name'),
                'secondrycontact_email_phone' => $this->input->post('secondrycontact_email_phone'),
                'secondrycontact_telephone' => $this->input->post('secondrycontact_telephone'),
                'management_executives_name' => $this->input->post('management_executives_name'),
                'management_executives_email_phone' => $this->input->post('management_executives_email_phone'),
                'management_executives_telephone' => $this->input->post('management_executives_telephone	'),
                'branches' => $this->input->post('branches'),

                'yearofestablism' => $this->input->post('yearofestablism'),
                'organisationtype' => $this->input->post('organisationtype'),
                'lstno' => $this->input->post('lstno'),
                'cstno' => $this->input->post('cstno'),
                'registrationno' => $this->input->post('registrationno'),
                'vatno' => $this->input->post('vatno'),
                'panno' => $this->input->post('panno'),
                'servicetaxno' => $this->input->post('servicetaxno'),
                'tanno' => $this->input->post('tanno'),
                'pfno' => $this->input->post('pfno'),
                'esisno' => $this->input->post('esisno'),
                'officeregistrationno' => $this->input->post('officeregistrationno'),
                'exceptiontax' => $this->input->post('exceptiontax'),
                'otherexceptiontax' => $this->input->post('otherexceptiontax'),
                'bank_benificialname' => $this->input->post('bank_benificialname'),
                'bank_benificialaccno' => $this->input->post('bank_benificialaccno'),
                'bank_benificialbankname' => $this->input->post('bank_benificialbankname'),
                'bank_benificialbranchname' => $this->input->post('bank_benificialbranchname'),
                'bank_benificialaddress' => $this->input->post('bank_benificialaddress'),
                'bank_benificialifsc' => $this->input->post('bank_benificialifsc'),
                'bank_benificialswiftcode' => $this->input->post('bank_benificialswiftcode'),
                'bank_benificialibanno' => $this->input->post('bank_benificialibanno'),
                'intermidiatebankname' => $this->input->post('intermidiatebankname'),
                'intermidiatebankaddress' => $this->input->post('intermidiatebankaddress'),

                'intermidiatebankswiftcode' => $this->input->post('intermidiatebankswiftcode'),
                'bank_ecs' => $this->input->post('bank_ecs'),
                'copypancart' => $pancart,
                'copyservicetax' => $servicetax,
                'copytan' => $tan,
                'copyaddressproof' => $addressproof,

                'username' => $this->session->userdata['pemailid'],
                'password' => base64_encode($this->input->post('password')),

                'countryid' => $data['branch']->countryid,
                'stateid' => $data['branch']->stateid,
                'cityid' => $data['branch']->cityid,
                'branch_id' => $data['branch']->branch_id,
                'locationid' => $data['branch']->branch_location,
                'bank_creditamount' => '',
                'bank_agentcommision' => '',


                'type' => 'web',
            );

            $path = api_url() . 'partnerapi/partnerregister/format/json/';
            $data['reg'] = curlpost($parameterreg, $path);

            header("location:partnerlogin");
        }


        $this->load->view("helper/header",$data);
        $this->load->view("helper/topbar",$data);
        $this->load->view("agent/form6",$data);
        $this->load->view("helper/footer");

    }
    public function agentform7()
    {
        $this->load->view("helper/header",$data);
        $this->load->view("helper/topbar",$data);
        $this->load->view("agent/form7",$data);
        $this->load->view("helper/footer");

    }

public function partnerlogout(){

    $this->session->sess_destroy();
    redirect(base_url()."partnerlogin");
}
  //partnerlogin  page
public function partnerlogin(){
    if ($this->session->userdata['ppid'] > 0) {
    redirect(base_url()."home");
  }
  else{
    $parameterseo=array('act_mode'=>'viewseotags','rowid'=>19);
     $data['seotags']=$this->supper_admin->call_procedureRow('proc_siteconfig',$parameterseo);

  if($this->input->post('submit')=='LOGIN'){

      $parameter=array(
          'act_mode' =>'partnerlogincorporate',
         'pusername' =>$this->input->post('emailmob'),
          'ppassword'=>base64_encode($this->input->post('pwd')),
         'type'=>'web',
          );

      $parameterofficer=array(
          'act_mode' =>'partnerloginofficer',
         'pusername' =>$this->input->post('emailmob'),
          'ppassword'=>base64_encode($this->input->post('pwd')),
         'type'=>'web',
          );

      $path=api_url().'partnerapi/partnerlogin/format/json/';

      $responsecorporate=curlpost($parameter,$path);
      $responseofficer=curlpost($parameterofficer,$path);


      if($responsecorporate->scalar!='Something Went Wrong' && !empty($responsecorporate) && isset($responsecorporate))
         {



if($responsecorporate->agentprofilecomplete!=0)
{
 $data = array(
     'ppid' => $responsecorporate->agent_id,
     'pemailid' => $responsecorporate->agent_username,
     'pid' => $responsecorporate->agent_id,
     'utype' => "partner",


);
$this->session->set_userdata($data);

   redirect(base_url()."partnerdashboard");
}
else
{
    redirect(base_url()."agentform1");
}

         }

elseif($responseofficer->scalar!='Something Went Wrong' && !empty($responseofficer) && isset($responseofficer))
         {

         $data = array(
             'ppid' => $responseofficer->agent_id,
   'pemailid' => $responseofficer->agent_username,
    'pid' => $responseofficer->agent_id,
             'utype' => "partner",

);
$this->session->set_userdata($data);


             if($responseofficer->agentprofilecomplete!=0)
             {redirect(base_url()."partnerdashboard");}
             else
             {
                 redirect(base_url()."agentform1");
             }
        }
else
{
   $this->session->set_flashdata('message', 'Invalid login details');
}

    }

      $siteurl= base_url();
      $parameterbranch=array(
          'act_mode' =>'selectbranch',
          'weburl' =>$siteurl,
          'type'=>'web',

      );

      $path=api_url().'selectsiteurl/branch/format/json/';
      $data['branch']=curlpost($parameterbranch,$path);

      $this->load->view("helper/header",$data);
      $this->load->view("helper/topbar",$data);
 $this->load->view("partnerlogin",$data);
     $this->load->view("helper/footer");
  }
}




//partnerregister update   page
public function agentregistrationupdate(){
    if ($this->session->userdata['ppid'] > 0) {
        redirect(base_url()."home");
    }
  else{
    $parameterseo=array('act_mode'=>'viewseotags','rowid'=>14);
     $data['seotags']=$this->supper_admin->call_procedureRow('proc_siteconfig',$parameterseo);

  if($this->input->post('submit')=='submit'){

    //  p($_POST);




      $siteurl= base_url();
      $parameterbranch=array(
          'act_mode' =>'selectbranch',
          'weburl' =>$siteurl,
          'type'=>'web',

      );

      $path=api_url().'selectsiteurl/branch/format/json/';
      $data['branch']=curlpost($parameterbranch,$path);











      $configUpload['upload_path']    = 'assets/admin/images/partner';              #the folder placed in the root of project
      $configUpload['allowed_types']  = 'gif|jpg|png|bmp|jpeg';       #allowed types description
      $configUpload['max_size']       = '0';                          #max size
      $configUpload['max_width']      = '0';                          #max width
      $configUpload['max_height']     = '0';                          #max height
      $configUpload['encrypt_name']   = true;                         #encrypt name of the uploaded file
      $this->load->library('upload', $configUpload);

      $this->upload->do_upload('copypancart');
      $this->upload->do_upload('copyservicetax');
      $this->upload->do_upload('copytan');
      $this->upload->do_upload('copyaddressproof');
      $pancart=($this->upload->data('copypancart')['file_name']);
      $servicetax=($this->upload->data('copyservicetax')['file_name']);
      $tan=($this->upload->data('copytan')['file_name']);
      $addressproof=($this->upload->data('copyaddressproof')['file_name']);






      $parameterregsel=array(
          'act_mode' =>'selectpartneremail',
           'row_id' =>'',
          'logintype' =>'',
          'agencyname' =>'',
          'office_address' =>'',
          'city' =>'',
          'state' =>'',
          'pincode' =>'',
          'telephone' =>'',
          'mobile' =>'',
          'fax' =>'',
          'website' =>'',
          'email' =>'',
          'primarycontact_name' =>'',
          'primarycontact_email_mobile' =>'',
          'primarycontact_telephone' =>'',
          'secondrycontact_name' =>'',
          'secondrycontact_email_phone' =>'',
          'secondrycontact_telephone' =>'',
          'management_executives_name' =>'',
          'management_executives_email_phone' =>'',
          'management_executives_telephone' =>'',
          'branches' =>'',

          'yearofestablism' =>'',
          'organisationtype' =>'',
          'lstno' =>'',
          'cstno' =>'',
          'registrationno' =>'',
          'vatno' =>'',
          'panno' =>'',
          'servicetaxno' =>'',
          'tanno' =>'',
          'pfno' =>'',
          'esisno' =>'',
          'officeregistrationno' =>'',
          'exceptiontax' =>'',
          'otherexceptiontax' =>'',
          'bank_benificialname' =>'',
          'bank_benificialaccno' =>'',
          'bank_benificialbankname' =>'',
          'bank_benificialbranchname' =>'',
          'bank_benificialaddress' =>'',
          'bank_benificialifsc' =>'',
          'bank_benificialswiftcode' =>'',
          'bank_benificialibanno' =>'',
          'intermidiatebankname' =>'',
          'intermidiatebankaddress' =>'',

          'intermidiatebankswiftcode' =>'',
          'bank_ecs' =>'',
          'copypancart' => '',
          'copyservicetax' =>'',
          'copytan' =>'',
          'copyaddressproof' =>'',
          'emailstatus' =>'',
          'emailcurrency' =>'',
          'emailcountry' =>'',
          'username' =>$this->input->post('username'),
          'password' =>'',
          'countryid' => $data['branch']->countryid,
          'stateid' => $data['branch']->stateid,
          'cityid' => $data['branch']->cityid,
          'branch_id' => $data['branch']->branch_id,
          'locationid' => $data['branch']->branch_location,
            'bank_creditamount' =>'',
              'bank_agentcommision' =>'',
          'type'=>'web',
      );

      $pathsel=api_url().'partnerapi/partnerregister/format/json/';
      $data['regsel']=curlpost($parameterregsel,$pathsel);


if($data['regsel']->scalar!='Something Went Wrong')
{
   $data['mesg']="Email Id Already Register";
}
else {











    $parameterreg = array(
        'act_mode' => 'insertpartner',
         'row_id' =>'',
        'logintype' => $this->input->post('logintype'),
        'agencyname' => $this->input->post('agencyname'),
        'office_address' => $this->input->post('office_address'),
        'city' => $this->input->post('city'),
        'state' => $this->input->post('state'),
        'pincode' => $this->input->post('pincode'),
        'telephone' => $this->input->post('telephone'),
        'mobile' => $this->input->post('mobile'),
        'fax' => $this->input->post('fax'),
        'website' => $this->input->post('website'),
        'email' => $this->input->post('email'),
        'primarycontact_name' => $this->input->post('primarycontact_name'),
        'primarycontact_email_mobile' => $this->input->post('primarycontact_email_mobile'),
        'primarycontact_telephone' => $this->input->post('primarycontact_telephone'),
        'secondrycontact_name' => $this->input->post('secondrycontact_name'),
        'secondrycontact_email_phone' => $this->input->post('secondrycontact_email_phone'),
        'secondrycontact_telephone' => $this->input->post('secondrycontact_telephone'),
        'management_executives_name' => $this->input->post('management_executives_name'),
        'management_executives_email_phone' => $this->input->post('management_executives_email_phone'),
        'management_executives_telephone' => $this->input->post('management_executives_telephone	'),
        'branches' => $this->input->post('branches'),

        'yearofestablism' => $this->input->post('yearofestablism'),
        'organisationtype' => $this->input->post('organisationtype'),
        'lstno' => $this->input->post('lstno'),
        'cstno' => $this->input->post('cstno'),
        'registrationno' => $this->input->post('registrationno'),
        'vatno' => $this->input->post('vatno'),
        'panno' => $this->input->post('panno'),
        'servicetaxno' => $this->input->post('servicetaxno'),
        'tanno' => $this->input->post('tanno'),
        'pfno' => $this->input->post('pfno'),
        'esisno' => $this->input->post('esisno'),
        'officeregistrationno' => $this->input->post('officeregistrationno'),
        'exceptiontax' => $this->input->post('exceptiontax'),
        'otherexceptiontax' => $this->input->post('otherexceptiontax'),
        'bank_benificialname' => $this->input->post('bank_benificialname'),
        'bank_benificialaccno' => $this->input->post('bank_benificialaccno'),
        'bank_benificialbankname' => $this->input->post('bank_benificialbankname'),
        'bank_benificialbranchname' => $this->input->post('bank_benificialbranchname'),
        'bank_benificialaddress' => $this->input->post('bank_benificialaddress'),
        'bank_benificialifsc' => $this->input->post('bank_benificialifsc'),
        'bank_benificialswiftcode' => $this->input->post('bank_benificialswiftcode'),
        'bank_benificialibanno' => $this->input->post('bank_benificialibanno'),
        'intermidiatebankname' => $this->input->post('intermidiatebankname'),
        'intermidiatebankaddress' => $this->input->post('intermidiatebankaddress'),

        'intermidiatebankswiftcode' => $this->input->post('intermidiatebankswiftcode'),
        'bank_ecs' => $this->input->post('bank_ecs'),
        'copypancart' => $pancart,
        'copyservicetax' => $servicetax,
        'copytan' => $tan,
        'copyaddressproof' => $addressproof,
        'emailstatus' => $this->input->post('emailstatus'),
        'emailcurrency' => $this->input->post('emailcurrency'),
        'emailcountry' => $this->input->post('emailcountry'),
        'username' => $this->input->post('username'),
        'password' => base64_encode($this->input->post('password')),

        'countryid' => $data['branch']->countryid,
        'stateid' => $data['branch']->stateid,
        'cityid' => $data['branch']->cityid,
        'branch_id' => $data['branch']->branch_id,
        'locationid' => $data['branch']->branch_location,
          'bank_creditamount' =>'',
              'bank_agentcommision' =>'',


        'type' => 'web',
    );

    $path = api_url() . 'partnerapi/partnerregister/format/json/';
    $data['reg'] = curlpost($parameterreg, $path);
     $data['mesg'] ="Agent Added Sucessfully";
}}
      $siteurl= base_url();
      $parameterbranch=array(
          'act_mode' =>'selectbranch',
          'weburl' =>$siteurl,
          'type'=>'web',

      );

      $path=api_url().'selectsiteurl/branch/format/json/';
      $data['branch']=curlpost($parameterbranch,$path);
      $this->load->view("helper/header",$data);
      $this->load->view("helper/topbar",$data);
 $this->load->view("agentregistrationupdate",$data);
     $this->load->view("helper/footer");
  }
}



    //partnerregister update   page
    public function agentregistration(){
        if ($this->session->userdata['ppid'] > 0) {
            redirect(base_url()."home");
        }
        else{
            $parameterseo=array('act_mode'=>'viewseotags','rowid'=>14);
            $data['seotags']=$this->supper_admin->call_procedureRow('proc_siteconfig',$parameterseo);

            if($this->input->post('submit')=='Register'){
                 $siteurl= base_url();
                  $parameterbranch=array(
                    'act_mode' =>'selectbranch',
                    'weburl' =>$siteurl,
                    'type'=>'web',

                );

                $path=api_url().'selectsiteurl/branch/format/json/';
                $data['branch']=curlpost($parameterbranch,$path);
                    $parameterregsel=array(
                    'act_mode' =>'selectpartneremail',
                        'title' =>'',
                        'firstname' =>'',
                        'lastname' =>'',
                    'row_id' =>'',
                    'logintype' =>'',
                    'agencyname' =>'',
                    'office_address' =>'',
                    'city' =>'',
                    'state' =>'',
                    'pincode' =>'',
                    'telephone' =>'',
                    'mobile' =>'',
                    'fax' =>'',
                    'website' =>'',
                    'email' =>'',
                    'primarycontact_name' =>'',
                    'primarycontact_email_mobile' =>'',
                    'primarycontact_telephone' =>'',
                    'secondrycontact_name' =>'',
                    'secondrycontact_email_phone' =>'',
                    'secondrycontact_telephone' =>'',
                    'management_executives_name' =>'',
                    'management_executives_email_phone' =>'',
                    'management_executives_telephone' =>'',
                    'branches' =>'',

                    'yearofestablism' =>'',
                    'organisationtype' =>'',
                    'lstno' =>'',
                    'cstno' =>'',
                    'registrationno' =>'',
                    'vatno' =>'',
                    'panno' =>'',
                    'servicetaxno' =>'',
                    'tanno' =>'',
                    'pfno' =>'',
                    'esisno' =>'',
                    'officeregistrationno' =>'',
                    'exceptiontax' =>'',
                    'otherexceptiontax' =>'',
                    'bank_benificialname' =>'',
                    'bank_benificialaccno' =>'',
                    'bank_benificialbankname' =>'',
                    'bank_benificialbranchname' =>'',
                    'bank_benificialaddress' =>'',
                    'bank_benificialifsc' =>'',
                    'bank_benificialswiftcode' =>'',
                    'bank_benificialibanno' =>'',
                    'intermidiatebankname' =>'',
                    'intermidiatebankaddress' =>'',

                    'intermidiatebankswiftcode' =>'',
                    'bank_ecs' =>'',
                    'copypancart' => '',
                    'copyservicetax' =>'',
                    'copytan' =>'',
                    'copyaddressproof' =>'',
                    'emailstatus' =>'',
                    'emailcurrency' =>'',
                    'emailcountry' =>'',
                    'username' =>$this->input->post('email'),
                    'password' =>'',
                    'countryid' => $data['branch']->countryid,
                    'stateid' => $data['branch']->stateid,
                    'cityid' => $data['branch']->cityid,
                    'branch_id' => $data['branch']->branch_id,
                    'locationid' => $data['branch']->branch_location,
                    'bank_creditamount' =>'',
                    'bank_agentcommision' =>'',
                    'type'=>'web',
                );

                $pathsel=api_url().'partnerapi/partnerregister/format/json/';
                $data['regsel']=curlpost($parameterregsel,$pathsel);


                if($data['regsel']->scalar!='Something Went Wrong')
                {
                    $data['mesg']="Email Id Already Register";
                }
                else {


                    $siteurl= base_url();
                    $parameterbranch=array(
                        'act_mode' =>'selectbranch',
                        'weburl' =>$siteurl,
                        'type'=>'web',

                    );

                    $path=api_url().'selectsiteurl/branch/format/json/';
                    $data['branch']=curlpost($parameterbranch,$path);



                    //select banner images
                    $parameterbanner = array(
                        'act_mode' => 'selectbannerimages',
                        'branchid' => $data['branch']->branch_id,
                        'type' => 'web',

                    );

                    $path = api_url() . 'selectsiteurl/banner/format/json/';
                    $data['banner'] = curlpost($parameterbanner, $path);





                    $message_new = '<table width="90%" style="line-height: 28px; font-family: sans-serif;"   >
<tr><td>Congratulations ' . $this->input->post("title").". " . ucfirst($this->input->post("first_name")).'&nbsp;'. $this->input->post("last_name") . ',</td></tr>

<tr><td>You have been successfully registered with  ' . $data['banner']->bannerimage_top3 . '.!</td></tr>

<tr><td>
You have been successfully registered with ' . $data['banner']->bannerimage_top3 . '. You can use ' . $this->input->post("email") . ' as your Login ID to access your ' . $data['banner']->bannerimage_top3 . ' Account online. 
</td></tr>
<tr><td>Please do not disclose/share your password to anyone.</td></tr>

<tr><td>Should you have any queries, please feel free to write to us on '.$data['banner']->bannerimage_branch_email.' or call us on '.$data['banner']->bannerimage_branch_contact.'.</td></tr>
<tr><td>Yours sincerely,<br>
'.$data['banner']->bannerimage_top3.' Team</td></tr>
</table>
';


                    $from_email = $data['banner']->bannerimage_from ;
                    $to_email = $this->input->post('email');
                    //Load email library
                    $this->load->library('email');
                    $this->email->from($from_email,  $data['banner']->bannerimage_top3 );
                    $this->email->reply_to($from_email,  $data['banner']->bannerimage_top3 );
                    $this->email->to($to_email);
                    $this->email->subject('' . $data['banner']->bannerimage_top3 . ' - Your Registration Details');
                    $this->email->message($message_new);
                    //Send mail
                    $this->email->send();
                  //  pend($this->email->print_debugger());


                    $parameterreg = array(
                        'act_mode' => 'insertpartnerstartstage',
                        'title' =>$this->input->post('title'),
                        'firstname' =>$this->input->post('first_name'),
                        'lastname' =>$this->input->post('last_name'),
                        'row_id' =>'',
                        'logintype' => '',
                        'agencyname' =>'',
                        'office_address' => '',
                        'city' => '',
                        'state' => '',
                        'pincode' => '',
                        'telephone' => '',
                        'mobile' => $this->input->post('mobile_number'),
                        'fax' => '',
                        'website' => '',
                        'email' => $this->input->post('email'),
                        'primarycontact_name' => '',
                        'primarycontact_email_mobile' => '',
                        'primarycontact_telephone' => '',
                        'secondrycontact_name' => '',
                        'secondrycontact_email_phone' => '',
                        'secondrycontact_telephone' => '',
                        'management_executives_name' => '',
                        'management_executives_email_phone' => '',
                        'management_executives_telephone' => '',
                        'branches' =>'',

                        'yearofestablism' =>'',
                        'organisationtype' =>'',
                        'lstno' => '',
                        'cstno' => '',
                        'registrationno' => '',
                        'vatno' =>'',
                        'panno' => '',
                        'servicetaxno' => '',
                        'tanno' => '',
                        'pfno' => '',
                        'esisno' => '',
                        'officeregistrationno' => '',
                        'exceptiontax' => '',
                        'otherexceptiontax' => '',
                        'bank_benificialname' => '',
                        'bank_benificialaccno' => '',
                        'bank_benificialbankname' => '',
                        'bank_benificialbranchname' => '',
                        'bank_benificialaddress' => '',
                        'bank_benificialifsc' => '',
                        'bank_benificialswiftcode' => '',
                        'bank_benificialibanno' => '',
                        'intermidiatebankname' => '',
                        'intermidiatebankaddress' => '',
                        'intermidiatebankswiftcode' => '',
                        'bank_ecs' => '',
                        'copypancart' =>'',
                        'copyservicetax' => '',
                        'copytan' => '',
                        'copyaddressproof' => '',
                        'emailstatus' => '',
                        'emailcurrency' => '',
                        'emailcountry' => '',
                        'username' => $this->input->post('email'),
                        'password' => base64_encode($this->input->post('password')),

                        'countryid' => $data['branch']->countryid,
                        'stateid' => $data['branch']->stateid,
                        'cityid' => $data['branch']->cityid,
                        'branch_id' => $data['branch']->branch_id,
                        'locationid' => $data['branch']->branch_location,
                        'bank_creditamount' =>'',
                        'bank_agentcommision' =>'',


                        'type' => 'web',
                    );

                    $path = api_url() . 'partnerapi/partnerregister/format/json/';
                    $data['reg'] = curlpost($parameterreg, $path);


                    $data = array(

                        'pemailid' => $this->input->post('email'),
                        'pid' => $data['reg']->lid,

                    );
    $this->session->set_userdata($data);


                 header("location:agentform1");
                  //  $data['mesg'] ="Agent Added Sucessfully";
                }}
            $siteurl= base_url();
            $parameterbranch=array(
                'act_mode' =>'selectbranch',
                'weburl' =>$siteurl,
                'type'=>'web',

            );

            $path=api_url().'selectsiteurl/branch/format/json/';
            $data['branch']=curlpost($parameterbranch,$path);
            $this->load->view("helper/header",$data);
            $this->load->view("helper/topbar",$data);
            $this->load->view("agentregistration",$data);
            $this->load->view("helper/footer");
        }
    }


    public function agentforget(){
        if ($this->session->userdata['pid'] > 0) {
            redirect(base_url()."home");
        }
else {
    $siteurl= base_url();
    $parameterbranch=array(
        'act_mode' =>'selectbranch',
        'weburl' =>$siteurl,
        'type'=>'web',
    );

    $path=api_url().'selectsiteurl/branch/format/json/';
    $data['branch']=curlpost($parameterbranch,$path);

    $new_pass = base64_encode(rand(000000,999999));


    $parameterupdatepassword_newpass = array(
        'act_mode' => 'forgetpassword_newpassagent',
        'email' => $this->input->post('emailmob'),
        'Param3' => $new_pass,
        'type' => 'web'
    );
//p($parameterupdatepassword_newpass);
    $path_newpass = api_url() . 'userapi/updatepassword/format/json/';
    $data['updateacc_new'] = curlpost($parameterupdatepassword_newpass, $path_newpass);

    $parameterupdatepassword = array(
        'act_mode' => 'forgetpasswordagent',
        'email' => $this->input->post('emailmob'),
        'type' => 'web'
    );

    $path = api_url() . 'userapi/updatepassword/format/json/';
    $data['updateacc'] = curlpost($parameterupdatepassword, $path);




    //pend($data['updateacc_new']);

    $message_new = '<table width="90%" style="line-height: 28px; font-family: sans-serif;"   >
<tr><td>Congratulations ' . $data['updateacc']->agent_agencyname.',</td></tr>
<tr><td>Greetings from ' . $data['banner']->bannerimage_top3 . '..!</td></tr>
<tr><td>You have received this communication in response to your request for your ' . $data['banner']->bannerimage_top3 . '. Account password to be sent to you via e-mail and sms.</td></tr>
<tr><td>Your temporary password is: '.base64_decode($data['updateacc']->agent_password).' </td></tr>
<tr><td>Please use the password exactly as it appears above.</td></tr>
<tr><td>We request you immediately change your password by logging on to '.base_url().'</td></tr>
<tr><td>Should you have any queries, please feel free to write to us on '.$data['banner']->bannerimage_branch_email.' or call us on '.$data['banner']->bannerimage_branch_contact.'.</td></tr>
<tr><td>Yours sincerely,<br>
'.$data['banner']->bannerimage_top3.' Team</td></tr>
</table>
';


    $from_email = $data['banner']->bannerimage_from ;
    $to_email = $this->input->post('emailmob');
    //Load email library
    $this->load->library('email');
    $this->email->from($from_email,  $data['banner']->bannerimage_top3 );
    $this->email->reply_to($from_email,  $data['banner']->bannerimage_top3 );
    $this->email->subject('' . $data['banner']->bannerimage_top3 . ' - Your Request For Password Reset');
    $this->email->message($message_new);
    //Send mail
    $this->email->send();
    //pend($this->email->print_debugger());


    $this->load->view("helper/header",$data);
    $this->load->view("helper/topbar",$data);
    $this->load->view("agentforget",$data);
    $this->load->view("helper/footer");
}
}

//update partner profile   updatepartnerrprofile
public function updatepartnerrprofile(){


    if ($this->input->post('submit')) {


        $configUpload['upload_path'] = 'assets/admin/images/partner';              #the folder placed in the root of project
        $configUpload['allowed_types'] = 'gif|jpg|png|bmp|jpeg';       #allowed types description
        $configUpload['max_size'] = '0';                          #max size
        $configUpload['max_width'] = '0';                          #max width
        $configUpload['max_height'] = '0';                          #max height
        $configUpload['encrypt_name'] = true;                         #encrypt name of the uploaded file
        $this->load->library('upload', $configUpload);
        if ($_FILES['copypancart']['name'] != '') {
            $this->upload->do_upload('copypancart');
            $pancart = ($this->upload->data('copypancart')['file_name']);
        } else {
            $pancart = $_POST['copypancartdata'];
        }
        if ($_FILES['copyservicetax']['name'] != '') {
            $this->upload->do_upload('copyservicetax');
            $servicetax = ($this->upload->data('copyservicetax')['file_name']);
        } else {
            $servicetax = $_POST['copyservicetaxdata'];
        }
        if ($_FILES['copytan']['name'] != '') {
            $this->upload->do_upload('copytan');
            $tan = ($this->upload->data('copytan')['file_name']);
        }

        else {
            $tan = $_POST['copytandata'];
        }


        if ($_FILES['copyaddressproof']['name'] != '') {
            $this->upload->do_upload('copyaddressproof');
            $addressproof = ($this->upload->data('copyaddressproof')['file_name']);
        } else {
            $addressproof = $_POST['copyaddressproofdata'];
        }


        $parameterregupdate = array(
            'act_mode' => 'updatepartner',
            'title' =>$this->input->post('title'),
            'firstname' =>$this->input->post('firstname'),
            'lastname' =>$this->input->post('lastname'),
            'row_id' => $this->session->userdata['ppid'],


            'logintype' => $this->input->post('logintype'),
            'agencyname' => $this->input->post('agencyname'),
            'office_address' => $this->input->post('office_address'),
            'city' => $this->input->post('city'),
            'state' => $this->input->post('state'),
            'pincode' => $this->input->post('pincode'),
            'telephone' => $this->input->post('telephone'),
            'mobile' => $this->input->post('mobile'),
            'fax' => $this->input->post('fax'),
            'website' => $this->input->post('website'),
            'email' => $this->input->post('email'),
            'primarycontact_name' => $this->input->post('primarycontact_name'),
            'primarycontact_email_mobile' => $this->input->post('primarycontact_email_mobile'),
            'primarycontact_telephone' => $this->input->post('primarycontact_telephone'),
            'secondrycontact_name' => $this->input->post('secondrycontact_name'),
            'secondrycontact_email_phone' => $this->input->post('secondrycontact_email_phone'),
            'secondrycontact_telephone' => $this->input->post('secondrycontact_telephone'),
            'management_executives_name' => $this->input->post('management_executives_name'),
            'management_executives_email_phone' => $this->input->post('management_executives_email_phone'),
            'management_executives_telephone' => $this->input->post('management_executives_telephone	'),
            'branches' => $this->input->post('branches'),

            'yearofestablism' => $this->input->post('yearofestablism'),
            'organisationtype' => $this->input->post('organisationtype'),
            'lstno' => $this->input->post('lstno'),
            'cstno' => $this->input->post('cstno'),
            'registrationno' => $this->input->post('registrationno'),
            'vatno' => $this->input->post('vatno'),
            'panno' => $this->input->post('panno'),
            'servicetaxno' => $this->input->post('servicetaxno'),
            'tanno' => $this->input->post('tanno'),
            'pfno' => $this->input->post('pfno'),
            'esisno' => $this->input->post('esisno'),
            'officeregistrationno' => $this->input->post('officeregistrationno'),
            'exceptiontax' => $this->input->post('exceptiontax'),
            'otherexceptiontax' => $this->input->post('otherexceptiontax'),
            'bank_benificialname' => $this->input->post('bank_benificialname'),
            'bank_benificialaccno' => $this->input->post('bank_benificialaccno'),
            'bank_benificialbankname' => $this->input->post('bank_benificialbankname'),
            'bank_benificialbranchname' => $this->input->post('bank_benificialbranchname'),
            'bank_benificialaddress' => $this->input->post('bank_benificialaddress'),
            'bank_benificialifsc' => $this->input->post('bank_benificialifsc'),
            'bank_benificialswiftcode' => $this->input->post('bank_benificialswiftcode'),
            'bank_benificialibanno' => $this->input->post('bank_benificialibanno'),
            'intermidiatebankname' => $this->input->post('intermidiatebankname'),
            'intermidiatebankaddress' => $this->input->post('intermidiatebankaddress'),

            'intermidiatebankswiftcode' => $this->input->post('intermidiatebankswiftcode'),
            'bank_ecs' => $this->input->post('bank_ecs'),
            'copypancart' => $pancart,
            'copyservicetax' => $servicetax,
            'copytan' => $tan,
            'copyaddressproof' => $addressproof,
            'emailstatus' => $this->input->post('emailstatus'),
            'emailcurrency' => $this->input->post('emailcurrency'),
            'emailcountry' => $this->input->post('emailcountry'),
            'username' => $this->input->post('username'),
            'password' => base64_encode($this->input->post('password')),

            'countryid' => '',
            'stateid' => '',
            'cityid' => '',
            'branch_id' => $this->input->post('username'),
            'locationid' => '',
            'bank_creditamount' => $this->input->post('bank_creditamount'),
            'bank_agentcommision' => $this->input->post('bank_agentcommision'),
            'bank_agentcommision' => $this->input->post('bank_agentcommision'),
        );

        $response['regselupdate'] = $this->supper_admin->call_procedure('proc_partnerregister_v', $parameterregupdate);

        $data['emsg'] = "Edit Sucessfully";

       // header("Location:agent");

    }



    $parametersel = array(


        'act_mode' => 'selectpartnerdata',
        'title' =>'',
        'firstname' =>'',
        'lastname' =>'',
        'row_id' => $this->session->userdata['ppid'],

        'logintype' => '',
        'agencyname' => '',
        'office_address' => '',
        'city' => '',
        'state' => '',
        'pincode' => '',
        'telephone' => '',
        'mobile' => '',
        'fax' => '',
        'website' => '',
        'email' => '',
        'primarycontact_name' => '',
        'primarycontact_email_mobile' => '',
        'primarycontact_telephone' => '',
        'secondrycontact_name' => '',
        'secondrycontact_email_phone' => '',
        'secondrycontact_telephone' => '',
        'management_executives_name' => '',
        'management_executives_email_phone' => '',
        'management_executives_telephone' => '',
        'branches' => '',

        'yearofestablism' => '',
        'organisationtype' => '',
        'lstno' => '',
        'cstno' => '',
        'registrationno' => '',
        'vatno' => '',
        'panno' => '',
        'servicetaxno' => '',
        'tanno' => '',
        'pfno' => '',
        'esisno' => '',
        'officeregistrationno' => '',
        'exceptiontax' => '',
        'otherexceptiontax' => '',
        'bank_benificialname' => '',
        'bank_benificialaccno' => '',
        'bank_benificialbankname' => '',
        'bank_benificialbranchname' => '',
        'bank_benificialaddress' => '',
        'bank_benificialifsc' => '',
        'bank_benificialswiftcode' => '',
        'bank_benificialibanno' => '',
        'intermidiatebankname' => '',
        'intermidiatebankaddress' => '',

        'intermidiatebankswiftcode' => '',
        'bank_ecs' => '',
        'copypancart' => '',
        'copyservicetax' => '',
        'copytan' => '',
        'copyaddressproof' => '',
        'emailstatus' => '',
        'emailcurrency' => '',
        'emailcountry' => '',
        'username' => '',
        'password' => '',

        'countryid' => '',
        'stateid' => '',
        'cityid' => '',
        'branch_id' => '',
        'locationid' => '',
        'bank_creditamount' => '',
        'bank_agentcommision' => '',
       
    );

 $data['agentviewwdata'] = $this->supper_admin->call_procedurerow('proc_partnerregister_v', $parametersel);

    //$path = api_url() . 'partnerapi/partnerregister/format/json/';
    //$data['agentviewwdata'] = curlpost($parametersel, $path);



    $this->load->view("agent/helper/header",$data);
    $this->load->view("agent/helper/leftbar",$data);
    $this->load->view("agent/updatepartnerrprofile",$data);
    $this->load->view("agent/helper/footer");
}

    public function changepartnerpassword(){


        if ($this->input->post('submit') == 'changePswrd') {
            $this->form_validation->set_rules('currentpassword', 'Enter Current  password ', 'required');
            $this->form_validation->set_rules('newpassword', 'Enter  password ', 'required');
          $this->form_validation->set_rules('confirmpassword', 'Enter Confirm  password ', 'required');


            if ($this->input->post('confirmpassword') != $this->input->post('newpassword')) {

                $this->form_validation->set_rules('confirmpassword', ' Password Not Match', 'required');
            }



            if ($this->form_validation->run() != FALSE) {

                $parametercheckpasswrd = array(
                    'act_mode' => 'partncheckpassword',
                    'userid' => $this->session->userdata['ppid'],
                    'currentpassword' => $this->input->post('currentpassword'),
                    'newpassword' => '',
                    'confirmpassword' => '',

                    'type' => 'web',
                );

                $path = api_url() . 'userapi/usercheckchangepassword/format/json/';
                $data['checkpasswrd'] = curlpost($parametercheckpasswrd, $path);
              
//echo (base64_decode($data['checkpasswrd']->agent_password)."---".$this->input->post('currentpassword');
                if ((base64_decode($data['checkpasswrd']->agent_password)) == $this->input->post('currentpassword')) {

                    $parameter = array(
                        'act_mode' => 'partnchangepassword',
                        'userid' => $this->session->userdata['ppid'],
                        'currentpassword' => $this->input->post('currentpassword'),
                        'newpassword' => $this->input->post('newpassword'),
                        'confirmpassword' => $this->input->post('confirmpassword'),
                        'type' => 'web',
                    );

                    $path = api_url() . 'userapi/userchangepassword/format/json/';
                    $data['userlog'] = curlpost($parameter, $path);

                    $data['message'] = "Password Change Sucessfully";

                } else {


                    $data['message'] = " Invalid Password ";

                }

            }
        }



        $this->load->view("agent/helper/header",$data);
        $this->load->view("agent/helper/leftbar",$data);
        $this->load->view("agent/changepartnerpassword",$data);
        $this->load->view("agent/helper/footer");
    }
    public function order_sess()
    {

        if ($_POST['Clear'] == 'Show All Records') {
            $this->session->unset_userdata('order_filter');
        }


        $a = explode(":", $this->input->post('filter_date_session'));
        $b = explode(" ", $this->input->post('filter_date_session'));
//p($a);
//p($b);

        if ($b['1'] == 'PM') {
            $session_time = $a['0'] + 12;
        } else {
            $session_time = $a['0'];
        }
        //p($_POST);
        //$this->session->unset_userdata('order_filter');
        $a = $this->input->post('filter_branch');
        $b = $this->input->post('filter_status');
        $c = $this->input->post('filter_tim');
        $d = $this->input->post('filter_sta');

        $branchids = implode(',', $a);
        $filterstatus = implode(',', $b);
        $filtertime = implode(',', $c);
        $filtersta = implode(',', $d);
        $array = array('branchids' => $branchids,
            'filter_name' => $this->input->post('filter_name'),
            'filter_email' => $this->input->post('filter_email'),
            'filter_ticket' => $this->input->post('filter_ticket'),
            'filter_mobile' => $this->input->post('filter_mobile'),
            'filter_payment' => $this->input->post('filter_payment'),
            'filter_date_booking_from' => $this->input->post('filter_date_booking_from'),
            'filter_date_booking_to' => $this->input->post('filter_date_booking_to'),
            'filter_date_session_from' => $this->input->post('filter_date_session_from'),
            'filter_date_session_to' => $this->input->post('filter_date_session_to'),
            'filter_status' => $filterstatus,
            'filter_date_printed_from' => $this->input->post('filter_date_printed_from'),
            'filter_date_printed_to' => $this->input->post('filter_date_printed_to'),
            'filter_date_ticket' => $this->input->post('filter_date_ticket'),
            'filter_date_session' => $session_time,
            'filter_tim' => $filtertime,
            'filter_sta' => $filtersta
        );
        $this->session->set_userdata('order_filter', $array);
        redirect('order');
    }

    public function order(){
        if ($this->session->userdata('order_filter')) {
            //p($this->session->userdata('order_filter'));
            $parameter1 = array('act_mode' => 'S_vieworder',
                'Param1' => $this->session->userdata('order_filter')['branchids'],
                'Param2' => $this->session->userdata('order_filter')['filter_name'],
                'Param3' => $this->session->userdata('order_filter')['filter_email'],
                'Param4' => $this->session->userdata('order_filter')['filter_ticket'],
                'Param5' => $this->session->userdata('order_filter')['filter_mobile'],
                'Param6' => $this->session->userdata('order_filter')['filter_payment'],
                'Param7' => $this->session->userdata('order_filter')['filter_date_booking_from'],
                'Param8' => $this->session->userdata('order_filter')['filter_date_booking_to'],
                'Param9' => $this->session->userdata('order_filter')['filter_date_session_from'],                  'Param10' => $this->session->userdata('order_filter')['filter_date_session_to'],                  'Param11' => $this->session->userdata('order_filter')['filter_status'],
                'Param12' => $this->session->userdata('order_filter')['filter_date_printed_from'],
                'Param13' => $this->session->userdata('order_filter')['filter_date_printed_to'],
                'Param14' => $this->session->userdata('order_filter')['filter_date_ticket'],
                'Param15' => $this->session->userdata('order_filter')['filter_date_session'],
                'Param16' => $this->session->userdata('order_filter')['filter_tim'],
                'Param17' => $this->session->userdata('order_filter')['filter_sta'],
                'Param18' => '',
                'Param19' => $this->session->userdata('ppid'));
            foreach ($parameter1 as $key => $val) {
                if ($parameter1[$key] == '') {
                    $parameter1[$key] = -1;
                }
                //echo $parameter1[$key];
            }

            // p($parameter1);
            $response['vieww_order'] = $this->supper_admin->call_procedure('proc_order_filter_v', $parameter1);
         //p($response['vieww_order']);

          
        } else {

            $parameter1 = array('act_mode' => 'S_vieworder_new',
                'Param1' => '',
                'Param2' => '',
                'Param3' => '',
                'Param4' => '',
                'Param5' => '',
                'Param6' => '',
                'Param7' => '',
                'Param8' => '',
                'Param9' => $this->session->userdata('ppid'));
            //pend($parameter);
            $response['vieww_order'] = $this->supper_admin->call_procedure('proc_order_partner', $parameter1);
        }

        $parameter_time = array('act_mode' => 'branch_list_for_filter',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',
            'Param16' => '',
            'Param17' => '',
            'Param18' => '',
            'Param19' => $this->session->userdata('ppid'));
        $response['vieww_time'] = $this->supper_admin->call_procedure('proc_order_filter_v', $parameter_time);

        $parameter_status = array('act_mode' => 'status_list_for_filter',
            'Param1' => '',
            'Param2' => '',
            'Param3' => '',
            'Param4' => '',
            'Param5' => '',
            'Param6' => '',
            'Param7' => '',
            'Param8' => '',
            'Param9' => '',
            'Param10' => '',
            'Param11' => '',
            'Param12' => '',
            'Param13' => '',
            'Param14' => '',
            'Param15' => '',
            'Param16' => '',
            'Param17' => '',
            'Param18' => '',
            'Param19' => '');



        $response['vieww_status'] = $this->supper_admin->call_procedure('proc_order_filter_v', $parameter_status);




        $this->load->view("agent/helper/header",$response);
        $this->load->view("agent/helper/leftbar",$response);
        $this->load->view("agent/order",$response);
        $this->load->view("agent/helper/footer");
    }

    public function updateamount(){

        if ($this->session->userdata['ppid'] < 0) {
            redirect(base_url()."home");
        }
        else {
            $siteurl= base_url();
            $parameterbranch=array(
                'act_mode' =>'selectbranch',
                'weburl' =>$siteurl,
                'type'=>'web',
            );

            $path=api_url().'selectsiteurl/branch/format/json/';
            $data['branch']=curlpost($parameterbranch,$path);


            $userdisplay = array('act_mode'=>'select_partner',
                'userid'=>$this->session->userdata['ppid'],
                'type'=>'web',

            );
            $path3 = api_url()."Ordersucess/selectuser/format/json/";
            $data['memuser']= curlpost($userdisplay,$path3);

            $walletselect = array('act_mode' => 'selectwalletdata',
                'orderid' => '',
                'tracking_id' => '',
                'order_status' => '',
                'status_message' => '',
                'paymentmode' => '',
                'paymentstatus' => '',
                'ordersucesmail' => '',
                'ordermailstatus' => '',
                'agent_id' => '',
                'cred_debet' => '',
                'amount' => '',
                'bankagentcommision' => '',
                'type' => 'web',

            );

            $path = api_url() . "Ordersucess/selectwalletdata/format/json/";
            $data['orderwalletselect'] = curlpost($walletselect, $path);













            $this->load->view("agent/helper/header",$data);
            $this->load->view("agent/helper/leftbar",$data);
            $this->load->view("agent/updateamount",$data);
            $this->load->view("agent/helper/footer");
        }
    }



    public function mywallet(){


        $orderwalletselect = array('act_mode' => 'orderwalletselectagentamount',
            'orderid' => '',
            'tracking_id' => '',
            'order_status' => '',
            'status_message' => '',
            'paymentmode' => '',
            'paymentstatus' => '',
            'ordersucesmail' =>'',
            'ordermailstatus' => '',
            'agent_id' => $this->session->userdata['ppid'],
            'cred_debet' =>'',
            'amount' => '',
            'bankagentcommision' => '',
            'type' => 'web',

        );

        $path = api_url() . "Ordersucess/selectwalletdatadispall/format/json/";
        $data['orderwalletselect'] = curlpost($orderwalletselect, $path);

        $this->load->view("agent/helper/header",$data);
        $this->load->view("agent/helper/leftbar",$data);
        $this->load->view("agent/mywallet",$data);
        $this->load->view("agent/helper/footer");
    }
}//end of class
?>
