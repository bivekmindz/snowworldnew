
    
    <section class="step_payment">
    <div class="container main-container">

      
      <div id="default-screen" class="content-part"><span id="updateFullPage">
        <div class="container margin_60">
  <h3 class="text-center text-uppercase">Activities</h3>
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-3">
                        <section class="box_cat_wp">
                        <div class="box_cat cat-hover">
                            <a href="#" class="cat-overlay">
                            <h2>Snow Park</h2>
                            <p>With the vast area of Snow Play Area, we make sure you have enough areas to have a perfect snow fight with your friends &amp; family.</p>
                            </a>
                            <div class="cat-img">
                                <img SRC="<?php echo base_url();?>img/activities/snow-park.jpg" alt="Snow Park">
                            </div>
                        </div>
                        </section>
                    </div>



                
                    <div class="col-lg-4 col-md-6 col-sm-3">
                        <section class="box_cat_wp">
                        <div class="box_cat cat-hover">
                            <a href="#" class="cat-overlay">
                            <h2>I - Slip (Ski Slide)</h2>
                            <p>Get on the pair of Skis and experience the ultimate transformation of freedom on our 100 feet slope.</p>
                            </a>
                            <div class="cat-img">
                                <img SRC="<?php echo base_url();?>img/activities/i-slip.jpg" alt="​​I - Slip (Ski Slide)">
                            </div>
                        </div>
                        </section>
                    </div>
                
                    <div class="col-lg-4 col-md-6 col-sm-3">
                        <section class="box_cat_wp">
                        <div class="box_cat cat-hover">
                            <a href="#" class="cat-overlay">
                            <h2>Multi-coaster (Toboggan)</h2>
                            <p>Get on the wildest steep slope to go down using various forms of sledges and toboggans.</p>
                            </a>
                            <div class="cat-img">
                                <img SRC="<?php echo base_url();?>img/activities/toboggan.jpg" alt="Multi-coaster (Toboggan)">
                            </div>
                        </div>
                        </section>
                    </div>   
                
                    <div class="col-lg-4 col-md-6 col-sm-3">
                        <section class="box_cat_wp">
                        <div class="box_cat cat-hover">
                            <a href="#" class="cat-overlay">
                            <h2>Icy Luge (Bob Sled)</h2>
                            <p>With number of twist and turns on a continuous downhill trajectory, it is one of the most fun things to do in Ski India. It's certain to bring smiles to the faces of both young and old.</p>
                            </a>
                            <div class="cat-img">
                                <img SRC="<?php echo base_url();?>img/activities/icy-luge.jpg" alt="Icy Luge (Bob Sled)">
                            </div>
                        </div>
                        </section>
                    </div>   
                
                    <div class="col-lg-4 col-md-6 col-sm-3">
                        <section class="box_cat_wp">
                        <div class="box_cat cat-hover">
                            <a href="#" class="cat-overlay">
                            <h2>Snow Play Area</h2>
                            <p>A snowball in the face is surely the perfect beginning to a lasting friendship So get on your toes & have the best time of your life while you  enjoy Ski India’s vast Snow Play Area.</p>
                            </a>
                            <div class="cat-img">
                                <img SRC="<?php echo base_url();?>img/activities/snow-play-area.jpg" alt="Snow Play Area">
                            </div>
                        </div>
                        </section>
                    </div>   
                
                    <div class="col-lg-4 col-md-6 col-sm-3">
                        <section class="box_cat_wp">
                        <div class="box_cat cat-hover">
                            <a href="#" class="cat-overlay">
                            <h2>Snow Fall</h2>
                            <p>Get Set Snow. Feel Different, Feel Magical. Come enjoy the magic of SnowFall at -10 degree Celcius</p>
                            </a>
                            <div class="cat-img">
                                <img SRC="<?php echo base_url();?>img/activities/snow-fall.jpg" alt="Snow Fall">
                            </div>
                        </div>
                        </section>
                    </div>   
                
                    <div class="col-lg-4 col-md-6 col-sm-3">
                        <section class="box_cat_wp">
                        <div class="box_cat cat-hover">
                            <a href="#" class="cat-overlay">
                            <h2>Drift on Ice (Skating)</h2>
                            <p>Glide with the greatest of ease around the capital's indoor ice rink. Whether you dance on ice or just scramble on, it is one of the most magical location u'll see  at Ski India.</p>
                            </a>
                            <div class="cat-img">
                                <img SRC="<?php echo base_url();?>img/activities/drift-on-ice.jpg" alt="Drift on Ice (Skating)">
                            </div>
                        </div>
                        </section>
                    </div>   
                
                    <div class="col-lg-4 col-md-6 col-sm-3">
                        <section class="box_cat_wp">
                        <div class="box_cat cat-hover">
                            <a href="#" class="cat-overlay">
                            <h2>Snow Runner (Sledging)</h2>
                            <p>Try your skill, ride the snow vehicle and master the art of driving.</p>
                            </a>
                            <div class="cat-img">
                                <img SRC="<?php echo base_url();?>img/activities/snow-runner.jpg" alt="Snow Runner (Sledging)">
                            </div>
                        </div>
                        </section>
                    </div>   
                
                    <div class="col-lg-4 col-md-6 col-sm-3">
                        <section class="box_cat_wp">
                        <div class="box_cat cat-hover">
                            <a href="#" class="cat-overlay">
                            <h2>Frozen Odyssey (Caves Journey)</h2>
                            <p>A Dark maze with emphatic lighting takes guest through an expedition to meet species of our planet.</p>
                            </a>
                            <div class="cat-img">
                                <img SRC="<?php echo base_url();?>img/activities/frozen-odyssey.jpg" alt="Frozen Odyssey (Caves Journey)">
                            </div>
                        </div>
                        </section>
                    </div>   
                
                    <div class="col-lg-4 col-md-6 col-sm-3">
                        <section class="box_cat_wp">
                        <div class="box_cat cat-hover">
                            <a href="#" class="cat-overlay">
                            <h2>Igloo</h2>
                            <p>Ever wondered how it feels to be an eskimo in  an igloo . Come experience it  & while you are at it , don’t forget to Star Gaze.</p>
                            </a>
                            <div class="cat-img">
                                <img SRC="img/activities/igloo.jpg" alt="Igloo">
                            </div>
                        </div>
                        </section>
                    </div>   
                
                    <div class="col-lg-4 col-md-6 col-sm-3">
                        <section class="box_cat_wp">
                        <div class="box_cat cat-hover">
                            <a href="#" class="cat-overlay">
                            <h2>Snow Mobile</h2>
                            <p>In the deep stuff, you’ve got to be able to react. To pick your way up the steeps, or sidehill through the trees, you need a light sled with enough power and snap to throw it around.</p>
                            </a>
                            <div class="cat-img">
                                <img SRC="img/activities/snow-mobile.jpg" alt="Snow Mobile">
                            </div>
                        </div>
                        </section>
                    </div>   
                
                    <div class="col-lg-4 col-md-6 col-sm-3">
                        <section class="box_cat_wp">
                        <div class="box_cat cat-hover">
                            <a href="#" class="cat-overlay">
                            <h2>Snow Scooter</h2>
                            <p>If you aren’t quite ready to shred the pow on a pair of skis or a snowboard, the  Snow scooter will allow  you to work up your confidence out on the snow.</p>
                            </a>
                            <div class="cat-img">
                                <img SRC="img/activities/snow-scooter.jpg" alt="Snow Scooter">
                            </div>
                        </div>
                        </section>
                    </div>    
                
                    <div class="col-lg-4 col-md-6 col-sm-3">
                        <section class="box_cat_wp">
                        <div class="box_cat cat-hover">
                            <a href="#" class="cat-overlay">
                            <h2>Spinn (Snow Carousel)</h2>
                            <p>Go round and bumping on the snow tubes with our &quot;Merry go round&quot;.</p>
                            </a>
                            <div class="cat-img">
                                <img SRC="img/activities/spinn.jpg" alt="Spinn (Snow Carousel)">
                            </div>
                        </div>
                        </section>
                    </div>    
                
                    <div class="col-lg-4 col-md-6 col-sm-3">
                        <section class="box_cat_wp">
                        <div class="box_cat cat-hover">
                            <a href="#" class="cat-overlay">
                            <h2>Waiting Area</h2>
                            <p>Wear our Parka jackets & gloves  and get ready to a  Snowmazing time.</p>
                            </a>
                            <div class="cat-img">
                                <img SRC="img/activities/waiting-area.jpg" alt="Waiting Area">
                            </div>
                        </div>
                        </section>
                    </div>          
                    
                    
                </div><!-- End row -->
            </div><!-- End col-lg-9 -->            
        </div><!-- End row -->
    </div><!-- End Container -->
   <script SRC="js/jquery-1.11.2.min.js"></script>
<script SRC="js/common_scripts_min.js"></script>
<script SRC="js/functions.js"></script>
<script SRC="assets/validate.js"></script>

<!-- Specifi scripts -->
<script SRC="js/mosaic.1.0.1.js"></script>
<script type="text/javascript">
      jQuery(function ($) {
          $('.cat-hover').mosaic({
              animation: 'slide'    //fade or slide
          });
      });
</script>
      </div>
    </div>
    
    </section>
    
   <script SRC="js/jquery-1.11.2.min.js"></script>
<script SRC="js/common_scripts_min.js"></script>
<script SRC="js/functions.js"></script>
<script SRC="assets/validate.js"></script>

<!-- Specifi scripts -->
<script SRC="js/mosaic.1.0.1.js"></script>
<script type="text/javascript">
      jQuery(function ($) {
          $('.cat-hover').mosaic({
              animation: 'slide'    //fade or slide
          });
      });
</script>