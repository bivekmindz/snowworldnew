
    
   <div class="container margin_30">
      <h3>Terms & Conditions</h3>
      <div class="row">         
          <div class="col-md-12">
              <ol class="text-justify" style="margin-left: -15px;">
                  <li>Admission ticket permits admission for one individual and needs to be retained at the respective time slot.</li>
                  <li>Children's below 2 years are exempted from ticket charges.</li>
                  <li>Total Duration: 1 Hour&#8203; 30 mins&#8203; ( Snow Room : &#8203;1 hour&#8203; + Reception : &#8203;30 mins &#8203;mins)</li>
                  <li>Parka Jackets, Gloves, Hood and Boots will be provided with each admission.</li>
                  <li>Tickets once purchased will not be RETURNED / REFUNDED back.</li>
                  <li>Children's below 10 years and senior citizens need to be accompanied by guardian.</li>
                  <li>All accessories are not of accurate sizes and are subject to availability.</li>
                  <li>Eatables and beverages of any kind are not allowed into the snow area.</li>
                  <li>The following articles are strictly prohibited: Eatables (Including Gutkha, Pan-Masala, Chewing Gum, Chocolate, Chips), Knife, and Cigarette, Lighter, Match-Box, Firearms and all kind of inflammable objects.</li>
                  <li>Smoking is an offence &amp; is strictly prohibited inside Ski India premises and will be fined according to Management’s discretion.</li>
                  <li>All images &amp; terminology used in all communication is for creative, descriptive and representation purpose only.</li>
                  <li>Ticket package, rates and governing conditions are subject to change without notice.</li>
                  <li>Management reserves the right to cancel any session due to unforeseen circumstances.</li>
                  <li>The Management shall not be liable for any losses/damages arising with such suspension.</li>
                  <li>No two offers can be clubbed together.</li>
                  <li>Any purposeful act to damage the environment, surrounding or assets will be fined according to Management’s discretion.</li>
                  <li>On the purchase of this ticket the holder confirms that he/she has read, understood and agreed to all the terms and conditions in the theme park and voluntarily assumes all the risks.</li>
                  <li>Booking on this website authorizes Ski India to contact the customer, on the Phone Number provided, for any booking related communication.</li>
              </ol>
            </div><!-- End col-md-8 -->
           
        </div><!-- End row -->
    </div><!-- End Container -->