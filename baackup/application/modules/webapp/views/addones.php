<style>

    .incere {
        width: 28px;
        height: 28px;
        background: linear-gradient(#fff,#f9f9f9);
        display: inline-block;
        border: 1px solid #c2c2c2;
        cursor: pointer;
        font-size: 16px;
        border-radius: 50%;
        padding-top: 1px;
        line-height: 1;
    }

    .addOnsList input {width: 28px;
        text-align: center;
        padding: 0px !important;
        margin-top: 0px !important;
        border-radius: 50%;
        border: 1px solid #cdcdcd; }
</style>



<section class="step_payment">


    <div class="container main-container">


        <div id="default-screen" class="content-part"><span id="updateFullPage">


          <section class="row block" id="ticketsAddOnesBlock">
            <div id="searchResult"><span id="ticketsAddOnsOpen"></span><span id="step1Done">
                <div id="ticketsAddOnsDone" class="ticketsAddOnsDone doneBlock aboveAll">
                  <div class="stepHeader stepDone stepDoneHere relative row" id="ticketsAddOnsInfo">
                    <p class="fRight subHead">Session Date:<?php

                        $date_array = explode("/",$this->session->userdata('txtDepartDate')); // split the array
                        $var_day = $date_array[0]; //day seqment
                        $var_month = $date_array[1]; //month segment
                        $var_year = $date_array[2]; //year segment
                        $new_date_format = strtotime("$var_year-$var_month-$var_day"); // join them together

                        $input = ("$var_year$var_month$var_day");

                        echo date(' jS F Y', $new_date_format); ?>&nbsp;|&nbsp;Session Time: <?php

                        $desttype=$this->session->userdata('destinationType');


                        if($timeslotses->timeslot_to>12)
                        {
                            $apmvar="PM";
                            $timeslotses->timeslot_to= ($timeslotses->timeslot_to-12);
                        }elseif($desttype==12) {
                            $apmvar="PM";
                        }
                        else {
                            $apmvar="AM";
                        }if($desttype>12)
                        { $desttype= ($desttype-12);
                            $apmvar1="PM";}elseif($desttype==12) {
                            $apmvar1="PM";
                        }else {

                            $apmvar1="AM";
                        }



                        echo $desttype; ?>:<?php  echo $timeslotses->timeslot_minfrom; ?> <?php echo $apmvar1; ?> - <?php  echo $timeslotses->timeslot_to; ?>:<?php  echo $timeslotses->timeslot_minto; ?> <?php echo $apmvar; ?>    | No. of Visiters: <?php echo $this->session->userdata('packageqtyval'); ?></p>
<form id="j_idt135" name="j_idt135" method="post" action="" enctype="application/x-www-form-urlencoded">
<input type="hidden" name="j_idt135" value="j_idt135">
<a id="j_idt135:step1Edit" href="packages" class="ui-commandlink ui-widget edit" onclick="">
                        <span class="icon-pencil"></span></a>
<input type="hidden" name="javax.faces.ViewState" value="4914754771058361795:4671725937336393998" autocomplete="off"></form>
                    <h2 class="relative">
                      <span class="steps"><span class="stepNum">Step
                          1</span></span>
                    </h2>
                    <p class="steps-title title1">Search
</p>
                  </div>

                </div></span>

            </div>
          </section>



                      <section class="row block" id="ticketsAddOnesBlock">
            <div id="searchResult"><span id="ticketsAddOnsOpen"></span><span id="step1Done">
                <div id="ticketsAddOnsDone" class="ticketsAddOnsDone doneBlock aboveAll">
                  <div class="stepHeader stepDone stepDoneHere relative row" id="ticketsAddOnsInfo">
                    <p class="fRight subHead">Ticket Cost :

<span><i class="icon-rupee" aria-hidden="true"></i></span>

           <span id="mainnumber">   <?php  echo $this->session->userdata('packagepriceval')."/-"; ?></span> </p>
<form id="j_idt135" name="j_idt135" method="post" action="" enctype="application/x-www-form-urlencoded">
<input type="hidden" name="j_idt135" value="j_idt135">
<a id="j_idt135:step1Edit" href="packagesstep" class="ui-commandlink ui-widget edit" onclick="">
                        <span class="icon-pencil"></span></a>
<input type="hidden" name="javax.faces.ViewState" value="4914754771058361795:4671725937336393998" autocomplete="off"></form>
                    <h2 class="relative">
                      <span class="steps"><span class="stepNum">Step
                          2</span></span>
                    </h2>
                    <p class="steps-title title2">Package
</p>
                  </div>

                </div></span>

            </div>
          </section>


         <form name="formName" id="formName" action="" method="post">
          <section class="row block" id="ticketsAddOnesBlock">
            <div id="searchResult"><span id="ticketsAddOnsOpen">
                  <div id="ticketsAddOnsOpen" class="openBlock aboveAll"><span id="step1Header">
                      <div class="stepHeader stepOpenHere row stepOpen">
                        <p class="fRight subHead">Which addons would you like to add?</p>
                        <h2 class="relative">
                          <span class="steps"><span class="stepNum">Step
                              3</span></span>
                        </h2>
                        <p class="steps-title title3">Addons</p>
                      </div></span><span id="refreshStep1Panel"><span id="step1AddonDisp">

<div id="AddOnsBlock" class="infoBlock relative addOnsBlock">
  <div class="ticketsWithAddOns">
    <div class="row ticketdescription mart20">
      <div class="col-xs-1">      <?php  $subtotal=0;
          if($this->session->userdata('packageimageval')!='') {
              ?>  <img style="max-width: 65px;" src="assets/admin/images/<?php echo $this->session->userdata('packageimageval'); ?>">
          <?php } else { ?>
              <img style="max-width: 65px;" src="img/1463739380583.jpeg">
          <?php } ?>
          <input type="hidden" name="cartpackageimage" value="<?php echo $this->session->userdata('packageimageval'); ?>">
      </div>
      <div class="col-xs-4">
        <div class="ticket-type "><?php echo $this->session->userdata('packagenameval'); ?></div>
      </div>
       <input type="hidden" name="cartpackagename" value="<?php echo $this->session->userdata('packagenameval'); ?>">


      <div class="col-xs-3 txtcenter visitorsCount mobile-visitorsCount">
        No. Of Visitors: <span><?php echo $this->session->userdata('packageqtyval'); ?></span>
      </div>
      <div class="col-xs-4 txtright ">

      <p class="reupespps2 mobile-visitorsCount2">
       <span><i class="icon-rupee" aria-hidden="true"></i></span><?php echo $this->session->userdata('packagepriceval');  $subtotal+=$this->session->userdata('packagepriceval'); ?>
        </p>

       <input type="hidden" name="cartpackageprice" value="<?php echo $this->session->userdata('packagepriceval'); ?>">
      </div>
    </div>
      <?php  if($addonsdisplay->scalar!='Something Went Wrong') {   foreach ($addonsdisplay as $key => $value) {  ?>
          <div class="row ticketdescription mart20">
      <div class="col-xs-1">
           <?php
           if($value['addonimage']!='') {
               ?>  <img style="max-width: 52px;height:52px;" src="assets/admin/images/<?php echo $value['addonimage']; ?>">
           <?php } else { ?>
               <img style="max-width: 52px; height:52px;" src="img/1463739380583.jpeg">
           <?php } ?>
          <input type="hidden" name="cartaddoneimage[]" value="<?php echo $value['addonimage']; ?>">

<input type="hidden" name="cartaddoneid[]" value="<?php echo $value['addonid']; ?>">



      </div>
      <div class="col-xs-4">
        <div class="ticket-type "><?php echo $value['addonname']; ?>
            <input type="hidden" name="cartaddonname[]" value="<?php echo $value['addonname']; ?>">

        </div>
      </div>
      <div class="col-xs-3 txtcenter visitorsCount">
        Qty: <span><?php echo $value['addonqty']; ?></span>
        <input type="hidden" name="cartaddonqty[]" value="<?php echo $value['addonqty']; ?>">
      </div>
      <div class="col-xs-4 txtright">
       <input type="hidden" name="cartaddonprice[]" value="<?php echo $value['addonprice']; ?>">
     <form name="formName" id="formName" action="" method="post">
      <p class="reupespps"><span><i class="icon-rupee" aria-hidden="true"></i></span><?php echo $value['addonprice'];$subtotal+= $value['addonprice'];?> <span class="close_but">
<button id="" name="delete" class=""  type="submit" role="button" aria-disabled="false" value = "<?php echo $value['tempaddon']; ?>" onclick="this.form.submit();"><i class="fa fa-close" aria-hidden="true"></i></button>
</span>
</p></form>





        <div  >




     </div>
     </div>


    </div>
      <?php }} ?>




      <span id="selectedAddonPanel">


      <div class="row ticketTotalPrice">
        <div class="col-xs-6" style="padding-left: 15px;">
          <div class="ticket-type">Total</div>
        </div>
        <div class="col-xs-6 txtright" style="padding-right:6px;">


        <p class="reupespps2 reupespps2ski" >
       <span><i class="icon-rupee" aria-hidden="true"></i></span><?php echo $subtotal; ?><input type="hidden" name="cartsubtotal" value="<?php echo $subtotal; ?>">      </p>



        </div>
      </div></span>

    <div class="row">
      <div class="col-xs-12 txtcenter">
        <div class="continue">


<button id="" name="submit" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only btn btn-custom btn-prime" type="submit" role="button" aria-disabled="false" value="cartses"><span class="ui-button-text ui-c">CONTINUE</span></button>


        </div>
      </div>
    </div>

  </div>
</div>
</form>
<div id="addOnsListBlock" class="infoBlock relative addOnsListBlock">
  <div class="widget-container ticketDetailsList ">
    <div class="widget-header">
      <div class="widget-title">Would you like some Addons?</div>
    </div>
    <div class="widget-content">
      <div class="addOnsListContainer">
      <form action="" method="post" >
 <?php  $countaddone = 0; foreach ($addonscart as $key => $value) {
     if($value=='Something Went Wrong') {echo "No Records";} else {

         if($this->session->userdata('addonquantitys')!='') {


             $addonqtyval= (explode(",",$this->session->userdata('addonquantitys')));
             $addonqtyval[$countaddone]=$addonqtyval[$countaddone];

         }
         else
         {
             $addonqtyval[$countaddone]=1;
         }

         ?>



         <script type="text/javascript">
function incrementValue<?php echo $value['addon_id']; ?>()
{
    //  alert('number<?php echo $value['addon_id']; ?>');
    var a = parseInt($("#mainnumbervalues<?php echo $value['addon_id']; ?>").val());
    var b = parseInt($("#mainnumber<?php echo $value['addon_id']; ?>").html());
    var c = parseInt($("#mainnumbervalue<?php echo $value['addon_id']; ?>").val());

    var value = parseInt(document.getElementById('number<?php echo $value['addon_id']; ?>').value);
    value = isNaN(value) ? 0 : value;
    if(value<19){
        value++;
        b = b+a;
        c = value*a;

        $("#mainnumbervalue<?php echo $value['addon_id']; ?>").val(c);

        $("#mainnumber<?php echo $value['addon_id']; ?>").html(b);
        document.getElementById('number<?php echo $value['addon_id']; ?>').value = value;

    }
}
function decrementValue<?php echo $value['addon_id']; ?>()
{

    var a = parseInt($("#mainnumbervalues<?php echo $value['addon_id']; ?>").val());
    var b = parseInt($("#mainnumber<?php echo $value['addon_id']; ?>").html());
    var c = parseInt($("#mainnumbervalue<?php echo $value['addon_id']; ?>").val());
    var value = parseInt(document.getElementById('number<?php echo $value['addon_id']; ?>').value);
    value = isNaN(value) ? 0 : value;
    if(value>1){
        value--;
        b = b-a;
        c = value*a;

        $("#mainnumbervalue<?php echo $value['addon_id']; ?>").val(c);
        $("#mainnumber<?php echo $value['addon_id']; ?>").html(b);

        document.getElementById('number<?php echo $value['addon_id']; ?>').value = value;
        document.cookie = "myJavascriptVar = " + value
    }

}
</script>          <div class="row addOnsList">
            <div class="col-xs-1 width1001-ski">
            <span data-target="#addOns_62000000003395889" data-parent="#addOnsListBlock" data-toggle="collapse" class="view-info">
 <img src="<?php if($value['addon_image']!='') { ?>assets/admin/images/<?php echo $value['addon_image']; ?><?php } else { ?>img/info.png<?php } ?>">

                                  <input type="hidden" name="addonimage[]" value="<?php echo $value['addon_image']; ?>">
</span>

<input type="hidden" name="addonsid[]" value="<?php echo $value['addon_id']; ?>,<?php echo $addonqtyval[$countaddone];?>">

            </div>
            <div class="col-xs-2 width1001-ski">
              <span class="ticket-type1"> <?php echo $value['addon_name']; ?>
                  <input type="hidden" name="addonname[]" value="<?php echo $value['addon_name']; ?>"></span>

            </div>



            <div class="col-xs-2 txtcenter width1001-ski">
                        <div class="product_text">

                        <h4>Qty</h4>
                               <div>
<input type="button" class="incere " onclick="decrementValue<?php echo $value['addon_id']; ?>()" value="-">


 <div class="test-incre">
 <input type="text" readonly id="number<?php echo $value['addon_id']; ?>" class="inen" name="addonqty[]" maxlength="2" value="<?php echo $addonqtyval[$countaddone]; ?>"></div>
  &nbsp;<input type="button" class="incere abouste-ski" onclick="incrementValue<?php echo $value['addon_id']; ?>()" value="+"></div>


                                  </div>

                        </div>





            <div class="col-xs-3 txtright width1001-ski">

            <p class="reupespps1">
       <span><i class="icon-rupee" aria-hidden="true"></i></span><span id="mainnumber<?php echo $value['addon_id']; ?>"><?php echo $addonqtyval[$countaddone]*$value['addon_price']; ?></span>      </p>





<input type="hidden" name="addonprice[]" id="mainnumbervalue<?php echo $value['addon_id']; ?>" value="<?php echo $addonqtyval[$countaddone]*$value['addon_price']; ?>">
<input type="hidden" name="addonprice1[]" id="mainnumbervalues<?php echo $value['addon_id']; ?>" value="<?php echo $value['addon_price']; ?>">




                </label>

              <span style="float: right; margin-top: 13px;">



<input type="hidden" name="addonid[]"  value="<?php echo $value['addon_id']; ?>">


</span>





            </div>

            <div class="col-xs-2 width1001-ski">
            <div style=" padding-top: 32px;">


<button id="" name="categories1[]" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only btn btn-info btn-add"  type="submit" role="button" aria-disabled="false" value = "<?php echo $value['addon_id']; ?>" onclick="this.form.submit();"><span class="ui-button-text ui-c">Add</span></button>



<input type="hidden" name="categories1[]" value="0"/>

</div>

            </div>
              <div class="addOnsInfo collapse" id="addOns_62000000003395889">The most useful apparel to keep your feet warm in such cold temperature. Its not compulsory but necessary.</div>
          </div>
         <?php $countaddone++;}} ?>
    </form>
      </div>

    </div>
  </div>





</div>



</span></span>
        </div></span>

    </div>
</section>






<?php if($this->session->userdata('skiindia')>0)  { ?>

    <section id="paymentBlock" class="row block relative"><span id="beforePanel3">
                <div id="summaryClose" class="stepHeader stepClose row summaryClose closeBlock">
                  <p class="fRight subHead"></p>
                  <h2 class="relative">
                    <span class="steps"><span class="stepNum">Step 4</span></span>
                  </h2>
                  <p class="steps-title title4">Summary</p>
                </div></span><span id="step3open"></span>

    </section>

    <section id="paymentBlock" class="row block relative"><span id="beforePanel3">
                <div id="summaryClose" class="stepHeader stepClose row summaryClose closeBlock">
                  <p class="fRight subHead"></p>
                  <h2 class="relative">
                    <span class="steps"><span class="stepNum">Step 5</span></span>
                  </h2>
                  <p class="steps-title title5">Payment</p>
                </div></span><span id="step3open"></span>

    </section>

<?php } else { ?>





    <section id="paymentBlock" class="row block relative"><span id="beforePanel3">
                <div id="summaryClose" class="stepHeader stepClose row summaryClose closeBlock">
                  <p class="fRight subHead"></p>
                  <h2 class="relative">
                    <span class="steps"><span class="stepNum">Step 4</span></span>
                  </h2>
                  <p class="steps-title title4">Login / Register</p>
                </div></span><span id="step3open"></span>

    </section>


    <section id="paymentBlock" class="row block relative"><span id="beforePanel3">
                <div id="summaryClose" class="stepHeader stepClose row summaryClose closeBlock">
                  <p class="fRight subHead"></p>
                  <h2 class="relative">
                    <span class="steps"><span class="stepNum">Step 5</span></span>
                  </h2>
                  <p class="steps-title title5">Summary</p>
                </div></span><span id="step3open"></span>

    </section>

    <section id="paymentBlock" class="row block relative"><span id="beforePanel3">
                <div id="summaryClose" class="stepHeader stepClose row summaryClose closeBlock">
                  <p class="fRight subHead"></p>
                  <h2 class="relative">
                    <span class="steps"><span class="stepNum">Step 6</span></span>
                  </h2>
                  <p class="steps-title title6">Payment</p>
                </div></span><span id="step3open"></span>

    </section>

<?php } ?>


</span>
</div>
</div>

</section>

<!-- End Container -->
    