<script type="text/javascript">

    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }

</script>

<section class="succe">
    <div class="container">

        <div class="row">

            <div class="bg_fail1">
                <div class="col-md-12">
                    <div class="succe_cont">
                        <div id="divPrint">
                            <div class="top_heading">

                                <h1><span>THANK YOU. YOUR BOOKING IS CONFIRMED NOW</span></h1>
                            </div>



                            <div class="p_class1">

                                <p>
                                    <span>Please note : </span>Below is your booking ID for all communication with Ski India. Please save and store the ID mentioned. You will have to carry a printout of this ticket when you visit the park.
                                    The details of your booking are mentioned below and a copy of the same has also been mailed to your email address. Please carry a prinout of the e-ticket along with a photo identity proof such as driving license, voters ID or passport to the check-in counter at Ski India. Ski India representative will issue your entry-ticket at the booking counter.</p>
                            </div>


                            <div class="tab_inner_section">
                                <div class="heading_tab_inner"><h5>Booking Details</h5></div>

                                <div class="tab_inner_body full_width">
                                    <div class="payment_details_main">

                                        <div class=" col-lg-9 col-md-9 col-sm-6 col-xs-6  review_content">
                                            <div class="top_head_bar">
                                                <h4>Ticket Type: <?php   echo($orderdisplaydataval->packproductname);?></h4>
                                            </div>
                                            <div class="scifiheader"><h4>Booking ID: <?php  echo($orderdisplaydataval->ticketid);  ?></h4></div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 ">
                                            <div class="doller_left">
                                                <h2> <span><i class="icon-rupee" aria-hidden="true"></i></span><?php echo $orderdisplaydataval->packprice/$orderdisplaydataval->packpkg;  ?> </h2>

                                                <p>Per Person</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="full_width package_table_section">
                                        <div class="col-lg-6 col-md-6 border_right">
                                            <div class="payment_table_package table_pad">
                                                <table class="table ">
                                                    <tbody><tr>
                                                        <td>Booking Date</td>
                                                        <td>:</td>
                                                        <td><?php
                                                            echo date("D", strtotime($input)) . "\n";
                                                            $date_array1 = explode("-",$orderdisplaydataval->addedon ); // split the array
                                                            $var_day1 = $date_array1[2]; //day seqment
                                                            $var_month1 = $date_array1[1]; //month segment
                                                            $var_year1 = $date_array1[0]; //year segment
                                                            $new_date_format1 = strtotime("$var_year1-$var_month1-$var_day1"); // join them together

                                                            echo date(' jS F Y', $new_date_format1);
                                                            ?>(<?php

                                                            $date = '19:24:15';
                                                            echo date('h:i:s a ');
                                                            //echo date("H:i:s") . "\n"; ?>)
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Visit Date</td>
                                                        <td>:</td>
                                                        <td> <?php

                                                            $date_array = explode("/",$orderdisplaydataval->departuredate); // split the array
                                                            $var_day = $date_array[0]; //day seqment
                                                            $var_month = $date_array[1]; //month segment
                                                            $var_year = $date_array[2]; //year segment
                                                            $new_date_format = strtotime("$var_year-$var_month-$var_day"); // join them together

                                                            $input = ("$var_year$var_month$var_day");

                                                            echo date("D", strtotime($input)) . "\n";


                                                            echo date(' jS F Y', $new_date_format); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Start Time</td>
                                                        <td>:</td>
                                                        <td> <?php






                                                            $desttype=$orderdisplaydataval->departuretime;
                                                            if($timeslotses->timeslot_to>=12)
                                                            {

                                                                $apmvar="PM";

                                                                $timeslotses->timeslot_to= ($timeslotses->timeslot_to-12);
                                                            }else {

                                                                $apmvar="AM";
                                                            }


                                                            if($desttype>='12:00')
                                                            {

                                                                $apmvar="PM";

                                                                $desttype= ($desttype-'12:00');
                                                            }else {

                                                                $apmvar="AM";
                                                            }


                                                            //change_s1
                                                            echo $desttype; ?>:<?php  echo $timeslotses->timeslot_minfrom; ?><?php echo $apmvar; ?> -<?php  echo $timeslotses->timeslot_to; ?>:<?php  echo $timeslotses->timeslot_minto; ?> <?php echo $apmvar; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td><?php if($orderdisplaydataval->packpkg>1) { ?>Visitors<?php } else { ?>Visitor<?php } ?></td>
                                                        <td>:</td>
                                                        <td><?php  echo($orderdisplaydataval->packpkg );  ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Addons</td>
                                                        <td>:</td>
                                                        <td><?php
                                                            foreach($orderaddonedisplaydata as $key =>$value){
                                                                if($value!='Something Went Wrong' ){
                                                                    echo($value['addonename']."&nbsp;x&nbsp;".$value['addonevisiter']."<br>");
                                                                } else { echo "N/A";} }
                                                            ?></td>
                                                    </tr>
                                                    </tbody></table>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6">
                                            <div class="payment_table_package">
                                                <table>
                                                    <tbody><tr>
                                                        <td>Base price</td>
                                                        <td> <span><i class="icon-rupee" aria-hidden="true"></i></span><?php echo $baseprice=$orderdisplaydataval->packprice/$orderdisplaydataval->packpkg;  ?></td>
                                                    </tr>

                                                    <tr>
                                                        <td>Ticket price</td>
                                                        <td> <span><i class="icon-rupee" aria-hidden="true"></i></span><?php echo ($orderdisplaydataval->packprice); ?>




                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Addons</td>

                                                        <td><span><i class="icon-rupee" aria-hidden="true"></i></span><?php
                                                            $addonetotal=0;foreach($orderaddonedisplaydata as $key =>$value){
                                                                if($value!='Something Went Wrong' ){

                                                                    $addonetotal +=  ($value['addoneprice']);
                                                                }
                                                                else{ $addonetotal= 0;}
                                                            }  echo $addonetotal;
                                                            ?></td>
                                                    </tr>

                                                    <tr>
                                                        <td>Discount (Promocode)</td>
                                                        <td> <span><i class="icon-rupee" aria-hidden="true"></i></span> - <?php echo ($orderdisplaydataval->promocodeprice); ?>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>Internet Handling Charge</td>
                                                        <td><span><i class="icon-rupee" aria-hidden="true"></i></span> <?php echo ($orderdisplaydataval->internethandlingcharges); ?>
                                                        </td>
                                                    </tr>
                                                    <tr class="total_row">
                                                        <td>Payable Price</td>
                                                        <td>

                                                            <span><i class="icon-rupee" aria-hidden="true"></i></span> <?php echo ($orderdisplaydataval->total); ?>

                                                        </td>
                                                    </tr>
                                                    </tbody></table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="full_width total_price_row">
                                        <p>Total Ticket Price - </p>
                                        <h2> <span><i class="icon-rupee" aria-hidden="true"></i></span><?php  echo($orderdisplaydataval->total );  ?></h2>
                                    </div>
                                </div>
                            </div>



                            <div class="full_width information_section">
                                <div class="information_title scifiheader"> Customer Information </div>
                                <div class="full_width information_table_main">
                                    <div class="col-lg-6 col-md-6 border_right">
                                        <div class="payment_table_package conyy">
                                            <table class="table">
                                                <tbody><tr>
                                                    <td>Booking ID :</td>
                                                    <td><?php  echo($orderdisplaydataval->ticketid);  ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Name :</td>
                                                    <td><?php  echo($orderdisplaydataval->billing_name);  ?> </td>
                                                </tr>
                                                <tr>
                                                    <td>Contact :</td>
                                                    <td> <?php  echo($orderdisplaydataval->billing_tel);  ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Email :</td>
                                                    <td><?php  echo($orderdisplaydataval->billing_email );  ?></td>
                                                </tr>
                                                </tbody></table>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 border_right">
                                        <div class="payment_table_package conyy">
                                            <table class="table">
                                                <tbody><tr>
                                                    <td>Address :</td>
                                                    <td><?php   if(($orderdisplaydataval->billing_address)!='') {  echo $orderdisplaydataval->billing_address ; } else { echo "N/A";} ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Town/City :</td>
                                                    <td>
                                                        <?php if(($orderdisplaydataval->billing_city)!='') {  echo($orderdisplaydataval->billing_city ); } else { echo "N/A";} ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Zip Code : </td>
                                                    <td>
                                                        <?php if(($orderdisplaydataval->billing_zip)!='') {  echo($orderdisplaydataval->billing_zip ); } else { echo "N/A";} ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Country :</td>
                                                    <td>
                                                        <?php if($orderdisplaydataval->billing_country!='') {  echo($orderdisplaydataval->billing_country ); } else { echo "N/A";} ?></td>
                                                </tr>
                                                </tbody></table>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="full_width information_section">
                                <div class="information_title scifiheader"> Disclaimer </div>
                                <div class="full_width information_table_main">
                                    <div class="paymentinfo_list">
                                        <ul>
                                            <li>Kindly report at least 15 minutes prior to your session timings to issue the admission pass.</li>
                                            <li>Management is not responsible for the loss or miss of session time, if you reach late.</li>
                                            <li>Kindly make sure you read the terms, conditions, safety rules and cancellation policy.</li>
                                            <li>This booking is not transferable/refundable or used in exchange of cash.</li>
                                            <li>The said booking is considered that you agree to all the terms and conditions of the Ski India Management.</li>
                                        </ul>
                                        <p>You have now confirmed and guaranteed your booking has done by : <span><?php  echo($orderdisplaydataval->paymenttype );  ?></span></p>
                                    </div>
                                </div>
                                <div class="full_width information_table_main">
                                    <div class="booking_text t_align_c">
                                        <span>Venue Address</span>
                                        <p class="scifiheader"><?php echo ($branch->branch_add); ?>.</p>
                                    </div>
                                </div>
                                <div class="full_width t_align_c">

                                    <div id="printableArea"  style="display:none" >

                                        <?php

                                        $date_array1 = explode("-", $orderdisplaydataval->addedon ); // split the array
                                        $var_day1 = $date_array1[2]; //day seqment
                                        $var_month1 = $date_array1[1]; //month segment
                                        $var_year1 = $date_array1[0]; //year segment
                                        $new_date_format1 = strtotime("$var_year1-$var_month1-$var_day1"); // join them together

                                        $d1= date(' jS F Y', $new_date_format1);


                                      //  $date = '19:24:15 ';
                                        $d2= date('h:i:s a ');

                                        $date_array9 = explode("/",$orderdisplaydataval->departuredate); // split the array
                                        $var_day9 = $date_array9[0]; //day seqment
                                        $var_month9 = $date_array9[1]; //month segment
                                        $var_year9 = $date_array9[2]; //year segment
                                        $new_date_format = strtotime("$var_year9-$var_month9-$var_day9"); // join them together

                                        $input = ("$var_year9$var_month9$var_day9");

                                        $d3= date("D", strtotime($input)) . "\n";

  $row['date'] = trim($var_year9," ")."-".$var_month9."-".$var_day9; // this is for example only - comment out when tested
$d4= date("F j, Y", strtotime($row['date'])); 



                                        if($timeslotses->timeslot_from > 12)
                                        {
                                            $from_session_time =  ($timeslotses->timeslot_from - 12).":".$timeslotses->timeslot_minfrom.'PM' ;

                                        }
                                        else{
                                            $from_session_time = $timeslotses->timeslot_from.":".$timeslotses->timeslot_minfrom."AM";
                                        }

                                        if($data['timeslotses']->timeslot_to > 12)
                                        {
                                            $to_session_time =  ($timeslotses->timeslot_to - 12).":".$timeslotses->timeslot_minto.$orderdisplaydataval->txttod;
                                        }
                                        else{
                                            $to_session_time = $timeslotses->timeslot_to.":".$timeslotses->timeslot_minto.$orderdisplaydataval->txttod;
                                        }


                                        ?>




                                        <html>
                                        <head>
                                            <title>Skiindia Booking</title>
                                            <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                                        </head>
                                        <body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
                                        <!-- Save for Web Slices (Untitled-1) -->
                                        <table width="953" height="967" cellpadding="0" cellspacing="0"  style="border:solid 1px #37ace1;font-family:Arial, Helvetica, sans-serif;color:#555;">
                                            <tr>
                                                <td colspan="3">
                                                    <table width="953">
                                                        <tr>
                                                            <td border="0" width="150" valign="top" style="padding-top:20px;padding-left:20px;"><img src="assets/admin/images/<?php echo $banner->bannerimage_logo; ?>"  alt=""></td>
                                                            <td border="0" width="800" style="text-align:center;">
                                                                <table width="800" style="margin:20px 20px;line-height:25px;">
                                                                    <tr>
                                                                        <td style="color: #37ACE1;font-weight:700;padding:0px 0px;font-size:29px;text-align:center;">
                                                                            <strong><?php echo $banner->bannerimage_top1; ?></strong>

                                                                        </td>
                                                                    </tr>
                                                                    <tr><td style="padding:8px 0px 5px;font-size:18px;font-weight:700;"><strong><?php echo $banner->bannerimage_top2; ?> </strong></td></tr>
                                                                    <tr><td style="padding:4px 0px;font-size:22px;font-weight:700;"><strong><?php echo $banner->bannerimage_top3; ?> </strong></td></tr>
                                                                    <tr><td style="color: #444;font-weight:700;padding:0px 0px 0px;font-size:22px;"><strong><?php echo $banner->bannerimage_top4; ?> </strong></td></tr>
                                                                    <tr><td style="color: #37ACE1;font-weight:600;padding:15px 0px 0px;font-size:25px;"><strong>Booking Confirmation Details</strong></td></tr>
                                                                </table>
                                                            </td>

                                                        </tr>
                                                    </table>
                                                </td>

                                            </tr>
                                            <tr><td  style="border-top:dashed 3px #37ace1;"><td></tr>
                                            <tr>
                                                <td colspan="3">
                                                    <table width="1000" cellpadding="0" cellspacing="0" style="margin:15px 0;">
                                                        <tr>
                                                            <td width="475">
                                                                <table style="margin:35px 35px 35px 35px;">
                                                                    <tr>
                                                                        <td style="line-height:25px;">
                                                                            <h3 style="font-size:23px;">Venue Details</h3>

                                                                        </td>
                                                                    </tr>
                                                                    <tr><td style="font-weight:normal;line-height:23px;font-size:18px;"><?php echo $branch->branch_add; ?></td></tr>
                                                                    <tr><td style="font-size:18px;"><span style="color: rgb(79, 193, 242);">Email :</span><?php echo $banner->bannerimage_branch_email; ?></td></tr>
                                                                    <tr><td style="font-size:18px;"><span style="color: rgb(79, 193, 242);">Phone :</span><?php echo $banner->bannerimage_branch_contact; ?></td></tr>

                                                                </table>
                                                            </td>
                                                            <td style="border-left:dashed 3px #37ace1;"></td>
                                                            <td width="475">
                                                                <table style="margin:35px 35px 35px 35px;">
                                                                    <tr>
                                                                        <td style="line-height:25px;">
                                                                            <h3 style="font-size:23px;">Guest Details</h3>

                                                                        </td>
                                                                    </tr>
                                                                    <tr><td style="font-weight:normal;line-height:23px;font-size:18px;"><?php  echo $orderdisplaydataval->billing_name; ?> <br></EX><?php echo $orderdisplaydataval->billing_address; ?></td></tr>
                                                                    <tr><td style="font-size:18px;"><span style="color: rgb(79, 193, 242);">Email :</span> <?php echo $orderdisplaydataval->billing_email; ?></td></tr>
                                                                    <tr><td style="font-size:18px;"><span style="color: rgb(79, 193, 242);">Phone :</span> <?php echo $orderdisplaydataval->billing_tel; ?></td></tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3">
                                                                <table width="953" cellpadding="0" cellspacing="0" style="margin:15px 0px;">
                                                                    <tr>
                                                                        <td colspan="3" style="border-top:dashed 3px #37ace1;"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><table width="475" style="margin:15px 0;border-right:dashed 3px #37ace1;">

                                                                                <tr><td style="margin-bottom:0;font-size:15px;margin-top:5px;text-align:center;font-weight:bold;">Booking No.</td><td style="margin-bottom:0;font-size:15px;margin-top:5px;text-align:center;font-weight:bold;">Booking Date.</td></tr>
                                                                                <tr><td style="text-align:center;margin-top:5px;font-size:13px;"><?php  echo $orderdisplaydataval->ticketid; ?> </td><td style="text-align:center;margin-top:5px;font-size:13px;"><?php  echo $d1; ?></td></tr></table></td>
                                                                        <td width="3"></td>
                                                                        <td><table width="475" style="margin:15px 0;">

                                                                                <tr><td style="margin-bottom:0;font-size:15px;margin-top:5px;text-align:center;font-weight:bold;">Visit Date.</td><td style="margin-bottom:0;font-size:15px;margin-top:5px;text-align:center;font-weight:bold;">Session Time</td></tr>
                                                                                <tr><td style="text-align:center;margin-top:5px;font-size:13px;"><?php  echo $d4; ?>.</td><td style="text-align:center;margin-top:5px;font-size:13px;"><?php  echo $from_session_time; ?> to <?php  echo $to_session_time; ?></td></tr></table></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="3" style="border-top:dashed 3px #37ace1;"></td>
                                                                    </tr>
                                                                </table>
                                                            </td></tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    <table width="860" cellpadding="0" cellspacing="0" style="border:solid 1px #37ace1;font-family:Arial, Helvetica, sans-serif;color:#555;margin:0px 50px 10px 50px; ">
                                                        <tr>
                                                            <td width="215" style="background:#444;text-align:center;font-size:18px;color:#fff;padding:15px 0px;font-weight:bold;">
                                                                Items
                                                            </td>
                                                            <td width="215" style="background:#444;text-align:center;font-size:18px;color:#fff;padding:15px 0px;font-weight:bold;">Qty
                                                            </td>
                                                            <td width="215" style="background:#444;text-align:center;font-size:18px;color:#fff;padding:15px 0px;font-weight:bold;">Cost</td>
                                                            <td width="215" style="background:#444;text-align:center;font-size:18px;color:#fff;padding:15px 0px;font-weight:bold;">Total</td>
                                                        </tr>

                                                        <tr><td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px">
                                                                <?php  echo $orderdisplaydataval->packproductname; ?> Package
                                                            </td>
                                                            <td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px"><?php  echo $orderdisplaydataval->packpkg; ?>
                                                            </td>
                                                            <td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px"><span><i class="icon-rupee" aria-hidden="true"></i></span><?php echo ($orderdisplaydataval->packprice / $orderdisplaydataval->packpkg); ?></td>
                                                            <td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px"><span><i class="icon-rupee" aria-hidden="true"></i></span><?php echo ($orderdisplaydataval->packprice); ?></td>
                                                        </tr>

                                                        <?php
                                                        foreach ($orderaddonedisplaydata as $a => $b) {
                                                            if($b!='Something Went Wrong' ){

                                                                ?>
                                                                <tr><td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px"><?php echo $b["addonename"]; ?>

                                                                    </td><td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px"><?php echo $b["addonqty"]; ?>
                                                                    </td>
                                                                    <td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px"><span><i class="icon-rupee" aria-hidden="true"></i></span><?php echo ($b["addoneprice"]/$b["addonqty"]); ?></td>
                                                                    <td width="215" style="text-align:center;font-size:18px;color:#000;padding:10px 0px"><span><i class="icon-rupee" aria-hidden="true"></i></span><?php echo $b["addoneprice"]; ?></td>
                                                                </tr>
                                                            <?php  }} ?>


                                                        <tr>
                                                            <td colspan="4">
                                                                <table width="760" cellpadding="0" cellspacing="0" style="border-top:solid 1px #37ace1;font-family:Arial, Helvetica, sans-serif;color:#555;margin:10px 50px 10px 50px; ">
                                                                    <tr>
                                                                        <td width="380" style="text-align:left;font-size:18px;color:#000;padding:10px 0px 10px 40px">Price:</td>
                                                                        <td width="380" style="text-align:right;font-size:18px;color:#000;padding:10px 40px 10px 0px"><span><i class="icon-rupee" aria-hidden="true"></i></span><?php echo $orderdisplaydataval->subtotal; ?></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>

                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    <table width="860" cellpadding="0" cellspacing="0" style="border:solid 1px #37ace1;font-family:Arial, Helvetica, sans-serif;color:#555;margin:0px 50px 10px 50px; ">
                                                        <tr><td width="430" style="text-align:left;font-size:18px;color:#000;padding:10px 0px 10px 40px">Discount Amount</td><td width="430" style="text-align:right;font-size:18px;color:#000;padding:10px 40px 10px 0px"><span><i class="icon-rupee" aria-hidden="true"></i></span><?php echo $orderdisplaydataval->promocodeprice	; ?></td></tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    <table width="860" cellpadding="0" cellspacing="0" style="border:solid 1px #37ace1;font-family:Arial, Helvetica, sans-serif;color:#555;margin:0px 50px 10px 50px; ">
                                                        <tr><td width="430" style="text-align:left;font-size:18px;color:#000;padding:10px 0px 10px 40px">Internet Handling Charges</td><td width="430" style="text-align:right;font-size:18px;color:#000;padding:10px 40px 10px 0px"><span><i class="icon-rupee" aria-hidden="true"></i></span><?php echo $orderdisplaydataval->internethandlingcharges	; ?></td></tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    <table width="860" cellpadding="0" cellspacing="0" style="border:solid 1px #37ace1;font-family:Arial, Helvetica, sans-serif;color:#555;margin:0px 50px 30px 50px; ">
                                                        <tr><td width="430" style="text-align:left;font-size:18px;color:#000;padding:10px 0px 10px 40px">Grand Total</td><td width="430" style="text-align:right;font-size:18px;color:#000;padding:10px 40px 10px 0px"><span><i class="icon-rupee" aria-hidden="true"></i></span><?php echo $orderdisplaydataval->total; ?></td></tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    <table width="475" style="margin:15px 0px 15px 50px;">
                                                        <tr><td style="font-size:17px;font-weight:bold;color:#000;padding:4px 0px;">Service Tax No. : AAFFC9989BSD001</td></tr>
                                                        <tr><td style="font-size:17px;font-weight:bold;color:#000;padding:4px 0px;">PAN No. : AAFFC9989B</td></tr>
                                                        <tr><td style="font-size:17px;font-weight:bold;color:#000;padding:4px 0px;">Encl. Terms and Conditions</td></tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>

                                        <br>
                                        <br>  <br>  <br>  <br>  <br>


                                        <table width="953" cellpadding="0" cellspacing="0"  style="border:solid 1px #37ace1;font-family:Arial, Helvetica, sans-serif;color:#555;">
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td width="150" valign="top" style="padding:15px 0px 0px 15px;"><img src="assets/admin/images/<?php echo $banner->bannerimage_logo; ?>"  alt=""></td>
                                                            <td style="text-align:center;">
                                                                <table width="653" style="margin:20px 20px;line-height:25px;">
                                                                    <tr>
                                                                        <td width="653" style="color: #37ACE1;font-weight:700;padding:0px 0px;font-size:29px;text-align:center;">
                                                                            <strong><?php echo $banner->bannerimage_top1; ?></strong>

                                                                        </td>
                                                                    </tr>
                                                                    <tr><td style="padding:8px 0px 5px;font-size:18px;font-weight:700;"><strong><?php echo $banner->bannerimage_top2; ?></strong></td></tr>


                                                                    <tr><td style="color: #37ACE1;font-weight:600;padding:15px 0px 0px;font-size:25px;"><strong>Terms and Conditions</strong></td></tr>
                                                                </table>
                                                            </td>
                                                            <td width="150" valign="top" style="padding:15px 15px 0px 0px;"><img src="assets/admin/images/<?php echo $banner->bannerimage_logo; ?>"  alt=""></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3">
                                                                <table width="940" style="margin-left:50px;margin-top:10px;margin-bottom:80px;color:#000;font-size:16px;line-height:25px;">
<?php  $i=1;foreach ($tearmsgatway as $key => $value11) {  ?>

                                                                    <tr>
                                                                        <td width="30" valign="top"><?php echo  $i; ?>.</td>
                                                                        <td width="900" valign="top"><?php echo $value11['term_name']; ?></td>
                                                                    </tr>

<?php  $i++;} ?>







                                                                  
                                                                    <tr>
                                                                        <td width="30" valign="top">&nbsp;</td>
                                                                        <td width="900" valign="top">
                                                                            <table style="font-size:16px;line-height:25px;">
                                                                              

                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                            </tr>
                                        </table>

                                        </td>
                                        </tr>



                                        </table>

                                    </div>


                                    <button type="button" onclick="printDiv('printableArea')" class="btn_green proceed_buttton btns">Print booking</button>
                                </div>
                            </div>


                        </div></div>

                </div>

            </div>
        </div>
    </div>




</section>
    
   