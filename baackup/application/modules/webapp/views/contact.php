
	<div class="container margin_30">
    	<div class="row">
        	<div class="col-md-8">
            <h3>Contact Us</h3>
            <div id="message-contact"></div>
            <div class='flashmsg'> <?php echo $message; ?></div>
				<form method="post" action="" id="html5Form" name="cont_form">
                    <div id="success" class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <div class="success-div"></div>
                    </div>
                    <div id="danger" class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <div class="error-div"></div>
                    </div>				
					<div class="row">
						<div class="col-md-6 col-sm-6">
							<div class="form-group">
								<label>First Name</label>
                                <input type="text" name="fname" class="form-control-1" placeholder="Enter First Name" pattern="[A-Za-z\s]+" required title="First Name should only contain  letters. e.g. John" maxlength="60">
								
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
							<div class="form-group">
								<label>Last Name</label>
                                  <input type="text" name="lname" class="form-control-1" placeholder="Enter Last Name" pattern="[A-Za-z\s]+" required title="Enter Last Name should only contain  letters. e.g. Singh" maxlength="60">
							</div>
						</div>
					</div>
					<!-- End row -->
					<div class="row">
						<div class="col-md-6 col-sm-6">
							<div class="form-group">
								<label>Email</label>
                                  <input type="email" name="email" class="form-control-1" placeholder="Enter Your Email" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="The input is not a valid email address" maxlength="50">
						
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
							<div class="form-group">
								<label>Phone</label>
                                  <input type="tel" name="phone" class="form-control-1" placeholder="Enter Your Phone Number" pattern="^\d{10}$" required title="Your Phone Number Should Only Contain  Letters. e.g. 9999999999" maxlength="10">
							
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Message</label>
								<textarea id="message_contact" name="message" class="form-control-1" placeholder="Write Your Message" style="height:100px;" required ></textarea>
							</div>
						</div>
					</div>
					<div class="row add_bottom_30">
						<div class="col-md-6">
                        	<div class="form-group">
                                <!--label>Human verification</label>
                                <input type="text" id="verify_contact" class="form-control-1 add_bottom_30" placeholder="Are you human? 3 + 1 ="-->
								<input type="submit" value="Submit" class="btn_1" id="submit-contact" name="Submit">
                            </div>
							
						</div>
					</div>
				</form>               
            </div><!-- End col-md-8 -->
            
            <div class="col-md-4">
				<h3></h3>
            	<div class="box_style_1">
                	<h3>Contacts</h3>
                    <h5>Address</h5>
                    <p>Ski India, L05 &amp; L06, DLF Mall of India, Sector 18, Noida, Gautam Buddha Nagar, Uttar Pradesh 201301, India.</p>
                    <h5>Telephone</h5>
                    <p><a href="tel://01202595150"> +91 120 259 51 50</a> / <a href="tel://01202595151">51</a> / <a href="tel://01202595152">52</a> / <a href="tel://01202595153">53</a></p>
                    <h5>Email</h5>
                    <p><a href="mailto:info@skiindiadelhi.com">info@skiindiadelhi.com</a></p>                    
                </div>
            </div><!-- End col-md-4 -->
        </div><!-- End row -->
    </div><!-- End Container -->
	
		