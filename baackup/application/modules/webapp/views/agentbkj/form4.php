<style type="text/css">

    .regiter_step {
        width: 100%;
        float: left;
        background-color: azure;
    }

    .regiter_step .stpe_con_all {
        width: 100%;
        float: left;
        padding: 20px 0px 20px 0px;
    }

    .regiter_step .stpe_con_all .left_contyi {
        width: 100%;
        float: left;
    }

    .regiter_step .stpe_con_all .left_contyi ul {
        width: 100%;
        float: left;
        list-style: none;
    }

    .regiter_step .stpe_con_all .left_contyi ul li {
        padding: 3px;
        float: left;
        width: 100%;
    }

    .regiter_step .stpe_con_all .left_contyi ul li a {
        text-decoration: none;
        border-left: 3px solid #d40735;
        background-color: #2657a6;
        padding: 10px 15px 10px 15px;
        color: #fff;
        font-weight: 700;
        width: 100%;
        float: left;
    }

    .acty {
        color: #fff !important;
        background-color: #d40735 !important;
        border-left: 3px solid #6fc27f !important;
    }

    .regiter_step .stpe_con_all .left_contyi ul li a:hover {
        color: #fff;
        background-color: #d40735;
        border-left: 3px solid #d40735;
    }

    .regiter_step .stpe_con_all .right_contyi {
        width: 100%;
        float: left;
        background-color: #fff;
    }

    .regiter_step .stpe_con_all .right_contyi .heading_tab {
        width: 100%;
        float: left;
        padding: 0px 20px 10px 20px;
        border-bottom: 1px dotted #CCC;
    }

    .regiter_step .stpe_con_all .right_contyi .heading_tab h1 {
        width: 100%;
        float: left;
        font-size: large;
    }

    .regiter_step .stpe_con_all .right_contyi .heading_tab h1 span {
        color: #2657a6;
        font-size: medium;
    }

    .regiter_step .stpe_con_all .right_contyi .form_details {
        width: 100%;
        float: left;
        padding: 25px 20px 10px 20px;
    }

    .regiter_step .stpe_con_all .right_contyi .form_details .form-group {
        width: 100%;
        float: left;
    }

    .regiter_step .stpe_con_all .right_contyi .form_details .form-group select {
        min-height: 35px;
    }

    .regiter_step .stpe_con_all .right_contyi .form_details .next_button {
        width: 100%;
        float: left;
        padding: 20px 0px 20px 0px;
        text-align: center;
    }

    .regiter_step .stpe_con_all .right_contyi .form_details .next_button a {

        background-color: #2657a6;
        padding: 7px 15px 7px 15px;
        line-height: 35px;
        border-radius: 2px;
        color: #fff;
    }

</style>

<section class="regiter_step">
    <div class="container">

        <div class="row">
            <div class="stpe_con_all">
                <div class="col-md-4">
                    <div class="left_contyi">
                        <ul>
                            <li><a href=""> Identity Verification</a></li>
                            <li><a href="" >Seller Details </a></li>
                            <li><a href="">Official Details</a></li>
                            <li><a href="agentform4" class="acty">Statutory Requirements</a></li>
                            <li><a href="">Bank Details</a></li>
                            <li><a href="">Documents Required</a></li>

                        </ul>

                    </div>
                </div>

                <div class="col-md-8">

                    <div class="right_contyi">
                        <div class="heading_tab">
                            <h1>Statutory Requirements : Mention NA where not applicable </h1>
                        </div>
                        <form action="" method="post" enctype="">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>LST No</label>
                                        <input type="text" name="lstno" class="form-control-1" placeholder="Enter LST No"  required title="Enter LST No" maxlength="60">
                                    </div>

                                    <div class="col-md-6">
                                        <label>CST No </label>
                                        <input type="text" name="cstno" class="form-control-1" placeholder="Enter CST No"  required title="Enter CST No" maxlength="60">
                                    </div>

                                </div>
                            </div>


                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Excise Registration No</label>
                                        <input type="text" name="registrationno" class="form-control-1" placeholder="Enter Excise Registration No"  required title="Enter Excise Registration No" maxlength="60">
                                    </div>

                                    <div class="col-md-6">
                                        <label>VAT No. / TIN No </label>
                                        <input type="text" name="vatno" class="form-control-1" placeholder="Enter VAT No. / TIN No"  required title="Enter VAT No. / TIN No" maxlength="60">
                                    </div>

                                </div>
                            </div>




                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>PAN No</label>
                                        <input type="text" name="panno" class="form-control-1" placeholder="Enter PAN No"  required title="Enter PAN No" maxlength="60">
                                    </div>

                                    <div class="col-md-6">
                                        <label>Service Tax Registration No </label>
                                        <input type="text" name="servicetaxno" class="form-control-1" placeholder="Enter Service Tax Registration No"  required title="Enter Service Tax Registration No" maxlength="60">
                                    </div>

                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>TAN No</label>
                                        <input type="text" name="tanno" class="form-control-1" placeholder="Enter TAN No"  required title="Enter TAN No" maxlength="60">
                                    </div>

                                    <div class="col-md-6">
                                        <label>PF No </label>
                                        <input type="text" name="pfno" class="form-control-1" placeholder="Enter PF No"  required title="Enter PF No" maxlength="60">
                                    </div>

                                </div>
                            </div>


                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>ESIC No</label>
                                        <input type="text" name="esisno" class="form-control-1" placeholder="Enter ESIC No"  required title="Enter ESIC No" maxlength="60">
                                    </div>

                                    <div class="col-md-6">
                                        <label>Office / Establishment / Factory Registration
                                            No./ Labour Licence no </label>
                                        <input type="text" name="officeregistrationno" class="form-control-1" placeholder="Enter Office / Establishment / Factory Registration
                                    No./ Labour Licence no"  required title="Enter Office / Establishment / Factory Registration
                                    No./ Labour Licence no" maxlength="60">
                                    </div>

                                </div>
                            </div>


                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Any exemption from tax</label>
                                        <input type="text" name="exceptiontax" class="form-control-1" placeholder="Enter Any exemption from tax"  required title="Enter Any exemption from tax" maxlength="60">
                                    </div>

                                    <div class="col-md-6">
                                        <label>Others if any, mention the details with number </label>
                                        <input type="text" name="otherexceptiontax" class="form-control-1" placeholder="Enter Others if any, mention the details with number"  required title="Enter Others if any, mention the details with number" maxlength="60">
                                    </div>

                                </div>
                            </div>
                            <div class="next_button">
                                <input type="submit" value="Submit" name="Submit" class="btn_1">
                            </div>


                        </form>
                    </div>

                </div>

            </div>

        </div>
    </div>
</section>