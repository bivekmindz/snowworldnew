<style>
    #CaptchaDiv {
        font: bold 30px verdana, arial, sans-serif;
        font-style: italic;
        color: #000000;
        background-color: #FFFFFF;
        padding: 11px 30px;
        -moz-border-radius: 4px;
        -webkit-border-radius: 4px;
        border-radius: 4px;
    }

    #CaptchaInput {
        width: 135px;
        font-size: 15px;
        min-height: 38px;
        margin: 7px 0px;
    }
	
	.continue{ width:100%; float:left;}
	
.continue button{    padding: 7px 10px 7px 10px;
    margin: 10px 20px 10px 20px;
    background-color: #3071a9;
    color: #fff;}
	
	.continue button:hover{ color:#fff;}
</style>
<div class="col-md-10 content">

    <div class="form-data">
        <div class="heading-t">
            Agent Amount


        </div>

        <div class="flashmsg"> <?php if($_GET['emsg']=='sucess') { echo "Payment Add"; } else {echo "Payment Not Added";} ?> </div>


        <script>
            window.onload = function () {
                var d = new Date().getTime();
                document.getElementById("tid").value = d;
            };
        </script>
        </head>

        <form method="POST" name="customerData" action="ccavRequestHandler.php" id="ccform"
              onsubmit="return checkform(this);">

            <div class="col-md-12">
                <div class="widget-header">
                    <div class="widget-title"></div>
                </div>
                <div class="widget-content clearfix">
                    
                    <div class="row">
                        <div class="col-xs-6 ">
                        
                        
                        
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="33.333%">Wallet Amount:</td>
    <td width="33.333%"> <input type="text" name="bank_creditamount" class="form-control-1"
                                       placeholder="Enter Your Amout" maxlength="6"
                                       value="<?php echo $memuser->bank_creditamount; ?>" readonly></td>
    <td width="33.333%">Rs</td>
  </tr>
  
 
</table>
         </div>
                        <div class="col-xs-6 ">
                        
                        
                         <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="33.333%">Add Amount:</td>
    <td width="33.333%"> <input type="text" name="amount" class="form-control-1" placeholder="Enter Your amount" required maxlength="20" value="1" id="txtwalletamount"></td>
    <td width="33.333%">Rs</td>
  </tr>
  
 
</table>
                            

                        </div>
                       
                    </div>
                    <div class="heading-t"> Agent Payment Details</div>

                    <div class="row">
                        <div class="col-xs-2">
                            <div class="form-group">

                                <select class="form-control " id="sel1" required style="min-width: 72px;"name="title" readonly="readonly">


                                    <option>Mr</option>
                                    <option>Mrs</option>
                                    <option>Ms</option>

                                </select>
                            </div>

                        </div>
                        <div class="col-xs-10">

                            <div class="form-group">

                                <input type="text" name="billing_name" class="form-control-1 form-control_width" placeholder="Enter Your Name" pattern="[A-Za-z\s]+" required title="First Name should only contain  Alphabets. e.g. John" maxlength="60" value="<?php echo $memuser->agent_firstname . ' ' . $memuser->agent_lastname; ?>" readonly="readonly">

                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-xs-6 ">
                            <div class="form-group">
                                <input type="email" name="billing_email" class="form-control-1"
                                       placeholder="Enter Your Email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" required title="The input is not a valid email address" maxlength="50"  value="<?php echo $memuser->agent_email; ?>" readonly="readonly">

                            </div>

                        </div>
                        <div class="col-xs-6 ">

                            <div class="form-group">

                                <input type="tel" name="billing_tel" class="form-control-1" placeholder="Enter Your Phone Number" pattern="^\d{10}$" required title="Your Phone Number Should Only Contain  Numbers. e.g. 9999999999" readonly="readonly"maxlength="10" value="<?php echo $memuser->agent_mobile; ?>">
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-xs-12 form-group">

                            <div class="widget-header">
                                <p>Contact Details</p>
                            </div>
                        </div>
                        <div class="col-xs-12 form-group">
                            <textarea id="message_contact" name="billing_address" class="form-control-1" readonly="readonly" placeholder="Write Your Address"><?php echo $memuser->agent_office_address; ?></textarea>


                        </div>

                    </div>
                    <div class="row"><span id="j_idt211:updateDetails">
                                <div class="col-xs-6 ">


                                <div class="form-group">
     <input type="text" name="billing_city" class="form-control-1" placeholder="Enter Your City" pattern="[A-Za-z\s]+"
            title="City Name should only contain  Alphabets. e.g. Delhi" maxlength="20" readonly="readonly"value="<?php echo $memuser->agent_city; ?>">


    </div>
                                </div>
                                <div class="col-xs-6 ">

                                <div class="form-group">

    <input type="text" name="billing_state" class="form-control-1" placeholder="Enter Your State" pattern="[A-Za-z\s]+"
           readonly="readonly"  title="State Name should only contain  Alphabets. e.g. Delhi" maxlength="20"
           value="<?php echo $memuser->agent_state; ?>">

    </div>
                                </div></span>
                    </div>
                    <div class="row">
                        <div class="col-xs-6 ">

                            <div class="form-group">


                                <input type="text" name="billing_zip" class="form-control-1" readonly="readonly"  placeholder="Enter Your Zipcode"
                                       title="Zip should only contain  Number. e.g. 110014" maxlength="6"
                                       value="<?php echo $memuser->agent_pincode; ?>">


                            </div>

                        </div>
                        <div class="col-xs-6 ">
                            <div class="form-group">

                                <input type="text" name="billing_country" class="form-control-1" readonly="readonly" placeholder="Enter Your Country" pattern="[A-Za-z\s]+"
                                       title="Country Name should only contain  Alphabets. e.g. Delhi" maxlength="20"
                                       value="<?php echo $memuser->agent_emailcountry; ?>">
                            </div>

                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-6">
                            <div id="CaptchaDiv" style="float:right;"></div>
                        </div>
                        <div class="col-md-6">
                            <div class="capbox-inner" style="float:left;">
                                Type the Captcha Number:<br>
                                <input type="hidden" id="txtCaptcha">
                                <input type="text" name="CaptchaInput" id="CaptchaInput" size="15"><br>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">

                            <input type="checkbox" id="creditTermsCheck" name="checkbox" required
                                   title="Accept Tearm And Condition">
                            &nbsp;I Agree to the <a href="tearmcondition" target="_blank"> Terms Conditions</a>
                        </div>
                    </div>


                </div>


            </div>

            <div class="sub_div">
                <input type="hidden" name="tid" id="tid" value="12345" readonly/>
                <input type="hidden" name="merchant_id" value="87391"/>
                <input type="hidden" name="order_id" value="SkiAgent_<?php echo $orderwalletselect->agid; ?>"/>
                <input type="hidden" name="order_idval" value="<?php echo $orderwalletselect->agid; ?>"/>
                <input type="hidden" name="user_id" value="<?php echo $this->session->userdata('ppid'); ?>"/>

                <input type="hidden" name="currency" value="INR"/>
                <input type="hidden" name="redirect_url" value="<?php echo base_url(); ?>agentamountsucess"/>
                <input type="hidden" name="cancel_url" value="<?php echo base_url(); ?>agentamountfail"/>
                <input type="hidden" name="language" value="EN"/>

                <div class="continue">
                    <button id="" name="submit"
                            class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only btn btn-custom btn-prime"
                            onclick="return cvalid()" type="submit" role="button" aria-disabled="false" value="cartses">
                        <span class="ui-button-text ui-c">Checkout</span></button>

                    <script type="text/javascript">

                        // Captcha Script

                        function checkform(theform) {
                            var why = "";

                            if (theform.CaptchaInput.value == "") {
                                why += "- Please Enter CAPTCHA Code.\n";
                            }
                            if (theform.CaptchaInput.value != "") {
                                if (ValidCaptcha(theform.CaptchaInput.value) == false) {
                                    why += "- The CAPTCHA Code Does Not Match.\n";
                                }
                            }
                            if (why != "") {
                                alert(why);
                                return false;
                            }
                        }

                        var a = Math.ceil(Math.random() * 9) + '';
                        var b = Math.ceil(Math.random() * 9) + '';
                        var c = Math.ceil(Math.random() * 9) + '';
                        var d = Math.ceil(Math.random() * 9) + '';
                        var e = Math.ceil(Math.random() * 9) + '';

                        var code = a + b + c + d + e;
                        document.getElementById("txtCaptcha").value = code;
                        document.getElementById("CaptchaDiv").innerHTML = code;

                        // Validate input against the generated number
                        function ValidCaptcha() {
                            var str1 = removeSpaces(document.getElementById('txtCaptcha').value);
                            var str2 = removeSpaces(document.getElementById('CaptchaInput').value);

                            var str3 = removeSpaces(document.getElementById('txtwalletamount').value);
                            if(str3>0)
                            {
                            if (str1 == str2) {
                                return true;
                            } else {
                                return false;
                            }}
else { alert("Amount is greater then 0");return false;}                   }
                        // Remove the spaces from the entered and generated code
                        function removeSpaces(string) {
                            return string.split(' ').join('');
                        }
                    </script>

                </div>
            </div>
        </form>

        <!-- <script language="javascript" type="text/javascript" src="json.js"></script>-->
        <!-- <script src="jquery-1.7.2.min.js"></script>-->
        <script language="javascript" type="text/javascript" src="json.js"></script>
        <script src="jquery-1.7.2.min.js"></script>
        <script type="text/javascript">
            $(function () {

                /* json object contains
                 1) payOptType - Will contain payment options allocated to the merchant. Options may include Credit Card, Net Banking, Debit Card, Cash Cards or Mobile Payments.
                 2) cardType - Will contain card type allocated to the merchant. Options may include Credit Card, Net Banking, Debit Card, Cash Cards or Mobile Payments.
                 3) cardName - Will contain name of card. E.g. Visa, MasterCard, American Express or and bank name in case of Net banking.
                 4) status - Will help in identifying the status of the payment mode. Options may include Active or Down.
                 5) dataAcceptedAt - It tell data accept at CCAvenue or Service provider
                 6)error -  This parameter will enable you to troubleshoot any configuration related issues. It will provide error description.
                 */
                var jsonData;
                var access_code = ""; // shared by CCAVENUE
                var amount = "<?php echo $paymentpac->total; ?>";
                var currency = "INR";

                $.ajax({
                    url: 'https://secure.ccavenue.com/transaction/transaction.do?command=getJsonData&access_code=' + access_code + '&currency=' + currency + '&amount=' + amount,
                    dataType: 'jsonp',
                    jsonp: false,
                    jsonpCallback: 'processData',
                    success: function (data) {
                        jsonData = data;
                        // processData method for reference
                        processData(data);
                        // get Promotion details
                        $.each(jsonData, function (index, value) {
                            if (value.Promotions != undefined && value.Promotions != null) {
                                var promotionsArray = $.parseJSON(value.Promotions);
                                $.each(promotionsArray, function () {
                                    console.log(this['promoId'] + " " + this['promoCardName']);
                                    var promotions = "<option value=" + this['promoId'] + ">"
                                        + this['promoName'] + " - " + this['promoPayOptTypeDesc'] + "-" + this['promoCardName'] + " - " + currency + " " + this['discountValue'] + "  " + this['promoType'] + "</option>";
                                    $("#promo_code").find("option:last").after(promotions);
                                });
                            }
                        });
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        alert('An error occurred! ' + ( errorThrown ? errorThrown : xhr.status ));
                        //console.log("Error occured");
                    }
                });

                $(".payOption").click(function () {
                    var paymentOption = "";
                    var cardArray = "";
                    var payThrough, emiPlanTr;
                    var emiBanksArray, emiPlansArray;

                    paymentOption = $(this).val();
                    $("#card_type").val(paymentOption.replace("OPT", ""));
                    $("#card_name").children().remove(); // remove old card names from old one
                    $("#card_name").append("<option value=''>Select</option>");
                    $("#emi_div").hide();

                    //console.log(jsonData);
                    $.each(jsonData, function (index, value) {
                        //console.log(value);
                        if (paymentOption != "OPTEMI") {
                            if (value.payOpt == paymentOption) {
                                cardArray = $.parseJSON(value[paymentOption]);
                                $.each(cardArray, function () {
                                    $("#card_name").find("option:last").after("<option class='" + this['dataAcceptedAt'] + " " + this['status'] + "'  value='" + this['cardName'] + "'>" + this['cardName'] + "</option>");
                                });
                            }
                        }

                        if (paymentOption == "OPTEMI") {
                            if (value.payOpt == "OPTEMI") {
                                $("#emi_div").show();
                                $("#card_type").val("CRDC");
                                $("#data_accept").val("Y");
                                $("#emi_plan_id").val("");
                                $("#emi_tenure_id").val("");
                                $("span.emi_fees").hide();
                                $("#emi_banks").children().remove();
                                $("#emi_banks").append("<option value=''>Select your Bank</option>");
                                $("#emi_tbl").children().remove();

                                emiBanksArray = $.parseJSON(value.EmiBanks);
                                emiPlansArray = $.parseJSON(value.EmiPlans);
                                $.each(emiBanksArray, function () {
                                    payThrough = "<option value='" + this['planId'] + "' class='" + this['BINs'] + "' id='" + this['subventionPaidBy'] + "' label='" + this['midProcesses'] + "'>" + this['gtwName'] + "</option>";
                                    $("#emi_banks").append(payThrough);
                                });

                                emiPlanTr = "<tr><td>&nbsp;</td><td>EMI Plan</td><td>Monthly Installments</td><td>Total Cost</td></tr>";

                                $.each(emiPlansArray, function () {
                                    emiPlanTr = emiPlanTr +
                                        "<tr class='tenuremonth " + this['planId'] + "' id='" + this['tenureId'] + "' style='display: none'>" +
                                        "<td> <input type='radio' name='emi_plan_radio' id='" + this['tenureMonths'] + "' value='" + this['tenureId'] + "' class='emi_plan_radio' > </td>" +
                                        "<td>" + this['tenureMonths'] + "EMIs. <label class='merchant_subvention'>@ <label class='emi_processing_fee_percent'>" + this['processingFeePercent'] + "</label>&nbsp;%p.a</label>" +
                                        "</td>" +
                                        "<td>" + this['currency'] + "&nbsp;" + this['emiAmount'].toFixed(2) +
                                        "</td>" +
                                        "<td><label class='currency'>" + this['currency'] + "</label>&nbsp;" +
                                        "<label class='emiTotal'>" + this['total'].toFixed(2) + "</label>" +
                                        "<label class='emi_processing_fee_plan' style='display: none;'>" + this['emiProcessingFee'].toFixed(2) + "</label>" +
                                        "<label class='planId' style='display: none;'>" + this['planId'] + "</label>" +
                                        "</td>" +
                                        "</tr>";
                                });
                                $("#emi_tbl").append(emiPlanTr);
                            }
                        }
                    });

                });


                $("#card_name").click(function () {
                    if ($(this).find(":selected").hasClass("DOWN")) {
                        alert("Selected option is currently unavailable. Select another payment option or try again later.");
                    }
                    if ($(this).find(":selected").hasClass("CCAvenue")) {
                        $("#data_accept").val("Y");
                    } else {
                        $("#data_accept").val("N");
                    }
                });

                // Emi section start
                $("#emi_banks").on("change", function () {
                    if ($(this).val() != "") {
                        var cardsProcess = "";
                        $("#emi_tbl").show();
                        cardsProcess = $("#emi_banks option:selected").attr("label").split("|");
                        $("#card_name").children().remove();
                        $("#card_name").append("<option value=''>Select</option>");
                        $.each(cardsProcess, function (index, card) {
                            $("#card_name").find("option:last").after("<option class=CCAvenue value='" + card + "' >" + card + "</option>");
                        });
                        $("#emi_plan_id").val($(this).val());
                        $(".tenuremonth").hide();
                        $("." + $(this).val() + "").show();
                        $("." + $(this).val()).find("input:radio[name=emi_plan_radio]").first().attr("checked", true);
                        $("." + $(this).val()).find("input:radio[name=emi_plan_radio]").first().trigger("click");

                        if ($("#emi_banks option:selected").attr("id") == "Customer") {
                            $("#processing_fee").show();
                        } else {
                            $("#processing_fee").hide();
                        }

                    } else {
                        $("#emi_plan_id").val("");
                        $("#emi_tenure_id").val("");
                        $("#emi_tbl").hide();
                    }


                    $("label.emi_processing_fee_percent").each(function () {
                        if ($(this).text() == 0) {
                            $(this).closest("tr").find("label.merchant_subvention").hide();
                        }
                    });

                });

                $(".emi_plan_radio").on("click", function () {
                    var processingFee = "";
                    $("#emi_tenure_id").val($(this).val());
                    processingFee =
                        "<span class='emi_fees' >" +
                        "Processing Fee:" + $(this).closest('tr').find('label.currency').text() + "&nbsp;" +
                        "<label id='processingFee'>" + $(this).closest('tr').find('label.emi_processing_fee_plan').text() +
                        "</label><br/>" +
                        "Processing fee will be charged only on the first EMI." +
                        "</span>";
                    $("#processing_fee").children().remove();
                    $("#processing_fee").append(processingFee);

                    // If processing fee is 0 then hiding emi_fee span
                    if ($("#processingFee").text() == 0) {
                        $(".emi_fees").hide();
                    }

                });


                $("#card_number").focusout(function () {
                    /*
                     emi_banks(select box) option class attribute contains two fields either allcards or bin no supported by that emi
                     */
                    if ($('input[name="payment_option"]:checked').val() == "OPTEMI") {
                        if (!($("#emi_banks option:selected").hasClass("allcards"))) {
                            if (!$('#emi_banks option:selected').hasClass($(this).val().substring(0, 6))) {
                                alert("Selected EMI is not available for entered credit card.");
                            }
                        }
                    }

                });


                // Emi section end


                // below code for reference

                function processData(data) {
                    var paymentOptions = [];
                    var creditCards = [];
                    var debitCards = [];
                    var netBanks = [];
                    var cashCards = [];
                    var mobilePayments = [];
                    $.each(data, function () {
                        // this.error shows if any error
                        // console.log(this.error);
                        paymentOptions.push(this.payOpt);
                        switch (this.payOpt) {
                            case 'OPTCRDC':
                                var jsonData = this.OPTCRDC;
                                var obj = $.parseJSON(jsonData);
                                $.each(obj, function () {
                                    creditCards.push(this['cardName']);
                                });
                                break;
                            case 'OPTDBCRD':
                                var jsonData = this.OPTDBCRD;
                                var obj = $.parseJSON(jsonData);
                                $.each(obj, function () {
                                    debitCards.push(this['cardName']);
                                });
                                break;
                            case 'OPTNBK':
                                var jsonData = this.OPTNBK;
                                var obj = $.parseJSON(jsonData);
                                $.each(obj, function () {
                                    netBanks.push(this['cardName']);
                                });
                                break;

                            case 'OPTCASHC':
                                var jsonData = this.OPTCASHC;
                                var obj = $.parseJSON(jsonData);
                                $.each(obj, function () {
                                    cashCards.push(this['cardName']);
                                });
                                break;

                            case 'OPTMOBP':
                                var jsonData = this.OPTMOBP;
                                var obj = $.parseJSON(jsonData);
                                $.each(obj, function () {
                                    mobilePayments.push(this['cardName']);
                                });
                        }

                    });

                    //console.log(creditCards);
                    // console.log(debitCards);
                    // console.log(netBanks);
                    // console.log(cashCards);
                    //  console.log(mobilePayments);

                }
            });
        </script>
    </div>

</div>
