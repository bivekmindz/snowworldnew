<style>

.change_pask{width: 100%;
    float: left;
    text-align: center;
    padding: 30px 0px 70px 0px;}
</style>
<div class="col-md-10 content">

    <div class="form-data">
        <div class="heading-t">
            Update Password
  </div>

        <div class="flashmsg"> <?php echo validation_errors(); ?> <?php echo $message; ?> </div>


        <form id="change_password_form" name="change_password_form" method="post" action="" enctype="">
            <input type="hidden" name="change_password_form" value="change_password_form" />
<span id="change_password_form:changePassword">
      <div class="row">
        <div class="col-md-6 col-md-offset-3">
          <div class="form-group">
            <label>Current Password</label>

            <input required name="currentpassword" type="password" class="form-control" placeholder="Enter Your Current Password" role="textbox" aria-disabled="false" aria-readonly="false" maxlength="8">
          </div>
          <div class="form-group">
            <label>New Password</label><input  id="password" name="newpassword" type="password" class="form-control" placeholder="Enter Your New Password" role="textbox" aria-disabled="false" aria-readonly="false" maxlength="35">


          </div>
          <div class="form-group">
            <label>Confirm Password</label><input id="confirm_password" name="confirmpassword" type="password" class="form-control" placeholder="Enter Your Confirm Password" role="textbox" aria-disabled="false" aria-readonly="false" required maxlength="35">
          <span class="err00"  style="color: red;display: none">PASSWORD DON'T MATCH</span>
  </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
        
        <div class="change_pask">
        
        <button  name="submit" class="btn btn-lg btn-primary"  onclick="return functi()"type="submit" role="button" aria-disabled="false" value="changePswrd"><span class="ui-button-text ui-c">Change Password</span></button>
        </div>
        </div>

        </div>
      </div></span>
       <script>

                     function functi() {
                          var password = document.getElementById("password");
                          var confirm_password = document.getElementById("confirm_password");

                             if(password.value != confirm_password.value) {
                                $(".err00").css('display','block').fadeOut(3000);

                                 return false;
                             } else {
                                 confirm_password.setCustomValidity('');
                             }
                      }
                  </script>
        </form>
    </div>

</div>
	