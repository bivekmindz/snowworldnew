<?php

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex">
    <!-- Favicons-->
    <link rel="apple-touch-icon" sizes="57x57" href="https://www.bookingprocessing.com/assets/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="https://www.bookingprocessing.com/assets/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="https://www.bookingprocessing.com/assets/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="https://www.bookingprocessing.com/assets/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="https://www.bookingprocessing.com/assets/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="https://www.bookingprocessing.com/assets/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="https://www.bookingprocessing.com/assets/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="https://www.bookingprocessing.com/assets/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="https://www.bookingprocessing.com/assets/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="https://www.bookingprocessing.com/assets/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="https://www.bookingprocessing.com/assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="https://www.bookingprocessing.com/assets/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="https://www.bookingprocessing.com/assets/favicon/favicon-16x16.png">
    <title>Indoor Amusement Park Ski India</title>
    <meta http-equiv="description" name="description" content="Ski India- Snow theme amusement park complete entertainment and hangout places to go with friends and family, India's first & biggest snow park Ski Dubai now in Delhi/NCR"/>
    <meta name="keywords" content="skiing in india, amusment park, snow theme park, indoor amusement park">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="assets/css/style.css" rel="stylesheet" id="bootstrap-css">
    <link href="assets/css/font-awesome.min.css" rel="stylesheet" id="bootstrap-css">


    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <script src="assets/js/jquery-1.10.2.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
<style>
    .flashmsg {
        color: red;
        width: 100%;
        text-align: center;
    }
</style>


</head>
<body>


<nav class="navbar navbar-default bg_t navbar-static-top">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle navbar-toggle-sidebar collapsed">
                MENU
            </button>
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">
               <img src="<?php echo base_url(); ?>img/logo-header.jpg" style="    height: 30px;width: auto;" >
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

            <ul class="nav navbar-nav navbar-right">

                <li class="dropdown ">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        Account
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li class="dropdown-header">SETTINGS</li>
                        <li class=""><a href="partnerdashboard"> Dashboard</a></li>
                        <li class=""><a href="updateamount">Update Amount</a></li>
                        <li class=""><a href="updatepartnerrprofile">Update Profile</a></li>
                        <li><a href="changepartnerpassword">Change Password</a></li>
                        <li><a href="order">Order </a></li>
                        <li><a href="mywallet"> WALLET HISTORY</a></li>
                        <li><a href="<?php echo base_url(); ?>"> Packages</a></li>
                        <li class="divider"></li>
                        <li><a href="partnerlogout">Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
