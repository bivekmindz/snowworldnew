
<div class="container-fluid main-container">
    <div class="col-md-2 sidebar">
        <div class="row">
            <!-- uncomment code for absolute positioning tweek see top comment in css -->
            <div class="absolute-wrapper"> </div>
            <!-- Menu -->
            <div class="side-menu">
                <nav class="navbar navbar-default" role="navigation">
                    <!-- Main Menu -->
                    <div class="side-menu-container">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="partnerdashboard"><span class="glyphicon glyphicon-dashboard"></span> Dashboard</a></li>

                            <li><a href="updateamount"><span class="glyphicon glyphicon-plane"></span> Update Amount</a></li>


                            <li><a href="updatepartnerrprofile"><span class="glyphicon glyphicon-plane"></span> Update Profile</a></li>
                            <li><a href="changepartnerpassword"><span class="glyphicon glyphicon-cloud"></span> Change Password</a></li>
                            <li><a href="order"><span class="glyphicon glyphicon-cloud"></span> Order </a></li>
                            <li><a href="mywallet"><span class="glyphicon glyphicon-cloud"></span> WALLET HISTORY</a></li>


                            <li><a href="partnerlogout"><span class="glyphicon glyphicon-cloud"></span> Logout</a></li>




                        </ul>
                    </div>
                </nav>

            </div>
        </div>  		</div>