
  
	
    <div class="container margin_30">
        <div class="main_title">
            <h2>Image Gallery</h2>
        </div>
        <hr>
        <div class="row magnific-gallery">
            <div class="col-md-3 col-sm-3">
                <a HREF="img/photo/slide1.jpg"><img SRC="img/photo/1.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/photo/slide2.jpg"><img SRC="img/photo/2.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/photo/slide3.jpg"><img SRC="img/photo/3.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/photo/slide4.jpg"><img SRC="img/photo/4.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/photo/slide5.jpg"><img SRC="img/photo/5.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/photo/slide6.jpg"><img SRC="img/photo/6.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/photo/slide7.jpg"><img SRC="img/photo/7.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/photo/slide8.jpg"><img SRC="img/photo/8.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/photo/slide9.jpg"><img SRC="img/photo/9.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/photo/slide10.jpg"><img SRC="img/photo/10.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/photo/slide11.jpg"><img SRC="img/photo/11.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/photo/slide12.jpg"><img SRC="img/photo/12.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/photo/slide13.jpg"><img SRC="img/photo/13.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/photo/slide14.jpg"><img SRC="img/photo/14.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/photo/slide15.jpg"><img SRC="img/photo/15.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/photo/slide16.jpg"><img SRC="img/photo/16.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/photo/slide17.jpg"><img SRC="img/photo/17.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/photo/slide18.jpg"><img SRC="img/photo/18.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/photo/slide19.jpg"><img SRC="img/photo/19.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/photo/slide20.jpg"><img SRC="img/photo/20.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/photo/slide21.jpg"><img SRC="img/photo/21.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/photo/slide22.jpg"><img SRC="img/photo/22.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/photo/slide23.jpg"><img SRC="img/photo/23.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/photo/slide24.jpg"><img SRC="img/photo/24.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/photo/slide25.jpg"><img SRC="img/photo/25.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/photo/slide26.jpg"><img SRC="img/photo/26.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/photo/slide27.jpg"><img SRC="img/photo/27.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/photo/slide28.jpg"><img SRC="img/photo/28.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/photo/slide29.jpg"><img SRC="img/photo/29.jpg" alt="" class="img-responsive styled"></a>
            </div>
            <div class="col-md-3 col-sm-3">
                <a HREF="img/photo/slide30.jpg"><img SRC="img/photo/30.jpg" alt="" class="img-responsive styled"></a>
            </div>
        </div><!-- End row -->
        
        <hr>    
    </div><!-- End container -->
    
   
<div id="toTop"></div><!-- Back to top button -->

<!-- Common scripts -->
<script SRC="js/jquery-1.11.2.min.js"></script>
<script SRC="js/common_scripts_min.js"></script>
<script SRC="js/functions.js"></script>
<script SRC="assets/validate.js"></script>
<!-- Specific scripts -->
<script SRC="js/bootstrap-datepicker.js"></script>
<script SRC="js/jquery.validate.min.js"></script>
</body>
</html>