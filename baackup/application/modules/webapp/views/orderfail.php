<style>
.doller_left p {
    color: #000!important;
}
</style>
<div class="failure container-fluid">
    
    
    <div class="container">
    <div class="row">
    
    <div class="bg_fail">
    
    <div class="col-md-12">
    
    <div class="all_content">
    <div class="heading_top">
    
    <h1><span>SORRY FOR THE INCONVENIENCE. YOUR BOOKING IS FAILED</span></h1>
    </div>
    
    <div class="p_class">
    
    <p>
<span>Please note : </span>Below is your booking ID for all communication with Snow world. Please save and store the ID mentioned.</p>
    </div>
    
    <div class="heading_tab_inner"><h5>Booking Details</h5></div>
    
    <div class="content_mid">
    
    <div class="payment_details_main"> 
                      
                        <div class=" col-lg-9 col-md-9 col-sm-6 col-xs-6 review_content ">
                          <div class="top_head_bar">
                            <h4>Ticket Type:  <?php  echo($orderdisplaydataval->packproductname);?></h4>
                          </div>
                          <div class="scifiheader"><h4>Booking ID:  <?php  echo($orderdisplaydataval->ticketid);  ?></h4></div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                          <div class="doller_left">
                            <h2><i class="icon-rupee" aria-hidden="true"></i> <?php echo ($orderdisplaydataval->packprice/$orderdisplaydataval->packpkg );  ?></h2>
                            <p>Per Person</p>


                         


                           </h2>
                            <p></p>
                          </div>
                        </div>
                      </div>
                      
                      
                      
                      
                      <div class="full_width package_table_section">
                        <div class="col-lg-6 col-md-6 border_right">
                          <div class="payment_table_package">
                            <table class="table">
                              <tbody><tr>
                                <td>Booking Date</td>
                                <td>:</td>
                                <td>
<?php 
echo date("D", strtotime($input)) . "\n";
$date_array1 = explode("-",$orderdisplaydataval->addedon ); // split the array
$var_day1 = $date_array1[2]; //day seqment
$var_month1 = $date_array1[1]; //month segment
 $var_year1 = $date_array1[0]; //year segment
 $new_date_format1 = strtotime("$var_year1-$var_month1-$var_day1"); // join them together

echo date(' jS F Y', $new_date_format1);
?> (<?php echo date('h:i:s A');?>)



                               </td>
                              </tr> 


                              <tr>
                                <td>Session Date</td>
                                <td>:</td>
                                <td>
<?php 


 $orderdisplaydataval->departuredate;
$date_array = explode("/",$orderdisplaydataval->departuredate); // split the array
$var_day = $date_array[0]; //day seqment
$var_month = $date_array[1]; //month segment
 $var_year = $date_array[2]; //year segment
  $new_date_format = strtotime("$var_year-$var_month-$var_day"); // join them together

$input = ("$var_year$var_month$var_day");

echo date("D", strtotime($input)) . "\n";


echo date(' jS F Y', $new_date_format); ?></td>
                              </tr>
                             
                              <tr>
                                <td><?php if($orderdisplaydataval->packpkg>1) { ?>Visitors<?php } else { ?>Visitor<?php } ?></td>
                                <td>:</td>
                                <td><?php  echo($orderdisplaydataval->packpkg );  ?></td>
                              </tr>
                              <tr>
                                <td>Addons</td>
                                <td>:</td>
                                <td><?php 
foreach($orderaddonedisplaydata as $key =>$value){ 
 if($value!='Something Went Wrong' ){
                            echo($value['addonename']."&nbsp;x&nbsp;".$value['addonevisiter']."<br>");
                         } else { echo "N/A";} } 
                          ?></td>
                              </tr>                              
                            </tbody></table>
                          </div> 
                        </div>
                      
                        <div class="col-lg-6 col-md-6">                         
                          <div class="payment_table_package">
                            <table>
                              <tbody><tr>
                                <td>Base price</td>
                                <td> <span><i class="icon-rupee" aria-hidden="true"></i></span><?php echo $baseprice=$orderdisplaydataval->packprice/$orderdisplaydataval->packpkg;  ?></td>
                              </tr>
                             
                              <tr>
                                <td>Ticket price</td>
                                <td> <span><i class="icon-rupee" aria-hidden="true"></i></span><?php echo ($orderdisplaydataval->packprice); ?>




                                </td>
                              </tr>
                              <tr>
                                <td>Addons</td>
                               
                                <td><span><i class="icon-rupee" aria-hidden="true"></i></span><?php 
$addonetotal=0;foreach($orderaddonedisplaydata as $key =>$value){
 if($value!='Something Went Wrong' ){

                    $addonetotal +=  ($value['addoneprice']);
                      }
                      else{ $addonetotal= 0;}
                          }  echo $addonetotal;
                          ?></td>
                              </tr>    
                             
                              <tr>
                                <td>Discount (Promocode)</td>
                                <td><span><i class="icon-rupee" aria-hidden="true"></i></span> - <?php echo ($orderdisplaydataval->promocodeprice); ?>
 </td>
                              </tr>

                              <tr>
                                <td>Internet Handling Charge</td>
                                <td><span><i class="icon-rupee" aria-hidden="true"></i></span> <?php echo ($orderdisplaydataval->internethandlingcharges); ?>
 </td>
                              </tr>
                              <tr class="total_row">
                                <td>Payable Price</td>
                                <td>

 <span><i class="icon-rupee" aria-hidden="true"></i></span> <?php echo ($orderdisplaydataval->total); ?>

</td>
                              </tr>
                            </tbody></table>
                          </div>
                        </div>
                      </div>
    
    </div>
    
  <div class="full_width information_section">
                    <div class="information_title scifiheader"> Customer Information </div>
                    <div class="full_width information_table_main">
                      <div class="col-lg-6 col-md-6 border_right">
                        <div class="payment_table_package conyy">
                          <table class="table">
                            <tbody><tr>
                              <td>Booking ID :</td>
                              <td><?php  echo($orderdisplaydataval->ticketid);  ?></td>
                            </tr>
                            <tr>
                              <td>Name :</td>
                              <td><?php  echo($orderdisplaydataval->billing_name);  ?></td>
                            </tr>
                            <tr>
                              <td>Contact :</td>
                              <td><?php  echo($orderdisplaydataval->billing_tel);  ?></td>
                            </tr>
                            <tr>
                              <td>Email :</td>
                              <td><?php  echo($orderdisplaydataval->billing_email );  ?></td>
                            </tr>
                          </tbody></table>
                        </div>
                      </div>
                      <div class="col-lg-6 col-md-6 border_right">
                        <div class="payment_table_package conyy">
                          <table class="table">
                            <tbody><tr>
                              <td>Address :</td>
                              <td><?php   if(($orderdisplaydataval->billing_address)!='') {  echo $orderdisplaydataval->billing_address ; } else { echo "N/A";} ?></td>
                            </tr>
                            <tr>
                              <td>Town/City :</td>
                              <td><?php if(($orderdisplaydataval->billing_city)!='') {  echo($orderdisplaydataval->billing_city ); } else { echo "N/A";} ?></td>
                            </tr>
                            <tr>
                              <td>Zip Code : </td>
                              <td><?php if(($orderdisplaydataval->billing_zip)!='') {  echo($orderdisplaydataval->billing_zip ); } else { echo "N/A";} ?></td>
                            </tr>
                            <tr>
                              <td>Country :</td>
                              <td><?php if($orderdisplaydataval->billing_country!='') {  echo($orderdisplaydataval->billing_country ); } else { echo "N/A";} ?></td>
                            </tr>
                          </tbody></table>
                        </div>
                      </div>
                    </div>
                  </div>
          
          <div class="full_width information_table_main">
                      <div class="booking_text t_align_c">
                        <span>Venue Address</span>
                        <p class="scifiheader"><?php echo ($branch->branch_add); ?>.</p>
                      </div>
                    </div> 
            
    
    </div>
    
    
    
    
    </div>
    
    </div>
    </div>
    
    </div>
    
    
    </div> 
  