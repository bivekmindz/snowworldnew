<?php session_start();?>
<!DOCTYPE html>
<!--[if IE 8]><html class="ie ie8"> <![endif]-->
<!--[if IE 9]><html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html>
<!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">	
	<?php if (empty($seotags->seo_meta_title)) { ?>
    <meta name="title" content="Snow World - Mumbai's Largest Snow Park" />
    <?php } else { ?>
	<meta name="title" content="<?php echo $seotags->seo_meta_title; ?>" />	
	<?php } if (empty($seotags->seo_meta_keyword)) { ?>
    <meta name="keywords" content="snow world mumbai, snow world, entertainment in mumbai,things to do in mumbai,amusement parks in mumbai, places to visit,snow in mumbai,theme parks in mumbai,snow park in mumbai,school picnic,mumbai darshan,ice skating,adventure in mumbai,adventure activities,corporate outing,kitting party places,tourism in mumbai,thing to do on weekend, temperature in mumbai, kids place in mumbai, indoor park in mumbai" />
    <?php } else { ?>
    <meta name="keywords" content="<?php echo $seotags->seo_meta_keyword; ?>">
    <?php } ?>	
    <meta name="description" content="Snow World - Booking">
    <meta name="author" content="Durgesh Chauhan">
	<?php if (empty($seotags->seo_meta_title)) { ?>
    <title>Snow World - Mumbai's Largest Snow Park</title>
    <?php } else { ?>
    <title><?php echo $seotags->seo_meta_title; ?></title>
	
    <?php } if (empty($seotags->seo_meta_desc)) { ?>
    <meta http-equiv="description" name="description" content="Ski India- Snow theme amusement park complete entertainment and hangout places to go with friends and family, India's first & biggest snow park Ski Dubai now in Delhi/NCR"/>
    <?php } else { ?>
    <meta http-equiv="description" name="description" content="<?php echo $seotags->seo_meta_desc; ?>"/>
    <?php } if (empty($seotags->seo_meta_keyword)) { ?>
    <meta name="keywords" content="skiing in india, amusment park, snow theme park, indoor amusement park">
    <?php } else { ?>
    <meta name="keywords" content="<?php echo $seotags->seo_meta_keyword; ?>">
    <?php } ?>
    <!-- Favicons-->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>img/favicon.png" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="<?php echo base_url(); ?>img/apple-touch-icon-57x57-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="<?php echo base_url(); ?>img/apple-touch-icon-72x72-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="<?php echo base_url(); ?>img/apple-touch-icon-114x114-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="<?php echo base_url(); ?>img/apple-touch-icon-144x144-precomposed.png">
    <!-- Google web fonts -->
    <link href='http://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Gochi+Hand' rel='stylesheet' type='text/css'>  
<!-- BASE CSS -->
    <link href="<?php echo base_url(); ?>css/base.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/ticketsearch.css" rel="stylesheet">
    <!-- SPECIFIC CSS -->
    <link href="<?php echo base_url(); ?>css/date_time_picker.css" rel="stylesheet">
    <!--[if lt IE 9]>
      <script src="<?php echo base_url(); ?>js/html5shiv.min.js"></script>
      <script src="<?php echo base_url(); ?>js/respond.min.js"></script>
    <![endif]-->

    <!-- Google Adwords Code -->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-46480743-1', 'auto');
		ga('send', 'pageview');
    </script>
</head>
<body>
<!--[if lte IE 8]>
    <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a>.</p>
<![endif]-->
	<div id="preloader">
        <div class="sk-spinner sk-spinner-wave">
			<div class="sk-rect1"></div>
            <div class="sk-rect2"></div>
            <div class="sk-rect3"></div>
            <div class="sk-rect4"></div>
            <div class="sk-rect5"></div>
        </div>
    </div>
    <!-- End Preload -->

	<div class="layer"></div>
    <!-- Mobile menu overlay mask -->        

    <!-- Header================================================== -->     
    <header>
        <nav>
            <a class="cmn-toggle-switch cmn-toggle-switch__rot  open_close" href="javascript:void(0);"><span>Menu mobile</span></a>
            <div class="main-menu">
                <div id="header_menu">
                	<img src="img/logo-small.png" height="100" alt="Snow World Mumbai" data-retina="true">
                </div>
                <a href="#" class="open_close" id="close_in"><i class="icon_set_1_icon-77"></i></a>
                <ul>
                    <li><a href="http://www.snowworldmumbai.com/index.html"><i class="icon-home"></i> Home</a></li>
					<li><a href="http://www.snowworldmumbai.com/activities.html"><i class="icon-heart"></i> Activities</a></li>                       
                    <li class="submenu">
                        <a href="javascript:void(0);" class="show-submenu"><i class="icon-group"></i> Group Booking <i class="icon-down-open-mini"></i></a>
                        <ul>
                            <li><a href="http://www.snowworldmumbai.com/birthday.html" class="scroll">Birthday Parties</a></li>
                            <li><a href="http://www.snowworldmumbai.com/school.html" class="scroll">School Package</a></li>
                            <li><a href="http://www.snowworldmumbai.com/corporate.html" class="scroll">Corporate Events</a></li>
                        </ul>
                    </li>    
                    <li class="submenu">
                        <a href="javascript:void(0);" class="show-submenu"><i class="icon-picture-1"></i> Gallery <i class="icon-down-open-mini"></i></a>
                        <ul>
                            <li><a href="http://www.snowworldmumbai.com/image_gallery.html"><i class="icon-picture"></i> Image</a></li>
                            <li><a href="http://www.snowworldmumbai.com/video_gallery.html"><i class="icon-video"></i> Video</a></li>
                        	<li><a href="http://www.snowworldmumbai.com/habitat.html"><i class="icon-th-large"></i> Habitat</a></li>
                            <li><a href="http://www.snowworldmumbai.com/story.html"><i class="icon-popup-4"></i> Story</a></li>
                        </ul>
                    </li>
                    <li><a href="http://www.snowworldmumbai.com/contacts.html"><i class="icon-contacts"></i> Contact us</a></li>                   
                
  <?php  if($this->session->userdata['ppid']>0) {} elseif ($this->session->userdata['skiindia'] > 0) { ?>
				<li id="myaccount-links" class="submenu"><a href="javascript:void(0);" class="show-submenu"><i class="icon-login-2"></i> My account<i class="icon-down-open-mini"></i></a>
					<ul>
						<li><a href="<?php echo base_url(); ?>myaccount"><i class="icon-user"></i> My account</a></li>
						<li><a href="<?php echo base_url(); ?>logout"><i class="icon-users-3"></i> Logout</a></li>
					<ul>
				</li>
				<?php }else { ?>
				<li id="myaccount-links" class="submenu"><a href="javascript:void(0);" class="show-submenu"><i class="icon-login-2"></i> Login<i class="icon-down-open-mini"></i></a>
					<ul>
						<li><a href="<?php echo base_url(); ?>userlogin"><i class="icon-user"></i>User Login</a></li>
						<li><a href="<?php echo base_url(); ?>userregister"><i class="icon-users-3"></i> User Register</a></li> 
					</ul>
				</li>                
				<?php } if($this->session->userdata['ppid']>0) {?>


                        <li id="myaccount-links" class="submenu"><a href="javascript:void(0);" class="show-submenu"><i class="icon-login-2"></i> My account<i class="icon-down-open-mini"></i></a>
                            <ul>
                                <li><a href="<?php echo base_url(); ?>partnerdashboard"><i class="icon-user"></i> My account</a></li>
                                <li><a href="<?php echo base_url(); ?>partnerlogout"><i class="icon-users-3"></i> Logout</a></li>
                                <ul>
                                    </li>
<?php } else { ?>
    



<?php } ?>
              




</ul>
            </div><!-- End main-menu -->
        </nav>
        <div class="col-md-2 col-sm-2 col-xs-2">
            <div id="logo">
                <a href="index.html">
                    <img class="logo" src="img/logo_big.png" alt="Snow World Mumbai" data-retina="true">
                </a>
            </div>
        </div>       
    </header><!-- End Header -->