
<script type="text/javascript">

    function validateModifySearch(){

        var txtDepartDate=document.excursionSearchForm.txtDepartDate.value.trim();
        var destinationType=document.excursionSearchForm.destinationType.value;
        var ddAdult=document.excursionSearchForm.ddAdult.value;

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!

        var yyyy = today.getFullYear();

        if(dd<10){
            dd='0'+dd;
        }
        if(mm<10){
            mm='0'+mm;
        }

        var today = mm+'/'+dd+'/'+yyyy;
        today = today.trim();

        var start = destinationType;
        var date = new Date();
        var hour1 = date.getHours();
        var min1 = date.getMinutes();

        var end = hour1+":"+min1;
        e = start.split(':');
        s = end.split(':');
        min = e[1]-s[1];
        hour_carry = 0;
        if(min < 0){
            min += 60;
            hour_carry += 1;
        }
        hour = e[0]-s[0]-hour_carry;
        min = ((min/60)*100).toString();
        diff = hour ;

        if (txtDepartDate==null || txtDepartDate=="" || txtDepartDate.length == 0){
            alert("Date can't be blank");
            return false;
        }

        if (destinationType==null || destinationType==""){

            alert(txtDepartDate+"Booking Time can't be blank");
            return false;
        }

        if (ddAdult==null || ddAdult==""){
            alert("No of person cannot  be blank");
            return false;
        }

//alert(today);
//alert(txtDepartDate);

        if(today >= txtDepartDate){
            if(diff<3){
                alert("Sorry, Online booking is allowed only 3 hours prior to the session time");
                return false;
            }
        }

    }
</script>


<h1>Dashboard</h1>
        <div class="arrow-ri"><span><i class="fa fa-home"></i></span></div>


        <div class="show-data">
            <h1>Showing data for <?php echo date("d-m-Y"); ?></h1>
        </div>


        <div class="row">

            <div class="stepl">
                <form id="rootwizard" method="post" action="#" class="form-horizontal form-wizard">
                    <div class="steps-progress">
                        <div class="progress-indicator"></div>
                    </div>
                    <ul>
                        <li class="completed"> <a href="partnerbookingsearch" ><span>1</span>Search</a> </li>
                        <li class="active"> <a href=""><span>2</span>Package</a> </li>
                        <li class="active"> <a href="" ><span>3</span>Addons</a> </li>
                        <li class="active"> <a href="" ><span>4</span>Summary</a> </li>
                        <li class="active"> <a href="" ><span>5</span>Payment</a> </li>


                    </ul>





                </form>
            </div>
        </div>
        <form  role="form" name="excursionSearchForm" onsubmit="return validateModifySearch()" action="partnerbookingsearch" method="post">
        <div class="row" style="background-color: #f9f9f9;">
            <div class="contactne">
                <div class="flashmsg" style="text-align: center;color:
red"><?php echo $emsg; ?></div>
                <div class="col-md-10 col-md-offset-1">
                    <div class="step_1">
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">

                            <div class="form-group">
                                <div class="input-group">




                                    <input type="text" class="form-control datepicker" placeholder="Select Date" data-format="MM/dd/yyyy" name="txtDepartDate" value="<?php echo date("m/d/Y"); ?> "> <div class="input-group-addon"> <a href="#"><i class="fa fa-calendar"></i></a> </div> </div>

                            </div>

                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">

                            <div class="form-group">
                                <div class="input-group">
                                    <select class="form-control formmm" name="destinationType" id="sessionType1">
                                        <option value="" disabled="disabled" selected="selected">Select Time</option>
                                        <?php
                                        foreach ($timeslot as $key => $value) {

                                            if($value['timeslot_from']>12)
                                            { $value['timeslot_from2']= (($value['timeslot_from']-12)."");
                                                $value['timeslot_from1']= ($value['timeslot_from']."");
                                                $apmvar1="PM";}elseif($value['timeslot_from']==12) {
                                                $apmvar1="PM";
                                                $value['timeslot_from1']= ($value['timeslot_from']);
                                                $value['timeslot_from2']= ($value['timeslot_from']);
                                            }else {

                                                $apmvar1="AM";
                                                $value['timeslot_from1']= ($value['timeslot_from']);

                                                $value['timeslot_from2']= ($value['timeslot_from']);
                                            }


                                            if($value['timeslot_to']>12)
                                            { $value['timeslot_to2']= (($value['timeslot_to']-12)."");
                                                $value['timeslot_to1']= ($value['timeslot_to']);
                                                $apmvar2="PM";}elseif($value['timeslot_to']==12) {
                                                $apmvar2="PM";
                                                $value['timeslot_to1']= ($value['timeslot_to']);
                                                $value['timeslot_to2']= ($value['timeslot_to']);
                                            }else {

                                                $apmvar2="AM";
                                                $value['timeslot_to1']= ($value['timeslot_to']);

                                                $value['timeslot_to2']= ($value['timeslot_to']);
                                            }




                                            ?>

                                            <option value="<?php echo $value['timeslot_from1']; ?>" <?php if($value['timeslot_from'].""==$this->session->userdata('destinationType')) { ?> selected="selected" <?php } ?>><?php echo $value['timeslot_from2']; ?>:<?php echo $value['timeslot_minfrom']; ?> <?php echo  $apmvar1; ?> - <?php echo $value['timeslot_to2']; ?>:<?php echo $value['timeslot_minto']; ?> <?php echo  $apmvar2; ?></option>
                                        <?php } ?>

                                    </select>



                                     <div class="input-group-addon"> <a href="#"><i class="fa fa-clock-o"></i></a> </div> </div>

                            </div>


                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">

                            <div class="form-group">
                                <div class="input-group">
                                    <input type="number" max="200" min="1" class="form-control formmm" id="inputVisitor1" name="ddAdult" placeholder="No. of visitor" value="<?php echo $this->session->userdata('ddAdult'); ?>">  <div class="input-group-addon"> <a href="#"><i class="fa fa-user"></i></a> </div> </div>

                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">

                            <div class="form-group formmm fadeInUp">

                              <input type="submit" value=" Search " class="animated fadeInUp btn_full" id="submit-booking">
                                <span class="animated fadeInUp input-icon_nobg"><i class="fa fa-search"></i></span>
                            </div>

                        </div>

                    </div>


                </div>
            </div>
        </div>


</form>

        <link rel="stylesheet" href="js/jvectormap/jquery-jvectormap-1.2.2.css" id="style-resource-1">


        <script src="assets/agent/js/bootstrap.js" id="script-resource-3"></script>
        <script src="assets/agent/js/bootstrap-datepicker.js"></script>
        <script src="assets/agent/js/daterangepicker.js"></script>



        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script>
            $( function() {
                $.noConflict();
                $( "#datepicker12" ).datepicker({ minDate: 0, maxDate: "+1M " });
            } );
        </script>
