<script type="text/javascript">


    function incrementValue()
    {
        var a = parseInt($("#mainnumbervalue1").val());
        var b = parseInt($("#mainnumber").html());
        var c = parseInt($("#mainnumbervalue").val());

        var value = parseInt(document.getElementById('number').value);

        value = isNaN(value) ? 0 : value;
        if(value<19){
            value++;
            b = b+a;
            c = value*a;

            $("#mainnumbervalue").val(c);

            $("#mainnumber").html(b);
            document.getElementById('number').value = value;

        }
    }
    function decrementValue()
    {
        var a = parseInt($("#mainnumbervalue1").val());
        var b = parseInt($("#mainnumber").html());
        var c = parseInt($("#mainnumbervalue").val());
        var value = parseInt(document.getElementById('number').value);
        value = isNaN(value) ? 0 : value;
        if(value>1){
            value--;
            b = b-a;
            c = value*a;

            $("#mainnumbervalue").val(c);
            $("#mainnumber").html(b);

            document.getElementById('number').value = value;
            document.cookie = "myJavascriptVar = " + value
        }

    }
</script>
        <h1>Packages</h1>
        <div class="arrow-ri"><span><i class="fa fa-home"></i></span></div>
        <div class="show-data">

        </div>
        <div class="row">
            <div class="stepl">
                <form id="rootwizard" method="post" action="#" class="form-horizontal form-wizard">
                    <div class="steps-progress">
                        <div class="progress-indicator"></div>
                    </div>
                    <ul>
                        <li class="active"> <a href="partnerbookingsearch" ><span>1</span>Search</a> </li>
                        <li class="completed" > <a href="agentpackagesstep"><span>2</span>Package</a> </li>
                        <li class="active"> <a href="" ><span>3</span>Addons</a> </li>
                        <li class="active"> <a href="" ><span>4</span>Summary</a> </li>
                        <li class="active"> <a href="" ><span>5</span>Payment</a> </li>
                    </ul>
                </form>
            </div>
        </div>
<form action="" method="post">
    <?php  $count = 0;

    if($this->session->userdata('packageqtyval'))
    {
        $pacqtyval[$count]=$this->session->userdata('packageqtyval');

    }

    elseif($this->session->userdata('packagequantitys')!='') {


        $pacqtyval= (explode(",",$this->session->userdata('packagequantitys')));
        $pacqtyval[$count]=$pacqtyval[$count];

    }
    elseif($this->session->userdata('ddAdult')){
        $pacqtyval[$count]=$this->session->userdata('ddAdult');


    }


    else{
        $pacqtyval[$count]=1;

    }
    ?>
        <div class="row" style="background-color: #f9f9f9;">
            <div class="contactne">
                <div class="col-md-12 col-md-offset-0">

                    <div class="package">
                        <div class="col-md-2"> <?php  if( $vieww->package_image!='') { ?>

                                <img src="assets/admin/images/<?php echo $vieww->package_image; ?>">
                            <?php } else { ?>
                                <img src="img/1463897752358.jpeg">
                            <?php } ?> </div>
                        <div class="col-md-4">
                            <div class="regular">
                                <h1><?php echo $vieww->package_name; ?></h1>
                                <p><?php echo  substr( $vieww->package_description,0,70); ?>.....</p>

                                <p><a href="javascript:;" onclick="jQuery('#modal-1').modal('show');">Read More</a></p>

                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="qual">

                                <h1>Qty</h1>

                                <div class="input-spinner"> <input type="button" class="btn btn-default1" onClick="decrementValue()" value="-">


                                    <div class="test-incre"><input type="text" readonly id="number" class="form-control size-5" name="packageqty" max="1200" min="1" value="<?php echo $pacqtyval[$count]; ?>"></div>
                                    &nbsp;<input type="button" class="btn btn-default1" onClick="incrementValue()" value="+"> </div>

                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="rupess">

                                <span><i class="fa fa-rupee" aria-hidden="true"></i> </span> <span id="mainnumber"><?php echo $pacqtyval[$count]*$vieww->package_price ; ?></span>
                            </div>

                        </div>
                        <div class="col-md-2">

                            <div class="show_dad">
                                <div class="form-group formmm ">
                                    <input type="hidden" name="packageid" value="<?php  echo $vieww->package_id; ?>"  >
                                    <input type="hidden" name="packageimage" value="<?php  echo $vieww->package_image; ?>">
                                    <input type="hidden" name="packagename" value="<?php echo $vieww->package_name; ?>">
                                    <input type="hidden" name="packagedescription" value="<?php echo $vieww->package_description; ?>">
                                    <input type="hidden" name="packageprice1" id="mainnumbervalue1" value="<?php echo $vieww->package_price; ?>">
                                    <input type="hidden" name="packageprice" id="mainnumbervalue" value="<?php echo $pacqtyval[$count]*$vieww->package_price; ?>">
                                    <button  name="submit" class="btn_full" type="submit" role="button" aria-disabled="false"  value="CONTINUE package"><span class="ui-button-text ui-c">BOOK</span></button>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="modal-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <div class="model_header">

                            <div class="col-md-2 col-lg-2 col-sm-2 col-xs-4">
                                <?php  if( $vieww->package_image!='') { ?>
                                    <img src="assets/admin/images/<?php echo $vieww->package_image; ?>" class="img-responsive">
                                <?php } else { ?>
                                    <img src="img/1463897752358.jpeg" class="img-responsive">
                                <?php } ?>

                            </div>

                            <div class="col-md-6 col-lg-6 col-sm-6 col-xs-4">
                                <div class="ticket_type">  <?php echo $vieww->package_name; ?></div>
                            </div>
                            <div class="col-md-3 col-lg-3 col-sm-3 col-xs-4">
                                <span class="web_rupee"><span>₹ </span>    <?php echo $vieww->package_price; ?>        </span></div>


                        </div>
                    </div>
                    <div class="modal-body"> <?php echo $vieww->package_description; ?> </div>
                    <div class="modal-footer">
                        </div>
                </div>
            </div>
        </div>

