<script src="<?php echo admin_url();?>js/moment.js"></script>


<script src = "https://code.jquery.com/jquery-1.10.2.js"></script>
<script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script>
    $( function() {
    } );
</script>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"
        crossorigin="anonymous"></script>


<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <div class="top_headadin">
                <h1>ALL ORDER LISTING</h1>
            </div>



        </div>

    </div>



    <div class="row">
        <table id="table1"  class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>S.NO</th>
                <th>Username</th>
                <th>User Email</th>
                <th>User Contact</th>
                <th>Ticket no.</th>
                <th>No. of Peoples </th>
                <th>Price After Discount(in Rs.)</th>
                <th>Date of Booking</th>
                <th>Visit Date - Time</th>
                <th>paymentstatus</th>
                <th>Payment mode</th>
                <th>CCAVENUE Tracking ID</th>
                <th>Voucher Quantity</th>
                <th>Voucher Price(Total in Rs.)</th>
                <th> Commision Percentage</th>
                <th> Commision Amount</th>

            </tr>
            </thead>
            <tbody>
            <?php $i = 1;
            //p($vieww_order[0]);
            foreach ($vieww_order as $v) {
                if($this->session->userdata('snowworld')->EmployeeId) {
                    if($this->session->userdata('snowworld')->LocationId != $v->locationid)
                    {
                        continue;
                    }
                }?>
                <tr>
                    <td><?= $i; ?></td>
                    <td><?php echo $v->billing_name; ?></td>
                    <td><?php echo $v->billing_email; ?></td>
                    <td><?php echo $v->billing_tel; ?></td>
                    <td><?php echo $v->ticketid; ?></td>
                    <td><?php echo $v->packpkg; ?></td>
                    <td><?php echo $v->total; ?></td>
                    <td><?php echo $v->addedon; ?></td>
                    <?php if($v->departuretime > 12){
                        $dt = $v->departuretime - 12;}else{$dt = $v->departuretime; } ?>

                    <td><?php echo $v->departuredate."<br>".$dt.":".$v->frommin.$v->txtfromd." TO ".$v->tohrs.":".$v->tomin.$v->txttod; ?></td>
                    <td><?php echo $v->order_status; ?></td>
                    <td><?php echo $v->paymenttype; ?></td>
                    <td><?php echo $v->tracking_id; ?></td>
                    <td><?php

                        $ex = explode(',', $v->addon);
                        $ex1 = explode(',', $v->addonquantity);
                        $ex2 = explode(',', $v->orderaddon_print_status);
                        $ex3 = explode(',', $v->orderaddon_print_quantity);
                        $ex4 = explode(',', $v->addonprice);
                        foreach ($ex as $key => $a) {
                            if ($a == "") {
                                echo "N/A";
                                continue;
                            } elseif ($ex1[$key] != $ex3[$key]) {
                                echo '<button disabled="disabled" style="float: left; text-align: center" type="button" class="btn btn-danger" > ' . $a . ' PENDING(' . $ex1[$key] . ')</button><br>';

                            } else {
                                echo '<button style="float: left; text-align: center" type="button" class="btn btn-success" disabled="disabled">ALL ' . $a . '(' . $ex1[$key] . ')</button><br>';


                            }
                        }

                        ?></td>
                    <td><?php


                        foreach ($ex4 as $a4) {
                            if ($a == "") {
                                echo "N/A";
                                continue;
                            }
                            echo "<li>" . $a4 . "</li>";
                        }
                        ?></td>
                    <td><?php echo $v->ph_agentdiscount; ?></td>
                    <td><?php echo ($v->total*$v->ph_agentdiscount/100); ?></td>
                </tr>

                <?php $i++;
            } ?>
            </tbody>
        </table>
    </div>
</div>

</div>


</div>
<script>

    function funct_change_status(a,b) {
        if(b == 2) {
            $(".ticket" + a).attr('disabled', true);
        }
        $.ajax({
            url: '<?php echo base_url()?>admin/ordercontrol/op_ticket_print_status',
            type: 'POST',
            data: {'pacorderid': a,'op_ticket_print_status':b},
            dataType: "JSON",
            success: function(data){


////console.log(data.s[0].ticket_status);
                if(data.s[0].ticket_status == 2)
                {
                    a.attr('disabled',true);

                }
////console.log(data1);
            }
        });
//var b = $(this).val();
// alert(b);return false;

    }

    /*    $(function () {
     $("#op_ticket_print_status").change(function () {
     alert("hello"); return false;
     var a = $(this);
     if($("#op_ticket_print_status").val() == 2)
     {
     a.attr('disabled',true);

     }
     //return false;
     $.ajax({
     url: ' echo base_url()?>admin/ordercontrol/op_ticket_print_status',
     type: 'POST',
     data: {'pacorderid': $(this).attr('data-id'),'op_ticket_print_status':$("#op_ticket_print_status").val()},
     dataType: "JSON",
     success: function(data){


     ////console.log(data.s[0].ticket_status);
     if(data.s[0].ticket_status == 2)
     {
     a.attr('disabled',true);

     };
     ////console.log(data1);
     }
     });
     });
     });*/

    function ticket_print(a) {
//alert(a);
        window.print();

    }

    function update_com(a, b) {
//alert(a +b);
        $("#com_name_update").val(a);
        $("#com_id").val(b);
        $("#update_com").css('display', 'block');
        $("#add_com").css('display', 'none');
        $("#com_name_update").focus();

    }
    function UpdateCancel() {
        $("#add_com").css('display', 'block');
        $("#update_com").css('display', 'none');
        return false;
    }


</script>



<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>

<!-- Include Date Range Picker -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

<script>
    $(document).ready(function(){
        $(".advance_btn").click(function(){
            $("#advance_search_d").slideToggle();
        });

    })
</script>

<script src="<?php echo admin_url();?>js/bootstrap-datetimepicker.min.js"></script>
<link href="<?php echo admin_url();?>css/bootstrap-datetimepicker.css" rel="stylesheet">


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<link rel="stylesheet"

      href="https://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css"></style>
<script type="text/javascript"

src="https://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
                                                                      <script type="text/javascript"

src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>


                                                                            <link rel="stylesheet"

href="https://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css"></style>
<script type="text/javascript"

        src="https://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
<script type="text/javascript"

        src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<script>
    $(document).ready(function() {
       // $.noConflict();
        var table = $('#table1').DataTable();
    });
</script>