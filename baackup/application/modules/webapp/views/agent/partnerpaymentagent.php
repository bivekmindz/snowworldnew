<style>
    .capbox {
        border-width: 0px 12px 0px 0px;
        display: inline-block;
        *display: inline; zoom: 1; /* FOR IE7-8 */
        padding: 8px 43px 8px 8px;
    }

    .capbox-inner {
        font: bold 12px arial, sans-serif;
        color: #000000;
        -moz-border-radius: 4px;
        -webkit-border-radius: 4px;
        border-radius: 4px;
    }

    #CaptchaDiv {
        font: bold 30px verdana, arial, sans-serif;
        font-style: italic;
        color: #000000;
        background-color: #FFFFFF;
        padding: 11px 30px;
        -moz-border-radius: 4px;
        -webkit-border-radius: 4px;
        border-radius: 4px;
    }

    #CaptchaDivwallet {
        font: bold 30px verdana, arial, sans-serif;
        font-style: italic;
        color: #000000;
        background-color: #FFFFFF;
        padding: 11px 30px;
        -moz-border-radius: 4px;
        -webkit-border-radius: 4px;
        border-radius: 4px;
    }

    #CaptchaInputwallet { width: 135px; font-size:15px;min-height: 38px;
        margin: 7px 0px; }

    #CaptchaInput { width: 135px; font-size:15px;min-height: 38px;
        margin: 7px 0px; }
.ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only btn btn-custom btn-prime

{
    border: none;
    padding: 8px 39px;
    font-size: large;
    background-color: #303641;
    color: #fff;
    border-radius: 5px;
}
</style>

<h1>Dashboard</h1>
        <div class="arrow-ri"><span><i class="fa fa-home"></i></span></div>
        <div class="show-data">
            <h1>Showing data for 21-06-2017</h1>
        </div>
        <div class="row">
            <div class="stepl">
                <form id="rootwizard" method="post" action="#" class="form-horizontal form-wizard">
                    <div class="steps-progress">
                        <div class="progress-indicator"></div>
                    </div>
                    <ul>
                        <li class="active"> <a href="partnerbookingsearch" ><span>1</span>Search</a> </li>
                        <li class="active" > <a href="agentpackagesstep"><span>2</span>Package</a> </li>
                        <li class="active"> <a href="agentaddones" ><span>3</span>Addons</a> </li>
                        <li class="active"> <a href="partnersummary" ><span>4</span>Summary</a> </li>
                        <li class="completed"> <a href="partnerpaymentagent" ><span>5</span>Payment</a> </li>
                    </ul>
                </form>
            </div>
        </div>
        <div class="row" style="background-color: #f9f9f9;">
            <div class="contactne1">
                <div class="col-md-12 col-md-offset-0">

                    <ul class="nav nav-tabs bordered">
                        <!-- available classes "bordered", "right-aligned" -->
                        <li class="active "> <a href="#home" data-toggle="tab" class="fo act" aria-expanded="false"> <span class="visible-xs"><i class="entypo-home"></i></span> <span class="hidden-xs">Wallet</span> </a> </li>
                        <li class=""> <a href="#profile" data-toggle="tab"  class="fo" aria-expanded="true"> <span class="visible-xs"><i class="entypo-user"></i></span> <span class="hidden-xs">Payment Gatway</span> </a> </li>

                    </ul>
                    <div class="tab-content contt">
                        <div class="tab-pane  active" id="home">
                            <form method="POST" name="customerData" action="ordersucesswallet" id="ccformwallet" onsubmit="return checkformwallet(this);">

                            <div class="wallwt">
                                <h1>Guest Details</h1>

                                <div class="form-payment">
                                    <div class="form-deta">

                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <select class="form-control " id="sel1"  required  name="titlewallet">


                                                    <option>Mr</option>
                                                    <option>Mrs</option>
                                                    <option>Ms</option>

                                                </select></div> <div class="col-sm-9">
                                                <input type="text" name="billing_namewallet" class="form-control input-lg" placeholder="Enter Your Name" pattern="[A-Za-z\s]+" required title="First Name should only contain  Alphabets. e.g. John" maxlength="60" value="<?php if($this->session->userdata('billing_name')!=''){echo $this->session->userdata('billing_name');} else { if($memuser->user_firstname!=''){echo $memuser->user_firstname.' '.$memuser->user_lastname;} } ?>">

                                              </div> </div>


                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <input type="email" name="billing_emailwallet" class="form-control input-lg" placeholder="Enter Your Email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" required title="The input is not a valid email address" maxlength="50" value="<?php if($this->session->userdata('billing_email')!=''){ echo $this->session->userdata('billing_email'); } else {echo $memuser->user_emailid;} ?>">
                                               </div>
                                            <div class="col-sm-6">
                                                <input type="tel" name="billing_telwallet" class="form-control input-lg" placeholder="Enter Your Phone Number" pattern="^\d{10}$" required title="Your Phone Number Should Only Contain  Numbers. e.g. 9999999999" maxlength="10" value="<?php if($this->session->userdata('billing_tel')!=''){ echo $this->session->userdata('billing_tel'); } else {echo $memuser->user_mobileno;} ?>">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-6">    <input type="text" name="billing_cometoknow" class="form-control input-lg" placeholder="Enter Where did you come to know?" pattern="[A-Za-z\s]+" required title="Enter Where did you come to know?" maxlength="50" value="<?php if($this->session->userdata('billing_cometoknow')!=''){ echo $this->session->userdata('billing_cometoknow'); } else {echo $memuser->billing_cometoknow;} ?>"></div> </div>


                                    </div>


                                </div>


                                <h1>Contact Details</h1>

                                <div class="form-payment">
                                    <div class="form-deta">

                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <textarea id="message_contact" name="billing_addresswallet" class="form-control" placeholder="Write Your Address"  ><?php if($this->session->userdata('billing_address')!=''){ echo $this->session->userdata('billing_address'); } else {echo $memuser->user_Address; } ?></textarea>

                                            </div> </div>


                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <input type="text" name="billing_citywallet" class="form-control input-lg" placeholder="Enter Your City" pattern="[A-Za-z\s]+"  title="City Name should only contain  Alphabets. e.g. Delhi" maxlength="20" value="<?php  if($this->session->userdata('billing_city')!=''){ echo $this->session->userdata('billing_city'); } else {echo $memuser->user_city; } ?>"></div>
                                            <div class="col-sm-6">
                                                <input type="text" name="billing_statewallet" class="form-control input-lg" placeholder="Enter Your State" pattern="[A-Za-z\s]+"  title="State Name should only contain  Alphabets. e.g. Delhi" maxlength="20" value="<?php  if($this->session->userdata('billing_state')!=''){ echo $this->session->userdata('billing_state'); } else {echo $memuser->user_state; } ?>">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <input type="text" name="billing_zipwallet" class="form-control input-lg" placeholder="Enter Your Zipcode"   title="Zip should only contain  Number. e.g. 110014" maxlength="6"  value="<?php if($this->session->userdata('billing_zip')!=''){ echo $this->session->userdata('billing_zip'); } else {echo $memuser->user_pincodes;} ?>">
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" name="billing_countrywallet" class="form-control input-lg" placeholder="Enter Your Country" pattern="[A-Za-z\s]+"  title="Country Name should only contain  Alphabets. e.g. Delhi" maxlength="20" value="<?php if($this->session->userdata('billing_country')!=''){ echo $this->session->userdata('billing_country'); } else {echo $memuser->user_country;} ?>">
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <div id="CaptchaDivwallet" style="float:right;"></div>  </div>
                                            <div class="col-sm-6">
                                                <input type="hidden" id="txtCaptchawallet">
                                                <input type="text" name="CaptchaInputwallet" id="CaptchaInputwallet" size="15" class="form-control input-lg"><br>
                                            </div>
                                        </div>

                                        <div class="col-xs-12">
                                            <div class="check-input">

                                                <input type="checkbox" id="creditTermsCheck" name="checkbox" required="" title="Accept Tearm And Condition">
                                                &nbsp;I Agree to the <a href="tearmcondition" target="_blank"> Terms Conditions</a>

                                            </div>
                                        </div>
                                        <input type="hidden" name="tid" id="tid" value="12345" readonly />
                                        <input type="hidden" name="merchant_id" value="<?php echo $ccavRequestHandler->pg_merchant_id; ?>"/>
                                        <input type="hidden" name="order_id" value="<?php echo $ccavRequestHandler->pg_prefix; ?>_<?php echo $paymentpac->pacorderid; ?>"/>
                                        <input type="hidden" name="order_idval" value="<?php echo $paymentpac->pacorderid; ?>"/>
                                        <input type="hidden" name="user_id" value="<?php echo $this->session->userdata('skiindia'); ?>"/>
                                        <input type="hidden" id="amount_s" name="amount" value="<?php echo $paymentpac->total; ?>" />
                                        <input type="hidden" name="currency" value="<?php echo $ccavRequestHandler->pg_currency; ?>"/>
                                        <input type="hidden" name="redirect_url" value="<?php echo $ccavRequestHandler->pg_sucess_link; ?>"/>
                                        <input type="hidden" name="cancel_url" value="<?php echo $ccavRequestHandler->pg_fail_link; ?>"/>
                                        <input type="hidden" name="language" value="<?php echo $ccavRequestHandler->pg_language; ?>"/>
                                        <div class="checkout">
                                            <button id="" name="submit" onclick="return cvalid()" type="submit" role="button" aria-disabled="false" value="cartses" style="border: none;padding: 8px 39px;font-size: large;background-color: #303641;color: #fff;border-radius: 5px;"><span class="ui-button-text ui-c">Checkout</span></button>
                                            <script type="text/javascript">

                                                // Captcha Script

                                                function checkformwallet(theform){
                                                    var why = "";

                                                    if(theform.CaptchaInputwallet.value == ""){
                                                        why += "- Please Enter CAPTCHA Code.\n";
                                                    }
                                                    if(theform.CaptchaInputwallet.value != ""){
                                                        if(ValidCaptchawallet(theform.CaptchaInputwallet.value) == false){
                                                            why += "- The CAPTCHA Code Does Not Match.\n";
                                                        }
                                                    }
                                                    if(why != ""){
                                                        alert(why);
                                                        return false;
                                                    }
                                                }

                                                var a = Math.ceil(Math.random() * 9)+ '';
                                                var b = Math.ceil(Math.random() * 9)+ '';
                                                var c = Math.ceil(Math.random() * 9)+ '';
                                                var d = Math.ceil(Math.random() * 9)+ '';
                                                var e = Math.ceil(Math.random() * 9)+ '';

                                                var code = a + b + c + d + e;
                                                document.getElementById("txtCaptchawallet").value = code;
                                                document.getElementById("CaptchaDivwallet").innerHTML = code;

                                                // Validate input against the generated number

                                                function ValidCaptchawallet(){
                                                    var str1 = removeSpaces(document.getElementById('txtCaptchawallet').value);
                                                    var str2 = removeSpaces(document.getElementById('CaptchaInputwallet').value);
                                                    var str3 = $("#amount_s").val();
                                                    var str4 = "<?php echo $paymentpac->total; ?>";


                                                    if (str1 == str2 && str3 == str4){

                                                        var formData = new FormData($("#ccformwallet")[0]);
                                                        $.ajax({
                                                            url: '<?php echo base_url(); ?>webapp/Addones/addagentwalet',
                                                            type: 'POST',
                                                            cache: false,
                                                            data: formData,
                                                            async: false,
                                                            processData: false,
                                                            contentType: false,
                                                            success: function(data){

                                                               //  alert(data);
                                                              //   return false;
                                                            }
                                                        });
                                                        return true;
                                                    }else{
                                                        return false;
                                                    }
                                                }

                                                // Remove the spaces from the entered and generated code
                                                function removeSpaces(string){
                                                    return string.split(' ').join('');
                                                }
                                            </script>
                                        </div>


                                    </div>


                                </div>

                            </div>
                                <script type="text/javascript">

                                    // Captcha Script

                                    function checkformwallet(theform){
                                        var why = "";

                                        if(theform.CaptchaInputwallet.value == ""){
                                            why += "- Please Enter CAPTCHA Code.\n";
                                        }
                                        if(theform.CaptchaInputwallet.value != ""){
                                            if(ValidCaptchawallet(theform.CaptchaInputwallet.value) == false){
                                                why += "- The CAPTCHA Code Does Not Match.\n";
                                            }
                                        }
                                        if(why != ""){
                                            alert(why);
                                            return false;
                                        }
                                    }

                                    var a = Math.ceil(Math.random() * 9)+ '';
                                    var b = Math.ceil(Math.random() * 9)+ '';
                                    var c = Math.ceil(Math.random() * 9)+ '';
                                    var d = Math.ceil(Math.random() * 9)+ '';
                                    var e = Math.ceil(Math.random() * 9)+ '';

                                    var code = a + b + c + d + e;
                                    document.getElementById("txtCaptchawallet").value = code;
                                    document.getElementById("CaptchaDivwallet").innerHTML = code;

                                    // Validate input against the generated number
                                    function ValidCaptchawallet(){
                                        var str1 = removeSpaces(document.getElementById('txtCaptchawallet').value);
                                        var str2 = removeSpaces(document.getElementById('CaptchaInputwallet').value);
                                        var str3 = $("#amount_s").val();
                                        var str4 = "<?php echo $paymentpac->total; ?>";

                                        if (str1 == str2 && str3 == str4){

                                            var formData = new FormData($("#ccformwallet")[0]);
                                            $.ajax({
                                                url: '<?php echo base_url(); ?>webapp/Addones/addagentwalet',
                                                type: 'POST',
                                                cache: false,
                                                data: formData,
                                                async: false,
                                                processData: false,
                                                contentType: false,
                                                success: function(data){

                                                    //  alert(data);
                                                    //  return false;
                                                }
                                            });
                                            return true;
                                        }else{
                                            return false;
                                        }
                                    }

                                    // Remove the spaces from the entered and generated code
                                    function removeSpaces(string){
                                        return string.split(' ').join('');
                                    }
                                </script>

                            </form>

                        </div>
                        <div class="tab-pane" id="profile">

                            <form method="POST" name="customerData" action="ccavRequestHandler.php" id="ccform" onsubmit="return checkform(this);">
                            <div class="wallwt">
                                <h1>Guest Details</h1>

                                <div class="form-payment">
                                    <div class="form-deta">

                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <select class="form-control " id="sel1"  required style="    min-width: 72px;" name="title">


                                                    <option>Mr</option>
                                                    <option>Mrs</option>
                                                    <option>Ms</option>

                                                </select></div> <div class="col-sm-9"> <input type="text" name="billing_name" class="form-control input-lg" placeholder="Enter Your Name" pattern="[A-Za-z\s]+" required title="First Name should only contain  Alphabets. e.g. John" maxlength="60" value="<?php if($this->session->userdata('billing_name')!=''){echo $this->session->userdata('billing_name');} else { if($memuser->user_firstname!=''){echo $memuser->user_firstname.' '.$memuser->user_lastname;} } ?>"> </div> </div>


                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <input type="email" name="billing_email" class="form-control input-lg" placeholder="Enter Your Email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" required title="The input is not a valid email address" maxlength="50" value="<?php if($this->session->userdata('billing_email')!=''){ echo $this->session->userdata('billing_email'); } else {echo $memuser->user_emailid;} ?>"></div>
                                            <div class="col-sm-6">
                                                <input type="tel" name="billing_tel" class="form-control input-lg" placeholder="Enter Your Phone Number" pattern="^\d{10}$" required title="Your Phone Number Should Only Contain  Numbers. e.g. 9999999999" maxlength="10" value="<?php if($this->session->userdata('billing_tel')!=''){ echo $this->session->userdata('billing_tel'); } else {echo $memuser->user_mobileno;} ?>">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-6">  <input type="text" name="billing_cometoknow" class="form-control input-lg" placeholder="Enter Where did you come to know?" pattern="[A-Za-z\s]+" required title="Enter Where did you come to know?" maxlength="50" value="<?php if($this->session->userdata('billing_cometoknow')!=''){ echo $this->session->userdata('billing_cometoknow'); } else {echo $memuser->billing_cometoknow;} ?>"> </div> </div>


                                    </div>


                                </div>


                                <h1>Contact Details</h1>

                                <div class="form-payment">
                                    <div class="form-deta">

                                        <div class="form-group">
                                            <div class="col-sm-12">

                                                <textarea id="message_contact" name="billing_address" class="form-control input-lg" placeholder="Write Your Address"  ><?php if($this->session->userdata('billing_address')!=''){ echo $this->session->userdata('billing_address'); } else {echo $memuser->user_Address; } ?></textarea>
                                            </div> </div>


                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <input type="text" name="billing_city" class="form-control input-lg" placeholder="Enter Your City" pattern="[A-Za-z\s]+"  title="City Name should only contain  Alphabets. e.g. Delhi" maxlength="20" value="<?php  if($this->session->userdata('billing_city')!=''){ echo $this->session->userdata('billing_city'); } else {echo $memuser->user_city; } ?>"></div>
                                            <div class="col-sm-6">
                                                <input type="text" name="billing_state" class="form-control input-lg" placeholder="Enter Your State" pattern="[A-Za-z\s]+"  title="State Name should only contain  Alphabets. e.g. Delhi" maxlength="20" value="<?php  if($this->session->userdata('billing_state')!=''){ echo $this->session->userdata('billing_state'); } else {echo $memuser->user_state; } ?>">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <input type="text" name="billing_zip" class="form-control input-lg" placeholder="Enter Your Zipcode"   title="Zip should only contain  Number. e.g. 110014" maxlength="6"  value="<?php if($this->session->userdata('billing_zip')!=''){ echo $this->session->userdata('billing_zip'); } else {echo $memuser->user_pincodes;} ?>">
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" name="billing_country" class="form-control input-lg" placeholder="Enter Your Country" pattern="[A-Za-z\s]+"  title="Country Name should only contain  Alphabets. e.g. Delhi" maxlength="20" value="<?php if($this->session->userdata('billing_country')!=''){ echo $this->session->userdata('billing_country'); } else {echo $memuser->user_country;} ?>">
                                            </div>
                                        </div>



                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <div id="CaptchaDiv" style="float:right;"></div>
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="hidden" id="txtCaptcha">
                                                <input type="text" name="CaptchaInput" id="CaptchaInput" size="15" class="form-control input-lg">
                                            </div>
                                        </div>







                                        <div class="col-xs-12">
                                            <div class="check-input">

                                                <input type="checkbox" id="creditTermsCheck" name="checkbox" required title="Accept Tearm And Condition">
                                                &nbsp;I Agree to the <a href="tearmcondition" target="_blank"> Terms Conditions</a>

                                            </div>
                                        </div>
                                        <input type="hidden" name="tid" id="tid" value="12345" readonly />
                                        <input type="hidden" name="merchant_id" value="<?php echo $ccavRequestHandler->pg_merchant_id; ?>"/>
                                        <input type="hidden" name="order_id" value="<?php echo $ccavRequestHandler->pg_prefix; ?>_<?php echo $paymentpac->pacorderid; ?>"/>
                                        <input type="hidden" name="order_idval" value="<?php echo $paymentpac->pacorderid; ?>"/>
                                        <input type="hidden" name="user_id" value="<?php echo $this->session->userdata('ppid'); ?>"/>
                                        <input type="hidden" id="amount_s" name="amount" value="<?php echo $paymentpac->total; ?>" />
                                        <input type="hidden" name="currency" value="<?php echo $ccavRequestHandler->pg_currency; ?>"/>
                                        <input type="hidden" name="redirect_url" value="<?php echo $ccavRequestHandler->pg_sucess_link; ?>"/>
                                        <input type="hidden" name="cancel_url" value="<?php echo $ccavRequestHandler->pg_fail_link; ?>"/>
                                        <input type="hidden" name="language" value="<?php echo $ccavRequestHandler->pg_language; ?>"/>
                                        <div class="checkout">

                                            <button id="" name="submit" style="border: none;padding: 8px 39px;font-size: large;background-color: #303641;color: #fff;border-radius: 5px;" onclick="return cvalid()" type="submit" role="button" aria-disabled="false" value="cartses"><span class="ui-button-text ui-c">Checkout</span></button>
                                            <script type="text/javascript">

                                                // Captcha Script

                                                function checkform(theform){
                                                    var why = "";

                                                    if(theform.CaptchaInput.value == ""){
                                                        why += "- Please Enter CAPTCHA Code.\n";
                                                    }
                                                    if(theform.CaptchaInput.value != ""){
                                                        if(ValidCaptcha(theform.CaptchaInput.value) == false){
                                                            why += "- The CAPTCHA Code Does Not Match.\n";
                                                        }
                                                    }
                                                    if(why != ""){
                                                        alert(why);
                                                        return false;
                                                    }
                                                }

                                                var a = Math.ceil(Math.random() * 9)+ '';
                                                var b = Math.ceil(Math.random() * 9)+ '';
                                                var c = Math.ceil(Math.random() * 9)+ '';
                                                var d = Math.ceil(Math.random() * 9)+ '';
                                                var e = Math.ceil(Math.random() * 9)+ '';

                                                var code = a + b + c + d + e;
                                                document.getElementById("txtCaptcha").value = code;
                                                document.getElementById("CaptchaDiv").innerHTML = code;

                                                // Validate input against the generated number
                                                function ValidCaptcha(){
                                                    var str1 = removeSpaces(document.getElementById('txtCaptcha').value);
                                                    var str2 = removeSpaces(document.getElementById('CaptchaInput').value);
                                                    var str3 = $("#amount_s").val();
                                                    var str4 = "<?php echo $paymentpac->total; ?>";
                                                    if (str1 == str2 && str3 == str4){

                                                        var formData = new FormData($("#ccform")[0]);
                                                        $.ajax({
                                                            url: '<?php echo base_url(); ?>webapp/Addones/addagent',
                                                            type: 'POST',
                                                            cache: false,
                                                            data: formData,
                                                            async: false,
                                                            processData: false,
                                                            contentType: false,
                                                            success: function(data){

                                                             //   alert(data);
                                                             //   return false;
                                                            }
                                                        });
                                                        return true;
                                                    }else{
                                                        return false;
                                                    }
                                                }

                                                // Remove the spaces from the entered and generated code
                                                function removeSpaces(string){
                                                    return string.split(' ').join('');
                                                }
                                            </script>
                                        </div>


                                    </div>


                                </div>

                            </div>
                            </form>

                        </div>


                    </div>

                </div>
            </div>
        </div>
