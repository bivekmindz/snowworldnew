<form id="loginform" name="loginform" class="form_pa" method="post" action="" enctype="">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-6">
                <div class="top_headadin">
                    <h1>Edit Partner</h1>
                </div>

            </div>
            <div class="col-lg-6">
                <div class="top_headadin1">
                    <a href=""><i class="glyphicon glyphicon-arrow-left"></i>  Back</a>
                </div>

            </div>

        </div>


        <div class="row">
            <div class='flashmsg'>

                <?php echo validation_errors(); ?>
                <?php

                echo $message;
                if($this->session->flashdata('message')){
                    echo $this->session->flashdata('message');
                }
                ?>
            </div>
            <div class="add-form">

                <h1> Basic Information</h1>
               

                <div class="col-md-6">
                    <div class="form-group">
                        <label>Title</label>
                        <select class="form-control" name="title" required>
                            <option value="Mr" <?php if($agentviewwdata->agent_title=='Mr') { ?> selected <?php } ?>>Mr.</option>
                            <option value="Mrs" <?php if($agentviewwdata->agent_title=='Mrs') { ?> selected <?php } ?>>Mrs.</option>
                            <option value="Ms" <?php if($agentviewwdata->agent_title=='Ms') { ?> selected <?php } ?>>Ms.</option>
                        </select>

                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label>First Name</label>
                        <input type="text" name="first_name" class="form-control" placeholder="Enter First Name" pattern="[A-Za-z\s]+" required title="First Name should only contain  Alphabet. e.g. John" maxlength="60" value="<?php echo $agentviewwdata->agent_firstname; ?>">

                    </div>
                </div>



                <div class="col-md-6">
                    <div class="form-group">
                        <label>Last Name</label>
                        <input type="text" name="last_name" class="form-control" placeholder="Enter Last Name" pattern="[A-Za-z\s]+" required title="Last Name should only contain  Alphabet. e.g. Singh" maxlength="60" value="<?php echo $agentviewwdata->agent_lastname; ?>">

                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label>Email Address</label>
                        <input type="email" name="email" class="form-control" placeholder="Enter Your Email Address" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" required title="The input is not a valid email address" maxlength="50" value="<?php echo $agentviewwdata->agent_username; ?>" readonly>

                    </div>
                </div>


  <div class="serach_bar">
      <input type="submit" name="submit" id="submit" onclick="return functi()" value="Edit Partner" class="btn btn-lg btn-primary">
                </div>

                <div class="line-do"></div>


            </div>
        </div>




    </div>


</div>
</form>
