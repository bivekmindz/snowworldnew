<style type="text/css">

    .regiter_step {
        width: 100%;
        float: left;
        background-color: azure;
    }

    .regiter_step .stpe_con_all {
        width: 100%;
        float: left;
        padding: 20px 0px 20px 0px;
    }

    .regiter_step .stpe_con_all .left_contyi {
        width: 100%;
        float: left;
    }

    .regiter_step .stpe_con_all .left_contyi ul {
        width: 100%;
        float: left;
        list-style: none;
    }

    .regiter_step .stpe_con_all .left_contyi ul li {
        padding: 3px;
        float: left;
        width: 100%;
    }

    .regiter_step .stpe_con_all .left_contyi ul li a {
        text-decoration: none;
        border-left: 3px solid #d40735;
        background-color: #2657a6;
        padding: 10px 15px 10px 15px;
        color: #fff;
        font-weight: 700;
        width: 100%;
        float: left;
    }

    .acty {
        color: #fff !important;
        background-color: #d40735 !important;
        border-left: 3px solid #6fc27f !important;
    }

    .regiter_step .stpe_con_all .left_contyi ul li a:hover {
        color: #fff;
        background-color: #d40735;
        border-left: 3px solid #d40735;
    }

    .regiter_step .stpe_con_all .right_contyi {
        width: 100%;
        float: left;
        background-color: #fff;
    }

    .regiter_step .stpe_con_all .right_contyi .heading_tab {
        width: 100%;
        float: left;
        padding: 0px 20px 10px 20px;
        border-bottom: 1px dotted #CCC;
    }

    .regiter_step .stpe_con_all .right_contyi .heading_tab h1 {
        width: 100%;
        float: left;
        font-size: large;
    }

    .regiter_step .stpe_con_all .right_contyi .heading_tab h1 span {
        color: #2657a6;
        font-size: medium;
    }

    .regiter_step .stpe_con_all .right_contyi .form_details {
        width: 100%;
        float: left;
        padding: 25px 20px 10px 20px;
    }

    .regiter_step .stpe_con_all .right_contyi .form_details .form-group {
        width: 100%;
        float: left;
    }

    .regiter_step .stpe_con_all .right_contyi .form_details .form-group select {
        min-height: 35px;
    }

    .regiter_step .stpe_con_all .right_contyi .form_details .next_button {
        width: 100%;
        float: left;
        padding: 20px 0px 20px 0px;
        text-align: center;
    }

    .regiter_step .stpe_con_all .right_contyi .form_details .next_button a {

        background-color: #2657a6;
        padding: 7px 15px 7px 15px;
        line-height: 35px;
        border-radius: 2px;
        color: #fff;
    }

</style>

<section class="regiter_step">
    <div class="container">

        <div class="row">
            <div class="stpe_con_all">
                <div class="col-md-4">
                    <div class="left_contyi">
                        <ul>
                            <li><a href="agentform1"> Identity Verification</a></li>
                            <li><a href="agentform2" >Seller Details </a></li>
                            <li><a href="agentform3">Official Details</a></li>
                            <li><a href="agentform4" >Statutory Requirements</a></li>
                            <li><a href="agentform5" class="acty">Bank Details</a></li>
                            <li><a href="">Documents Required</a></li>

                        </ul>

                    </div>
                </div>

                <div class="col-md-8">

                    <div class="right_contyi">
                        <div class="heading_tab">
                            <h1>Bank Details </h1>
                        </div>
                        <form action="" method="post" enctype="">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Beneficiary Name</label>
                                        <input type="text" name="bank_benificialname" class="form-control-1" placeholder="Enter Beneficiary Name"  required title="Enter Beneficiary Name" maxlength="60">
                                    </div>

                                    <div class="col-md-6">
                                        <label>Beneficiary Account Number</label>
                                        <input type="text" name="bank_benificialaccno" class="form-control-1" placeholder="Enter Beneficiary Account Number"  required title="Enter Beneficiary Account Number" maxlength="60">
                                    </div>

                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label> Beneficiary Bank Name</label>
                                        <input type="text" name="bank_benificialbankname" class="form-control-1" placeholder="Enter Beneficiary Bank Name"  required title="Enter Beneficiary Bank Name" maxlength="60">
                                    </div>

                                    <div class="col-md-6">
                                        <label>Beneficiary Bank Branch Name </label>
                                        <input type="text" name="bank_benificialbranchname" class="form-control-1" placeholder="Enter Beneficiary Bank Branch Name"  required title="Enter Beneficiary Bank Branch Name" maxlength="60">
                                    </div>

                                </div>
                            </div>


                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Bank Address</label>
                                        <input type="text" name="bank_benificialaddress" class="form-control-1" placeholder="Enter Bank Address"  required title="Enter Bank Address" maxlength="60">
                                    </div>

                                    <div class="col-md-6">
                                        <label> IFSC Code/SORT/ABA </label>
                                        <input type="text" name="bank_benificialifsc" class="form-control-1" placeholder="Enter IFSC Code/SORT/ABA "  required title="Enter IFSC Code/SORT/ABA " maxlength="60">
                                    </div>

                                </div>
                            </div>


                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Swift Code</label>
                                        <input type="text" name="bank_benificialswiftcode" class="form-control-1" placeholder="Enter Swift Code"  required title="Enter Swift Code" maxlength="60">
                                    </div>

                                    <div class="col-md-6">
                                        <label>IBAN No</label>
                                        <input type="text" name="bank_benificialibanno" class="form-control-1" placeholder="Enter IBAN No"  required title="Enter IBAN No" maxlength="60">
                                    </div>

                                </div>
                            </div>

                            <h2> Intermediatery bank</h2>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Bank name</label>
                                        <input type="text" name="intermidiatebankname" class="form-control-1" placeholder="Enter Bank name"  required title="Enter Bank name" maxlength="60">
                                    </div>

                                    <div class="col-md-6">
                                        <label>Bank Address</label>
                                        <input type="text" name="intermidiatebankaddress" class="form-control-1" placeholder="Enter Bank Address"  required title="Enter Bank Address" maxlength="60">
                                    </div>

                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>SWIFT Code</label>
                                        <input type="text" name="intermidiatebankswiftcode" class="form-control-1" placeholder="Enter SWIFT Code"  required title="Enter SWIFT Code" maxlength="60">
                                    </div>

                                    <div class="col-md-6">
                                        <label>ECS form duly signed by authorised person attached?</label>
                                        </label>
                                        <select  name="bank_ecs" class="form-control-1">
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>

                                    </div>

                                </div>
                            </div>

                            <div class="next_button">
                                <input type="submit" value="Submit" name="Submit" class="btn_1">
                            </div>


                        </form>
                    </div>

                </div>

            </div>

        </div>
    </div>
</section>