<style type="text/css">

    .regiter_step {
        width: 100%;
        float: left;
        background-color: azure;
    }

    .regiter_step .stpe_con_all {
        width: 100%;
        float: left;
        padding: 20px 0px 20px 0px;
    }

    .regiter_step .stpe_con_all .left_contyi {
        width: 100%;
        float: left;
    }

    .regiter_step .stpe_con_all .left_contyi ul {
        width: 100%;
        float: left;
        list-style: none;
    }

    .regiter_step .stpe_con_all .left_contyi ul li {
        padding: 3px;
        float: left;
        width: 100%;
    }

    .regiter_step .stpe_con_all .left_contyi ul li a {
        text-decoration: none;
        border-left: 3px solid #d40735;
        background-color: #2657a6;
        padding: 10px 15px 10px 15px;
        color: #fff;
        font-weight: 700;
        width: 100%;
        float: left;
    }

    .acty {
        color: #fff !important;
        background-color: #d40735 !important;
        border-left: 3px solid #6fc27f !important;
    }

    .regiter_step .stpe_con_all .left_contyi ul li a:hover {
        color: #fff;
        background-color: #d40735;
        border-left: 3px solid #d40735;
    }

    .regiter_step .stpe_con_all .right_contyi {
        width: 100%;
        float: left;
        background-color: #fff;
    }

    .regiter_step .stpe_con_all .right_contyi .heading_tab {
        width: 100%;
        float: left;
        padding: 0px 20px 10px 20px;
        border-bottom: 1px dotted #CCC;
    }

    .regiter_step .stpe_con_all .right_contyi .heading_tab h1 {
        width: 100%;
        float: left;
        font-size: large;
    }

    .regiter_step .stpe_con_all .right_contyi .heading_tab h1 span {
        color: #2657a6;
        font-size: medium;
    }

    .regiter_step .stpe_con_all .right_contyi .form_details {
        width: 100%;
        float: left;
        padding: 25px 20px 10px 20px;
    }

    .regiter_step .stpe_con_all .right_contyi .form_details .form-group {
        width: 100%;
        float: left;
    }

    .regiter_step .stpe_con_all .right_contyi .form_details .form-group select {
        min-height: 35px;
    }

    .regiter_step .stpe_con_all .right_contyi .form_details .next_button {
        width: 100%;
        float: left;
        padding: 20px 0px 20px 0px;
        text-align: center;
    }

    .regiter_step .stpe_con_all .right_contyi .form_details .next_button a {

        background-color: #2657a6;
        padding: 7px 15px 7px 15px;
        line-height: 35px;
        border-radius: 2px;
        color: #fff;
    }

</style>

<section class="regiter_step">
    <div class="container">

        <div class="row">
            <div class="stpe_con_all">
                <div class="col-md-4">
                    <div class="left_contyi">
                        <ul>
                            <li><a href=""> Identity Verification</a></li>
                            <li><a href="">Seller Details </a></li>
                            <li><a href="agentform3"  class="acty">Official Details</a></li>
                            <li><a href="">Statutory Requirements</a></li>
                            <li><a href="">Bank Details</a></li>
                            <li><a href="">Documents Required</a></li>

                        </ul>

                    </div>
                </div>

                <div class="col-md-8">

                    <div class="right_contyi">
                        <div class="heading_tab">
                            <h1>Official to Lialise with Chiliad Procons </h1>
                        </div>
                        <form action="" method="post" enctype="">

                            <div class="form_details">




                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Primary Contact Person</label>
                                            <input type="text" name="primarycontact_name" class="form-control-1" placeholder="Enter Primary Contact Person" pattern="[A-Za-z\s]+" required title="Primary Contact Person Name should only contain  letters. e.g. John" maxlength="60">
                                        </div>
                                        <div class="col-md-6">
                                            <label>Email
                                            </label>
                                            <input type="email" name="primarycontact_email_mobile" class="form-control-1" placeholder="Enter  Email" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="The input is not a valid email address" maxlength="50">

                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Mobile</label>
                                            <input type="tel" name="primarycontact_telephone" class="form-control-1" placeholder="Enter Mobile Number" pattern="^\d{10}$" required title="Mobile Number Should Only Contain  Letters. e.g. 9999999999" maxlength="10">
                                        </div>
                                        <div class="col-md-6">
                                            <label>Secondary Contact Person
                                            </label>
                                            <input type="text" name="secondrycontact_name" class="form-control-1" placeholder="Enter Secondary Contact Person" pattern="[A-Za-z\s]+" required title="Secondary Contact Person Name should only contain  letters. e.g. John" maxlength="60">

                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Email / Mobile
                                            </label>
                                            <input type="email" name="secondrycontact_email_phone" class="form-control-1" placeholder="Enter  Email" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="The input is not a valid email address" maxlength="50">
                                        </div>
                                        <div class="col-md-6">
                                            <label>Residence Telephone

                                            </label>
                                            <input type="tel" name="secondrycontact_telephone" class="form-control-1" placeholder="Enter  Phone Number" pattern="^\d{10}$" required title=" Phone Number Should Only Contain  Letters. e.g. 9999999999" maxlength="10">

                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Management Executives</label>
                                            <input type="text" name="management_executives_name" class="form-control-1" placeholder="Management Executives  Name" pattern="[A-Za-z\s]+" required title=" Management Executives should only contain  letters. e.g. Singh" maxlength="60">
                                        </div>
                                        <div class="col-md-6">
                                            <label>Email

                                            </label>
                                            <input type="email" name="management_executives_email_phone" class="form-control-1" placeholder="Management Executives Email" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="The input is not a valid email address" maxlength="50">

                                        </div>
                                    </div>
                                </div>



                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Branches( if any)</label>
                                            <input type="text" name="branches" class="form-control-1"  placeholder="Enter Branches">
                                        </div>

                                        <div class="col-md-6">
                                            <label>Year of establishment </label>
                                            <input type="text" name="yearofestablism" class="form-control-1"  required placeholder="Enter year of establism" maxlength="4">
                                        </div>

                                    </div>
                                </div>



                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label>Type of Organization - Whether Partnership /
                                                Proprietorship</label>
                                            <select class="form-control-1" name="organisationtype" >
                                                <option value="Proprietorship">Proprietorship</option>
                                                <option value="Limited">Limited</option>

                                                <option value="Limited Liability">Limited Liability</option>

                                                <option value="Any other">Any other</option>

                                            </select>
                                        </div>



                                    </div>
                                </div>



                                <div class="next_button">
                                    <input type="submit" value="Submit" name="Submit" class="btn_1">
                                </div>

                            </div>
                        </form>
                    </div>

                </div>

            </div>

        </div>
    </div>
</section>