<section class="login_rege">
    <div class="container">

        <div class="row">

            <div class="login box_style_1">


                <h3 class="form-signin-heading to_p">
                    <i class="icon-user"></i> Agent Registration Form
                </h3><span id="updateRegister">
                <form id="loginform" name="loginform" class="form_pa" method="post" action="" enctype="">
                <input type="hidden" name="loginform" value="loginform">


            <div class="row">
              <div class="col-md-12">

                            <div class="form-group">
                      <div class="row">
                               <div class='flashmsg'>
                                    <?php
                                    echo $mesg;
                                    echo validation_errors(); ?>
                                    <?php
                                    if ($this->session->flashdata('message')) {
                                        echo $this->session->flashdata('message');
                                    }
                                    ?>
                                </div>
              <div class="col-md-2">
                        <label>Title</label>
<select id="" name="title" class="form-control-1 select_mr" size="1" required> <option value="Mr">Mr.</option>
  <option value="Mrs">Mrs.</option>
  <option value="Ms">Ms.</option>
</select>                         </div>

                            <div class="col-md-5">
                        <label>First Name</label>
                        <input type="text" name="first_name" class="form-control-1" placeholder="Enter First Name"
                               pattern="[A-Za-z\s]+" required
                               title="First Name should only contain  Alphabet. e.g. John" maxlength="60">

                          </div>
                            <div class="col-md-5">
                        <label>Last Name </label>

<input type="text" name="last_name" class="form-control-1" placeholder="Enter Last Name" pattern="[A-Za-z\s]+" required
       title="Last Name should only contain  Alphabet. e.g. Singh" maxlength="60">


                            </div>
                      </div>
                    </div>


                           <div class="form-group">
                      <div class="row">
              <div class="col-md-6">
                        <label>Mobile Number</label>

                         <input type="tel" name="mobile_number" class="form-control-1"
                                placeholder="Enter Your Phone Number" pattern="^\d{10}$" required
                                title="Your Phone Number Should Only Contain  Letters. e.g. 9810445577" maxlength="10">

                          </div>
                            <div class="col-md-6">
                        <label>Email Address </label>

                         <input type="email" name="email" class="form-control-1" placeholder="Enter Your Email Address"
                                pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" required
                                title="The input is not a valid email address" maxlength="50">

                            </div>
                      </div>
                    </div>

                    <div class="form-group">
                      <div class="row">
              <div class="col-md-6">
                        <label>Password</label>
                        <input type="password" id="password" name="password" class="form-control-1"
                               placeholder="Enter Password" required title="Enter Password" maxlength="35"
                               minlength="6">


                          </div>
                            <div class="col-md-6">
                        <label>Confirm Password </label>
                         <input type="password" id="confirm_password" name="cpassword" class="form-control-1"
                                placeholder="Enter Confirm Password" required title="Enter Confirm Password"
                                maxlength="35" minlength="6">
                                <span class="err00" style="color: red;display: none">PASSWORD DON'T MATCH</span>


                            </div>
                      </div>
                    </div>
                  <script>

                     function functi() {
                         var password = document.getElementById("password");
                         var confirm_password = document.getElementById("confirm_password");

                         if (password.value != confirm_password.value) {
                             $(".err00").css('display', 'block').fadeOut(3000);

                             return false;
                         } else {
                             confirm_password.setCustomValidity('');
                         }
                     }
                  </script>




                            <div class="form-group">
                      <div class="row">
              <div class="col-md-2">
                       <input type="submit" name="submit" id="submit" onclick="return functi()" value="Register"
                              class="btn_1">
                          </div> </div>

                          <div class="row">
                            <div class="col-md-6">
                    <label class="checkbox">
        <a href="<?php echo base_url(); ?>partnerlogin" class="foge">User Login</a>
      </label>
                            </div>
                      </div>
                    </div>



              </div>
            </div>

</form></span>
            </div>
            <div>


            </div>


        </div>
    </div>


</section>

<!-- End Container -->

