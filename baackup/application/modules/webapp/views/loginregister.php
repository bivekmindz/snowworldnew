<script src="<?php echo base_url()?>assets/js/jquery-1.9.1.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>

<!-- jQuery Form Validation code -->
<style>
    input.error{border:1px solid red!important;}
    select.error{border:1px solid red!important;}
    textarea.error{border:1px solid red!important;}
    .error{border:1px solid red;}
    label.error{border:0px solid red!important; color:red; font-weight: normal; display:inline; }
    .sessionerror{border:0px solid red!important; color:red; font-weight: normal; }
    .skep {
        width: 100%;
        float: left;
        text-align: center;
        padding: 35px 0px 10px 0px;
    }

    .skep a {
        color: #fff;
        background-color: #2957A4;
        border-color: #2957A4;
        color: #fff;
        padding: 8px 20px 8px 20px;
        font-weight: 600;
        border-radius: 3px;
    }
</style>

<script language='javascript'>

    // When the browser is ready...
    $(document).ready(function() {

        // Setup form validation on the #register-form element
        $("#addCont").validate({
            // Specify the validation rules

            rules: {
                emailmob     : {
                    required: true,
                    email: true
                },
                acctype      : "required",
                pwd          : {
                    required  : true,
                    minlength : 6
                },

            },

            submitHandler: function(form) {
                form.submit();
            }
        });

    });

</script>

<section class="step_payment">


    <div class="container main-container">


        <div id="default-screen" class="content-part"><span id="updateFullPage">
            
            
            
					<section class="row block" id="ticketsAddOnesBlock">
						<div id="searchResult"><span id="ticketsAddOnsOpen"></span><span id="step1Done">
								<div id="ticketsAddOnsDone" class="ticketsAddOnsDone doneBlock aboveAll">
									<div class="stepHeader stepDone stepDoneHere relative row" id="ticketsAddOnsInfo">
										<p class="fRight subHead">Session Date:<?php

                                            $date_array = explode("/",$this->session->userdata('txtDepartDate')); // split the array
                                            $var_day = $date_array[0]; //day seqment
                                            $var_month = $date_array[1]; //month segment
                                            $var_year = $date_array[2]; //year segment
                                            $new_date_format = strtotime("$var_year-$var_month-$var_day"); // join them together

                                            $input = ("$var_year$var_month$var_day");



                                            echo date(' jS F Y', $new_date_format); ?>&nbsp;|&nbsp;Session Time: <?php

                                            $desttype=$this->session->userdata('destinationType');


                                            if($timeslotses->timeslot_to>12)
                                            {
                                                $apmvar="PM";
                                                $timeslotses->timeslot_to= ($timeslotses->timeslot_to-12);
                                            }elseif($desttype==12) {
                                                $apmvar="PM";
                                            }
                                            else {
                                                $apmvar="AM";
                                            }if($desttype>12)
                                            { $desttype= ($desttype-12);
                                                $apmvar1="PM";}elseif($desttype==12) {
                                                $apmvar1="PM";
                                            }else {

                                                $apmvar1="AM";
                                            }



                                            echo $desttype; ?>:<?php   echo $timeslotses->timeslot_minfrom; ?><?php echo $apmvar1; ?> - <?php  echo $timeslotses->timeslot_to; ?>:<?php  echo $timeslotses->timeslot_minto; ?> <?php echo $apmvar; ?>   | No. of Visiters: <?php echo $this->session->userdata('packageqtyval'); ?></p>
<form id="j_idt135" name="j_idt135" method="post" action="" enctype="application/x-www-form-urlencoded">
<input type="hidden" name="j_idt135" value="j_idt135">
<a id="j_idt135:step1Edit" href="packages" class="ui-commandlink ui-widget edit" onclick="">
												<span class="icon-pencil"></span></a>
<input type="hidden" name="javax.faces.ViewState" value="4914754771058361795:4671725937336393998" autocomplete="off"></form>
										<h2 class="relative">
											<span class="steps"><span class="stepNum">Step
													1</span></span>
										</h2>
										<p class="steps-title title1">Search
</p>
									</div>

								</div></span>

						</div>
					</section>


                    	<section class="row block" id="ticketsAddOnesBlock">
						<div id="searchResult"><span id="ticketsAddOnsOpen"></span><span id="step1Done">
								<div id="ticketsAddOnsDone" class="ticketsAddOnsDone doneBlock aboveAll">
									<div class="stepHeader stepDone stepDoneHere relative row" id="ticketsAddOnsInfo">
										<p class="fRight subHead">Ticket Cost :

<span><i class="icon-rupee" aria-hidden="true"></i><?php echo $this->session->userdata('cartpackageprice')."/-"; ?></span>

           </p>
<form id="j_idt135" name="j_idt135" method="post" action="" enctype="application/x-www-form-urlencoded">
<input type="hidden" name="j_idt135" value="j_idt135">
<a id="j_idt135:step1Edit" href="packagesstep" class="ui-commandlink ui-widget edit" onclick="">
												<span class="icon-pencil"></span></a>
<input type="hidden" name="javax.faces.ViewState" value="4914754771058361795:4671725937336393998" autocomplete="off"></form>
										<h2 class="relative">
											<span class="steps"><span class="stepNum">Step
													2</span></span>
										</h2>
										<p class="steps-title title2">Package
</p>
									</div>

								</div></span>

						</div>
					</section>
                    <section class="row block" id="ticketsAddOnesBlock">
						<div id="searchResult"><span id="ticketsAddOnsOpen"></span><span id="step1Done">
								<div id="ticketsAddOnsDone" class="ticketsAddOnsDone doneBlock aboveAll">
									<div class="stepHeader stepDone stepDoneHere relative row" id="ticketsAddOnsInfo">
										<p class="fRight subHead">Addons Cost :
<span><i class="icon-rupee" aria-hidden="true"></i><?php
    $valaddone=0;  $valadd=explode(",",$this->session->userdata('cartaddonprice'));
    foreach ($valadd as $key => $value)
    {
        $valaddone+=$value;
    } echo $valaddone."/-";
    ?></span>
</p>
<form id="j_idt135" name="j_idt135" method="post" action="" enctype="application/x-www-form-urlencoded">
<input type="hidden" name="j_idt135" value="j_idt135">
<a id="j_idt135:step1Edit" href="addones" class="ui-commandlink ui-widget edit" onclick="">
												<span class="icon-pencil"></span></a>
<input type="hidden" name="javax.faces.ViewState" value="4914754771058361795:4671725937336393998" autocomplete="off"></form>
										<h2 class="relative">
											<span class="steps"><span class="stepNum">Step
													3</span></span>
										</h2>
										<p class="steps-title title3">Addons
</p>
									</div>

								</div></span>

						</div>
					</section>



					<section class="row block" id="ticketsAddOnesBlock">
						<div id="searchResult"><span id="ticketsAddOnsOpen">
									<div id="ticketsAddOnsOpen" class="openBlock aboveAll"><span id="step1Header">
											<div class="stepHeader stepOpenHere row stepOpen">
												<p class="fRight subHead">Which ticket would you like to
													book?</p>
												<h2 class="relative">
													<span class="steps"><span class="stepNum">Step
															4</span></span>
												</h2>
												<p class="steps-title title4">Login / Register</p>
											</div></span><span id="refreshStep1Panel"><span id="step1AddonDisp">

<div id="AddOnsBlock" class="infoBlock relative addOnsBlock">
	<div class="ticketsWithAddOns">
		<div class="row ticketdescription mart20">


			<div class="row">
         <div class="col-md-12  col-bdr-right">



				<div class="panel-group" id="accordion">
    <div class="panel panel-default">
      <div class="panel-heading" style="background-color:#2EADF4;padding:4px 10px;">
        <h4 class="panel-title collapse1">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">Login <i class="more-minus fa fa-minus" style="float:right;cursor:pointer;"></i> <i class="more-plus fa fa-plus" style="float:right;cursor:pointer;"></i></a>
        </h4>
      </div>
      <div id="collapse1" class="panel-collapse collapse in">
        <div class="panel-body">

			<div class="panel panel-default">

                <div class="panel-body">

				<div class="flashmsg"><?php echo $message; ?></div>



                   <form method="post" action="" id="addCont" name="register-form"  class="formm">
                        <div class="form-group">
                            <label for="fname">Email Address</label>
                            <input type="email" name="emailmob" id="emailmob" class="form-control required_field" placeholder="Email " required>
                        </div>
                        <div class="form-group">
                            <label for="lname">Password</label>
                             <input type="password" class="form-control required_field" id="pwd" name="pwd" placeholder="Password" required>
						</div>




                        <div class="form-inline">
							<div class="form-group">
								<div class="buton" style="padding:0px 0px 0px 0px;">
								<button  name="submit" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only btn btn-custom btn-prime" type="submit" role="button" aria-disabled="false"  value="Sign In"><span class="ui-button-text ui-c">Sign In</span></button>&nbsp;
								</div>
							</div>

							<div class="form-group" style="float:right;">
								<label for="offers">
								   <a href="forgetpassword" target="_blank">Forgot Password?</a>
								</label>
							</div>
						</div>


                    </form>
                </div>

            </div>

		</div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading" style="background-color:#2EADF4;padding:4px 10px;">
        <h4 class="panel-title collapse2">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" class="collapsed">Register <i class="more-minus fa fa-minus" style="float:right;cursor:pointer;"></i> <i class="more-plus fa fa-plus" style="float:right;cursor:pointer;"></i></a>
        </h4>
      </div>
      <div id="collapse2" class="panel-collapse collapse">
        <div class="panel-body">

		<div class="panel panel-default">

                <div class="panel-body">
                    <form name="register-form" class="formm" id="registration-form" method="POST" action="">
                        <div class="flashmsg">    <?php echo validation_errors(); ?> <?php echo $msg; ?>  </div>



                        <div class="form-group">
                            <label for="fname">First Name</label>

 <input type="text" name="fname" class="form-control-1" placeholder="Enter First Name" pattern="[A-Za-z\s]+" required title="First Name should only contain  Alphabet. e.g. John" maxlength="60">


                        </div>
                        <div class="form-group">
                            <label for="lname">Last Name</label>

                            <input type="text" name="lastName" class="form-control-1" placeholder="Enter Last Name" pattern="[A-Za-z\s]+" required title="Last Name should only contain  Alphabet. e.g. Singh" maxlength="60">


                        </div>
                        <div class="form-group">
                            <label for="mobile">Mobile Number</label>
                            <div class="isd-mobile-input-section">
                              <span class="country-code-highlighter">
 <input type="tel" name="mobileno" class="form-control-1" placeholder="Enter Your Phone Number" pattern="^\d{10}$" required title="Your Phone Number Should Only Contain  Letters. e.g. 9810445577" maxlength="10">
</span>



                            </div>
                        </div>
                        <div class="form-group">
                            <label for="pemail">Email Address</label>
                             <input type="email" name="email" class="form-control-1" placeholder="Enter Your Email Address" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" required title="The input is not a valid email address" maxlength="50">


                        </div>
                        <div id="validationErrorMessage" class="has-error"><span class="help-block"></span></div>
                        <div class="form-group">
                            <label for="password">Password</label>
                           <input type="password" id="password" name="password" class="form-control-1" placeholder="Enter Password" required title="Enter Password" maxlength="35" minlength="6" >
                        </div>


                         <div class="form-group">
                            <label for="password">Confirm Password</label>
                           <input type="password" id="confirm_password" name="cpassword" class="form-control-1" placeholder="Enter Confirm Password" required title="Enter Confirm Password" maxlength="35" minlength="6" >
                              <span class="err00"  style="color: red;display: none">PASSWORD DON'T MATCH</span>
                        </div>


                        <div class="form-group">
                            <div class=" buton" style="padding:15px 0px 0px 0px;">
                            <button name="submit" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only btn btn-custom btn-prime" type="submit"  onclick="return functi()" role="button" aria-disabled="false" value="Sign up"><span class="ui-button-text ui-c">Sign up</span></button>&nbsp;
                                 <script>

                     function functi() {
                         var password = document.getElementById("password");
                         var confirm_password = document.getElementById("confirm_password");

                         if(password.value != confirm_password.value) {
                             $(".err00").css('display','block').fadeOut(3000);

                             return false;
                         } else {
                             confirm_password.setCustomValidity('');
                         }
                     }
                  </script>


                            </div>
                        </div>
                         <div class="form-group">
                            <input type="hidden" name="returnUrl" value="">
                        </div>
                    </form>
                </div>



            </div>

		</div>
      </div>
    </div>


                    <!--<script>
                        $(document).ready(function(){
                            $(.collapse).on('shown.bs.collapse', function(){
                                $(this).parent().find('.fa-plus').removeClass('fa-plus').addClass('fa-minus');
                            }).on('hidden.bs.collapse', function(){
                                $(this).parent().find('.fa-minus').removeClass('fa-minus').addClass('fa-plus');
                            });
                        });
                    </script>-->

                    <!--<script>
                        function toggleIcon(e) {
                        $(e.target)
                            .prev('.panel-heading')
                            .find(".more-less")
                            .toggleClass('fa-plus fa-minus');
                            }
                            $('.panel-group').on('hidden.bs.collapse', toggleIcon);
                            $('.panel-group').on('shown.bs.collapse', toggleIcon);
                    </script>-->

  </div>

        </div>

            <div class="skep">
      <a href="summary" style="font-size:18px;padding:6px 32px;">Skip</a>

       </div>



    </div>
		</div>



	</div>
</div>





</span></span>
									</div></span>

						</div>
					</section>














                    <section id="paymentBlock" class="row block relative"><span id="beforePanel3">
								<div id="summaryClose" class="stepHeader stepClose row summaryClose closeBlock">
									<p class="fRight subHead"></p>
									<h2 class="relative">
										<span class="steps"><span class="stepNum">Step 5</span></span>
									</h2>
									<p class="steps-title title5">Summary</p>
								</div></span><span id="step3open"></span>

					</section>

                    <section id="paymentBlock" class="row block relative"><span id="beforePanel3">
								<div id="summaryClose" class="stepHeader stepClose row summaryClose closeBlock">
									<p class="fRight subHead"></p>
									<h2 class="relative">
										<span class="steps"><span class="stepNum">Step 6</span></span>
									</h2>
									<p class="steps-title title6">Payment</p>
								</div></span><span id="step3open"></span>

					</section>
                    
                    
                    
                    
                    </span>
        </div>
    </div>

</section>
   
  