<form action="" method="post" enctype="multipart/form-data">
<section class="form_fill">


    <div class="container">

        <div class="col-md-10 col-md-offset-1">

            <div class="form-data">
                <div class="heading-t">
                    Agent Profile

                    <span>( Applicable to all direct agencies selling Skiindia Tickets )</span>

                </div>

                <div class="flashmsg"> <?php if($mesg!='') { echo $mesg; } ?> </div>



                <div class="col-md-12">
                    <div class="form-group">

                    <div class="row">
                        <div class="col-md-6">
                            <label>Login Type</label>
                            <select name="logintype"  class="form-control-1" required>
                               <option value="1">Corporate Login  </option>
                                <option value="2">Officer Login </option>

                            </select></div>
                        <div class="col-md-6">
                            <label>Country:</label>
                            <input type="text" name="emailcountry" class="form-control-1" placeholder="Enter Your Country" pattern="[A-Za-z\s]+" required title="Enter Your Country" maxlength="50">
                        </div>
                    </div>  </div>



                    <div class="form-group">

                        <div class="row">
                            <div class="col-md-6">
                                <label>Agency Name</label>

                                <input type="text" name="agencyname" class="form-control-1" placeholder="Enter Agency Name" required title="Agency Name should only contain  letters. e.g. John" maxlength="60">

                               </div>
                            <div class="col-md-6">
                                <label>Registered / Head Office Address </label>
                                <input type="text" name="office_address" class="form-control-1" placeholder="Enter Office Address"  required title="Enter Registered / Head Office Address " maxlength="60">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label>City</label>
                                <input type="text" name="city" class="form-control-1" placeholder="Enter City" pattern="[A-Za-z\s]+" required title="City Name should only contain  letters. e.g. Delhi" maxlength="60">

                            </div>
                            <div class="col-md-6">
                                <label>State</label>
                                <input type="text" name="state" class="form-control-1" placeholder="Enter State" pattern="[A-Za-z\s]+" required title="State Name should only contain  letters. e.g. Delhi" maxlength="60">

                            </div>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Email id</label>
                                <input type="email" name="username" class="form-control-1" placeholder="Enter Your Email" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="The input is not a valid email address" maxlength="50">




                            </div>
                            <div class="col-md-6">
                                <label>Password</label>
                                <input type="password" name="password" class="form-control-1" placeholder="Enter Password"  required  maxlength="35">

                            </div>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Pincode</label>
                                <input type="text" name="pincode" class="form-control-1" placeholder="Enter Your Pincode" pattern="^\d{6}$" required title="Your Pincode Should Only Contain  Number. e.g. 110011" maxlength="10">
                            </div>
                            <div class="col-md-6">
                                <label>Telephone</label>
                                <input type="tel" name="telephone" class="form-control-1" placeholder="Enter Your Telephone Number" pattern="^\d{10}$"  required title="Your Phone Number Should Only Contain  Number. e.g. 9999999999" maxlength="20">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Mobile</label>
                                <input type="tel" name="mobile" class="form-control-1" placeholder="Enter Your Phone Number" pattern="^\d{10}$" required title="Your Phone Number Should Only Contain  Number. e.g. 9999999999" maxlength="10">
                            </div>
                            <div class="col-md-6">
                                <label>Fax</label>
                                <input type="text" name="fax" class="form-control-1" placeholder="Enter Your Fax Number"  required title="Your Fax Number Should Only Contain  Number. e.g. 9999999999" maxlength="20"  pattern="^\d{10}$" >
                            </div>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Website</label>
                                <input type="text" name="website" class="form-control-1" placeholder="Enter Your Website"   maxlength="60">
                            </div>
                            <div class="col-md-6">
                                <label>Email</label>
                                <input type="email" name="email" class="form-control-1" placeholder="Enter Your Email" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="The input is not a valid email address" maxlength="50">
                            </div>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Status</label>
                                <select name="emailstatus" class="form-control-1">
                                <option value="International">International</option>
                                    <option value="Domestic">Domestic</option>
                                </select>
                                </select></div>
                            <div class="col-md-6">
                                <label>Currency in which payments will be made:</label>
                                <input type="text" name="emailcurrency" class="form-control-1" placeholder="Enter Your Currency" required title="Enter Your Currency" maxlength="50">
                            </div>
                        </div>
                    </div>






                    <h2>Official to Lialise with Chiliad Procons</h2>


                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Primary Contact Person</label>
                                <input type="text" name="primarycontact_name" class="form-control-1" placeholder="Enter Primary Contact Person" pattern="[A-Za-z\s]+" required title="Primary Contact Person Name should only contain  letters. e.g. John" maxlength="60">
                            </div>
                            <div class="col-md-6">
                                <label>Email
                                </label>
                                <input type="email" name="primarycontact_email_mobile" class="form-control-1" placeholder="Enter  Email" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="The input is not a valid email address" maxlength="50">

                            </div>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Mobile</label>
                                <input type="tel" name="primarycontact_telephone" class="form-control-1" placeholder="Enter Mobile Number" pattern="^\d{10}$" required title="Mobile Number Should Only Contain  Letters. e.g. 9999999999" maxlength="10">
                            </div>
                            <div class="col-md-6">
                                <label>Secondary Contact Person
                                </label>
                                <input type="text" name="secondrycontact_name" class="form-control-1" placeholder="Enter Secondary Contact Person" pattern="[A-Za-z\s]+" required title="Secondary Contact Person Name should only contain  letters. e.g. John" maxlength="60">

                            </div>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Email / Mobile
                                </label>
                                <input type="email" name="secondrycontact_email_phone" class="form-control-1" placeholder="Enter  Email" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="The input is not a valid email address" maxlength="50">
                            </div>
                            <div class="col-md-6">
                                <label>Residence Telephone

                                </label>
                                <input type="tel" name="secondrycontact_telephone" class="form-control-1" placeholder="Enter  Phone Number" pattern="^\d{10}$" required title=" Phone Number Should Only Contain  Letters. e.g. 9999999999" maxlength="10">

                            </div>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Management Executives</label>
                                <input type="text" name="management_executives_name" class="form-control-1" placeholder="Management Executives  Name" pattern="[A-Za-z\s]+" required title=" Management Executives should only contain  letters. e.g. Singh" maxlength="60">
                            </div>
                            <div class="col-md-6">
                                <label>Email

                                </label>
                                <input type="email" name="management_executives_email_phone" class="form-control-1" placeholder="Management Executives Email" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="The input is not a valid email address" maxlength="50">

                            </div>
                        </div>
                    </div>



                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Branches( if any)</label>
                                <input type="text" name="branches" class="form-control-1"  placeholder="Enter Branches">
                            </div>

                            <div class="col-md-6">
                                <label>Year of establishment </label>
                                <input type="text" name="yearofestablism" class="form-control-1"  required placeholder="Enter year of establism" maxlength="4">
                            </div>

                        </div>
                    </div>



                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <label>Type of Organization - Whether Partnership /
                                    Proprietorship</label>
                                <select class="form-control-1" name="organisationtype" >
                                    <option value="Proprietorship">Proprietorship</option>
                                    <option value="Limited">Limited</option>

                                    <option value="Limited Liability">Limited Liability</option>

                                    <option value="Any other">Any other</option>

                                </select>
                            </div>



                        </div>
                    </div>

                    <h2> Statutory Requirements : Mention NA where not applicable </h2>


                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label>LST No</label>
                                <input type="text" name="lstno" class="form-control-1" placeholder="Enter LST No"  required title="Enter LST No" maxlength="60">
                            </div>

                            <div class="col-md-6">
                                <label>CST No </label>
                                <input type="text" name="cstno" class="form-control-1" placeholder="Enter CST No"  required title="Enter CST No" maxlength="60">
                            </div>

                        </div>
                    </div>


                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Excise Registration No</label>
                                <input type="text" name="registrationno" class="form-control-1" placeholder="Enter Excise Registration No"  required title="Enter Excise Registration No" maxlength="60">
                            </div>

                            <div class="col-md-6">
                                <label>VAT No. / TIN No </label>
                                <input type="text" name="vatno" class="form-control-1" placeholder="Enter VAT No. / TIN No"  required title="Enter VAT No. / TIN No" maxlength="60">
                            </div>

                        </div>
                    </div>




                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label>PAN No</label>
                                <input type="text" name="panno" class="form-control-1" placeholder="Enter PAN No"  required title="Enter PAN No" maxlength="60">
                            </div>

                            <div class="col-md-6">
                                <label>Service Tax Registration No </label>
                                <input type="text" name="servicetaxno" class="form-control-1" placeholder="Enter Service Tax Registration No"  required title="Enter Service Tax Registration No" maxlength="60">
                            </div>

                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label>TAN No</label>
                                <input type="text" name="tanno" class="form-control-1" placeholder="Enter TAN No"  required title="Enter TAN No" maxlength="60">
                            </div>

                            <div class="col-md-6">
                                <label>PF No </label>
                                <input type="text" name="pfno" class="form-control-1" placeholder="Enter PF No"  required title="Enter PF No" maxlength="60">
                            </div>

                        </div>
                    </div>


                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label>ESIC No</label>
                                <input type="text" name="esisno" class="form-control-1" placeholder="Enter ESIC No"  required title="Enter ESIC No" maxlength="60">
                            </div>

                            <div class="col-md-6">
                                <label>Office / Establishment / Factory Registration
                                    No./ Labour Licence no </label>
                                <input type="text" name="officeregistrationno" class="form-control-1" placeholder="Enter Office / Establishment / Factory Registration
                                    No./ Labour Licence no"  required title="Enter Office / Establishment / Factory Registration
                                    No./ Labour Licence no" maxlength="60">
                            </div>

                        </div>
                    </div>


                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Any exemption from tax</label>
                                <input type="text" name="exceptiontax" class="form-control-1" placeholder="Enter Any exemption from tax"  required title="Enter Any exemption from tax" maxlength="60">
                            </div>

                            <div class="col-md-6">
                                <label>Others if any, mention the details with number </label>
                                <input type="text" name="otherexceptiontax" class="form-control-1" placeholder="Enter Others if any, mention the details with number"  required title="Enter Others if any, mention the details with number" maxlength="60">
                            </div>

                        </div>
                    </div>

                    <h2>Bank Details</h2>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Beneficiary Name</label>
                                <input type="text" name="bank_benificialname" class="form-control-1" placeholder="Enter Beneficiary Name"  required title="Enter Beneficiary Name" maxlength="60">
                            </div>

                            <div class="col-md-6">
                                <label>Beneficiary Account Number</label>
                                <input type="text" name="bank_benificialaccno" class="form-control-1" placeholder="Enter Beneficiary Account Number"  required title="Enter Beneficiary Account Number" maxlength="60">
                            </div>

                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label> Beneficiary Bank Name</label>
                                <input type="text" name="bank_benificialbankname" class="form-control-1" placeholder="Enter Beneficiary Bank Name"  required title="Enter Beneficiary Bank Name" maxlength="60">
                            </div>

                            <div class="col-md-6">
                                <label>Beneficiary Bank Branch Name </label>
                                <input type="text" name="bank_benificialbranchname" class="form-control-1" placeholder="Enter Beneficiary Bank Branch Name"  required title="Enter Beneficiary Bank Branch Name" maxlength="60">
                            </div>

                        </div>
                    </div>


                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Bank Address</label>
                                <input type="text" name="bank_benificialaddress" class="form-control-1" placeholder="Enter Bank Address"  required title="Enter Bank Address" maxlength="60">
                            </div>

                            <div class="col-md-6">
                                <label> IFSC Code/SORT/ABA </label>
                                <input type="text" name="bank_benificialifsc" class="form-control-1" placeholder="Enter IFSC Code/SORT/ABA "  required title="Enter IFSC Code/SORT/ABA " maxlength="60">
                            </div>

                        </div>
                    </div>


                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Swift Code</label>
                                <input type="text" name="bank_benificialswiftcode" class="form-control-1" placeholder="Enter Swift Code"  required title="Enter Swift Code" maxlength="60">
                            </div>

                            <div class="col-md-6">
                                <label>IBAN No</label>
                                <input type="text" name="bank_benificialibanno" class="form-control-1" placeholder="Enter IBAN No"  required title="Enter IBAN No" maxlength="60">
                            </div>

                        </div>
                    </div>

                    <h2> Intermediatery bank</h2>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Bank name</label>
                                <input type="text" name="intermidiatebankname" class="form-control-1" placeholder="Enter Bank name"  required title="Enter Bank name" maxlength="60">
                            </div>

                            <div class="col-md-6">
                                <label>Bank Address</label>
                                <input type="text" name="intermidiatebankaddress" class="form-control-1" placeholder="Enter Bank Address"  required title="Enter Bank Address" maxlength="60">
                            </div>

                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label>SWIFT Code</label>
                                <input type="text" name="intermidiatebankswiftcode" class="form-control-1" placeholder="Enter SWIFT Code"  required title="Enter SWIFT Code" maxlength="60">
                            </div>

                            <div class="col-md-6">
                                <label>ECS form duly signed by authorised person attached?</label>
                                </label>
                                <select  name="bank_ecs" class="form-control-1">
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>

                                 </div>

                        </div>
                    </div>









                    <h2> Documents Required</h2>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label class="control-label"> Copy of PAN CARD</label>
                                <input  type="file" name="copypancart" class="form-control-1"  required>	</div>


                            <div class="col-md-6">
                                <label class="control-label">  Copy of Service Tax/ Sales Tax/ VAT</label>
                                <input  type="file" name="copyservicetax" class="form-control-1" required>	</div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label class="control-label"> Copy of TAN</label>
                                <input  type="file" name="copytan" class="form-control-1" required>	</div>


                            <div class="col-md-6">
                                <label class="control-label"> Address Proof</label>
                                <input  type="file" name="copyaddressproof" class="form-control-1" required>	</div>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="row" style="text-align:center;">
                            <input type="submit" name="submit" class="btn btn-lg btn-primary" value="submit" >

            </div>


        </div>
    </div>


</section></form>

<!-- End Container -->






<!-- Common scripts -->
<script SRC="js/jquery-1.11.2.min.js"></script>
<script SRC="js/common_scripts_min.js"></script>
<script SRC="js/functions.js"></script>


</body>
</html>