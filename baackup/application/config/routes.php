<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/


/* B2c */
$route['default_controller'] = 'webapp/webapp/landing';
$route['packages'] 	 		 = 'webapp/Packages/packages';
$route['home']               = 'webapp/webapp/index';
$route['orderstatusresult'] = 'webapp/Cms/orderstatusresult';
$route['admin']		 		 = 'admin/login';

$route['ordersucess'] 	 		 = 'webapp/Ordersucess/ordersucess';
$route['orderfail'] 	 		 = 'webapp/Ordersucess/orderfail';
$route['orderagentsucess'] 	 		 = 'webapp/Ordersucess/orderagentsucess';

$route['ordersucesswallet'] 	 		 = 'webapp/Ordersucess/ordersucesswallet';
$route['agentamountsucess'] 	 		 = 'webapp/Ordersucess/agentamountsucess';
$route['agentamountfail'] 	 		 = 'webapp/Ordersucess/agentamountfail';
$route['agentsucess'] 	 		 = 'webapp/Ordersucess/agentsucess';


$route['emailverified'] 	 		 = 'webapp/User/emailverified';
$route['emailverifiedsucess'] 	 		 = 'webapp/User/emailverifiedsucess';
$route['forgetpassword'] 	 		 = 'webapp/User/forgetpassword';
$route['group_booking'] = 'webapp/group/groupbooking';
$route['userlogin'] = 'webapp/User/userlogin';
$route['userregister'] = 'webapp/User/userregister';
$route['myaccount'] = 'webapp/User/myaccount';
$route['cancelorder'] = 'webapp/User/cancelorder';
$route['logout'] = 'webapp/User/logout';
$route['activities'] = 'webapp/Cms/activities';
$route['park_map'] = 'webapp/Cms/park_map';
$route['image_gallery'] = 'webapp/Media/image_gallery';
$route['video_gallery'] = 'webapp/Media/video_gallery';
$route['paclanding'] = 'webapp/webapp/paclanding';
$route['orderaborted'] = 'webapp/webapp/orderaborted';
$route['bookingdesc'] = 'webapp/Cms/bookingdesc';


$route['habitat'] = 'webapp/Media/habitat';
$route['press_releases'] = 'webapp/Cms/press_releases';
$route['contacts'] = 'webapp/Cms/contacts';
$route['about_us'] = 'webapp/Cms/about_us';
$route['marketing_promotion'] = 'webapp/Cms/marketing_promotion';
$route['park_rules'] = 'webapp/Cms/park_rules';
$route['privacy_policy'] = 'webapp/Cms/privacy_policy';
$route['sitemap'] = 'webapp/Cms/sitemap';
$route['operating_hours'] = 'webapp/Cms/operating_hours';
$route['tearmcondition'] = 'webapp/Cms/tearmcondition';


$route['tickets'] = 'webapp/Cms/tickets';
$route['operating_hours'] = 'webapp/Cms/operating_hours';
$route['facilities'] = 'webapp/Cms/facilities';
$route['faqs'] = 'webapp/Cms/faqs';
$route['bookingquery'] = 'webapp/Cms/bookingquery';
/* B2b */   

$route['partnerlogin']               = 'webapp/Partner/partnerlogin';
$route['agentregistration']               = 'webapp/Partner/agentregistration';
$route['agentregistrationupdate']               = 'webapp/Partner/agentregistrationupdate';
$route['partnerdashboard']               = 'webapp/Partner/dashboard';
$route['agentforget']               = 'webapp/Partner/agentforget';
$route['agentform1']               = 'webapp/Partner/agentform1';
$route['agentform2']               = 'webapp/Partner/agentform2';
$route['agentform3']               = 'webapp/Partner/agentform3';
$route['agentform4']               = 'webapp/Partner/agentform4';
$route['agentform5']               = 'webapp/Partner/agentform5';
$route['agentform6']               = 'webapp/Partner/agentform6';
$route['agentstatus']               = 'webapp/Partner/agentstatus';
$route['agentedit']               = 'webapp/Partner/agentedit';
$route['agentformverify']               = 'webapp/Partner/agentformverify';
$route['partnerlogout']               = 'webapp/Partner/partnerlogout';
$route['updatepartnerrprofile']               = 'webapp/Partner/updatepartnerrprofile';

$route['partners']               = 'webapp/Partner/partners';
$route['partnerusercreate']               = 'webapp/Partner/partnerusercreate';


$route['changepartnerpassword']               = 'webapp/Partner/changepartnerpassword';
$route['order']               = 'webapp/Partner/order';
$route['order_sess']               = 'webapp/Partner/order_sess';
$route['updateamount']               = 'webapp/Partner/updateamount';
$route['mywallet']               = 'webapp/Partner/mywallet';
$route['orderstatusresultupdate'] = 'webapp/Cms/orderstatusresultupdate';
$route['partnerbookingsearch'] = 'webapp/Partnerbooking/partnerbookingsearch';
$route['agentpackagesstep'] = 'webapp/Partnerbooking/agentpackagesstep';
$route['agentaddones'] = 'webapp/Partnerbooking/agentaddones';
$route['partnersummary'] = 'webapp/Partnerbooking/partnersummary';
$route['partnerpaymentagent'] = 'webapp/Partnerbooking/partnerpaymentagent';

$route['partners']               = 'webapp/Partner/partners';
$route['partnerusercreate']               = 'webapp/Partner/partnerusercreate';
$route['agentedit']               = 'webapp/Partner/agentedit';
$route['agentformverify']               = 'webapp/Partner/agentformverify';
$route['partnerlogout']               = 'webapp/Partner/partnerlogout';
$route['updatepartnerrprofile']               = 'webapp/Partner/updatepartnerrprofile';


$route['order']               = 'webapp/Partner/order';
$route['order_sess']               = 'webapp/Partner/order_sess';
$route['updateamount']               = 'webapp/Partner/updateamount';
$route['mywallet']               = 'webapp/Partner/mywallet';
$route['orderstatusresultupdate'] = 'webapp/Cms/orderstatusresultupdate';
/* Checkout */

$route['packagesstep'] 	 		 = 'webapp/Packagesstep/packagesstep';
$route['packagesajaxadd'] 	 		 = 'webapp/Packagesstep/packagesajaxadd';
$route['addones'] 	 		 = 'webapp/Addones/addones';
$route['step4'] 	 		 = 'webapp/Addones/step4';

$route['summary'] 	 		 = 'webapp/Addones/summary';
$route['payment'] 	 		 = 'webapp/Addones/payment';
$route['paymentagent'] 	 		 = 'webapp/Addones/paymentagent';
$route['timeslotadd'] = 'webapp/Cronejob/timeslotadd';

$route['messageadd'] = 'webapp/Message/messageadd';
$route['message'] = 'webapp/Message/message';
$route['messagedisplay'] = 'webapp/Message/messagedisplay';

$route['install'] = 'webapp/webapp/installa';
/* CCAVENUE */

$route['ccavRequestHandler'] 	 		 = 'webapp/Ccavenue/ccavRequestHandler';
$route['ccavResponseHandler'] 	 		 = 'webapp/Ccavenue/ccavResponseHandler';

$route['partnerbookingsearch'] = 'webapp/Partnerbooking/partnerbookingsearch';
$route['agentpackagesstep'] = 'webapp/Partnerbooking/agentpackagesstep';
$route['agentaddones'] = 'webapp/Partnerbooking/agentaddones';
$route['partnersummary'] = 'webapp/Partnerbooking/partnersummary';
$route['partnerpaymentagent'] = 'webapp/Partnerbooking/partnerpaymentagent';
$route['paccoupon']               = 'webapp/webapp/paccoupon';
$route['404_override'] = 'webapp/webapp/errorfnf';
$route['errorfnf']               = 'webapp/webapp/errorfnf';

