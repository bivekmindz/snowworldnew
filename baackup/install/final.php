<?php
$title = "";
$username = "";
$password ="";
$email = "";
session_start();


mysql_connect($_SESSION['host'],$_SESSION['username'],$_SESSION['password'] ) or die("Unable to Connect");

mysql_select_db($_SESSION['database']) or die("Unable to Select");

$root = '../' . dirname($_SERVER['SCRIPT_NAME']);
function is_alpha($val)
    {
        return (bool)preg_match("/^([a-zA-Z ])+$/i", $val);
 
    }
function checkEmail($email)
{
  return preg_match('/^\S+@[\w\d.-]{2,}\.[\w]{2,6}$/iU', $email) ? TRUE : FALSE;
}
error_reporting(0);
$error = "";
if(isset($_POST['title']))
{
$title = $_POST['title'];
$username = $_POST['username'];
$password = md5($_POST['password']);
$email = $_POST['email'];
if(!is_alpha($username))
{
$error .="o. Username Can Only Contain Letters a-Z<br />";
}
if(strlen($username)<1 || strlen($username)>5)
{
$error .="o. Username Length Must Be Between 1 to 5 Characters<br />";
}
if(strlen($_POST["password"])<1 || strlen($_POST["password"])>18)
{
$error .="o. Password Length Must Be Between 1 to 18 Characters<br />";
}
$title=mysql_real_escape_string($title);
$update_query = mysql_query("
INSERT INTO `tbladminmenu` (`id`, `menuname`, `parentid`, `url`, `position`, `amlevel`, `r_status`) VALUES
(1, 'Manage Geographic', 0, 'admin/geographic/geomaster', '', 13, 'Active'),
(13, 'Country', 0, 'admin/geographic/viewcountry', NULL, NULL, 'Inactive'),
(14, 'State', 0, 'admin/geographic/statelist', NULL, NULL, 'Inactive'),
(15, 'City', 0, 'admin/geographic/viewcity', NULL, NULL, 'Inactive'),
(16, 'Location', 0, 'admin/geographic/locationlist', NULL, NULL, 'Inactive'),
(17, 'PromoCode', 0, '', '', 5, 'Active'),
(18, 'Add Promo Code', 17, 'admin/promocodes/s_promo', NULL, NULL, 'Active'),
(19, 'View Promo Code', 17, 'admin/promocodes/s_viewpromo', NULL, NULL, 'Active'),
(20, 'Activity', 0, NULL, '', 1, 'Inactive'),
(21, 'Add Activity', 20, 'admin/activity/addactivity', NULL, NULL, 'Active'),
(22, 'Branch', 0, NULL, '', 2, 'Inactive'),
(23, 'Add Branch', 22, 'admin/branchs/addbranch', NULL, NULL, 'Active'),
(24, 'Company', 0, NULL, '', 3, 'Inactive'),
(25, 'Add Company', 24, 'admin/companys/s_addCompany', NULL, NULL, 'Active'),
(26, 'Package', 0, NULL, '', 4, 'Inactive'),
(27, 'Add Package', 26, 'admin/packages/addPackage', NULL, NULL, 'Active'),
(28, 'Addon', 0, NULL, '', 5, 'Inactive'),
(29, 'Add Addon', 28, 'admin/addon/addAddon', NULL, NULL, 'Active'),
(30, 'Time Slot', 0, NULL, '', 6, 'Inactive'),
(31, 'Add Timeslot', 30, 'admin/timeslots/addTimeslot', NULL, NULL, 'Active'),
(32, 'Geography Master', 1, 'admin/geographic/geomaster', NULL, NULL, 'Active'),
(33, 'Role Management', 0, NULL, '', 6, 'Active'),
(34, 'Add Role ', 33, 'admin/managerole/addrole', NULL, NULL, 'Inactive'),
(35, 'Add Employee', 33, 'admin/managerole/addemployee', NULL, NULL, 'Active'),
(37, 'Role Assign to employee', 33, 'admin/managerole/assignroles', NULL, NULL, 'Inactive'),
(38, 'Order Management', 0, NULL, '', 1, 'Active'),
(39, 'ALL ORDER LIST', 38, 'admin/ordercontrol/order', NULL, NULL, 'Active'),
(40, 'Group Booking <br> Management', 0, NULL, '', 9, 'Active'),
(41, 'BOOKING TYPE MASTER', 40, 'admin/groupbooking/bookingtype', NULL, NULL, 'Active'),
(42, 'Media Galary', 0, NULL, '', 3, 'Inactive'),
(43, 'Image', 42, 'admin/galary/image', NULL, NULL, 'Active'),
(44, 'Video', 42, 'admin/galary/video', '', 9, 'Active'),
(45, 'Habitat', 42, 'admin/galary/habitat', '', 9, 'Active'),
(46, 'Press Release', 42, 'admin/galary/pressrelease', '', 9, 'Active'),
(47, 'PENDING ORDERS', 38, 'admin/ordercontrol/pendingorder', NULL, NULL, 'Active'),
(48, 'SUCESS ORDERS', 38, 'admin/ordercontrol/sucessgorder', NULL, NULL, 'Active'),
(49, 'Registered User', 0, NULL, '', 2, 'Active'),
(50, 'User List', 49, 'admin/user/reguser', NULL, NULL, 'Active'),
(51, 'Group Order Management', 0, NULL, '', 1, 'Inactive'),
(52, 'GROUP SUCESS ORDERS', 51, 'admin/ordercontrolgp/sucessgordergp', NULL, NULL, 'Inactive'),
(53, 'GROUP PENDING ORDERS', 51, 'admin/ordercontrolgp/pendingordergp', NULL, NULL, 'Inactive'),
(54, 'ALL GROUP ORDER LIST', 51, 'admin/ordercontrolgp/ordergp', NULL, NULL, 'Active'),
(55, 'Manage Mail', 0, NULL, '', 3, 'Active'),
(56, 'MAIL VIEW', 55, 'admin/managemail/viewmail', NULL, NULL, 'Active'),
(57, 'Inventory & Agent', 0, NULL, '', 3, 'Active'),
(58, 'Agent Regestration', 57, 'admin/b2b/agent', NULL, NULL, 'Active'),
(59, 'Date wise Inventory', 57, 'admin/inventory/datewiseinventory', NULL, NULL, 'Active'),
(60, 'Inventory Calender', 57, 'admin/inventory/calenderview', NULL, NULL, 'Active'),
(61, 'All Master Manager', 0, NULL, '', 1, 'Active'),
(62, 'Manage  Master', 61, 'admin/allmanage/managemaster', NULL, NULL, 'Active');
");
$update_query = mysql_query("
INSERT INTO `tblassignmenu` (`assign_id`, `role_id`, `emp_id`, `main_menu`, `sub_menu`) VALUES
(1, 5, 2, '17', '19'),
(2, 5, 2, '17', '19'),
(3, 9, 2, '1', '32'),
(4, 9, 2, '17', '19'),
(5, 5, 2, '22', '23'),
(6, 9, 2, '20', '21'),
(7, 11, 6, '1', '32'),
(8, 11, 6, '17', '18'),
(9, 11, 6, '17', '19'),
(10, 11, 6, '20', '21'),
(11, 11, 6, '22', '23');");
$update_query = mysql_query("INSERT INTO `tbl_adminlogin` (`LoginID`, `UserName`, `Password`, `EmployeeId`, `Role`, `EmpStatus`, `Modified_By`, `Modified_On`, `CreatedOn`) VALUES
(1, 'snowworld@gmail.com', 'c822c1b63853ed273b89687ac505f9fa', NULL, NULL, 'A', NULL, NULL, '2016-02-15 18:19:29');");



$update_query = mysql_query("update tbl_adminlogin  set UserName='".$email."',Password='".md5($_POST["password"])."' ");



	
}


?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="robots" content="index, follow" />
  <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	  	    <link href="bootstrapinstaller.min.css" rel="stylesheet">
    <link href="bootstrapinstaller-responsive.min.css" rel="stylesheet">
	
    <script type="text/javascript" src="../js/assets/jquery-1.9.0.min.js"></script>
	 <script src="../js/bootstrap.min.js"></script>
<title>Installation:   Step 2</title>
</head>
<body style="padding-top:0">
<div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <button data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar" type="button">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a href="#" class="brand"> Booking Process</a>
        </div>
      </div>
    </div>
<div id="wrap">
       <div class="container" style="padding-top:150px">
    <div class="row">
    <div class="offset3 span6">
                <h1>Website Information : Step 2 </h1>
				<br> <br>
<?php
if(isset($_POST['title']) || isset($_POST['email']) || isset($_POST['username']) || isset($_POST['password']))
{ ?>
<form action="final.php" method="post">
<legend>Website Title</legend>
<input class="span5" type="text" name="title" value="<?php echo($_POST['title']) ?>"/>
<?php
if(strlen($title)<1 || strlen($title)>50)
{
echo('<br /><span class="label label-important">Title Length Must Be Between 1 to 70 Characters</span>');
}
?>
<legend>Admin Email</legend>
<input class="span5" type="text" name="username" value="<?php echo($_POST['username']) ?>"/>

<legend>Admin Password</legend>
<input class="span5" type="password" name="password" value="<?php echo($_POST['password']) ?>"/>
<?php
if(strlen($_POST["password"])<1 || strlen($_POST["password"])>20)
{
echo('<br /><span class="label label-important">Password Length Must Be Between 1 to 20 Characters</span>');
}
?>
<legend></legend>
<input type="submit" class="btn btn-large btn-primary" value="Finish">
</form>
<?php 
}
else
{ ?>
<form action="final.php" method="post">
<legend>Website Title</legend>
<input class="span5" type="text" name="title"/>
<legend>Admin Username</legend>
<input class="span5" type="text" name="username"/>
<legend>Admin Email</legend>
<input class="span5" type="text" name="email"/>
<legend>Admin Password</legend>
<input class="span5" type="password" name="password"/>
<legend></legend>
<input type="submit" class="btn btn-large btn-primary" value="Finish">
<a href="index.php" class="btn btn-large btn-primary">Back</a>
</form>
    </div>
	<?php
}
if(isset($_POST['title']) && $_POST['title']!="")
{
?>
<span class="label label-success">Installation Successful Please Wait Adding Products It may Take Some Minuets</span>
<?php 
echo ('<META HTTP-EQUIV="Refresh" Content="2; URL=../packages">');
} 
?>
    </div>
    </div>
<?php 
include '../common/footer.php';
?>